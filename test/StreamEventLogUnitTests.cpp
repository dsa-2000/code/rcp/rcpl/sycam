// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "StreamEventLog.hpp"
#include "libsymcam.hpp"
#include "gtest/gtest.h"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

// state with two integer members
struct StateA : public rcp::symcam::StreamStateBase<StateA> {

  int i{};
  int j{};

  StateA() noexcept = default;
  StateA(int _i, int _j) noexcept : i{_i}, j{_j} {}
  StateA(StateA const&) noexcept = default;
  StateA(StateA&&) noexcept = default;
  auto
  operator=(StateA const&) noexcept -> StateA& = default;
  auto
  operator=(StateA&&) noexcept -> StateA& = default;
  ~StateA() noexcept = default;

  auto
  operator==(StateA const& rhs) const noexcept -> bool {
    return i == rhs.i && j == rhs.j;
  }

  auto
  operator!=(StateA const& rhs) const noexcept -> bool {
    return !(*this == rhs);
  }
};

// event that increments the "i" member of a StateA instance
struct FooEvent : public rcp::symcam::StreamStateEvent<StateA, FooEvent> {

  FooEvent(rcp::symcam::StreamClock::timestamp_t ts) noexcept
    : rcp::symcam::StreamStateEvent<StateA, FooEvent>(ts) {}
  FooEvent() = default;
  FooEvent(FooEvent const&) = default;
  FooEvent(FooEvent&&) noexcept = default;
  /*! \brief copy constructor with updated timestamp */
  FooEvent(
    FooEvent const& event, rcp::symcam::StreamClock::timestamp_t const& ts)
    : rcp::symcam::StreamStateEvent<StateA, FooEvent>(event, ts) {}
  /*! \brief move constructor with updated timestamp */
  FooEvent(FooEvent&& event, rcp::symcam::StreamClock::timestamp_t const& ts)
    : rcp::symcam::StreamStateEvent<StateA, FooEvent>(std::move(event), ts) {}
  auto
  operator=(FooEvent const&) -> FooEvent& = default;
  auto
  operator=(FooEvent&&) noexcept -> FooEvent& = default;
  ~FooEvent() override = default;

  [[nodiscard]] auto
  tag() const noexcept -> char const* override {
    return "FooEvent";
  }

  [[nodiscard]] auto
  apply_to(StateA&& st) const -> StateA override {
    st.i++;
    return this->set_timestamp(st);
  }
};

// event that increments the "j" member of a StateA instance
struct BarEvent : public rcp::symcam::StreamStateEvent<StateA, BarEvent> {

  BarEvent(rcp::symcam::StreamClock::timestamp_t ts) noexcept
    : rcp::symcam::StreamStateEvent<StateA, BarEvent>(ts) {}
  BarEvent() = default;
  BarEvent(BarEvent const&) = default;
  BarEvent(BarEvent&&) noexcept = default;
  /*! \brief copy constructor with updated timestamp */
  BarEvent(
    BarEvent const& event, rcp::symcam::StreamClock::timestamp_t const& ts)
    : rcp::symcam::StreamStateEvent<StateA, BarEvent>(event, ts) {}
  /*! \brief move constructor with updated timestamp */
  BarEvent(BarEvent&& event, rcp::symcam::StreamClock::timestamp_t const& ts)
    : rcp::symcam::StreamStateEvent<StateA, BarEvent>(std::move(event), ts) {}
  auto
  operator=(BarEvent const&) -> BarEvent& = default;
  auto
  operator=(BarEvent&&) noexcept -> BarEvent& = default;
  ~BarEvent() override = default;

  [[nodiscard]] auto
  tag() const noexcept -> char const* override {
    return "BarEvent";
  }

  [[nodiscard]] auto
  apply_to(StateA&& st) const -> StateA override {
    st.j++;
    return this->set_timestamp(st);
  }
};

TEST(StreamEventLog, BasicTests) {
  using namespace rcp::symcam;
  using external_time = StreamClock::external_time;

  external_time t_foo{external_time::min() + StreamClock::duration{10}};
  auto foo = std::make_shared<FooEvent>(t_foo);
  external_time t_bar{external_time::min() + StreamClock::duration{20}};
  auto bar = std::make_shared<BarEvent>(t_bar);

  // check initial values of default constructed log
  StreamEventLog<StateA> log;
  EXPECT_EQ(log.state(), StateA());
  EXPECT_EQ(log.timestamp(), external_time::min());

  // record two events
  auto log1 = log.record(bar).record(foo);
  // check that initial state and timestamp of log are unchanged
  EXPECT_EQ(log1.state(), log.state());
  EXPECT_EQ(log1.timestamp(), log.timestamp());

  {
    // try recording an event in the past
    auto log2 = log1.record(std::make_shared<FooEvent>(--log1.timestamp()));
    EXPECT_EQ(log2.state_at(log1.timestamp()), log1.state());
  }

  // evaluate log at time 0
  auto state0 = log1.state_at(external_time::min());
  EXPECT_EQ(state0, StateA(0, 0));
  EXPECT_EQ(state0.timestamp(), external_time::min());
  // evaluate log at time t_foo
  auto state1 = log1.state_at(t_foo);
  EXPECT_EQ(state1, StateA(1, 0));
  EXPECT_EQ(state1.timestamp(), t_foo);
  // evaluate log at time t_bar
  auto state2 = log1.state_at(t_bar);
  EXPECT_EQ(state2, StateA(1, 1));
  EXPECT_EQ(state2.timestamp(), t_bar);

  {
    // roll up log to time t_foo and evaluate the new log
    auto lg = log1.rollup(t_foo);
    EXPECT_EQ(lg.state(), StateA(1, 0));
    EXPECT_EQ(lg.timestamp(), t_foo);
    // check that log1 is unchanged
    EXPECT_EQ(log1.state(), log.state());
    EXPECT_EQ(log1.timestamp(), log.timestamp());
  }
  {
    // roll up log to time t_bar and evaluate the new log
    auto lg = log1.rollup(t_bar);
    EXPECT_EQ(lg.state(), StateA(1, 1));
    EXPECT_EQ(lg.timestamp(), t_bar);
  }
  // roll up log1 to time t_bar using rvalue reference target and evaluate the
  // new log
  log1 = std::move(log1).rollup(t_bar);
  EXPECT_EQ(log1.state(), StateA(1, 1));
  EXPECT_EQ(log1.timestamp(), t_bar);

  // check effects of event with zero-valued relative timestamp
  auto log2 =
    log1.record(std::make_shared<FooEvent>(StreamClock::relative_time{0}));
  EXPECT_GT(log2.next_timestamp(), log2.timestamp());
  auto state3 = log2.state_at(log2.next_timestamp());
  EXPECT_EQ(state3, StateA(2, 1));
  EXPECT_EQ(log2.timestamp(), t_bar);

  // record both events at once, and do similar tests
  auto log3 = log.record(
    std::vector<std::shared_ptr<StreamStateEventBase<StateA>>>{bar, foo});
  {
    // roll up log to time t_foo and evaluate the new log
    auto lg = log3.rollup(t_foo);
    EXPECT_EQ(lg.state(), StateA(1, 0));
    EXPECT_EQ(lg.timestamp(), t_foo);
  }
  {
    // roll up log to time t_bar and evaluate the new log
    auto lg = log3.rollup(t_bar);
    EXPECT_EQ(lg.state(), StateA(1, 1));
    EXPECT_EQ(lg.timestamp(), t_bar);
  }
}
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
