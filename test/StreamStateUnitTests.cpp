// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "ImagingEvent.hpp"
#include "StringValueEvent.hpp"
#include "SubarrayEvent.hpp"
#include "libsymcam.hpp"
#include "gtest/gtest.h"

#include <nlohmann/json.hpp>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,
// readability-magic-numbers,cppcoreguidelines-pro-bounds-array-to-pointer-decay)

TEST(StreamState, BasicTests) {
  using namespace rcp::symcam::st;
  using namespace rcp::symcam;

  auto ts = rcp::symcam::StreamClock::external_time::min();
  subarray_name_t main{"main"};
  subarray_t subarray;
  ExtendedState state;
  EXPECT_TRUE(state.subarrays().empty());
  EXPECT_TRUE(state.is_alive());
  state = std::move(state).set_subarray(0, main, subarray);
  {
    State::img_id_t img_id = 10;
    auto state1 = state.apply(StartImaging(main, img_id, ts));
    EXPECT_TRUE(state1.is_imaging(0));
    EXPECT_TRUE(state1.imaging_id(0));
    EXPECT_EQ(*state1.imaging_id(0), img_id);
    EXPECT_EQ(state1.timestamp(), ts);
    state1 = std::move(state1).apply(StopImaging(main, ts));
    EXPECT_FALSE(state1.is_imaging(0));
    EXPECT_FALSE(state1.imaging_id(0));
    EXPECT_EQ(state1.timestamp(), ts);
    state1 = std::move(state1).apply(PoisonPill(ts));
    EXPECT_FALSE(state1.is_alive());
    EXPECT_EQ(state1.timestamp(), ts);
  }
  {
    State::img_id_t img_id = 11;
    auto state1 = state.apply(StartImaging(main, img_id, ts));
    EXPECT_TRUE(state1.is_imaging(0));
    state1 = std::move(state1).apply(PoisonPill(ts));
    EXPECT_FALSE(state1.is_imaging(0));
    // no transition out of dead state
    state1 = std::move(state1).apply(StartImaging(main, img_id, ts));
    EXPECT_FALSE(state1.is_imaging(0));
  }
}

TEST(EventLog, BasicTests) {
  using namespace rcp::symcam::st;
  using namespace rcp::symcam;

  subarray_name_t main{"main"};
  subarray_t subarray;
  ExtendedState state0;
  state0 = std::move(state0).set_subarray(0, main, subarray);
  EventLog log(std::move(state0), true);
  {
    auto pp = std::make_shared<PoisonPill>(StreamClock::relative_time{0});
    auto log1 = log.record(pp);
    State state = log1.rollup(log1.next_timestamp()).state();
    EXPECT_FALSE(state.is_alive());
    EXPECT_EQ(state.timestamp(), log1.next_timestamp());
  }
  {
    auto delay = std::chrono::seconds{10};
    auto pp = std::make_shared<PoisonPill>(StreamClock::relative_time(delay));
    auto ts = log.next_timestamp() + delay;
    auto log1 = log.record(pp);
    auto state1 = log1.state_at(log.next_timestamp());
    EXPECT_TRUE(state1.is_alive());
    EXPECT_EQ(state1.timestamp(), log.next_timestamp());
    auto state2 = log1.state_at(ts);
    EXPECT_FALSE(state2.is_alive());
  }
  {
    State::img_id_t img_id = 72;
    auto delay = std::chrono::seconds{10};
    auto start = log.next_timestamp() + delay;
    auto duration = std::chrono::minutes{10};
    auto start_img = std::make_shared<StartImaging>(
      main, img_id, StreamClock::relative_time(delay));
    auto stop_img = std::make_shared<StopImaging>(
      main, StreamClock::relative_time(delay + duration));
    auto log1 = log.record(start_img).record(stop_img);
    auto state1 = log1.state_at(log.next_timestamp());
    auto state2 = log1.state_at(start);
    EXPECT_TRUE(state2.is_imaging(0));
    auto state3 = log1.state_at(start + duration);
    EXPECT_FALSE(state3.is_imaging(0));
  }
  {
    State::img_id_t img_id = 73;
    auto delay = std::chrono::seconds{10};
    auto start = log.next_timestamp() + delay;
    auto duration = std::chrono::minutes{10};
    auto interrupt = duration / 2;
    auto log1 = log
                  .record(std::make_shared<StartImaging>(
                    main, img_id, StreamClock::relative_time(delay)))
                  .record(std::make_shared<StopImaging>(
                    main, StreamClock::relative_time(delay + duration)))
                  .record(std::make_shared<PoisonPill>(
                    StreamClock::external_time(start + interrupt)));
    auto state1 = log1.state_at(start);
    EXPECT_TRUE(state1.is_imaging(0));
    auto state2 = log1.state_at(start + interrupt);
    EXPECT_FALSE(state2.is_imaging(0));
    EXPECT_FALSE(state2.is_alive());
  }
}

TEST(Subarrays, JSON) {

  using namespace rcp::symcam::st;
  using namespace rcp::symcam;
  using json = nlohmann::json;

  CreateSubarray first{
    0,
    subarray_name_t{"first"},
    ReceiverPairs{{{0, 1}, {1, 2}, {3, 4}}},
    StreamClock::external_time::min()};
  json first_js = first;
  ASSERT_NO_THROW(first_js.get<CreateSubarray>());
  EXPECT_EQ(first, first_js.get<CreateSubarray>());
  auto firstp = get_event(first_js);
  EXPECT_EQ(firstp->tag(), CreateSubarray::class_tag);
  EXPECT_EQ(dynamic_cast<CreateSubarray&>(*firstp), first);

  CreateSubarray second{
    1,
    subarray_name_t{"second"},
    ReceiverSetOuterProduct{ReceiverSet::all()},
    StreamClock::external_time::min()};
  json second_js = second;
  ASSERT_NO_THROW(second_js.get<CreateSubarray>());
  EXPECT_EQ(second, second_js.get<CreateSubarray>());
}

TEST(SimpleEvents, JSON) {

  using namespace rcp::symcam::st;
  using namespace rcp::symcam;
  using namespace std::chrono_literals;
  using json = nlohmann::json;

  State::img_id_t img_id = 35;
  subarray_name_t main{"main"};

  auto const start =
    StartImaging{main, img_id, StreamClock::external_time::min()};
  auto const pp = PoisonPill{StreamClock::external_time::min()};
  EXPECT_EQ(start, StartImaging(start));
  EXPECT_TRUE(
    start.timestamp() < start.timestamp() + StreamClock::duration{10});
  EXPECT_EQ(start.name(), main);

  json start_js = start;
  ASSERT_NO_THROW(start_js.get<StartImaging>());
  EXPECT_EQ(start_js.get<StartImaging>(), start);
  EXPECT_THROW(start_js.get<PoisonPill>(), st::BadEventTagError);

  json pp_js = pp;
  ASSERT_NO_THROW(pp_js.get<PoisonPill>());
  EXPECT_EQ(pp_js.get<PoisonPill>(), pp);

  EXPECT_EQ(
    start_js,
    as_json(static_cast<StreamStateEventBase<ExtendedState> const&>(start)));
  EXPECT_EQ(
    pp_js,
    as_json(static_cast<StreamStateEventBase<ExtendedState> const&>(pp)));

  subarray_t subarray;
  auto create_ts =
    StreamClock::external_time::min() + StreamClock::duration{100};
  auto const create = CreateSubarray{0, main, subarray, create_ts};
  json create_js = create;
  ASSERT_NO_THROW(create_js.get<CreateSubarray>());
  auto ccopy = create_js.get<CreateSubarray>();
  EXPECT_EQ(create_js.get<CreateSubarray>(), create);

  auto destroy_ts = create_ts + StreamClock::duration{100};
  auto const destroy = DestroySubarray{main, destroy_ts};
  json destroy_js = destroy;
  ASSERT_NO_THROW(destroy_js.get<DestroySubarray>());
  EXPECT_EQ(destroy_js.get<DestroySubarray>(), destroy);

  // For initial implementation of CreateSubarray, I neglected to copy the
  // m_subarray member in the constructors that update the event timestamp. I
  // can see this being a fairly common error with new event implementations, so
  // something like the following should be added to the unit tests for all
  // event types.
  auto create_offset = create.with_timestamp_offset(1h);
  EXPECT_EQ(
    const_cast<CreateSubarray const&>(create).subarray(),
    ((CreateSubarray const*)(create_offset.get()))->subarray());

  auto const record = StartRecordEventStream{main, "/a/b/c", destroy_ts};
  json record_js = record;
  ASSERT_NO_THROW(record_js.get<StartRecordEventStream>());
  EXPECT_EQ(record_js.get<StartRecordEventStream>(), record);
  auto record_offset = record.with_timestamp_offset(1h);
  EXPECT_EQ(
    const_cast<StartRecordEventStream const&>(record).name(),
    ((StartRecordEventStream const*)(record_offset.get()))->name());

  auto const unrecord = StopRecordEventStream{main, destroy_ts + 2s};
  json unrecord_js = unrecord;
  ASSERT_NO_THROW(unrecord_js.get<StopRecordEventStream>());
  EXPECT_EQ(unrecord_js.get<StopRecordEventStream>(), unrecord);
  auto unrecord_offset = unrecord.with_timestamp_offset(1h);
  EXPECT_EQ(
    const_cast<StopRecordEventStream const&>(unrecord).name(),
    ((StopRecordEventStream const*)(unrecord_offset.get()))->name());

  auto const import_fftw = ImportFFTWWisdom{"/a/b/c", destroy_ts};
  json import_js = import_fftw;
  ASSERT_NO_THROW(import_js.get<ImportFFTWWisdom>());
  EXPECT_EQ(import_js.get<ImportFFTWWisdom>(), import_fftw);
  auto import_offset = import_fftw.with_timestamp_offset(1h);
  EXPECT_EQ(
    const_cast<ImportFFTWWisdom const&>(import_fftw).value(),
    ((ImportFFTWWisdom const*)(import_offset.get()))->value());

  auto const export_fftw = ExportFFTWWisdom{"/a/b/c", destroy_ts + 2s};
  json export_js = export_fftw;
  ASSERT_NO_THROW(export_js.get<ExportFFTWWisdom>());
  EXPECT_EQ(export_js.get<ExportFFTWWisdom>(), export_fftw);
  auto export_offset = export_fftw.with_timestamp_offset(1h);
  EXPECT_EQ(
    const_cast<ExportFFTWWisdom const&>(export_fftw).value(),
    ((ExportFFTWWisdom const*)(export_offset.get()))->value());
}

TEST(ImagingEvents, EventLogTests) {

  using namespace rcp::symcam::st;
  using namespace rcp::symcam;

  auto dt = StreamClock::duration{1000};
  subarray_name_t main{"main"};
  subarray_t subarray;
  ExtendedState state0;
  state0 = std::move(state0).set_subarray(0, main, subarray);

  State::img_id_t img_id0 = 86;
  auto start0_ts = StreamClock::external_time::min() + std::chrono::seconds{10};
  auto const start0 = StartImaging{main, img_id0, start0_ts};
  auto stop0_ts = start0_ts + dt;
  auto const stop0 = StopImaging{main, stop0_ts};

  State::img_id_t img_id1 = 87;
  auto start1_ts = start0_ts + dt / 2;
  auto const start1 = StartImaging{main, img_id1, start1_ts};
  auto stop1_ts = start1_ts + dt;
  auto const stop1 = StopImaging{main, stop1_ts};

  EventLog log(std::move(state0), true);
  log = std::move(log).record(
    std::vector<std::shared_ptr<StreamStateEventBase<ExtendedState>>>{
      std::make_shared<StartImaging>(start0),
      std::make_shared<StopImaging>(stop0),
      std::make_shared<StartImaging>(start1),
      std::make_shared<StopImaging>(stop1)});

  State state_init = log.state();
  EXPECT_FALSE(state_init.is_imaging(0));

  auto state_start0 = log.state_at(start0_ts);
  EXPECT_TRUE(state_start0.is_imaging(0));
  EXPECT_EQ(state_start0.timestamp(), start0_ts);
  EXPECT_EQ(*state_start0.imaging_id(0), img_id0);

  auto state_start1 = log.state_at(start1_ts);
  EXPECT_TRUE(state_start1.is_imaging(0));
  EXPECT_EQ(state_start1.timestamp(), start1_ts);
  EXPECT_EQ(*state_start1.imaging_id(0), img_id1);

  auto state_stop0 = log.state_at(stop0_ts);
  EXPECT_FALSE(state_stop0.is_imaging(0));
  EXPECT_EQ(state_stop0.timestamp(), stop0_ts);

  auto state_stop1 = log.state_at(stop1_ts);
  EXPECT_FALSE(state_stop1.is_imaging(0));
  EXPECT_EQ(state_stop1.timestamp(), stop1_ts);
}

TEST(SubarrayEvents, EventLogTests) {

  using namespace rcp::symcam::st;
  using namespace rcp::symcam;

  auto dt = StreamClock::duration{1000};

  auto subarray0_name = subarray_name_t{"foo"};
  subarray_t subarray0;
  auto create0_ts =
    StreamClock::external_time::min() + std::chrono::seconds{10};
  auto const create0 = CreateSubarray{0, subarray0_name, subarray0, create0_ts};
  auto destroy0_ts = create0_ts + dt;
  auto const destroy0 = DestroySubarray{subarray0_name, destroy0_ts};

  auto subarray1_name = subarray_name_t{"bar"};
  subarray_t subarray1;
  auto create1_ts = create0_ts + dt / 2;
  auto const create1 = CreateSubarray{1, subarray1_name, subarray1, create1_ts};
  auto destroy1_ts = create1_ts + dt;
  auto const destroy1 = DestroySubarray{subarray1_name, destroy1_ts};

  EventLog log;
  log = std::move(log).record(
    std::vector<std::shared_ptr<StreamStateEventBase<ExtendedState>>>{
      std::make_shared<CreateSubarray>(create0),
      std::make_shared<DestroySubarray>(destroy0),
      std::make_shared<CreateSubarray>(create1),
      std::make_shared<DestroySubarray>(destroy1)});

  State state_init = log.state();
  EXPECT_TRUE(state_init.subarray_names().empty());

  auto includes_name = [&](auto&& subarrays, auto&& name) {
    return std::ranges::find_if(
             subarrays, [&](auto&& sa) { return std::get<0>(sa) == name; })
           != subarrays.end();
  };

  auto state_create0 = log.state_at(create0_ts);
  EXPECT_EQ(state_create0.timestamp(), create0_ts);
  {
    auto subarrays = state_create0.subarrays();
    EXPECT_TRUE(includes_name(subarrays, subarray0_name));
    EXPECT_FALSE(includes_name(subarrays, subarray1_name));
  }

  auto state_create1 = log.state_at(create1_ts);
  EXPECT_EQ(state_create1.timestamp(), create1_ts);
  {
    auto subarrays = state_create1.subarrays();
    EXPECT_TRUE(includes_name(subarrays, subarray0_name));
    EXPECT_TRUE(includes_name(subarrays, subarray1_name));
  }

  auto state_destroy0 = log.state_at(destroy0_ts);
  EXPECT_EQ(state_destroy0.timestamp(), destroy0_ts);
  {
    auto subarrays = state_destroy0.subarrays();
    EXPECT_FALSE(includes_name(subarrays, subarray0_name));
    EXPECT_TRUE(includes_name(subarrays, subarray1_name));
  }

  auto state_destroy1 = log.state_at(destroy1_ts);
  EXPECT_EQ(state_destroy1.timestamp(), destroy1_ts);
  EXPECT_TRUE(state_destroy1.subarrays().empty());
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,
// readability-magic-numbers,cppcoreguidelines-pro-bounds-array-to-pointer-decay)

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
