// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "EventScript.hpp"
#include "SimpleEvent.hpp"
#include "libsymcam.hpp"
#include "gtest/gtest.h"
#include <chrono>
#include <functional>
#include <memory>
#include <source_location>
#include <type_traits>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
TEST(EventScript, ParseDuration) {
  using namespace rcp::symcam::st;
  using namespace rcp::symcam;
  using namespace std::literals::chrono_literals;

  using duration_t = StreamClock::duration;

  EXPECT_FALSE(bool(parse_duration(u8"")));
  EXPECT_FALSE(bool(parse_duration(u8"88")));
  EXPECT_FALSE(bool(parse_duration(u8"16ps")));
  EXPECT_FALSE(bool(parse_duration(u8"16us2")));
  EXPECT_FALSE(bool(parse_duration(u8"16.i0us")));
  EXPECT_FALSE(bool(parse_duration(u8"d16us")));

  EXPECT_EQ(parse_duration(u8"1h"), duration_t(1h));
  EXPECT_EQ(parse_duration(u8"+2min"), duration_t(2min));
  EXPECT_EQ(parse_duration(u8"-12s"), duration_t(-12s));
  EXPECT_EQ(parse_duration(u8"1300ms"), duration_t(1300ms));
  EXPECT_EQ(parse_duration(u8"110us"), duration_t(110us));
  EXPECT_EQ(parse_duration(u8"7500ns"), duration_t(7500ns));

  EXPECT_EQ(
    parse_duration(u8"1.2h"), std::chrono::duration_cast<duration_t>(1.2h));
  EXPECT_EQ(
    parse_duration(u8"+2e6min"),
    std::chrono::duration_cast<duration_t>(2e6min));
  EXPECT_EQ(
    parse_duration(u8"-12.0s"), std::chrono::duration_cast<duration_t>(-12s));
  EXPECT_EQ(
    parse_duration(u8"1300e-2ms"),
    std::chrono::duration_cast<duration_t>(1300e-2ms));
  EXPECT_EQ(
    parse_duration(u8"-110e+1us"),
    std::chrono::duration_cast<duration_t>(-110e+1us));
  EXPECT_EQ(
    parse_duration(u8"7500.0ns"),
    std::chrono::duration_cast<duration_t>(7500.0ns));
}

TEST(EventScript, ParseTimespec) {
  using namespace rcp::symcam::st;
  using namespace rcp::symcam;
  using namespace std::literals::chrono_literals;

  using external_time_t = StreamClock::external_time;
  using duration_t = StreamClock::duration;

  EXPECT_FALSE(bool(parse_timestamp(u8"")));
  EXPECT_FALSE(bool(parse_timestamp(u8"88")));
  EXPECT_FALSE(bool(parse_timestamp(u8"16ps")));
  EXPECT_FALSE(bool(parse_timestamp(u8"16us2")));
  EXPECT_FALSE(bool(parse_timestamp(u8"16.i0us")));
  EXPECT_FALSE(bool(parse_timestamp(u8"d16us")));
  EXPECT_FALSE(bool(parse_timestamp(u8"Δ88")));
  EXPECT_FALSE(bool(parse_timestamp(u8"Δ88f")));
  EXPECT_FALSE(bool(parse_timestamp(u8"Δx88ms")));

  auto test = [](
                std::u8string const& str,
                auto&& expected,
                std::source_location loc = std::source_location::current()) {
    {
      auto ts = parse_timestamp(str);
      EXPECT_TRUE(ts && std::holds_alternative<external_time_t>(*ts))
        << "at l" << loc.line();
      EXPECT_EQ(
        ts, StreamClock::timestamp_t{external_time_t(duration_t(expected))})
        << "at l" << loc.line();
    }
    {
      auto ts = parse_timestamp(u8"Δ" + str);
      EXPECT_TRUE(ts && std::holds_alternative<duration_t>(*ts))
        << "at l" << loc.line();
      EXPECT_EQ(ts, StreamClock::timestamp_t{duration_t(expected)})
        << "at l" << loc.line();
    }
  };

  test(u8"1h", 1h);
  test(u8"+2min", 2min);
  test(u8"-12s", -12s);
  test(u8"1300ms", 1300ms);
  test(u8"110us", 110us);
  test(u8"7500ns", 7500ns);

  test(u8"1.2h", std::chrono::duration_cast<duration_t>(1.2h));
  test(u8"+2e6min", std::chrono::duration_cast<duration_t>(2e6min));
  test(u8"-12.0s", std::chrono::duration_cast<duration_t>(-12s));
  test(u8"1300e-2ms", std::chrono::duration_cast<duration_t>(1300e-2ms));
  test(u8"-110e+1us", std::chrono::duration_cast<duration_t>(-110e+1us));
  test(u8"7500.0ns", std::chrono::duration_cast<duration_t>(7500.0ns));
}

TEST(EventScript, Scripts) {
  using namespace rcp::symcam::st;
  using namespace rcp::symcam;
  using namespace std::literals::chrono_literals;

  // using external_time_t = StreamClock::external_time;
  using relative_time_t = StreamClock::relative_time;
  using duration_t = StreamClock::duration;

  {
    std::string str =
      "{\"event script\":"
      " [{\"events\":"
      "   [ {\"tag\": \"PoisonPill\", \"value\": {\"timestamp\": \"Δ+10h\"}}]"
      "  }]"
      "}";
    auto js = nlohmann::json::parse(str);
    auto event_script = EventScript{js};
    EXPECT_EQ(event_script.size(), 1);
    if (event_script.size() == 1) {
      auto evt = *event_script.begin();
      std::invoke_result_t<decltype(get_event<PoisonPill>), nlohmann::json> pp;
      ASSERT_NO_THROW(pp = get_event<PoisonPill>(evt));
      EXPECT_TRUE(
        static_cast<PoisonPill const*>(pp.get())->timestamp()
        == relative_time_t{duration_t(10h)});
    }
  }
  {
    std::string str =
      "{\"event script\":"
      " [{\"events\":"
      "   [{\"tag\": \"PoisonPill\", \"value\": {\"timestamp\": \"Δ+10h\"}}],"
      "   \"count\": 2,"
      "   \"period\": \"1min\""
      "  }]"
      "}";
    auto js = nlohmann::json::parse(str);
    auto event_script = EventScript{js};
    EXPECT_EQ(event_script.size(), 2);
    if (event_script.size() == 2) {
      auto evts = event_script.begin();
      auto evt0 = *evts++;
      auto evt1 = *evts++;
      std::invoke_result_t<decltype(get_event<PoisonPill>), nlohmann::json> pp0;
      std::invoke_result_t<decltype(get_event<PoisonPill>), nlohmann::json> pp1;
      ASSERT_NO_THROW(pp0 = get_event<PoisonPill>(evt0));
      ASSERT_NO_THROW(pp1 = get_event<PoisonPill>(evt1));
      auto ts0 = static_cast<PoisonPill const*>(pp0.get())->timestamp();
      auto ts1 = static_cast<PoisonPill const*>(pp1.get())->timestamp();
      if (
        std::holds_alternative<relative_time_t>(ts0)
        && std::holds_alternative<relative_time_t>(ts1)) {
        EXPECT_EQ(
          std::get<relative_time_t>(ts1) - std::get<relative_time_t>(ts0),
          relative_time_t{duration_t(1min)});
      }
    }
  }
  {
    std::string str =
      "{\"event script\":"
      " [{\"events\":"
      "   [{\"tag\": \"PoisonPill\", \"value\": {\"timestamp\": \"Δ+10h\"}}],"
      "   \"count\": 0,"
      "   \"period\": \"1min\""
      "  }]"
      "}";
    auto js = nlohmann::json::parse(str);
    EXPECT_THROW(EventScript{js}, CountTooSmallError);
  }
  {
    std::string str =
      "{\"event script\":"
      " [{\"events\":"
      "   [{\"tag\": \"PoisonPill\", \"value\": {\"timestamp\": \"Δ+10h\"}}],"
      "   \"count\": 2"
      "  }]"
      "}";
    auto js = nlohmann::json::parse(str);
    EXPECT_THROW(EventScript{js}, ZeroPeriodError);
  }
  {
    std::string str =
      "{\"event script\":"
      " [{\"events\":"
      "   [{\"tag\": \"PoisonPill\", \"value\": {\"timestamp\": \"Δ+10h\"}}],"
      "   \"count\": 1"
      "  }]"
      "}";
    auto js = nlohmann::json::parse(str);
    EventScript event_script;
    EXPECT_NO_THROW(event_script = EventScript{js});
    EXPECT_EQ(event_script.size(), 1);
  }
  {
    std::string str = "{\"event script\":"
                      " [{\"events\":"
                      "   [{\"tag\": \"PoisonPill\", \"value\": {}}],"
                      "   \"count\": 1"
                      "  }]"
                      "}";
    auto js = nlohmann::json::parse(str);
    EXPECT_THROW(EventScript{js}, NoTimestampError);
  }
  {
    std::string str =
      "{\"event script\":"
      " [{\"events\":"
      "   [{\"tag\": \"PoisonPill\", \"value\": {\"timestamp\": \"Δ~10h\"}}]}]"
      "}";
    auto js = nlohmann::json::parse(str);
    EXPECT_THROW(EventScript{js}, TimestampParseError);
  }
  {
    std::string str =
      "{\"event script\":"
      " [{\"events\":"
      "   [{\"tag\": \"PoisonPill\", \"value\": {\"timestamp\": \"Δ+10h\"}}],"
      "   \"count\": 2,"
      "   \"period\": \"1\""
      "  }]"
      "}";
    auto js = nlohmann::json::parse(str);
    EXPECT_THROW(EventScript{js}, DurationParseError);
  }
  {
    std::string str =
      "{\"event script\":"
      " [{\"events\":"
      "   [ {\"tag\": \"FooBar\", \"value\": {\"timestamp\": \"Δ+10h\"}}]"
      "  }]"
      "}";
    auto js = nlohmann::json::parse(str);
    EXPECT_THROW(EventScript{js}, EventTagError);
  }
  {
    std::string past_str =
      "{\"events\":"
      "   [{\"tag\": \"PoisonPill\", \"value\": {\"timestamp\": \"Δ-1s\"}}]}";
    std::string future_str =
      "{\"events\":"
      "   [{\"tag\": \"PoisonPill\", \"value\": {\"timestamp\": \"Δ0s\"}},"
      "    {\"tag\": \"PoisonPill\", \"value\": {\"timestamp\": \"Δ+1s\"}}]}";
    std::string str =
      "{\"event script\":[" + future_str + "," + past_str + "]}";
    auto js = nlohmann::json::parse(str);
    auto past_js = nlohmann::json::parse(past_str);
    auto future_js = nlohmann::json::parse(future_str);
    auto script = EventScript(js);
    auto [past, future] = script.partition_relative_past_events();
    EXPECT_EQ(past.as_json().at(EventScript::script_tag).at(0), past_js);
    EXPECT_EQ(future.as_json().at(EventScript::script_tag).at(0), future_js);
  }
}
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
