// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "libsymcam.hpp"
#include "gtest/gtest.h"
#include <algorithm>
#include <legion/legion_config.h>
#include <legion/legion_types.h>
#include <limits>
#include <optional>
#include <type_traits>
#include <variant>

namespace L = Legion;

using namespace rcp;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
TEST(BasesScript, MyRegion) {
  L::Context ctx{};
  L::Runtime* rt = nullptr;

  {
    enum class Axes { a = LEGION_DIM_0, b };
    using IndexSpec = IndexSpec<Axes, Axes::a, Axes::b, int>;
    using DoubleField = StaticField<double, 22>;
    using IntField = StaticField<int, 23>;

    using BarRegion = StaticRegionSpec<
      IndexSpec,
      std::array{
        IndexInterval<IndexSpec::coord_t>{-4, std::nullopt},
        IndexInterval<IndexSpec::coord_t>{0, 10}},
      IntField,
      DoubleField>;

    auto is = BarRegion::index_space(
      ctx,
      rt,
      {{IndexSpec::axes_t::a,
        IndexInterval<IndexSpec::coord_t>{std::nullopt, 4}}});
    EXPECT_TRUE(is);
    auto fs = BarRegion::field_space(ctx, rt);
    // NOLINTBEGIN(bugprone-unchecked-optional-access)
    auto lr =
      BarRegion::LogicalRegion{rt->create_logical_region(ctx, is.value(), fs)};
    // NOLINTEND(bugprone-unchecked-optional-access)

    auto pr = rt->map_region(
      ctx,
      lr.requirement(
        LEGION_WRITE_DISCARD,
        LEGION_EXCLUSIVE,
        StaticFields{DoubleField{}, IntField{}}));
    auto doubles = BarRegion::values<LEGION_WRITE_DISCARD>(pr, DoubleField{});
    auto ints = BarRegion::values<LEGION_WRITE_DISCARD>(pr, IntField{});

    auto rg = DenseArrayRegionGenerator<BarRegion>();
    auto lr1 = rg.make(ctx, rt);
    rt->destroy_logical_region(ctx, lr1);
    rg.destroy(ctx, rt);
  }
}
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
