// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "libsymcam.hpp"
#include "gtest/gtest.h"

#include <vector>

using namespace rcp::symcam;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
TEST(ChannelSet, Tests) {
  auto empty_set = ChannelSet();
  EXPECT_TRUE(empty_set.empty());
  EXPECT_EQ(empty_set.size(), 0);
  auto left = ChannelSet({{0, 2}});
  auto mid = ChannelSet({{4, 5}});
  auto right = ChannelSet({{6, 7}});
  auto big = ChannelSet({{0, 8}});
  auto broken = ChannelSet({{1, 1}, {3, 3}, {7, 7}});
  auto left_right = left.add(right);
  using intervals_t = std::vector<std::array<ChannelSet::channel_t, 2>>;
  EXPECT_EQ(left_right.intervals(), (intervals_t{{0, 2}, {6, 7}}));
  auto mid_right = mid.add(right);
  EXPECT_EQ(mid_right.intervals(), (intervals_t{{4, 7}}));
  EXPECT_EQ(right.add(mid).intervals(), mid_right.intervals());
  auto left_mid_right = left_right.add(mid);
  EXPECT_EQ(left_mid_right.intervals(), (intervals_t{{0, 2}, {4, 7}}));
  auto big_left_mid_right = big.add(left_mid_right);
  EXPECT_EQ(big_left_mid_right.intervals(), big.intervals());
  EXPECT_EQ(big_left_mid_right, big);
  EXPECT_EQ(left_mid_right.add(big), big);
  EXPECT_EQ(broken.add(left).intervals(), (intervals_t{{0, 3}, {7, 7}}));
  EXPECT_EQ(
    broken.add(right).intervals(), (intervals_t{{1, 1}, {3, 3}, {6, 7}}));
  EXPECT_EQ(broken.add(big), big);
}
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
