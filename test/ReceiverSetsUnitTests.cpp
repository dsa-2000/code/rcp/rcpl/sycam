// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "libsymcam.hpp"
#include "gtest/gtest.h"
#include <stdexcept>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
TEST(ReceiverSets, BasicTests) {
  using namespace rcp::symcam;

  ReceiverSet empty;
  EXPECT_TRUE(empty.empty());
  EXPECT_EQ(empty.size(), 0);

  ReceiverSet none = ReceiverSet::none();
  EXPECT_TRUE(none.empty());

  ReceiverSet all = ReceiverSet::all();
  EXPECT_FALSE(all.empty());
  EXPECT_EQ(all.size(), Configuration::num_receivers);
  EXPECT_TRUE(std::ranges::all_of(all.receivers(), [](auto&& rcv) {
    return rcv < Configuration::num_receivers;
  }));

  std::set<ReceiverSet::receiver_t> evens;
  std::ranges::generate_n(
    std::inserter(evens, evens.begin()),
    Configuration::num_receivers / 2 + Configuration::num_receivers % 2,
    [idx = 0U]() mutable { return 2 * idx++; });
  ReceiverSet even_rcv{evens};
  EXPECT_EQ(
    even_rcv.size(),
    Configuration::num_receivers / 2 + Configuration::num_receivers % 2);

  std::set<ReceiverSet::receiver_t> odds;
  std::ranges::generate_n(
    std::inserter(odds, odds.begin()),
    Configuration::num_receivers / 2,
    [idx = 0U]() mutable { return 2 * idx++ + 1; });
  ReceiverSet odd_rcv{odds};
  EXPECT_EQ(odd_rcv.size(), Configuration::num_receivers / 2);

  auto all_rcv = even_rcv.merge(odd_rcv);
  EXPECT_EQ(all_rcv.size(), Configuration::num_receivers);
  EXPECT_EQ(all_rcv, all);
  EXPECT_THROW(ReceiverSet({Configuration::num_receivers}), std::out_of_range);
}

TEST(ReceiverPairs, BasicTests) {
  using namespace rcp::symcam;
  static_assert(Configuration::num_receivers >= 4);

  auto one_pairs = ReceiverPairs::set_t{{{0, 1}, {2, 3}}};
  auto one = ReceiverPairs{one_pairs};
  EXPECT_FALSE(one.empty());
  EXPECT_EQ(one.size(), one_pairs.size());
  EXPECT_EQ((ReceiverPairs::set_t(one.begin(), one.end())), one_pairs);
  EXPECT_TRUE(one.contains({1, 0}));
  EXPECT_TRUE(one.contains({0, 1}));

  auto also_one_pairs = ReceiverPairs::set_t{{{0, 1}, {2, 3}, {1, 0}}};
  EXPECT_EQ(one_pairs, also_one_pairs);

  auto two_pairs = ReceiverPairs::set_t{{{0, 2}, {0, 3}, {0, 1}}};
  auto two = ReceiverPairs{two_pairs};
  EXPECT_EQ(two.size(), two_pairs.size());

  auto three = one.merge(two);
  ReceiverPairs::set_t three_pairs;
  std::ranges::set_union(
    one_pairs, two_pairs, std::inserter(three_pairs, three_pairs.end()));

  EXPECT_EQ((ReceiverPairs::set_t(three.begin(), three.end())), three_pairs);

  EXPECT_THROW(
    ReceiverPairs({{0, Configuration::num_receivers}}), std::out_of_range);
}

TEST(ReceiverSetOuterProducts, BasicTests) {
  using namespace rcp::symcam;
  static_assert(Configuration::num_receivers >= 4);

  auto left = ReceiverSet{0, 1, 2, 3};
  auto right = ReceiverSet{4, 5};
  auto lr = ReceiverSetOuterProduct(left, right);
  EXPECT_FALSE(lr.empty());
  EXPECT_EQ(lr.size(), left.size() * right.size());
  auto pairs = lr.pairs();
  auto left_major_iter = lr.begin();
  for (ReceiverSet::receiver_t row = 0; row < Configuration::num_receivers;
       ++row)
    for (ReceiverSet::receiver_t col = 0; col <= row; ++col)
      if (lr.contains({row, col})) {
        EXPECT_EQ(*left_major_iter++, (std::array{row, col}));
      }
  EXPECT_EQ(left_major_iter, lr.end());
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
