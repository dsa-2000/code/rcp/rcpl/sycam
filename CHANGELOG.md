## 20241213.0.0
 (2024-12-13)

### New features (1 change)

- [Refactor VisProcessing for both inline and child task processing](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/46288ab1b2d5df322b53f90f5469c79f3e4d1954)

### Feature changes (3 changes)

- [Change name of StokesWplaneChannelRegion to GridWeightsRegion](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/a1b6bb2eba03d1c2096fb8236e388537292b60ff)
- [Change name of ChannelBaselinePolprodRegion to BsetChannelBaselinePolprodRegion](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/a49c8bbc989e7c8e62da087159a74eb00a2248a8)
- [Move ComputeUVW task launch out of VisProcessing](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/dfc88cdef7fe00523cac65e982e031a992e1c84a)

## 20241210.0.0
 (2024-12-10)

### New features (3 changes)

- [Visibility processing in a task](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/ab0a79cbe0c745b7b3f615115dbeb0aa125c375d)
- [Fix fftw wisdom file handling under control replication](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/20ab81cbc3cdbaec4abbcfc41602e03854045cdf)
- [Add EventLogRegion.hpp](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/dd63ccc81d1638ce54199ec4c4f9140d9511b915)

### Bug fixes (9 changes)

- [Fix RecordVisibilities with hdf5 output](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/1a92b749ed48dafe30c60ec538d15cae3d3a7e8e)
- [Fix uvw & vis partitioning](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/9dec3ebb19dfe619bd36283637de744b276b1ffc)
- [Handle State processing in control replicated context](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/553ad298f33ea4965bf158d2009c6638446e46fa)
- [Add use of external resources for event processing](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/4cd1605e488e22206f34f7fc27df9597ca3d4698)
- [Fix array to DataCapture instance pointers for external resource](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/46bb996a3a1b60e467ed6f90a25649b154cef4da)
- [Fix conversion from streams to channel ranges in CapturedFSamplesPartition](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/6f58d5c21ad0c6679d275061b29f8b625a76946e)
- [Fix direction of inequality in visibility_channels_partition_kind assertion](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/42543591be3d3a4227300b18d7d35afcca5e5a61)
- [Set result in layout construction function of RecordVisibilities](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/7008bb835e0eb2429aac0e443298437cd8ba328f)
- [Fix name of samples_layout()](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/850bae72d965fecee68e3a47fcf3c3eff2068301)

### Feature changes (8 changes)

- [Update librcp dependency to 0.108.0](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/39a6e4137b15465383c1bf9b20e73021e6a9efdc)
- [Replace simultaneous with exclusive coherence on attached regions](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/9f59a0bd3fa8e816a1d4c0c9b248a37cc019a49f)
- [Revise event handling to account for control replication](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/4494b556f4020b47e1e98514bc3ff3e0ac4a235c)
- [Replace index task with loop over plain tasks in subarray_predicates()](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/2d5e1b2444d3931c9c2cc086ff15f396b7ae8e88)
- [Convert methods in EventProcessing not using an EventLog to static](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/aa76df78ae443680ed257f0f110fed4450c19b7f)
- [Replace task "last launch" tracking with top-level execution fence](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/6c2be36256a0eff4d02fccaf20b81c562d0eb076)
- [Use Legion::ExternalResources for capture thread instance pointers](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/4195efec5e0cc21b52044d21fb6c4836f7f08582)
- [Rename uvws_layout2() to uvws_layout()](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/752d9fde9d1dcba3448f138c8bee29043a3a7b29)

## 20241105.0.0
 (2024-11-05)

### Bug fixes (4 changes)

- [Fix header file include statement confusion](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/5e88706a18b1d13a29334cbef9b9abf86f4b38a3)
- [Use DataPacketConfiguration::is_flagged() to test for flagged sample values](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/f6d6a5203f2b9a24568cab36cd4e322cd3747ccd)
- [Use hpg::...run_griddder() in place of hpg::...grid_visibilities()](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/5841e8f5b2686fd85851b6db5fe10ae69183aaca)
- [Replace use of atomic_ref with atomic in CaptureFSamplesTask](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/b8559270169f790ab8d7e33965851dc9879946eb)

### Feature changes (2 changes)

- [Update dependencies: librcp to 0.107.0, rcpdc to 0.106.0](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/6ea0313b52a74c7687542f06e1ec1378f9ac88a7)
- [Use PortableTask and mixin classes for task implementations](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/4a6f6190dbc354e3c409cbc24aa405772a352415)

## v20240923.0.0
 (2024-09-23)

### Feature changes (2 changes)

- [Use HPG library for gridding](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/f903f8dc6054387b5d8f7fe10a5838a99a0bab7d)
- [Bump librcp dependency to 0.106.0](https://gitlab.com/dsa-2000/rcp/symcam/-/commit/12c297122e76ff835530c761522555d5d93c5e72)

## v20240130.0.0
 (2024-01-30)

### New features

- Implement event streams
- Implement subarrays

### Feature removals (2 changes)

- [Remove "interval" from ImageConfiguration](dsa-2000/rcp/rcpl/symcam@9aa00da02edf29e4e390531d70a54f41c1349b30)
- [Remove dead cycle counting & tracking](dsa-2000/rcp/rcpl/symcam@a05475b317b479cf2fbc1b2f9d5a794cd2bab310)

## v20231121.0.0
 (2023-11-21)

### Other (1 change)

- [Update dependencies: bark to 0.103.0, librcpl to 0.106.0](dsa-2000/rcp/rcpl/symcam@0c5aeb3bbece866f13fbcf5e3dd851f2a8200e0a)

## v20231116.0.0
 (2023-11-16)

### New features (1 change)

- [Add EventProcessing class](dsa-2000/code/rcp/rcpl/symcam@1cae87443ded1869aaafc00c2f623a5fae2ab85b)

### Other (1 change)

- [Update librcpl dependency version to 0.105.0](dsa-2000/code/rcp/rcpl/symcam@607544049def8e09042397099c086321d651a184)

## v20231022.0.0
 (2023-10-22)

### Other

- Documentation updates

## v20231006.0.0
 (2023-10-06)

### Other

- Documentation updates

## v20230928.0.0
 (2023-09-28)

### New features (1 change)

- [Implement use of Stokes mask in gridding](dsa-2000/code/rcp/rcpl/symcam@3193bd58b65a519dea60c9d516c7b76d79121aeb)

### Other

- Documentation updates

## v20230807.0.0
 (2023-08-07)

### New features (6 changes)

- [Implement universal uvw tiles optimization](dsa-2000/code/rcp/rcpl/symcam@4c267668778c7d403ddeb3e43819035d91682430)
- [Add semantic info parameters to GridTilesRegionMetadata](dsa-2000/code/rcp/rcpl/symcam@80133739ff939da935993e6b2b611e8c42c2f4a1)
- [Add option to enable parallel compensated summation](dsa-2000/code/rcp/rcpl/symcam@06ab5ee022dd2513a7bbe1f4aebf5974a6cc3888)
- [Add configuration variable for precision of intermediate grid sums](dsa-2000/code/rcp/rcpl/symcam@df0e15676f64bf2bcb38e07a5d3142949efadf5d)
- [Add capability to report intermediate gridding types](dsa-2000/code/rcp/rcpl/symcam@713620769e578d9f5a46732073c439b516c381a1)
- [Add config for compensated summation on intermediate sum-of-weights](dsa-2000/code/rcp/rcpl/symcam@9d61ceaf403863a4632192d21826a1da33fe3fec)

### Bug fixes (3 changes)

- [Add persistence to CountedResources created from visibilities regions](dsa-2000/code/rcp/rcpl/symcam@4ff3e7c0cb34647d9a042c8894e74ad8e9ff92d3)
- [Add persistence flag to CountedResource](dsa-2000/code/rcp/rcpl/symcam@19251f81b978989fd451d133f00e1956a6cfa495)
- [Fix (pseudo|quasi)_atomic_add](dsa-2000/code/rcp/rcpl/symcam@67ff3218256b67316a94f5782fcc79964d8a9403)

### Feature changes (3 changes)

- [Explicitly map regions of GridVisibilitiesOnImageTask virtually](dsa-2000/code/rcp/rcpl/symcam@0e57e48a9f33d53a2497ea38f7704332753c9854)
- [Standardize grid and sum-of-weights layouts](dsa-2000/code/rcp/rcpl/symcam@1d73612d6a7e4beba01f146def21512e3116f17d)
- [Add finer control of intermediate values precision during gridding](dsa-2000/code/rcp/rcpl/symcam@36cdf510c046b79e12d78e465e324cdbb68ea615)

## v20230714.0.0
 (2023-07-14)

### New features (8 changes)

- [Implement Legion reduction ops on accval for grid and weights](dsa-2000/code/rcp/rcpl/symcam@1a31db793bc6b2a11764bb164ec5217e32dd934b)
- [Add accval shim type to support optional compensated summation](dsa-2000/code/rcp/rcpl/symcam@5f5b641635bc42411c89cfdb7ea16ae965fd75b5)
- [Add Symcam_USE_COMPENSATED_SUMMATION build configuration variable](dsa-2000/code/rcp/rcpl/symcam@e0f6be5d7cebbab7187f02d69b04e1b6be235bf9)
- [Add warnings for configurations with too few regions for double-buffering](dsa-2000/code/rcp/rcpl/symcam@8a5a5fee74a896bd6c91483a09e3f661af55ab52)
- [Implement Kokkos CUDA variant for PartitionUVWByTiles](dsa-2000/code/rcp/rcpl/symcam@3dfed360f5a11d111271c0161a813023dd133c95)
- [Add Configuration::max_visibility_regions](dsa-2000/code/rcp/rcpl/symcam@e17a9dc162d2ef51ec745da776f171444c6547b9)
- [Add configuration variable for grid (and weights) value precision](dsa-2000/code/rcp/rcpl/symcam@19be1668095dae0d6f18a216b3b5c69a6d1d5bdb)
- [Add dead cycles holdoff configuration parameter](dsa-2000/code/rcp/rcpl/symcam@0154b4c9509e4278a10bd788decc422e297bc32c)

### Bug fixes (2 changes)

- [Fix array index error](dsa-2000/code/rcp/rcpl/symcam@9fe1445f920e99c854d8178192bbec61addd88bd)
- [Return all grids in GridVisibilitiesOnImage::flush()](dsa-2000/code/rcp/rcpl/symcam@6fd0de58f3ebaf6a64ecfdbc42c13b7fa81908a2)

### Feature changes (4 changes)

- [Change max correlation time parameter to integration factor](dsa-2000/code/rcp/rcpl/symcam@06640546199aa1289904a2f2644415183564d6ba)
- [Add exception when no matching OFI fabric interface exists](dsa-2000/code/rcp/rcpl/symcam@8135a0adf9b6f16504f7b00abacb8f8e342ddf82)
- [Rename "live_capture_regions" to "capture_regions"](dsa-2000/code/rcp/rcpl/symcam@3fbe121e326154cc4ba7d2d1e19fc0d1776a9ccd)
- [Capitalize CMake target namespaces](dsa-2000/code/rcp/rcpl/symcam@355912ddf9b86b8f70b322599ad7bb23f11138a8)

### Feature removals (2 changes)

- [Remove Configuration::max_fill_wait_us](dsa-2000/code/rcp/rcpl/symcam@04e03823c60a573ec02fa55678cba778816d0c33)
- [Remove Configuration::num_active_captures](dsa-2000/code/rcp/rcpl/symcam@0ab81b4f51536d7b72f3e06ea50d6045524842f0)

### Other (4 changes)

- [Update librcpl dependency to v0.104.0](dsa-2000/code/rcp/rcpl/symcam@885103ccfdaafa8824826b1d8d07f88d9e8cf8b8)
- [Remove (explicit) librcp build dependency](dsa-2000/code/rcp/rcpl/symcam@f96d869b568e7672ccdeeaa59f5805e3ee2622bf)
- [Update bark, librcp and librcpl dependency versions](dsa-2000/code/rcp/rcpl/symcam@e20dd701614200f81dbd96bbda0665e9c25ef0b9)

## v20230613.0.0
 (2023-06-13)

### New features (1 change)

- [Support Kokkos CUDA implementation of cross-correlation](dsa-2000/code/rcp/rcpl/symcam@5ae61d77bfad5258591cbf2be9fce6044823cf5e)

### Bug fixes (1 change)

- [Add TCC dependency](dsa-2000/code/rcp/rcpl/symcam@7adb2a287c658d4e95167d9fc9444cee35871cbf)

### Other (1 change)

- [Set minimum librcpl dependency to v0.101.0](dsa-2000/code/rcp/rcpl/symcam@c946606cd4481fff4a2e5080e4fa2ac583dfb5b5)

## v20230612.0.1
 (2023-06-12)

### Bug fixes (1 change)

- [Add capture buffer timesteps tile size parameter](dsa-2000/code/rcp/rcpl/symcam@35fc51c4eb4b78d6d994aa1772b45e1fdc66e3ab)

### Other (1 change)

- [Add explicit dependency on librcp](dsa-2000/code/rcp/rcpl/symcam@3a9d34d354bc4b00f639d25bc60d3959327f7971)

## v20230612.0.0
 (2023-06-12)

### Other (1 change)

- Reset version number
