# symcam

SYnthesis iMaging CAMera application: an implementation of the
[DSA-2000](https://deepsynoptic.org/overview)
[RCP](https://gitlab.com/dsa-2000/rcp) imaging pipeline
application.

## Build guidance

- Minimum [CMake](https://cmake.org) version: 3.23
- C++ standard: 20
  - tested with [gcc](https://gcc.gnu.org) v12.1.0, v13.2.0
- Dependencies
  - [bark](https://gitlab.com/dsa-2000/rcp/bark), minimum version
    0.103.0
  - [librcp](https://gitlab.com/dsa-2000/rcp/librcp), minimum
    version 0.108.0
  - [rcpdc](https://gitlab.com/dsa-2000/rcp/rcpdc), minimum version
    0.106.0
  - [hpg](https://gitlab.com/dsa-2000/rcp/hpg), minimum version 3.0.2
  - [tensor-core-correlator](https://gitlab.com/dsa-2000/rcp/tensor-core-correlator)
  - [boost](https://boost.org), `program_options` library, minimum
    version 1.80.0
  - [JSON for Modern C++](https://github.com/nlohmann/json), minimum
    version 3.11.2
  - [FFTW](https://fftw.org), version 3
- Optional dependencies
  - [HDF5](https://hdfgroup.org), version 1.14.3 (known, undetermined
    minimum)
  - [ADIOS2](https://github.com/ornladios/ADIOS2), version 2.9.2
    (known, undetermined minimum)
  - [doxygen](https://www.doxygen.nl), version 1.9.8 (known,
    undetermined minimum)

## Spack package

[Spack](https://spack.io) package is available in the DSA-2000 Spack
[package repository](https://gitlab.com/dsa-2000/code/spack).

## Documentation

Project documentation may be built using the `-DSymcam_BUILD_DOCS=ON`
or `-DSymcam_BUILD_DOCS_ONLY=ON` CMake options. Note that building the
documentation requires [Doxygen](https://www.doxygen.org) at a
minimum, and preferably also [graphviz](https://graphviz.org) and
[mscgen](https://www.mcternan.me.uk/mscgen/).

### Documentation site

_The documentation site is currently off-line_
~~A CI pipeline on gitlab.com runs whenever the main branch is updated,
which builds the documentation and uploads the results to the [symcam
documentation site](https://dsa-2000.gitlab.io/rcp/symcam).~~

## Getting involved

The project web site may be found at
https://gitlab.com/dsa-2000/rcp/symcam. Instructions for cloning the
code repository can be found on that site. To provide feedback (*e.g*
bug reports or feature requests), please see the issue tracker on that
site. If you wish to contribute to this project, please read
[CONTRIBUTING.md](CONTRIBUTING.md) for guidelines.
