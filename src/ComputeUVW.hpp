// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BaselineChannelRegion.hpp"
#include "BaselineRegion.hpp"
#include "BsetChannelBaselinePolprodRegion.hpp"
#include "Configuration.hpp"
#include "Serializable.hpp"
#include "StateRegion.hpp"

namespace rcp::symcam {

/*! \brief launcher for task to compute UVW coordinates */
class ComputeUVW {
public:
  /*! \brief baselines region type alias */
  using bl_t = BaselineRegion;

  /*! \brief uvws region type alias */
  using bc_t = BaselineChannelRegion;

  /*! \brief state region type alias */
  using state_t = st::StateRegion;

  /*! \brief visibilities region type alias */
  using cbp_t = BsetChannelBaselinePolprodRegion;

  /*! \brief task arguments */
  struct Args {
    /* \brief channel 0 center frequency (Hz) */
    Configuration::frequency_t sample_channel0_frequency_hz;

    /*! \brief channel width (Hz) */
    Configuration::frequency_t sample_channel_width_hz;
  };

private:
  /*! \brief UVWs region generator */
  DenseArrayRegionGenerator<bc_t> m_uvws_gen;

  /*! \brief task arguments */
  Args m_args;

public:
  /*! \brief constructor argument */
  struct Serialized {
    DenseArrayRegionGenerator<bc_t>::Serialized m_uvws_gen;
    Configuration::frequency_t m_sample_channel0_frequency_hz{};
    Configuration::frequency_t m_sample_channel_width_hz{};
  };

  /*! \brief default constructor (defaulted) */
  ComputeUVW() = default;

  /*! \brief copy constructor (deleted) */
  ComputeUVW(ComputeUVW const&) = delete;

  /*! \brief move constructor (defaulted) */
  ComputeUVW(ComputeUVW&&) noexcept = default;

  /*! \brief construction from Configuration */
  ComputeUVW(L::Context ctx, L::Runtime* rt, Configuration const& config);

  /*! \brief deserialization constructor */
  ComputeUVW(
    L::Context ctx, L::Runtime* rt, Serialized const& serialized) noexcept;

  /*! \brief destructor (defaulted) */
  ~ComputeUVW() = default;

  /*! \brief copy assignment operator (deleted) */
  auto
  operator=(ComputeUVW const&) -> ComputeUVW& = delete;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ComputeUVW&&) noexcept -> ComputeUVW& = default;

  /*! \brief serialized form */
  [[nodiscard]] auto
  serialized(L::Context ctx, L::Runtime* rt) const noexcept -> Serialized;

  /*! \brief requirements for child tasks that use Serialized values as
   *  arguments */
  [[nodiscard]] auto
  child_requirements(L::Context ctx, L::Runtime* rt) const -> ChildRequirements;

  /*! \brief get index uvw region space */
  auto
  index_space() -> bc_t::index_space_t;

  /*! \brief destroy resources */
  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void;

  /*! \brief create a logical region that can be used by the task */
  auto
  make(L::Context ctx, L::Runtime* rt) -> bc_t::LogicalRegion;

  /*! \brief launch the task
   *
   * \param baselines baselines
   * \param uvws uvws region
   * \param stream_states stream states at interval endpoints
   */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<bl_t::LogicalRegion> const& baselines,
    std::array<RegionPair<state_t::LogicalRegion>, 2> const& stream_states,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    L::Predicate pred = L::Predicate::TRUE_PRED) -> void;

  /*! \brief launch a task to fill the VisProjectionField of a region */
  auto
  fill_vis_projection_field(
    L::Context ctx,
    L::Runtime* rt,
    cbp_t::index_space_t vis,
    bc_t::LogicalRegion const& uvws) -> void;

  /*! \brief pre-register tasks with Legion runtime */
  static auto
  preregister_tasks(std::array<L::TaskID, 2> const& task_ids) -> void;

  /*! \brief register tasks with Legion runtime */
  static auto
  register_tasks(L::Runtime* rt, std::array<L::TaskID, 2> const& task_ids)
    -> void;
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
