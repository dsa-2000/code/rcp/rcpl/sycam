// Copyright 2022 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BaselineChannelRegion.hpp"
#include "CFRegion.hpp"
#include "Configuration.hpp"
#include "EventProcessing.hpp"
#include "MuellerIndexRegion.hpp"
#include "Serializable.hpp"

namespace rcp::symcam {

/*! launcher for task to get convolution function values for some visibilities
 */
class GetCFValues {
public:
  using cf_t = CFRegion;
  using mi_t = MuellerIndexRegion;
  using bc_t = BaselineChannelRegion;

  struct IndexTaskArgs {
    Configuration::uvw_value_t w_depth_arcsec{0};
  };

private:
  /*! all the CF array values */
  cf_t::LogicalRegion m_cf_arrays{};

  IndexTaskArgs m_index_task_args{};

  /*! sparse Mueller matrix index region */
  mi_t::LogicalRegion m_mueller_indexes{};

  static auto
  cf_array_region(
    L::Context ctx,
    L::Runtime* rt,
    ImagingConfiguration const& img) -> cf_t::LogicalRegion;

public:
  struct Serialized {
    cf_t::LogicalRegion m_cf_arrays;
    IndexTaskArgs m_index_task_args;
    mi_t::LogicalRegion m_mueller_indexes;
  };

  GetCFValues(L::Context ctx, L::Runtime* rt, ImagingConfiguration const& img);

  GetCFValues(L::Context ctx, L::Runtime* rt, Serialized const& serialized);

  GetCFValues() = default;

  GetCFValues(GetCFValues const&) = delete;

  GetCFValues(GetCFValues&&) = default;

  ~GetCFValues() = default;

  auto
  operator=(GetCFValues const&) -> GetCFValues& = delete;

  auto
  operator=(GetCFValues&&) -> GetCFValues& = default;

  /*! \brief serialized form */
  [[nodiscard]] auto
  serialized(L::Context ctx, L::Runtime* rt) const noexcept -> Serialized;

  /*! \brief requirements for child tasks that use Serialized values as
   *  arguments */
  [[nodiscard]] auto
  child_requirements(L::Context ctx, L::Runtime* rt) const noexcept
    -> ChildRequirements;

  [[nodiscard]] auto
  mueller_indexes_region() const -> mi_t::LogicalRegion const&;

  void
  destroy(L::Context ctx, L::Runtime* rt);

  /*! \brief compute indexes into CF arrays */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    L::Predicate pred = L::Predicate::TRUE_PRED) const -> cf_t::LogicalRegion;

  static auto
  preregister_tasks(L::TaskID init_task_id, L::TaskID index_task_id) -> void;

  static auto
  register_tasks(
    L::Runtime* rt, L::TaskID init_task_id, L::TaskID index_task_id) -> void;
};
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
