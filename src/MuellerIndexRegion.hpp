// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include "StaticFields.hpp"

namespace rcp::symcam {

/* \brief Mueller index region index space axes definition */
enum class MuellerIndexAxes { stokes0 = LEGION_DIM_0, stokes1 };

/* \brief Mueller index field definition */
using ForwardIndexField = StaticField<int, field_ids::forward_index>;

/* \brief inverse Mueller index field definition */
using InverseIndexField = StaticField<int, field_ids::inverse_index>;

/*! \brief Mueller index region definition
 *
 * A 2-d index space with coordinates on both axes in the range [0, 3], with
 * two fields MuellerIndexField and InverseMuellerIndexField.
 */
using MuellerIndexRegion = StaticRegionSpec<
  IndexSpec<
    MuellerIndexAxes,
    MuellerIndexAxes::stokes0,
    MuellerIndexAxes::stokes1,
    int>,
  std::array{
    IndexInterval<int>::bounded(
      0, Configuration::num_polarization_products - 1),
    IndexInterval<int>::bounded(
      0, Configuration::num_polarization_products - 1)},
  ForwardIndexField,
  InverseIndexField>;

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
