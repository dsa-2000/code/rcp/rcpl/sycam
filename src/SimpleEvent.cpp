// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "SimpleEvent.hpp"

/*! \file SimpleEvent.cpp
 */

namespace rcp::symcam::st {

auto
JSONSimpleEvent::has_timestamp(nlohmann::json const& js) noexcept -> bool {
  return js.contains(timestamp_tag);
}

auto
JSONSimpleEvent::get_timestamp(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(timestamp_tag);
}

auto
JSONSimpleEvent::get_timestamp(nlohmann::json& js) -> nlohmann::json& {
  return js.at(timestamp_tag);
}

auto
JSONSimpleEvent::is_valid(nlohmann::json const& js) noexcept -> bool {
  return has_timestamp(js) && js.at(timestamp_tag).is_string();
}

auto
PoisonPill::apply_to_no_ts(ExtendedState&& st) -> ExtendedState {
  // NOLINTNEXTLINE(performance-move-const-arg)
  return std::move(st).set_alive(false);
}

} // end namespace rcp::symcam::st

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
