// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "CFRegion.hpp"

using namespace rcp::symcam;

void
CFRegion::attach_params(
  L::Runtime* rt, LogicalRegion region, Params const& params) {

  rt->attach_semantic_information(region, params_tag, &params, sizeof(params));
}

auto
CFRegion::has_params(L::Runtime* rt, LogicalRegion region) -> bool {

  void const* result = nullptr;
  std::size_t size{};
  return rt->retrieve_semantic_information(
    region, params_tag, result, size, true);
}

auto
CFRegion::retrieve_params(L::Runtime* rt, LogicalRegion region)
  -> CFRegion::Params const& {

  void const* result = nullptr;
  std::size_t size{};
  rt->retrieve_semantic_information(region, params_tag, result, size);
  assert(size == sizeof(Params));
  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
  return *reinterpret_cast<Params const*>(result);
}

auto
CFRegion::extents_to_bounds(
  std::array<unsigned, 2> const& extents,
  unsigned num_mueller,
  unsigned num_indexes,
  Params const& params) -> dynamic_bounds_t {

  coord_t sz0 = extents[0] + 2 * params.padding;
  coord_t sz1 = extents[1] + 2 * params.padding;
  return {
    {axes_t::x_major,
     IndexInterval<coord_t>::bounded(-(sz0 / 2), -(sz0 / 2) + sz0 - 1)},
    {axes_t::y_major,
     IndexInterval<coord_t>::bounded(-(sz1 / 2), -(sz1 / 2) + sz1 - 1)},
    {axes_t::x_minor,
     IndexInterval<coord_t>::right_bounded(params.oversampling - 1)},
    {axes_t::y_minor,
     IndexInterval<coord_t>::right_bounded(params.oversampling - 1)},
    {axes_t::index, IndexInterval<coord_t>::right_bounded(num_indexes - 1)},
    {axes_t::mueller, IndexInterval<coord_t>::right_bounded(num_mueller - 1)}};
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
