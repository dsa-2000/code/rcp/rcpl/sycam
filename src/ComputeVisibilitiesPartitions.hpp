// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BaselineChannelRegion.hpp"
#include "Configuration.hpp"
#include "DataPartitionsRegion.hpp"
#include "EventProcessing.hpp"
#include "GridRegion.hpp"
#include "GridTileRegion.hpp"
#include "Serializable.hpp"
#include "StateRegion.hpp"
#include "StreamState.hpp"
#include "libsymcam.hpp"
#include <rcp/rcp.hpp>

namespace rcp::symcam {

/*! \brief launcher for task to compute partition of visibilities for each
 * subarray
 */
class ComputeVisibilitiesPartitions {
public:
  using bc_t = BaselineChannelRegion;
  using state_t = st::StateRegion;
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using part_t = DataPartitionsRegion;
  using grid_t = GridRegion;
  using tiles_t = GridTileRegion;

  /*! \brief final visibilities partition dimension */
  static constexpr unsigned vis_partition_dim = GridTileRegion::dim + 1;

private:
  /*! \brief region that has BaselineChannelProjectionField */
  cbp_t::LogicalRegion m_uvw_projection_region;

  /*! \brief region with partitions for subarrays and imaging */
  PartitionPair<part_t::LogicalPartition> m_partitions_by_subarray;

public:
  struct Serialized {
    cbp_t::LogicalRegion m_uvw_projection_region;
    PartitionPair<part_t::LogicalPartition> m_partitions_by_subarray;
  };

  /*! \brief default constructor (defaulted) */
  ComputeVisibilitiesPartitions() = default;

  /*! \brief deserialization constructor region */
  ComputeVisibilitiesPartitions(
    L::Context ctx, L::Runtime* rt, Serialized const& serialized) noexcept;

  /*! \brief constructor with projection region, which will be initialized */
  ComputeVisibilitiesPartitions(
    L::Context ctx,
    L::Runtime* rt,
    cbp_t::LogicalRegion const& uvw_projection_region);

  /*! \brief copy constructor (defaulted) */
  ComputeVisibilitiesPartitions(ComputeVisibilitiesPartitions const&) = default;

  /*! \brief move constructor (defaulted) */
  ComputeVisibilitiesPartitions(ComputeVisibilitiesPartitions&&) noexcept =
    default;

  /*! \brief destructor (defaulted) */
  ~ComputeVisibilitiesPartitions() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(ComputeVisibilitiesPartitions const&)
    -> ComputeVisibilitiesPartitions& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ComputeVisibilitiesPartitions&&)
    -> ComputeVisibilitiesPartitions& = default;

  /*! \brief free owned resources */
  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void;

  /*! \brief region for partitions, partitioned by subarray */
  [[nodiscard]] auto
  partitions_by_subarray() const noexcept
    -> PartitionPair<part_t::LogicalPartition>;

  /*! \brief serialized form */
  [[nodiscard]] auto
  serialized(L::Context ctx, L::Runtime* rt) const noexcept -> Serialized;

  /*! \brief requirements for child tasks that use Serialized values as
   *  arguments */
  [[nodiscard]] auto
  child_requirements(L::Context ctx, L::Runtime* rt) const -> ChildRequirements;

  /*! \brief launch task to update partitions when subarrays have changed */
  auto
  update_by_subarray(
    L::Context ctx,
    L::Runtime* rt,
    std::array<RegionPair<state_t::LogicalRegion>, 3> const& stream_states,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    st::subarray_predicates_array_t const& predicates) -> void;

  /*! \brief launch task to update partitions when subarrays have changed */
  auto
  update_by_subarray(
    L::Context ctx,
    L::Runtime* rt,
    std::array<RegionPair<state_t::LogicalRegion>, 3> const& stream_states,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    st::AllSubarraysQuery const& state_query) -> void;

  /*! \brief launch task to update partitions before imaging occurs */
  auto
  update_by_tile(
    L::Context ctx,
    L::Runtime* rt,
    ImagingConfiguration const& img,
    sarray<tiles_t::LogicalRegion> const& tile_bounds,
    sarray<grid_t::index_partition_t> const& grid_partitions,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    st::subarray_predicates_array_t const& predicates) const -> void;

  /*! \brief launch task to update partitions before imaging occurs */
  auto
  update_by_tile(
    L::Context ctx,
    L::Runtime* rt,
    ImagingConfiguration const& img,
    sarray<tiles_t::LogicalRegion> const& tile_bounds,
    sarray<grid_t::index_partition_t> const& grid_partitions,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    st::AllSubarraysQuery const& state_query) const -> void;

  /*! \brief pre-register task with Legion runtime */
  static auto
  preregister_tasks(std::array<L::TaskID, 4> const& task_ids) -> void;

  /*! \brief register task with Legion runtime */
  static auto
  register_tasks(L::Runtime* rt, std::array<L::TaskID, 4> const& task_ids)
    -> void;

private:
  auto
  init_update_by_subarray_launcher(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& state,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    L::UntypedBuffer arg) -> L::TaskLauncher;

  auto
  init_update_by_tile_launcher(
    ImagingConfiguration const& img,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    bool use_imaging_start_future,
    std::vector<char>& argbuf) const -> L::TaskLauncher;

  auto
  update_update_by_tile_launcher(
    L::Context ctx,
    L::Runtime* rt,
    sarray<tiles_t::LogicalRegion> const& tile_bounds,
    sarray<grid_t::index_partition_t> const& grid_partitions,
    unsigned subarray_slot,
    L::TaskLauncher& launcher,
    std::vector<char>& argbuf,
    bool do_imaging_start = false) const -> void;
};
} // namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
