// Copyright 2023,2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "EventProcessing.hpp"
#include "BaselineRegion.hpp"
#include "EventScript.hpp"
#include "libsymcam.hpp"

#include <bits/ranges_algo.h>
#include <cassert>
#include <experimental/mdspan>
#include <legion/legion_config.h>
#include <log.hpp>
#include <mutex>
#include <ranges>
#include <rcp/rcp.hpp>
#include <sstream>

using namespace rcp::symcam::st;

namespace rcp::symcam::st {

// Allocator adaptor that interposes construct() calls to
// convert value initialization into default initialization.
template <typename T, typename A = std::allocator<T>>
class default_init_allocator : public A {
  using a_t = std::allocator_traits<A>;

public:
  template <typename U>
  struct rebind {
    using other =
      default_init_allocator<U, typename a_t::template rebind_alloc<U>>;
  };

  using A::A;

  template <typename U>
  void
  construct(U* ptr) noexcept(std::is_nothrow_default_constructible<U>::value) {
    ::new (static_cast<void*>(ptr)) U;
  }
  template <typename U, typename... Args>
  void
  construct(U* ptr, Args&&... args) {
    a_t::construct(static_cast<A&>(*this), ptr, std::forward<Args>(args)...);
  }
};

/*! task to record events in an EventLog
 *
 * This task can both record a vector of events, and unroll an event script and
 * record all of those events.
 */
struct RecordEventsTask : public SerialOnlyTaskMixin<RecordEventsTask, void> {

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;
  static constexpr char const* task_name = "RecordEventsTask";

  enum { elog_region_index = 0, num_regions };

  using elog_t = EventLogRegion;

  template <typename Events>
  static auto
  collect_events(Events&& events_js)
    -> std::tuple<
      std::vector<std::string>,
      std::vector<std::shared_ptr<StreamStateEventBase<ExtendedState>>>> {

    using eventp_t = std::shared_ptr<StreamStateEventBase<ExtendedState>>;
    std::vector<std::string> errors;
    std::vector<eventp_t> events;
    for (auto&& js : events_js) {
      try {
        events.push_back(get_event(js));
      } catch (JSONError const& err) {
        errors.emplace_back(err.what());
      }
    }
    return {errors, events};
  }

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> result_t {

    assert(regions.size() == num_regions);

    // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const args = nlohmann::json::parse(std::string_view(
      reinterpret_cast<char* const>(task->args), task->arglen));
    // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)
    std::vector<std::string> errors;
    std::vector<std::shared_ptr<StreamStateEventBase<ExtendedState>>> events;
    std::string err_prefix = "Error parsing ";
    if (args.contains(EventScript::script_tag)) {
      // this looks like an event script, so try parsing it as one
      std::tie(errors, events) = collect_events(EventScript{args});
      err_prefix.append("event script: ");
    } else {
      // this doesn't look like an event script, so try parsing it as a vector
      // of events
      std::tie(errors, events) = collect_events(args);
      err_prefix.append("event: ");
    }
    // log parsing errors
    for (auto&& err : errors)
      log().error(err_prefix + err);
    // record (valid) events
    auto const& elog_region = regions[elog_region_index];
    auto& elog_block = elog_t::values<LEGION_READ_WRITE>(
      elog_region, EventLogPtrsBlockField{})[elog_t::rect_t{elog_region}.lo];
    {
      auto lk = std::lock_guard(*elog_block.mtx);
      *elog_block.log = std::move(*elog_block.log).record(events);
    }
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID RecordEventsTask::task_id;

/*! task to roll-up an EventLog */
struct RollupTask : public SerialOnlyTaskMixin<RollupTask, void> {

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;
  static constexpr char const* task_name = "RollupTask";

  enum { elog_region_index = 0, states_region_index };

  struct Args {
    // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
    bool is_initial;
    BaselineRegion::index_space_t baseline_index_space;
    L::IndexSpaceT<1, int> subarrays_color_space;
    std::vector<StreamClock::external_time> timestamps;
    // NOLINTEND(misc-non-private-member-variables-in-classes)

    [[nodiscard]] auto
    serialized_size() const -> std::size_t {
      return sizeof(is_initial) + sizeof(baseline_index_space)
             + sizeof(subarrays_color_space) + rcp::serialized_size(timestamps);
    }
    // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,
    // cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto
    serialize(void* buffer) const -> std::size_t {
      char* ch = reinterpret_cast<char*>(buffer);
      ch += rcp::serialize(is_initial, ch);
      ch += rcp::serialize(baseline_index_space, ch);
      ch += rcp::serialize(subarrays_color_space, ch);
      ch += rcp::serialize(timestamps, ch);
      return ch - reinterpret_cast<char*>(buffer);
    }
    auto
    deserialize(void const* buffer) -> std::size_t {
      char const* ch = reinterpret_cast<char const*>(buffer);
      ch += rcp::deserialize(is_initial, ch);
      ch += rcp::deserialize(baseline_index_space, ch);
      ch += rcp::deserialize(subarrays_color_space, ch);
      ch += rcp::deserialize(timestamps, ch);
      return ch - reinterpret_cast<char const*>(buffer);
    }
    // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,
    // cppcoreguidelines-pro-bounds-pointer-arithmetic))
  };

  using elog_t = EventLogRegion;
  using state_t = StateRegion;

  [[nodiscard]] static auto
  find_updated_subarrays(
    std::optional<State> const& prev_state,
    ExtendedState const& new_state) -> std::set<unsigned> {

    std::set<unsigned> result;
    auto new_subarray_names = std::vector<subarray_name_t>{};
    std::ranges::copy(
      new_state.subarrays()
        | std::views::transform(
          [](auto&& nm_sa) -> subarray_name_t { return std::get<0>(nm_sa); }),
      std::back_inserter(new_subarray_names));

    if (!prev_state) {
      for (auto&& subarray : new_subarray_names) {
        assert(new_state.find_subarray(subarray));
        // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
        auto slot = new_state.find_subarray(subarray).value();
        result.insert(slot);
        log().info([&]() -> std::string {
          std::ostringstream oss;
          oss << "created subarray " << slot << " '" << subarray.data() << "'";
          return oss.str();
        }());
      }
    } else {
      auto prev_subarray_names = prev_state->subarray_names();
      // report destroyed subarrays
      for (auto&& prev_subarray : prev_subarray_names) {
        auto found_new = std::ranges::find(new_subarray_names, prev_subarray);
        if (found_new == new_subarray_names.end()) {
          assert(prev_state->find_subarray(prev_subarray));
          // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
          auto slot = prev_state->find_subarray(prev_subarray).value();
          result.insert(slot);
          log().info([&]() {
            std::ostringstream oss;
            oss << "destroyed subarray " << slot << " '" << prev_subarray.data()
                << "'";
            return oss.str();
          }());
        }
      }
      // report new subarrays
      for (auto&& new_subarray : new_subarray_names) {
        auto found_prev = std::ranges::find(prev_subarray_names, new_subarray);
        if (found_prev == prev_subarray_names.end()) {
          assert(new_state.find_subarray(new_subarray));
          // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
          auto slot = new_state.find_subarray(new_subarray).value();
          result.insert(slot);
          log().info([&]() -> std::string {
            std::ostringstream oss;
            oss << "created subarray " << slot << " '" << new_subarray.data()
                << "'";
            return oss.str();
          }());
        }
      }
    }
    return result;
  }

  template <typename R>
  static auto
  compute_baseline_index_rectangles(R&& pairs)
    -> std::vector<L::Rect<1, BaselineRegion::coord_t>> {

    // TODO: parallelize?
    //
    // Partition linear baseline index space. Use (parallel) prefix scan to
    // compute total number of rectangles in each sub-space, and the offset for
    // the first rectangle of each sub-space in a vector. Allocate memory for
    // vector of rectangles. Parallel-for loop to compute rectangles in each
    // sub-space. Sequential pass over vector to merge contiguous rectangles.
    std::vector<L::Rect<1, BaselineRegion::coord_t>> result;
    if (!pairs.empty()) {
      auto const& pair0 = *pairs.begin();
      auto start = receiver_pair_to_baseline(pair0);
      auto prev{start};
      auto bl{start};
      for (auto&& pair : pairs | std::views::drop(1)) {
        bl = receiver_pair_to_baseline(pair);
        assert(bl > prev);
        if (bl != prev + 1) {
          result.emplace_back(start, prev);
          start = bl;
        }
        prev = bl;
      }
      result.emplace_back(start, bl);
    }
    return result;
  }

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    Args args;
    args.deserialize(task->args);

    auto const& elog_region = regions[elog_region_index];
    auto& elog_block = elog_t::values<LEGION_READ_WRITE>(
      elog_region, EventLogPtrsBlockField{})[elog_t::rect_t{elog_region}.lo];

    // roll up event log for each timestamp, and save every rolled-up state
    std::optional<State> prev_state;
    // all states regions are write-discard except when !args.is_initial, in
    // which case the first region is read-only
    unsigned const new_states_region_index =
      states_region_index + (args.is_initial ? 0 : 1);
    if (!args.is_initial) {
      auto const& starting_state_region = regions[states_region_index];
      prev_state = state_t::values<LEGION_READ_ONLY>(
        starting_state_region,
        StateField{})[state_t::rect_t{starting_state_region}.lo];
    }
    {
      auto lk = std::lock_guard(*elog_block.mtx);
      for (auto&& states_region :
           regions | std::views::drop(new_states_region_index)) {
        elog_block.log->state() =
          std::move(elog_block.log->state()).reset_fftw_wisdom_paths();
        auto const states =
          state_t::values<LEGION_WRITE_ONLY>(states_region, StateField{});
        std::vector<JSONError> errors;
        std::tie(*elog_block.log, errors) =
          std::move(*elog_block.log)
            .safe_rollup<JSONError>(
              args.timestamps
                [std::distance(regions.data(), &states_region)
                 - states_region_index]);
        auto& extended_state = elog_block.log->state();
        if (!errors.empty())
          for (auto&& err : errors)
            log().warn(std::string("Event error: ") + err.what());
        auto updated = find_updated_subarrays(prev_state, extended_state);
        // compute partition of baselines index space if subarrays have changed
        if (elog_block.log->is_primary() && !updated.empty()) {
          std::map<
            L::Point<1, int>,
            std::vector<L::Rect<1, BaselineRegion::coord_t>>>
            rectangles;
          for (auto&& slot : subarray_slots()) {
            auto maybe_subarray_p = extended_state.get_subarray(slot);
            if (maybe_subarray_p)
              rectangles[slot] = std::visit(
                [&](auto&& sa) {
                  return compute_baseline_index_rectangles(sa);
                },
                *(maybe_subarray_p.value()));
          }
          if (!rectangles.empty())
            extended_state =
              std::move(extended_state)
                .set_subarray_partition(rt->create_partition_by_rectangles(
                  ctx,
                  args.baseline_index_space,
                  rectangles,
                  args.subarrays_color_space,
                  false));
          else
            extended_state =
              std::move(extended_state).set_subarray_partition({});
        }
        assert(state_t::rect_t(states_region).volume() == 1);
        auto pt = state_t::rect_t{states_region}.lo;
        states[pt] = extended_state;
        prev_state = states[pt];
        if (elog_block.log->is_primary())
          log().debug([&]() {
            std::ostringstream oss;
            oss << states[pt];
            return oss.str();
          }());
      }
    }
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    // registrar.set_leaf();
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    // registrar.set_leaf();
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID RollupTask::task_id;

/*! task to query a State to produce a L::Future based on a State predicate */
struct QueryTask : public SerialOnlyTaskMixin<QueryTask, bool> {

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;
  static constexpr char const* task_name = "QueryTask";

  using Args = StatePredicate;
  using states_t = StateRegion;

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> result_t {

    assert(task->arglen == sizeof(Args));
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& pred = *reinterpret_cast<Args const*>(task->args);
    return std::visit(
      overloaded{
        [&](AlivePredicate const& /*alive*/) {
          // alive whenever any of the given states is alive
          return std::ranges::any_of(regions, [&](auto&& region) {
            auto pt = states_t::rect_t{region}.lo;
            return states_t::values<LEGION_READ_ONLY>(region, StateField{})[pt]
              .is_alive();
          });
        },
        [&](SubarrayChangePredicate const& /*chg*/) {
          assert(regions.size() == 2);
          auto const st0 = states_t::values<LEGION_READ_ONLY>(
            regions[0], StateField{})[states_t::rect_t{regions[0]}.lo];
          auto const st1 = states_t::values<LEGION_READ_ONLY>(
            regions[1], StateField{})[states_t::rect_t{regions[1]}.lo];
          return !st0.has_same_subarrays(st1);
        },
        [&](FFTWImportPredicate const&) {
          assert(regions.size() == 1);
          auto const st = states_t::values<LEGION_READ_ONLY>(
            regions[0], StateField{})[states_t::rect_t{regions[0]}.lo];
          return st.fftw_wisdom_import_path()[0] != '\0';
        },
        [&](FFTWExportPredicate const&) {
          assert(regions.size() == 1);
          auto const st = states_t::values<LEGION_READ_ONLY>(
            regions[0], StateField{})[states_t::rect_t{regions[0]}.lo];
          return st.fftw_wisdom_export_path()[0] != '\0';
        }},
      pred);
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<result_t, body<Variant>>(
      registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<result_t, body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID QueryTask::task_id;

/*! task to query a State for a specific subarray to produce a L::Future based
 * on a State predicate */
struct SubarrayQueryTask : public SerialOnlyTaskMixin<SubarrayQueryTask, bool> {

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;
  static constexpr char const* task_name = "SubarrayQueryTask";

  /*! \brief task arguments */
  struct Args {
    /*! \brief subarray slot number */
    unsigned subarray_slot;
    /*! \brief predicate */
    SubarrayStreamPredicate predicate;
    /*! \brief previous state region index */
    int prev_state_region_index;
    /*! \brief current state region index */
    int current_state_region_index;
    /*! \brief next state region index */
    int next_state_region_index;
  };

  /*! \brief local StateRegion type alias */
  using states_t = StateRegion;

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> result_t {

    assert(task->arglen == sizeof(Args));
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& args = *reinterpret_cast<Args const*>(task->args);

    auto const num_regions = static_cast<int>(regions.size());
    // previous interval State
    std::optional<State> prev;
    if (args.prev_state_region_index >= 0) {
      assert(args.prev_state_region_index < num_regions);
      prev = states_t::values<LEGION_READ_ONLY>(
        regions[args.prev_state_region_index],
        StateField{})[states_t::rect_t{regions[args.prev_state_region_index]}
                        .lo];
    }

    // current interval State
    assert(
      0 <= args.current_state_region_index
      && args.current_state_region_index < num_regions);
    auto const current = states_t::values<LEGION_READ_ONLY>(
      regions[args.current_state_region_index],
      StateField{})[states_t::rect_t{regions[args.current_state_region_index]}
                      .lo];

    // next interval State
    std::optional<State> next;
    if (args.next_state_region_index >= 0) {
      assert(args.next_state_region_index < num_regions);
      next = states_t::values<LEGION_READ_ONLY>(
        regions[args.next_state_region_index],
        StateField{})[states_t::rect_t{regions[args.next_state_region_index]}
                        .lo];
    }

    st::SubarrayQuery query{args.subarray_slot, prev, current, next};
    switch (args.predicate) {
    case SubarrayStreamPredicate::is_subarray: {
      return query.is_subarray();
    }
    case SubarrayStreamPredicate::is_imaging_start: {
      return query.do_imaging_start();
    }
    case SubarrayStreamPredicate::is_imaging: {
      return query.do_imaging();
    }
    case SubarrayStreamPredicate::is_imaging_end: {
      return query.do_imaging_stop();
    }
    case SubarrayStreamPredicate::is_recording_visibilities: {
      return query.do_record_visibilities();
    }
    case SubarrayStreamPredicate::is_pre_calibration_flagging: {
      return query.do_pre_calibration_flagging();
    }
    case SubarrayStreamPredicate::do_fftw_import: {
      return query.do_fftw_wisdom_import();
    }
    case SubarrayStreamPredicate::do_fftw_export: {
      return query.do_fftw_wisdom_export();
    }
    default:
      abort();
      return false;
    }
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<result_t, body<Variant>>(
      registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<result_t, body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID SubarrayQueryTask::task_id;

/*! task to get current state of event log */
struct StateTask : public SerialOnlyTaskMixin<StateTask, void> {

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;
  static constexpr char const* task_name = "StateTask";

  enum { elog_region_index = 0, state_region_index, num_regions };

  using elog_t = EventLogRegion;
  using state_t = StateRegion;

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    auto const& elog_region = regions[elog_region_index];
    auto& elog_block = elog_t::values<LEGION_READ_WRITE>(
      elog_region, EventLogPtrsBlockField{})[elog_t::rect_t{elog_region}.lo];
    auto const& state_region = regions[state_region_index];
    auto& state = state_t::values<LEGION_WRITE_ONLY>(
      state_region, StateField{})[state_t::rect_t{state_region}.lo];
    {
      auto lk = std::lock_guard(*elog_block.mtx);
      state = elog_block.log->state();
    }
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID StateTask::task_id;

EventProcessing::EventProcessing(
  L::Context ctx,
  L::Runtime* rt,
  BaselineRegion::index_space_t baseline_index_space,
  std::shared_ptr<EventLog> const& log,
  std::shared_ptr<std::mutex> const& mtx,
  std::size_t shard_point,
  std::size_t num_shards)
  : m_elog(log)
  , m_elog_mtx(mtx)
  , m_elog_block{m_elog.get(), m_elog_mtx.get()}
  , m_baseline_index_space(baseline_index_space) {

  using elog_t = EventLogRegion;

  // create a region for EventCapture instance pointers
  {
    auto maybe_is = elog_t::index_space(
      ctx,
      rt,
      {{elog_t::axes_t::shard,
        elog_t::range_t::right_bounded(static_cast<int>(num_shards - 1))}});
    assert(maybe_is);
    // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
    m_elog_is = maybe_is.value();
    auto fs = elog_t::field_space(ctx, rt);
    m_elog_lr =
      elog_t::LogicalRegion{rt->create_logical_region(ctx, m_elog_is, fs)};
    auto ip = rt->create_equal_partition(ctx, m_elog_is, m_elog_is);
    m_elog_lp =
      elog_t::LogicalPartition{rt->get_logical_partition(m_elog_lr, ip)};
    m_record_launcher = L::IndexTaskLauncher(
      RecordEventsTask::task_id,
      m_elog_is,
      L::UntypedBuffer(),
      L::ArgumentMap());
    m_record_launcher.region_requirements.resize(RecordEventsTask::num_regions);
    m_record_launcher.region_requirements[RecordEventsTask::elog_region_index] =
      m_elog_lp.requirement(
        0,
        LEGION_READ_WRITE,
        LEGION_EXCLUSIVE,
        m_elog_lr,
        StaticFields{EventLogPtrsBlockField{}});
  }

  // Initialize EventCapture instances. Although we run an instance on every
  // shard, only the event log from the first instance (index 0) should be used.
  auto subregion = rt->get_logical_subregion_by_color(m_elog_lp, shard_point);
  {
    auto eptr_ext =
      Realm::ExternalMemoryResource(&m_elog_block, sizeof(m_elog_block));
    auto eptr_launcher =
      L::IndexAttachLauncher(LEGION_EXTERNAL_INSTANCE, m_elog_lr, true);
    eptr_launcher.initialize_constraints(
      false, false, {EventLogPtrsBlockField::field_id});
    eptr_launcher.add_external_resource(subregion, &eptr_ext);
    eptr_launcher.privilege_fields.insert(EventLogPtrsBlockField::field_id);
    m_ext_resources = rt->attach_external_resources(ctx, eptr_launcher);
  }
  m_subarrays_color_space = rt->create_index_space(
    ctx, L::Rect<1, int>{0, Configuration::max_num_subarrays - 1});
}

void
EventProcessing::destroy(L::Context ctx, L::Runtime* rt) {
  auto detached = rt->detach_external_resources(ctx, m_ext_resources);
  rt->destroy_index_space(ctx, m_subarrays_color_space);
  rt->destroy_index_space(ctx, m_elog_is);
  rt->destroy_field_space(ctx, m_elog_lr.get_field_space());
  rt->destroy_logical_region(ctx, m_elog_lr);
  detached.wait();
}

auto
EventProcessing::preregister_tasks(std::array<L::TaskID, 5> const& task_ids)
  -> void {
  PortableTask<RecordEventsTask>::preregister_task_variants(task_ids[0]);
  PortableTask<RollupTask>::preregister_task_variants(task_ids[1]);
  PortableTask<QueryTask>::preregister_task_variants(task_ids[2]);
  PortableTask<SubarrayQueryTask>::preregister_task_variants(task_ids[3]);
  PortableTask<StateTask>::preregister_task_variants(task_ids[4]);
}

auto
EventProcessing::register_tasks(
  L::Runtime* rt, std::array<L::TaskID, 5> const& task_ids) -> void {
  PortableTask<RecordEventsTask>::register_task_variants(rt, task_ids[0]);
  PortableTask<RollupTask>::register_task_variants(rt, task_ids[1]);
  PortableTask<QueryTask>::register_task_variants(rt, task_ids[2]);
  PortableTask<SubarrayQueryTask>::register_task_variants(rt, task_ids[3]);
  PortableTask<StateTask>::register_task_variants(rt, task_ids[4]);
}

auto
EventProcessing::rollup(
  L::Context ctx,
  L::Runtime* rt,
  std::vector<
    std::tuple<StreamClock::external_time, RegionPair<state_t::LogicalRegion>>>
    points,
  std::optional<L::Future> const& trigger,
  L::Predicate pred) -> void {

  if (points.empty())
    return;

  // create the task argument, containing the sorted timestamps
  auto tsval = [](auto&& ts_rg) { return std::get<0>(ts_rg); };
  std::ranges::sort(points, {}, tsval);
  auto args = RollupTask::Args{};
  args.is_initial = !m_most_recent_point;
  args.baseline_index_space = m_baseline_index_space;
  args.subarrays_color_space = m_subarrays_color_space;
  if (m_most_recent_point)
    points.insert(points.begin(), *m_most_recent_point);
  args.timestamps.reserve(points.size());
  std::ranges::copy(
    points | std::views::transform(tsval), std::back_inserter(args.timestamps));
  std::vector<char*> args_buff(args.serialized_size());
  args.serialize(args_buff.data());

  auto launcher = L::IndexTaskLauncher(
    RollupTask::task_id,
    m_elog_is,
    L::UntypedBuffer(args_buff.data(), args_buff.size()),
    L::ArgumentMap(),
    pred);

  // add task predicate and trigger
  if (trigger)
    launcher.add_future(trigger.value());

  launcher.region_requirements.resize(
    RollupTask::states_region_index + points.size());
  // add event log
  launcher.region_requirements[RollupTask::elog_region_index] =
    m_elog_lp.requirement(
      0,
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      m_elog_lr,
      StaticFields{EventLogPtrsBlockField{}});
  // add states regions
  unsigned region_index = RollupTask::states_region_index;
  // first state region may be read-only, if it comes from m_most_recent_point
  L::PrivilegeMode mode =
    (m_most_recent_point ? LEGION_READ_ONLY : LEGION_WRITE_ONLY);
  // assuming here that all State regions share the same index space
  auto& first_rg = get<1>(points.front());
  auto is = first_rg.region().get_index_space();
  auto ip = state_t::index_partition_t{rt->create_equal_partition(ctx, is, is)};
  for (auto&& ts_rg : points) {
    auto& rg = std::get<1>(ts_rg);
    auto lp =
      state_t::LogicalPartition{rt->get_logical_partition(rg.region(), ip)};
    launcher.region_requirements[region_index++] = lp.requirement(
      0, mode, LEGION_EXCLUSIVE, rg.region(), StaticFields{StateField{}});
    // all remaining regions are write-discard
    mode = LEGION_WRITE_ONLY;
  }
  // launch the task
  rt->execute_index_space(ctx, launcher);
  // remember the last point, to include it as the first in the next task launch
  m_most_recent_point = points.back();
}

auto
EventProcessing::current_state(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<state_t::LogicalRegion> const& state) -> void {

  auto launcher = L::IndexTaskLauncher(
    StateTask::task_id, m_elog_is, L::UntypedBuffer(), L::ArgumentMap());
  launcher.region_requirements.resize(StateTask::num_regions);
  launcher.region_requirements[StateTask::elog_region_index] =
    m_elog_lp.requirement(
      0,
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      m_elog_lr,
      StaticFields{EventLogPtrsBlockField{}});
  auto is = state.region().get_index_space();
  auto ip = state_t::index_partition_t{rt->create_equal_partition(ctx, is, is)};
  auto lp =
    state_t::LogicalPartition{rt->get_logical_partition(state.region(), ip)};
  launcher.region_requirements[StateTask::state_region_index] = lp.requirement(
    0,
    LEGION_WRITE_ONLY,
    LEGION_EXCLUSIVE,
    state.region(),
    StaticFields{st::StateField{}});
  rt->execute_index_space(ctx, launcher);
}

auto
EventProcessing::launch_record(
  L::Context ctx, L::Runtime* rt, std::string const& serialized) -> void {
  m_record_launcher.global_arg =
    L::UntypedBuffer(serialized.c_str(), serialized.size());
  rt->execute_index_space(ctx, m_record_launcher);
}

auto
EventProcessing::query_state(
  L::Context ctx,
  L::Runtime* rt,
  std::vector<RegionPair<state_t::LogicalRegion>> const& states,
  StatePredicate const& pred) -> L::Predicate {

  static_assert(std::is_same_v<StatePredicate, QueryTask::Args>);
  auto launcher =
    L::TaskLauncher(QueryTask::task_id, L::UntypedBuffer(&pred, sizeof(pred)));
  launcher.region_requirements.reserve(states.size());
  for (auto&& state0 :
       std::views::transform(states, State0Select(ctx, rt, states.front()))) {
    launcher.region_requirements.push_back(state0.requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{StateField{}}));
  }
  return rt->create_predicate(ctx, rt->execute_task(ctx, launcher));
}

auto
EventProcessing::subarray_predicates(
  L::Context ctx,
  L::Runtime* rt,
  std::array<RegionPair<state_t::LogicalRegion>, 3> const& states)
  -> subarray_predicates_array_t {

  subarray_predicates_array_t result;
  SubarrayQueryTask::Args args;
  auto launcher = L::TaskLauncher(
    SubarrayQueryTask::task_id, L::UntypedBuffer(&args, sizeof(args)));
  for (auto&& subarray : std::views::iota(0U, result.extent(0)))
    for (auto&& predicate : std::views::iota(0U, result.extent(1))) {
      args.predicate = static_cast<SubarrayStreamPredicate>(predicate);
      args.subarray_slot = subarray;
      args.prev_state_region_index = -1;
      args.current_state_region_index = -1;
      args.next_state_region_index = -1;

      launcher.region_requirements.clear();
      assert(states[1].region() != state_t::LogicalRegion{});
      auto state0_select = State0Select(ctx, rt, states[1]);
      if (states[0].region() != state_t::LogicalRegion{}) {
        args.prev_state_region_index =
          static_cast<int>(launcher.region_requirements.size());
        launcher.add_region_requirement(state0_select(states[0]).requirement(
          LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{StateField{}}));
      }
      args.current_state_region_index =
        static_cast<int>(launcher.region_requirements.size());
      launcher.add_region_requirement(state0_select(states[1]).requirement(
        LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{StateField{}}));
      if (states[2].region() != state_t::LogicalRegion{}) {
        args.next_state_region_index =
          static_cast<int>(launcher.region_requirements.size());
        launcher.add_region_requirement(state0_select(states[2]).requirement(
          LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{StateField{}}));
      }
      result(subarray, predicate) =
        rt->create_predicate(ctx, rt->execute_task(ctx, launcher));
    }
  return result;
}

auto
predicate_slice_subarray_predicates(
  subarray_predicates_array_t const& subarray_predicates,
  SubarrayStreamPredicate const& predicate) -> subarray_predicates_t {
  return {
    &subarray_predicates(0, static_cast<unsigned>(predicate)),
    std::experimental::layout_stride::mapping(
      std::experimental::extents<
        subarray_predicates_array_t::size_type,
        Configuration::max_num_subarrays>{},
      std::array{subarray_predicates.stride(0)})};
}

auto
predicate_slice_subarray_futures(
  L::Context ctx,
  L::Runtime* rt,
  subarray_predicates_array_t const& subarray_predicates,
  SubarrayStreamPredicate const& predicate) -> subarray_futures_t {

  subarray_futures_t result;
  auto predicates =
    predicate_slice_subarray_predicates(subarray_predicates, predicate);
  for (auto&& slot : subarray_slots())
    result[slot] = rt->get_predicate_future(ctx, predicates[slot]);
  return result;
}

State0Select::State0Select(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<state_t::LogicalRegion> const& model)
  : m_rt(rt) {
  // will assume here that all State regions share the same index space (this
  // could be tested by storing the index space of the model, but we'll let
  // Legion tell us if we've goofed)
  auto is = model.region().get_index_space();
  m_fs = model.region().get_field_space();
  m_ip = rt->create_equal_partition(ctx, is, is);
}

auto
State0Select::operator()(RegionPair<state_t::LogicalRegion> const& state) const
  -> RegionPair<state_t::LogicalRegion> {
  return {
    state_t::LogicalRegion{m_rt->get_logical_subregion_by_color(
      m_rt->get_logical_partition(
        state_t::logical_region_t{state.region()}, m_ip),
      0)},
    state.parent()};
}

} // end namespace rcp::symcam::st

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
