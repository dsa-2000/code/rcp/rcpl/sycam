// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "GridFourierTransform.hpp"
#include "EventProcessing.hpp"
#include "GridRegion.hpp"
#include "StaticFields.hpp"
#include "libsymcam.hpp"

#include <bark/log.hpp>
#include <cassert>
#include <default_mapper.h>
#include <fftw3.h>

#include <bits/ranges_util.h>
#include <map>
#include <mutex>
#include <rcp/Mapper.hpp>
#include <rcp/rcp.hpp>
#include <variant>
#include <vector>

template <typename T>
struct Plans {
  static std::mutex mutex;

  using tpl = rcp::symcam::FFT_TPL<T>;
  using args_t =
    typename tpl::template FFTArgs<rcp::symcam::GridPixelField::value_t>;

  using plan_key_t = rcp::symcam::GridFourierTransform::Plan::id_t;

  static std::map<plan_key_t, args_t> args;
};

template <typename T>
std::mutex Plans<T>::mutex{};

template <typename T>
std::map<typename Plans<T>::plan_key_t, typename Plans<T>::args_t>
  Plans<T>::args{};

namespace {
template <typename T>
auto
make_plan_args(
  rcp::symcam::GridRegion::rect_t const& rect,
  int direction,
  unsigned flags,
  rcp::symcam::GridPixelField::value_t const* in,
  rcp::symcam::GridPixelField::value_t const* out) ->
  typename Plans<T>::args_t {

  using namespace rcp::symcam;

  typename Plans<T>::args_t result;
  result.in = const_cast<GridPixelField::value_t*>(in);
  result.out = const_cast<GridPixelField::value_t*>(out);
  result.rank = 2;
  result.dims.resize(2);
  int idx0 = grid_ordering().ordering[0];
  auto constexpr xidx = rcp::dim(GridRegion::axes_t::x);
  auto constexpr yidx = rcp::dim(GridRegion::axes_t::y);
  static_assert(std::min(xidx, yidx) == 0 && xidx + yidx == 1);
  assert(idx0 == xidx || idx0 == yidx);
  if (idx0 == xidx) { // (x,y) is column major order in layout
    result.dims[1] = rcp::dim(rect.hi[xidx] - rect.lo[xidx] + 1);
    result.dims[0] = rcp::dim(rect.hi[yidx] - rect.lo[yidx] + 1);
  } else { // (x, y) is row major order in layout
    result.dims[0] = rcp::dim(rect.hi[xidx] - rect.lo[xidx] + 1);
    result.dims[1] = rcp::dim(rect.hi[yidx] - rect.lo[yidx] + 1);
  }
  result.idist = result.dims[0] * result.dims[1];
  result.odist = result.idist;
  result.howmany = rect.volume() / result.idist;
  result.sign = direction;
  result.flags = flags;
  return result;
}

static constexpr auto fftw_variants = rcp::variant_set{
  rcp::task_variant<rcp::TaskVariant::Serial>{}
#ifdef ALLOW_FFTW_OMP
  ,
  rcp::task_variant<rcp::TaskVariant::OpenMP>{}
#endif // ALLOW_FFTW_OMP
};

} // namespace

namespace rcp::symcam {

struct FFTTask {
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::FieldID task_id;

  static constexpr char const* task_name = "FFT";

  static constexpr auto enabled_variants = fftw_variants;

  static constexpr auto max_num_regions = 2;

  struct Args {
    GridFourierTransform::Plan::id_t plan_id;
    int direction;
    unsigned planner_flags;
    std::array<char, 20> name;
    unsigned subarray;
  };

  template <typename T>
  static auto
  make_plan_args(
    Args const& task_args,
    GridRegion::rect_t rect,
    GridPixelField::value_t const* in,
    GridPixelField::value_t const* out) -> typename Plans<T>::args_t {

    if constexpr (std::is_same_v<T, FFTW> || std::is_same_v<T, FFTW_OMP>) {
      // need FFTW_ESTIMATE to avoid overwriting arrays while computing plan
      //
      // TODO: not useful for cufft, for example
      unsigned flags = (task_args.planner_flags
                        & ~(FFTW_MEASURE | FFTW_PATIENT | FFTW_EXHAUSTIVE))
                       | FFTW_ESTIMATE;
      if (in != out)
        flags |= FFTW_PRESERVE_INPUT;
      return ::make_plan_args<T>(rect, task_args.direction, flags, in, out);
    }
    return ::make_plan_args<T>(
      rect, task_args.direction, task_args.planner_flags, in, out);
  }

  template <typename T>
  static auto
  make_plan_args(Args const& task_args, L::PhysicalRegion const& region) ->
    typename Plans<T>::args_t {

    typename Plans<T>::args_t result;
    auto rect = GridRegion::rect_t{region};
    auto values =
      GridRegion::values<LEGION_READ_WRITE>(region, GridPixelField{});
    return make_plan_args<T>(
      task_args, rect, &values[rect.lo], &values[rect.lo]);
  }

  template <typename T>
  static auto
  make_plan_args(
    Args const& task_args,
    L::PhysicalRegion const& input_region,
    L::PhysicalRegion const& output_region) -> typename Plans<T>::args_t {

    typename Plans<T>::args_t result;
    auto in_rect = GridRegion::rect_t{input_region};
    [[maybe_unused]] auto out_rect = GridRegion::rect_t{output_region};
    assert(in_rect == out_rect);
    auto in_values =
      GridRegion::values<LEGION_READ_ONLY>(input_region, GridPixelField{});
    auto out_values =
      GridRegion::values<LEGION_WRITE_ONLY>(output_region, GridPixelField{});
    return make_plan_args<T>(
      task_args, in_rect, &in_values[in_rect.lo], &out_values[in_rect.lo]);
  }

  template <typename T>
  static auto
  execute(
    typename Plans<T>::args_t& plan_args,
    std::vector<L::PhysicalRegion> const& regions) -> void {

    // initialize plan_args members in and out
    if (regions.size() == 1) {
      auto rect = GridRegion::rect_t{regions[0]};
      auto values =
        GridRegion::values<LEGION_READ_WRITE>(regions[0], GridPixelField{});
      plan_args.in = &values[rect.lo];
      plan_args.out = plan_args.in;
    } else {
      assert(regions.size() == 2);
      auto in_rect = GridRegion::rect_t{regions[0]};
      auto out_rect = GridRegion::rect_t{regions[1]};
      assert(in_rect == out_rect);
      auto in_values =
        GridRegion::values<LEGION_READ_ONLY>(regions[0], GridPixelField{});
      auto out_values =
        GridRegion::values<LEGION_WRITE_ONLY>(regions[1], GridPixelField{});
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
      plan_args.in =
        const_cast<GridPixelField::value_t*>(&in_values[in_rect.lo]);
      plan_args.out = &out_values[out_rect.lo];
    }
    FFT_TPL<T>::execute(plan_args);
  }

  template <typename T>
  static auto
  fftw_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    using plans_t = Plans<T>;

    assert(sizeof(Args) == task->arglen);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& task_args = *reinterpret_cast<Args const*>(task->args);
    auto plan_key = typename plans_t::plan_key_t{task_args.plan_id};

    std::optional<typename plans_t::args_t> matching_plan_args;
    {
      // hold plans lock throughout this section, to avoid race creating a new
      // plan
      auto lk = std::lock_guard(plans_t::mutex);
      matching_plan_args = [&]() -> std::optional<typename plans_t::args_t> {
        if (plans_t::args.contains(plan_key))
          return plans_t::args.at(plan_key);
        return std::nullopt;
      }();

      if (!matching_plan_args) {
        log().debug<unsigned long long, int>(
          "lazy make FFT plan",
          bark::val_field_t{"processor", task->current_proc.id},
          bark::val_field_t{"id", task_args.plan_id});
        typename Plans<T>::args_t plan_args;
        if (regions.size() == 1)
          plan_args = make_plan_args<T>(task_args, regions[0]);
        else
          plan_args = make_plan_args<T>(task_args, regions[0], regions[1]);
        if constexpr (std::is_same_v<T, FFTW>) {
          FFT_TPL<T>::make_plan(plan_args);
        } else {
#pragma omp parallel
          {
#pragma omp single
            { FFT_TPL<T>::make_plan(plan_args, omp_get_num_threads()); }
          }
        }
        plans_t::args[plan_key] = plan_args;
        matching_plan_args = plan_args;
      }
    }
    log().debug<char const*, unsigned>(
      "compute FFT",
      bark::val_field_t{"name", task_args.name.data()},
      bark::val_field_t{"subarray", task_args.subarray});
    assert(matching_plan_args);
    // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
    execute<T>(*matching_plan_args, regions);
  }

  template <TaskVariant Variant>
  static auto
  body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    if constexpr (Variant == TaskVariant::Serial) {
      fftw_body<FFTW>(task, regions, ctx, rt);
    }
#ifdef ALLOW_FFTW_OMP
    else {
      static_assert(Variant == TaskVariant::OpenMP);
      fftw_body<FFTW_OMP>(task, regions, ctx, rt);
    }
#endif
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    auto layout =
      grid_layout().constraint_id({traits::layout_style, false}).value();
    for (auto&& idx : std::views::iota(0, max_num_regions))
      registrar.add_layout_constraint_set(idx, layout);
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    auto layout =
      grid_layout().constraint_id(rt, {traits::layout_style, false}).value();
    for (auto&& idx : std::views::iota(0, max_num_regions))
      registrar.add_layout_constraint_set(idx, layout);
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::FieldID FFTTask::task_id;

struct MakePlanTask {
  using grid_t = GridRegion;

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::FieldID task_id;

  static constexpr char const* task_name = "MakePlan";

  static constexpr auto enabled_variants = fftw_variants;

  struct Args {
    int direction;
    GridFourierTransform::Plan::id_t plan_id;
    unsigned planner_flags;
    grid_t::rect_t subspace_rect;
    bool in_place;
  };

  template <typename T>
  static auto
  make_plan_args(
    Args const& task_args,
    GridPixelField::value_t const* in,
    GridPixelField::value_t const* out) -> typename Plans<T>::args_t {

    return ::make_plan_args<T>(
      task_args.subspace_rect,
      task_args.direction,
      task_args.planner_flags,
      in,
      out);
  }

  template <typename T>
  static auto
  fftw_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& /*regions*/,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> bool {

    using plans_t = Plans<T>;

    assert(sizeof(Args) == task->arglen);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& task_args = *reinterpret_cast<Args const*>(task->args);
    auto plan_key = typename plans_t::plan_key_t{task_args.plan_id};
    {
      log().debug<unsigned long long, int>(
        "eager make FFT plan",
        bark::val_field_t{"processor", task->current_proc.id},
        bark::val_field_t{"id", task_args.plan_id});
      auto lk = std::lock_guard(plans_t::mutex);
      if (!plans_t::args.contains(plan_key)) {
        GridPixelField::value_t* in{nullptr};
        GridPixelField::value_t* out{nullptr};
        try {
          FFT_TPL<T>::alloc(in, task_args.subspace_rect.volume());
          if (!task_args.in_place)
            FFT_TPL<T>::alloc(out, task_args.subspace_rect.volume());
          else
            out = in;
          auto plan_args = make_plan_args<T>(task_args, in, out);
          if constexpr (std::is_same_v<T, FFTW>) {
            FFT_TPL<T>::make_plan(plan_args);
          } else {
#pragma omp parallel
            {
#pragma omp single

              { FFT_TPL<T>::make_plan(plan_args, omp_get_num_threads()); }
            }
          }
          plans_t::args[plan_key] = plan_args;
          log().debug<unsigned long long, int>(
            "FFT plan ready",
            bark::val_field_t{"processor", task->current_proc.id},
            bark::val_field_t{"id", task_args.plan_id});
        } catch (...) {
          if (in != nullptr)
            FFT_TPL<T>::free(in);
          if (!task_args.in_place && out != nullptr)
            FFT_TPL<T>::free(out);
          throw;
        }
        if (in != nullptr)
          FFT_TPL<T>::free(in);
        if (!task_args.in_place && out != nullptr)
          FFT_TPL<T>::free(out);
      }
    }
    return true;
  }

  template <TaskVariant Variant>
  static auto
  body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> bool {

    if constexpr (Variant == TaskVariant::Serial) {
      return fftw_body<FFTW>(task, regions, ctx, rt);
    }
#ifdef ALLOW_FFTW_OMP
    else {
      static_assert(Variant == TaskVariant::OpenMP);
      return fftw_body<FFTW_OMP>(task, regions, ctx, rt);
    }
#endif
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<bool, body<Variant>>(
      registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<bool, body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::FieldID MakePlanTask::task_id;

struct DestroyPlanTask {
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::FieldID task_id;

  static constexpr char const* task_name = "DestroyPlan";

  static constexpr auto enabled_variants = fftw_variants;

  struct Args {
    GridFourierTransform::Plan::id_t plan_id;
  };

  template <typename T>
  static auto
  fftw_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& /*regions*/,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> bool {

    using plans_t = Plans<T>;

    assert(sizeof(Args) == task->arglen);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& task_args = *reinterpret_cast<Args const*>(task->args);
    auto plan_key = typename plans_t::plan_key_t{task_args.plan_id};

    log().debug<unsigned long long, int>(
      "destroy FFT plan",
      bark::val_field_t{"processor", task->current_proc.id},
      bark::val_field_t{"id", task_args.plan_id});
    // gather plans with the plan_id for the target processor, and erase them
    // from plans_t::args
    auto lk = std::lock_guard(plans_t::mutex);
    if (plans_t::args.contains(plan_key)) {
      auto plan = plans_t::args.at(plan_key);
      FFT_TPL<T>::destroy_plan(plan);
      plans_t::args.erase(plan_key);
      log().debug<unsigned long long, int>(
        "FFT plan removed",
        bark::val_field_t{"processor", task->current_proc.id},
        bark::val_field_t{"id", task_args.plan_id});
    }
    return true;
  }

  template <TaskVariant Variant>
  static auto
  body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> bool {

    if constexpr (Variant == TaskVariant::Serial) {
      return fftw_body<FFTW>(task, regions, ctx, rt);
    }
#ifdef ALLOW_FFTW_OMP
    else {
      static_assert(Variant == TaskVariant::OpenMP);
      return fftw_body<FFTW_OMP>(task, regions, ctx, rt);
    }
#endif
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<bool, body<Variant>>(
      registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<bool, body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::FieldID DestroyPlanTask::task_id;

GridFourierTransform::Plan::Plan(
  L::Runtime* rt,
  ImagingConfiguration const& img_config,
  id_t id,
  int direction,
  PartitionPair<grid_t::LogicalPartition> const& input_partition,
  PartitionPair<grid_t::LogicalPartition> const& output_partition,
  unsigned planner_flags)
  : m_id{id}
  , m_direction{normalized_direction(direction)}
  , m_input_partition{input_partition}
  , m_output_partition{(output_partition.partition() == grid_t::LogicalPartition{}) ? m_input_partition : output_partition}
  , m_planner_flags(planner_flags) {

  assert(img_config.im.name.size() < m_name.size());
  auto namelen = std::min(m_name.size() - 1, img_config.im.name.size());
  std::strncpy(m_name.data(), img_config.im.name.data(), namelen);
  m_name[namelen] = '\0';

  // require identical index partitions
  auto index_partition = grid_t::index_partition_t{
    m_input_partition.partition().get_index_partition()};
  assert(
    m_output_partition.partition().get_index_partition() == index_partition);

  // require complete, uniform, affine partition
  assert(rt->is_index_partition_complete(index_partition));
  assert(rt->is_index_partition_disjoint(index_partition));

  // check partition for uniformity, and record m_subspace_domain
  auto check_partition = [&, this](auto const& cs) {
    auto pics = L::PointInDomainIterator(cs);
    auto rid = L::RectInDomainIterator(rt->get_index_space_domain(
      rt->get_index_subspace(index_partition, *pics)));
    assert(rid());
    m_subspace_domain = *rid;
    auto ext0 = extents(*rid);
    rid++;
    assert(!rid());
    pics++;
    while (pics()) {
      auto rid = L::RectInDomainIterator(rt->get_index_space_domain(
        rt->get_index_subspace(index_partition, *pics)));
      assert(rid());
      assert(ext0 == extents(*rid));
      rid++;
      assert(!rid());
      pics++;
    }
  };

  auto cs = rt->get_index_partition_color_space(index_partition);
#define CHECK_PARTITION(N)                                                     \
  case N:                                                                      \
    check_partition(L::DomainT<N>(cs));                                        \
    break;
  switch (cs.get_dim()) {
    LEGION_FOREACH_N (CHECK_PARTITION)
    default:
      assert(false);
    abort();
  }
#undef CHECK_PARTITION

  // make the domain indexing be zero-based
  L::DomainPoint new_lo = L::Point<grid_t::dim, grid_t::coord_t>::ZEROES();
  L::DomainPoint new_hi = m_subspace_domain.hi() - m_subspace_domain.lo();
  m_subspace_domain = L::Domain(new_lo, new_hi);
}

auto
GridFourierTransform::Plan::substitute_grids(
  L::Runtime* rt,
  grid_t::LogicalRegion input_grid,
  grid_t::LogicalRegion output_grid) const -> Plan {

  assert(
    input_grid.get_index_space()
    == rt->get_parent_index_space(
      m_input_partition.partition().get_index_partition()));
  assert(
    output_grid.get_index_space()
    == rt->get_parent_index_space(
      m_output_partition.partition().get_index_partition()));

  auto result = Plan{*this};
  result.m_input_partition = PartitionPair<grid_t::LogicalPartition>{
    grid_t::LogicalPartition{rt->get_logical_partition(
      grid_t::logical_region_t{input_grid},
      m_input_partition.partition().get_index_partition())},
    grid_t::LogicalRegion{grid_t::logical_region_t{input_grid}}};
  if (in_place())
    result.m_output_partition = result.m_input_partition;
  else
    result.m_output_partition = PartitionPair<grid_t::LogicalPartition>{
      grid_t::LogicalPartition{rt->get_logical_partition(
        grid_t::logical_region_t{output_grid},
        m_output_partition.partition().get_index_partition())},
      grid_t::LogicalRegion{grid_t::logical_region_t{output_grid}}};
  return result;
}

auto
GridFourierTransform::Plan::extents(grid_t::rect_t const& rect)
  -> L::Point<grid_t::dim, grid_t::coord_t> {
  return rect.hi - rect.lo + L::Point<grid_t::dim, grid_t::coord_t>::ONES();
}

auto
GridFourierTransform::make_plan(
  L::Context ctx,
  L::Runtime* rt,
  Plan const& plan,
  L::Future wisdom,
  L::Predicate pred) -> std::vector<L::Future> {

  std::vector<L::Future> result;
  {
    auto tunable = Legion::Mapping::DefaultMapper::DefaultTunables ::
      DEFAULT_TUNABLE_GLOBAL_CPUS;
    auto num_cpus = rt->select_tunable_value(ctx, tunable).get<std::size_t>();
    // launch task over all cpus, task implementation ensures that plans are
    // computed only once per process address space
    if (num_cpus > 0) {
      auto launch_space = rt->create_index_space(
        ctx, L::Rect<1>{0, static_cast<coord_t>(num_cpus) - 1});
      MakePlanTask::Args args{
        .plan_id = plan.m_id,
        .planner_flags = plan.m_planner_flags,
        .subspace_rect = grid_t::rect_t{plan.m_subspace_domain},
        .in_place = plan.in_place()};
      auto launcher = L::IndexTaskLauncher{
        MakePlanTask::task_id,
        launch_space,
        L::UntypedBuffer(&args, sizeof(args)),
        L::ArgumentMap{},
        pred}; // NOLINT(performance-unnecessary-value-param)
      launcher.tag = Mapper::loc_restricted_tag;
      result.emplace_back(rt->reduce_future_map(
        ctx,
        rt->execute_index_space(ctx, launcher),
        L::MaxReduction<bool>::REDOP_ID));
      rt->destroy_index_space(ctx, launch_space);
    }
  }
#ifdef ALLOW_FFTW_OMP
  {
    auto tunable = Legion::Mapping::DefaultMapper::DefaultTunables ::
      DEFAULT_TUNABLE_GLOBAL_OMPS;
    auto num_omps = rt->select_tunable_value(ctx, tunable).get<std::size_t>();
    // launch task over all OMP_PROC, task implementation ensures that plans are
    // computed only once per process address space
    if (num_omps > 0) {
      auto launch_space = rt->create_index_space(
        ctx, L::Rect<1>{0, static_cast<coord_t>(num_omps) - 1});
      MakePlanTask::Args args{
        .plan_id = plan.m_id,
        .planner_flags = plan.m_planner_flags,
        .subspace_rect = grid_t::rect_t{plan.m_subspace_domain},
        .in_place = plan.in_place()};
      auto launcher = L::IndexTaskLauncher{
        MakePlanTask::task_id,
        launch_space,
        L::UntypedBuffer(&args, sizeof(args)),
        L::ArgumentMap{},
        pred};
      launcher.add_future(wisdom);
      launcher.tag = Mapper::omp_restricted_tag;
      result.emplace_back(rt->reduce_future_map(
        ctx,
        rt->execute_index_space(ctx, launcher),
        L::MaxReduction<bool>::REDOP_ID));
      rt->destroy_index_space(ctx, launch_space);
    }
  }
#endif // ALLOW_FFTW_OMP
  return result;
}

auto
GridFourierTransform::init_launcher(L::UntypedBuffer arg)
  -> L::IndexTaskLauncher {

  return L::IndexTaskLauncher(
    FFTTask::task_id, L::IndexSpace(), arg, L::ArgumentMap());
}

auto
GridFourierTransform::update_launcher(
  L::Context ctx,
  L::Runtime* rt,
  sarray<Plan>& plans,
  unsigned subarray_slot,
  L::IndexTaskLauncher& launcher) -> void {

  auto& plan = plans.at(subarray_slot);
  launcher.launch_space = rt->get_index_partition_color_space_name(
    ctx, plan.m_input_partition.partition().get_index_partition());
  launcher.region_requirements.clear();
  if (plan.in_place()) {
    launcher.add_region_requirement(plan.m_input_partition.requirement(
      rt,
      0,
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{GridPixelField{}}));
  } else {
    launcher.add_region_requirement(plan.m_input_partition.requirement(
      rt,
      0,
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{GridPixelField{}}));
    launcher.add_region_requirement(plan.m_output_partition.requirement(
      rt,
      0,
      LEGION_WRITE_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{GridPixelField{}}));
  }
  auto& args = *reinterpret_cast<FFTTask::Args*>(launcher.global_arg.get_ptr());
  args.plan_id = plan.m_id;
  args.direction = plan.m_direction;
  args.subarray = subarray_slot;
  std::ranges::copy(plan.m_name, args.name.begin());
}

auto
GridFourierTransform::launch(
  L::Context ctx,
  L::Runtime* rt,
  sarray<Plan>& plans,
  st::subarray_predicates_array_t const& predicates) -> void {

  auto is_imaging_end = predicate_slice_subarray_predicates(
    predicates, st::SubarrayStreamPredicate::is_imaging_end);

  FFTTask::Args args;
  auto launcher = init_launcher(L::UntypedBuffer(&args, sizeof(args)));
  for (auto&& subarray_slot : subarray_slots()) {
    update_launcher(ctx, rt, plans, subarray_slot, launcher);
    launcher.predicate = is_imaging_end(subarray_slot);
    rt->execute_index_space(ctx, launcher);
  }
}

auto
GridFourierTransform::launch(
  L::Context ctx,
  L::Runtime* rt,
  sarray<Plan>& plans,
  st::AllSubarraysQuery const& state_query) -> void {

  auto do_imaging_stop = state_query.do_imaging_stop();

  FFTTask::Args args;
  auto launcher = init_launcher(L::UntypedBuffer(&args, sizeof(args)));
  for (auto&& subarray_slot : filtered_subarray_slots(do_imaging_stop)) {
    update_launcher(ctx, rt, plans, subarray_slot, launcher);
    rt->execute_index_space(ctx, launcher);
  }
}

auto
GridFourierTransform::destroy_plan(
  L::Context ctx, L::Runtime* rt, Plan const& plan, L::Predicate pred)
  -> std::vector<L::Future> {

  std::vector<L::Future> result;
  {
    auto tunable = Legion::Mapping::DefaultMapper::DefaultTunables ::
      DEFAULT_TUNABLE_GLOBAL_CPUS;
    auto num_cpus = rt->select_tunable_value(ctx, tunable).get<std::size_t>();
    if (num_cpus > 0) {
      auto launch_space = rt->create_index_space(
        ctx, L::Rect<1>{0, static_cast<coord_t>(num_cpus) - 1});
      DestroyPlanTask::Args args{.plan_id = plan.m_id};
      auto launcher = L::IndexTaskLauncher{
        DestroyPlanTask::task_id,
        launch_space,
        L::UntypedBuffer(&args, sizeof(args)),
        L::ArgumentMap{},
        pred}; // NOLINT(performance-unnecessary-value-param)
      launcher.tag = Mapper::loc_restricted_tag;
      result.emplace_back(rt->reduce_future_map(
        ctx,
        rt->execute_index_space(ctx, launcher),
        L::MaxReduction<bool>::REDOP_ID));
      rt->destroy_index_space(ctx, launch_space);
    }
  }
#ifdef ALLOW_FFTW_OMP
  {
    auto tunable = Legion::Mapping::DefaultMapper::DefaultTunables ::
      DEFAULT_TUNABLE_GLOBAL_OMPS;
    auto num_omps = rt->select_tunable_value(ctx, tunable).get<std::size_t>();
    if (num_omps > 0) {
      auto launch_space = rt->create_index_space(
        ctx, L::Rect<1>{0, static_cast<coord_t>(num_omps) - 1});
      DestroyPlanTask::Args args{.plan_id = plan.m_id};
      auto launcher = L::IndexTaskLauncher{
        DestroyPlanTask::task_id,
        launch_space,
        L::UntypedBuffer(&args, sizeof(args)),
        L::ArgumentMap{},
        pred};
      launcher.tag = Mapper::omp_restricted_tag;
      result.emplace_back(rt->reduce_future_map(
        ctx,
        rt->execute_index_space(ctx, launcher),
        L::MaxReduction<bool>::REDOP_ID));
      rt->destroy_index_space(ctx, launch_space);
    }
  }
#endif // ALLOW_FFTW_OMP
  return result;
}

auto
GridFourierTransform::preregister_tasks(
  std::array<L::TaskID, 3> const& task_ids) -> void {

  FFT_TPL<FFTW>::init<GridPixelField::value_t>();
  PortableTask<MakePlanTask>::preregister_task_variants(task_ids[0]);
  PortableTask<FFTTask>::preregister_task_variants(task_ids[1]);
  PortableTask<DestroyPlanTask>::preregister_task_variants(task_ids[2]);
}

auto
GridFourierTransform::register_tasks(
  L::Runtime* rt, std::array<L::TaskID, 3> const& task_ids) -> void {

  FFT_TPL<FFTW>::init<GridPixelField::value_t>();
  PortableTask<MakePlanTask>::register_task_variants(rt, task_ids[0]);
  PortableTask<FFTTask>::register_task_variants(rt, task_ids[1]);
  PortableTask<DestroyPlanTask>::register_task_variants(rt, task_ids[2]);
}

GridFourierTransform::Plan::Plan(
  L::Context ctx, L::Runtime* rt, Serialized const& serialized) noexcept
  : m_name(serialized.m_name)
  , m_id(serialized.m_id)
  , m_direction(serialized.m_direction)
  , m_input_partition(serialized.m_input_partition)
  , m_output_partition(serialized.m_output_partition) {}

auto
GridFourierTransform::Plan::serialized(
  L::Context ctx, L::Runtime* rt) const noexcept -> Serialized {

  return {m_name, m_id, m_direction, m_input_partition, m_output_partition};
}

auto
GridFourierTransform::Plan::child_requirements(
  L::Context ctx, L::Runtime* rt) const -> ChildRequirements {

  std::vector<L::RegionRequirement> reqs;
  if (in_place()) {
    reqs.push_back(m_input_partition.parent(rt).requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{GridPixelField{}},
      std::nullopt,
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP));
  } else {
    reqs.push_back(m_input_partition.parent(rt).requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{GridPixelField{}},
      std::nullopt,
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP));
    reqs.push_back(m_output_partition.parent(rt).requirement(
      LEGION_WRITE_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{GridPixelField{}},
      std::nullopt,
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP));
  }
  return {reqs, {}, {}};
}

auto
GridFourierTransform::Plan::in_place() const -> bool {
  return m_input_partition.partition() == m_output_partition.partition();
}

} // namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
