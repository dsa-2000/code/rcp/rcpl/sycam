// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include "ReceiverRegion.hpp"
#include "libsymcam.hpp"

#include <array>
#include <memory>

namespace rcp::symcam {

/*! \brief launcher for task to get receiver (antenna) position coordinates
 */
class GetReceiverPositions {
public:
  /*! \brief type alias for antenna positions regions */
  using rcv_t = ReceiverRegion;

  /*! \brief task argument type, currently defined for testing */
  struct Args {
    // TODO: remove...for testing only
    std::array<unsigned, 3> array_extent_m;
  };

private:
  /*! \brief task arguments */
  std::shared_ptr<Args> m_args;

  /*! \brief task launcher */
  L::TaskLauncher m_launcher;

public:
  /*! \brief constructor from Configuration */
  GetReceiverPositions(Configuration const& config);

  /*! \brief copy constructor (defaulted) */
  GetReceiverPositions(GetReceiverPositions const&) = default;

  /*! \brief move constructor (defaulted) */
  GetReceiverPositions(GetReceiverPositions&&) noexcept = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(GetReceiverPositions const&) -> GetReceiverPositions& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(GetReceiverPositions&&) noexcept -> GetReceiverPositions& = default;

  /*! \brief destructor (defaulted) */
  ~GetReceiverPositions() = default;

  /*! \brief launch task to get receiver positions */
  auto
  launch(L::Context ctx, L::Runtime* rt) -> rcv_t::LogicalRegion;

  /*! \brief preregister task with Legion runtime */
  static auto
  preregister_tasks(L::TaskID tid) -> void;

  /*! \brief register task with Legion runtime */
  static auto
  register_tasks(L::Runtime* rt, L::TaskID tid) -> void;
};
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
