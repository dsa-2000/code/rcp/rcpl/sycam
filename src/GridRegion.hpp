// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include "StaticFields.hpp"
#include "libsymcam.hpp"

#include <rcp/dc/rcpdc.hpp>

#include <array>
#include <complex>
#include <map>
#include <vector>

/*! \file GridRegion.hpp
 *
 * GridRegion definitions
 *
 * \todo add bset axis to GridAxes
 */

namespace rcp::symcam {

/*! \brief axes for (x, y, stokes, w-plane, channel) index */
enum class GridAxes {
  x = LEGION_DIM_0, /*!< x pixel */
  y,                /*!< y pixel */
  stokes,           /*!< Stokes component */
  w_plane,          /*!< w-plane index */
  channel,          /*!< channel index */
};

/*! \brief IndexSpec specialization for GridRegionAxes
 *
 * \tparam Coord index coordinate value type
 */
template <typename Coord>
using grid_index_spec_t =
  IndexSpec<GridAxes, GridAxes::x, GridAxes::channel, Coord>;

/*! \brief default static bounds for regions with GridAxes
 *
 * \tparam Coord index coordinate value type
 *
 * Bounds are x: [Coord min, Coord max]; y: [Coord min, Coord max]; stokes [0,
 * Configuration::num_polarization_products - 1], w_plane: [0, Coord max],
 * channel: [0, Coord max].
 */
template <typename Coord>
constexpr auto default_grid_bounds = std::array{
  IndexInterval<Coord>::unbounded(),
  IndexInterval<Coord>::unbounded(),
  IndexInterval<Coord>::bounded(
    0, Configuration::num_polarization_products - 1),
  IndexInterval<Coord>::left_bounded(0),
  IndexInterval<Coord>::left_bounded(0)};

/* \brief grid pixel field definition
 *
 * COMPLEX_NAMESPACE is defined in Legion mathtypes/complex.h
 */
using GridPixelField = StaticField<
  accval<std::complex<Configuration::grid_value_precision_t>>,
  field_ids::grid_pixel,
  nullptr,
  accval<K::complex<Configuration::grid_value_precision_t>>,
  accval<COMPLEX_NAMESPACE::complex<Configuration::grid_value_precision_t>>>;

/*! \brief grid region definition */
struct GridRegion
  : public StaticRegionSpec<
      grid_index_spec_t<coord_t>,
      default_grid_bounds<coord_t>,
      GridPixelField> {

  static_assert(std::is_same_v<
                std::complex<Configuration::grid_value_precision_t>,
                accval_value_t<GridPixelField::value_t>>);

  using static_region_t = StaticRegionSpec<
    grid_index_spec_t<coord_t>,
    default_grid_bounds<coord_t>,
    GridPixelField>;

  struct Params {
    std::array<Configuration::uvw_value_t, 3> cell_size;
  };
  static constexpr L::SemanticTag params_tag = 18;

  static void
  attach_params(L::Runtime* rt, LogicalRegion region, Params const& params);

  [[nodiscard]] static auto
  has_params(L::Runtime* rt, LogicalRegion region) -> bool;

  [[nodiscard]] static auto
  retrieve_params(L::Runtime* rt, LogicalRegion region) -> Params const&;

  static auto
  create_ixy_tile_partition(
    L::Context ctx,
    L::Runtime* rt,
    index_space_t grid,
    Configuration const& config,
    ImageConfiguration const& im,
    std::ranges::range auto&& x_cuts_m,
    std::ranges::range auto&& y_cuts_m,
    coord_t x_padding,
    coord_t y_padding,
    L::Color color = LEGION_AUTO_GENERATE_ID,
    char const* provenance = nullptr) -> index_partition_t {

    static_assert(std::is_same_v<
                  std::ranges::range_value_t<decltype(x_cuts_m)>,
                  std::vector<Configuration::uvw_value_t>>);
    static_assert(std::is_same_v<
                  std::ranges::range_value_t<decltype(y_cuts_m)>,
                  std::vector<Configuration::uvw_value_t>>);

    assert(
      std::ranges::all_of(x_cuts_m, [](auto&& xc) { return xc.size() >= 2; }));
    assert(
      std::ranges::all_of(y_cuts_m, [](auto&& yc) { return yc.size() >= 2; }));

    L::Rect<dim, coord_t> grid_bounds = rt->get_index_space_domain(grid).bounds;
    auto channels = std::views::iota(
      grid_bounds.lo[static_cast<int>(axes_t::channel)],
      grid_bounds.hi[static_cast<int>(axes_t::channel)] + 1);
    assert(channels.size() == x_cuts_m.size());
    assert(channels.size() == y_cuts_m.size());
    auto channels_lo = channels.front();

    // frequency at upper edge of each image channel
    std::vector<Configuration::frequency_t> freq_hi;
    std::ranges::transform(
      config.image_channel_configurations(im),
      std::back_inserter(freq_hi),
      [](auto&& ch_config) { return ch_config[1]; });
    assert(freq_hi.size() == channels.size());

    // color space of partition
    std::vector<L::Rect<3, coord_t>> cs_rects;
    for (auto&& ch : channels)
      cs_rects.push_back(L::Rect<3, coord_t>{
        {ch, 0, 0},
        {ch,
         coord_t(x_cuts_m[ch - channels_lo].size() - 2),
         coord_t(y_cuts_m[ch - channels_lo].size() - 2)}});
    L::IndexSpaceT<3, coord_t> cs = rt->create_index_space(ctx, cs_rects);

    // create the partition one channel at a time
    std::map<L::Point<3, coord_t>, L::DomainT<dim, coord_t>> domains;
    L::Rect<dim, coord_t> tile_bounds = grid_bounds;
    for (auto&& ch : channels) {
      // convert cuts to grid coordinates
      auto m_to_c = rcp::meters_to_cycles(freq_hi[ch - channels_lo]);
      std::vector<coord_t> x_cuts_px;
      std::ranges::transform(
        x_cuts_m[ch - channels_lo],
        std::back_inserter(x_cuts_px),
        [&](auto&& xc) -> coord_t {
          return im.cell_width_arcsec * m_to_c(xc);
        });
      std::vector<coord_t> y_cuts_px;
      std::ranges::transform(
        y_cuts_m[ch - channels_lo],
        std::back_inserter(y_cuts_px),
        [&](auto&& yc) -> coord_t {
          return im.cell_height_arcsec * m_to_c(yc);
        });

      // construct tile domains
      tile_bounds.lo[static_cast<int>(axes_t::channel)] =
        tile_bounds.hi[static_cast<int>(axes_t::channel)] = ch;
      coord_t xc_min = x_cuts_px.front();
      for (auto&& xc : std::views::drop(x_cuts_px, 1)) {
        tile_bounds.lo[static_cast<int>(axes_t::x)] = xc_min - x_padding;
        tile_bounds.hi[static_cast<int>(axes_t::x)] = xc + x_padding;
        coord_t yc_min = y_cuts_px.front();
        for (auto&& yc : std::views::drop(y_cuts_px, 1)) {
          // note that we're letting the Legion runtime do the intersection with
          // the grid index space in x and y (if we knew that the index space
          // was dense, we might do it ourselves, but this implementation covers
          // the general case to avoid restricting that index space)
          tile_bounds.lo[static_cast<int>(axes_t::y)] = yc_min - y_padding;
          tile_bounds.hi[static_cast<int>(axes_t::y)] = yc + y_padding;
          auto pt = L::Point<3, coord_t>{
            ch,
            coord_t(std::distance(x_cuts_px.data(), &xc)) - 1,
            coord_t(std::distance(y_cuts_px.data(), &yc)) - 1};
          domains[pt] = tile_bounds;
          // TODO: enable log classes, then use the following
          // log().info(
          //   std::string("tile @") + showl(pt) + ": " + showl(tile_bounds));
          yc_min = yc;
        }
        xc_min = xc;
      }
    }
    return rt->create_partition_by_domain(
      ctx, grid, domains, cs, true, LEGION_COMPUTE_KIND, color, provenance);
  }
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
