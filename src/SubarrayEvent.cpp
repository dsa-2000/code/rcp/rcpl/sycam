// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "SubarrayEvent.hpp"
#include <rcp/dc/rcpdc.hpp>

namespace rcp::symcam::st {

auto
JSONSubarrayEvent::has_subarray(nlohmann::json const& js) noexcept -> bool {
  return js.contains(subarray_tag);
}

/*! \brief get subarray name value */
auto
JSONSubarrayEvent::get_subarray(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(subarray_tag);
}

auto
JSONSubarrayEvent::get_subarray(nlohmann::json& js) -> nlohmann::json& {
  return js.at(subarray_tag);
}

auto
JSONSubarrayEvent::is_valid(nlohmann::json const& js) noexcept -> bool {
  return JSONSimpleEvent::is_valid(js) && has_subarray(js)
         && js.at(subarray_tag).is_string()
         && get_subarray(js).get<std::string>().size()
              < subarray_name_t{}.size();
}

auto
JSONReceiversOuterProduct::get_left(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(left_tag);
}

auto
JSONReceiversOuterProduct::get_right(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(right_tag);
}

auto
JSONReceiversOuterProduct::is_valid(nlohmann::json const& js) noexcept -> bool {
  return js.contains(left_tag) && js.contains(right_tag)
         && JSONReceiverSet::is_valid(get_left(js))
         && JSONReceiverSet::is_valid(get_right(js));
}

auto
JSONReceiverPairs::is_valid(nlohmann::json const& js) noexcept -> bool {
  return js.is_array() && std::ranges::all_of(js, [](auto&& aval) {
           return aval.is_array() && aval.size() == 2
                  && JSONReceiverSet::is_valid(aval);
         });
}

auto
JSONReceiverPairSet::is_valid(nlohmann::json const& js) noexcept -> bool {
  return (js.is_array() && JSONReceiverPairs::is_valid(js))
         || JSONReceiversOuterProduct::is_valid(js);
}

auto
JSONCreateSubarray::has_slot(nlohmann::json const& js) noexcept -> bool {
  return js.contains(slot_tag);
}

auto
JSONCreateSubarray::get_slot(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(slot_tag);
}

auto
JSONCreateSubarray::get_slot(nlohmann::json& js) -> nlohmann::json& {
  return js.at(slot_tag);
}

auto
JSONCreateSubarray::has_pairs(nlohmann::json const& js) noexcept -> bool {
  return js.contains(pairs_tag);
}

auto
JSONCreateSubarray::get_pairs(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(pairs_tag);
}

auto
JSONCreateSubarray::get_pairs(nlohmann::json& js) -> nlohmann::json& {
  return js.at(pairs_tag);
}

auto
JSONCreateSubarray::is_valid(nlohmann::json const& js) noexcept -> bool {
  return JSONSubarrayEvent::is_valid(js) && has_slot(js)
         && get_slot(js).is_number_integer()
         && get_slot(js).get<unsigned>() < Configuration::max_num_subarrays
         && has_pairs(js) && JSONReceiverPairSet::is_valid(get_pairs(js));
}

CreateSubarray::CreateSubarray(
  unsigned slot,
  subarray_name_t const& subarray_name,
  subarray_t subarray,
  StreamClock::timestamp_t const& ts) noexcept
  : SubarrayEvent<CreateSubarray>(subarray_name, ts)
  , m_slot(slot)
  , m_subarray(std::move(subarray)) {}

CreateSubarray::CreateSubarray(
  CreateSubarray const& event, StreamClock::timestamp_t const& ts)
  : SubarrayEvent<CreateSubarray>(event, ts)
  , m_slot(event.m_slot)
  , m_subarray(event.m_subarray) {}

CreateSubarray::CreateSubarray(
  CreateSubarray&& event, StreamClock::timestamp_t const& ts)
  : SubarrayEvent<CreateSubarray>(std::move(event), ts)
  , m_slot(std::move(event).m_slot)
  , m_subarray(std::move(event).m_subarray) {}

auto
CreateSubarray::apply_to_no_ts(ExtendedState&& st) const -> ExtendedState {
  if (st.is_alive()) {
    st = std::move(st).set_subarray(m_slot, name(), subarray());
  }
  return st;
}

auto
CreateSubarray::slot() const -> unsigned const& {
  return m_slot;
}

auto
CreateSubarray::subarray() const -> subarray_t const& {
  return m_subarray;
}

auto
CreateSubarray::slot() -> unsigned& {
  return m_slot;
}

auto
CreateSubarray::subarray() -> subarray_t& {
  return m_subarray;
}

auto
DestroySubarray::apply_to_no_ts(ExtendedState&& st) const -> ExtendedState {
  if (st.is_alive()) {
    auto maybe_slot = st.find_subarray(name());
    if (!maybe_slot)
      throw InvalidSubarrayName(name());
    st = std::move(st).reset_subarray(*maybe_slot);
  }
  return st;
}

auto
JSONStartRecordEventStream::has_filepath(nlohmann::json const& js) noexcept
  -> bool {
  return js.contains(filepath_tag);
}

auto
JSONStartRecordEventStream::get_filepath(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(filepath_tag);
}

auto
JSONStartRecordEventStream::get_filepath(nlohmann::json& js)
  -> nlohmann::json& {
  return js.at(filepath_tag);
}

auto
JSONStartRecordEventStream::is_valid(nlohmann::json const& js) noexcept
  -> bool {
  return JSONSubarrayEvent::is_valid(js) && has_filepath(js)
         && get_filepath(js).is_string();
}

StartRecordEventStream::StartRecordEventStream(
  subarray_name_t const& subarray_name,
  std::filesystem::path filepath,
  StreamClock::timestamp_t const& ts) noexcept
  : SubarrayEvent<StartRecordEventStream>(subarray_name, ts)
  , m_filepath(std::move(filepath)) {}

StartRecordEventStream::StartRecordEventStream(
  StartRecordEventStream const& event, StreamClock::timestamp_t const& ts)
  : SubarrayEvent<StartRecordEventStream>(event, ts)
  , m_filepath(event.m_filepath) {}
/*! \brief move constructor with updated timestamp */
StartRecordEventStream::StartRecordEventStream(
  StartRecordEventStream&& event, StreamClock::timestamp_t const& ts)
  : SubarrayEvent<StartRecordEventStream>(std::move(event), ts)
  , m_filepath(std::move(event).m_filepath) {}

auto
StartRecordEventStream::apply_to_no_ts(ExtendedState&& st) const
  -> ExtendedState {
  if (st.is_alive()) {
    auto maybe_slot = st.find_subarray(name());
    if (!maybe_slot)
      throw InvalidSubarrayName(name());
    st = std::move(st).set_record_event_stream(*maybe_slot, m_filepath);
  }
  return st;
}

auto
StartRecordEventStream::filepath() const -> std::filesystem::path const& {
  return m_filepath;
}

auto
StartRecordEventStream::filepath() -> std::filesystem::path& {
  return m_filepath;
}

auto
StopRecordEventStream::apply_to_no_ts(ExtendedState&& st) const
  -> ExtendedState {
  if (st.is_alive()) {
    auto maybe_slot = st.find_subarray(name());
    if (!maybe_slot)
      throw InvalidSubarrayName(name());
    st = std::move(st).reset_record_event_stream(*maybe_slot);
  }
  return st;
}

auto
JSONStartRecordDataStream::has_filepath(nlohmann::json const& js) noexcept
  -> bool {
  return js.contains(filepath_tag);
}

auto
JSONStartRecordDataStream::get_filepath(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(filepath_tag);
}

auto
JSONStartRecordDataStream::get_filepath(nlohmann::json& js) -> nlohmann::json& {
  return js.at(filepath_tag);
}

auto
JSONStartRecordDataStream::has_channels(nlohmann::json const& js) noexcept
  -> bool {
  return js.contains(channels_tag);
}

auto
JSONStartRecordDataStream::get_channels(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(channels_tag);
}

auto
JSONStartRecordDataStream::get_channels(nlohmann::json& js) -> nlohmann::json& {
  return js.at(channels_tag);
}

auto
JSONStartRecordDataStream::parse_channels(nlohmann::json const& js)
  -> std::optional<ChannelSet> {
  using channel_t = ChannelSet::channel_t;
  std::optional<ChannelSet> result;
  if (!js.empty()) {
    if (js.is_array()) {
      // an array of channel index values
      std::vector<std::array<channel_t, 2>> channels;
      for (auto&& chjs : js) {
        if (!chjs.is_number_integer())
          return result;
        auto ch = chjs.get<int>();
        if (ch < 0)
          return result;
        channels.push_back(
          {static_cast<channel_t>(ch), static_cast<channel_t>(ch)});
      }
      result = ChannelSet(channels);
    } else if (js.is_string()) {
      // a string of the form XXX-YYY, for a range of channels where XXX is
      // the first channel, and YYY, the last
      auto str = js.get<std::string>();
      auto const regex = std::regex("(\\d+)-(\\d+)");
      std::smatch match;
      if (std::regex_match(str, match, regex) && match.size() == 3) {
        auto lo = std::strtoul(match[1].str().c_str(), nullptr, 0);
        auto hi = std::strtoul(match[2].str().c_str(), nullptr, 0);
        result = ChannelSet(
          {{static_cast<channel_t>(lo), static_cast<channel_t>(hi)}});
      }
    }
  }
  return result;
}

auto
JSONStartRecordDataStream::has_parameters(nlohmann::json const& js) noexcept
  -> bool {
  return js.contains(parameters_tag);
}

auto
JSONStartRecordDataStream::get_parameters(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(parameters_tag);
}

auto
JSONStartRecordDataStream::get_parameters(nlohmann::json& js)
  -> nlohmann::json& {
  return js.at(parameters_tag);
}

auto
JSONStartRecordDataStream::parse_parameters(nlohmann::json const& js)
  -> std::optional<std::unordered_map<std::string, std::string>> {
  try {
    auto obj = js.get<std::unordered_map<std::string, nlohmann::json>>();
    std::unordered_map<std::string, std::string> result;
    for (auto&& kv : obj) {
      auto& [key, val] = kv;
      result.insert({key, val.get<std::string>()});
    }
    return result;
  } catch (...) {
    return std::nullopt;
  }
}

auto
JSONStartRecordDataStream::is_valid(nlohmann::json const& js) noexcept -> bool {
  return JSONSubarrayEvent::is_valid(js) && has_filepath(js)
         && get_filepath(js).is_string() && has_channels(js)
         && bool(parse_channels(get_channels(js)))
         && (!has_parameters(js) || bool(parse_parameters(get_parameters(js))));
}

StartRecordDataStream::StartRecordDataStream(
  subarray_name_t const& subarray_name,
  ChannelSet channels,
  std::unordered_map<std::string, std::string> parameters,
  std::filesystem::path filepath,
  StreamClock::timestamp_t const& ts) noexcept
  : SubarrayEvent<StartRecordDataStream>(subarray_name, ts)
  , m_channels(std::move(channels))
  , m_parameters(std::move(parameters))
  , m_filepath(std::move(filepath)) {}

StartRecordDataStream::StartRecordDataStream(
  StartRecordDataStream const& event, StreamClock::timestamp_t const& ts)
  : SubarrayEvent<StartRecordDataStream>(event, ts)
  , m_channels(event.m_channels)
  , m_parameters(event.m_parameters)
  , m_filepath(event.m_filepath) {}

StartRecordDataStream::StartRecordDataStream(
  StartRecordDataStream&& event, StreamClock::timestamp_t const& ts)
  : SubarrayEvent<StartRecordDataStream>(std::move(event), ts)
  , m_channels(std::move(event).m_channels)
  , m_parameters(std::move(event).m_parameters)
  , m_filepath(std::move(event).m_filepath) {}

auto
StartRecordDataStream::apply_to_no_ts(ExtendedState&& st) const
  -> ExtendedState {
  if (st.is_alive()) {
    auto maybe_slot = st.find_subarray(name());
    if (!maybe_slot)
      throw InvalidSubarrayName(name());
    st = std::move(st).set_record_data_stream(
      *maybe_slot, m_channels, m_parameters, m_filepath);
  }
  return st;
}

auto
StartRecordDataStream::filepath() const -> std::filesystem::path const& {
  return m_filepath;
}

auto
StartRecordDataStream::channels() const -> ChannelSet const& {
  return m_channels;
}

auto
StartRecordDataStream::parameters() const
  -> std::unordered_map<std::string, std::string> const& {
  return m_parameters;
}

auto
StartRecordDataStream::filepath() -> std::filesystem::path& {
  return m_filepath;
}

auto
StartRecordDataStream::channels() -> ChannelSet& {
  return m_channels;
}

auto
StartRecordDataStream::parameters()
  -> std::unordered_map<std::string, std::string>& {
  return m_parameters;
}

auto
StopRecordDataStream::apply_to_no_ts(ExtendedState&& st) const
  -> ExtendedState {
  if (st.is_alive()) {
    auto maybe_slot = st.find_subarray(name());
    if (!maybe_slot)
      throw InvalidSubarrayName(name());
    st = std::move(st).reset_record_data_stream(*maybe_slot);
  }
  return st;
}

auto
StartPreCalibrationFlagging::apply_to_no_ts(ExtendedState&& st) const
  -> ExtendedState {
  if (st.is_alive()) {
    auto maybe_slot = st.find_subarray(name());
    if (!maybe_slot)
      throw InvalidSubarrayName(name());
    st = std::move(st).set_pre_calibration_flagging(*maybe_slot, true);
  }
  return st;
}

auto
StopPreCalibrationFlagging::apply_to_no_ts(ExtendedState&& st) const
  -> ExtendedState {
  if (st.is_alive()) {
    auto maybe_slot = st.find_subarray(name());
    if (!maybe_slot)
      throw InvalidSubarrayName(name());
    st = std::move(st).set_pre_calibration_flagging(*maybe_slot, false);
  }
  return st;
}

} // end namespace rcp::symcam::st

auto
rcp::symcam::st::to_json(nlohmann::json& js, CreateSubarray const& evt)
  -> void {
  js = {
    {JSONValue::meta_tag_name, CreateSubarray::class_tag},
    {JSONValue::meta_value_name,
     {{JSONCreateSubarray::timestamp_tag, to_string(evt.timestamp())},
      {JSONCreateSubarray::subarray_tag, evt.name().data()},
      {JSONCreateSubarray::slot_tag, evt.slot()},
      {JSONCreateSubarray::pairs_tag,
       std::visit(
         [](auto&& pairs) -> nlohmann::json { return pairs; },
         evt.subarray())}}}};
}

auto
rcp::symcam::from_json(nlohmann::json const& js, ReceiverSet& receivers)
  -> void {
  std::set<ReceiverSet::receiver_t> rcvs;
  for (auto&& jsval : js) {
    if (jsval.is_number_integer()) {
      rcvs.insert(jsval.get<ReceiverSet::receiver_t>());
    } else {
      assert(jsval.is_string());
      assert(jsval.get<std::string>() == "*");
      std::ranges::copy(
        ReceiverSet::all().receivers(), std::inserter(rcvs, rcvs.end()));
    }
  }
  receivers = ReceiverSet{rcvs};
}

auto
rcp::symcam::to_json(nlohmann::json& js, ReceiverSet const& receivers) -> void {
  js = receivers.receivers();
}

auto
rcp::symcam::from_json(nlohmann::json const& js, ReceiverSetOuterProduct& outer)
  -> void {
  ReceiverSet left;
  ReceiverSet right;
  from_json(st::JSONReceiversOuterProduct::get_left(js), left);
  from_json(st::JSONReceiversOuterProduct::get_right(js), right);
  outer = ReceiverSetOuterProduct{left, right};
}

auto
rcp::symcam::to_json(nlohmann::json& js, ReceiverSetOuterProduct const& outer)
  -> void {
  js = {
    {st::JSONReceiversOuterProduct::left_tag, outer.left()},
    {st::JSONReceiversOuterProduct::right_tag, outer.right()}};
}

auto
rcp::symcam::from_json(nlohmann::json const& js, ReceiverPairs& pairs) -> void {
  ReceiverPairs::set_t pairset;
  for (auto&& jsval : js)
    pairset.insert(
      {jsval.at(0).get<ReceiverSet::receiver_t>(),
       jsval.at(1).get<ReceiverSet::receiver_t>()});
  pairs = ReceiverPairs{pairset};
}

auto
rcp::symcam::to_json(nlohmann::json& js, ReceiverPairs const& pairs) -> void {
  js = pairs.pairs();
}

auto
rcp::symcam::st::from_json(nlohmann::json const& js, CreateSubarray& evt)
  -> void {
  if (!JSONValue::is_valid(js))
    throw JSONValueFormatError(js);

  auto tag = JSONValue::get_tag(js);
  if (tag != std::string_view(CreateSubarray::class_tag))
    throw BadEventTagError(CreateSubarray::class_tag, tag);

  auto jsval = JSONValue::get_value(js);
  if (!JSONCreateSubarray::is_valid(jsval))
    throw JSONValueFormatError(js);

  auto const timestamp_str =
    JSONCreateSubarray::get_timestamp(jsval).get<std::string>();
  auto const maybe_timestamp =
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    parse_timestamp(reinterpret_cast<char8_t const*>(timestamp_str.c_str()));
  if (!maybe_timestamp)
    throw TimestampParseError(timestamp_str.c_str());
  evt.timestamp() = *maybe_timestamp;

  auto const subarray_str =
    JSONCreateSubarray::get_subarray(jsval).get<std::string>();
  if (subarray_str.size() >= subarray_name_t{}.size())
    throw SubarrayNameLengthError(subarray_str);
  std::ranges::fill(evt.name(), '\0');
  std::ranges::copy(subarray_str, evt.name().data());

  evt.slot() = JSONCreateSubarray::get_slot(jsval);
  auto const pairs_js = JSONCreateSubarray::get_pairs(jsval);
  if (JSONReceiversOuterProduct::is_valid(pairs_js)) {
    ReceiverSetOuterProduct outer;
    from_json(pairs_js, outer);
    evt.subarray() = outer;
  } else if (JSONReceiverPairs::is_valid(pairs_js)) {
    ReceiverPairs pairs;
    from_json(pairs_js, pairs);
    evt.subarray() = pairs;
  } else {
    assert(false);
  }
}

auto
rcp::symcam::st::from_json(
  nlohmann::json const& js, StartRecordEventStream& evt) -> void {
  if (!JSONValue::is_valid(js))
    throw JSONValueFormatError(js);

  auto tag = JSONValue::get_tag(js);
  if (tag != std::string_view(StartRecordEventStream::class_tag))
    throw BadEventTagError(StartRecordEventStream::class_tag, tag);

  auto jsval = JSONValue::get_value(js);
  if (!JSONSubarrayEvent::is_valid(jsval))
    throw JSONValueFormatError(js);

  auto const timestamp_str =
    JSONSubarrayEvent::get_timestamp(jsval).get<std::string>();
  auto const maybe_timestamp =
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    parse_timestamp(reinterpret_cast<char8_t const*>(timestamp_str.c_str()));
  if (!maybe_timestamp)
    throw TimestampParseError(timestamp_str.c_str());
  evt.timestamp() = *maybe_timestamp;

  auto const subarray_str =
    JSONSubarrayEvent::get_subarray(jsval).get<std::string>();
  if (subarray_str.size() >= subarray_name_t{}.size())
    throw SubarrayNameLengthError(subarray_str);
  std::ranges::fill(evt.name(), '\0');
  std::ranges::copy(subarray_str, evt.name().data());

  if (!JSONStartRecordEventStream::is_valid(jsval))
    throw JSONValueFormatError(js);
  auto filepath =
    JSONStartRecordEventStream::get_filepath(jsval).get<std::string>();
  evt.filepath() = filepath;
}

auto
rcp::symcam::st::to_json(nlohmann::json& js, StartRecordEventStream const& evt)
  -> void {
  js = {
    {JSONValue::meta_tag_name, StartRecordEventStream::class_tag},
    {JSONValue::meta_value_name,
     {{JSONSubarrayEvent::timestamp_tag, to_string(evt.timestamp())},
      {JSONSubarrayEvent::subarray_tag, evt.name().data()},
      {JSONStartRecordEventStream::filepath_tag, evt.filepath()}}}};
}

auto
rcp::symcam::st::from_json(nlohmann::json const& js, StartRecordDataStream& evt)
  -> void {
  if (!JSONValue::is_valid(js))
    throw JSONValueFormatError(js);

  auto tag = JSONValue::get_tag(js);
  if (tag != std::string_view(StartRecordDataStream::class_tag))
    throw BadEventTagError(StartRecordDataStream::class_tag, tag);

  auto jsval = JSONValue::get_value(js);
  if (!JSONSubarrayEvent::is_valid(jsval))
    throw JSONValueFormatError(js);

  auto const timestamp_str =
    JSONSubarrayEvent::get_timestamp(jsval).get<std::string>();
  auto const maybe_timestamp =
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    parse_timestamp(reinterpret_cast<char8_t const*>(timestamp_str.c_str()));
  if (!maybe_timestamp)
    throw TimestampParseError(timestamp_str.c_str());
  evt.timestamp() = *maybe_timestamp;

  auto const subarray_str =
    JSONSubarrayEvent::get_subarray(jsval).get<std::string>();
  if (subarray_str.size() >= subarray_name_t{}.size())
    throw SubarrayNameLengthError(subarray_str);
  std::ranges::fill(evt.name(), '\0');
  std::ranges::copy(subarray_str, evt.name().data());

  if (!JSONStartRecordDataStream::is_valid(jsval))
    throw JSONValueFormatError(js);
  auto filepath =
    JSONStartRecordDataStream::get_filepath(jsval).get<std::string>();
  evt.filepath() = filepath;
  auto maybe_channels = JSONStartRecordDataStream::parse_channels(
    JSONStartRecordDataStream::get_channels(jsval));
  assert(maybe_channels);
  // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
  evt.channels() = maybe_channels.value();
  if (JSONStartRecordDataStream::has_parameters(jsval)) {
    auto maybe_parameters = JSONStartRecordDataStream::parse_parameters(
      JSONStartRecordDataStream::get_parameters(jsval));
    assert(maybe_parameters);
    // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
    evt.parameters() = maybe_parameters.value();
  }
}

auto
rcp::symcam::st::to_json(nlohmann::json& js, StartRecordDataStream const& evt)
  -> void {
  auto channel_set = evt.channels();
  js = {
    {JSONValue::meta_tag_name, StartRecordDataStream::class_tag},
    {JSONValue::meta_value_name,
     {{JSONSubarrayEvent::timestamp_tag, to_string(evt.timestamp())},
      {JSONSubarrayEvent::subarray_tag, evt.name().data()},
      {JSONStartRecordDataStream::filepath_tag, evt.filepath()},
      {JSONStartRecordDataStream::channels_tag, channel_set.channels()},
      {JSONStartRecordDataStream::parameters_tag, evt.parameters()}}}};
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
