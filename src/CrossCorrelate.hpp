// Copyright 2022 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BsetChannelBaselinePolprodRegion.hpp"
#include "Configuration.hpp"
#include "libsymcam.hpp"
#include <rcp/FSamplesRegion.hpp>

#include <optional>

#include <vector>

namespace rcp::symcam {

/*! launcher for task to do cross correlation on region of (unflagged) samples
 *
 * By "unflagged" we mean that all flag values in the samples region have been
 * converted to zero prior to launching this task.
 */
class CrossCorrelate {
public:
  using samples_t = FSamplesRegion;

  using cbp_t = BsetChannelBaselinePolprodRegion;

private:
  samples_t::index_partition_t m_samples_index_partition;

  cbp_t::index_partition_t m_visibilities_index_partition;

  L::IndexTaskLauncher m_launcher;

public:
  /*! non-default constructor
   *
   * \param samples_index_space index space of region with sample values
   *        (samples_md_t::sample_fid)
   * \param visibilities_index_space index space of region with unnormalized
   *        visibilities (visibilities_md_t::visibility_fid)
   */
  CrossCorrelate(
    L::Context ctx,
    L::Runtime* rt,
    samples_t::index_space_t samples_index_space,
    cbp_t::index_space_t visibilities_index_space);

  CrossCorrelate() = default;

  CrossCorrelate(CrossCorrelate const&) = default;

  CrossCorrelate(CrossCorrelate&&) = default;

  ~CrossCorrelate() = default;

  auto
  operator=(CrossCorrelate const&) -> CrossCorrelate& = default;

  auto
  operator=(CrossCorrelate&&) -> CrossCorrelate& = default;

  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    samples_t::LogicalRegion samples,
    cbp_t::LogicalRegion visibilities,
    L::Predicate pred = L::Predicate::TRUE_PRED) -> void;

  static auto
  preregister_tasks(L::TaskID task_id) -> void;

  static auto
  register_tasks(L::Runtime* rt, L::TaskID task_id) -> void;
};
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
