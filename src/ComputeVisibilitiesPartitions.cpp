// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "ComputeVisibilitiesPartitions.hpp"
#include "BsetChannelBaselinePolprodRegion.hpp"
#include "DataPartitionsRegion.hpp"
#include "EventProcessing.hpp"
#include "GridTileRegion.hpp"
#include "Serializable.hpp"
#include <default_mapper.h>
#include <rcp/dc/rcpdc.hpp>
#include <rcp/rcp.hpp>

namespace rcp::symcam {

inline auto
grid_tile_mapping_none_value() -> GridTileMappingField::value_t {
  std::array<GridTileRegion::coord_t, GridTileRegion::dim> result{};
  std::ranges::fill(result, -1);
  return GridTileMappingField::value_t{result.data()};
}

/*! partition visibility channel axis on preimage partition of grid channels */
template <typename T>
auto
grid_channel_preimage(
  L::Context ctx,
  L::Runtime* rt,
  typename T::index_space_t domain_is,
  unsigned image_channel_offset,
  std::vector<std::array<rcp::dc::channel_type, 2>> const&
    im_vis_channel_blocks) -> typename T::index_partition_t {

  L::Rect<T::dim, coord_t> domain_bounds =
    rt->get_index_space_domain(domain_is).bounds;
  std::map<L::Point<1, coord_t>, typename T::domain_t> ch_domains;
  for (auto&& vblk : im_vis_channel_blocks) {
    typename T::rect_t rect = domain_bounds;
    rect.lo[dim(T::axes_t::channel)] = vblk[0];
    rect.hi[dim(T::axes_t::channel)] = vblk[1];
    ch_domains
      [std::distance(im_vis_channel_blocks.data(), &vblk)
       + image_channel_offset] = typename T::domain_t{rect};
  }
  auto cs = rt->create_index_space(
    ctx,
    L::Rect<1, coord_t>{
      image_channel_offset,
      static_cast<coord_t>(image_channel_offset + im_vis_channel_blocks.size())
        - 1});
  return rt->create_partition_by_domain(ctx, domain_is, ch_domains, cs, true);
}

/*! partition an index space with a visibility channel axis on (grid channel,
 *  visibility channel block)
 *
 * result may be a sparse partition
 */
template <typename T>
auto
partition_by_grid_and_vis_channels(
  L::Context ctx,
  L::Runtime* rt,
  typename T::index_space_t domain_is,
  typename T::index_space_t parent_is,
  unsigned image_channel_offset,
  std::vector<std::array<rcp::dc::channel_type, 2>> const&
    im_vis_channel_blocks) -> typename T::index_partition_t {

  auto vch_ip = T::create_partition_by_slicing(
    ctx,
    rt,
    parent_is,
    {{T::axes_t::channel, DataPacketConfiguration::num_channels_per_packet}});
  auto vch_cs = rt->get_index_partition_color_space<
                    T::dim,
                    typename T::coord_t,
                    1,
                    typename T::coord_t>(vch_ip)
                  .bounds;
  auto gch_ip = grid_channel_preimage<T>(
    ctx, rt, parent_is, image_channel_offset, im_vis_channel_blocks);
  auto gch_cs = rt->get_index_partition_color_space<
                    T::dim,
                    typename T::coord_t,
                    1,
                    typename T::coord_t>(gch_ip)
                  .bounds;
  // (grid channel, visibility channel block) color space bounds
  L::Rect<2, typename T::coord_t> cs2_bounds{
    {gch_cs.lo[0], vch_cs.lo[0]}, {gch_cs.hi[0], vch_cs.hi[0]}};
  // the partition may be sparse, first find which intersections are non-empty
  std::vector<L::Point<2, coord_t>> cs2_points;
  for (L::PointInRectIterator pir(cs2_bounds); pir(); pir++) {
    std::vector<typename T::index_space_t> handles{domain_is};
    handles.push_back(
      rt->get_index_subspace(gch_ip, L::Point<1, coord_t>{pir[0]}));
    handles.push_back(
      rt->get_index_subspace(vch_ip, L::Point<1, coord_t>{pir[1]}));
    auto intersection = rt->intersect_index_spaces(ctx, handles);
    if (rt->get_index_space_domain(intersection).volume() > 0)
      cs2_points.push_back(*pir);
    rt->destroy_index_space(ctx, intersection);
  }
  // create the pending partition
  auto cs2 = rt->create_index_space(ctx, cs2_points);
  typename T::index_partition_t result =
    rt->create_pending_partition(ctx, parent_is, cs2);
  // fill the pending partition with the intersecting sub-spaces of the grid and
  // visibility channel block partitions
  L::DomainT<2, coord_t> cs2_domain = rt->get_index_space_domain(cs2);
  for (L::PointInDomainIterator pid(cs2_domain); pid(); pid++) {
    std::vector<typename T::index_space_t> handles{domain_is};
    handles.push_back(
      rt->get_index_subspace(gch_ip, L::Point<1, coord_t>{pid[0]}));
    handles.push_back(
      rt->get_index_subspace(vch_ip, L::Point<1, coord_t>{pid[1]}));
    rt->create_index_space_intersection(ctx, result, *pid, handles);
  }
  // clean up
  rt->destroy_index_space(
    ctx, rt->get_index_partition_color_space_name(gch_ip));
  rt->destroy_index_space(
    ctx, rt->get_index_partition_color_space_name(vch_ip));
  return result;
}

/*! /brief add another dimension to an index partition of an index space with
 *  visibility channel axis by blocking the visibility channel axis
 *
 * result may be a sparse partition
 */
template <typename T, int ColorDim>
auto
append_vis_channel_partition(
  L::Context ctx, L::Runtime* rt, typename T::index_partition_t domain_ip) ->
  typename T::index_partition_t {

  auto domain_is = rt->get_parent_index_space(domain_ip);
  auto vch_ip = T::create_partition_by_slicing(
    ctx,
    rt,
    domain_is,
    {{T::axes_t::channel, DataPacketConfiguration::num_channels_per_packet}});
  auto vch_cs = rt->template get_index_partition_color_space<
                    T::dim,
                    typename T::coord_t,
                    1,
                    typename T::coord_t>(vch_ip)
                  .bounds;
  auto domain_cs = rt->get_index_partition_color_space<
                       T::dim,
                       typename T::coord_t,
                       ColorDim,
                       coord_t>(domain_ip)
                     .bounds;

  // color space bounds
  L::Rect<ColorDim + 1, coord_t> cs_bounds;
  for (auto&& color_idx : std::views::iota(0, ColorDim)) {
    cs_bounds.lo[color_idx] = domain_cs.lo[color_idx];
    cs_bounds.hi[color_idx] = domain_cs.hi[color_idx];
  }
  cs_bounds.lo[ColorDim] = vch_cs.lo[0];
  cs_bounds.hi[ColorDim] = vch_cs.hi[0];
  // the partition may be sparse, first find which intersections are non-empty
  std::vector<L::Point<ColorDim + 1, coord_t>> cs_points;
  for (L::PointInRectIterator pir(cs_bounds); pir(); pir++) {
    std::vector<typename T::index_space_t> handles;
    L::Point<ColorDim, coord_t> ptC;
    for (auto&& color_idx : std::views::iota(0, ColorDim))
      ptC[color_idx] = pir[color_idx];
    if (rt->has_index_subspace(domain_ip, ptC)) {
      handles.push_back(rt->get_index_subspace(domain_ip, ptC));
      handles.push_back(
        rt->get_index_subspace(vch_ip, L::Point<1, coord_t>{pir[ColorDim]}));
      auto intersection = rt->intersect_index_spaces(ctx, handles);
      if (rt->get_index_space_domain(intersection).volume() > 0)
        cs_points.push_back(*pir);
      rt->destroy_index_space(ctx, intersection);
    }
  }
  // create the pending partition
  if (!cs_points.empty()) {
    auto cs = rt->create_index_space(ctx, cs_points);
    typename T::index_partition_t result =
      rt->create_pending_partition(ctx, domain_is, cs);
    // fill the pending partition with the intersecting sub-spaces of the
    // initial partition and visibility channel block partitions
    L::DomainT<ColorDim + 1, coord_t> cs_domain =
      rt->get_index_space_domain(cs);
    for (L::PointInDomainIterator pid(cs_domain); pid(); pid++) {
      std::vector<typename T::index_space_t> handles;
      L::Point<ColorDim, coord_t> ptC;
      for (auto&& color_idx : std::views::iota(0, ColorDim))
        ptC[color_idx] = pid[color_idx];
      handles.push_back(rt->get_index_subspace(domain_ip, ptC));
      handles.push_back(
        rt->get_index_subspace(vch_ip, L::Point<1, coord_t>{pid[ColorDim]}));
      rt->create_index_space_intersection(ctx, result, *pid, handles);
    }
    return result;
  }
  return typename T::index_partition_t();
}

/*! \brief task to fill the uvw index space projection field */
struct FillUVWProjectionFieldTask
  : public DefaultKokkosTaskMixin<FillUVWProjectionFieldTask, void> {

  /*! \brief task name */
  static constexpr char const* task_name = "FillUVWProjectionField";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  enum { uvwpr_region_index = 0, num_regions };

  using bc_t = BaselineChannelRegion;
  using cbp_t = BsetChannelBaselinePolprodRegion;

#ifdef RCP_USE_KOKKOS
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* /*task*/,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    assert(regions.size() == num_regions);
    auto const& points_region = regions[uvwpr_region_index];
    auto points_bounds = cbp_t::rect_t{points_region};
    auto points = cbp_t::view<execution_space, LEGION_WRITE_ONLY>(
      points_region, BaselineChannelProjectionField{});

    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);
    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();

    K::parallel_for(
      task_name,
      mdrange_policy<
        execution_space,
        cbp_t,
        cbp_t::axes_t::baseline,
        cbp_t::axes_t::channel,
        cbp_t::axes_t::polarization_product>(work_space, points_bounds),
      KOKKOS_LAMBDA(
        cbp_t::coord_t const bl,
        cbp_t::coord_t const ch,
        cbp_t::coord_t const pp) {
        auto& pt = points(ch, bl, pp);
        pt[dim(bc_t::axes_t::baseline)] = bl;
        pt[dim(bc_t::axes_t::channel)] = ch;
      });
  }
#else  // !RCP_USE_KOKKOS
  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    auto const& points_region = regions[uvwpr_region_index];
    auto points_bounds = cbp_t::rect_t{points_region};
    auto points = cbp_t::values<LEGION_WRITE_ONLY>(
      points_region, TimeBaselineChannelProjectionField{});

    for (auto pir = L::PointInRectIterator<cbp_t::dim>(points_region); pir();
         pir++) {
      auto& pt = points[*pir];
      pt[dim(bc_t::axes_t::baseline)] = pir[dim(cbp_t::axes_t::baseline)];
      pt[dim(bc_t::axes_t::channel)] = pir[dim(cbp_t::axes_t::channel)];
    }
  }
#endif // RCP_USE_KOKKOS

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID FillUVWProjectionFieldTask::task_id;

class ComputeVisibilitiesPartitionBySubarrayTask
  : public SerialOnlyTaskMixin<
      ComputeVisibilitiesPartitionBySubarrayTask,
      void> {
public:
  /*! \brief task name */
  static constexpr const char* task_name =
    "ComputeVisibilitiesPartitionBySubarray";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  using bc_t = BaselineChannelRegion;
  using state_t = st::StateRegion;
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using part_t = DataPartitionsRegion;

  enum {
    state_region_index = 0,
    projection_region_index,
    partitions_region_index,
    num_regions
  };

  struct Args {
    /*! \brief visibilities index space */
    bc_t::index_space_t uvws_index_space;
  };

  template <typename Region>
  static auto
  compute_partition_by_subarray(
    L::Context ctx,
    L::Runtime* rt,
    L::IndexPartitionT<1, int> const& baselines_partition,
    typename Region::index_space_t const& index_space)
    -> Region::index_partition_t {

    auto fs = rt->create_field_space(ctx);
    auto fa = rt->create_field_allocator(ctx, fs);
    constexpr L::FieldID baseline_field_id = 5;
    fa.allocate_field(sizeof(int), baseline_field_id);
    auto baselines_lr = rt->create_logical_region(ctx, index_space, fs);
    L::RegionRequirement req(
      baselines_lr, LEGION_WRITE_ONLY, LEGION_EXCLUSIVE, baselines_lr);
    req.add_field(baseline_field_id);
    auto pr = rt->map_region(ctx, req);
    auto baselines = L::FieldAccessor<
      LEGION_WRITE_ONLY,
      int,
      Region::dim,
      typename Region::coord_t,
      L::AffineAccessor<int, Region::dim, typename Region::coord_t>>(
      pr, baseline_field_id);
    for (auto pir = L::PointInRectIterator<Region::dim>(pr); pir(); pir++)
      baselines[*pir] = static_cast<int>(pir[dim(Region::axes_t::baseline)]);
    rt->unmap_region(ctx, pr);
    auto result =
      typename Region::index_partition_t{rt->create_partition_by_preimage(
        ctx,
        baselines_partition,
        baselines_lr,
        baselines_lr,
        baseline_field_id,
        rt->get_index_partition_color_space_name(baselines_partition))};
    // rt->destroy_field_space(ctx, fs);
    return result;
  }

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& args = *reinterpret_cast<Args const*>(task->args);

    // get partition of baselines by subarray from state value
    auto const& state_region = regions[state_region_index];
    auto const state = state_t::values<LEGION_READ_ONLY>(
      state_region, st::StateField{})[state_t::rect_t{state_region}
                                        .lo[dim(state_t::axes_t::shard)]];

    auto const baselines_partition = state.subarray_partition();
    auto vis_partitions = part_t::values<LEGION_WRITE_ONLY>(
      regions[partitions_region_index], VisibilitiesSubarrayPartitionField{});
    auto uvw_partitions = part_t::values<LEGION_WRITE_ONLY>(
      regions[partitions_region_index], UVWsSubarrayPartitionField{});
    auto uvw_image_partitions = part_t::values<LEGION_WRITE_ONLY>(
      regions[partitions_region_index], UVWsImagePartitionField{});

    VisibilitiesSubarrayPartitionField::value_t vis_subarray_partition{};
    UVWsSubarrayPartitionField::value_t uvw_subarray_partition{};
    if (baselines_partition != L::IndexPartitionT<1, int>{}) {
      // partition the uvws index space by subarray
      uvw_subarray_partition = compute_partition_by_subarray<bc_t>(
        ctx, rt, baselines_partition, args.uvws_index_space);
      // projections region has cbp_t shape
      auto proj_lr = RegionPair<cbp_t::LogicalRegion>{
        task->regions[projection_region_index]};
      // partition the projections region by subarray using a preimage of the
      // mapping from visibilities index space to uvws index space
      vis_subarray_partition = rt->create_partition_by_preimage<
        cbp_t::dim,
        cbp_t::coord_t,
        bc_t::dim,
        bc_t::coord_t>(
        ctx,
        uvw_subarray_partition,
        proj_lr.region(),
        proj_lr.parent(),
        BaselineChannelProjectionField::field_id,
        // TODO: variable for coord type
        L::IndexSpaceT<1, int>{
          rt->get_index_partition_color_space_name(baselines_partition)});
      assert(uvw_subarray_partition.exists());
      assert(vis_subarray_partition.exists());
    }
    // copy the partitions to every element in the appropriate partition region
    // TODO: write sub-regions to per-subarray values
    for (auto&& slot : subarray_slots()) {
      vis_partitions[static_cast<int>(slot)] = vis_subarray_partition;
      uvw_partitions[static_cast<int>(slot)] = uvw_subarray_partition;
      uvw_image_partitions[static_cast<int>(slot)] =
        UVWsImagePartitionField::value_t{};
    }
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID ComputeVisibilitiesPartitionBySubarrayTask::task_id;

class ComputeTileMappingsTask
  : public DefaultKokkosTaskMixin<ComputeTileMappingsTask, void> {
public:
  /*! \brief task name */
  static constexpr const char* task_name = "ComputeTileMappings";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  using bc_t = BaselineChannelRegion;
  using tiles_t = GridTileRegion;
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using grid_t = GridRegion;
  using part_t = DataPartitionsRegion;

  enum {
    uvws_region_index = 0,
    uvw_tile_mapping_region_index,
    tile_bounds_region_index,
    num_regions
  };

  struct Args {
    bool has_universal_tile;
    unsigned x_padding;
    unsigned y_padding;
    Configuration::frequency_t cell_width_arcsec;
    Configuration::frequency_t cell_height_arcsec;
    Configuration::frequency_t w_depth_arcsec;
  };

  template <typename T>
  static RCP_INLINE_FUNCTION auto
  uvw_to_xyz(
    T cell_width, T cell_height, T w_depth, std::array<T, 3> const& uvw)
    -> std::array<coord_t, 3> {
    return {
      std::lrint(uvw[0] * cell_width),
      std::lrint(uvw[1] * cell_height),
      std::lrint(std::floor(std::abs(uvw[2] * w_depth)))};
  }

#ifdef RCP_USE_KOKKOS
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    assert(regions.size() == num_regions);
    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);
    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();
    assert(task->arglen == sizeof(Args));

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& args = *reinterpret_cast<Args const*>(task->args);
    auto const& uvws_region = regions[uvws_region_index];
    auto const& uvw_tile_mapping_region =
      regions[uvw_tile_mapping_region_index];
    auto const& tile_bounds_region = regions[tile_bounds_region_index];

    auto const uvws =
      bc_t::view<execution_space, LEGION_READ_ONLY>(uvws_region, UVWField{});
    auto const tile_bounds = tiles_t::view<execution_space, LEGION_READ_ONLY>(
      tile_bounds_region, GridTileField{});

    // index task launch space is (grid channel, vis channel)
    auto const grid_channel = task->index_point[0];

    if (!args.has_universal_tile) {
      // iterate over the index space of uvw_tile_mapping_region, not
      // uvws_region (uvws_region is an upper bound, but it can include points
      // for which we don't have privilege to write, so we should skip them)
      for (auto piece = L::PieceIteratorT<bc_t::dim, bc_t::coord_t>(
             uvw_tile_mapping_region, GridTileMappingField::field_id, true);
           piece();
           ++piece) {
        log().debug(std::string("uvw rect ") + rcp::show(*piece));
        auto const uvw_tile_mappings =
          bc_t::view<execution_space, LEGION_READ_WRITE>(
            uvw_tile_mapping_region, GridTileMappingField{}, *piece);
        K::parallel_for(
          task_name,
          mdrange_policy<
            execution_space,
            bc_t,
            bc_t::axes_t::baseline,
            bc_t::axes_t::channel>(work_space, *piece),
          KOKKOS_LAMBDA(bc_t::coord_t const bl, bc_t::coord_t const ch) {
            // convert uvw to grid coordinates plus a w-plane index
            auto const pt = uvw_to_xyz(
              args.cell_width_arcsec,
              args.cell_height_arcsec,
              args.w_depth_arcsec,
              uvws(bl, ch));
            // Iterate over all tiles to find the match. Unfortunately, we
            // can't restrict the iteration to a subspace of tiles_domain, so
            // we have to iterate over all tiles
            for (auto&& uv_idx : dim_range<tiles_t>(
                   tiles_t::axes_t::uv, tiles_t::rect_t{tile_bounds_region})) {
              auto const tb = tile_bounds(grid_channel, pt[2], uv_idx);
              if (
                tb.lo[tiles_t::bound_x] + args.x_padding <= pt[0]
                && pt[0] <= tb.hi[tiles_t::bound_x] - args.x_padding
                && tb.lo[tiles_t::bound_y] + args.y_padding <= pt[1]
                && pt[1] <= tb.hi[tiles_t::bound_y] - args.y_padding)
                uvw_tile_mappings(bl, ch) =
                  GridTileMappingField::value_t(grid_channel, pt[2], uv_idx);
            }
          });
      }
    } else {
      for (auto piece = L::PieceIteratorT<bc_t::dim, bc_t::coord_t>(
             uvw_tile_mapping_region, GridTileMappingField::field_id, true);
           piece();
           ++piece) {
        log().debug(std::string("uvw rect ") + rcp::show(*piece));
        auto const uvw_tile_mappings =
          bc_t::view<execution_space, LEGION_READ_WRITE>(
            uvw_tile_mapping_region, GridTileMappingField{}, *piece);
        K::parallel_for(
          task_name,
          mdrange_policy<
            execution_space,
            bc_t,
            bc_t::axes_t::baseline,
            bc_t::axes_t::channel>(work_space, *piece),
          KOKKOS_LAMBDA(bc_t::coord_t const bl, bc_t::coord_t const ch) {
            uvw_tile_mappings(bl, ch) =
              GridTileMappingField::value_t(grid_channel, 0, 0);
          });
      }
    }
  }
#endif

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(Args));

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& args = *reinterpret_cast<Args const*>(task->args);
    auto const& uvws_region = regions[uvws_region_index];
    auto const& uvw_tile_mapping_region =
      regions[uvw_tile_mapping_region_index];
    auto const& tile_bounds_region = regions[tile_bounds_region_index];

    auto const uvws = bc_t::values<LEGION_READ_ONLY>(uvws_region, UVWField{});
    auto const tile_bounds =
      tiles_t::values<LEGION_READ_ONLY>(tile_bounds_region, GridTileField{});

    // index task launch space is (grid channel, vis channel)
    auto const grid_channel = task->index_point[0];

    if (!args.has_universal_tile) {
      // iterate over the index space of uvw_tile_mapping_region, not
      // uvws_region (uvws_region is an upper bound, but it can include points
      // for which we don't have privilege to write, so we should skip them)
      for (auto piece = L::PieceIteratorT<bc_t::dim, bc_t::coord_t>(
             uvw_tile_mapping_region, GridTileMappingField::field_id, true);
           piece();
           ++piece) {
        log().debug(std::string("uvw rect ") + rcp::show(*piece));
        auto const uvw_tile_mappings = bc_t::values<LEGION_READ_WRITE>(
          uvw_tile_mapping_region, GridTileMappingField{}, *piece);
        for (auto&& bl : range(*piece, bc_t::axes_t::baseline))
          for (auto&& ch : range(*piece, bc_t::axes_t::channel)) {
            // convert uvw to grid coordinates plus a w-plane index
            auto const pt = uvw_to_xyz(
              args.cell_width_arcsec,
              args.cell_height_arcsec,
              args.w_depth_arcsec,
              uvws[{bl, ch}]);
            // Iterate over all tiles to find the match. Unfortunately, we
            // can't restrict the iteration to a subspace of tiles_domain, so
            // we have to iterate over all tiles
            for (auto&& uv_idx : dim_range<tiles_t>(
                   tiles_t::axes_t::uv, tiles_t::rect_t{tile_bounds_region})) {
              auto const tb = tile_bounds[{grid_channel, pt[2], uv_idx}];
              if (
                tb.lo[tiles_t::bound_x] + args.x_padding <= pt[0]
                && pt[0] <= tb.hi[tiles_t::bound_x] - args.x_padding
                && tb.lo[tiles_t::bound_y] + args.y_padding <= pt[1]
                && pt[1] <= tb.hi[tiles_t::bound_y] - args.y_padding)
                uvw_tile_mappings[{bl, ch}] =
                  GridTileMappingField::value_t(grid_channel, pt[2], uv_idx);
            }
          }
      }
    } else {
      for (auto piece = L::PieceIteratorT<bc_t::dim, bc_t::coord_t>(
             uvw_tile_mapping_region, GridTileMappingField::field_id, true);
           piece();
           ++piece) {
        log().debug(std::string("uvw rect ") + rcp::show(*piece));
        auto const uvw_tile_mappings = bc_t::values<LEGION_READ_WRITE>(
          uvw_tile_mapping_region, GridTileMappingField{}, *piece);
        for (auto&& bl : range(*piece, bc_t::axes_t::baseline))
          for (auto&& ch : range(*piece, bc_t::axes_t::channel)) {
            uvw_tile_mappings[{bl, ch}] =
              GridTileMappingField::value_t(grid_channel, 0, 0);
          }
      }
    }
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;

    // GridChannelProjection::projection_id =
    //   L::Runtime::generate_static_projection_id();
    // L::Runtime::preregister_projection_functor(
    //   GridChannelProjection::projection_id, new GridChannelProjection());

    registrar.set_leaf();
    auto layout = uvws_layout().constraint_id(traits::layout_style).value();
    registrar.add_layout_constraint_set(uvws_region_index, layout)
      .add_layout_constraint_set(uvw_tile_mapping_region_index, layout);
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;

    // GridChannelProjection::projection_id =
    //   L::Runtime::generate_static_projection_id();
    // L::Runtime::preregister_projection_functor(
    //   GridChannelProjection::projection_id, new GridChannelProjection());

    registrar.set_leaf();
    auto layout = uvws_layout().constraint_id(rt, traits::layout_style).value();
    registrar.add_layout_constraint_set(uvws_region_index, layout)
      .add_layout_constraint_set(uvw_tile_mapping_region_index, layout);
    rt->register_task_variant<body<Variant>>(registrar);
  }
};
// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID ComputeTileMappingsTask::task_id;

class ComputeVisibilitiesPartitionByTileTask
  : public SerialOnlyTaskMixin<ComputeVisibilitiesPartitionByTileTask, void> {
public:
  /*! \brief task name */
  static constexpr const char* task_name = "ComputeVisibilitiesPartitionByTile";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  using bc_t = BaselineChannelRegion;
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using grid_t = GridRegion;
  using part_t = DataPartitionsRegion;
  using tiles_t = GridTileRegion;

  enum {
    vis_tile_partition_region_index = 0,
    subarray_partitions_region_index,
    uvws_region_index,
    tile_bounds_region_index,
    projections_region_index,
    uvw_image_partition_region_index,
    uvw_tile_partition_region_index,
    num_regions
  };

  // I don't want the default initializer to initialize members, in order to
  // help me catch errors where members are uninitialized upon use of the
  // constructor.
  // NOLINTBEGIN(cppcoreguidelines-pro-type-member-init,
  // misc-non-private-member-variables-in-classes)
  struct Args {
    bool use_imaging_start_future;
    bool do_imaging_start;
    bool has_universal_tile;
    unsigned image_channel_offset;
    tiles_t::index_space_t grid_tiles_color_space;
    std::vector<std::array<rcp::dc::channel_type, 2>> im_vis_channel_blocks;
    unsigned x_padding;
    unsigned y_padding;
    Configuration::frequency_t cell_width_arcsec;
    Configuration::frequency_t cell_height_arcsec;
    Configuration::frequency_t w_depth_arcsec;
    // NOLINTEND(cppcoreguidelines-pro-type-member-init,
    // misc-non-private-member-variables-in-classes)
    [[nodiscard]] auto
    serialized_size() const -> std::size_t {
      return sizeof(use_imaging_start_future) + sizeof(do_imaging_start)
             + sizeof(has_universal_tile) + sizeof(image_channel_offset)
             + sizeof(grid_tiles_color_space)
             + rcp::serialized_size(im_vis_channel_blocks) + sizeof(x_padding)
             + sizeof(y_padding) + sizeof(cell_width_arcsec)
             + sizeof(cell_height_arcsec) + sizeof(w_depth_arcsec);
    }
    // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,
    // cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto
    serialize(void* buffer) const -> std::size_t {
      char* bch = reinterpret_cast<char*>(buffer);
      bch += rcp::serialize(use_imaging_start_future, bch);
      bch += rcp::serialize(do_imaging_start, bch);
      bch += rcp::serialize(has_universal_tile, bch);
      bch += rcp::serialize(image_channel_offset, bch);
      bch += rcp::serialize(grid_tiles_color_space, bch);
      bch += rcp::serialize(im_vis_channel_blocks, bch);
      bch += rcp::serialize(x_padding, bch);
      bch += rcp::serialize(y_padding, bch);
      bch += rcp::serialize(cell_width_arcsec, bch);
      bch += rcp::serialize(cell_height_arcsec, bch);
      bch += rcp::serialize(w_depth_arcsec, bch);
      return bch - reinterpret_cast<char*>(buffer);
    }
    auto
    deserialize(void const* buffer) -> std::size_t {
      char const* bch = reinterpret_cast<char const*>(buffer);
      bch += rcp::deserialize(use_imaging_start_future, bch);
      bch += rcp::deserialize(do_imaging_start, bch);
      bch += rcp::deserialize(has_universal_tile, bch);
      bch += rcp::deserialize(image_channel_offset, bch);
      bch += rcp::deserialize(grid_tiles_color_space, bch);
      bch += rcp::deserialize(im_vis_channel_blocks, bch);
      bch += rcp::deserialize(x_padding, bch);
      bch += rcp::deserialize(y_padding, bch);
      bch += rcp::deserialize(cell_width_arcsec, bch);
      bch += rcp::deserialize(cell_height_arcsec, bch);
      bch += rcp::deserialize(w_depth_arcsec, bch);
      return bch - reinterpret_cast<char const*>(buffer);
    }
    // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,
    // cppcoreguidelines-pro-bounds-pointer-arithmetic)
  };

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    Args args;
    args.deserialize(task->args);
    assert(!args.use_imaging_start_future || task->futures.size() == 1);
    auto is_imaging_start = args.use_imaging_start_future
                              ? task->futures[0].get_result<bool>()
                              : args.do_imaging_start;

    // One caution about the implementation of this method. It is very important
    // to compute the partitions here using the parent index spaces of the uvw
    // and visibility regions, in order that downstream tasks can apply them
    // properly. Although the regions provided to the launcher are typically
    // sub-regions, we need to compute partitions on the parents of those
    // regions. TODO: There may be some inefficiency here, which we should
    // investigate.

    static_assert(part_t::dim == 1);
    auto subarray = part_t::rect_t{regions[vis_tile_partition_region_index]}.lo;

    auto vis_tile_partition_region = RegionPair<part_t::LogicalRegion>{
      task->regions[vis_tile_partition_region_index]};
    auto uvws_region =
      RegionPair<bc_t::LogicalRegion>{task->regions[uvws_region_index]};
    auto tile_bounds_region = RegionPair<tiles_t::LogicalRegion>{
      task->regions[tile_bounds_region_index]};
    auto projections_region =
      RegionPair<cbp_t::LogicalRegion>{task->regions[projections_region_index]};

    auto& uvw_image_partition = part_t::values<LEGION_READ_WRITE>(
      regions[uvw_image_partition_region_index],
      UVWsImagePartitionField{})[subarray];
    auto& uvw_tile_partition = part_t::values<LEGION_READ_WRITE>(
      regions[uvw_tile_partition_region_index],
      UVWsTilePartitionField{})[subarray];

    // where to write the visibilities tile partition
    auto& vis_tile_partition = part_t::values<LEGION_READ_WRITE>(
      regions[vis_tile_partition_region_index],
      VisibilitiesTilePartitionField{})[subarray];

    // work only on a uvw subregion for the current subarray
    auto const uvw_subarray_ip = part_t::values<LEGION_READ_ONLY>(
      regions[subarray_partitions_region_index],
      UVWsSubarrayPartitionField{})[subarray];
    auto const uvw_subarray_lp = rt->get_logical_partition(
      bc_t::logical_region_t{uvws_region.region()}, uvw_subarray_ip);
    auto const uvw_subarray_region = RegionPair{
      bc_t::LogicalRegion{
        rt->get_logical_subregion_by_color(uvw_subarray_lp, subarray)},
      uvws_region.parent()};

    // if uvw_subarray_region is empty, then the visibilities partition is
    // too, and we have an early return
    if (bc_t::logical_region_t{} == uvw_subarray_region.region()) {
      vis_tile_partition = VisibilitiesTilePartitionField::value_t{};
      uvw_tile_partition = UVWsTilePartitionField::value_t{};
      return;
    }

    // fast path: when grids have a single tile, and we're not starting a new
    // image, the visibilities partition doesn't change
    if (args.has_universal_tile && !is_imaging_start)
      return;

    // use projections_region and VisibilitiesSubarrayPartitionField of
    // regions[subarray_partitions_region_index] to get visibilities subspace
    auto const vis_subarray_ip = part_t::values<LEGION_READ_ONLY>(
      regions[subarray_partitions_region_index],
      VisibilitiesSubarrayPartitionField{})[subarray];
    assert(vis_subarray_ip.exists());
    auto const vis_subarray_region = get_visibilities_subarray_subregion(
      rt, projections_region, vis_subarray_ip, subarray);

    // Create a region for the tile mapping field. Make sure we create a region
    // with the same index space as uvws_region, because the partitions of the
    // tile mapping regions will be applied to uvws_region by other tasks.
    auto uvws_region2 = rt->create_logical_region(
      ctx,
      uvws_region.region().get_index_space(),
      uvws_region.region().get_field_space());
    auto const uvw_subarray_lp2 = rt->get_logical_partition(
      bc_t::logical_region_t{uvws_region2}, uvw_subarray_ip);
    auto const uvw_tile_mapping_region = RegionPair{
      bc_t::LogicalRegion{
        rt->get_logical_subregion_by_color(uvw_subarray_lp2, subarray)},
      bc_t::LogicalRegion{uvws_region2}};
    // fill tile mapping field with "none" values using a fill operation
    uvw_tile_mapping_region.region().fill_fields(
      ctx,
      rt,
      uvw_tile_mapping_region.parent(),
      std::nullopt,
      std::make_tuple(GridTileMappingField{}, grid_tile_mapping_none_value()));

    // the following two regions will be accessed in sub-tasks, so we unmap
    // them here
    rt->unmap_region(ctx, regions[uvws_region_index]);
    rt->unmap_region(ctx, regions[tile_bounds_region_index]);

    // launch a task to compute the tile mappings
    {
      // only when an image is started, we need to recompute the
      // uvw_image_partition value
      if (is_imaging_start) {
        uvw_image_partition = partition_by_grid_and_vis_channels<bc_t>(
          ctx,
          rt,
          uvw_tile_mapping_region.region().get_index_space(),
          uvw_tile_mapping_region.parent().get_index_space(),
          args.image_channel_offset,
          args.im_vis_channel_blocks);
        assert(rt->is_index_partition_disjoint(uvw_image_partition));
      }

      ComputeTileMappingsTask::Args const mapping_args{
        .has_universal_tile = args.has_universal_tile,
        .x_padding = args.x_padding,
        .y_padding = args.y_padding,
        .cell_width_arcsec = args.cell_width_arcsec,
        .cell_height_arcsec = args.cell_height_arcsec,
        .w_depth_arcsec = args.w_depth_arcsec};
      auto uvw_tile_mapping_lp =
        bc_t::LogicalPartition{rt->get_logical_partition(
          bc_t::logical_region_t{uvw_tile_mapping_region.region()},
          uvw_image_partition)};
      auto launcher = L::IndexTaskLauncher(
        ComputeTileMappingsTask::task_id,
        rt->get_index_partition_color_space(uvw_image_partition),
        L::UntypedBuffer(&mapping_args, sizeof(mapping_args)),
        L::ArgumentMap());

      launcher.region_requirements.resize(ComputeTileMappingsTask::num_regions);
      launcher.region_requirements
        [ComputeTileMappingsTask::uvw_tile_mapping_region_index] =
        uvw_tile_mapping_lp.requirement(
          0,
          LEGION_READ_WRITE,
          LEGION_EXCLUSIVE,
          uvw_tile_mapping_region.parent(),
          StaticFields{GridTileMappingField{}});
      launcher.region_requirements[ComputeTileMappingsTask::uvws_region_index] =
        uvws_region.requirement(
          LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{UVWField{}});
      launcher.region_requirements
        [ComputeTileMappingsTask::tile_bounds_region_index] =
        tile_bounds_region.requirement(
          LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{GridTileField{}});
      rt->execute_index_space(ctx, launcher);
    }
    // create partition of uvws_subregion index space by using the tile
    // index as the partition color
    auto uvw_tiles_ip = rt->create_partition_by_field(
      ctx,
      bc_t ::logical_region_t{uvw_tile_mapping_region.region()},
      bc_t::logical_region_t{uvw_tile_mapping_region.parent()},
      GridTileMappingField::field_id,
      args.grid_tiles_color_space);

    // add visibility channel block index to partition
    uvw_tile_partition =
      append_vis_channel_partition<bc_t, tiles_t::dim>(ctx, rt, uvw_tiles_ip);

    // the preimage of the mapping from vis_subarray_region index space to
    // uvw_tile partition by BaselineChannelProjectionField creates the
    // partition on the index space of vis_subarray_region
    vis_tile_partition = rt->create_partition_by_preimage(
      ctx,
      uvw_tile_partition,
      cbp_t::logical_region_t{vis_subarray_region.parent()},
      cbp_t::logical_region_t{vis_subarray_region.parent()},
      BaselineChannelProjectionField::field_id,
      rt->get_index_partition_color_space_name<
        bc_t::dim,
        bc_t::coord_t,
        tiles_t::dim + 1,
        tiles_t::coord_t>(uvw_tile_partition));
    rt->destroy_logical_region(ctx, uvws_region2);
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID ComputeVisibilitiesPartitionByTileTask::task_id;

ComputeVisibilitiesPartitions::ComputeVisibilitiesPartitions(
  L::Context /*ctx*/, L::Runtime* /*rt*/, Serialized const& serialized) noexcept
  : m_uvw_projection_region(serialized.m_uvw_projection_region)
  , m_partitions_by_subarray(serialized.m_partitions_by_subarray) {}

ComputeVisibilitiesPartitions::ComputeVisibilitiesPartitions(
  L::Context ctx,
  L::Runtime* rt,
  cbp_t::LogicalRegion const& uvw_projection_region)
  : m_uvw_projection_region(uvw_projection_region) {

  auto part_fs = part_t::field_space(ctx, rt);
  assert(part_t::index_space(ctx, rt));
  // NOLINTBEGIN(bugprone-unchecked-optional-access)
  auto part_is = part_t::index_space(ctx, rt).value();
  // NOLINTEND(bugprone-unchecked-optional-access)
  auto partitions =
    part_t::LogicalRegion{rt->create_logical_region(ctx, part_is, part_fs)};
  partitions.fill_fields(
    ctx,
    rt,
    std::tuple{
      VisibilitiesSubarrayPartitionField{},
      VisibilitiesSubarrayPartitionField::value_t{}},
    std::tuple{
      VisibilitiesTilePartitionField{},
      VisibilitiesTilePartitionField::value_t{}},
    std::tuple{
      UVWsSubarrayPartitionField{}, UVWsSubarrayPartitionField::value_t{}},
    std::tuple{UVWsImagePartitionField{}, UVWsImagePartitionField::value_t{}},
    std::tuple{UVWsTilePartitionField{}, UVWsTilePartitionField::value_t{}});
  auto partitions_ip = part_t::create_partition_by_slicing(
    ctx, rt, part_is, {{part_t::axes_t::subarray, 1}});
  auto partitions_lp = part_t::LogicalPartition{rt->get_logical_partition(
    part_t::logical_region_t{partitions}, partitions_ip)};
  m_partitions_by_subarray = {partitions_lp, partitions};

  auto launcher =
    L::TaskLauncher(FillUVWProjectionFieldTask::task_id, L::UntypedBuffer());
  launcher.region_requirements.resize(FillUVWProjectionFieldTask::num_regions);
  launcher.region_requirements[FillUVWProjectionFieldTask::uvwpr_region_index] =
    m_uvw_projection_region.requirement(
      LEGION_WRITE_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{BaselineChannelProjectionField{}});
  rt->execute_task(ctx, launcher);
}

auto
ComputeVisibilitiesPartitions::destroy(L::Context ctx, L::Runtime* rt) -> void {
  auto partitions = m_partitions_by_subarray.parent(rt);
  rt->destroy_index_space(ctx, partitions.get_index_space());
  rt->destroy_field_space(ctx, partitions.get_field_space());
  rt->destroy_logical_region(ctx, partitions);
}

auto
ComputeVisibilitiesPartitions::partitions_by_subarray() const noexcept
  -> PartitionPair<part_t::LogicalPartition> {
  return m_partitions_by_subarray;
}

auto
ComputeVisibilitiesPartitions::serialized(
  L::Context /*ctx*/, L::Runtime* /*rt*/) const noexcept -> Serialized {
  return {m_uvw_projection_region, m_partitions_by_subarray};
}

auto
ComputeVisibilitiesPartitions::child_requirements(
  L::Context /*ctx*/, L::Runtime* rt) const -> ChildRequirements {

  return {
    {m_uvw_projection_region.requirement(
       LEGION_READ_ONLY,
       LEGION_EXCLUSIVE,
       StaticFields{BaselineChannelProjectionField{}},
       std::nullopt,
       std::nullopt,
       L::Mapping::DefaultMapper::VIRTUAL_MAP),
     m_partitions_by_subarray.parent(rt).requirement(
       LEGION_READ_WRITE,
       LEGION_EXCLUSIVE,
       StaticFields{
         VisibilitiesSubarrayPartitionField{},
         UVWsSubarrayPartitionField{},
         UVWsImagePartitionField{},
         VisibilitiesTilePartitionField{},
         UVWsTilePartitionField{}},
       std::nullopt,
       std::nullopt,
       L::Mapping::DefaultMapper::VIRTUAL_MAP)},
    {},
    {}};
}

auto
ComputeVisibilitiesPartitions::init_update_by_subarray_launcher(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<state_t::LogicalRegion> const& state,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  L::UntypedBuffer arg) -> L::TaskLauncher {

  assert(
    arg.get_size() == sizeof(ComputeVisibilitiesPartitionBySubarrayTask::Args));
  reinterpret_cast<ComputeVisibilitiesPartitionBySubarrayTask::Args*>(
    arg.get_ptr())
    ->uvws_index_space = uvws.region().get_index_space();

  auto result =
    L::TaskLauncher(ComputeVisibilitiesPartitionBySubarrayTask::task_id, arg);
  result.region_requirements.resize(
    ComputeVisibilitiesPartitionBySubarrayTask::num_regions);
  result.region_requirements
    [ComputeVisibilitiesPartitionBySubarrayTask::state_region_index] =
    st::State0Select(ctx, rt, state)(state).requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}});
  result.region_requirements
    [ComputeVisibilitiesPartitionBySubarrayTask::projection_region_index] =
    m_uvw_projection_region.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{BaselineChannelProjectionField{}},
      std::nullopt,
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);
  result.region_requirements
    [ComputeVisibilitiesPartitionBySubarrayTask::partitions_region_index] =
    m_partitions_by_subarray.parent(rt).requirement(
      LEGION_WRITE_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{
        VisibilitiesSubarrayPartitionField{},
        UVWsSubarrayPartitionField{},
        UVWsImagePartitionField{}});
  return result;
}

auto
ComputeVisibilitiesPartitions::update_by_subarray(
  L::Context ctx,
  L::Runtime* rt,
  std::array<RegionPair<state_t::LogicalRegion>, 3> const& stream_states,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  st::subarray_predicates_array_t const& /*predicates*/) -> void {

  auto is_subarray_change = st::EventProcessing::is_subarray_change(
    ctx, rt, stream_states[0], stream_states[1]);

  ComputeVisibilitiesPartitionBySubarrayTask::Args arg;
  auto launcher = init_update_by_subarray_launcher(
    ctx, rt, stream_states[1], uvws, L::UntypedBuffer(&arg, sizeof(arg)));
  launcher.predicate = is_subarray_change;
  rt->execute_task(ctx, launcher);
}

auto
ComputeVisibilitiesPartitions::update_by_subarray(
  L::Context ctx,
  L::Runtime* rt,
  std::array<RegionPair<state_t::LogicalRegion>, 3> const& stream_states,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  st::AllSubarraysQuery const& state_query) -> void {

  if (!state_query.do_subarray_change())
    return;

  ComputeVisibilitiesPartitionBySubarrayTask::Args arg;
  auto launcher = init_update_by_subarray_launcher(
    ctx, rt, stream_states[1], uvws, L::UntypedBuffer(&arg, sizeof(arg)));
  rt->execute_task(ctx, launcher);
}

auto
ComputeVisibilitiesPartitions::init_update_by_tile_launcher(
  ImagingConfiguration const& img,
  RegionPair<cbp_t::LogicalRegion> const& visibilities,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  bool use_imaging_start_future,
  std::vector<char>& argbuf) const -> L::TaskLauncher {

  ComputeVisibilitiesPartitionByTileTask::Args const args{
    .use_imaging_start_future = use_imaging_start_future,
    .do_imaging_start = false,
    .has_universal_tile = true,
    .image_channel_offset = img.im.image_channel_offset,
    .grid_tiles_color_space = {},
    .im_vis_channel_blocks = img.im.visibility_channel_blocks(),
    .x_padding = (img.gr.test.cf_size + 1) / 2,
    .y_padding = (img.gr.test.cf_size + 1) / 2,
    .cell_width_arcsec = img.im.cell_width_arcsec,
    .cell_height_arcsec = img.im.cell_height_arcsec,
    .w_depth_arcsec = img.im.w_depth_arcsec};
  argbuf.resize(args.serialized_size());
  args.serialize(argbuf.data());

  auto result = L::TaskLauncher(
    ComputeVisibilitiesPartitionByTileTask::task_id,
    L::UntypedBuffer(argbuf.data(), argbuf.size()));
  if (use_imaging_start_future)
    result.futures.resize(1);
  result.region_requirements.resize(
    ComputeVisibilitiesPartitionByTileTask::num_regions);

  // be careful with uvw and visibility regions here: because they will be
  // partitioned on parent index spaces, we need to include requirements on
  // the parent spaces
  result.region_requirements
    [ComputeVisibilitiesPartitionByTileTask::uvws_region_index] =
    uvws.requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{UVWField{}});

  result.region_requirements
    [ComputeVisibilitiesPartitionByTileTask::projections_region_index] =
    visibilities.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{BaselineChannelProjectionField{}},
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);

  return result;
}

auto
ComputeVisibilitiesPartitions::update_update_by_tile_launcher(
  L::Context ctx,
  L::Runtime* rt,
  sarray<tiles_t::LogicalRegion> const& tile_bounds,
  sarray<grid_t::index_partition_t> const& grid_partitions,
  unsigned subarray_slot,
  L::TaskLauncher& launcher,
  std::vector<char>& argbuf,
  bool do_imaging_start) const -> void {

  assert(tiles_t::has_params(rt, tile_bounds.at(subarray_slot)));
  tiles_t::Params params =
    tiles_t::retrieve_params(rt, tile_bounds.at(subarray_slot));
  ComputeVisibilitiesPartitionByTileTask::Args args;
  args.deserialize(argbuf.data());
  args.do_imaging_start = do_imaging_start;
  args.has_universal_tile = params.universal_uvw_tiles;
  args.grid_tiles_color_space = rt->get_index_partition_color_space_name<
    grid_t::dim,
    grid_t::coord_t,
    tiles_t::dim,
    tiles_t::coord_t>(grid_partitions.at(subarray_slot));
  argbuf.resize(args.serialized_size());
  args.serialize(argbuf.data());
  launcher.argument = L::UntypedBuffer(argbuf.data(), argbuf.size());

  auto partitions_subregion = get_partitions_region_for_subarray(
    rt, m_partitions_by_subarray.partition(), subarray_slot);
  launcher.region_requirements
    [ComputeVisibilitiesPartitionByTileTask::vis_tile_partition_region_index] =
    partitions_subregion.requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{VisibilitiesTilePartitionField{}});

  launcher.region_requirements
    [ComputeVisibilitiesPartitionByTileTask::subarray_partitions_region_index] =
    partitions_subregion.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{
        UVWsSubarrayPartitionField{}, VisibilitiesSubarrayPartitionField{}});

  launcher.region_requirements
    [ComputeVisibilitiesPartitionByTileTask::uvw_image_partition_region_index] =
    partitions_subregion.requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{UVWsImagePartitionField{}});

  launcher.region_requirements
    [ComputeVisibilitiesPartitionByTileTask::uvw_tile_partition_region_index] =
    partitions_subregion.requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{UVWsTilePartitionField{}});

  launcher.region_requirements
    [ComputeVisibilitiesPartitionByTileTask::tile_bounds_region_index] =
    tile_bounds.at(subarray_slot)
      .requirement(
        LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{GridTileField{}});
}

auto
ComputeVisibilitiesPartitions::update_by_tile(
  L::Context ctx,
  L::Runtime* rt,
  ImagingConfiguration const& img,
  sarray<tiles_t::LogicalRegion> const& tile_bounds,
  sarray<grid_t::index_partition_t> const& grid_partitions,
  RegionPair<cbp_t::LogicalRegion> const& visibilities,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  st::subarray_predicates_array_t const& predicates) const -> void {

  auto imaging_start_futures = predicate_slice_subarray_futures(
    ctx, rt, predicates, st::SubarrayStreamPredicate::is_imaging_start);
  auto is_imaging = predicate_slice_subarray_predicates(
    predicates, st::SubarrayStreamPredicate::is_imaging);

  std::vector<char> argbuf;
  auto launcher =
    init_update_by_tile_launcher(img, visibilities, uvws, true, argbuf);

  for (auto&& subarray_slot : subarray_slots()) {
    update_update_by_tile_launcher(
      ctx, rt, tile_bounds, grid_partitions, subarray_slot, launcher, argbuf);
    launcher.predicate = is_imaging(subarray_slot);
    launcher.futures[0] = imaging_start_futures.at(subarray_slot);
    rt->execute_task(ctx, launcher);
  }
}

auto
ComputeVisibilitiesPartitions::update_by_tile(
  L::Context ctx,
  L::Runtime* rt,
  ImagingConfiguration const& img,
  sarray<tiles_t::LogicalRegion> const& tile_bounds,
  sarray<grid_t::index_partition_t> const& grid_partitions,
  RegionPair<cbp_t::LogicalRegion> const& visibilities,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  st::AllSubarraysQuery const& state_query) const -> void {

  auto do_imaging_start = state_query.do_imaging_start();
  auto do_imaging = state_query.do_imaging();

  std::vector<char> argbuf;
  auto launcher =
    init_update_by_tile_launcher(img, visibilities, uvws, false, argbuf);

  for (auto&& subarray_slot : filtered_subarray_slots(do_imaging))
    if (do_imaging.at(subarray_slot)) {
      update_update_by_tile_launcher(
        ctx,
        rt,
        tile_bounds,
        grid_partitions,
        subarray_slot,
        launcher,
        argbuf,
        do_imaging_start.at(subarray_slot));
      rt->execute_task(ctx, launcher);
    }
}

auto
ComputeVisibilitiesPartitions::preregister_tasks(
  std::array<L::TaskID, 4> const& task_ids) -> void {

  PortableTask<ComputeVisibilitiesPartitionBySubarrayTask>::
    preregister_task_variants(task_ids[0]);
  PortableTask<ComputeVisibilitiesPartitionByTileTask>::
    preregister_task_variants(task_ids[1]);
  PortableTask<ComputeTileMappingsTask>::preregister_task_variants(task_ids[2]);
  PortableTask<FillUVWProjectionFieldTask>::preregister_task_variants(
    task_ids[3]);
}

auto
ComputeVisibilitiesPartitions::register_tasks(
  L::Runtime* rt, std::array<L::TaskID, 4> const& task_ids) -> void {

  PortableTask<ComputeVisibilitiesPartitionBySubarrayTask>::
    register_task_variants(rt, task_ids[0]);
  PortableTask<ComputeVisibilitiesPartitionByTileTask>::register_task_variants(
    rt, task_ids[1]);
  PortableTask<ComputeTileMappingsTask>::register_task_variants(
    rt, task_ids[2]);
  PortableTask<FillUVWProjectionFieldTask>::register_task_variants(
    rt, task_ids[3]);
}

} // namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
