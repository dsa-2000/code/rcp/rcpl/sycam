// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "FFTWSupport.hpp"
#include "EventProcessing.hpp"
#include "StateRegion.hpp"

#include <rcp/Mapper.hpp>
#include <rcp/rcp.hpp>

using namespace rcp::symcam;

namespace rcp::symcam {

/*! \brief task to import fftw wisdom
 */
struct ImportFFTWWisdomTask
  : public SerialOnlyTaskMixin<ImportFFTWWisdomTask, bool> {

  using state_t = st::StateRegion;

  /*! \brief task name */
  static constexpr const char* task_name = "ImportFFTWWisdom";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief region indexes */
  enum { state_region_index = 0, proxy_region_index, num_regions };

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> result_t {

    assert(regions.size() == num_regions);

    auto pt = state_t::rect_t{regions[state_region_index]}.lo;
    auto const state = state_t::values<LEGION_READ_ONLY>(
      regions[state_region_index], st::StateField{})[pt];
    bool imp = state.fftw_wisdom_import_path()[0] != '\0';
    if (imp)
      state.do_fftw_wisdom_import(true);
    return imp;
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<result_t, body<Variant>>(
      registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<result_t, body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID ImportFFTWWisdomTask::task_id;

/*! \brief task to export fftw wisdom
 */
struct ExportFFTWWisdomTask
  : public SerialOnlyTaskMixin<ExportFFTWWisdomTask, bool> {

  using state_t = st::StateRegion;

  /*! \brief task name */
  static constexpr const char* task_name = "ExportFFTWWisdom";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief region indexes */
  enum { state_region_index = 0, proxy_region_index, num_regions };

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> result_t {

    assert(regions.size() == num_regions);

    auto pt = state_t::rect_t{regions[state_region_index]}.lo;
    auto const state = state_t::values<LEGION_READ_ONLY>(
      regions[state_region_index], st::StateField{})[pt];
    bool exp = state.fftw_wisdom_export_path()[0] != '\0';
    if (exp)
      state.do_fftw_wisdom_export(task->index_point == task->index_domain.lo());
    return exp;
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<result_t, body<Variant>>(
      registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<result_t, body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID ExportFFTWWisdomTask::task_id;

FFTWSupport::FFTWSupport(
  L::Context ctx,
  L::Runtime* rt,
  std::size_t shard_point,
  std::size_t num_shards)
  : m_proxy(std::make_unique<decltype(m_proxy)::element_type>()) {

  using fftw_t = FFTWSupportRegion;

  // create a region for proxy instance pointers
  {
    auto maybe_is = fftw_t::index_space(
      ctx,
      rt,
      {{fftw_t::axes_t::shard,
        fftw_t::range_t::right_bounded(static_cast<int>(num_shards - 1))}});
    assert(maybe_is);
    // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
    auto is = maybe_is.value();
    auto fs = fftw_t::field_space(ctx, rt);
    m_support_lr =
      fftw_t::LogicalRegion{rt->create_logical_region(ctx, is, fs)};
    m_support_lr.fill_fields(
      ctx, rt, std::make_tuple(FFTWProxyField{}, FFTWProxyField::value_t{}));
    auto ip = rt->create_equal_partition(ctx, is, is);
    m_support_lp =
      fftw_t::LogicalPartition{rt->get_logical_partition(m_support_lr, ip)};
  }

  // Initialize FFTWSupportRegion instances.
  auto subregion =
    rt->get_logical_subregion_by_color(m_support_lp, shard_point);
  {
    auto ext = Realm::ExternalMemoryResource(m_proxy.get(), sizeof(*m_proxy));
    auto launcher =
      L::IndexAttachLauncher(LEGION_EXTERNAL_INSTANCE, m_support_lr, true);
    launcher.initialize_constraints(false, false, {FFTWProxyField::field_id});
    launcher.add_external_resource(subregion, &ext);
    launcher.privilege_fields.insert(FFTWProxyField::field_id);
    m_ext_resources = rt->attach_external_resources(ctx, launcher);
  }
}

auto
FFTWSupport::destroy(L::Context ctx, L::Runtime* rt) -> void {
  auto detached = rt->detach_external_resources(ctx, m_ext_resources);
  rt->destroy_index_space(ctx, m_support_lr.get_index_space());
  rt->destroy_field_space(ctx, m_support_lr.get_field_space());
  rt->destroy_logical_region(ctx, m_support_lr);
  detached.wait();
}

auto
FFTWSupport::import_wisdom(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<state_t::LogicalRegion> const& state,
  L::Predicate pred) -> L::Future {

  auto launcher = L::IndexTaskLauncher(
    ImportFFTWWisdomTask::task_id,
    m_support_lr.get_index_space(),
    L::UntypedBuffer(),
    L::ArgumentMap(),
    pred,
    false,
    0,
    Mapper::level0_sharding_tag);
  launcher.region_requirements.resize(ImportFFTWWisdomTask::num_regions);
  launcher.region_requirements[ImportFFTWWisdomTask::state_region_index] =
    st::State0Select(ctx, rt, state)(state).requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}});
  launcher.region_requirements[ImportFFTWWisdomTask::proxy_region_index] =
    m_support_lp.requirement(
      0,
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      m_support_lr,
      StaticFields{FFTWProxyField{}});
  launcher.set_predicate_false_future(L::Future::from_value(false));
  return rt->execute_index_space(ctx, launcher, LEGION_REDOP_AND_BOOL);
}

auto
FFTWSupport::export_wisdom(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<state_t::LogicalRegion> const& state,
  L::Predicate pred) -> L::Future {

  auto launcher = L::IndexTaskLauncher(
    ExportFFTWWisdomTask::task_id,
    m_support_lr.get_index_space(),
    L::UntypedBuffer(),
    L::ArgumentMap(),
    pred,
    false,
    0,
    Mapper::level0_sharding_tag);
  launcher.region_requirements.resize(ExportFFTWWisdomTask::num_regions);
  launcher.region_requirements[ExportFFTWWisdomTask::state_region_index] =
    st::State0Select(ctx, rt, state)(state).requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}});
  launcher.region_requirements[ExportFFTWWisdomTask::proxy_region_index] =
    m_support_lp.requirement(
      0,
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      m_support_lr,
      StaticFields{FFTWProxyField{}});
  launcher.set_predicate_false_future(L::Future::from_value(false));
  return rt->execute_index_space(ctx, launcher, LEGION_REDOP_AND_BOOL);
}

auto
FFTWSupport::preregister_tasks(std::array<L::TaskID, 2> const& task_ids)
  -> void {
  PortableTask<ImportFFTWWisdomTask>::preregister_task_variants(task_ids[0]);
  PortableTask<ExportFFTWWisdomTask>::preregister_task_variants(task_ids[1]);
}

auto
FFTWSupport::register_tasks(
  L::Runtime* rt, std::array<L::TaskID, 2> const& task_ids) -> void {
  PortableTask<ImportFFTWWisdomTask>::register_task_variants(rt, task_ids[0]);
  PortableTask<ExportFFTWWisdomTask>::register_task_variants(rt, task_ids[1]);
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
