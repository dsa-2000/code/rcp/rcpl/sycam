// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "ComputeUVW.hpp"
#include "EventProcessing.hpp"
#include "StateRegion.hpp"
#include <rcp/rcp.hpp>

namespace rcp::symcam {

/*! task to compute UVW coordinates */
struct ComputeUVWTask : public DefaultKokkosTaskMixin<ComputeUVWTask, void> {

  using bl_t = BaselineRegion;
  using bc_t = BaselineChannelRegion;
  using state_t = st::StateRegion;

  /*! \brief task name */
  static constexpr char const* task_name = "ComputeUVW";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief region indexes */
  enum {
    baselines_region_index = 0,
    uvws_region_index,
    state_region_begin_index,
    state_region_end_index,
    num_regions
  };

  static RCP_INLINE_FUNCTION auto
  rotate_baselines(
    BaselineField::value_t const& baselines,
    st::State::rotation_t const& rotation) -> UVWField::value_t {
    return {
      (baselines[0] * rotation[0 * 3 + 0] + baselines[1] * rotation[0 * 3 + 1]
       + baselines[2] * rotation[0 * 3 + 2]),
      (baselines[0] * rotation[1 * 3 + 0] + baselines[1] * rotation[1 * 3 + 1]
       + baselines[2] * rotation[1 * 3 + 2]),
      (baselines[0] * rotation[2 * 3 + 0] + baselines[1] * rotation[2 * 3 + 1]
       + baselines[2] * rotation[2 * 3 + 2])};
  }
#ifdef RCP_USE_KOKKOS
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(ComputeUVW::Args));

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& args = *reinterpret_cast<ComputeUVW::Args const*>(task->args);

    auto const baselines = bl_t::view<execution_space, LEGION_READ_ONLY>(
      regions[baselines_region_index], BaselineField{});
    auto const& uvws_region = regions[uvws_region_index];
    bc_t::rect_t bounds = uvws_region;
    auto const uvws =
      bc_t::view<execution_space, LEGION_WRITE_ONLY>(uvws_region, UVWField{});

    auto const state0 = state_t::values<LEGION_READ_ONLY>(
      regions[state_region_begin_index],
      st::StateField{})[state_t::rect_t{regions[state_region_begin_index]}
                          .lo[dim(state_t::axes_t::shard)]];
    auto const state1 = state_t::values<LEGION_READ_ONLY>(
      regions[state_region_end_index],
      st::StateField{})[state_t::rect_t{regions[state_region_end_index]}
                          .lo[dim(state_t::axes_t::shard)]];
    auto rotation = state0.field_center();
    {
      auto rot1 = state1.field_center();
      for (auto&& idx : std::views::iota(0U, rotation.size()))
        rotation[idx] = (rotation[idx] + rot1[idx]) / 2;
    }

    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();
    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);

    auto const num_baselines = bounds.hi[dim(bc_t::axes_t::baseline)]
                               - bounds.lo[dim(bc_t::axes_t::baseline)] + 1;
    using team_handle = typename TeamPolicy<execution_space>::member_type;
    K::parallel_for(
      task_name,
      TeamPolicy<execution_space>(work_space, num_baselines, K::AUTO),
      KOKKOS_LAMBDA(team_handle const& team) {
        auto const bl =
          team.league_rank() + bounds.lo[dim(bc_t::axes_t::baseline)];
        auto const rbl = rotate_baselines(baselines(bl), rotation);
        K::parallel_for(
          K::TeamThreadRange(
            team,
            bounds.lo[dim(bc_t::axes_t::channel)],
            bounds.hi[dim(bc_t::axes_t::channel)] + 1),
          [&](bc_t::coord_t const ch) {
            auto m_to_c = meters_to_cycles(
              args.sample_channel0_frequency_hz
              + ch * args.sample_channel_width_hz);
            uvws(bl, ch) =
              UVWField::value_t{m_to_c(rbl[0]), m_to_c(rbl[1]), m_to_c(rbl[2])};
          });
      });
  }
#else  // !RCP_USE_KOKKOS
  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> void {

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(ComputeUVW::Args));

    ComputeUVW::Args const& args =
      *reinterpret_cast<ComputeUVW::Args const*>(task->args);

    auto& baselines_region = regions[baselines_region_index];
    auto const baselines =
      bl_t::values<LEGION_READ_ONLY>(baselines_region, BaselineField{});

    auto& uvws_region = regions[uvws_region_index];
    bc_t::rect_t bounds = uvws_region;
    auto const uvws = bc_t::values<LEGION_WRITE_ONLY>(uvws_region, UVWField{});

    auto const state0 = state_t::values<LEGION_READ_ONLY>(
      regions[state_region_begin_index], st::StateField{})[0];
    auto const state1 = state_t::values<LEGION_READ_ONLY>(
      regions[state_region_end_index], st::StateField{})[0];
    auto rotation = state0.field_center();
    {
      auto rot1 = state1.field_center();
      for (auto&& idx : std::views::iota(0U, rotation.size()))
        rotation[idx] = (rotation[idx] + rot1[idx]) / 2;
    }

    auto wavelength = [&](auto const& ch) -> Configuration::
                                            frequency_t {
                                              return
          constants::c
          / (args.sample_channel0_frequency_hz
             + ch * args.sample_channel_width_hz);
                                            };

    for (auto pir = L::PointInRectIterator<bc_t::dim>(uvws_region); pir();
         pir++) {
      auto const& bl = pir[dim(bc_t::axes_t::baseline)];
      auto const& ch = pir[dim(bc_t::axes_t::channel)];
      auto val = rotate_baselines(baselines[bl], rotation)
                 | std::views::transform(meters_to_cycles(
                   args.sample_channel0_frequency_hz
                   + ch * args.sample_channel_width_hz));
      uvws[{bl, ch}] = UVWField::value_t{val[0], val[1], val[2]};
    }
  }
#endif // RCP_USE_KOKKOS

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar.add_layout_constraint_set(
      uvws_region_index,
      uvws_layout().constraint_id(traits::layout_style).value());
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar.add_layout_constraint_set(
      uvws_region_index,
      uvws_layout().constraint_id(rt, traits::layout_style).value());
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID ComputeUVWTask::task_id;

/*! \brief task to fill the visibilities index space projection field */
struct FillVisProjectionFieldTask
  : public DefaultKokkosTaskMixin<FillVisProjectionFieldTask, void> {

  /*! \brief task name */
  static constexpr char const* task_name = "FillVisProjectionField";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  enum { vispr_region_index = 0, num_regions };

  using bc_t = BaselineChannelRegion;
  using cbp_t = BsetChannelBaselinePolprodRegion;

  using Args = cbp_t::index_space_t;

#ifdef RCP_USE_KOKKOS
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    auto const& args = *reinterpret_cast<Args const*>(task->args);
    auto const& vis_bounds = rt->get_index_space_domain(args).bounds;

    assert(regions.size() == num_regions);
    auto const& rects_region = regions[vispr_region_index];
    auto rects_bounds = bc_t::rect_t{rects_region};
    auto rects = bc_t::view<execution_space, LEGION_WRITE_ONLY>(
      rects_region, VisProjectionField{});

    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);
    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();

    K::parallel_for(
      task_name,
      mdrange_policy<
        execution_space,
        bc_t,
        bc_t::axes_t::baseline,
        bc_t::axes_t::channel>(work_space, rects_bounds),
      KOKKOS_LAMBDA(bc_t::coord_t const bl, bc_t::coord_t const ch) {
        auto rect = vis_bounds;
        rect.lo[dim(cbp_t::axes_t::baseline)] =
          rect.hi[dim(cbp_t::axes_t::baseline)] = bl;
        rect.lo[dim(cbp_t::axes_t::channel)] =
          rect.hi[dim(cbp_t::axes_t::channel)] = ch;
        rects(bl, ch) = rect;
      });
  }
#else  // !RCP_USE_KOKKOS
  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    auto const& args = *reinterpret_cast<Args const*>(task->args);
    auto const& vis_bounds = rt->get_index_space_domain(args).bounds;

    assert(regions.size() == num_regions);
    auto const& rects_region = regions[vispr_region_index];
    auto rects =
      bc_t::values<LEGION_WRITE_ONLY>(rects_region, VisProjectionField{});

    for (auto pir = L::PointInRectIterator<bc_t::dim>(rects_region); pir();
         pir++) {
      auto rect = vis_bounds;
      rect.lo[dim(cbp_t::axes_t::baseline)] =
        rect.hi[dim(cbp_t::axes_t::baseline)] =
          pir[dim(bc_t::axes_t::baseline)];
      rect.lo[dim(cbp_t::axes_t::channel)] =
        rect.hi[dim(cbp_t::axes_t::channel)] = pir[dim(bc_t::axes_t::channel)];
      rects[*pir] = rect;
    }
  }
#endif // RCP_USE_KOKKOS

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar.add_layout_constraint_set(
      vispr_region_index,
      uvws_layout().constraint_id(traits::layout_style).value());
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar.add_layout_constraint_set(
      vispr_region_index,
      uvws_layout().constraint_id(rt, traits::layout_style).value());
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID FillVisProjectionFieldTask::task_id;

ComputeUVW::ComputeUVW(
  L::Context ctx, L::Runtime* rt, Configuration const& config)
  : m_uvws_gen(
      ctx,
      rt,
      baseline_channel_dynamic_bounds<bc_t::coord_t>(
        config.sample_channel_range_offset, config.sample_channel_range_size))
  , m_args{
      config.sample_channel0_frequency_hz, config.sample_channel_width_hz} {}

ComputeUVW::ComputeUVW(
  L::Context ctx, L::Runtime* rt, Serialized const& serialized) noexcept
  : m_uvws_gen(ctx, rt, serialized.m_uvws_gen)
  , m_args{
      serialized.m_sample_channel0_frequency_hz,
      serialized.m_sample_channel_width_hz} {}

auto
ComputeUVW::serialized(L::Context ctx, L::Runtime* rt) const noexcept
  -> Serialized {
  return {
    m_uvws_gen.serialized(ctx, rt),
    m_args.sample_channel0_frequency_hz,
    m_args.sample_channel_width_hz};
}

auto
ComputeUVW::child_requirements(L::Context /*ctx*/, L::Runtime* /*rt*/) const
  -> ChildRequirements {
  return {};
}

auto
ComputeUVW::index_space() -> bc_t::index_space_t {
  return m_uvws_gen.index_space();
}

auto
ComputeUVW::destroy(L::Context ctx, L::Runtime* rt) -> void {
  m_uvws_gen.destroy(ctx, rt);
}

auto
ComputeUVW::make(L::Context ctx, L::Runtime* rt) -> bc_t::LogicalRegion {
  return m_uvws_gen.make(ctx, rt);
}

auto
ComputeUVW::launch(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<bl_t::LogicalRegion> const& baselines,
  std::array<RegionPair<state_t::LogicalRegion>, 2> const& stream_states,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  L::Predicate pred) -> void {

  auto uvws_is = uvws.region().get_index_space();

  auto uvws_ip = bc_t::create_partition_by_slicing(
    ctx,
    rt,
    uvws_is,
    {{bc_t::axes_t::channel,
      DataPacketConfiguration::num_channels_per_packet}});
  auto launcher = L::IndexTaskLauncher(
    ComputeUVWTask::task_id,
    rt->get_index_partition_color_space(uvws_ip),
    L::UntypedBuffer(&m_args, sizeof(m_args)),
    L::ArgumentMap());

  launcher.region_requirements.resize(ComputeUVWTask::num_regions);
  launcher.region_requirements[ComputeUVWTask::baselines_region_index] =
    baselines.requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{BaselineField{}});
  auto lp =
    bc_t::LogicalPartition{rt->get_logical_partition(uvws.region(), uvws_ip)};
  launcher.region_requirements[ComputeUVWTask::uvws_region_index] =
    lp.requirement(
      0,
      LEGION_WRITE_ONLY,
      LEGION_EXCLUSIVE,
      uvws.parent(),
      StaticFields{UVWField{}});
  // assume that a single State0Select instance works for both elements of
  // stream_states
  auto state0_select = st::State0Select(ctx, rt, stream_states[0]);
  launcher.region_requirements[ComputeUVWTask::state_region_begin_index] =
    state0_select(stream_states[0])
      .requirement(
        LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}});
  launcher.region_requirements[ComputeUVWTask::state_region_end_index] =
    state0_select(stream_states[1])
      .requirement(
        LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}});
  launcher.predicate = pred;
  rt->execute_index_space(ctx, launcher);
}

auto
ComputeUVW::fill_vis_projection_field(
  L::Context ctx,
  L::Runtime* rt,
  cbp_t::index_space_t vis,
  bc_t::LogicalRegion const& uvws) -> void {

  auto launcher = L::TaskLauncher(
    FillVisProjectionFieldTask::task_id, L::UntypedBuffer(&vis, sizeof(vis)));
  launcher.region_requirements.resize(FillVisProjectionFieldTask::num_regions);
  launcher.region_requirements[FillVisProjectionFieldTask::vispr_region_index] =
    uvws.requirement(
      LEGION_WRITE_ONLY, LEGION_EXCLUSIVE, StaticFields{VisProjectionField{}});
  rt->execute_task(ctx, launcher);
}

auto
ComputeUVW::preregister_tasks(std::array<L::TaskID, 2> const& task_ids)
  -> void {
  PortableTask<ComputeUVWTask>::preregister_task_variants(task_ids[0]);
  PortableTask<FillVisProjectionFieldTask>::preregister_task_variants(
    task_ids[1]);
}

auto
ComputeUVW::register_tasks(
  L::Runtime* rt, std::array<L::TaskID, 2> const& task_ids) -> void {
  PortableTask<ComputeUVWTask>::register_task_variants(rt, task_ids[0]);
  PortableTask<FillVisProjectionFieldTask>::register_task_variants(
    rt, task_ids[1]);
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
