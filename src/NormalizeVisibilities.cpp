// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "NormalizeVisibilities.hpp"
#include "libsymcam.hpp"

#include <rcp/rcp.hpp>
#include <type_traits>
#include <vector>

using namespace rcp::symcam;

namespace rcp::symcam {

/*! task to compute visibility weights
 */
struct NormalizeVisibilitiesTask
  : public DefaultKokkosTaskMixin<NormalizeVisibilitiesTask, void> {

  using cbp_t = BsetChannelBaselinePolprodRegion;

  static constexpr char const* task_name = "NormalizeVisibilities";

  static L::TaskID task_id;

  enum { vis_region_index = 0, normvis_region_index, num_regions };

  /* \brief flag to convert nan-valued outcomes to zero
   *
   * Compile-time switch that determines whether zero-valued flag correlation
   * values (i.e, weights) result in zero-valued normalized values. Setting this
   * to 'true' is likely better for downstream calculations in which
   * visibilities are eventually multiplied by weights -- 0 (vis) * W (wgt) is
   * going to be less disruptive than NaN (vis) * W (wgt), even when W is zero.
   */
  static constexpr auto nan_to_zero = true;

  /*! \brief minimum denominator in normalization */
  static constexpr auto min_norm_denominator = std::conditional_t<
    nan_to_zero,
    std::integral_constant<FlagCorrelationField::value_t, 1>,
    std::integral_constant<FlagCorrelationField::value_t, 0>>::value;

#ifdef RCP_USE_KOKKOS
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(NormalizeVisibilities::Args));

    auto integration_factor =
      reinterpret_cast<NormalizeVisibilities::Args const*>(task->args)
        ->integration_factor;
    auto inverse_integration_factor =
      WeightField::value_t(1.0 / integration_factor);

    auto const& vis_region = regions[vis_region_index];
    cbp_t::rect_t bounds = vis_region;

    auto const vis = cbp_t::view<execution_space, LEGION_READ_ONLY>(
      vis_region, SampleCorrelationField{});
    auto const wgt = cbp_t::view<execution_space, LEGION_READ_ONLY>(
      vis_region, FlagCorrelationField{});

    auto const normvis = cbp_t::view<execution_space, LEGION_WRITE_ONLY>(
      regions[normvis_region_index], VisibilityField{});
    auto const normwgt = cbp_t::view<execution_space, LEGION_WRITE_ONLY>(
      regions[normvis_region_index], WeightField{});

    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();
    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);

    // auto const nan_vis = VisibilityField::kvalue_t{
    //   Kokkos::Experimental::quiet_NaN_v<VisibilityField::kvalue_t::value_type>,
    //   Kokkos::Experimental::quiet_NaN_v<VisibilityField::kvalue_t::value_type>};

    // TODO: It's not clear to me what the best policy is here. MDRangePolicy is
    // simple, but perhaps alternatives should be tested.
    K::parallel_for(
      task_name,
      mdrange_policy<
        execution_space,
        cbp_t,
        cbp_t::axes_t::polarization_product,
        cbp_t::axes_t::baseline,
        cbp_t::axes_t::channel>(work_space, bounds),
      KOKKOS_LAMBDA(
        cbp_t::coord_t const pp,
        cbp_t::coord_t const bl,
        cbp_t::coord_t const ch) {
        auto const& flgcorr = wgt(ch, bl, pp);
        auto const& smpcorr = vis(ch, bl, pp);
        // it is expected that when flgcorr is zero, so is smpcorr; this becomes
        // a requirement in the following when nan_to_zero is true
        assert(flgcorr != 0 || smpcorr == 0);
        auto const norm =
          WeightField::value_t{1} / std::max(flgcorr, min_norm_denominator);
        normvis(ch, bl, pp) = VisibilityField::kvalue_t{
          VisibilityField::kvalue_t::value_type(smpcorr.real()) * norm,
          VisibilityField::kvalue_t::value_type(smpcorr.imag()) * norm};
        normwgt(ch, bl, pp) =
          WeightField::value_t(flgcorr) * inverse_integration_factor;
      });
  }
#else  // !RCP_USE_KOKKOS
  static auto
  serial_body(
    L::Task const*,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context,
    L::Runtime*) -> void {

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(NormalizeVisibilities::Args));

    auto integration_factor =
      reinterpret_cast<NormalizeVisibilities::Args const*>(task->args)
        ->integration_factor;

    auto& vis_region = regions[vis_region_index];
    cbp_t::rect_t bounds = vis_region;
    auto const vis =
      cbp_t::values<LEGION_READ_ONLY>(vis_region, SampleCorrelationField{});
    auto const wgt =
      cbp_t::values<LEGION_READ_ONLY>(vis_region, FlagCorrelationField{});

    auto& normvis_region = regions[normvis_region_index];
    auto const normvis =
      cbp_t::values<LEGION_WRITE_ONLY>(normvis_region, VisibilityField{});
    auto const normwgt =
      cbp_t::values<LEGION_WRITE_ONLY>(normvis_region, WeightField{});

    for (auto pp = bounds.lo[md_t::polarization_product];
         pp <= bounds.hi[md_t::polarization_product];
         ++pp)
      for (auto bl = bounds.lo[md_t::baseline]; bl <= bounds.hi[md_t::baseline];
           ++bl)
        for (auto ch = bounds.lo[md_t::channel]; ch <= bounds.hi[md_t::channel];
             ++ch) {
          cbp_t::coord_t p[]{ch, bl, pp};
          L::Point<cbp_t_t::dim, cbp_t::coord_t> pt(p);
          // it is expected that when wgt[pt] is zero, so is vis[pt]; this
          // becomes a requirement in the following when nan_to_zero is true
          assert(wgt[pt] != 0 || vis[pt] == 0);
          auto const norm =
            WeightField::value_t{1} / std::max(wgt[pt], min_norm_denominator);
          normvis[pt] = VisibilityField::value_t(
            VisibilityField::value_t::value_type(vis[pt].real()) * norm,
            VisibilityField::value_t::value_type(vis[pt].imag()) * norm);
          normwgt[pt] =
            WeightField::value_t(w[pt]) * inverse_integration_factor;
        }
  }
#endif // RCP_USE_KOKKOS

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));

    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    auto vis_layout =
      visibilities_layout().constraint_id(traits::layout_style).value();
    registrar.add_layout_constraint_set(vis_region_index, vis_layout)
      .add_layout_constraint_set(normvis_region_index, vis_layout);
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));

    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    auto vis_layout =
      visibilities_layout().constraint_id(rt, traits::layout_style).value();
    registrar.add_layout_constraint_set(vis_region_index, vis_layout)
      .add_layout_constraint_set(normvis_region_index, vis_layout);
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

L::TaskID NormalizeVisibilitiesTask::task_id;

} // end namespace rcp::symcam

NormalizeVisibilities::NormalizeVisibilities(
  L::Context ctx,
  L::Runtime* rt,
  Configuration const& config,
  cbp_t::index_space_t index_space)
  : m_args(std::make_shared<Args>()) {

  m_args->integration_factor =
    config.visibility_interval() / Configuration::fsample_interval;

  m_index_partition = cbp_t::create_partition_by_slicing(
    ctx,
    rt,
    index_space,
    {{cbp_t::axes_t::channel,
      DataPacketConfiguration::num_channels_per_packet}});

  m_launcher = L::IndexTaskLauncher(
    NormalizeVisibilitiesTask::task_id,
    rt->get_index_partition_color_space(m_index_partition),
    L::UntypedBuffer(m_args.get(), sizeof(Args)),
    L::ArgumentMap());
  m_launcher.region_requirements.resize(NormalizeVisibilitiesTask::num_regions);
}

auto
NormalizeVisibilities::launch(
  L::Context ctx,
  L::Runtime* rt,
  cbp_t::LogicalRegion visibilities,
  L::Predicate pred) -> void {

  // static bool did_fill = false;
  auto lp = cbp_t::LogicalPartition{
    rt->get_logical_partition(visibilities, m_index_partition)};
  m_launcher.region_requirements[NormalizeVisibilitiesTask::vis_region_index] =
    lp.requirement(
      0,
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      visibilities,
      StaticFields{SampleCorrelationField{}, FlagCorrelationField{}});
  m_launcher
    .region_requirements[NormalizeVisibilitiesTask::normvis_region_index] =
    lp.requirement(
      0,
      LEGION_WRITE_ONLY,
      LEGION_EXCLUSIVE,
      visibilities,
      StaticFields{VisibilityField{}, WeightField{}});
  m_launcher.predicate = pred;
  rt->execute_index_space(ctx, m_launcher);
  // if (!did_fill) {
  //   auto not_pred = rt->predicate_not(ctx, pred);
  //   rt->fill_field(
  //     ctx,
  //     visibilities,
  //     visibilities,
  //     md_t::normalized_visibility_fid,
  //     md_t::normalized_visibility_t{0},
  //     not_pred);
  //   rt->fill_field(
  //     ctx,
  //     visibilities,
  //     visibilities,
  //     md_t::normalized_weight_fid,
  //     md_t::normalized_weight_t{0},
  //     not_pred);
  //   did_fill = true;
  // }
}

auto
NormalizeVisibilities::preregister_tasks(L::TaskID task_id) -> void {
  PortableTask<NormalizeVisibilitiesTask>::preregister_task_variants(task_id);
}

auto
NormalizeVisibilities::register_tasks(L::Runtime* rt, L::TaskID task_id)
  -> void {
  PortableTask<NormalizeVisibilitiesTask>::register_task_variants(rt, task_id);
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
