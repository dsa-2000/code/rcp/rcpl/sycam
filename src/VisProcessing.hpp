// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BaselineRegion.hpp"
#include "ComputeVisibilitiesPartitions.hpp"
#include "EventProcessing.hpp"
#include "ExportGrid.hpp"
#include "FlagVisibilities.hpp"
#include "GetCFValues.hpp"
#include "GridFourierTransform.hpp"
#include "GridVisibilitiesOnImage.hpp"
#include "RecordVisibilities.hpp"
#include "Serializable.hpp"
#include "StreamState.hpp"

#include <rcp/rcp.hpp>
#include <type_traits>
#include <variant>

/*! \file VisProcessing.hpp
 *
 * Visibilities processing task support.
 */
namespace rcp::symcam {

class VisProcessingBase {
public:
  using bc_t = BaselineChannelRegion;
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using state_t = st::StateRegion;
};

// forward declaration
struct VisProcessingTask;

/*! \brief visibilities processing
 *
 * This class provides a uniform interface to invoking the visibility processing
 * stages of symcam. The options are:
 *
 * - predicated task launches of the sub-tasks in the context of the caller's
 *   task (sort of like an inline launch of this task but with predication
 *   applied to each of the sub-tasks). "Inline" version or option.
 *
 * - launching one child task within which each of the sub-tasks will be
 *   launched. "Child task" version of option.
 *
 * Note that we can't use a single VisProcessing task implementation, launched
 * using an inline task or not for the two cases, because sub-task launches are
 * predicated for the inline version but not for the child version (where they
 * are simply conditional).
 *
 * These top-level "tasks" are launched for one visibilities region.
 */
class VisProcessing {
public:
  /*! \brief BaselineChannelRegion type alias */
  using bc_t = BaselineChannelRegion;

  /*! \brief ChannelBaselinePolprodRegion type alias */
  using cbp_t = BsetChannelBaselinePolprodRegion;

  /*! \brief StateRegion type alias */
  using state_t = st::StateRegion;

  /*! \brief serialized form, for passing to a child task */
  struct Serialized {

    static_assert(Serializable<ComputeVisibilitiesPartitions>);
    static_assert(Serializable<FlagVisibilitiesStub>);
    static_assert(Serializable<RecordVisibilities>);
    static_assert(Serializable<GridFourierTransform::Plan>);
    static_assert(Serializable<ExportGrid>);
    static_assert(Serializable<GetCFValues>);
    static_assert(Serializable<GridVisibilitiesOnImage>);

    /*! \brief number of imaging configurations */
    unsigned m_num_imaging_configurations;

    /*! \brief baseline region */
    BaselineRegion::LogicalRegion m_baselines;

    /*! \brief fft plans regions*/
    std::array<
      sarray<GridFourierTransform::Plan::Serialized>,
      Configuration::max_num_imaging_configurations>
      m_fft_plans;

    /*! \brief ComputeVisibilitiesPartitions stage */
    ComputeVisibilitiesPartitions::Serialized m_compute_visibilities_partitions;

    /*! \brief FlagVisibilities stage */
    FlagVisibilitiesStub::Serialized m_flag_visibilities;

    /*! \brief RecordVisibilities stage */
    RecordVisibilities::Serialized m_record_visibilities;

    /*! \brief ExportGrid stage */
    std::array<
      ExportGrid::Serialized,
      Configuration::max_num_imaging_configurations>
      m_export_grid;

    /*! \brief GetCFValues stage */
    std::array<
      GetCFValues::Serialized,
      Configuration::max_num_imaging_configurations>
      m_get_cf_values;

    /*! \brief GridVisibilitiesOnImage stage */
    std::array<
      GridVisibilitiesOnImage::Serialized,
      Configuration::max_num_imaging_configurations>
      m_grid_visibilities;
  };
  static_assert(std::is_trivially_copyable_v<Serialized>);

  /*! \brief baselines region */
  BaselineRegion::LogicalRegion m_baselines;

  // the following members use std::shared_ptrs to support lazy serialization of
  // instances

  /*! \brief regions for FFT plans */
  std::shared_ptr<std::vector<sarray<GridFourierTransform::Plan>>> m_fft_plans;

  /*! \brief ComputeVisibilitiesPartitions instance */
  std::shared_ptr<ComputeVisibilitiesPartitions>
    m_compute_visibilities_partitions;

  /*! \brief FlagVisibilitiesStub instance */
  std::shared_ptr<FlagVisibilitiesStub> m_flag_visibilities;

  /*! \brief RecordVisibilities instance */
  std::shared_ptr<RecordVisibilities> m_record_visibilities;

  /*! \brief ExportGrid instance */
  std::shared_ptr<std::vector<ExportGrid>> m_export_grid;

  /*! \brief GetCFValues instance */
  std::shared_ptr<std::vector<GetCFValues>> m_get_cf_values;

  /*! \brief GridVisibilitiesOnImage instance */
  std::shared_ptr<std::vector<GridVisibilitiesOnImage>> m_grid_visibilities;

  /*! \brief task requirements for child tasks
   *
   * Needed for child task version, not inline version.
   */
  ChildRequirements m_child_requirements;

public:
  /*! \brief constructor */
  VisProcessing(
    L::Context ctx,
    L::Runtime* rt,
    BaselineRegion::LogicalRegion baselines,
    std::shared_ptr<std::vector<sarray<GridFourierTransform::Plan>>> const&
      fft_plans,
    std::shared_ptr<ComputeVisibilitiesPartitions> const&
      compute_visibilities_partitions,
    std::shared_ptr<FlagVisibilitiesStub> const& flag_visibilities,
    std::shared_ptr<RecordVisibilities> const& record_visibilities,
    std::shared_ptr<std::vector<ExportGrid>> const& export_grid,
    std::shared_ptr<std::vector<GetCFValues>> const& get_cf_values,
    std::shared_ptr<std::vector<GridVisibilitiesOnImage>> const&
      grid_visibilities);

  /*! \brief deserialization constructor */
  VisProcessing(
    L::Context ctx,
    L::Runtime* rt,
    Serialized const& serialized,
    bool no_child_requirements = true);

  /*! \brief default constructor (deleted) */
  VisProcessing() = delete;

  /*! \brief copy constructor (defaulted) */
  VisProcessing(VisProcessing const&) = default;

  /*! \brief move constructor (defaulted) */
  VisProcessing(VisProcessing&&) noexcept = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(VisProcessing const&) -> VisProcessing& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(VisProcessing&&) noexcept -> VisProcessing& = default;

  /*! \brief destructor */
  virtual ~VisProcessing() = default;

  /*! \brief inline launch of this task
   *
   * Not a Legion inline launch, but calls result in a sequence of predicated
   * child task launches (from the task that calls this function) that comprise
   * the visibility processing pipelines for all subarrays.
   */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<cbp_t::LogicalRegion> visibilities,
    RegionPair<bc_t::LogicalRegion> uvws,
    std::array<RegionPair<state_t::LogicalRegion>, 3> stream_states,
    st::subarray_predicates_array_t const& predicates,
    L::Predicate is_alive) -> void;

  /*! \brief individual launch of this task
   *
   * This implements the visibility processing pipelines for all subarrays in a
   * child task.
   */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<cbp_t::LogicalRegion> visibilities,
    RegionPair<bc_t::LogicalRegion> uvws,
    std::array<RegionPair<state_t::LogicalRegion>, 3> stream_states,
    L::Predicate is_alive) -> void;

  /*! \brief serialized representation of this instance */
  auto
  serialized(L::Context ctx, L::Runtime* rt) const -> Serialized;

  /*! \brief preregister task */
  static auto
  preregister_tasks(L::TaskID tid) -> void;

  /*! \brief register task */
  static auto
  register_tasks(L::Runtime* rt, L::TaskID tid) -> void;

protected:
  friend struct VisProcessingTask;

  /*! \brief common implementation for both implementation versions
   *
   * The use of a std::variant argument provides the means to choose an
   * implementation.
   */
  auto
  run(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<cbp_t::LogicalRegion> visibilities,
    RegionPair<bc_t::LogicalRegion> uvws,
    std::array<RegionPair<state_t::LogicalRegion>, 3> stream_states,
    std::variant<st::subarray_predicates_array_t, st::AllSubarraysQuery> const&
      predicates,
    L::Predicate is_alive = L::Predicate::TRUE_PRED) -> void;

private:
  /*! \brief initialize m_child_requirements member */
  auto
  init_child_requirements(L::Context ctx, L::Runtime* rt) -> void;

  /*! \brief compute ChildRequirements value that is a minimal upper bound for
   * the given requirements
   *
   * Useful for merging ChildRequirements for each of the sub-task launcher
   * instances into a single value that the Legion runtime will accept for
   * requirements of a child task.
   */
  static auto
  requirements_min_upper_bound(ChildRequirements const& requirements) noexcept
    -> ChildRequirements;
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
