// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BsetChannelBaselinePolprodRegion.hpp"
#include "DataPartitionsRegion.hpp"
#include "EventProcessing.hpp"
#include "Serializable.hpp"
#include "StreamState.hpp"
#include "libsymcam.hpp"

#include <algorithm>
#include <rcp/Mapper.hpp>
#include <rcp/rcp.hpp>

namespace rcp::symcam {

/*! \brief weights copy task for flagging
 *
 * This task copies weights from an input region to an output region,
 * restricted to the subarray subregions.
 */
struct FlagVisibilitiesCopyTask
  : public SerialOnlyTaskMixin<FlagVisibilitiesCopyTask, void> {

  using cbp_t = BsetChannelBaselinePolprodRegion;
  using part_t = DataPartitionsRegion;

  static constexpr char const* task_name = "FlagVisibilitiesCopy";
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief task region requirement indexes */
  enum {
    partition_region_index = 0,
    src_region_index,
    dst_region_index,
    num_regions
  };

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    auto subarray = part_t::rect_t{regions[partition_region_index]}
                      .lo[dim(part_t::axes_t::subarray)];
    auto partition = part_t::values<LEGION_READ_ONLY>(
      regions[partition_region_index],
      VisibilitiesSubarrayPartitionField{})[subarray];
    assert(partition.exists());
    auto src_region =
      RegionPair<cbp_t::LogicalRegion>(task->regions[src_region_index]);
    auto dst_region =
      RegionPair<cbp_t::LogicalRegion>(task->regions[dst_region_index]);

    rt->unmap_all_regions(ctx);

    L::CopyLauncher copy_weights;
    auto src_subregion =
      get_visibilities_subarray_subregion(rt, src_region, partition, subarray);
    auto dst_subregion =
      get_visibilities_subarray_subregion(rt, dst_region, partition, subarray);
    copy_weights.add_copy_requirements(
      src_subregion.requirement(
        LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{WeightField{}}),
      dst_subregion.requirement(
        LEGION_WRITE_ONLY, LEGION_EXCLUSIVE, StaticFields{WeightField{}}));
    rt->issue_copy_operation(ctx, copy_weights);
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

/*! \brief flagging task
 *
 * This task launches the flagging implementation task using the subarray
 * subregions of the visibilities and weights regions.
 */
struct FlagVisibilitiesSubarrayTask
  : public SerialOnlyTaskMixin<FlagVisibilitiesSubarrayTask, void> {

  using cbp_t = BsetChannelBaselinePolprodRegion;
  using part_t = DataPartitionsRegion;

  static constexpr char const* task_name = "FlagVisibilitiesSubarray";
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief task region requirement indexes */
  enum {
    partition_region_index = 0,
    visibilities_region_index,
    weights_region_index,
    num_regions
  };

  /*! \brief task arguments */
  struct Args {
    L::TaskID flagging_task_id;
    bool verify_regions;
  };

  /*! \brief required region index for visibilities region of flagging
   * implementation tasks */
  static constexpr auto impl_visibilities_region_index = 0;

  /*! \brief required region index for weights region of flagging implementation
   * tasks */
  static constexpr auto impl_weights_region_index = 1;

  /*! \brief task preregistration */
  static auto
  preregister(L::TaskID tid) -> void;

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    assert(sizeof(Args) == task->arglen);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    Args const& args = *reinterpret_cast<Args const*>(task->args);
    auto subarray = part_t::rect_t{regions[partition_region_index]}
                      .lo[dim(part_t::axes_t::subarray)];
    auto partition = part_t::values<LEGION_READ_ONLY>(
      regions[partition_region_index],
      VisibilitiesSubarrayPartitionField{})[subarray];

    // full regions provided to this task (which are virtually mapped)
    auto vis_region = RegionPair<cbp_t::LogicalRegion>(
      task->regions[visibilities_region_index]);
    auto wgt_region =
      RegionPair<cbp_t::LogicalRegion>(task->regions[weights_region_index]);

    // visibilities subregion for the subarray
    auto vis_subregion =
      get_visibilities_subarray_subregion(rt, vis_region, partition, subarray);
    // partition the channel axis
    auto channel_sliced_partition = cbp_t::create_partition_by_slicing(
      ctx,
      rt,
      vis_subregion.region().get_index_space(),
      {{cbp_t::axes_t::channel,
        DataPacketConfiguration::num_channels_per_packet}});
    // partition of subarray visibilities subregion
    auto vis_subregion_partition = PartitionPair<cbp_t::LogicalPartition>{
      cbp_t::LogicalPartition{rt->get_logical_partition(
        ctx,
        cbp_t::logical_region_t{vis_subregion.region()},
        channel_sliced_partition)},
      vis_subregion.parent()};
    // weights subregion for the subarray
    auto wgt_subregion =
      get_visibilities_subarray_subregion(rt, wgt_region, partition, subarray);
    // partition of subarray weights subregion
    auto wgt_subregion_partition = PartitionPair<cbp_t::LogicalPartition>{
      cbp_t::LogicalPartition{rt->get_logical_partition(
        ctx,
        cbp_t::logical_region_t{wgt_subregion.region()},
        channel_sliced_partition)},
      wgt_subregion.parent()};

    rt->unmap_all_regions(ctx);

    // index launch of flagging implementation task over the channels partition
    auto flagging_launcher = L::IndexTaskLauncher(
      args.flagging_task_id,
      rt->get_index_partition_color_space(channel_sliced_partition),
      L::UntypedBuffer(&args.verify_regions, sizeof(args.verify_regions)),
      L::ArgumentMap());
    flagging_launcher.region_requirements.resize(2);
    flagging_launcher.region_requirements[impl_visibilities_region_index] =
      vis_subregion_partition.requirement(
        rt,
        0,
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        StaticFields{VisibilityField{}});
    flagging_launcher.region_requirements[impl_weights_region_index] =
      wgt_subregion_partition.requirement(
        rt,
        0,
        LEGION_READ_WRITE,
        LEGION_EXCLUSIVE,
        StaticFields{WeightField{}});
    rt->execute_index_space(ctx, flagging_launcher);
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

/*! \brief pre-calibration flagging entry-point task
 *
 * \tparam Impl flagging implementation task launcher class
 *
 * This launcher handles the narrowing of regions for subarrays, copying
 * weights for secondary subarrays into new regions, and launching the
 * task that actually does the flagging.
 */
template <typename Impl>
class FlagVisibilities {
public:
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using part_t = DataPartitionsRegion;

private:
  /*! \brief weights regions
   *
   * Values in these regions are modified by flagging. The primary subarray is
   * configured to use the weights field of the input region, but other
   * subarrays get new regions with only a weight field.
   */
  sarray<RegionPair<cbp_t::LogicalRegion>> m_weights;

public:
  struct Serialized {
    decltype(FlagVisibilities<Impl>::m_weights) m_weights;
  };

  /*! \brief default constructor */
  FlagVisibilities() {
    std::ranges::fill(m_weights, typename decltype(m_weights)::value_type{});
  }

  /*! \brief constructor from a visibilities region
   *
   * \param visibilities_region complete visibilities region (only used as a
   * template here)
   */
  FlagVisibilities(
    L::Context ctx,
    L::Runtime* rt,
    cbp_t::LogicalRegion const& visibilities_region) {

    m_weights[0] = {};
    for (auto&& wgts : std::views::drop(m_weights, 1))
      wgts = cbp_t::LogicalRegion{rt->create_logical_region(
        ctx,
        cbp_t::index_space_t{visibilities_region.get_index_space()},
        visibilities_region.get_field_space())};
  }

  /*! \brief deserialization constructor */
  FlagVisibilities(L::Context ctx, L::Runtime* rt, Serialized const& serialized)
    : m_weights{serialized.m_weights} {}

  /*! \brief copy constructor (defaulted) */
  FlagVisibilities(FlagVisibilities const&) = default;

  /*! \brief move constructor (defaulted) */
  FlagVisibilities(FlagVisibilities&&) noexcept = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(FlagVisibilities const&) -> FlagVisibilities& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(FlagVisibilities&&) noexcept -> FlagVisibilities& = default;

  /*! \brief serialized form */
  [[nodiscard]] auto
  serialized(L::Context /*ctx*/, L::Runtime* /*rt*/) const noexcept
    -> Serialized {
    return {m_weights};
  }

  /*! \brief requirements for child tasks that use Serialized values as
   *  arguments */
  [[nodiscard]] auto
  child_requirements(L::Context /*ctx*/, L::Runtime* /*rt*/) const
    -> ChildRequirements {

    std::vector<L::RegionRequirement> result;
    auto secondary_weights = std::views::drop(m_weights, 1);
    result.reserve(secondary_weights.size());
    for (auto&& wgts : secondary_weights)
      result.push_back(wgts.requirement(
        LEGION_READ_WRITE,
        LEGION_EXCLUSIVE,
        StaticFields{WeightField{}},
        std::nullopt,
        L::Mapping::DefaultMapper::VIRTUAL_MAP));
    return {result, {}, {}};
  }

  /*! \brief get weights regions for secondary subarrays
   */
  [[nodiscard]] auto
  weights() const -> sarray<RegionPair<cbp_t::LogicalRegion>> const& {
    return m_weights;
  }

  /*! \brief destroy allocated resources */
  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void {
    for (auto&& wgts :
         std::views::drop(m_weights, 1) | std::views::filter([](auto&& rg) {
           return rg.region() != cbp_t::LogicalRegion{};
         }))
      rt->destroy_logical_region(ctx, wgts.region());
  }

  /*! \brief task do flagging for a subarray
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param partitions_by_subarray visibilities/weights partition
   * \param visibilities visibilities to flag
   * \param is_subarray predicates for active subarrays
   * \param is_flagging predicates for flagging status of subarrays
   *
   * For consistency, at a minimum, weights are always copied from the input
   * region to the output region for each active subarray, regardless of whether
   * flaggins is active or not.
   */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    st::subarray_predicates_array_t const& predicates) -> void {

    auto is_subarray = predicate_slice_subarray_predicates(
      predicates, st::SubarrayStreamPredicate::is_subarray);
    auto is_flagging = predicate_slice_subarray_predicates(
      predicates, st::SubarrayStreamPredicate::is_pre_calibration_flagging);

    typename FlagVisibilitiesSubarrayTask::Args args{
      .flagging_task_id = Impl::task_id};
    auto flagging_launcher = L::TaskLauncher(
      FlagVisibilitiesSubarrayTask::task_id,
      L::UntypedBuffer(&args, sizeof(args)));
    flagging_launcher.region_requirements.resize(
      FlagVisibilitiesSubarrayTask::num_regions);
    flagging_launcher.region_requirements
      [FlagVisibilitiesSubarrayTask::visibilities_region_index] =
      visibilities.requirement(
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        StaticFields{VisibilityField{}},
        std::nullopt,
        L::Mapping::DefaultMapper::VIRTUAL_MAP);

    // Copy weights for secondary subarrays
    auto copy_launcher =
      L::TaskLauncher(FlagVisibilitiesCopyTask::task_id, L::UntypedBuffer());
    copy_launcher.region_requirements.resize(
      FlagVisibilitiesCopyTask::num_regions);
    copy_launcher
      .region_requirements[FlagVisibilitiesCopyTask::src_region_index] =
      visibilities.requirement(
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        StaticFields{WeightField{}},
        std::nullopt,
        L::Mapping::DefaultMapper::VIRTUAL_MAP);

    // Iterate over secondary subarrays prior to the primary subarray, because
    // we must first copy weights for all secondary subarrays before flagging in
    // the primary subarray changes them. We accomplish this by iterating over
    // subarrays in reverse order.
    for (auto&& subarray : std::views::reverse(subarray_slots())) {
      auto partition_subregion = get_partitions_region_for_subarray(
        rt, partitions_by_subarray, subarray);
      auto& weights = m_weights.at(subarray);
      if (subarray != 0) {
        // in the case of a secondary subarray, we need to copy weights into
        // another region
        copy_launcher.predicate = is_subarray(subarray);
        copy_launcher.region_requirements
          [FlagVisibilitiesCopyTask::partition_region_index] =
          partition_subregion.requirement(
            LEGION_READ_ONLY,
            LEGION_EXCLUSIVE,
            StaticFields{VisibilitiesSubarrayPartitionField{}});
        copy_launcher
          .region_requirements[FlagVisibilitiesCopyTask::dst_region_index] =
          weights.requirement(
            LEGION_WRITE_ONLY,
            LEGION_EXCLUSIVE,
            StaticFields{WeightField{}},
            std::nullopt,
            L::Mapping::DefaultMapper::VIRTUAL_MAP);
        rt->execute_task(ctx, copy_launcher);
      } else {
        // For primary subarray, we don't use another region. Note that this
        // assignment updates the local m_weights[0] value, which is required
        // for the proper values to be returned from weights() after a call to
        // this function.
        weights = visibilities;
      }
      flagging_launcher.region_requirements
        [FlagVisibilitiesSubarrayTask::weights_region_index] =
        weights.requirement(
          LEGION_READ_WRITE,
          LEGION_EXCLUSIVE,
          StaticFields{WeightField{}},
          std::nullopt,
          L::Mapping::DefaultMapper::VIRTUAL_MAP);
      flagging_launcher.region_requirements
        [FlagVisibilitiesSubarrayTask::partition_region_index] =
        partition_subregion.requirement(
          LEGION_READ_ONLY,
          LEGION_EXCLUSIVE,
          StaticFields{VisibilitiesSubarrayPartitionField{}});
      flagging_launcher.predicate = is_flagging(subarray);
      rt->execute_task(ctx, flagging_launcher);
    }
  }

  /*! \brief task do flagging for a subarray, without predicates
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param partitions_by_subarray visibilities/weights partition
   * \param visibilities visibilities to flag
   * \param is_subarray flags for active subarrays
   * \param is_flagging flags for flagging status of subarrays
   *
   * For consistency, at a minimum, weights are always copied from the input
   * region to the output region for each active subarray, regardless of whether
   * flaggins is active or not.
   */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    st::AllSubarraysQuery const& state_query) -> void {

    auto const is_subarray = state_query.is_subarray();
    auto const is_flagging = state_query.do_pre_calibration_flagging();

    typename FlagVisibilitiesSubarrayTask::Args args{
      .flagging_task_id = Impl::task_id};
    auto flagging_launcher = L::TaskLauncher(
      FlagVisibilitiesSubarrayTask::task_id,
      L::UntypedBuffer(&args, sizeof(args)));
    flagging_launcher.region_requirements.resize(
      FlagVisibilitiesSubarrayTask::num_regions);
    flagging_launcher.region_requirements
      [FlagVisibilitiesSubarrayTask::visibilities_region_index] =
      visibilities.requirement(
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        StaticFields{VisibilityField{}},
        std::nullopt,
        L::Mapping::DefaultMapper::VIRTUAL_MAP);

    // Copy weights for secondary subarrays
    auto copy_launcher =
      L::TaskLauncher(FlagVisibilitiesCopyTask::task_id, L::UntypedBuffer());
    copy_launcher.region_requirements.resize(
      FlagVisibilitiesCopyTask::num_regions);
    copy_launcher
      .region_requirements[FlagVisibilitiesCopyTask::src_region_index] =
      visibilities.requirement(
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        StaticFields{WeightField{}},
        std::nullopt,
        L::Mapping::DefaultMapper::VIRTUAL_MAP);

    // Iterate over secondary subarrays prior to the primary subarray, because
    // we must first copy weights for all secondary subarrays before flagging in
    // the primary subarray changes them. We accomplish this by iterating over
    // subarrays in reverse order.
    for (auto&& subarray_slot :
         std::views::reverse(filtered_subarray_slots(is_flagging))) {
      auto partition_subregion = get_partitions_region_for_subarray(
        rt, partitions_by_subarray, subarray_slot);
      auto& weights = m_weights.at(subarray_slot);
      if (subarray_slot != 0) {
        // in the case of a secondary subarray, we need to copy weights into
        // another region
        if (is_subarray[subarray_slot]) {
          copy_launcher.region_requirements
            [FlagVisibilitiesCopyTask::partition_region_index] =
            partition_subregion.requirement(
              LEGION_READ_ONLY,
              LEGION_EXCLUSIVE,
              StaticFields{VisibilitiesSubarrayPartitionField{}});
          copy_launcher
            .region_requirements[FlagVisibilitiesCopyTask::dst_region_index] =
            weights.requirement(
              LEGION_WRITE_ONLY,
              LEGION_EXCLUSIVE,
              StaticFields{WeightField{}},
              std::nullopt,
              L::Mapping::DefaultMapper::VIRTUAL_MAP);
          rt->execute_task(ctx, copy_launcher);
        }
      } else {
        // For primary subarray, we don't use another region. Note that this
        // assignment updates the local m_weights[0] value, which is required
        // for the proper values to be returned from weights() after a call to
        // this function.
        weights = visibilities;
      }
      flagging_launcher.region_requirements
        [FlagVisibilitiesSubarrayTask::weights_region_index] =
        weights.requirement(
          LEGION_READ_WRITE,
          LEGION_EXCLUSIVE,
          StaticFields{WeightField{}},
          std::nullopt,
          L::Mapping::DefaultMapper::VIRTUAL_MAP);
      flagging_launcher.region_requirements
        [FlagVisibilitiesSubarrayTask::partition_region_index] =
        partition_subregion.requirement(
          LEGION_READ_ONLY,
          LEGION_EXCLUSIVE,
          StaticFields{VisibilitiesSubarrayPartitionField{}});
      rt->execute_task(ctx, flagging_launcher);
    }
  }
  static auto
  preregister_tasks(std::array<L::TaskID, 3> const& task_ids) -> void {
    PortableTask<FlagVisibilitiesCopyTask>::preregister_task_variants(
      task_ids[0]);
    PortableTask<FlagVisibilitiesSubarrayTask>::preregister_task_variants(
      task_ids[1]);
    PortableTask<Impl>::preregister_task_variants(task_ids[2]);
  }

  static auto
  register_tasks(L::Runtime* rt, std::array<L::TaskID, 3> const& task_ids)
    -> void {
    PortableTask<FlagVisibilitiesCopyTask>::register_task_variants(
      rt, task_ids[0]);
    PortableTask<FlagVisibilitiesSubarrayTask>::register_task_variants(
      rt, task_ids[1]);
    PortableTask<Impl>::register_task_variants(rt, task_ids[2]);
  }
};

/*! \brief stub flagging implementation task class
 *
 * This task does not alter any weights values.
 */
struct FlagVisibilitiesStubImpl
  : public SerialOnlyTaskMixin<FlagVisibilitiesStubImpl, void> {

  using cbp_t = BsetChannelBaselinePolprodRegion;
  using part_t = DataPartitionsRegion;

  static constexpr char const* task_name = "FlagVisibilitiesStubImpl";
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief task implementation */
  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {
    assert(task->arglen == sizeof(bool));
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    // bool verify_regions = *reinterpret_cast<bool const*>(task->args);
    // auto const& vis_region = regions[visibilities_region_index];
    // auto const& wgt_region = regions[weights_region_index];
    // for (auto piece = L::PieceIteratorT<cbp_t::dim, cbp_t::coord_t>(
    //        vis_region, VisibilityField::field_id, true);
    //      piece();
    //      piece++) {
    // }
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

using FlagVisibilitiesStub = FlagVisibilities<FlagVisibilitiesStubImpl>;

extern template class FlagVisibilities<FlagVisibilitiesStubImpl>;

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
