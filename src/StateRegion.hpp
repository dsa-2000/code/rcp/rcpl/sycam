// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "StaticFields.hpp"
#include "StreamState.hpp"

/*! \file StateRegion.hpp
 *
 * Definition of region for st::State value.
 */

namespace rcp::symcam::st {

/* \brief States region index space axes definition */
enum class StateAxes { shard = LEGION_DIM_0 };

/* \brief State field definition */
using StateField = StaticField<st::State, field_ids::state>;

/*! \brief States region definition
 *
 * A 1-d index space from coordinate value 0, with a single field
 * StateField
 */
using StateRegion = StaticRegionSpec<
  IndexSpec<StateAxes, StateAxes::shard, StateAxes::shard, coord_t>,
  std::array{IndexInterval<coord_t>::left_bounded(0)},
  StateField>;

} // namespace rcp::symcam::st

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
