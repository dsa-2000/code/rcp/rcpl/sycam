// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "EventScript.hpp"
#include "SimpleEvent.hpp"

#include <chrono>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>

using namespace rcp::symcam::st;

static_assert(std::ranges::range<EventScript>);
static_assert(std::ranges::enable_view<EventScript>);
static_assert(std::ranges::input_range<EventScript>);
static_assert(std::ranges::common_range<EventScript>);
static_assert(
  std::is_same_v<std::ranges::range_value_t<EventScript>, nlohmann::json>);

auto
EventScriptIterator::block_max_count(
  std::optional<nlohmann::json const*> const& maybe_blockp) -> unsigned {
  if (!maybe_blockp)
    return 0;
  auto const* blockp = *maybe_blockp;
  return (
    blockp->contains(EventScript::block_count_tag)
      ? (*blockp)[EventScript::block_count_tag].get<unsigned>()
      : 1);
}

auto
EventScriptIterator::block_period(std::optional<nlohmann::json const*> const&
                                    maybe_blockp) -> StreamClock::duration {
  StreamClock::duration result{0};
  if (!maybe_blockp)
    return result;
  auto const* blockp = *maybe_blockp;
  if (blockp->contains(EventScript::block_period_tag)) {
    auto pstr = (*blockp)[EventScript::block_period_tag].get<std::string>();
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto maybe_duration =
      parse_duration(reinterpret_cast<char8_t const*>(pstr.c_str()));
    if (!maybe_duration)
      throw DurationParseError(pstr.c_str());
    result = *maybe_duration;
  }
  return result;
}

[[nodiscard]] auto
EventScriptIterator::current_block() const
  -> std::optional<nlohmann::json const*> {
  if (!script || at_end)
    return std::nullopt;
  auto const& blocks = (*script)[EventScript::script_tag];
  assert(blocks.is_array());
  if (at_end || current_block_index >= blocks.size()) {
    return std::nullopt;
  }
  return &blocks.at(current_block_index);
}

EventScriptIterator::EventScriptIterator(
  std::shared_ptr<nlohmann::json const> const& js)
  : script(js)
  , at_end(!js)
  , current_block_max_count(block_max_count(current_block()))
  , current_block_period(block_period(current_block())) {

  at_end = !current_block().has_value();
  validate_current_block();
}

auto
EventScriptIterator::operator++() -> EventScriptIterator& {
  if (at_end)
    throw std::out_of_range("Iterator is at end");
  // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
  auto const* blockp = current_block().value();
  auto events_size = (*blockp)[EventScript::block_events_tag].size();
  ++current_block_event_index;
  if (current_block_event_index == events_size) {
    current_block_event_index = 0;
    ++current_block_current_count;
    if (current_block_current_count == current_block_max_count) {
      current_block_current_count = 0;
      ++current_block_index;
      current_block_max_count = block_max_count(current_block());
      current_block_period = block_period(current_block());
      if (current_block_index >= (*script)[EventScript::script_tag].size()) {
        at_end = true;
      }
      validate_current_block();
    }
  }
  return *this;
}

auto
EventScriptIterator::operator++(int) -> EventScriptIterator {
  auto result{*this};
  ++(*this);
  return result;
}

auto
EventScriptIterator::operator*() const -> nlohmann::json {
  if (at_end)
    throw std::out_of_range("Iterator is at end");
  auto event =
    // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
    (*current_block().value())[EventScript::block_events_tag].at(
      current_block_event_index);
  auto& event_val = JSONValue::get_value(event);
  if (!JSONSimpleEvent::is_valid(event_val))
    throw NoTimestampError();
  auto& ts = JSONSimpleEvent::get_timestamp(event_val);
  auto ts_str = ts.get<std::string>();
  auto maybe_ts =
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    parse_timestamp(reinterpret_cast<char8_t const*>(ts_str.c_str()));
  if (!maybe_ts)
    throw TimestampParseError(ts_str.c_str());
  auto new_ts = *maybe_ts + current_block_current_count * current_block_period;
  ts = to_string(new_ts);
  return event;
}

auto
EventScriptIterator::operator==(EventScriptIterator const& rhs) const -> bool {
  return (at_end && rhs.at_end)
    || ((script.get() == rhs.script.get()
         || (script && rhs.script && *script == *rhs.script))
        && at_end == rhs.at_end
        && current_block_index == rhs.current_block_index
        && current_block_current_count == rhs.current_block_current_count
        && current_block_event_index == rhs.current_block_event_index);
}

auto
EventScriptIterator::validate_current_block() -> void {
  if (!at_end) {
    if (current_block_max_count == 0)
      throw CountTooSmallError();
    if (
      current_block_max_count > 1
      && current_block_period == StreamClock::duration{0})
      throw ZeroPeriodError();
  }
}

EventScript::EventScript(nlohmann::json js)
  : m_json(std::make_shared<nlohmann::json>(std::move(js))) {
  // traverse script once to validate and compute total number of
  // unrolled events
  if (!m_json->contains(script_tag))
    throw NotEventScriptError();
  for (auto&& event : *this) {
    [[maybe_unused]] auto evp = get_event(event);
    ++m_num_events;
  }
}

[[nodiscard]] auto
EventScript::begin() const -> EventScriptIterator {
  return {m_json};
}

auto
EventScript::end() noexcept -> EventScriptIterator {
  return {};
}

auto
EventScript::size() const -> unsigned {
  return m_num_events;
}

auto
EventScript::as_json() const -> nlohmann::json const& {
  static nlohmann::json empty;
  if (m_json)
    return *m_json;
  return empty;
}

auto
EventScript::with_fixed_epoch(
  StreamClock::external_time const& epoch) const& -> EventScript {
  return EventScript(*this).with_fixed_epoch(epoch);
}

auto
EventScript::with_fixed_epoch(
  StreamClock::external_time const& epoch) && -> EventScript {

  if (m_json) {
    auto fixed = std::make_shared<nlohmann::json>(*m_json);
    for (auto&& block : fixed->at(script_tag)) {
      for (auto&& event : block.at(block_events_tag))
        event =
          rcp::symcam::st::as_json(*get_event(event)->with_fixed_epoch(epoch));
    }
    m_json = fixed;
  }
  return std::move(*this);
}

static auto
only_relative_past(nlohmann::json const& jss) -> bool {
  for (auto&& js : jss) {
    auto const timestamp_str =
      JSONSimpleEvent::get_timestamp(js.at(JSONValue::meta_value_name))
        .get<std::string>();
    auto const maybe_timestamp =
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      parse_timestamp(reinterpret_cast<char8_t const*>(timestamp_str.c_str()));
    if (
      !maybe_timestamp
      || !std::holds_alternative<rcp::symcam::StreamClock::duration>(
        *maybe_timestamp))
      return false;
    auto ts = std::get<rcp::symcam::StreamClock::duration>(*maybe_timestamp);
    if (ts.count() >= 0)
      return false;
  }
  return true;
}

auto
EventScript::partition_relative_past_events() const
  -> std::tuple<EventScript, EventScript> {
  std::vector<nlohmann::json> relative_past;
  std::vector<nlohmann::json> remainder;

  for (auto&& js : m_json->at(script_tag)) {
    assert(js.contains(block_events_tag));
    if (
      js.contains(block_count_tag) || js.contains(block_period_tag)
      || !only_relative_past(js.at(block_events_tag))) {
      remainder.push_back(js);
    } else {
      relative_past.push_back(js);
    }
  }
  return {
    nlohmann::json{{script_tag, relative_past}},
    nlohmann::json{{script_tag, remainder}}};
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
