// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "SimpleEvent.hpp"
#include "libsymcam.hpp"
#include <cstdlib>
#include <filesystem>
#include <regex>
#include <unordered_map>

/*! \file SubarrayEvent.hpp
 *
 * Base class for event with a timestamp and a subarray id for payload.
 */

namespace rcp::symcam::st {

/*! \brief error representing failure to parse subarray (name) value */
struct SubarrayNameLengthError : public JSONError {
  SubarrayNameLengthError(std::string const& subarray)
    : JSONError(
        std::string("Failed to parse imaging id value '") + subarray + "'") {}
};

struct InvalidSubarrayName : public JSONError {
  InvalidSubarrayName(subarray_name_t const& name)
    : JSONError(
        std::string("Invalid subarray name, value '") + name.data() + "'") {}
};

struct JSONSubarrayEvent : public JSONSimpleEvent {

  /*! \brief JSON imaging event subarray name value member tag */
  static constexpr char const* subarray_tag = "subarray";

  /*! \brief test for subarray name member */
  [[nodiscard]] static auto
  has_subarray(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get subarray name value */
  [[nodiscard]] static auto
  get_subarray(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get subarray name value */
  [[nodiscard]] static auto
  get_subarray(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief test for valid format */
  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

template <typename Event>
class SubarrayEvent;

// forward declaration of from_json() for ImagingEvent<Event> types
template <typename Event>
auto
from_json(nlohmann::json const& js, SubarrayEvent<Event>& evt) -> void;

/*! \brief stream event with subarray payload */
template <typename Event>
class SubarrayEvent : public SimpleEvent<Event> {

  subarray_name_t m_subarray_name{'\0'};

  friend auto
  from_json<>(nlohmann::json const& js, SubarrayEvent<Event>& evt) -> void;

public:
  /*! \brief default constructor (defaulted) */
  SubarrayEvent() noexcept = default;
  /*! \brief constructor from subarray (name) and timestamp */
  SubarrayEvent(
    subarray_name_t const& subarray_name,
    StreamClock::timestamp_t const& ts) noexcept
    : SimpleEvent<Event>(ts), m_subarray_name(subarray_name) {}
  /*! \brief copy constructor (defaulted) */
  SubarrayEvent(SubarrayEvent const&) noexcept = default;
  /*! \brief move constructor (defaulted) */
  SubarrayEvent(SubarrayEvent&&) noexcept = default;
  /*! \brief copy constructor with updated timestamp */
  SubarrayEvent(SubarrayEvent const& event, StreamClock::timestamp_t const& ts)
    : SimpleEvent<Event>(event, ts), m_subarray_name(event.m_subarray_name) {}
  /*! \brief move constructor with updated timestamp */
  SubarrayEvent(SubarrayEvent&& event, StreamClock::timestamp_t const& ts)
    : SimpleEvent<Event>(std::move(event), ts)
    , m_subarray_name(event.m_subarray_name) {}

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(SubarrayEvent const&) -> SubarrayEvent& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(SubarrayEvent&&) noexcept -> SubarrayEvent& = default;
  /*! \brief destructor (defaulted) */
  ~SubarrayEvent() override = default;

  [[nodiscard]] auto
  name() const -> subarray_name_t const& {
    return m_subarray_name;
  }

protected:
  [[nodiscard]] auto
  name() -> subarray_name_t& {
    return m_subarray_name;
  }
};

/*! \brief convert ImagingEvent instance to JSON
 *
 * Form defined by nlohmann::json
 */
template <typename Event>
auto
to_json(nlohmann::json& js, SubarrayEvent<Event> const& evt) -> void {
  js = {
    {JSONValue::meta_tag_name, Event::class_tag},
    {JSONValue::meta_value_name,
     {{JSONSubarrayEvent::timestamp_tag, to_string(evt.timestamp())},
      {JSONSubarrayEvent::subarray_tag, evt.name().data()}}}};
}

/*! \brief convert JSON value to SubarrayEvent instance
 *
 * Form defined by nlohmann::json
 */
template <typename Event>
auto
from_json(nlohmann::json const& js, SubarrayEvent<Event>& evt) -> void {
  if (!JSONValue::is_valid(js))
    throw JSONValueFormatError(js);

  auto tag = JSONValue::get_tag(js);
  if (tag != std::string_view(Event::class_tag))
    throw BadEventTagError(Event::class_tag, tag.c_str());

  auto jsval = JSONValue::get_value(js);
  if (!JSONSubarrayEvent::is_valid(jsval))
    throw JSONValueFormatError(js);

  auto const timestamp_str =
    JSONSubarrayEvent::get_timestamp(jsval).get<std::string>();
  auto const maybe_timestamp =
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    parse_timestamp(reinterpret_cast<char8_t const*>(timestamp_str.c_str()));
  if (!maybe_timestamp)
    throw TimestampParseError(timestamp_str.c_str());
  evt.timestamp() = *maybe_timestamp;

  auto const subarray_str =
    JSONSubarrayEvent::get_subarray(jsval).get<std::string>();
  if (subarray_str.size() >= subarray_name_t{}.size())
    throw SubarrayNameLengthError(subarray_str);
  std::ranges::fill(evt.name(), '\0');
  std::ranges::copy(subarray_str, evt.name().data());
}

struct JSONReceiverSet {

  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) -> bool {
    return js.is_array() && std::ranges::all_of(js, [](auto&& aval) {
             return (aval.is_number_integer()
                     && aval.template get<unsigned>()
                          < Configuration::num_receivers)
                    || (aval.is_string() && aval.template get<std::string>() == "*");
           });
  }
};

struct JSONReceiversOuterProduct {
  static constexpr char const* left_tag = "left";
  static constexpr char const* right_tag = "right";

  [[nodiscard]] static auto
  get_left(nlohmann::json const& js) -> nlohmann::json const&;

  [[nodiscard]] static auto
  get_right(nlohmann::json const& js) -> nlohmann::json const&;

  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

struct JSONReceiverPairs {

  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

struct JSONReceiverPairSet {

  /*! \brief */
  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

struct JSONCreateSubarray : public JSONSubarrayEvent {

  /*! \brief slot member tag */
  static constexpr char const* slot_tag = "slot";
  /*! \brief receiver pairs member tag */
  static constexpr char const* pairs_tag = "pairs";

  /*! \brief test for slot name member */
  [[nodiscard]] static auto
  has_slot(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get slot value */
  [[nodiscard]] static auto
  get_slot(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get slot value */
  [[nodiscard]] static auto
  get_slot(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief test for slot name member */
  [[nodiscard]] static auto
  has_pairs(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get slot value */
  [[nodiscard]] static auto
  get_pairs(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get slot value */
  [[nodiscard]] static auto
  get_pairs(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief test for valid format */
  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

/*! \brief create subarray event
 */
class CreateSubarray : public SubarrayEvent<CreateSubarray> {
public:
  static constexpr char const* class_tag = "CreateSubarray";

  friend auto
  from_json(nlohmann::json const& js, CreateSubarray& evt) -> void;

private:
  unsigned m_slot{0};

  subarray_t m_subarray;

public:
  /*! \brief default constructor (defaulted) */
  CreateSubarray() noexcept = default;
  /*! \brief constructor from subarray slot, name, definition and timestamp */
  CreateSubarray(
    unsigned slot,
    subarray_name_t const& subarray_name,
    subarray_t subarray,
    StreamClock::timestamp_t const& ts) noexcept;
  /*! \brief copy constructor (defaulted) */
  CreateSubarray(CreateSubarray const&) noexcept = default;
  /*! \brief move constructor (defaulted) */
  CreateSubarray(CreateSubarray&&) noexcept = default;
  /*! \brief copy constructor with updated timestamp */
  CreateSubarray(
    CreateSubarray const& event, StreamClock::timestamp_t const& ts);
  /*! \brief move constructor with updated timestamp */
  CreateSubarray(CreateSubarray&& event, StreamClock::timestamp_t const& ts);

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(CreateSubarray const&) -> CreateSubarray& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(CreateSubarray&&) noexcept -> CreateSubarray& = default;
  /*! \brief destructor (defaulted) */
  ~CreateSubarray() override = default;

  /*! \brief set subarray name for the slot
   *
   * \param st state instance
   *
   * \return copy of st, but with subarray name in slot changed
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState;

  [[nodiscard]] auto
  slot() const -> unsigned const&;

  [[nodiscard]] auto
  subarray() const -> subarray_t const&;

protected:
  auto
  slot() -> unsigned&;

  auto
  subarray() -> subarray_t&;
};

/*! \brief convert CreateSubarray instance to JSON
 *
 * Form defined by nlohmann::json
 */
auto
to_json(nlohmann::json& js, CreateSubarray const& evt) -> void;

auto
from_json(nlohmann::json const& js, CreateSubarray& evt) -> void;

/*! \brief destroy subarray event
 */
struct DestroySubarray : public SubarrayEvent<DestroySubarray> {

  static constexpr char const* class_tag = "DestroySubarray";

  /*! \brief inherited constructors */
  using SubarrayEvent<DestroySubarray>::SubarrayEvent;

  /*! \brief set state's "imaging" status to false
   *
   * \param st state instance
   *
   * \return copy of st, but with imaging status for affected id altered
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState;
};

/*! \brief StartRecordEventStream */

struct JSONStartRecordEventStream : public JSONSubarrayEvent {

  /*! \brief file path member tag */
  static constexpr char const* filepath_tag = "filepath";

  /*! \brief test for filepath name member */
  [[nodiscard]] static auto
  has_filepath(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get filepath value */
  [[nodiscard]] static auto
  get_filepath(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get filepath value */
  [[nodiscard]] static auto
  get_filepath(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief test for valid format */
  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

/*! \brief start saving event stream event
 */
class StartRecordEventStream : public SubarrayEvent<StartRecordEventStream> {
public:
  static constexpr char const* class_tag = "StartRecordEventStream";

  friend auto
  from_json(nlohmann::json const& js, StartRecordEventStream& evt) -> void;

private:
  std::filesystem::path m_filepath;

public:
  /*! \brief default constructor (defaulted) */
  StartRecordEventStream() noexcept = default;
  /*! \brief constructor from subarray (name), output file path and timestamp */
  StartRecordEventStream(
    subarray_name_t const& subarray_name,
    std::filesystem::path filepath,
    StreamClock::timestamp_t const& ts) noexcept;
  /*! \brief copy constructor (defaulted) */
  StartRecordEventStream(StartRecordEventStream const&) noexcept = default;
  /*! \brief move constructor (defaulted) */
  StartRecordEventStream(StartRecordEventStream&&) noexcept = default;
  /*! \brief copy constructor with updated timestamp */
  StartRecordEventStream(
    StartRecordEventStream const& event, StreamClock::timestamp_t const& ts);
  /*! \brief move constructor with updated timestamp */
  StartRecordEventStream(
    StartRecordEventStream&& event, StreamClock::timestamp_t const& ts);

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(StartRecordEventStream const&) -> StartRecordEventStream& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(StartRecordEventStream&&) noexcept
    -> StartRecordEventStream& = default;
  /*! \brief destructor (defaulted) */
  ~StartRecordEventStream() override = default;

  /*! \brief start recording event stream for a subarray
   *
   * \param st state instance
   *
   * \return copy of st, but with recording to the given file enabled
   *
   * First stops recording to the current recording file, if recording is in
   * progress.
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState;

  [[nodiscard]] auto
  filepath() const -> std::filesystem::path const&;

protected:
  auto
  filepath() -> std::filesystem::path&;
};

auto
from_json(nlohmann::json const& js, StartRecordEventStream& evt) -> void;

auto
to_json(nlohmann::json& js, StartRecordEventStream const& evt) -> void;

struct StopRecordEventStream : public SubarrayEvent<StopRecordEventStream> {
  static constexpr char const* class_tag = "StopRecordEventStream";

  /*! \brief inherited constructors */
  using SubarrayEvent<StopRecordEventStream>::SubarrayEvent;

  /*! \brief stop recording event stream for a subarray
   *
   * \param st state instance
   *
   * \return copy of st, but with recording disabled
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState;
};

/*! \brief JSON representation of StartRecordDataStream */
struct JSONStartRecordDataStream : public JSONSubarrayEvent {

  /*! \brief file path member tag */
  static constexpr char const* filepath_tag = "filepath";

  /*! \brief channel selection member tag */
  static constexpr char const* channels_tag = "channels";

  /*! \brief parameters member tag */
  static constexpr char const* parameters_tag = "parameters";

  /*! \brief test for filepath name member */
  [[nodiscard]] static auto
  has_filepath(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get filepath value */
  [[nodiscard]] static auto
  get_filepath(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get filepath value */
  [[nodiscard]] static auto
  get_filepath(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief test for channels name member */
  [[nodiscard]] static auto
  has_channels(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get channels value */
  [[nodiscard]] static auto
  get_channels(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get channels value */
  [[nodiscard]] static auto
  get_channels(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief parse the channels element value */
  [[nodiscard]] static auto
  parse_channels(nlohmann::json const& js) -> std::optional<ChannelSet>;

  /*! \brief test for parameters name member */
  [[nodiscard]] static auto
  has_parameters(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get parameters value */
  [[nodiscard]] static auto
  get_parameters(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get parameters value */
  [[nodiscard]] static auto
  get_parameters(nlohmann::json& js) -> nlohmann::json&;

  [[nodiscard]] static auto
  parse_parameters(nlohmann::json const& js)
    -> std::optional<std::unordered_map<std::string, std::string>>;

  /*! \brief test for valid format */
  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

/*! \brief start saving event stream event
 */
class StartRecordDataStream : public SubarrayEvent<StartRecordDataStream> {
public:
  static constexpr char const* class_tag = "StartRecordDataStream";

  friend auto
  from_json(nlohmann::json const& js, StartRecordDataStream& evt) -> void;

private:
  ChannelSet m_channels;

  std::unordered_map<std::string, std::string> m_parameters;

  std::filesystem::path m_filepath;

public:
  /*! \brief default constructor (defaulted) */
  StartRecordDataStream() noexcept = default;
  /*! \brief constructor from subarray name, channel selection, output file path
   * and timestamp */
  StartRecordDataStream(
    subarray_name_t const& subarray_name,
    ChannelSet channels,
    std::unordered_map<std::string, std::string> parameters,
    std::filesystem::path filepath,
    StreamClock::timestamp_t const& ts) noexcept;
  /*! \brief copy constructor (defaulted) */
  StartRecordDataStream(StartRecordDataStream const&) noexcept = default;
  /*! \brief move constructor (defaulted) */
  StartRecordDataStream(StartRecordDataStream&&) noexcept = default;
  /*! \brief copy constructor with updated timestamp */
  StartRecordDataStream(
    StartRecordDataStream const& event, StreamClock::timestamp_t const& ts);
  /*! \brief move constructor with updated timestamp */
  StartRecordDataStream(
    StartRecordDataStream&& event, StreamClock::timestamp_t const& ts);

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(StartRecordDataStream const&) -> StartRecordDataStream& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(StartRecordDataStream&&) noexcept
    -> StartRecordDataStream& = default;
  /*! \brief destructor (defaulted) */
  ~StartRecordDataStream() override = default;

  /*! \brief start recording event stream for a subarray
   *
   * \param st state instance
   *
   * \return copy of st, but with recording to the given file enabled
   *
   * First stops recording to the current recording file, if recording is in
   * progress.
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState;

  [[nodiscard]] auto
  filepath() const -> std::filesystem::path const&;

  [[nodiscard]] auto
  channels() const -> ChannelSet const&;

  [[nodiscard]] auto
  parameters() const -> std::unordered_map<std::string, std::string> const&;

protected:
  auto
  filepath() -> std::filesystem::path&;

  auto
  channels() -> ChannelSet&;

  auto
  parameters() -> std::unordered_map<std::string, std::string>&;
};

auto
from_json(nlohmann::json const& js, StartRecordDataStream& evt) -> void;

auto
to_json(nlohmann::json& js, StartRecordDataStream const& evt) -> void;

struct StopRecordDataStream : public SubarrayEvent<StopRecordDataStream> {
  static constexpr char const* class_tag = "StopRecordDataStream";

  /*! \brief inherited constructors */
  using SubarrayEvent<StopRecordDataStream>::SubarrayEvent;

  /*! \brief stop recording event stream for a subarray
   *
   * \param st state instance
   *
   * \return copy of st, but with recording disabled
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState;
};

/*! \brief JSON representation of StartPreCalibrationFlagging */
struct JSONStartPreCalibrationFlagging : public JSONSubarrayEvent {

  /*! \brief test for filepath name member */
  [[nodiscard]] static auto
  has_filepath(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get filepath value */
  [[nodiscard]] static auto
  get_filepath(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get filepath value */
  [[nodiscard]] static auto
  get_filepath(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief test for channels name member */
  [[nodiscard]] static auto
  has_channels(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get channels value */
  [[nodiscard]] static auto
  get_channels(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get channels value */
  [[nodiscard]] static auto
  get_channels(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief parse the channels element value */
  [[nodiscard]] static auto
  parse_channels(nlohmann::json const& js) -> std::optional<ChannelSet>;

  /*! \brief test for parameters name member */
  [[nodiscard]] static auto
  has_parameters(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get parameters value */
  [[nodiscard]] static auto
  get_parameters(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get parameters value */
  [[nodiscard]] static auto
  get_parameters(nlohmann::json& js) -> nlohmann::json&;

  [[nodiscard]] static auto
  parse_parameters(nlohmann::json const& js)
    -> std::optional<std::unordered_map<std::string, std::string>>;

  /*! \brief test for valid format */
  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

/*! \brief start pre-calibration flagging event
 */
struct StartPreCalibrationFlagging
  : public SubarrayEvent<StartPreCalibrationFlagging> {

  static constexpr char const* class_tag = "StartPreCalibrationFlagging";

  /*! \brief inherited constructors */
  using SubarrayEvent<StartPreCalibrationFlagging>::SubarrayEvent;

  /*! \brief set state's "flagging" status to true
   *
   * \param st state instance
   *
   * \return copy of st, but with asserted flagging status (for given subarray)
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState;
};

/*! \brief stop pre-calibration flagging event
 */
struct StopPreCalibrationFlagging
  : public SubarrayEvent<StopPreCalibrationFlagging> {

  static constexpr char const* class_tag = "StopPreCalibrationFlagging";

  /*! \brief inherited constructors */
  using SubarrayEvent<StopPreCalibrationFlagging>::SubarrayEvent;

  /*! \brief set state's "flagging" status to false
   *
   * \param st state instance
   *
   * \return copy of st, but with negated flagging status (for given subarray)
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState;
};

} // end namespace rcp::symcam::st

namespace rcp::symcam {

auto
from_json(nlohmann::json const& js, ReceiverSet& receivers) -> void;

auto
to_json(nlohmann::json& js, ReceiverSet const& receivers) -> void;

auto
from_json(nlohmann::json const& js, ReceiverSetOuterProduct& outer) -> void;

auto
to_json(nlohmann::json& js, ReceiverSetOuterProduct const& outer) -> void;

auto
from_json(nlohmann::json const& js, ReceiverPairs& pairs) -> void;

auto
to_json(nlohmann::json& js, ReceiverPairs const& pairs) -> void;

} // namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
