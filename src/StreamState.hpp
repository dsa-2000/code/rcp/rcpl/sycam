// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include "StreamEventLog.hpp"
#include "libsymcam.hpp"

#include <algorithm>
#include <bits/ranges_algo.h>
#include <compare>
#include <fstream>
#include <istream>
#include <memory>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <type_traits>

/*! \file StreamState.hpp
 *
 * Stream state for symcam.
 */

namespace rcp::symcam::st {

class ExtendedState;

/*! \brief StreamState class
 *
 * Adds some methods to StreamStateValue.
 */
class StateMixin {
public:
  using img_id_t = int;

  /*! \brief number of elements in rotation matrix */
  static auto constexpr rotation_array_sz = 3 * 3;

  /*! \brief rotation matrix type alias */
  using rotation_t = std::array<Configuration::uvw_value_t, rotation_array_sz>;

  /*! \brief maximum path length for data stream files */
  static auto constexpr max_path_length = 80;

  /*! \brief fixed length path type */
  using path_t = std::array<char, max_path_length>;

  /*! \brief maximum JSON-serialized parameters length */
  static auto constexpr max_parameters_length = 128;

  /*! \brief maximum number of channel intervals for data recording */
  static auto constexpr max_num_channel_intervals = 20;

  /*! \brief value for channel_intervals representing an invalid value */
  static constexpr std::array<ChannelSet::channel_t, 2>
    invalid_channel_interval{
      std::numeric_limits<ChannelSet::channel_t>::max(),
      std::numeric_limits<ChannelSet::channel_t>::max()};

  /*! \brief output data stream configuration */
  struct DataStream {
    path_t path; /*!< output file path */
    std::array<std::array<ChannelSet::channel_t, 2>, max_num_channel_intervals>
      channel_intervals;
    std::array<char, max_parameters_length> parameters; /*!< parameters */
    auto
    clear() -> void;
    [[nodiscard]] auto
    filepath() const -> std::filesystem::path;
  };

protected:
  bool m_is_alive{true};

  /*! \brief baselines index space partition by subarray */
  L::IndexPartitionT<1, int> m_subarray_partition{};

  /*! \brief current imaging ids */
  sarray<img_id_t> m_imaging{};

  /*! \brief current file paths for recording data streams */
  sarray<DataStream> m_record_data_streams{};

  /*! \brief current pre-calibration flagging states */
  sarray<bool> m_pre_calibration_flagging{};

  path_t m_fftw_wisdom_import_path{};

  path_t m_fftw_wisdom_export_path{};

public:
  /*! \brief get alive status */
  [[nodiscard]] auto
  is_alive() const noexcept -> bool;

  /*! \brief get index partition of baselines by subarrays */
  [[nodiscard]] auto
  subarray_partition() const -> L::IndexPartitionT<1, int>;

  /*! \brief get imaging status for a subarray
   *
   * \param slot subarray slot index
   *
   * \return imaging status for given subarray
   */
  [[nodiscard]] auto
  is_imaging(unsigned slot) const -> bool;

  /*! \brief current imaging id for a subarray */
  [[nodiscard]] auto
  imaging_id(unsigned slot) const -> std::optional<img_id_t>;

  /*! \brief field center direction, as rotation matrix */
  [[nodiscard]] auto
  field_center() const -> rotation_t;

  /*! \brief get data recording stream for a subarray
   *
   * \param slot subarray slot index
   *
   * \return data recording stream for given subarray
   */
  [[nodiscard]] auto
  record_data_stream(unsigned slot) -> DataStream&;

  /*! \brief get data recording stream for a subarray
   *
   * \param slot subarray slot index
   *
   * \return data recording stream for given subarray
   */
  [[nodiscard]] auto
  record_data_stream(unsigned slot) const -> DataStream const&;

  /*! \brief get pre-calibration flagging state for a subarray
   *
   * \param slot subarray slot index
   *
   * \return pre-calibration flagging state for given subarray
   */
  [[nodiscard]] auto
  is_pre_calibration_flagging(unsigned slot) -> bool&;

  /*! \brief get pre-calibration flagging state for a subarray
   *
   * \param slot subarray slot index
   *
   * \return pre-calibration flagging state for given subarray
   */
  [[nodiscard]] auto
  is_pre_calibration_flagging(unsigned slot) const -> bool;

  /*! \brief path to import fftw wisdom */
  [[nodiscard]] auto
  fftw_wisdom_import_path() const noexcept -> path_t const&;

  /*! \brief path to import fftw wisdom */
  [[nodiscard]] auto
  fftw_wisdom_import_path() noexcept -> path_t&;

  /*! \brief path to export fftw wisdom */
  [[nodiscard]] auto
  fftw_wisdom_export_path() const noexcept -> path_t const&;

  /*! \brief path to export fftw wisdom */
  [[nodiscard]] auto
  fftw_wisdom_export_path() noexcept -> path_t&;

  /*! \brief import fftw wisdom
   *
   * \param local flag to control whether to actually import wisdom on this
   * shard (normally asserted everywhere)
   */
  auto
  do_fftw_wisdom_import(bool local) const -> void;

  /*! \brief import fftw wisdom
   *
   * \param local flag to control whether to actually export wisdom on this
   * shard (normally asserted on only one shard)
   */
  auto
  do_fftw_wisdom_export(bool local) const -> void;
};

class State
  : public StreamStateBase<State>
  , public StateMixin {

  /*! \brief current subarrays */
  sarray<subarray_name_t> m_subarray_names{};

public:
  /*! \brief whether state has callbacks pre and post event application */
  static auto constexpr has_apply_event_callbacks = false;

  /*! \brief default constructor */
  State();
  /*! \brief construct from an ExtendedState instance */
  State(ExtendedState const& extd);
  /*! \brief copy constructor (defaulted) */
  State(State const&) = default;
  /*! \brief move constructor (defaulted) */
  State(State&&) noexcept = default;
  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(State const&) -> State& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(State&&) noexcept -> State& = default;
  /*! \brief destructor (defaulted) */
  ~State() = default;

  /*! \brief compare subarray names and slots with another state */
  [[nodiscard]] auto
  has_same_subarrays(State const& st) const -> bool;

  /*! \brief current subarrays */
  [[nodiscard]] auto
  subarray_names() const -> std::vector<subarray_name_t>;

  /*! \brief find a subarray slot associated with a given name */
  [[nodiscard]] auto
  find_subarray(subarray_name_t const& subarray_name) const
    -> std::optional<unsigned>;

  /*! \brief test for active subarray by slot index */
  [[nodiscard]] auto
  has_subarray(unsigned slot) const -> bool;

  static auto
  is_subarray(
    std::optional<State> const& prev,
    State const& current,
    std::optional<State> const& next,
    unsigned slot) -> bool;

  static auto
  is_imaging_start(
    std::optional<State> const& prev,
    State const& current,
    std::optional<State> const& next,
    unsigned slot) -> bool;

private:
  /*! \brief post-construction initialization of an instance */
  auto
  init() -> void;
};

static_assert(std::is_trivially_copyable_v<State>);

/*! \brief query wrapper for State of a single subarray */
struct SubarrayQuery {
  unsigned m_slot{};
  std::optional<State> m_prev{};
  State m_current{};
  std::optional<State> m_next{};

  SubarrayQuery() = default;

  SubarrayQuery(
    unsigned slot,
    std::optional<State> const& prev,
    State const& current,
    std::optional<State> const& next);

  SubarrayQuery(SubarrayQuery const&) = default;

  SubarrayQuery(SubarrayQuery&&) = default;

  ~SubarrayQuery() = default;

  auto
  operator=(SubarrayQuery const&) -> SubarrayQuery& = default;

  auto
  operator=(SubarrayQuery&&) -> SubarrayQuery& = default;

  [[nodiscard]] auto
  is_subarray() const -> bool;

  [[nodiscard]] auto
  do_imaging_start() const -> bool;

  [[nodiscard]] auto
  do_imaging() const -> bool;

  [[nodiscard]] auto
  do_imaging_stop() const -> bool;

  [[nodiscard]] auto
  do_record_visibilities() const -> bool;

  [[nodiscard]] auto
  do_pre_calibration_flagging() const -> bool;

  [[nodiscard]] auto
  do_fftw_wisdom_export() const noexcept -> bool;

  [[nodiscard]] auto
  do_fftw_wisdom_import() const noexcept -> bool;
};

/*! \brief query wrapper for State of a single subarray */
struct AllSubarraysQuery {
  std::optional<State> m_prev{};
  State m_current{};
  std::optional<State> m_next{};

  AllSubarraysQuery() = default;

  AllSubarraysQuery(
    std::optional<State> const& prev,
    State const& current,
    std::optional<State> const& next);

  AllSubarraysQuery(AllSubarraysQuery const&) = default;

  AllSubarraysQuery(AllSubarraysQuery&&) = default;

  ~AllSubarraysQuery() = default;

  auto
  operator=(AllSubarraysQuery const&) -> AllSubarraysQuery& = default;

  auto
  operator=(AllSubarraysQuery&&) -> AllSubarraysQuery& = default;

  [[nodiscard]] auto
  is_alive() const -> bool;

  [[nodiscard]] auto
  do_subarray_change() const -> bool;

  [[nodiscard]] auto
  is_subarray() const -> sarray<bool>;

  [[nodiscard]] auto
  do_imaging_start() const -> sarray<bool>;

  [[nodiscard]] auto
  do_imaging() const -> sarray<bool>;

  [[nodiscard]] auto
  do_imaging_stop() const -> sarray<bool>;

  [[nodiscard]] auto
  do_record_visibilities() const -> sarray<bool>;

  [[nodiscard]] auto
  do_pre_calibration_flagging() const -> sarray<bool>;

  [[nodiscard]] auto
  do_fftw_wisdom_export() const -> sarray<bool>;

  [[nodiscard]] auto
  do_fftw_wisdom_import() const -> sarray<bool>;

private:
  template <typename QueryFn>
  [[nodiscard]] auto
  all_subarrays(QueryFn&& query_fn) const -> sarray<bool> {
    sarray<bool> result;
    std::ranges::transform(
      subarray_slots(), result.begin(), [&, this](auto&& slot) -> bool {
        return (SubarrayQuery{slot, m_prev, m_current, m_next}.*query_fn)();
      });
    return result;
  }
};

/*! \brief ExtendedStreamState class
 *
 * Adds some methods to State, for use by EventProcessing. This class is not
 * trivially copyable, and is not intended for passing around in Legion regions
 * or futures.
 */
class ExtendedState
  : public StreamStateBase<ExtendedState>
  , public StateMixin {
public:
  /*! \brief whether state has callbacks pre and post event application */
  static auto constexpr has_apply_event_callbacks = true;

private:
  /*! \brief configuration instance */
  Configuration m_config;

  /*! \brief current subarrays */
  sarray<std::tuple<subarray_name_t, subarray_t>> m_subarrays{};

  /*! \brief current ofstream for recording event streams, with current
   * list separator string for the next event */
  sarray<std::tuple<std::ofstream, std::array<char, 2>>>
    m_record_event_streams{};

public:
  /*! \brief default constructor (deleted) */
  ExtendedState() = default;
  /*! \brief constructor from Legion context, runtime and Configuration
   *
   * \fixme Legion context and runtime are not used
   */
  ExtendedState(L::Context ctx, L::Runtime* rt, Configuration config);
  /*! \brief copy constructor
   *
   * This does not copy event and data streams.
   */
  ExtendedState(ExtendedState const& other) noexcept;
  /*! \brief move constructor (defaulted) */
  ExtendedState(ExtendedState&&) noexcept = default;
  /*! \brief copy assignment operator
   *
   * This does not copy event and data streams.
   */
  auto
  operator=(ExtendedState const& rhs) noexcept -> ExtendedState&;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ExtendedState&&) noexcept -> ExtendedState& = default;
  /*! \brief destructor (defaulted) */
  ~ExtendedState() = default;

  /*! \brief release allocated resources */
  auto
  destroy() noexcept -> void;

  /*! \brief callback prior to event application */
  [[nodiscard]] auto
  pre_apply(
    StreamStateEventBase<ExtendedState> const& evt) const& -> ExtendedState;

  /*! \brief callback prior to event application */
  [[nodiscard]] auto
  pre_apply(StreamStateEventBase<ExtendedState> const& evt) && -> ExtendedState;

  /*! \brief callback prior to event application */
  [[nodiscard]] auto
  post_apply(
    StreamStateEventBase<ExtendedState> const& evt) const& -> ExtendedState;

  /*! \brief callback prior to event application */
  [[nodiscard]] auto
  post_apply(
    StreamStateEventBase<ExtendedState> const& evt) && -> ExtendedState;

  /*! \brief set alive status */
  [[nodiscard]] auto
  set_alive(bool alive) const& noexcept -> ExtendedState;

  /*! \brief set alive status */
  [[nodiscard]] auto
  set_alive(bool alive) && noexcept -> ExtendedState;

  /*! \brief current subarrays */
  [[nodiscard]] auto
  subarrays() const -> std::vector<std::tuple<subarray_name_t, subarray_t>>;

  /*! \brief current subarrays */
  [[nodiscard]] auto
  subarray_names() const -> std::vector<subarray_name_t>;

  /*! \brief find a subarray slot associated with a given name */
  [[nodiscard]] auto
  find_subarray(subarray_name_t const& subarray_name) const
    -> std::optional<unsigned>;

  /*! \brief compare subarray names and slots with another state */
  [[nodiscard]] auto
  has_same_subarrays(ExtendedState const& st) const -> bool;

  /*! \brief set a subarray in a subarray slot */
  [[nodiscard]] auto
  set_subarray(
    unsigned slot,
    subarray_name_t const& subarray_name,
    subarray_t const& subarray) const& -> ExtendedState;

  /*! \brief set a subarray in a subarray slot */
  [[nodiscard]] auto
  set_subarray(
    unsigned slot,
    subarray_name_t const& subarray_name,
    subarray_t const& subarray) && -> ExtendedState;

  /*! \brief remove a subarray in a slot */
  auto
  reset_subarray(unsigned slot) const& -> ExtendedState;

  /*! \brief remove a subarray in a slot */
  auto
  reset_subarray(unsigned slot) && -> ExtendedState;

  /*! \brief get subarray by slot index */
  [[nodiscard]] auto
  get_subarray(unsigned slot) const& -> std::optional<subarray_t const*>;

  /*! \brief set index partition of baselines by subarrays */
  [[nodiscard]] auto
  set_subarray_partition(
    L::IndexPartitionT<1, int> partition) const& -> ExtendedState;

  /*! \brief set index partition of baselines by subarrays */
  [[nodiscard]] auto
  set_subarray_partition(
    L::IndexPartitionT<1, int> partition) && -> ExtendedState;

  /*! \brief set imaging id for a subarray */
  [[nodiscard]] auto
  set_imaging(unsigned slot, img_id_t const& id) const& -> ExtendedState;

  /*! \brief set imaging id for a subarray */
  [[nodiscard]] auto
  set_imaging(unsigned slot, img_id_t const& id) && -> ExtendedState;

  /*! \brief reset imaging id for a subarray */
  [[nodiscard]] auto
  reset_imaging(unsigned slot) const& -> ExtendedState;

  /*! \brief reset imaging id for a subarray */
  [[nodiscard]] auto
  reset_imaging(unsigned slot) && -> ExtendedState;

  /*! \brief set filepath for saving event stream
   *
   * \throw RecordEventStreamFilepathError for inability to create file, or path
   * is not absolute
   *
   * Stops current recording regardless of whether exception is thrown.
   */
  [[nodiscard]] auto
  set_record_event_stream(unsigned slot, std::filesystem::path const& filepath)
    const& -> ExtendedState;

  /*! \brief set filepath for saving event stream
   *
   * \throw RecordEventStreamFilepathError for inability to create file, or path
   * is not absolute
   *
   * Stops current recording regardless of whether exception is thrown.
   */
  [[nodiscard]] auto
  set_record_event_stream(
    unsigned slot, std::filesystem::path const& filepath) && -> ExtendedState;

  /*! \brief stop recording the event stream */
  [[nodiscard]] auto
  reset_record_event_stream(unsigned slot) const& -> ExtendedState;

  /*! \brief stop recording the event stream */
  [[nodiscard]] auto
  reset_record_event_stream(unsigned slot) && -> ExtendedState;

  /*! \brief record an event */
  auto
  record_event(StreamStateEventBase<ExtendedState> const* event) -> void;

  /*! \brief set data recording status for a subarray */
  [[nodiscard]] auto
  set_record_data_stream(
    unsigned slot,
    ChannelSet const& channels,
    std::unordered_map<std::string, std::string> const& parameters,
    std::filesystem::path const& filepath) const& -> ExtendedState;

  /*! \brief set data recording status for a subarray */
  [[nodiscard]] auto
  set_record_data_stream(
    unsigned slot,
    ChannelSet const& channels,
    std::unordered_map<std::string, std::string> const& parameters,
    std::filesystem::path const& filepath) && -> ExtendedState;

  /*! \brief reset data recording for a subarray */
  [[nodiscard]] auto
  reset_record_data_stream(unsigned slot) const& -> ExtendedState;

  /*! \brief reset data recording for a subarray */
  [[nodiscard]] auto
  reset_record_data_stream(unsigned slot) && -> ExtendedState;

  /*! \brief set pre-calibration flagging state for subarray */
  [[nodiscard]] auto
  set_pre_calibration_flagging(
    unsigned slot, bool state) const& -> ExtendedState;

  /*! \brief set pre-calibration flagging state for subarray */
  [[nodiscard]] auto
  set_pre_calibration_flagging(unsigned slot, bool state) && -> ExtendedState;

  /*! \brief set fftw wisdom import path */
  [[nodiscard]] auto
  set_fftw_wisdom_import_path(
    std::filesystem::path const& path) const& -> ExtendedState;

  /*! \brief set fftw wisdom import path */
  [[nodiscard]] auto
  set_fftw_wisdom_import_path(
    std::filesystem::path const& path) && -> ExtendedState;

  /*! \brief set fftw wisdom export path */
  [[nodiscard]] auto
  set_fftw_wisdom_export_path(
    std::filesystem::path const& path) const& -> ExtendedState;

  /*! \brief set fftw wisdom export path */
  [[nodiscard]] auto
  set_fftw_wisdom_export_path(
    std::filesystem::path const& path) && -> ExtendedState;

  /*! \brief reset both fftw wisdom paths */
  [[nodiscard]] auto
  reset_fftw_wisdom_paths() const& -> ExtendedState;

  /*! \brief reset both fftw wisdom paths */
  [[nodiscard]] auto
  reset_fftw_wisdom_paths() && -> ExtendedState;

private:
  /*! \brief post-construction initialization of an instance */
  auto
  init() -> void;

  /*! \brief value representing an empty subarray slot */
  [[nodiscard]] static auto
  empty_subarray() -> std::tuple<subarray_name_t, subarray_t> const&;

  /*! \brief record an event for a given subarray */
  auto
  record_event(unsigned slot, StreamStateEventBase<ExtendedState> const* event)
    -> void;

  /*! \brief convert path to fftw wisdom file to fftwf wisdom file path
   *
   * Simply adds a single "f" character to the file name prior to the extension
   * (the stem, in c++17 parlance).
   */
  static auto
  float_wisdom_file_path(std::filesystem::path const& path)
    -> std::filesystem::path;
};

/*! \brief stream insertion for ExtendedState */
auto
operator<<(std::ostream&, ExtendedState const& st) -> std::ostream&;

/*! \brief stream insertion for State */
auto
operator<<(std::ostream&, State const& st) -> std::ostream&;

/*! \brief generic JSON value */
struct JSONValue {

  /*! \brief JSON event type tag metadata member name */
  static constexpr char const* meta_tag_name = "tag";

  /*! \brief JSON event value metadata member name */
  static constexpr char const* meta_value_name = "value";

  /*! \brief test for meta_tag_name member */
  [[nodiscard]] static auto
  has_tag(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get tag */
  [[nodiscard]] static auto
  get_tag(nlohmann::json const& js) -> std::string;

  /*! \brief test for meta_value_name member */
  [[nodiscard]] static auto
  has_value(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get value */
  [[nodiscard]] static auto
  get_value(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get value */
  [[nodiscard]] static auto
  get_value(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief test for valid format */
  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

/*! \brief base class for JSON parsing errors of events and event scripts */
struct JSONError : std::runtime_error {
  JSONError(char const* msg) : std::runtime_error(msg) {}
  JSONError(std::string const& msg) : std::runtime_error(msg) {}
};

/*! \brief error representing malformed JSON format for event type */
struct JSONValueFormatError : public JSONError {

  /*! \brief constructor
   *
   * \param lacks_tag JSON lacks 'tag' member
   * \param lacks_value JSON lacks 'value' member
   */
  JSONValueFormatError(nlohmann::json const& js)
    : JSONError(
        std::string("JSON event is missing member(s) ")
        + (!JSONValue::has_tag(js) ? JSONValue::meta_tag_name : "")
        + (!JSONValue::has_tag(js) && !JSONValue::has_value(js) ? " and " : "")
        + (!JSONValue::has_value(js) ? JSONValue::meta_value_name : "")) {}
};

struct RecordEventStreamFilepathError : public JSONError {
  RecordEventStreamFilepathError(std::filesystem::path const& filepath)
    : JSONError(
        "Unable to open " + filepath.string() + " for recording event stream") {
  }
};

struct FilepathTooLongError : public JSONError {
  FilepathTooLongError(std::filesystem::path const& filepath)
    : JSONError("File path '" + filepath.string() + "' is too long") {}
};

struct ParametersTooLongError : public JSONError {
  ParametersTooLongError(std::string const& params)
    : JSONError(
        "Serialized output parameters dictionary '" + params
        + "' is too long") {}
};

using EventLog = StreamEventLog<ExtendedState>;

template <typename Event>
auto
get_event(nlohmann::json const& js) -> std::unique_ptr<Event> {

  Event result;
  from_json(js, result);
  return std::make_unique<Event>(std::move(result));
}

auto
get_event(nlohmann::json const& event_js)
  -> std::unique_ptr<StreamStateEventBase<ExtendedState>>;

auto
as_json(StreamStateEventBase<ExtendedState> const& event) -> nlohmann::json;

} // end namespace rcp::symcam::st

namespace rcp::symcam {
extern template class StreamEventLog<st::ExtendedState>;
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
