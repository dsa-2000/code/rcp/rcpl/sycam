// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "StaticFields.hpp"
#include "StreamState.hpp"
#include <rcp/rcp.hpp>

namespace rcp::symcam::st {

/*! \brief event log region axes
 *
 * The event log exists on each shard, but to avoid inconsistencies all shards
 * should defer to the instance at index 0 when the log is read.
 */
enum class EventLogAxes { shard = LEGION_DIM_0 };

/*! \brief pointer values for access to an event log in tasks */
struct EventLogPtrsBlock {
  /*! \brief pointer to event log instance */
  EventLog* log;
  /*! \brief pointer to mutex for access to event log instance */
  std::mutex* mtx;
};

using EventLogPtrsBlockField =
  StaticField<EventLogPtrsBlock, field_ids::event_log>;

/*! \brief event log region definition */
using EventLogRegion = StaticRegionSpec<
  IndexSpec<EventLogAxes, EventLogAxes::shard, EventLogAxes::shard, int>,
  std::array{IndexInterval<int>::left_bounded(0)},
  EventLogPtrsBlockField>;

} // end namespace rcp::symcam::st

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
