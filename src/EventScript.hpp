// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "StreamState.hpp"
#include "libsymcam.hpp"

#include <bits/iterator_concepts.h>
#include <memory>
#include <optional>
#include <ranges>
#include <stdexcept>
#include <utility>

#include <nlohmann/json.hpp>

/*! \file EventScript.hpp
 *
 * Event script for symcam.
 */

namespace rcp::symcam::st {

/*! \brief error representing script "count" value that is < 1 */
struct CountTooSmallError : public JSONError {
  CountTooSmallError()
    : JSONError("'count' value of script element is less than 1") {}
};

/*! \brief error representing script "period" value that is == 0 */
struct ZeroPeriodError : public JSONError {
  ZeroPeriodError()
    : JSONError("'period' value of script element is equal to 0") {}
};

/*! \brief error representing no top-level "event script" tag */
struct NotEventScriptError : public JSONError {
  NotEventScriptError()
    : JSONError("Top-level 'event script' tag is missing") {}
};

/*! \brief iterator class for EventScript values */
class EventScriptIterator {
public:
  using difference_type = int;       /*!< iterator difference type */
  using value_type = nlohmann::json; /*< iterator value type */

private:
  /*! \brief script
   *
   * This value is the same as that of the original EventScript used
   * to construct the instance.
   */
  std::shared_ptr<nlohmann::json const> script;

  /*! \brief flag used to indicate the EventScriptIterator end value */
  bool at_end{true};

  /*! \brief current script block index value */
  unsigned current_block_index{0};

  /*! \brief current script block repetition count value */
  unsigned current_block_current_count{0};

  /*! \brief current script block maximum repetition count value */
  unsigned current_block_max_count{0};

  /*! \brief current script block period value */
  StreamClock::duration current_block_period{0};

  /*! \breif current event index within current script block */
  unsigned current_block_event_index{0};

  /*! \brief get maximum repetition count of a script block
   *
   * \return 0, if block is absent; 1, if block "count" value is
   * absent; otherwise, value of block "count"
   */
  static auto
  block_max_count(std::optional<nlohmann::json const*> const& maybe_blockp)
    -> unsigned;

  /*! \brief get period of a script block
   *
   * \return 0, if block is absent or block "period" value is
   * absent; otherwise, value of block "period"
   */
  static auto
  block_period(std::optional<nlohmann::json const*> const& maybe_blockp)
    -> StreamClock::duration;

  /*! \brief get pointer to current script block */
  [[nodiscard]] auto
  current_block() const -> std::optional<nlohmann::json const*>;

public:
  /*! \brief default constructor (defaulted) */
  EventScriptIterator() = default;

  /*! \brief construct from nlohmann::json form of an event
   *  script */
  EventScriptIterator(std::shared_ptr<nlohmann::json const> const& js);

  // EventScriptIterator(EventScriptIterator const&) = default;

  // EventScriptIterator(EventScriptIterator&&) = default;

  /*! \brief pre-increment operator */
  auto
  operator++() -> EventScriptIterator&;

  /*! \brief post-increment operator */
  auto
  operator++(int) -> EventScriptIterator;

  /*! \brief dereference operator
   *
   * This always returns a nlohmann::json value, not a reference to
   * a value.
   */
  auto
  operator*() const -> nlohmann::json;

  /*! \brief equality operator */
  auto
  operator==(EventScriptIterator const& rhs) const -> bool;

private:
  /*! \brief validate the current block */
  auto
  validate_current_block() -> void;
};

static_assert(std::input_or_output_iterator<EventScriptIterator>);
static_assert(std::sentinel_for<EventScriptIterator, EventScriptIterator>);

/*! \brief JSON-encoded event script
 *
 * Example:
 *
 * { "event script":
 *    [{"events":
 *      [{ "tag": "StartImaging", "value": {"timestamp": xxxx}},
 *       { "tag": "StopImaging", "value": {"timestamp": yyyy}}],
 *      "count": 2
 *      "period": zzzz
 *     },
 *     {"events":
 *       [{ "tag": "PoisonPill", "value": {"timestamp": qqqq}}]}]
 * }
 *
 * Durations may be represented by any string that can be parsed as an
 * integer or floating point number immediately followed by a suffix
 * in the set ("h", "min", "s", "ms", "us", "ns"). Relative timestamps
 * may be represented by a duration value immediately preceded by a
 * "Δ". Fixed (external) timestamps may be represented by a duration
 * since the epoch of the StreamClock.
 */
class EventScript : public std::ranges::view_base {

  /*! \brief nlohmann::json form of an event script */
  std::shared_ptr<nlohmann::json> m_json;

  /*! \brief number of events in unrolled script */
  unsigned m_num_events{0};

public:
  /*! \brief top-level name of an event script element */
  static constexpr auto script_tag = "event script";
  /*! \brief name of element with vector of events */
  static constexpr auto block_events_tag = "events";
  /*! \brief name of element for repetition count of events in the
   *  same object */
  static constexpr auto block_count_tag = "count";
  /*! \brief name of element for period associated with repetition of
   *  event in the same object */
  static constexpr auto block_period_tag = "period";
  /*! \brief default constructor (defaulted) */
  EventScript() = default;

  /*! \brief constructor from json value
   *
   * \todo this does a pass over the complete script so that parsing
   * errors are caught on construction...is this OK?
   */
  EventScript(nlohmann::json js);

  /*! \brief get begin EventScriptIterator */
  [[nodiscard]] auto
  begin() const -> EventScriptIterator;

  /*! \brief get end EventScriptIterator */
  static auto
  end() noexcept -> EventScriptIterator;

  /*! \brief get number of events in unrolled event script */
  [[nodiscard]] auto
  size() const -> unsigned;

  [[nodiscard]] auto
  as_json() const -> nlohmann::json const&;

  [[nodiscard]] auto
  with_fixed_epoch(
    StreamClock::external_time const& epoch) const& -> EventScript;

  [[nodiscard]] auto
  with_fixed_epoch(StreamClock::external_time const& epoch) && -> EventScript;

  [[nodiscard]] auto
  partition_relative_past_events() const
    -> std::tuple<EventScript, EventScript>;
};

} // end namespace rcp::symcam::st

/*! \brief rcp::symcam::st::EventScript satisfies the
 *  borrowed_range concept */
template <>
inline constexpr bool
  std::ranges::enable_borrowed_range<rcp::symcam::st::EventScript> = true;

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
