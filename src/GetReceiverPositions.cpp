// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "GetReceiverPositions.hpp"

#include <random>
#include <rcp/rcp.hpp>

namespace rcp::symcam {

/*! \brief task to get receiver (antenna) position coordinates
 */
struct GetReceiverPositionsTask
  : public SerialOnlyTaskMixin<GetReceiverPositionsTask, void> {

  /*! \brief task name */
  static constexpr char const* task_name = "GetReceiverPositions";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief region indexes */
  enum { positions_region_index = 0, num_regions };

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> result_t {

    using rcv_t = ReceiverRegion;

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(GetReceiverPositions::Args));
    GetReceiverPositions::Args const& args =
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      *reinterpret_cast<GetReceiverPositions::Args const*>(task->args);

    using coordval_t = PositionField::value_t::value_type;
    // NOLINTNEXTLINE(cert-msc32-c,cert-msc51-cpp,cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    std::mt19937 gen(29);
    std::uniform_real_distribution<PositionField::value_t::value_type> angle(
      coordval_t{0.0}, coordval_t{2 * std::numbers::pi});
    std::uniform_real_distribution<coordval_t> radius(
      coordval_t{0.0},
      coordval_t(std::min(args.array_extent_m[0], args.array_extent_m[1])) / 2);
    auto const& region = regions[positions_region_index];
    rcv_t::rect_t bounds = region;
    auto const positions =
      rcv_t::values<LEGION_WRITE_ONLY>(region, PositionField{});
    for (L::PointInRectIterator pir(bounds); pir(); ++pir) {
      auto ang = angle(gen);
      auto rad = radius(gen);
      positions[*pir] =
        PositionField::value_t{rad * std::cos(ang), rad * std::sin(ang), 0};
    }
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID GetReceiverPositionsTask::task_id;

GetReceiverPositions::GetReceiverPositions(Configuration const& config)
  : m_args(std::make_shared<Args>(config.array_extent_m))
  , m_launcher(
      GetReceiverPositionsTask::task_id,
      L::UntypedBuffer(m_args.get(), sizeof(Args))) {}

auto
GetReceiverPositions::launch(L::Context ctx, L::Runtime* rt)
  -> rcv_t::LogicalRegion {

  auto gen = DenseArrayRegionGenerator<ReceiverRegion>(
    ctx, rt, ReceiverRegion::dynamic_bounds_t{});
  auto result = gen.make(ctx, rt);
  gen.destroy(ctx, rt);

  m_launcher.region_requirements.resize(1);
  m_launcher
    .region_requirements[GetReceiverPositionsTask::positions_region_index] =
    result.requirement(
      LEGION_WRITE_ONLY, LEGION_EXCLUSIVE, StaticFields{PositionField{}});
  rt->execute_task(ctx, m_launcher);

  return result;
  // NOLINTEND(bugprone-unchecked-optional-access)
}

auto
GetReceiverPositions::preregister_tasks(L::TaskID tid) -> void {
  PortableTask<GetReceiverPositionsTask>::preregister_task_variants(tid);
}

auto
GetReceiverPositions::register_tasks(L::Runtime* rt, L::TaskID tid) -> void {
  PortableTask<GetReceiverPositionsTask>::register_task_variants(rt, tid);
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
