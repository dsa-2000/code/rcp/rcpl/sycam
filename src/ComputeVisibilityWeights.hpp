// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BsetChannelBaselinePolprodRegion.hpp"
#include <rcp/FSamplesRegion.hpp>

namespace rcp::symcam {

/*! launcher for task to compute visibility weights
 */
class ComputeVisibilityWeights {
public:
  using samples_t = FSamplesRegion;

  using cbp_t = BsetChannelBaselinePolprodRegion;

  using result_t = std::uint64_t;

private:
  samples_t::index_partition_t m_samples_index_partition;

  cbp_t::index_partition_t m_visibilities_index_partition;

public:
  /*! \brief non-default constructor
   *
   * \param samples_index_space index space of region with sample weights
   *        (samples_md_t::weight_fid)
   * \param visibilities_index_space index space of region with visibility
   *        weights (visibilities_md_t::weight_fid)
   */
  ComputeVisibilityWeights(
    L::Context ctx,
    L::Runtime* rt,
    samples_t::index_space_t samples_index_space,
    cbp_t::index_space_t visibilities_index_space);

  ComputeVisibilityWeights() = default;

  ComputeVisibilityWeights(ComputeVisibilityWeights const&) = default;

  ComputeVisibilityWeights(ComputeVisibilityWeights&&) = default;

  ~ComputeVisibilityWeights() = default;

  auto
  operator=(ComputeVisibilityWeights const&)
    -> ComputeVisibilityWeights& = default;

  auto
  operator=(ComputeVisibilityWeights&&) -> ComputeVisibilityWeights& = default;

  [[nodiscard]] auto
  max_weight() const -> coord_t;

  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    samples_t::LogicalRegion samples,
    cbp_t::LogicalRegion visibilities,
    L::Predicate pred = L::Predicate::TRUE_PRED) -> L::FutureMap;

  static auto
  preregister_tasks(L::TaskID task_id) -> void;

  static auto
  register_tasks(L::Runtime* rt, L::TaskID task_id) -> void;
};
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
