// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "StateRegion.hpp"

#include <array>
#include <legion.h>

namespace rcp::symcam {

/* \brief FFTWSupport region index space axes definition */
enum class FFTWSupportAxes { shard = LEGION_DIM_0 };

/* \brief FFTWSupport field definition */
using FFTWProxyField = StaticField<int, field_ids::fftw_proxy>;

/*! \brief FFTWSupport region definition
 *
 * A 1-d index space from coordinate value 0, with a single field
 * ProxyField
 */
using FFTWSupportRegion = StaticRegionSpec<
  IndexSpec<
    FFTWSupportAxes,
    FFTWSupportAxes::shard,
    FFTWSupportAxes::shard,
    int>,
  std::array{IndexInterval<int>::left_bounded(0)},
  FFTWProxyField>;

/*! launcher for task to import and export FFTW wisdom */
class FFTWSupport {
public:
  using state_t = st::StateRegion;

private:
  std::unique_ptr<FFTWProxyField::value_t> m_proxy;

  L::ExternalResources m_ext_resources;

  FFTWSupportRegion::LogicalRegion m_support_lr;

  FFTWSupportRegion::LogicalPartition m_support_lp;

public:
  FFTWSupport(
    L::Context ctx,
    L::Runtime* rt,
    std::size_t shard_point,
    std::size_t num_shards);

  FFTWSupport() = default;

  FFTWSupport(FFTWSupport const&) = delete;

  FFTWSupport(FFTWSupport&&) noexcept = default;

  auto
  operator=(FFTWSupport const&) -> FFTWSupport& = delete;

  auto
  operator=(FFTWSupport&&) noexcept -> FFTWSupport& = default;

  virtual ~FFTWSupport() = default;

  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void;

  auto
  import_wisdom(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& state,
    L::Predicate pred = L::Predicate::TRUE_PRED) -> L::Future;

  auto
  export_wisdom(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& state,
    L::Predicate pred = L::Predicate::TRUE_PRED) -> L::Future;

  static auto
  preregister_tasks(std::array<L::TaskID, 2> const& task_ids) -> void;

  static auto
  register_tasks(L::Runtime* rt, std::array<L::TaskID, 2> const& task_ids)
    -> void;
};
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
