// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BaselineChannelRegion.hpp"
#include "BsetChannelBaselinePolprodRegion.hpp"
#include "StaticFields.hpp"

/*! \file DataPartitionsRegion.hpp
 */
namespace rcp::symcam {

enum class DataPartitionsAxes { subarray = LEGION_DIM_0 };

using subarray_index_spec_t = IndexSpec<
  DataPartitionsAxes,
  DataPartitionsAxes::subarray,
  DataPartitionsAxes::subarray,
  int>;

using VisibilitiesSubarrayPartitionField = StaticField<
  L::IndexPartitionT<3, coord_t>,
  field_ids::visibilities_subarray_partition>;

using VisibilitiesTilePartitionField = StaticField<
  L::IndexPartitionT<3, coord_t>,
  field_ids::visibilities_tile_partition>;

using UVWsSubarrayPartitionField = StaticField<
  L::IndexPartitionT<2, coord_t>,
  field_ids::uvws_subarray_partition>;

using UVWsImagePartitionField =
  StaticField<L::IndexPartitionT<2, coord_t>, field_ids::uvws_image_partition>;

using UVWsTilePartitionField =
  StaticField<L::IndexPartitionT<2, coord_t>, field_ids::uvws_tile_partition>;

using DataPartitionsRegion = StaticRegionSpec<
  subarray_index_spec_t,
  std::array{
    IndexInterval<int>::bounded(0, Configuration::max_num_subarrays - 1)},
  VisibilitiesSubarrayPartitionField,
  UVWsSubarrayPartitionField,
  VisibilitiesTilePartitionField,
  UVWsImagePartitionField,
  UVWsTilePartitionField>;

/*! \brief get visibilities partition by subarray
 *
 * \param ctx Legion context
 * \param rt Legion runtime
 * \param region LogicalRegion instance for DataPartitionsRegion
 *
 * NOTE: this function maps the region using an inline mapping, and will
 * therefore block
 */
auto
get_visibilities_subarray_partition(
  L::Context ctx, L::Runtime* rt, DataPartitionsRegion::LogicalRegion region)
  -> VisibilitiesSubarrayPartitionField::value_t;

/*! \brief get visibilities subspace for given subarray */
auto
get_visibilities_subarray_subspace(
  L::Runtime* rt,
  VisibilitiesSubarrayPartitionField::value_t partition,
  unsigned subarray) -> BsetChannelBaselinePolprodRegion::index_space_t;

/*! \brief test for empty visibilities subspace for given subarray */
auto
is_visibilities_subarray_subspace_empty(
  L::Runtime* rt,
  VisibilitiesSubarrayPartitionField::value_t partition,
  unsigned subarray) -> bool;

/*! \brief get visibilities subregion for given subarray*/
auto
get_visibilities_subarray_subregion(
  L::Runtime* rt,
  RegionPair<BsetChannelBaselinePolprodRegion::LogicalRegion> const& region,
  VisibilitiesSubarrayPartitionField::value_t partition,
  unsigned subarray)
  -> RegionPair<BsetChannelBaselinePolprodRegion::LogicalRegion>;

/*! \brief get subregion of a DataPartitionsRegion::LogicalRegion
 * instance for a given subarray */
auto
get_partitions_region_for_subarray(
  L::Runtime* rt,
  PartitionPair<DataPartitionsRegion::LogicalPartition> partitions,
  unsigned subarray) -> RegionPair<DataPartitionsRegion::LogicalRegion>;

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
