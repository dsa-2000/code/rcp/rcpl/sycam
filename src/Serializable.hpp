// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include <legion.h>
#include <vector>

namespace rcp::symcam {

/*! \brief container of requirements for tasks using serialized task launcher
 *  classes in child tasks */
class ChildRequirements {
  std::vector<Legion::RegionRequirement> m_region_requirements;
  std::vector<Legion::OutputRequirement> m_output_requirements;
  std::vector<Legion::IndexSpaceRequirement> m_index_space_requirements;

public:
  ChildRequirements() = default;
  ChildRequirements(
    std::vector<Legion::RegionRequirement> const& region_requirements,
    std::vector<Legion::OutputRequirement> const& output_requirements,
    std::vector<Legion::IndexSpaceRequirement> const& index_space_requirements);
  ChildRequirements(
    std::vector<Legion::RegionRequirement>&& region_requirements,
    std::vector<Legion::OutputRequirement>&& output_requirements,
    std::vector<Legion::IndexSpaceRequirement>&&
      index_space_requirements) noexcept;
  ChildRequirements(ChildRequirements const&) = default;
  ChildRequirements(ChildRequirements&&) noexcept = default;
  ~ChildRequirements() = default;
  auto
  operator=(ChildRequirements const&) -> ChildRequirements& = default;
  auto
  operator=(ChildRequirements&&) noexcept -> ChildRequirements& = default;
  auto
  operator+=(ChildRequirements const& rhs) -> ChildRequirements&;
  auto
  operator+(ChildRequirements const& other) const -> ChildRequirements;
  auto
  region_requirements() const -> std::vector<Legion::RegionRequirement> const&;
  auto
  region_requirements() -> std::vector<Legion::RegionRequirement>&;
  auto
  output_requirements() const -> std::vector<Legion::OutputRequirement> const&;
  auto
  output_requirements() -> std::vector<Legion::OutputRequirement>&;
  auto
  index_space_requirements() const
    -> std::vector<Legion::IndexSpaceRequirement> const&;
  auto
  index_space_requirements() -> std::vector<Legion::IndexSpaceRequirement>&;
};

/*! \brief interface requirements for serializable task launcher classes */
template <typename T>
concept Serializable =
  requires { typename T::Serialized; }
  && std::is_trivially_copyable_v<typename T::Serialized>
  && requires(
    Legion::Context ctx,
    Legion::Runtime* rt,
    typename T::Serialized const& serialized) { T(ctx, rt, serialized); }
  && requires(Legion::Context ctx, Legion::Runtime* rt, T const& tval) {
       { tval.serialized(ctx, rt) } -> std::same_as<typename T::Serialized>;
       { tval.child_requirements(ctx, rt) } -> std::same_as<ChildRequirements>;
     };
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
