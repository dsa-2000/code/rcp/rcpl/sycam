// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "ImagingEvent.hpp"

namespace rcp::symcam::st {

auto
JSONImagingEvent::has_imaging_id(nlohmann::json const& js) noexcept -> bool {
  return js.contains(imaging_id_tag);
}

auto
JSONImagingEvent::get_imaging_id(nlohmann::json const& js)
  -> nlohmann::json const& {
  return js.at(imaging_id_tag);
}

auto
JSONImagingEvent::get_imaging_id(nlohmann::json& js) -> nlohmann::json& {
  return js.at(imaging_id_tag);
}

auto
JSONImagingEvent::is_valid(nlohmann::json const& js) noexcept -> bool {
  return JSONSubarrayEvent::is_valid(js) && has_imaging_id(js)
         && js.at(imaging_id_tag).is_number_integer()
         && get_imaging_id(js).get<int>() >= 0;
}

auto
StartImaging::apply_to_no_ts(ExtendedState&& st) const -> ExtendedState {
  if (st.is_alive()) {
    auto maybe_slot = st.find_subarray(name());
    if (!maybe_slot)
      throw InvalidSubarrayName(name());
    st = std::move(st).set_imaging(*maybe_slot, imaging_id());
  }
  return st;
}

auto
StopImaging::apply_to_no_ts(ExtendedState&& st) const -> ExtendedState {
  if (st.is_alive()) {
    auto maybe_slot = st.find_subarray(name());
    if (!maybe_slot)
      throw InvalidSubarrayName(name());
    st = std::move(st).reset_imaging(*maybe_slot);
  }
  return st;
}

} // end namespace rcp::symcam::st

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
