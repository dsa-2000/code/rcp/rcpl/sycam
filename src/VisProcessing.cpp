// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "VisProcessing.hpp"
#include "BaselineChannelRegion.hpp"
#include "BaselineRegion.hpp"
#include "BsetChannelBaselinePolprodRegion.hpp"
#include "CFRegion.hpp"
#include "FlagVisibilities.hpp"
#include "GetCFValues.hpp"
#include "RecordVisibilities.hpp"
#include "StateRegion.hpp"

#include <algorithm>
#include <default_mapper.h>
#include <rcp/rcp.hpp>
#include <type_traits>
#include <variant>
#include <vector>

namespace rcp::symcam {

/*! \brief task for visibility processing
 */
struct VisProcessingTask : public SerialOnlyTaskMixin<VisProcessingTask, void> {

  using bc_t = BaselineChannelRegion;
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using cf_t = CFRegion;
  using state_t = st::StateRegion;

  /*! \brief task name */
  static constexpr char const* task_name = "VisProcessing";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  enum { visibilities_region_index = 0, uvws_region_index, num_fixed_regions };

  struct Args {
    VisProcessing::Serialized vis_processing_state;
    /*! \brief previous state region index */
    int prev_state_region_index;
    /*! \brief current state region index */
    int current_state_region_index;
    /*! \brief next state region index */
    int next_state_region_index;
  };

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    assert(regions.size() >= num_fixed_regions);
    assert(task->arglen == sizeof(Args));
    Args const& args = *reinterpret_cast<Args const*>(task->args);
    auto const& vpstate = args.vis_processing_state;

    // need to create RegionPairs from the state region requirements using the
    // local regions themselves as parent regions, so be sure to use the
    // "region" member of the task RegionRequirements when constructing the
    // RegionPair values
    std::array<RegionPair<state_t::LogicalRegion>, 3> state_regions;
    // previous State, optional
    std::optional<st::State> prev_state;
    if (args.prev_state_region_index >= 0) {
      prev_state = state_t::values<LEGION_READ_ONLY>(
        regions[args.prev_state_region_index], st::StateField{})[0];
      state_regions[0] = task->regions[args.prev_state_region_index].region;
    }
    // current State
    assert(args.current_state_region_index >= 0);
    auto current_state = state_t::values<LEGION_READ_ONLY>(
      regions[args.current_state_region_index], st::StateField{})[0];
    state_regions[1] = task->regions[args.current_state_region_index].region;

    // next State, optional
    std::optional<st::State> next_state;
    if (args.next_state_region_index >= 0) {
      next_state = state_t::values<LEGION_READ_ONLY>(
        regions[args.next_state_region_index], st::StateField{})[0];
      state_regions[2] = task->regions[args.next_state_region_index].region;
    }

    auto state_query =
      st::AllSubarraysQuery{prev_state, current_state, next_state};
    if (!state_query.is_alive())
      return;

    // we're done with the States regions, and all other regions are virtually
    // mapped
    rt->unmap_all_regions(ctx);

    // deserialize vpstate members (TODO: do as needed, not
    // all up front)
    std::vector<sarray<GridFourierTransform::Plan>> fft_plans;
    fft_plans.reserve(vpstate.m_num_imaging_configurations);
    std::ranges::transform(
      std::views::take(
        vpstate.m_fft_plans, vpstate.m_num_imaging_configurations),
      std::back_inserter(fft_plans),
      [&](auto&& img_plans) {
        sarray<GridFourierTransform::Plan> result;
        std::ranges::transform(
          img_plans, result.begin(), [&](auto&& serialized) {
            return GridFourierTransform::Plan(ctx, rt, serialized);
          });
        return result;
      });

    VisProcessing(ctx, rt, vpstate)
      .run(
        ctx,
        rt,
        task->regions[visibilities_region_index],
        task->regions[uvws_region_index],
        state_regions,
        state_query);
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID VisProcessingTask::task_id;

VisProcessing::VisProcessing(
  L::Context ctx,
  L::Runtime* rt,
  BaselineRegion::LogicalRegion baselines,
  std::shared_ptr<std::vector<sarray<GridFourierTransform::Plan>>> const&
    fft_plans,
  std::shared_ptr<ComputeVisibilitiesPartitions> const&
    compute_visibilities_partitions,
  std::shared_ptr<FlagVisibilitiesStub> const& flag_visibilities,
  std::shared_ptr<RecordVisibilities> const& record_visibilities,
  std::shared_ptr<std::vector<ExportGrid>> const& export_grid,
  std::shared_ptr<std::vector<GetCFValues>> const& get_cf_values,
  std::shared_ptr<std::vector<GridVisibilitiesOnImage>> const&
    grid_visibilities)
  : m_baselines{baselines}
  , m_fft_plans{fft_plans}
  , m_compute_visibilities_partitions{compute_visibilities_partitions}
  , m_flag_visibilities{flag_visibilities}
  , m_record_visibilities{record_visibilities}
  , m_export_grid{export_grid}
  , m_get_cf_values{get_cf_values}
  , m_grid_visibilities{grid_visibilities} {

  init_child_requirements(ctx, rt);
}

VisProcessing::VisProcessing(
  L::Context ctx,
  L::Runtime* rt,
  Serialized const& serialized,
  bool no_child_requirements) {

  m_baselines = serialized.m_baselines;
  m_fft_plans =
    std::make_shared<std::remove_reference_t<decltype(*m_fft_plans)>>();
  m_fft_plans->reserve(serialized.m_num_imaging_configurations);
  std::ranges::transform(
    std::views::take(
      serialized.m_fft_plans, serialized.m_num_imaging_configurations),
    std::back_inserter(*m_fft_plans),
    [&](auto&& img_plans) {
      sarray<GridFourierTransform::Plan> plans;
      std::ranges::transform(
        img_plans, plans.begin(), [&](auto&& serialized_plan) {
          return GridFourierTransform::Plan(ctx, rt, serialized_plan);
        });
      return plans;
    });

  m_compute_visibilities_partitions =
    std::make_shared<ComputeVisibilitiesPartitions>(
      ctx, rt, serialized.m_compute_visibilities_partitions);
  m_flag_visibilities = std::make_shared<FlagVisibilitiesStub>(
    ctx, rt, serialized.m_flag_visibilities);
  m_record_visibilities = std::make_shared<RecordVisibilities>(
    ctx, rt, serialized.m_record_visibilities);

  m_export_grid =
    std::make_shared<std::remove_reference_t<decltype(*m_export_grid)>>();
  m_export_grid->reserve(serialized.m_num_imaging_configurations);
  std::ranges::transform(
    std::views::take(
      serialized.m_export_grid, serialized.m_num_imaging_configurations),
    std::back_inserter(*m_export_grid),
    [&](auto&& eg) { return ExportGrid(ctx, rt, eg); });

  m_get_cf_values =
    std::make_shared<std::remove_reference_t<decltype(*m_get_cf_values)>>();
  m_get_cf_values->reserve(serialized.m_num_imaging_configurations);
  std::ranges::transform(
    std::views::take(
      serialized.m_get_cf_values, serialized.m_num_imaging_configurations),
    std::back_inserter(*m_get_cf_values),
    [&](auto&& gcfv) { return GetCFValues(ctx, rt, gcfv); });

  m_grid_visibilities =
    std::make_shared<std::remove_reference_t<decltype(*m_grid_visibilities)>>();
  m_grid_visibilities->reserve(serialized.m_num_imaging_configurations);
  std::ranges::transform(
    std::views::take(
      serialized.m_grid_visibilities, serialized.m_num_imaging_configurations),
    std::back_inserter(*m_grid_visibilities),
    [&](auto&& gv) { return GridVisibilitiesOnImage(ctx, rt, gv); });

  if (!no_child_requirements)
    init_child_requirements(ctx, rt);
}

auto
VisProcessing::init_child_requirements(L::Context ctx, L::Runtime* rt) -> void {

  m_child_requirements = {
    {m_baselines.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{BaselineField{}},
      std::nullopt,
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP)},
    {},
    {}};

  assert(m_fft_plans->size() <= Configuration::max_num_imaging_configurations);
  auto usable_fft_plans = std::views::take(
    *m_fft_plans, Configuration::max_num_imaging_configurations);
  for (auto&& img_plans : usable_fft_plans)
    for (auto&& plan : img_plans)
      m_child_requirements += plan.child_requirements(ctx, rt);

  m_child_requirements +=
    m_compute_visibilities_partitions->child_requirements(ctx, rt);
  m_child_requirements += m_flag_visibilities->child_requirements(ctx, rt);
  m_child_requirements += m_record_visibilities->child_requirements(ctx, rt);

  assert(m_export_grid->size() == m_fft_plans->size());
  for (auto&& eg : *m_export_grid)
    m_child_requirements += eg.child_requirements(ctx, rt);

  assert(m_get_cf_values->size() == m_fft_plans->size());
  for (auto&& gcf : *m_get_cf_values)
    m_child_requirements += gcf.child_requirements(ctx, rt);

  assert(m_grid_visibilities->size() == m_fft_plans->size());
  for (auto&& gv : *m_grid_visibilities)
    m_child_requirements += gv.child_requirements(ctx, rt);

  // we don't support output requirements or index space requirements (yet)
  assert(m_child_requirements.output_requirements().empty());
  assert(m_child_requirements.index_space_requirements().empty());

  m_child_requirements = requirements_min_upper_bound(m_child_requirements);
  assert(std::ranges::all_of(
    m_child_requirements.region_requirements(), [](auto&& req) {
      return (req.tag & L::Mapping::DefaultMapper::VIRTUAL_MAP) != 0;
    }));
}

auto
VisProcessing::launch(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<cbp_t::LogicalRegion> visibilities,
  RegionPair<bc_t::LogicalRegion> uvws,
  std::array<RegionPair<state_t::LogicalRegion>, 3> stream_states,
  st::subarray_predicates_array_t const& predicates,
  L::Predicate is_alive) -> void {

  run(ctx, rt, visibilities, uvws, stream_states, predicates, is_alive);
}

auto
VisProcessing::launch(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<cbp_t::LogicalRegion> visibilities,
  RegionPair<bc_t::LogicalRegion> uvws,
  std::array<RegionPair<state_t::LogicalRegion>, 3> stream_states,
  L::Predicate is_alive) -> void {

  VisProcessingTask::Args args{
    .vis_processing_state = serialized(ctx, rt),
    .prev_state_region_index = -1,
    .current_state_region_index = -1,
    .next_state_region_index = -1};
  auto launcher = L::TaskLauncher(
    VisProcessingTask::task_id,
    L::UntypedBuffer(&args, sizeof(args)),
    is_alive);
  launcher.region_requirements.resize(VisProcessingTask::num_fixed_regions);
  launcher.region_requirements[VisProcessingTask::visibilities_region_index] =
    visibilities.requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{VisibilityField{}, WeightField{}},
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);
  launcher.region_requirements[VisProcessingTask::uvws_region_index] =
    uvws.requirement(
      LEGION_WRITE_DISCARD,
      LEGION_EXCLUSIVE,
      StaticFields{UVWField{}, CFIndexField{}},
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);

  assert(stream_states[1].region() != state_t::LogicalRegion{});
  auto state0_select = st::State0Select(ctx, rt, stream_states[1]);
  if (stream_states[0].region() != state_t::LogicalRegion{}) {
    args.prev_state_region_index =
      static_cast<int>(launcher.region_requirements.size());
    launcher.add_region_requirement(
      state0_select(stream_states[0])
        .requirement(
          LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}}));
  }
  args.current_state_region_index =
    static_cast<int>(launcher.region_requirements.size());
  launcher.add_region_requirement(
    state0_select(stream_states[1])
      .requirement(
        LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}}));
  if (stream_states[2].region() != state_t::LogicalRegion{}) {
    args.next_state_region_index =
      static_cast<int>(launcher.region_requirements.size());
    launcher.add_region_requirement(
      state0_select(stream_states[2])
        .requirement(
          LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}}));
  }
  std::ranges::copy(
    m_child_requirements.region_requirements(),
    std::back_inserter(launcher.region_requirements));
  rt->execute_task(ctx, launcher);
}

auto
VisProcessing::preregister_tasks(L::TaskID tid) -> void {
  PortableTask<VisProcessingTask>::preregister_task_variants(tid);
}

auto
VisProcessing::register_tasks(L::Runtime* rt, L::TaskID tid) -> void {
  PortableTask<VisProcessingTask>::register_task_variants(rt, tid);
}

auto
VisProcessing::requirements_min_upper_bound(
  ChildRequirements const& requirements) noexcept -> ChildRequirements {

  assert(requirements.output_requirements().empty());
  assert(requirements.index_space_requirements().empty());

  ChildRequirements result{requirements};
  auto& out_region_requirements = result.region_requirements();
  for (auto&& in_req : requirements.region_requirements()) {
    auto matches =
      std::views::filter(out_region_requirements, [&](auto&& out_req) {
        return out_req.region == in_req.region
               && out_req.parent == in_req.parent
               && std::ranges::any_of(in_req.privilege_fields, [&](auto&& fid) {
                    return out_req.privilege_fields.contains(fid);
                  });
      });
    auto new_req = in_req;
    if (!matches.empty()) {
      for (auto&& out_req : matches) {
        for (auto&& fid : new_req.privilege_fields) {
          out_req.privilege_fields.erase(fid);
          std::erase(out_req.instance_fields, fid);
        }
        new_req.privilege =
          L::PrivilegeMode{new_req.privilege | out_req.privilege};
      }
      std::erase_if(out_region_requirements, [](auto&& req) {
        return req.privilege_fields.empty();
      });
    }
    out_region_requirements.emplace_back(std::move(new_req));
  }
  return result;
}

auto
VisProcessing::serialized(L::Context ctx, L::Runtime* rt) const -> Serialized {

  Serialized result;

  result.m_baselines = m_baselines;
  result.m_num_imaging_configurations = m_fft_plans->size();

  auto usable_fft_plans = std::views::take(
    *m_fft_plans, Configuration::max_num_imaging_configurations);
  std::ranges::transform(
    usable_fft_plans, result.m_fft_plans.begin(), [&](auto&& img_plans) {
      std::array<
        GridFourierTransform::Plan::Serialized,
        Configuration::max_num_subarrays>
        result;
      std::ranges::transform(img_plans, result.begin(), [&](auto&& plan) {
        return plan.serialized(ctx, rt);
      });
      return result;
    });

  result.m_compute_visibilities_partitions =
    m_compute_visibilities_partitions->serialized(ctx, rt);

  result.m_flag_visibilities = m_flag_visibilities->serialized(ctx, rt);

  result.m_record_visibilities = m_record_visibilities->serialized(ctx, rt);

  auto serialize = [&](auto&& element) { return element.serialized(ctx, rt); };
  std::ranges::transform(
    *m_export_grid, result.m_export_grid.begin(), serialize);
  std::ranges::transform(
    *m_get_cf_values, result.m_get_cf_values.begin(), serialize);
  std::ranges::transform(
    *m_grid_visibilities, result.m_grid_visibilities.begin(), serialize);

  return result;
}

auto
VisProcessing::run(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<cbp_t::LogicalRegion> visibilities,
  RegionPair<bc_t::LogicalRegion> uvws,
  std::array<RegionPair<state_t::LogicalRegion>, 3> stream_states,
  std::variant<st::subarray_predicates_array_t, st::AllSubarraysQuery> const&
    predicates,
  L::Predicate is_alive) -> void {

  auto body = [&, this](auto const& pred_or_query) -> void {
    if constexpr (std::is_same_v<
                    std::remove_cvref_t<decltype(pred_or_query)>,
                    st::AllSubarraysQuery>) {
      if (!pred_or_query.is_alive())
        return;
    }
    m_compute_visibilities_partitions->update_by_subarray(
      ctx, rt, stream_states, uvws, pred_or_query);
    auto partitions_by_subarray =
      m_compute_visibilities_partitions->partitions_by_subarray();
    m_flag_visibilities->launch(
      ctx, rt, partitions_by_subarray, visibilities, pred_or_query);
    m_record_visibilities->launch(
      ctx,
      rt,
      partitions_by_subarray,
      visibilities,
      stream_states,
      pred_or_query);
    for (auto&& imager : *m_grid_visibilities) {
      imager.next_grid_and_weights(ctx, rt, pred_or_query);
      m_compute_visibilities_partitions->update_by_tile(
        ctx,
        rt,
        imager.imaging_configuration(),
        imager.tile_bounds(),
        imager.grid_partitions(),
        visibilities,
        uvws,
        pred_or_query);
      auto img_idx = std::distance(m_grid_visibilities->data(), &imager);
      auto cf_arrays =
        m_get_cf_values->at(img_idx).launch(ctx, rt, uvws, is_alive);
      imager.launch(
        ctx,
        rt,
        stream_states[1],
        partitions_by_subarray,
        visibilities,
        m_flag_visibilities->weights(),
        uvws,
        cf_arrays,
        pred_or_query);
      GridFourierTransform::launch(
        ctx, rt, m_fft_plans->at(img_idx), pred_or_query);
      m_export_grid->at(img_idx).launch(
        ctx,
        rt,
        imager.grid_and_weight_metadata(),
        imager.grids(),
        pred_or_query);
    }
  };
  std::visit(body, predicates);
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
