// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "DataPartitionsRegion.hpp"

namespace rcp::symcam {

auto
get_visibilities_subarray_partition(
  L::Context ctx, L::Runtime* rt, DataPartitionsRegion::LogicalRegion region)
  -> VisibilitiesSubarrayPartitionField::value_t {

  auto pr = rt->map_region(
    ctx,
    region.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{VisibilitiesSubarrayPartitionField{}}));
  auto result = DataPartitionsRegion::values<LEGION_READ_ONLY>(
    pr, VisibilitiesSubarrayPartitionField{})[0];
  rt->unmap_region(ctx, pr);
  return result;
}

auto
get_visibilities_subarray_subspace(
  L::Runtime* rt,
  VisibilitiesSubarrayPartitionField::value_t partition,
  unsigned subarray) -> BsetChannelBaselinePolprodRegion::index_space_t {

  assert(subarray < Configuration::max_num_subarrays);
  return rt->get_index_subspace(
    partition, L::Point<1, subarray_index_spec_t::coord_t>(subarray));
}

auto
is_visibilities_subarray_subspace_empty(
  L::Runtime* rt,
  VisibilitiesSubarrayPartitionField::value_t partition,
  unsigned subarray) -> bool {

  return get_visibilities_subarray_subspace(rt, partition, subarray)
         == BsetChannelBaselinePolprodRegion::index_space_t{};
}

auto
get_visibilities_subarray_subregion(
  L::Runtime* rt,
  RegionPair<BsetChannelBaselinePolprodRegion::LogicalRegion> const& region,
  VisibilitiesSubarrayPartitionField::value_t partition,
  unsigned subarray)
  -> RegionPair<BsetChannelBaselinePolprodRegion::LogicalRegion> {

  using cbp_t = BsetChannelBaselinePolprodRegion;
  assert(subarray < Configuration::max_num_subarrays);
  return RegionPair{
    cbp_t::LogicalRegion{rt->get_logical_subregion_by_color(
      rt->get_logical_partition(
        cbp_t::logical_region_t{region.region()}, partition),
      L::Point<1, subarray_index_spec_t::coord_t>(subarray))},
    region.parent()};
}

auto
get_partitions_region_for_subarray(
  L::Runtime* rt,
  PartitionPair<DataPartitionsRegion::LogicalPartition> partitions,
  unsigned subarray) -> RegionPair<DataPartitionsRegion::LogicalRegion> {

  using part_t = DataPartitionsRegion;
  assert(subarray < Configuration::max_num_subarrays);
  return RegionPair{
    part_t::LogicalRegion{rt->get_logical_subregion_by_color(
      part_t::logical_partition_t{partitions.partition()},
      L::Point<part_t::dim, part_t::coord_t>(subarray))},
    partitions.parent(rt)};
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
