// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include "StaticFields.hpp"

#include <complex>
#include <cstdint>

/*! \file ChannelBaselinePolprodAxes.hpp
 *
 * \fixme Add bset axis to BsetChannelBaselinePolprodAxes
 */

namespace rcp::symcam {

/*! \brief axes for (channel, baseline, polarization product) index */
enum class BsetChannelBaselinePolprodAxes {
  channel = LEGION_DIM_0, /*!< channel */
  baseline,               /*< baseline */
  polarization_product    /*< polarization product */
};

/*! \brief IndexSpec specialization for ChannelBaselinePolprodAxes
 *
 * \tparam Coord index coordinate value type
 */
template <typename Coord>
using bset_channel_baseline_polprod_index_spec_t = IndexSpec<
  BsetChannelBaselinePolprodAxes,
  BsetChannelBaselinePolprodAxes::channel,
  BsetChannelBaselinePolprodAxes::polarization_product,
  Coord>;

/*! \brief default static bounds for regions with BsetChannelBaselinePolprodAxes
 *
 * \tparam Coord index coordinate value type
 *
 * Bounds are bset: [0, Configuration::max_num_subarrays - 1], channel: [0,
 * Coord max]; baseline: [0, Coord max]; polarization_product: [0,
 * Configuration::num_polarization_products - 1]
 */
template <typename Coord>
constexpr auto default_bset_channel_baseline_polprod_bounds = std::array{
  IndexInterval<Coord>::left_bounded(0),
  IndexInterval<Coord>::left_bounded(0),
  IndexInterval<Coord>::bounded(
    0, Configuration::num_polarization_products - 1)};

/* \brief correlated samples value field definition */
using SampleCorrelationField =
  StaticField<std::complex<std::int32_t>, field_ids::sample_correlation>;

/* \brief correlated flags value field definition */
using FlagCorrelationField =
  StaticField<std::uint32_t, field_ids::flag_correlation>;

#ifdef RCP_USE_KOKKOS
/* \brief visibility value field definition */
using VisibilityField = StaticField<
  std::complex<float>,
  field_ids::visibility,
  nullptr,
  Kokkos::complex<float>>;
#else
/* \brief visibility value field definition */
using VisibilityField = StaticField<std::complex<float>, field_ids::visibility>;
#endif

/* \brief visibility weight value field definition */
using WeightField = StaticField<float, field_ids::weight>;

/*! \brief field to project back to BaselineChannelRegion index */
using BaselineChannelProjectionField =
  StaticField<L::Point<2, coord_t>, field_ids::baseline_channel_projection>;

/*! \brief visibilities region definition */
struct BsetChannelBaselinePolprodRegion
  : public StaticRegionSpec<
      bset_channel_baseline_polprod_index_spec_t<coord_t>,
      default_bset_channel_baseline_polprod_bounds<coord_t>,
      SampleCorrelationField,
      FlagCorrelationField,
      VisibilityField,
      WeightField,
      BaselineChannelProjectionField> {

  using static_region_t = StaticRegionSpec<
    bset_channel_baseline_polprod_index_spec_t<coord_t>,
    default_bset_channel_baseline_polprod_bounds<coord_t>,
    SampleCorrelationField,
    FlagCorrelationField,
    VisibilityField,
    WeightField,
    BaselineChannelProjectionField>;

  /*! \brief convert polarization pair to linear index */
  static constexpr auto
  polarization_product_index(std::array<coord_t, 2> const& p0p1) -> coord_t {
    auto const& [p0, p1] = p0p1;
    return p1 * Configuration::num_polarizations + p0;
  }

  /*! \brief convert polarization_product_index linear index to first element of
   *  pair
   */
  static constexpr auto
  polarization0_index(coord_t pp) -> coord_t {
    return pp % Configuration::num_polarizations;
  }

  /*! \brief convert polarization_product_index linear index to second element
   *  of pair
   */
  static constexpr auto
  polarization1_index(coord_t pp) -> coord_t {
    return pp / Configuration::num_polarizations;
  }
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
