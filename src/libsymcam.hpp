// Copyright 2022-2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

/*! \file symcam.hpp
 *
 * Collection of types and functions that are useful for symcam.cpp as well as
 * various symcam modules.
 */
#include "Configuration.hpp"

#include <algorithm>
#include <array>
#include <chrono>
#include <compare>
#include <mutex>
#include <rcp/rcp.hpp>
#include <set>
#include <stdexcept>
#include <variant>
#include <vector>

#include <fftw3.h>

/*! \namespace symcam */
namespace rcp::symcam {

/*! \brief flag to tell Legion about voltage samples tiling
 *
 * Legion does a poor job copying a tiled region instance, so we can keep Legion
 * unaware of the tiling of sample regions. This works OK because many of the
 * accesses to samples region elements don't require Legion accessors -- as long
 * as we obtain pointers to the start of instances, we can implement our own
 * index mapping (which is what TCC does, regardless of whether Legion knows
 * about the tiling or not). We still implement tiled layouts; the only effect
 * of this workaround is on what Legion knows of the tiling. The only part of
 * symcam code where we need to implement the index mapping workaround
 * explicitly is in ComputeVisibilityWeights. Ideally, Legion will fix the
 * performance issues and we can remove this workaround.
 */
static constexpr bool legion_is_unaware_of_samples_tiling =
  SYMCAM_LEGION_IS_UNAWARE_OF_SAMPLES_TILING;

/*! \brief flag to control partitioning of CF regions
 *
 * Define this to partition the CF regions in parallel with the visibility
 * region partitions. In the current implementation, this operation is
 * relatively expensive to complete, and may become a bottleneck. The following
 * macro supports maintaining both APIs (mainly in GridVisibilitiesOnTiles)
 * while we experiment and dither.
 */
static constexpr bool partition_cf_regions = SYMCAM_PARTITION_CF_REGIONS;

/*! \brief namespace tag for bark logging */
struct Tag {};

} // end namespace rcp::symcam

/*! \namespace bark */
namespace bark {

/*! \brief symcam logging static configuration. */
template <>
struct StaticContext<rcp::symcam::Tag> {
  static constexpr char const* nm = "symcam"; //!< logging source identity
  // to avoid proliferation of configuration macros, we reuse the librcp
  // logging values
  /*! \brief static minimum logging level */
  static constexpr bark::Level min_log_level = RCP_MIN_LOG_LEVEL;
  /*! \brief log source code location on trace messages */
  static constexpr bool trace_location = RCP_TRACE_LOCATION;
  /*! \brief log source code location on debug messages */
  static constexpr bool debug_location = RCP_DEBUG_LOCATION;
  /*! \brief log source code location on info messages */
  static constexpr bool info_location = RCP_INFO_LOCATION;
  /*! \brief log source code location on warn messages */
  static constexpr bool warn_location = RCP_WARN_LOCATION;
  /*! \brief log source code location on error messages */
  static constexpr bool error_location = RCP_ERROR_LOCATION;
  /*! \brief log source code location on fatal messages */
  static constexpr bool fatal_location = RCP_FATAL_LOCATION;
  /*! \brief log source code location on print messages */
  static constexpr bool print_location = false;
  /*! \brief logging source identity string */
  static constexpr auto
  source_identity() -> char const* {
    return nm;
  }
};

} // end namespace bark

/*! \namespace rcp */
namespace rcp {

/*! \brief specialization of rcp::rcpl::get_log_sink() for symcam logging */
template <>
auto
get_log_sink<symcam::Tag>() -> LogSink<symcam::Tag>&;

} // namespace rcp

namespace rcp::symcam {

/*! \brief initialize log sink for a symcam instance */
auto
init_log_sink(std::string_view const& name) -> void;

/*! \brief log instance with default for symcam logging */
template <typename T = Tag>
inline auto
log() {
  return bark::Log<T>{get_log_sink<T>()};
}

/*! \brief scope trace log instance with default for symcam logging */
template <typename T = Tag>
inline auto
trace(std::source_location loc = bark::source_location::current()) {
  return bark::ScopeTraceLog<T>(get_log_sink<T>(), loc);
}

/*! \brief value type for compensated summation
 *
 * \tparam T accumulation value type
 *
 * This type implements the compensated summation algorithm for values of type
 * T. See the article [What every computer scientist should know about
 * floating-point arithmetic](https://dl.acm.org/doi/10.1145/103162.103163) for
 * a description of the compensated summation algorithm.
 */
template <typename T>
class compsum {

  T m_val{}; //!< accumulated value
  T m_err{}; //!< residual error value

public:
  /*! \brief default constructor */
  RCP_INLINE_FUNCTION
  compsum() noexcept = default;
  /*! \brief (defaulted) copy constructor */
  RCP_INLINE_FUNCTION
  compsum(compsum const&) noexcept = default;
  /*! \brief (defaulted) move constructor */
  RCP_INLINE_FUNCTION
  compsum(compsum&&) noexcept = default;
  /*! \brief construction from value of type T
   *
   * Initialize error value to 0
   */
  RCP_INLINE_FUNCTION
  compsum(T const& val_) noexcept : m_val(val_) {}
  /*! \brief construction from rref value of type T
   *
   * Initialize error value to 0
   */
  RCP_INLINE_FUNCTION
  compsum(T&& val_) noexcept : m_val(std::move(val_)) {}
  /*! \brief construction from value and error */
  RCP_INLINE_FUNCTION
  compsum(T const& val_, T const& err_) noexcept : m_val(val_), m_err(err_) {}
  /*! \brief construction from rref value and rref error */
  RCP_INLINE_FUNCTION
  compsum(T&& val_, T&& err_) noexcept
    : m_val(std::move(val_)), m_err(std::move(err_)) {}
  /*! \brief (defaulted) destructor */
  RCP_INLINE_FUNCTION ~compsum() = default;
  /*! \brief (defaulted) copy assignment */
  RCP_INLINE_FUNCTION auto
  operator=(compsum const&) noexcept -> compsum& = default;
  /*! \brief (defaulted) move assignment */
  RCP_INLINE_FUNCTION auto
  operator=(compsum&&) noexcept -> compsum& = default;
  /*! \brief assignment from value of type T
   *
   * Sets error value to 0.
   */
  RCP_INLINE_FUNCTION auto
  operator=(T const& val_) noexcept -> compsum& {
    m_val = val_;
    m_err = 0;
    return *this;
  }
  /*! \brief accumulation operator with value of type T
   *
   * Implements the compensated summation algorithm.
   */
  RCP_INLINE_FUNCTION auto
  operator+=(T const& rhs) noexcept -> compsum& {
    auto comp_rhs = rhs - m_err;
    auto new_val = m_val + comp_rhs;
    m_err = (new_val - m_val) - comp_rhs;
    m_val = new_val;
    return *this;
  }
  /*! \brief addition operator with value of type T
   *
   * Implements the compensated summation algorithm.
   */
  RCP_INLINE_FUNCTION auto
  operator+(T const& rhs) const noexcept -> compsum {
    auto comp_rhs = rhs - m_err;
    auto new_val = m_val + comp_rhs;
    return compsum(new_val, (new_val - m_val) - comp_rhs);
  }
  /*! \brief accumulation operator with value of type compsum<T>
   *
   * Implements the compensated summation algorithm.
   */
  RCP_INLINE_FUNCTION auto
  operator+=(compsum const& rhs) noexcept -> compsum& {
#ifndef SYMCAM_ENABLE_PARALLEL_COMPENSATED_SUMMATION
    return *this += rhs.val;
#else
    return (*this += rhs.m_val) += -rhs.m_err;
#endif // SYMCAM_ENABLE_PARALLEL_COMPENSATED_SUMMATION
  }
  /*! \brief addition operator with value of type compsum<T>
   *
   * Implements the compensated summation algorithm.
   */
  RCP_INLINE_FUNCTION auto
  operator+(compsum const& rhs) const noexcept -> compsum {
#ifndef SYMCAM_ENABLE_PARALLEL_COMPENSATED_SUMMATION
    return *this + rhs.val;
#else
    return (*this + rhs.m_val) += -rhs.m_err;
#endif // SYMCAM_ENABLE_PARALLEL_COMPENSATED_SUMMATION
  }
  /*! \brief conversion function to reference to value of type T */
  RCP_INLINE_FUNCTION explicit
  operator T const&() const {
    return m_val;
  }
};

/*! \brief concept for identifying a value of type compsum<.> */
template <typename T>
concept CompensatedSum = requires(T tval) {
  tval.val;
  tval.err;
};

/*! \brief wrapper to get type T of accval<T> */
template <typename T>
struct accval_value {
  using type = T; //!< simple value type
};
/*! \brief specialization of accval_value for CompensatedSum types */
template <CompensatedSum T>
struct accval_value<T> {
  using type = decltype(T::val); //!< base value type
};
/*! \brief alias for accval_type<T>::type */
template <typename T>
using accval_value_t = typename accval_value<T>::type;

/*! \brief type trait to deduce CompensatedSum types */
template <typename T>
struct is_compensated_sum {
  /*! \brief condition flag, not a CompensatedSum */
  static constexpr bool value = false;
};
/*! \brief specialization of is_compensated_sum for CompensatedSum types */
template <CompensatedSum T>
struct is_compensated_sum<T> {
  /*! \brief condition flag, a CompensatedSum */
  static constexpr bool value = true;
};
/*! \brief alias for is_compensated_sum<T>::value */
template <typename T>
inline constexpr bool is_compensated_sum_v = is_compensated_sum<T>::value;

/*! \brief standard samples layout
 */
auto
samples_layout() -> PortableLayout<> const&;

/*! \brief standard visibilities layout
 */
auto
visibilities_layout() -> PortableLayout<> const&;

/*! \brief standard UVWs layout */
auto
uvws_layout() -> PortableLayout<> const&;

/*! \brief standard CF values layout */
auto
cf_layout() -> PortableLayout<> const&;

/*! \brief OrderingConstraint for grid layout */
auto
grid_ordering() -> L::OrderingConstraint;

/*! \brief key value type for layout with reduction option */
struct OptionalReductionLayoutStyle {
  /*! \brief normal layout style */
  LayoutStyle style;
  /*! \brief flag to indicate need for affine reduction specialized
   * constraint */
  bool for_reduction;

  auto
  operator<(OptionalReductionLayoutStyle const& rhs) const noexcept -> bool {
    return static_cast<int>(style) < static_cast<int>(rhs.style)
           || ((
             static_cast<int>(style) == static_cast<int>(rhs.style)
             // for_reduction < rhs.for_reduction
             && !for_reduction && rhs.for_reduction));
  }
};

/*! \brief standard grid pixels layout */
auto
grid_layout() -> PortableLayout<OptionalReductionLayoutStyle> const&;

/*! \brief standard grid weights layout */
auto
grid_weights_layout() -> PortableLayout<OptionalReductionLayoutStyle> const&;

/*! \brief stream clock class */
class StreamClock {
  /*! \brief external reference clock */
  using reference_clock = std::chrono::system_clock;

public:
  /*! \brief value type for reference clock time values */
  using external_time = reference_clock::time_point;

  /*! \brief value type for time values relative to current time */
  using relative_time = reference_clock::duration;

  /*! \brief value type for duration values */
  using duration = external_time::duration;

  /*! \brief value type for timestamps */
  using timestamp_t = std::variant<external_time, relative_time>;

private:
  /*! \brief current time */
  external_time m_now{external_time::min()};

public:
  /*! \brief default constructor (defaulted) */
  StreamClock() noexcept = default;
  /*! \brief copy constructor (defaulted) */
  StreamClock(StreamClock const&) noexcept = default;
  /*! \brief move constructor (defaulted) */
  StreamClock(StreamClock&&) noexcept = default;
  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(StreamClock const&) noexcept -> StreamClock& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(StreamClock&&) noexcept -> StreamClock& = default;
  /*! \brief destructor (defaulted) */
  virtual ~StreamClock() = default;

  /*! \brief update current time
   *
   * \param tp time value
   *
   * Update applied only if tp advances the clock's current time.
   */
  auto
  update(timestamp_t const& tp) -> void {
    std::visit(
      overloaded{
        [this](external_time const& et) {
          if (et > m_now)
            m_now = et;
        },
        [this](relative_time const& lt) { update(m_now + lt); }},
      tp);
  }

  /*! \brief get current time */
  [[nodiscard]] auto
  now() const -> external_time const& {
    return m_now;
  }
};

inline auto
operator<=>(
  StreamClock::timestamp_t const& t1,
  StreamClock::timestamp_t const& t2) noexcept -> std::partial_ordering {
  if (
    std::holds_alternative<StreamClock::external_time>(t1)
    && std::holds_alternative<StreamClock::external_time>(t2))
    return std::get<StreamClock::external_time>(t1)
           <=> std::get<StreamClock::external_time>(t2);
  if (
    std::holds_alternative<StreamClock::relative_time>(t1)
    && std::holds_alternative<StreamClock::relative_time>(t2))
    return std::get<StreamClock::relative_time>(t1)
           <=> std::get<StreamClock::relative_time>(t2);
  return std::partial_ordering::unordered;
}

inline auto
operator==(
  StreamClock::timestamp_t const& t1,
  StreamClock::timestamp_t const& t2) noexcept -> bool {
  return std::is_eq(t1 <=> t2);
}

inline auto
operator+(
  StreamClock::timestamp_t const& ts,
  StreamClock::relative_time const& dur) -> StreamClock::timestamp_t {
  return std::visit(
    [&](auto const& t0) -> StreamClock::timestamp_t { return t0 + dur; }, ts);
}

/*! \brief accumulation value type
 *
 * \tparam T value type
 */
template <typename T>
using accval = std::
  conditional_t<Configuration::use_compensated_grid_summation, compsum<T>, T>;

/*! \brief intermediate weights accumulation value type
 *
 * \tparam T value type
 */
template <typename T>
using weights_accval = std::conditional_t<
  Configuration::use_compensated_weights_summation,
  compsum<T>,
  T>;

/*! \brief set of receiver (indexes) */
class ReceiverSet {
public:
  using receiver_t = unsigned short; /*!< receiver index type alias */

private:
  /*! \brief the set of receivers */
  std::set<receiver_t> m_receivers{};

public:
  /*! \brief default constructor (defaulted) */
  ReceiverSet() = default;

  /*! \brief constructor from std::set of receiver indexes */
  ReceiverSet(std::set<receiver_t> receivers);

  /*! \brief constructor from iterator (pair) for receiver indexes  */
  template <class InputIt>
  ReceiverSet(InputIt first, InputIt last)
    : ReceiverSet(std::set<receiver_t>(first, last)) {
    if (std::ranges::any_of(
          m_receivers, [](auto&& rcv) { return !in_range(rcv); }))
      throw std::out_of_range("One or more receiver indexes is out of range");
  }

  /*! \brief copy constructor (defaulted) */
  ReceiverSet(ReceiverSet const& rcvset) = default;

  /*! \brief move constructor (defaulted) */
  ReceiverSet(ReceiverSet&& rcvset) noexcept = default;

  /*! \brief consstructor from initializer_list */
  ReceiverSet(std::initializer_list<receiver_t> init);

  /*! \brief destructor (defaulted) */
  ~ReceiverSet() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(ReceiverSet const&) -> ReceiverSet& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ReceiverSet&&) noexcept -> ReceiverSet& = default;

  /*! \brief number of receivers in set */
  [[nodiscard]] auto
  size() const -> std::size_t;

  /*! \brief query for the empty set */
  [[nodiscard]] auto
  empty() const -> bool;

  /*! \brief receivers as std::set */
  [[nodiscard]] auto
  receivers() const -> std::set<receiver_t> const&;

  /*! \brief membership test */
  [[nodiscard]] auto
  contains(receiver_t receiver) const -> bool;

  /*! \brief merge another ReceiverSet with this one, const target */
  [[nodiscard]] auto
  merge(ReceiverSet const& other) const& -> ReceiverSet;

  /*! \brief merge another ReceiverSet with this one, rval reference target */
  [[nodiscard]] auto
  merge(ReceiverSet const& other) && -> ReceiverSet;

  /*! \brief equality test */
  auto
  operator==(ReceiverSet const& rhs) const -> bool;

  /*! \brief inequality test */
  auto
  operator!=(ReceiverSet const& rhs) const -> bool;

  /*! \brief receiver set with all receivers */
  static auto
  all() -> ReceiverSet const&;

  /*! \brief receiver set with no receivers */
  static auto
  none() -> ReceiverSet const&;

  /*! \brief test whether receiver index value is in range */
  static constexpr auto
  in_range(receiver_t receiver) -> bool {
    return receiver < Configuration::num_receivers;
  }
};

/*! \brief base class for set of receiver pairs */
struct ReceiverPairSetBase {
  /*! \brief receiver pair type alias */
  using pair_t = std::array<ReceiverSet::receiver_t, 2>;

  /*! \brief Compare instance for receiver pair values
   *
   * comparison is insensitive to order of elements in the pairs
   */
  struct ReceiverPairLess {
    auto
    operator()(pair_t const& left, pair_t const& right) const -> bool {
      auto left_ = (left[0] < left[1]) ? std::array{left[1], left[0]} : left;
      auto right_ =
        (right[0] < right[1]) ? std::array{right[1], right[0]} : right;
      return left_ < right_;
    }
  };

  /*! \brief std::set specialization for a set of receiver pairs */
  using set_t = std::set<pair_t, ReceiverPairLess>;
};

/*! \brief receiver pair set CRTP base class
 *
 * \tparam Impl implementation class
 * \tparam Iter iterator class for Impl
 */
template <typename Impl, typename Iter>
struct ReceiverPairSet : public ReceiverPairSetBase {

  /*! \brief receiver pairs in the instance */
  [[nodiscard]] auto
  pairs() const -> set_t {
    return static_cast<Impl const&>(*this).pairs();
  }

  /*! \brief membership test */
  [[nodiscard]] auto
  contains(pair_t const& pair) -> bool {
    return static_cast<Impl const&>(*this).contains(pair);
  }

  /*! \brief begin iterator for pairs in the set */
  [[nodiscard]] auto
  begin() const -> Iter {
    return static_cast<Impl const&>(*this).begin();
  }

  /*! \brief end iterator for pairs in the set */
  [[nodiscard]] static auto
  end() -> Iter {
    return Impl::end();
  }
};

class ReceiverPairsIterator;

/*! \brief receiver pairs as an explicit list of pairs
 *
 * This is the most general form of a set of receiver pairs, but it is
 * inefficient because all the pairs are explicitly maintained in a std;:set,
 * which may be somewhat large.
 */
class ReceiverPairs
  : public ReceiverPairSet<ReceiverPairs, ReceiverPairsIterator> {

public:
  using iterator_t = ReceiverPairsIterator; /*!< iterator type */
  using set_t = ReceiverPairSetBase::set_t; /*!< receiver pair set type */

private:
  /*! \brief receiver pairs in the set */
  ReceiverPairSetBase::set_t m_pairs{};

public:
  /*! \brief default constructor (defaulted) */
  ReceiverPairs() = default;

  /*! \brief construct from a set of receiver pairs */
  ReceiverPairs(set_t pairs);

  /*! \brief copy constructor (defaulted) */
  ReceiverPairs(ReceiverPairs const&) = default;

  /*! \brief move constructor (defaulted) */
  ReceiverPairs(ReceiverPairs&&) noexcept = default;

  /*! \brief destructor (defaulted) */
  ~ReceiverPairs() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(ReceiverPairs const&) -> ReceiverPairs& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ReceiverPairs&&) noexcept -> ReceiverPairs& = default;

  /*! \brief number of pairs in the set */
  [[nodiscard]] auto
  size() const -> std::size_t;

  /*! \brief test for empty set of pairs */
  [[nodiscard]] auto
  empty() const -> bool;

  /*! \brief receiver pairs as std::set */
  [[nodiscard]] auto
  pairs() const -> set_t;

  /*! \brief pointer to set of receiver pairs maintained by this instance */
  [[nodiscard]] auto
  pairs_p() const -> set_t const*;

  /*! \brief membership test */
  [[nodiscard]] auto
  contains(pair_t const& pair) const -> bool;

  /*! \brief merge pairs from another set into this set, const target version */
  [[nodiscard]] auto
  merge(ReceiverPairs const& other) const& -> ReceiverPairs;

  /*! \brief merge pairs from another set into this set, rval reference target
   * version */
  [[nodiscard]] auto
  merge(ReceiverPairs const& other) && -> ReceiverPairs;

  /*! \brief equality test */
  auto
  operator==(ReceiverPairs const& rhs) const -> bool;

  /*! \brief inequality test */
  auto
  operator!=(ReceiverPairs const& rhs) const -> bool;

  /*! \brief begin iterator for pairs in the set */
  [[nodiscard]] auto
  begin() const -> iterator_t;

  /*! \brief end iterator for pairs in the set */
  [[nodiscard]] static auto
  end() -> iterator_t;
};

/*! \brief iterator for ReceiverPairs */
class ReceiverPairsIterator {
public:
  using difference_type = int; /*!< iterator difference type */
  using value_type = ReceiverPairSetBase::pair_t; /*< iterator value type */

private:
  /*! \brief iterator type for std::set maintained by target */
  using pair_iter_t = decltype(ReceiverPairs{}.pairs_p()->begin());

  /*! \brief current iterator */
  pair_iter_t m_current;

  /*! \brief end iterator */
  pair_iter_t m_end;

public:
  /*! \brief default constructor (defaulted) */
  ReceiverPairsIterator() = default;

  /*! \brief constructor from ReceiverPairs instance */
  ReceiverPairsIterator(ReceiverPairs const& pairs);

  /*! \brief pre-increment operator */
  auto
  operator++() -> ReceiverPairsIterator&;

  /*! \brief post-increment operator */
  auto
  operator++(int) -> ReceiverPairsIterator;

  /*! \brief dereference operator */
  auto
  operator*() const -> ReceiverPairSetBase::pair_t;

  /*! \brief equality operator */
  auto
  operator==(ReceiverPairsIterator const& rhs) const -> bool;

private:
  /*! \brief test end condition */
  [[nodiscard]] auto
  at_end() const -> bool;
};

static_assert(std::input_or_output_iterator<ReceiverPairsIterator>);
static_assert(std::sentinel_for<ReceiverPairsIterator, ReceiverPairsIterator>);

class ReceiverSetOuterProductIterator;

/*! \brief receiver pairs formed from outer product of two ReceiverSets
 *
 * This is a more efficient representation of those sets that can be expressed
 * as an outer product over two (possibly identical) receiver sets than
 * ReceiverPairs. The definition of traditional sub-arrays falls into this
 * category.
 */
class ReceiverSetOuterProduct
  : public ReceiverPairSet<
      ReceiverSetOuterProduct,
      ReceiverSetOuterProductIterator> {

  ReceiverSet m_left;  /*!< left receiver set */
  ReceiverSet m_right; /*!< right receiver set */

public:
  /*! \brief iterator type alias */
  using iterator_t = ReceiverSetOuterProductIterator;

  /*! \brief default constructor (defaulted) */
  ReceiverSetOuterProduct() = default;

  /*! \brief constructor from a single ReceiverSet
   *
   * Creates receiver pairs that can be used for a traditional sub-array.
   */
  ReceiverSetOuterProduct(ReceiverSet const& left_and_right);

  /*! \brief constructor from a pair of ReceiverSets */
  ReceiverSetOuterProduct(ReceiverSet left, ReceiverSet right);

  /*! \brief copy constructor (defaulted) */
  ReceiverSetOuterProduct(ReceiverSetOuterProduct const&) = default;

  /*! \brief move constructor (defaulted) */
  ReceiverSetOuterProduct(ReceiverSetOuterProduct&&) noexcept = default;

  /*! \brief destructor (defaulted) */
  ~ReceiverSetOuterProduct() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(ReceiverSetOuterProduct const&)
    -> ReceiverSetOuterProduct& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ReceiverSetOuterProduct&&) noexcept
    -> ReceiverSetOuterProduct& = default;

  /*! \brief number of receiver pairs */
  [[nodiscard]] auto
  size() const -> std::size_t;

  /*! \brief test for empty set of receiver pairs */
  [[nodiscard]] auto
  empty() const -> bool;

  /*! \brief ReceiverSet for left "index" of outer product elements */
  [[nodiscard]] auto
  left() const -> ReceiverSet const&;

  /*! \brief ReceiverSet for right "index" of outer product elements */
  [[nodiscard]] auto
  right() const -> ReceiverSet const&;

  /*! \brief receiver pairs as set_t
   *
   * Using an iterator for this class to access the pairs is generally preferred
   * over this function. All pairs need to be created explicitly for the result
   * value of this function. An iterator can use the ReceiverSets directly to
   * obtain each its values, and never creates all pairs at one time.
   */
  [[nodiscard]] auto
  pairs() const -> set_t;

  /*! \brief membership test */
  [[nodiscard]] auto
  contains(pair_t const& pair) const -> bool;

  /*! \brief merge pairs from another ReceiverSetOuterProduct, const target
   *  version */
  [[nodiscard]] auto
  merge(ReceiverSetOuterProduct const& other) const& -> ReceiverSetOuterProduct;

  /*! \brief merge pairs from another ReceiverSetOuterProduct, rval reference
   *  target version */
  [[nodiscard]] auto
  merge(ReceiverSetOuterProduct const& other) && -> ReceiverSetOuterProduct;

  /*! \brief equality operator */
  auto
  operator==(ReceiverSetOuterProduct const& rhs) const -> bool;

  /*! \brief inequality operator */
  auto
  operator!=(ReceiverSetOuterProduct const& rhs) const -> bool;

  /*! \brief begin iterator for pairs in the set
   *
   * Iteration order should be consistent with
   * ReceiverPairSetBase::ReceiverPairLess
   */
  [[nodiscard]] auto
  begin() const -> iterator_t;

  /*! \brief end iterator for pairs in the set
   */
  [[nodiscard]] static auto
  end() -> iterator_t;
};

/*! \brief iterator for ReceiverSetOuterProduct */
class ReceiverSetOuterProductIterator {
public:
  using difference_type = int; /*!< iterator difference type */
  using value_type = ReceiverPairSetBase::pair_t; /*< iterator value type */

private:
  using index_t = ReceiverSet::receiver_t;

  /* \brief pointer to referenced outer product */
  ReceiverSetOuterProduct const* m_outer{nullptr};

  /*! \brief current row index */
  index_t m_row_index{Configuration::num_receivers};

  /*! \brief current column index */
  index_t m_col_index{0};

public:
  /*! \brief default constructor (defaulted) */
  ReceiverSetOuterProductIterator() = default;

  /*! \brief constructor from a ReceiverSetOuterProductIterator */
  ReceiverSetOuterProductIterator(ReceiverSetOuterProduct const& outer);

  /*! \brief pre-increment operator */
  auto
  operator++() -> ReceiverSetOuterProductIterator&;

  /*! \brief post-increment operator */
  auto
  operator++(int) -> ReceiverSetOuterProductIterator;

  /*! \brief pre-decrement operator */
  auto
  operator--() -> ReceiverSetOuterProductIterator&;

  /*! \brief post-decrement operator */
  auto
  operator--(int) -> ReceiverSetOuterProductIterator;

  /*! \brief random access increment operator */
  auto
  operator+=(difference_type diff) -> ReceiverSetOuterProductIterator&;

  /*! \brief random access addition operator */
  auto
  operator+(difference_type diff) const -> ReceiverSetOuterProductIterator;

  /*! \brief random access decrement operator */
  auto
  operator-=(difference_type diff) -> ReceiverSetOuterProductIterator&;

  /*! \brief random access subtraction operator */
  auto
  operator-(difference_type diff) const -> ReceiverSetOuterProductIterator;

  /*! \brief subtraction operator */
  auto
  operator-(ReceiverSetOuterProductIterator const& other) const
    -> difference_type;

  /*! \brief dereference operator */
  auto
  operator*() const -> ReceiverPairSetBase::pair_t;

  /*! \brief indexed dereference operator */
  auto
  operator[](difference_type diff) const -> ReceiverPairSetBase::pair_t;

  /*! \brief equality operator */
  auto
  operator==(ReceiverSetOuterProductIterator const& rhs) const -> bool;

  /*! \brief comparison operator */
  auto
  operator<=>(ReceiverSetOuterProductIterator const& rhs) const
    -> std::strong_ordering;

private:
  /*! \brief end of iteration test */
  [[nodiscard]] auto
  at_end() const -> bool;

  /*! \brief current baseline index */
  [[nodiscard]] inline auto
  baseline() const -> ReceiverSet::receiver_t {
    return receiver_pair_to_baseline(std::array{m_row_index, m_col_index});
  }
};

auto
operator+(
  ReceiverSetOuterProductIterator::difference_type diff,
  ReceiverSetOuterProductIterator const& it) -> ReceiverSetOuterProductIterator;

auto
operator-(
  ReceiverSetOuterProductIterator::difference_type diff,
  ReceiverSetOuterProductIterator const& it) -> ReceiverSetOuterProductIterator;

static_assert(std::random_access_iterator<ReceiverSetOuterProductIterator>);
static_assert(std::sentinel_for<
              ReceiverSetOuterProductIterator,
              ReceiverSetOuterProductIterator>);

/*! \brief set of channels (indexes) */
class ChannelSet {
public:
  using channel_t = rcp::dc::channel_type; /*!< channel index type alias */

private:
  /*! \brief set of channel intervals
   *
   * Represented by an ordered sequence of disjoint, closed intervals of channel
   * indexes.
   */
  std::vector<std::array<channel_t, 2>> m_intervals{};

  /*! \brief add a channel interval to m_channels */
  auto
  add_in(std::array<channel_t, 2> const& interval) -> void;

public:
  /*! \brief default constructor (deleted) */
  ChannelSet() = default;

  /*! \brief constructor from std::set of channel index intervals */
  ChannelSet(std::vector<std::array<channel_t, 2>> const& intervals);

  /*! \brief constructor from iterator (pair) for channel index intervals  */
  template <class InputIt>
  ChannelSet(InputIt first, InputIt last) {
    while (first != last) {
      add_in(*first);
      ++first;
    }
  }

  /*! \brief copy constructor (defaulted) */
  ChannelSet(ChannelSet const& rcvset) = default;

  /*! \brief move constructor (defaulted) */
  ChannelSet(ChannelSet&& rcvset) noexcept = default;

  /*! \brief constructor from initializer_list */
  ChannelSet(std::initializer_list<std::array<channel_t, 2>> init);

  /*! \brief destructor (defaulted) */
  ~ChannelSet() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(ChannelSet const&) -> ChannelSet& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ChannelSet&&) noexcept -> ChannelSet& = default;

  /*! \brief number of channels in set */
  [[nodiscard]] auto
  size() const -> std::size_t;

  /*! \brief query for the empty set */
  [[nodiscard]] auto
  empty() const -> bool;

  /*! \brief channel intervals as std::vector */
  [[nodiscard]] auto
  intervals() const -> std::vector<std::array<channel_t, 2>> const&;

  /*! \brief channels as std::vector */
  [[nodiscard]] auto
  channels() const -> std::vector<channel_t>;

  /*! \brief membership test */
  [[nodiscard]] auto
  contains(channel_t channel) const -> bool;

  /*! \brief merge another ChannelSet with this one, const target */
  [[nodiscard]] auto
  add(ChannelSet const& other) const& -> ChannelSet;

  /*! \brief merge another ChannelSet with this one, rval reference target */
  [[nodiscard]] auto
  add(ChannelSet const& other) && -> ChannelSet;

  /*! \brief equality test */
  auto
  operator==(ChannelSet const& rhs) const -> bool;

  /*! \brief inequality test */
  auto
  operator!=(ChannelSet const& rhs) const -> bool;

  /*! \brief channel set with all channels */
  static auto
  all(Configuration const& config) -> ChannelSet;

  /*! \brief channel set with no channels */
  static auto
  none() -> ChannelSet const&;

  /*! \brief intersection of channels in this set with the range specified by a
   * Configuration instance
   */
  [[nodiscard]] auto
  intersection(Configuration const& config) const -> ChannelSet;
};

/*! \brief subarray name type
 *
 * Must be fixed length byte string
 */
using subarray_name_t =
  std::array<char, Configuration::subarray_name_max_length + 1>;

/*! \brief subarray type */
using subarray_t = std::variant<ReceiverPairs, ReceiverSetOuterProduct>;

/*! \brief range of subarray slot indexes */
constexpr auto
subarray_slots() {
  return std::views::iota(0U, symcam::Configuration::max_num_subarrays);
}

template <typename... Predicate>
auto
filtered_subarray_slots(Predicate const&... predicate) {
  return std::views::filter(
    subarray_slots(), [&](auto&& slot) { return (predicate.at(slot) && ...); });
}

/*! \brief wrapper for FFT third-party library */
template <typename T>
struct FFT_TPL {
  template <typename V>
  struct FFTArgs {
    using plan_t = void;
  };
  template <typename V>
  static auto
  alloc(V*&, std::size_t) -> void;
  template <typename V>
  static auto
  free(V*&) -> void;
  template <typename V>
  static auto
  make_plan(FFTArgs<V>&) -> void;
  template <typename V>
  static auto
  destroy_plan(FFTArgs<V>&) -> void;
};

/*! \brief helper class to get precision of real and complex FP values */
template <typename T>
struct precision {
  using type = void;
};
template <>
struct precision<std::complex<double>> {
  using type = double;
};
template <>
struct precision<std::complex<float>> {
  using type = float;
};
template <>
struct precision<double> {
  using type = double;
};
template <>
struct precision<float> {
  using type = float;
};
template <typename T>
using precision_t = precision<T>::type;

/*! \brief fftw library */
struct FFTW {};

/*! \brief fftw library, OpenMP version */
struct FFTW_OMP {};

struct FFTWBase {
private:
  static std::mutex mtx;

protected:
  static auto
  mutex() -> std::mutex& {
    return mtx;
  }

public:
  template <typename V>
  struct FFTArgs {
    using plan_t = std::conditional_t<
      std::is_same_v<precision_t<V>, double>,
      fftw_plan,
      fftwf_plan>;
    plan_t plan{};
    unsigned rank{};
    std::vector<int> dims{};
    int howmany{1};
    V* in{};
    std::vector<int> inembed{};
    int istride{1};
    int idist{0};
    V* out{};
    std::vector<int> onembed{};
    int ostride{1};
    int odist{0};
    int sign{};
    unsigned flags{};
  };

  template <typename V>
  static auto
  validate_args(FFTArgs<V> const& args) -> void {
    assert(args.rank == args.dims.size());
    assert([&]() {
      for (auto&& embed : {args.onembed, args.inembed}) {
        if (
          !embed.empty()
          && ((args.rank != embed.size()) || std::ranges::any_of(embed, [&](auto const& edim) {
                return args.dims.at(std::distance(embed.data(), &edim)) > edim;
              })))
          return false;
      }
      return true;
    }());
    assert(
      (args.in != args.out)
      || ((args.istride == args.ostride) && (args.idist == args.odist)));
  }

  template <typename V>
  static auto
  alloc(V*& ptr, std::size_t num) -> void {
    // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)
    if constexpr (std::is_same_v<precision_t<V>, double>) {
      ptr = reinterpret_cast<V*>(fftw_alloc_complex(num));
    } else {
      ptr = reinterpret_cast<V*>(fftwf_alloc_complex(num));
    }
    // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)
  }

  template <typename V>
  static auto
  free(V*& ptr) -> void {
    if constexpr (std::is_same_v<precision_t<V>, double>) {
      fftw_free(ptr);
    } else {
      fftwf_free(ptr);
    }
  }

  template <typename T>
  static auto
  init() -> void {
    if constexpr (std::is_same_v<precision_t<T>, double>) {
      [[maybe_unused]] auto rc = fftw_init_threads();
      assert(rc != 0);
    } else {
      [[maybe_unused]] auto rc = fftwf_init_threads();
      assert(rc != 0);
    }
  }

  template <typename V>
  static auto
  make_plan_unsafe(FFTArgs<V>& args) -> void {
    validate_args(args);
    // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)
    if constexpr (std::is_same_v<V, std::complex<double>>) {
      args.plan = fftw_plan_many_dft(
        args.rank,
        args.dims.data(),
        args.howmany,
        reinterpret_cast<fftw_complex*>(args.in),
        ((args.inembed.size() > 0) ? args.inembed.data() : nullptr),
        args.istride,
        args.idist,
        reinterpret_cast<fftw_complex*>(args.out),
        ((args.onembed.size() > 0) ? args.onembed.data() : nullptr),
        args.ostride,
        args.odist,
        args.sign,
        args.flags);
    } else {
      args.plan = fftwf_plan_many_dft(
        args.rank,
        args.dims.data(),
        args.howmany,
        reinterpret_cast<fftwf_complex*>(args.in),
        ((args.inembed.size() > 0) ? args.inembed.data() : nullptr),
        args.istride,
        args.idist,
        reinterpret_cast<fftwf_complex*>(args.out),
        ((args.onembed.size() > 0) ? args.onembed.data() : nullptr),
        args.ostride,
        args.odist,
        args.sign,
        args.flags);
    }
    // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)
  }
  template <typename V>
  static auto
  destroy_plan(FFTArgs<V>& args) -> void {
    auto lk = std::lock_guard(mtx);
    if constexpr (std::is_same_v<precision_t<V>, double>)
      fftw_destroy_plan(args.plan);
    else
      fftwf_destroy_plan(args.plan);
    args.plan = 0;
  }
  template <typename V>
  static auto
  execute(FFTArgs<V> const& args) -> void {
    // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)
    if constexpr (std::is_same_v<precision_t<V>, double>)
      fftw_execute_dft(
        args.plan,
        reinterpret_cast<fftw_complex*>(args.in),
        reinterpret_cast<fftw_complex*>(args.out));
    else
      fftwf_execute_dft(
        args.plan,
        reinterpret_cast<fftwf_complex*>(args.in),
        reinterpret_cast<fftwf_complex*>(args.out));
    // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)
  }
};

/*! \brief FFT_TPL specialization for fftw */
template <>
struct FFT_TPL<FFTW> : public FFTWBase {
  template <typename V>
  static auto
  make_plan(FFTArgs<V>& args) -> void {
    auto lk = std::lock_guard(mutex());
    if constexpr (std::is_same_v<precision_t<V>, double>)
      fftw_plan_with_nthreads(1);
    else
      fftwf_plan_with_nthreads(1);
    make_plan_unsafe(args);
  }
};

// keeping this here while the Legion+Kokkos officially support multiple OMP
// processors per process (I have a patch that seems to fix the issue, but
// it's my own patch, not well tested)
#define ALLOW_FFTW_OMP

#ifdef ALLOW_FFTW_OMP
/*! \brief FFT_TPL specialization for fftw */
template <>
struct FFT_TPL<FFTW_OMP> : public FFTWBase {
  template <typename V>
  static auto
  make_plan(FFTArgs<V>& args, int num_threads) -> void {
    auto lk = std::lock_guard(mutex());
    if constexpr (std::is_same_v<precision_t<V>, double>)
      fftw_plan_with_nthreads(num_threads);
    else
      fftwf_plan_with_nthreads(num_threads);
    make_plan_unsafe(args);
  }
};
#endif // ALLOW_FFTW_OMP

/*! \brief fixed size array with Configuration::max_num_subarrays
 *  elements */
template <typename T>
using sarray = std::array<T, Configuration::max_num_subarrays>;

} // namespace rcp::symcam

namespace rcp {

template <typename Region>
constexpr inline auto
dim_range(typename Region::axes_t axis, typename Region::rect_t const& rect) {
  return std::views::iota(
    rect.lo[static_cast<int>(axis)], rect.hi[static_cast<int>(axis)] + 1);
}

inline auto
showt(rcp::symcam::StreamClock::duration const& val) -> std::string {
  auto count = val.count();
  // I don't know why clang-tidy wants the following to be const, but it's wrong
  std::ostringstream oss; // NOLINT(misc-const-correctness)
  oss << (count / rcp::symcam::StreamClock::external_time::period::den) << "."
      << (std::abs(count)
          % rcp::symcam::StreamClock::external_time::period::den);
  return oss.str();
}

inline auto
show(rcp::symcam::StreamClock::external_time const& val) -> std::string {
  return showt(val.time_since_epoch());
}

template <>
struct layout_key_traits<symcam::OptionalReductionLayoutStyle> {
  static constexpr auto
  name(symcam::OptionalReductionLayoutStyle key) -> char const* {
    if (key.style == LayoutStyle::CPU) {
      if (!key.for_reduction)
        return " (CPU)";
      return " (CPU,r)";
    } else {
      if (!key.for_reduction)
        return " (GPU)";
      return " (GPU,r)";
    }
  }
};

} // namespace rcp

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
