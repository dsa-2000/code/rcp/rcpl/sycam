// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include "EventProcessing.hpp"
#include "GridRegion.hpp"
#include "Serializable.hpp"
#include "StreamState.hpp"

/*! \file FastFourierTransform.hpp */

namespace rcp::symcam {

/*! \brief launcher for task to compute FFT */
class GridFourierTransform {
public:
  using grid_t = GridRegion;

  GridFourierTransform() = default;

  GridFourierTransform(GridFourierTransform const&) = default;

  GridFourierTransform(GridFourierTransform&&) = default;

  ~GridFourierTransform() = default;

  auto
  operator=(GridFourierTransform const&) -> GridFourierTransform& = default;

  auto
  operator=(GridFourierTransform&&) -> GridFourierTransform& = default;

  /*! \brief FFT plan representation
   *
   * Intended to make the dependence on a backend generic. TODO: make this into
   * a template with a parameter for backend implementation
   */
  struct Plan {
    static constexpr auto max_name_length = 20;

    /*! \brief id type alias */
    using id_t = int;

    /*! \brief plan (image) name
     *
     * \fixme size should be configuration value
     */
    std::array<char, max_name_length> m_name;

    /*! \brief plan id */
    id_t m_id;

    /*! \brief transform direction */
    int m_direction;

    /*! \brief input partition */
    PartitionPair<grid_t::LogicalPartition> m_input_partition;

    /*! \brief output partition */
    PartitionPair<grid_t::LogicalPartition> m_output_partition;

    /*! \brief domain of first (and only) partition subspace */
    L::Domain m_subspace_domain{};

    /*! \brief task processor preference */
    // bool m_prefer_omp;

    /*! \brief planner flags
     *
     * Usage of flags depends on the use case; flags may be ignored entirely in
     * some cases.
     *
     * TODO: backend-dependent, should be removed from generic class
     * implementation
     */
    unsigned m_planner_flags{0};

    struct Serialized {
      std::array<char, max_name_length> m_name;
      id_t m_id;
      int m_direction;
      PartitionPair<grid_t::LogicalPartition> m_input_partition;
      PartitionPair<grid_t::LogicalPartition> m_output_partition;
    };

    Plan() = default;

    Plan(
      L::Runtime* rt,
      ImagingConfiguration const& img_config,
      id_t id,
      int direction,
      PartitionPair<grid_t::LogicalPartition> const& input_partition,
      PartitionPair<grid_t::LogicalPartition> const& output_partition =
        PartitionPair<grid_t::LogicalPartition>{},
      unsigned planner_flags = 0);

    Plan(Plan const& other) = default;

    Plan(Plan&& other) noexcept = default;

    Plan(L::Context ctx, L::Runtime* rt, Serialized const& serialized) noexcept;

    ~Plan() = default;

    auto
    operator=(Plan const& rhs) -> Plan& = default;

    auto
    operator=(Plan&& rhs) noexcept -> Plan& = default;

    /*! \brief serialized form */
    [[nodiscard]] auto
    serialized(L::Context ctx, L::Runtime* rt) const noexcept -> Serialized;

    /*! \brief requirements for child tasks that use Serialized values as
     *  arguments */
    [[nodiscard]] auto
    child_requirements(L::Context ctx, L::Runtime* rt) const
      -> ChildRequirements;

    [[nodiscard]] auto
    substitute_grids(
      L::Runtime* rt,
      grid_t::LogicalRegion input_grid,
      grid_t::LogicalRegion output_grid) const -> Plan;

    static auto
    extents(grid_t::rect_t const& rect)
      -> L::Point<grid_t::dim, grid_t::coord_t>;

    auto
    in_place() const -> bool;
  };

  /*! \brief launch task to compute fft plans
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param plan Plan instance
   * \param planner_flags planner flags, usage depends on backend
   * \param pred task launch predicate
   *
   * \return vector of futures from task launch on various processors
   *
   * Use of this is optional, as the task launched by launch() is able to
   * compute the plans as needed. However, depending on the use case, this task
   * may provide some benefit.
   */
  static auto
  make_plan(
    L::Context ctx,
    L::Runtime* rt,
    Plan const& plan,
    L::Future wisdom = L::Future::from_value(true),
    L::Predicate pred = L::Predicate::TRUE_PRED) -> std::vector<L::Future>;

  /*! \brief launch task to apply fft
   *
   * The plan argument is not const, as the first execution of the task (only)
   * will modify the value.
   */
  static auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    sarray<Plan>& plans,
    st::subarray_predicates_array_t const& predicates) -> void;

  /*! \brief launch task to apply fft
   *
   * The plan argument is not const, as the first execution of the task (only)
   * will modify the value.
   */
  static auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    sarray<Plan>& plans,
    st::AllSubarraysQuery const& state_query) -> void;

  /*! \brief launch task to destroy fft plans
   *
   * Use of this is mandatory, if one wishes to reclaim resources used by a
   * plan.
   */
  static auto
  destroy_plan(
    L::Context ctx,
    L::Runtime* rt,
    Plan const& plan,
    L::Predicate pred = L::Predicate::TRUE_PRED) -> std::vector<L::Future>;

  static auto
  preregister_tasks(std::array<L::TaskID, 3> const& task_ids) -> void;

  static auto
  register_tasks(L::Runtime* rt, std::array<L::TaskID, 3> const& task_ids)
    -> void;

  static inline auto
  normalized_direction(int dir) -> int {
    return (dir >= 0) ? 1 : -1;
  }

private:
  static auto
  init_launcher(L::UntypedBuffer arg) -> L::IndexTaskLauncher;

  static auto
  update_launcher(
    L::Context ctx,
    L::Runtime* rt,
    sarray<Plan>& plans,
    unsigned subarray_slot,
    L::IndexTaskLauncher& launcher) -> void;
};
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
