// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "ReportFillFraction.hpp"
#include "libsymcam.hpp"

#include <format>
#include <numeric>
#include <ranges>
#include <rcp/rcp.hpp>
#include <string>
#include <vector>

namespace rcp::symcam {

/*! \brief task to produce data capture fill fraction reports
 */
struct ReportFillFractionTask
  : public SerialOnlyTaskMixin<ReportFillFractionTask, void> {

  /*! \brief task name*/
  static constexpr char const* task_name = "ReportFillFraction";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& /*regions*/,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> void {

    assert(task->arglen == sizeof(ReportFillFraction::Args));
    auto const& args =
      *reinterpret_cast<ReportFillFraction::Args const*>(task->args);

    // expected total number of samples
    auto total_volume = task->futures.size() * args.capture_volume;
    // convert futures to filled sample counts
    auto filled = std::views::transform(
      task->futures, [](L::Future const& f) -> std::size_t {
        return f.is_empty() ? 0 : f.get_result<std::size_t>();
      });
    // sum the filled sample counts
    auto total_filled = std::accumulate(filled.begin(), filled.end(), 0);
    // produce the report
    log().print(std::format(
      "F-engine samples fill fraction: {}",
      float(total_filled) / total_volume));
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID ReportFillFractionTask::task_id;

ReportFillFraction::ReportFillFraction(
  Configuration const& config, std::size_t capture_volume)
  : m_period(config.fill_report_period())
  , m_args{capture_volume}
  , m_launcher(ReportFillFractionTask::task_id, L::UntypedBuffer()) {

  m_launcher.predicate = L::Predicate::FALSE_PRED;
  m_launcher.futures.reserve(m_period);
}

auto
ReportFillFraction::record(
  L::Context ctx, L::Runtime* rt, L::Future new_count, L::Predicate live)
  -> void {

  m_launcher.predicate = rt->predicate_or(ctx, m_launcher.predicate, live);
  m_launcher.futures.push_back(new_count);
  if (m_launcher.futures.size() == m_period) {
    m_launcher.argument = L::UntypedBuffer(&m_args, sizeof(m_args));
    rt->execute_task(ctx, m_launcher);
    m_launcher.predicate = L::Predicate::FALSE_PRED;
    m_launcher.futures.clear();
  }
}

auto
ReportFillFraction::preregister_tasks(L::TaskID task_id) -> void {
  PortableTask<ReportFillFractionTask>::preregister_task_variants(task_id);
}

auto
ReportFillFraction::register_tasks(L::Runtime* rt, L::TaskID task_id) -> void {
  PortableTask<ReportFillFractionTask>::register_task_variants(rt, task_id);
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
