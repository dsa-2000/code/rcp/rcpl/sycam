// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BsetChannelBaselinePolprodRegion.hpp"
#include "DataPartitionsRegion.hpp"
#include "EventProcessing.hpp"
#include "Serializable.hpp"
#include "StateRegion.hpp"
#include "StaticFields.hpp"
#include "StreamState.hpp"
#include <rcp/rcp.hpp>
#include <variant>

#ifdef SYMCAM_ENABLE_HDF5
#include <H5Cpp.h>
#endif // SYMCAM_ENABLE_HDF5

#ifdef SYMCAM_ENABLE_ADIOS2
#include <adios2.h>
#include <adios2/cxx11/Engine.h>
#endif // SYMCAM_ENABLE_ADIOS2

/*! \file RecordVisibilities.hpp
 */

namespace rcp::symcam {

enum class VisRecorderAxes { shard = LEGION_DIM_0 };

#ifdef SYMCAM_ENABLE_HDF5
/*! \brief handle type for HDF5 */
struct H5Handle {
  H5::H5File file{};
  H5::DataSet visibility_data_set{};
  H5::ArrayType visibility_type{};
  H5::DataSpace mem_space{};
};
#endif // SYMCAM_ENABLE_HDF5

#ifdef SYMCAM_ENABLE_ADIOS2
struct Adios2Handle {
  adios2::ADIOS adios{};
  adios2::IO io{};
  adios2::Engine engine{};
  bool initial_step{true};
};
#endif // SYMCAM_ENABLE_ADIOS2

/*! \brief handle type for POSIX file */
struct POSIXHandle {
  int fd{-1};
};

/*! \brief union type for visibility record handles */
#if defined(SYMCAM_ENABLE_HDF5) && defined(SYMCAM_ENABLE_ADIOS2)
using RecordVisibilitiesHandle =
  std::variant<POSIXHandle, H5Handle, Adios2Handle>;
#elif defined(SYMCAM_ENABLE_HDF5)
using RecordVisibilitiesHandle = std::variant<POSIXHandle, H5Handle>;
#elif defined(SYMCAM_ENABLE_ADIOS2)
using RecordVisibilitiesHandle = std::variant<POSIXHandle, Adios2Handle>;
#else
using RecordVisibilitiesHandle = std::variant<POSIXHandle>;
#endif // SYMCAM_ENABLE_HDF5 && SYMCAM_ENABLE_ADIOS2

auto
reset_handle(RecordVisibilitiesHandle* handle) -> void;

auto
valid_handle(RecordVisibilitiesHandle const* handle) -> bool;

/*! \brief handle field definition */
using RecordingHandleField =
  StaticField<RecordVisibilitiesHandle, field_ids::recording_handle>;

/*! \brief region definition for visibility recorder instances */
using VisRecorderRegion = StaticRegionSpec<
  IndexSpec<
    VisRecorderAxes,
    VisRecorderAxes::shard,
    VisRecorderAxes::shard,
    int>,
  std::array{IndexInterval<int>::left_bounded(0)},
  RecordingHandleField>;

/*! \brief launcher for task to record visibilities */
class RecordVisibilities {
public:
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using part_t = DataPartitionsRegion;
  using state_t = st::StateRegion;
  using vrec_t = VisRecorderRegion;

  using handle_t = RecordVisibilitiesHandle;

  static auto constexpr max_instance_name_size = 40;

private:
  PartitionPair<vrec_t::LogicalPartition> m_vrec_lp;

  StreamClock::duration m_interval;

  std::array<char, max_instance_name_size> m_instance_name{};

  /*! \brief launcher to record visibility data
   *
   * Note that, although m_vrec_lr will have a size that is equal to the number
   * of shards, we are not using parallel i/o at this time, and subsequently, we
   * use a plain task launch, not an index task launch.
   *
   * \todo add support for parallel i/o
   */
  L::TaskLauncher m_launcher;

  std::unique_ptr<RecordingHandleField::value_t> m_vrec_handle;

  L::ExternalResources m_vrec_ext_resources;

public:
  /*! \brief constructor argument */
  struct Serialized {
    PartitionPair<vrec_t::LogicalPartition> m_vrec_lp;
    StreamClock::duration m_interval;
    std::array<char, max_instance_name_size> m_instance_name{};
  };

  RecordVisibilities() = default;

  RecordVisibilities(
    L::Context ctx,
    L::Runtime* rt,
    Configuration const& config,
    std::size_t shard_point,
    std::size_t num_shards);

  RecordVisibilities(
    L::Context ctx, L::Runtime* rt, Serialized const& serialized);

  RecordVisibilities(RecordVisibilities const&) = delete;

  RecordVisibilities(RecordVisibilities&&) noexcept = default;

  auto
  operator=(RecordVisibilities const&) -> RecordVisibilities& = delete;

  auto
  operator=(RecordVisibilities&&) noexcept -> RecordVisibilities& = default;

  virtual ~RecordVisibilities() = default;

  /*! \brief serialized form */
  [[nodiscard]] auto
  serialized(L::Context ctx, L::Runtime* rt) const noexcept -> Serialized;

  /*! \brief requirements for child tasks that use Serialized values as
   *  arguments */
  [[nodiscard]] auto
  child_requirements(L::Context ctx, L::Runtime* rt) const -> ChildRequirements;

  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void;

  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    std::array<RegionPair<state_t::LogicalRegion>, 3> const& states,
    st::subarray_predicates_array_t const& predicates) -> void;

  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    std::array<RegionPair<state_t::LogicalRegion>, 3> const& states,
    st::AllSubarraysQuery const& state_query) -> void;

  static auto
  preregister_tasks(L::TaskID task_id) -> void;

private:
  auto
  init_launcher(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    std::array<RegionPair<state_t::LogicalRegion>, 3> const& states,
    L::UntypedBuffer arg) -> void;

  auto
  update_launcher_for_subarray(
    L::Runtime* rt,
    PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
    unsigned subarray_slot) -> void;
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
