// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "StaticFields.hpp"
#include <rcp/rcp.hpp>

/*! \file GridTileRegion.hpp
 *
 * GridTileRegion definitions
 */
namespace rcp::symcam {

/*! \brief axes for (channel, w-plane, uv tile index) index */
enum class GridTileAxes {
  channel = LEGION_DIM_0, /*!> channel index */
  w_plane,                /*!> w-plane index */
  uv                      /*!> linearized index in UV dimensions */
};

/*! \brief IndexSpec specialization for GridTileAxes
 *
 * \tparam Coord index coordinate value type
 */
template <typename Coord>
using grid_tile_index_spec_t =
  IndexSpec<GridTileAxes, GridTileAxes::channel, GridTileAxes::uv, Coord>;

/*! \brief default static bounds for regions with GridTileAxes
 *
 * \tparam Coord index coordinate value type
 *
 * Bounds are channel: [0, Coord max]; w_plane: [0, Coord max], uv: [0, Coord
 * max]
 */
template <typename Coord>
constexpr auto default_grid_tile_bounds = std::array{
  IndexInterval<Coord>::left_bounded(0),
  IndexInterval<Coord>::left_bounded(0),
  IndexInterval<Coord>::left_bounded(0)};

/* \brief grid tile field definition */
using GridTileField = StaticField<L::Rect<2, coord_t>, field_ids::grid_tile>;

/*! \brief grid tile region definition */
struct GridTileRegion
  : public StaticRegionSpec<
      grid_tile_index_spec_t<coord_t>,
      default_grid_tile_bounds<coord_t>,
      GridTileField> {

  // GridTileField::value_t value dimension indexes
  static constexpr auto bound_x = 0;
  static constexpr auto bound_y = 1;

  struct Params {
    bool universal_uvw_tiles;
  };
  static constexpr L::SemanticTag params_tag = 20;

  static void
  attach_params(L::Runtime* rt, LogicalRegion region, Params const& params);

  [[nodiscard]] static auto
  has_params(L::Runtime* rt, LogicalRegion region) -> bool;

  [[nodiscard]] static auto
  retrieve_params(L::Runtime* rt, LogicalRegion region) -> Params const&;
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
