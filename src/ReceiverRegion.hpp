// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

/*! \file ReceiverRegion.hpp
 */
#include "Configuration.hpp"
#include "StaticFields.hpp"

namespace rcp::symcam {

/*! \brief receiver index space axes */
enum class ReceiverAxes { index = LEGION_DIM_0 };

/*! \brief field for receiver/antenna position */
using PositionField =
  StaticField<std::array<Configuration::uvw_value_t, 3>, field_ids::position>;

/*! \brief receivers position region definition
 *
 * 1-d region with a single field, PositionField
 */
using ReceiverRegion = StaticRegionSpec<
  IndexSpec<ReceiverAxes, ReceiverAxes::index, ReceiverAxes::index, int>,
  std::array{IndexInterval<int>::bounded(0, Configuration::num_receivers - 1)},
  PositionField>;

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
