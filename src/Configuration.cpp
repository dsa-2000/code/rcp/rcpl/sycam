// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "Configuration.hpp"
#include "GridVisibilitiesOnTiles.hpp"

#include <algorithm>
#include <cctype>
#include <cmath>
#include <filesystem>
#include <fstream>
#include <numeric>
#include <ranges>
#include <rcp/dc/rcpdc.hpp>
#include <regex>
#include <typeinfo>

#include <boost/core/demangle.hpp>
#include <boost/program_options.hpp>

namespace {
// configuration helper functions
//
auto
symcam_config_var(std::string_view ev) -> std::string {

  using namespace std::string_literals;

  std::string result;
  static std::string const symcam = "SYMCAM_";
  if (ev.starts_with(symcam)) {
    std::ranges::transform(
      std::string_view(symcam.c_str(), symcam.size() - 1),
      std::back_inserter(result),
      [](auto&& ch) { return std::tolower(ch); });
    result += ".";
    ev = ev.substr(symcam.size());
    for (auto&& pre : {"DC_"s, "XC_"s, "GR_"s, "IM_"s, "EG_"s}) {
      if (ev.starts_with(pre)) {
        std::ranges::transform(
          std::string_view(pre.c_str(), pre.size() - 1),
          std::back_inserter(result),
          [](auto&& ch) { return std::tolower(ch); });
        result += ".";
        ev = ev.substr(pre.size());
        for (auto&& sub : {"I_"s, "IQUV_"s}) {
          if (ev.starts_with(sub)) {
            std::ranges::transform(
              std::string_view(sub.c_str(), sub.size() - 1),
              std::back_inserter(result),
              [](auto&& ch) { return std::tolower(ch); });
            result += ".";
            ev = ev.substr(sub.size());
            std::string const test = "TEST_";
            if (ev.starts_with(test)) {
              std::ranges::transform(
                std::string_view(test.c_str(), test.size() - 1),
                std::back_inserter(result),
                [](auto&& ch) { return std::tolower(ch); });
              result += ".";
              ev = ev.substr(test.size());
              break;
            }
            break;
          }
        }
        break;
      }
    }
    std::ranges::transform(
      ev, std::back_inserter(result), [](auto&& ch) -> char {
        if (ch == '_')
          return '-';
        return std::tolower(ch);
      });
  }
  return result;
}

auto
input_cfgfile_var(std::string_view ev) -> std::string {
  std::string result;
  if (ev == "SYMCAM_CONFIG_FILE")
    result = "symcam.config-file";
  return result;
}

auto
usage() -> std::string const& {
  using namespace std::string_literals;
  static std::string const result =
    "symcam: Synthesis imaging camera application\n"s
    + "  (Contact: Martin Pokorny <mpokorny@caltech.edu>)\n\n"s
    + "Every command line option below corresponds with an environment\n"s
    + "variable named according to the pattern symcam.wx.yz.var-iable -> \n"s
    + "SYMCAM_WX_YZ_VAR_IABLE (e.g, symcam.sample-channel-range-offset -> \n"s
    + "SYMCAM_SAMPLE_CHANNEL_RANGE_OFFSET)\n\n"s + "Options";
  return result;
}

auto
operator<<(std::ostream& os, rcp::Insertable auto insertable) -> std::ostream& {
  return insertable.insert_to(os);
}

template <typename T>
struct Show {
  T tval;
  auto
  insert_to(std::ostream& os) const -> std::ostream& {
    return os << tval;
  }
};

struct ShowExpanded {
  rcp::symcam::VisibilityChannelsDescriptor desc;
  auto
  insert_to(std::ostream& os) -> std::ostream& {
    return os << desc.first << "/" << desc.width << "/" << desc.stride << "/"
              << desc.num_blocks;
  }
};

template <>
struct Show<rcp::symcam::VisibilityChannelsDescriptor> {
  rcp::symcam::VisibilityChannelsDescriptor desc;
  auto
  insert_to(std::ostream& os) const -> std::ostream& {
    os << desc.first;
    if (desc.width > 1 || desc.stride > 1) {
      os << "/" << desc.width;
      if (desc.num_blocks > 1 || desc.stride > 1) {
        os << "/" << desc.stride;
        if (desc.num_blocks != 0)
          os << "/" << desc.num_blocks;
      }
    }
    return os;
  }
};

template <typename T>
struct Show<std::vector<T>> {
  std::vector<T> tvect;
  auto
  insert_to(std::ostream& os) const -> std::ostream& {
    char const* sep = "";
    for (auto&& tval : tvect) {
      os << sep;
      Show<T>{tval}.insert_to(os);
      sep = " ";
    }
    return os;
  }
};

template <typename T>
Show(T&&) -> Show<std::remove_cvref_t<T>>;

template <typename S>
struct ShowV {
  std::vector<rcp::symcam::VisibilityChannelsDescriptor> vdesc;
  auto
  insert_to(std::ostream& os) -> std::ostream& {
    std::vector<std::string> vstr;
    std::ranges::transform(vdesc, std::back_inserter(vstr), [](auto&& vcd) {
      std::ostringstream oss;
      oss << S{vcd};
      return oss.str();
    });
    return os << Show{vstr};
  }
};

template <typename... Params>
auto
write_config(
  std::ofstream& of,
  std::string const& section,
  boost::program_options::options_description const& opts,
  Params&&... params) -> void {

  auto par = [&opts](auto&& nm_val) -> std::string {
    auto const& [nm, val] = nm_val;
    auto short_nm = std::string_view(&nm[nm.rfind('.') + 1]);
    std::ostringstream oss;
    oss << "# " << opts.find_nothrow(nm, false)->description() << '\n'
        << short_nm << '=' << Show(val) << '\n';
    return oss.str();
  };
  of << '[' << section << "]\n";
  (void)(of << ... << par(std::forward<Params>(params)));
}

} // namespace

namespace rcp {

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
template <>
auto
serialize(
  std::vector<rcp::symcam::GriddingConfiguration> const& val,
  void* buffer) -> std::size_t {

  char* ch = reinterpret_cast<char*>(buffer);
  auto sz = val.size();
  ch += serialize(sz, ch);
  for (auto&& gc : val)
    ch += gc.serialize(ch);
  return ch - reinterpret_cast<char*>(buffer);
}

template <>
auto
serialize(std::vector<rcp::symcam::ImageConfiguration> const& val, void* buffer)
  -> std::size_t {

  char* ch = reinterpret_cast<char*>(buffer);
  auto sz = val.size();
  ch += serialize(sz, ch);
  for (auto&& ic : val)
    ch += ic.serialize(ch);
  return ch - reinterpret_cast<char*>(buffer);
}

template <>
auto
deserialize(
  std::vector<rcp::symcam::GriddingConfiguration>& val,
  void const* buffer) -> std::size_t {

  char const* ch = reinterpret_cast<char const*>(buffer);
  std::size_t sz{};
  ch += deserialize(sz, ch);
  new (&val) std::vector<rcp::symcam::GriddingConfiguration>;
  val.resize(sz);
  for (auto&& gc : val)
    ch += gc.deserialize(ch);
  return ch - reinterpret_cast<char const*>(buffer);
}

template <>
auto
deserialize(
  std::vector<rcp::symcam::ImageConfiguration>& val,
  void const* buffer) -> std::size_t {

  char const* ch = reinterpret_cast<char const*>(buffer);
  std::size_t sz{};
  ch += deserialize(sz, ch);
  new (&val) std::vector<rcp::symcam::ImageConfiguration>;
  val.resize(sz);
  for (auto&& ic : val)
    ch += ic.deserialize(ch);
  return ch - reinterpret_cast<char const*>(buffer);
}
// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
} // end namespace rcp

namespace rcp::symcam {
using std::chrono::nanoseconds;

using namespace std::string_literals;

using namespace std::chrono_literals;

namespace po = boost::program_options;

auto
validate(
  boost::any& anyv,
  std::vector<std::string> const& values,
  std::vector<VisibilityChannelsDescriptor>* /*desc*/,
  int /*nada*/) -> void {

  // regex for whitespace-separated descriptors
  static std::regex word_regex("(?:\\s*((\\d|/)+))");
  // regex for a single descriptor
  static std::regex desc_regex(R"(^(\d+)(?:/(\d+))?(?:/(\d+))?(?:/(\d+))?$)");

  // Make sure no previous assignment to 'a' was made.
  po::validators::check_first_occurrence(anyv);

  std::vector<VisibilityChannelsDescriptor> descriptors;
  for (auto&& val : values) {
    if (!val.empty()) {
      std::string str = val;
      std::smatch words;
      while (std::regex_search(str, words, word_regex)) {
        auto w1str = words[1].str();
        std::smatch match;
        if (std::regex_match(w1str, match, desc_regex)) {
          switch (std::ranges::count_if(
            match, [](auto&& mtch) { return mtch.str().size() > 0; })) {
          case 2:
            descriptors.emplace_back(std::stoul(match[1]));
            break;
          case 3: {
            auto width = std::stoul(match[2]);
            if (width > 0)
              descriptors.emplace_back(
                std::stoul(match[1]), std::stoul(match[2]) - 1);
            else
              throw po::invalid_option_value(w1str);
            break;
          }
          case 4: {
            auto width = std::stoul(match[2]);
            auto stride = std::stoul(match[3]);
            if (width > 0 && stride >= width)
              descriptors.emplace_back(
                std::stoul(match[1]), width - 1, stride - width);
            else
              throw po::invalid_option_value(w1str);
            break;
          }
          case 5: {
            auto width = std::stoul(match[2]);
            auto stride = std::stoul(match[3]);
            auto num_blocks = std::stoul(match[4]);
            if (width > 0 && stride >= width && num_blocks > 0)
              descriptors.emplace_back(
                std::stoul(match[1]),
                width - 1,
                stride - width,
                num_blocks - 1);
            else
              throw po::invalid_option_value(w1str);
            break;
          }
          default:
            break;
          }
          str = words.suffix();
        } else {
          throw po::invalid_option_value(w1str);
        }
      }
    }
  }
  anyv = descriptors;
}

// to get program_options to parse space-separated sequences of strings as a
// std::vector<std::string>, we need to wrap std::string to prevent the built-in
// string vector parser from being used
struct Name {
  std::string s;
};

auto
validate(
  boost::any& anyv,
  std::vector<std::string> const& values,
  std::vector<Name>* /*name*/,
  int /*nada*/) -> void {

  // regex for whitespace-separated words
  static std::regex word_regex("(?:\\s*(\\S+))");

  // Make sure no previous assignment to 'a' was made.
  po::validators::check_first_occurrence(anyv);

  std::vector<Name> names;
  for (auto&& val : values) {
    if (!val.empty()) {
      std::string str = val;
      std::smatch words;
      while (std::regex_search(str, words, word_regex)) {
        names.push_back(Name{words[1].str()});
        str = words.suffix();
      }
    }
  }
  anyv = names;
}

using ShowVExpanded = ::ShowV<::ShowExpanded>;

// DataCaptureConfiguration non-member functions
//
auto
options(DataCaptureConfiguration& config) -> po::options_description {
  po::options_description result("data-capture options");
  result.add_options()(
    "symcam.dc.max-instances-per-shard",
    po::value(&config.max_instances_per_shard)
      ->default_value(config.max_instances_per_shard),
    "maximum number of data capture instances per shard")(
    "symcam.dc.packet-block-size",
    po::value(&config.packet_block_size)
      ->default_value(config.packet_block_size),
    "packet capture memory block size (number of packets)")(
    "symcam.dc.completion-queue-size",
    po::value(&config.completion_queue_size)
      ->default_value(config.completion_queue_size),
    "completion queue size (number of packets)")(
    "symcam.dc.fabric-provider",
    po::value(&config.fabric_provider)->default_value(config.fabric_provider),
    "fabric provider")(
    "symcam.dc.multicast-address",
    po::value(&config.multicast_address)
      ->default_value(config.multicast_address),
    "multicast address")(
    "symcam.dc.max-packet-arrival-skew-factor",
    po::value(&config.max_packet_arrival_skew_factor)
      ->default_value(config.max_packet_arrival_skew_factor),
    "maximum packet arrival skew factor")
#ifndef SYMCAM_RCF_SEND_MULTICAST
    ("symcam.dc.fast-multicast-period",
     po::value(&config.fast_multicast_period_sec)
       ->default_value(config.fast_multicast_period_sec),
     "fast multicast period (seconds)")(
      "symcam.dc.slow-multicast-period",
      po::value(&config.slow_multicast_period_sec)
        ->default_value(config.slow_multicast_period_sec),
      "slow multicast period (seconds)")
#endif
      ("symcam.dc.min-capture-interval",
       po::value(&config.min_capture_interval_ms)
         ->default_value(config.min_capture_interval_ms),
       "minimum capture interval (milliseconds)")(
        "symcam.dc.sweep-delay",
        po::value(&config.sweep_delay_ms)->default_value(config.sweep_delay_ms),
        "sweep delay (milliseconds)")(
        "symcam.dc.max-sweep-period",
        po::value(&config.max_sweep_period_ms)
          ->default_value(config.max_sweep_period_ms),
        "maximum sweep period (milliseconds)");
  return result;
}

// DataCaptureConfiguration member functions
//
DataCaptureConfiguration::DataCaptureConfiguration()
  : max_instances_per_shard(4)
  , packet_block_size(100'000)
  , completion_queue_size(50'000)
  , fabric_provider("udp")
  , multicast_address("239.255.1.100:12000")
  , max_packet_arrival_skew_factor(1)
  , fast_multicast_period_sec(1)
  , slow_multicast_period_sec(10)
  , sweep_delay_ms(1000)
  , max_sweep_period_ms(500)
  , min_capture_interval_ms(1000) {}

auto
DataCaptureConfiguration::write_config(std::ofstream& of) -> void {
  ::write_config(
    of,
    "symcam.dc",
    options(*this),
    std::make_tuple(
      "symcam.dc.max-instances-per-shard"s, max_instances_per_shard),
    std::make_tuple("symcam.dc.packet-block-size"s, packet_block_size),
    std::make_tuple("symcam.dc.completion-queue-size"s, completion_queue_size),
    std::make_tuple("symcam.dc.fabric-provider"s, fabric_provider),
    std::make_tuple("symcam.dc.multicast-address"s, multicast_address),
    std::make_tuple(
      "symcam.dc.max-packet-arrival-skew-factor"s,
      max_packet_arrival_skew_factor),
#ifndef SYMCAM_RCF_SEND_MULTICAST
    std::make_tuple(
      "symcam.dc.fast-multicast-period"s, fast_multicast_period_sec),
    std::make_tuple(
      "symcam.dc.slow-multicast-period"s, slow_multicast_period_sec),
#endif
    std::make_tuple("symcam.dc.sweep-delay"s, sweep_delay_ms),
    std::make_tuple("symcam.dc.max-sweep-period"s, max_sweep_period_ms),
    std::make_tuple(
      "symcam.dc.min-capture-interval"s, min_capture_interval_ms));
}

auto
DataCaptureConfiguration::serialized_size() const -> std::size_t {
  return rcp::serialized_size(max_instances_per_shard)
         + rcp::serialized_size(packet_block_size)
         + rcp::serialized_size(completion_queue_size)
         + rcp::serialized_size(fabric_provider)
         + rcp::serialized_size(multicast_address)
         + rcp::serialized_size(max_packet_arrival_skew_factor)
#ifndef SYMCAM_RCF_SEND_MULTICAST
         + rcp::serialized_size(fast_multicast_period_sec)
         + rcp::serialized_size(slow_multicast_period_sec)
#endif
         + rcp::serialized_size(sweep_delay_ms)
         + rcp::serialized_size(max_sweep_period_ms)
         + rcp::serialized_size(min_capture_interval_ms);
}

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
auto
DataCaptureConfiguration::serialize(void* buffer) const -> std::size_t {
  char* ch = reinterpret_cast<char*>(buffer);
  ch += rcp::serialize(max_instances_per_shard, ch);
  ch += rcp::serialize(packet_block_size, ch);
  ch += rcp::serialize(completion_queue_size, ch);
  ch += rcp::serialize(fabric_provider, ch);
  ch += rcp::serialize(multicast_address, ch);
  ch += rcp::serialize(max_packet_arrival_skew_factor, ch);
#ifndef SYMCAM_RCF_SEND_MULTICAST
  ch += rcp::serialize(fast_multicast_period_sec, ch);
  ch += rcp::serialize(slow_multicast_period_sec, ch);
#endif
  ch += rcp::serialize(sweep_delay_ms, ch);
  ch += rcp::serialize(max_sweep_period_ms, ch);
  ch += rcp::serialize(min_capture_interval_ms, ch);
  return ch - reinterpret_cast<char*>(buffer);
}

auto
DataCaptureConfiguration::deserialize(void const* buffer) -> std::size_t {
  char const* ch = reinterpret_cast<char const*>(buffer);
  ch += rcp::deserialize(max_instances_per_shard, ch);
  ch += rcp::deserialize(packet_block_size, ch);
  ch += rcp::deserialize(completion_queue_size, ch);
  ch += rcp::deserialize(fabric_provider, ch);
  ch += rcp::deserialize(multicast_address, ch);
  ch += rcp::deserialize(max_packet_arrival_skew_factor, ch);
#ifndef SYMCAM_RCF_SEND_MULTICAST
  ch += rcp::deserialize(fast_multicast_period_sec, ch);
  ch += rcp::deserialize(slow_multicast_period_sec, ch);
#endif
  ch += rcp::deserialize(sweep_delay_ms, ch);
  ch += rcp::deserialize(max_sweep_period_ms, ch);
  ch += rcp::deserialize(min_capture_interval_ms, ch);
  return ch - reinterpret_cast<char const*>(buffer);
}
// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)

// CrossCorrelationConfiguration non-member functions
//
auto
options(CrossCorrelationConfiguration& config) -> po::options_description {

  po::options_description result("cross-correlation options");
  result.add_options()(
    "symcam.xc.integration-factor",
    po::value(&config.integration_factor)
      ->default_value(config.integration_factor),
    "integration factor");
  return result;
}

// CrossCorrelationConfiguration member functions
//
CrossCorrelationConfiguration::CrossCorrelationConfiguration()
  : integration_factor(200'000) {}

auto
CrossCorrelationConfiguration::write_config(std::ofstream& of) -> void {
  ::write_config(
    of,
    "symcam.xc",
    options(*this),
    std::make_tuple("symcam.xc.integration-factor"s, integration_factor));
}

auto
CrossCorrelationConfiguration::serialized_size() const -> std::size_t {
  return rcp::serialized_size(integration_factor);
}

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
auto
CrossCorrelationConfiguration::serialize(void* buffer) const -> std::size_t {
  char* ch = reinterpret_cast<char*>(buffer);
  ch += rcp::serialize(integration_factor, ch);
  return ch - reinterpret_cast<char*>(buffer);
}

auto
CrossCorrelationConfiguration::deserialize(void const* buffer) -> std::size_t {
  char const* ch = reinterpret_cast<char const*>(buffer);
  ch += rcp::deserialize(integration_factor, ch);
  return ch - reinterpret_cast<char const*>(buffer);
}
// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)

// GriddingConfiguration non-member functions
//
auto
options(GriddingConfiguration& config) -> po::options_description {

  po::options_description result("'"s + config.name + "' gridding options");
  result.add_options()(
    config.stokes_mask_name().c_str(),
    po::value(&config.stokes_mask)->default_value(config.stokes_mask),
    "Stokes planes gridding mask")(
    config.test_cf_oversampling_factor_name().c_str(),
    po::value(&config.test.cf_oversampling_factor)
      ->default_value(config.test.cf_oversampling_factor),
    "convolution function oversampling factor")(
    config.test_cf_array_padding_name().c_str(),
    po::value(&config.test.cf_array_padding)
      ->default_value(config.test.cf_array_padding),
    "convolution function array padding")(
    config.test_cf_size_name().c_str(),
    po::value(&config.test.cf_size)->default_value(config.test.cf_size),
    "size of CF kernel")(
    config.vector_length_name().c_str(),
    po::value(&config.vector_length)->default_value(config.vector_length),
    "gridding kernel vector length");
  return result;
}

// GriddingConfiguration member functions
//
GriddingConfiguration::GriddingConfiguration(std::string_view const& name_)
  : name(name_)
  , stokes_mask({1, 1, 1, 1})
  , test({21, 30, 2})
  , vector_length(1) {}

auto
GriddingConfiguration::write_config(std::ofstream& of) -> void {
  auto opts = options(*this);
  ::write_config(
    of,
    "symcam.gr."s + name,
    opts,
    std::make_tuple(stokes_mask_name(), stokes_mask),
    std::make_tuple(vector_length_name(), vector_length));

  // test parameters section
  of << "\n# test/development gridding options\n";
  ::write_config(
    of,
    "symcam.gr."s + name + ".test",
    opts,
    std::make_tuple(test_cf_size_name(), test.cf_size),
    std::make_tuple(
      test_cf_oversampling_factor_name(), test.cf_oversampling_factor),
    std::make_tuple(test_cf_array_padding_name(), test.cf_array_padding));
}

auto
GriddingConfiguration::serialized_size() const -> std::size_t {
  return rcp::serialized_size(name) + rcp::serialized_size(stokes_mask)
         + rcp::serialized_size(test) + rcp::serialized_size(vector_length);
}

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
auto
GriddingConfiguration::serialize(void* buffer) const -> std::size_t {
  char* ch = reinterpret_cast<char*>(buffer);
  ch += rcp::serialize(name, ch);
  ch += rcp::serialize(stokes_mask, ch);
  ch += rcp::serialize(test, ch);
  ch += rcp::serialize(vector_length, ch);
  return ch - reinterpret_cast<char*>(buffer);
}

auto
GriddingConfiguration::deserialize(void const* buffer) -> std::size_t {
  char const* ch = reinterpret_cast<char const*>(buffer);
  ch += rcp::deserialize(name, ch);
  ch += rcp::deserialize(stokes_mask, ch);
  ch += rcp::deserialize(test, ch);
  ch += rcp::deserialize(vector_length, ch);
  return ch - reinterpret_cast<char const*>(buffer);
}
// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)

auto
GriddingConfiguration::stokes_mask_name() const -> std::string {
  return "symcam.gr."s + name + ".stokes-mask";
}

auto
GriddingConfiguration::test_cf_oversampling_factor_name() const -> std::string {
  return "symcam.gr."s + name + ".test.cf-oversampling-factor";
}

auto
GriddingConfiguration::test_cf_array_padding_name() const -> std::string {
  return "symcam.gr."s + name + ".test.cf-array-padding";
}

auto
GriddingConfiguration::test_cf_size_name() const -> std::string {
  return "symcam.gr."s + name + ".test.cf-size";
}

auto
GriddingConfiguration::vector_length_name() const -> std::string {
  return "symcam.gr."s + name + ".vector-length";
}

auto
GriddingConfiguration::grid_precision_type_info() -> std::type_info const& {
  return GridVisibilitiesOnTiles::grid_precision_type_info();
}

auto
GriddingConfiguration::grid_uses_compensated_summation() -> bool {
  return GridVisibilitiesOnTiles::grid_uses_compensated_summation();
}

auto
GriddingConfiguration::hpg_intermediate_grid_precision_type_info()
  -> std::type_info const& {
  return GridVisibilitiesOnTiles::hpg_intermediate_grid_precision_type_info();
}

auto
GriddingConfiguration::hpg_intermediate_weights_precision_type_info()
  -> std::type_info const& {
  return GridVisibilitiesOnTiles::
    hpg_intermediate_weights_precision_type_info();
}

auto
GriddingConfiguration::hpg_intermediate_weights_uses_compensated_summation()
  -> bool {
  return GridVisibilitiesOnTiles::
    hpg_intermediate_weights_uses_compensated_summation();
}

// ImageConfiguration non-member functions
//
auto
options(ImageConfiguration& config) -> po::options_description {

  po::options_description result("'"s + config.name + "' image options");
  std::ostringstream cd_desc;
  cd_desc << Show{config.channel_descriptors};
  result.add_options()(
    config.gridding_configuration_name().c_str(),
    po::value(&config.gridding_configuration)
      ->default_value(config.gridding_configuration),
    "gridding configuration name")(
    config.width_name().c_str(),
    po::value(&config.width)->default_value(config.width),
    "image width (px)")(
    config.height_name().c_str(),
    po::value(&config.height)->default_value(config.height),
    "image height (px)")(
    config.w_size_name().c_str(),
    po::value(&config.w_size)->default_value(config.w_size),
    "number of w-planes")(
    config.cell_width_arcsec_name().c_str(),
    po::value(&config.cell_width_arcsec)
      ->default_value(config.cell_width_arcsec),
    "UV cell width (arcsec)")(
    config.cell_height_arcsec_name().c_str(),
    po::value(&config.cell_height_arcsec)
      ->default_value(config.cell_height_arcsec),
    "UV cell height (arcsec)")(
    config.w_depth_arcsec_name().c_str(),
    po::value(&config.w_depth_arcsec)->default_value(config.w_depth_arcsec),
    "w layer size (arcsec)")(
    config.full_stokes_name().c_str(),
    po::value(&config.full_stokes)->default_value(config.full_stokes),
    "full Stokes image")(
    config.image_channel_offset_name().c_str(),
    po::value(&config.image_channel_offset)
      ->default_value(config.image_channel_offset),
    "offset of first image channel")(
    config.channel_descriptors_name().c_str(),
    po::value(&config.channel_descriptors)
      ->multitoken()
      ->default_value(config.channel_descriptors, cd_desc.str()),
    "visibility to image channel mapping");
  return result;
}

// ImageConfiguration member functions
//
ImageConfiguration::ImageConfiguration(std::string_view const& name_)
  : name(name_)
  , gridding_configuration(name_)
  , channel_descriptors({{0, 0, 0}})
  , width(5000)
  , height(5000)
  , w_size(1)
  , cell_width_arcsec(0.001)
  , cell_height_arcsec(0.001) {}

ImageConfiguration::ImageConfiguration(
  std::string_view const& name_,
  std::string_view const& gridding_configuration_,
  std::vector<VisibilityChannelsDescriptor> const& channel_descriptors_,
  unsigned width_,
  unsigned height_,
  unsigned w_size_,
  Configuration::uvw_value_t cell_width_arcsec_,
  Configuration::uvw_value_t cell_height_arcsec_,
  Configuration::uvw_value_t w_depth_arcsec_,
  bool full_stokes_,
  unsigned image_channel_offset_)
  : name(name_)
  , gridding_configuration(gridding_configuration_)
  , channel_descriptors(channel_descriptors_)
  , width(width_)
  , height(height_)
  , w_size(w_size_)
  , cell_width_arcsec(cell_width_arcsec_)
  , cell_height_arcsec(cell_height_arcsec_)
  , w_depth_arcsec(w_depth_arcsec_)
  , full_stokes(full_stokes_)
  , image_channel_offset{image_channel_offset_} {}

auto
ImageConfiguration::gridding_configuration_name() const -> std::string {
  return "symcam.im."s + name + ".gridding-configuration";
}
auto
ImageConfiguration::width_name() const -> std::string {
  return "symcam.im."s + name + ".width";
}
auto
ImageConfiguration::height_name() const -> std::string {
  return "symcam.im."s + name + ".height";
}
auto
ImageConfiguration::w_size_name() const -> std::string {
  return "symcam.im."s + name + ".w-size";
}
auto
ImageConfiguration::cell_width_arcsec_name() const -> std::string {
  return "symcam.im."s + name + ".cell-width";
}
auto
ImageConfiguration::cell_height_arcsec_name() const -> std::string {
  return "symcam.im."s + name + ".cell-height";
}
auto
ImageConfiguration::w_depth_arcsec_name() const -> std::string {
  return "symcam.im."s + name + ".w-depth";
}
auto
ImageConfiguration::full_stokes_name() const -> std::string {
  return "symcam.im."s + name + ".full-stokes";
}
auto
ImageConfiguration::channel_descriptors_name() const -> std::string {
  return "symcam.im."s + name + ".channels";
}
auto
ImageConfiguration::image_channel_offset_name() const -> std::string {
  return "symcam.im."s + name + ".image-channel-offset";
}

auto
ImageConfiguration::num_channels() const noexcept -> unsigned {
  auto nch = std::views::transform(
    channel_descriptors, [](auto&& cd) { return cd.num_blocks; });
  return std::accumulate(nch.begin(), nch.end(), 0U);
}

auto
ImageConfiguration::num_stokes() const noexcept -> unsigned {
  return full_stokes ? 4 : 1;
}

auto
ImageConfiguration::visibility_channel_blocks() const
  -> std::vector<std::array<rcp::dc::channel_type, 2>> {

  std::vector<std::array<rcp::dc::channel_type, 2>> result;
  result.reserve(num_channels());
  for (auto&& cd : channel_descriptors)
    std::ranges::move(cd.blocks(), std::back_inserter(result));
  return result;
}

auto
ImageConfiguration::write_config(std::ofstream& of) -> void {
  auto opts = options(*this);

  ::write_config(
    of,
    "symcam.im."s + name,
    opts,
    std::make_tuple(gridding_configuration_name(), gridding_configuration),
    std::make_tuple(channel_descriptors_name(), channel_descriptors),
    std::make_tuple(width_name(), width),
    std::make_tuple(height_name(), height),
    std::make_tuple(w_size_name(), w_size),
    std::make_tuple(cell_width_arcsec_name(), cell_width_arcsec),
    std::make_tuple(cell_height_arcsec_name(), cell_height_arcsec),
    std::make_tuple(w_depth_arcsec_name(), w_depth_arcsec),
    std::make_tuple(full_stokes_name(), full_stokes),
    std::make_tuple(image_channel_offset_name(), image_channel_offset));
}

auto
ImageConfiguration::serialized_size() const -> std::size_t {
  return rcp::serialized_size(name)
         + rcp::serialized_size(gridding_configuration)
         + rcp::serialized_size(channel_descriptors)
         + rcp::serialized_size(height) + rcp::serialized_size(w_size)
         + rcp::serialized_size(cell_width_arcsec)
         + rcp::serialized_size(cell_height_arcsec)
         + rcp::serialized_size(w_depth_arcsec)
         + rcp::serialized_size(full_stokes)
         + rcp::serialized_size(image_channel_offset);
}

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
auto
ImageConfiguration::serialize(void* buffer) const -> std::size_t {
  char* ch = reinterpret_cast<char*>(buffer);
  ch += rcp::serialize(name, ch);
  ch += rcp::serialize(gridding_configuration, ch);
  ch += rcp::serialize(channel_descriptors, ch);
  ch += rcp::serialize(width, ch);
  ch += rcp::serialize(height, ch);
  ch += rcp::serialize(w_size, ch);
  ch += rcp::serialize(cell_width_arcsec, ch);
  ch += rcp::serialize(cell_height_arcsec, ch);
  ch += rcp::serialize(w_depth_arcsec, ch);
  ch += rcp::serialize(full_stokes, ch);
  ch += rcp::serialize(image_channel_offset, ch);
  return ch - reinterpret_cast<char*>(buffer);
}

auto
ImageConfiguration::deserialize(void const* buffer) -> std::size_t {
  char const* ch = reinterpret_cast<char const*>(buffer);
  ch += rcp::deserialize(name, ch);
  ch += rcp::deserialize(gridding_configuration, ch);
  ch += rcp::deserialize(channel_descriptors, ch);
  ch += rcp::deserialize(width, ch);
  ch += rcp::deserialize(height, ch);
  ch += rcp::deserialize(w_size, ch);
  ch += rcp::deserialize(cell_width_arcsec, ch);
  ch += rcp::deserialize(cell_height_arcsec, ch);
  ch += rcp::deserialize(w_depth_arcsec, ch);
  ch += rcp::deserialize(full_stokes, ch);
  ch += rcp::deserialize(image_channel_offset, ch);
  return ch - reinterpret_cast<char const*>(buffer);
}
// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)

// VisibilityChannelsDescriptor member functions
//

VisibilityChannelsDescriptor::VisibilityChannelsDescriptor(
  rcp::dc::channel_type first_)
  : first(first_), width(1), stride(1), num_blocks(1) {}

VisibilityChannelsDescriptor::VisibilityChannelsDescriptor(
  rcp::dc::channel_type first_, rcp::dc::channel_type width1)
  : first(first_), width(width1 + 1), stride(width), num_blocks(1) {}

VisibilityChannelsDescriptor::VisibilityChannelsDescriptor(
  rcp::dc::channel_type first_,
  rcp::dc::channel_type width1,
  rcp::dc::channel_type gap,
  rcp::dc::channel_type blocks1)
  : first(first_)
  , width(width1 + 1)
  , stride(width + gap)
  , num_blocks(blocks1 + 1) {}

auto
VisibilityChannelsDescriptor::bounds() const
  -> std::array<rcp::dc::channel_type, 2> {
  return {
    first,
    static_cast<rcp::dc::channel_type>(
      first + (num_blocks - 1) * stride + width - 1)};
}

auto
VisibilityChannelsDescriptor::blocks() const
  -> std::vector<std::array<rcp::dc::channel_type, 2>> {

  std::vector<std::array<rcp::dc::channel_type, 2>> result(num_blocks);
  std::ranges::generate(
    result,
    [start = first, this]() mutable -> std::array<rcp::dc::channel_type, 2> {
      auto result = std::array{
        start, static_cast<rcp::dc::channel_type>(start + width - 1)};
      start += stride;
      return result;
    });
  return result;
}

auto
VisibilityChannelsDescriptor::find_block_index(
  rcp::dc::channel_type channel) const noexcept -> std::optional<unsigned> {

  if (channel < first)
    return std::nullopt;
  rcp::dc::channel_type dch = channel - first;
  auto blk = floor_div(dch, stride);
  if (num_blocks > 0 && blk >= num_blocks)
    return std::nullopt;
  auto blk_lo = blk * stride + first;
  auto blk_hi = blk * stride + first + width - 1;
  if (blk_lo <= channel && channel <= blk_hi)
    return blk;
  return std::nullopt;
}

auto
ImagingConfiguration::serialized_size() const -> std::size_t {
  auto result = gr.serialized_size() + im.serialized_size();
  assert(result <= max_serialized_size);
  return result;
}

auto
ImagingConfiguration::serialize(void* buffer) const -> std::size_t {
  char* ch = reinterpret_cast<char*>(buffer);
  ch += gr.serialize(ch);
  ch += im.serialize(ch);
  return ch - reinterpret_cast<char*>(buffer);
}

auto
ImagingConfiguration::deserialize(void const* buffer) -> std::size_t {
  char const* ch = reinterpret_cast<char const*>(buffer);
  ch += gr.deserialize(ch);
  ch += im.deserialize(ch);
  return ch - reinterpret_cast<char const*>(buffer);
}

// ExportGridConfiguration non-member functions
//
auto
options(ExportGridConfiguration& config) -> po::options_description {

  po::options_description result("export-grid options");
  result.add_options()(
    "symcam.eg.channel-block-size",
    po::value(&config.channel_block_size)
      ->default_value(config.channel_block_size),
    "number channels per partition")(
    "symcam.eg.stokes-block-size",
    po::value(&config.stokes_block_size)
      ->default_value(config.stokes_block_size),
    "number Stokes products per partition");
  return result;
}

// ExportGridConfiguration member functions
//
auto
ExportGridConfiguration::write_config(std::ofstream& of) -> void {
  ::write_config(
    of,
    "symcam.eg",
    options(*this),
    std::make_tuple("symcam.eg.channel-block-size"s, channel_block_size),
    std::make_tuple("symcam.eg.stokes-block-size"s, stokes_block_size));
}

auto
ExportGridConfiguration::serialized_size() const -> std::size_t {
  return rcp::serialized_size(channel_block_size)
         + rcp::serialized_size(stokes_block_size);
}

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
auto
ExportGridConfiguration::serialize(void* buffer) const -> std::size_t {
  char* ch = reinterpret_cast<char*>(buffer);
  ch += rcp::serialize(channel_block_size, ch);
  ch += rcp::serialize(stokes_block_size, ch);
  return ch - reinterpret_cast<char*>(buffer);
}

auto
ExportGridConfiguration::deserialize(void const* buffer) -> std::size_t {
  char const* ch = reinterpret_cast<char const*>(buffer);
  ch += rcp::deserialize(channel_block_size, ch);
  ch += rcp::deserialize(stokes_block_size, ch);
  return ch - reinterpret_cast<char const*>(buffer);
}
// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)

// Configuration non-member functions
//
auto
options(Configuration& config) -> po::options_description {

  po::options_description result("top-level options");
  result.add_options()(
    "symcam.rcp-instance-id",
    po::value(&config.rcp_instance_id)->default_value(config.rcp_instance_id),
    "name of RCP instance (optional value)")(
    "symcam.num-sample-channels-full-band",
    po::value(&config.num_sample_channels_full_band)
      ->default_value(config.num_sample_channels_full_band),
    "number of sample channels across full band")(
    "symcam.sample-channel0-frequency",
    po::value(&config.sample_channel0_frequency_hz)
      ->default_value(config.sample_channel0_frequency_hz),
    "sample channel 0 frequency (Hz)")(
    "symcam.sample-channel-width",
    po::value(&config.sample_channel_width_hz)
      ->default_value(config.sample_channel_width_hz),
    "sample channel width (Hz)")(
    "symcam.sample-channel-range-offset",
    po::value(&config.sample_channel_range_offset)
      ->default_value(config.sample_channel_range_offset),
    "offset of first sample channel")(
    "symcam.sample-channel-range-size",
    po::value(&config.sample_channel_range_size)
      ->default_value(config.sample_channel_range_size),
    "number of sample channels")(
    "symcam.array-u-extent",
    po::value(config.array_extent_m.data())
      ->default_value(config.array_extent_m[0]),
    "extent in u direction of array baselines (m)")(
    "symcam.array-v-extent",
    po::value(&config.array_extent_m[1])
      ->default_value(config.array_extent_m[1]),
    "extent in v direction of array baselines (m)")(
    "symcam.min-fill-report",
    po::value(&config.min_fill_report_interval_sec)
      ->default_value(config.min_fill_report_interval_sec),
    "minimum fill fraction report interval (seconds)")(
    "symcam.initial-events-file",
    po::value(&config.initial_events_file)->default_value(""),
    "initial events script file path")(
    "symcam.state-capture-offset",
    po::value(&config.state_capture_offset)
      ->default_value(config.state_capture_offset),
    "offset of data and state capture")(
    "symcam.live-capture-offset",
    po::value(&config.live_capture_offset)
      ->default_value(config.live_capture_offset),
    "offset of data and liveness capture")(
    "symcam.child-task-per-integration",
    po::value(&config.child_task_per_integration)
      ->default_value(config.child_task_per_integration),
    "launch child task per visibilities integration");
  return result;
}

auto
component_options(
  std::vector<std::string>& gr_names,
  std::vector<std::string>& im_names,
  std::vector<std::string> const& default_gr_names = {},
  std::vector<std::string> const& default_im_names = {})
  -> po::options_description {

  std::ostringstream gr_oss;
  gr_oss << Show(default_gr_names);
  std::ostringstream im_oss;
  im_oss << Show(default_im_names);
  po::options_description result("component name options");
  // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)
  auto& grs = reinterpret_cast<std::vector<Name>&>(gr_names);
  auto& ims = reinterpret_cast<std::vector<Name>&>(im_names);
  auto const& grds =
    reinterpret_cast<std::vector<Name> const&>(default_gr_names);
  auto const& imds =
    reinterpret_cast<std::vector<Name> const&>(default_im_names);
  // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)
  result.add_options()(
    "symcam.gr.names",
    po::value(&grs)->multitoken()->composing()->default_value(
      grds, gr_oss.str()),
    "gridding configuration names")(
    "symcam.im.names",
    po::value(&ims)->multitoken()->composing()->default_value(
      imds, im_oss.str()),
    "image names");
  return result;
}

// Configuration member functions
//
Configuration::Configuration(int argc, char* const* argv, bool no_write)
  : num_sample_channels_full_band{16}
  , sample_channel_range_size(16)
  , sample_channel0_frequency_hz(7.0e8)
  , sample_channel_width_hz(70000.0)
  , array_extent_m{15'000, 19'000, 0}
  , min_fill_report_interval_sec(1) {

  po::options_description generic("generic options");
  generic.add_options()("help", "produce help message")(
    "dry-run,n", po::bool_switch(&dry_run), "show configuration, then exit")(
    "show", po::bool_switch(&show), "show configuration");

  std::vector<std::string> gr_names;
  std::vector<std::string> default_gr_names{"std"};
  std::vector<std::string> im_names;
  std::vector<std::string> default_im_names{"std"};
  auto component_names =
    component_options(gr_names, im_names, default_gr_names, default_im_names);

  bool update_config = false;
  std::string config_out;
  po::options_description input("input options");
  input.add_options()(
    "symcam.config-file,C", po::value(&input_file), "configuration file");
  if (!no_write)
    input.add_options()(
      "update-config-file,u",
      po::bool_switch(&update_config),
      "update configuration file")(
      "write-config-file,w", po::value(&config_out), "new configuration file");

  // handle input options
  try {
    po::variables_map input_vm;
    po::store(
      po::command_line_parser(argc, argv)
        .options(input)
        .allow_unregistered()
        .run(),
      input_vm);
    po::store(po::parse_environment(input, input_cfgfile_var), input_vm);
    po::notify(input_vm);
  } catch (boost::program_options::error const&) {
  }

  // first scan to determine gridding and image names
  po::options_description setup("setup options");
  setup.add(input).add(component_names);
  try {
    po::variables_map setup_vm;
    po::store(
      po::command_line_parser(argc, argv)
        .options(setup)
        .allow_unregistered()
        .run(),
      setup_vm);
    po::store(po::parse_environment(setup, symcam_config_var), setup_vm);
    if (!input_file.empty())
      po::store(
        po::parse_config_file(input_file.c_str(), setup, true), setup_vm);
    po::notify(setup_vm);
  } catch (boost::program_options::error const&) {
  }

  for (auto&& nm : gr_names)
    gr.emplace_back(nm);
  for (auto&& nm : im_names)
    im.emplace_back(nm);
  auto config = options(*this);
  config.add(options(dc))
    .add(options(xc))
    .add(options(eg))
    .add(component_names);
  for (auto&& g : gr)
    config.add(options(g));
  for (auto&& i : im)
    config.add(options(i));

  po::options_description desc(usage());
  desc.add(generic).add(input).add(config);

  // now do option parsing
  po::variables_map vm;
  try {
    po::store(
      po::command_line_parser(argc, argv)
        .options(desc)
        .allow_unregistered()
        .run(),
      vm);
    po::store(po::parse_environment(desc, symcam_config_var), vm);
    if (!input_file.empty()) {
      po::options_description cfgfile_options;
      cfgfile_options.add(config);
      po::store(po::parse_config_file(input_file.c_str(), cfgfile_options), vm);
    }
    po::notify(vm);
    if (vm.count("help") > 0) {
      std::ostringstream oss;
      oss << desc;
      help = oss.str();
    }
  } catch (boost::program_options::error const& e) {
    error = e.what();
  }

  sample_channel_range_offset = rcp::round_down(
    sample_channel_range_offset,
    DataPacketConfiguration::num_channels_per_packet);
  sample_channel_range_size = rcp::round_up(
    sample_channel_range_size,
    DataPacketConfiguration::num_channels_per_packet);

  if (
    xc.integration_factor % CrossCorrelationConfiguration::timesteps_block_size
    != 0) {
    if (do_run())
      throw CorrelationNotMultipleOfBlock(*this);
    else
      log().warn(CorrelationNotMultipleOfBlock(*this).what());
  }
  if (
    DataPacketConfiguration::num_timesteps_per_packet
    <= xc.integration_factor) {
    if (
      xc.integration_factor % DataPacketConfiguration::num_timesteps_per_packet
      != 0) {
      if (do_run())
        throw CorrelationNotMultipleOfPacket(*this);
      else
        log().warn(CorrelationNotMultipleOfPacket(*this).what());
    }
  } else {
    if (
      DataPacketConfiguration::num_timesteps_per_packet % xc.integration_factor
      != 0) {
      if (do_run())
        throw PacketNotMultipleOfCorrelation(*this);
      else
        log().warn(PacketNotMultipleOfCorrelation(*this).what());
    }
  }

  if (correlation_interval() > visibility_interval())
    throw InvalidVisibilityInterval(*this);

  if (error.empty() && do_run()) {

    if (
      !initial_events_file.empty()
      && !std::filesystem::exists(initial_events_file))
      throw InitialEventsFileNotFound(*this);

    assert(input_file.size() < 200);
    assert(initial_events_file.size() < 200);
    assert(dc.multicast_address.size() < 40);
    assert(dc.fabric_provider.size() < 40);
    assert(
      ConfigurationSerdez::serialized_size(*this)
      <= ConfigurationSerdez::MAX_SERIALIZED_SIZE);
  }

  if (error.empty() && help.empty()) {
    if (update_config && !input_file.empty() && config_out.empty()) {
      if (std::filesystem::exists(input_file))
        std::filesystem::copy_file(input_file, input_file + ".bak");
      std::ofstream f(input_file, std::ios::trunc | std::ios::out);
      write_config(f);
    } else if (!config_out.empty()) {
      if (std::filesystem::exists(config_out))
        std::filesystem::copy_file(config_out, config_out + ".bak");
      std::ofstream f(config_out, std::ios::trunc | std::ios::out);
      write_config(f);
    }
  }
}

auto
Configuration::sweep_period() const -> std::chrono::milliseconds {
  return std::min(
    std::chrono::milliseconds(dc.max_sweep_period_ms),
    std::chrono::ceil<std::chrono::milliseconds>(capture_interval() / 2));
}

auto
Configuration::capture_interval() const
  -> DataPacketConfiguration::fsample_duration_t {

  return std::chrono::duration_cast<
    DataPacketConfiguration::fsample_duration_t>(visibility_interval());
}

auto
Configuration::fill_report_period() const -> unsigned {

  return rcp::ceil_div(
    std::uint64_t(std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::seconds(min_fill_report_interval_sec))
                    .count()),
    std::uint64_t(
      std::chrono::duration_cast<std::chrono::nanoseconds>(capture_interval())
        .count()));
}

auto
Configuration::correlation_interval() const
  -> DataPacketConfiguration::fsample_duration_t {

  return DataPacketConfiguration::fsample_duration_t(xc.integration_factor);
}

auto
Configuration::visibility_interval() const -> std::chrono::nanoseconds {

  return correlation_interval();
}

auto
Configuration::max_correlation_timesteps_per_capture_interval() const
  -> std::size_t {
  using unano_t = std::make_unsigned_t<std::chrono::nanoseconds::rep>;
  return rcp::ceil_div(
    unano_t(
      std::chrono::duration_cast<std::chrono::nanoseconds>(capture_interval())
        .count()),
    unano_t(std::chrono::duration_cast<std::chrono::nanoseconds>(
              correlation_interval())
              .count()));
}

auto
Configuration::visibility_integration_factor() const -> std::uint64_t {
  return visibility_interval() / correlation_interval();
}

auto
Configuration::imaging_configurations() const
  -> std::vector<ImagingConfiguration> {

  auto reify = [lo = sample_channel_range_offset,
                hi = sample_channel_range_offset + sample_channel_range_size
                     - 1](auto&& in)
    -> std::tuple<
      std::optional<unsigned>,
      std::vector<VisibilityChannelsDescriptor>> {
    std::vector<VisibilityChannelsDescriptor> out;
    std::optional<rcp::dc::channel_type> first;
    rcp::dc::channel_type last{};
    std::optional<unsigned> initial_block_index;
    for (auto&& ch :
         std::views::iota(lo, static_cast<rcp::dc::channel_type>(hi + 1))) {
      auto maybe_blk = in.find_block_index(ch);
      if (maybe_blk) {
        if (!initial_block_index)
          initial_block_index = *maybe_blk;
        if (!first) {
          first = ch;
          last = ch;
        } else if (ch == last + 1) {
          last = ch;
        } else {
          out.emplace_back(*first, last - *first);
          first = ch;
          last = ch;
        }
      }
    }
    if (first)
      out.emplace_back(*first, last - *first);
    assert(bool(initial_block_index) == !out.empty());
    return {initial_block_index, out};
  };
  std::vector<ImagingConfiguration> result;
  for (auto&& imcfg : im) {
    ImageConfiguration new_ic = imcfg;
    assert(new_ic.channel_descriptors.size() == 1);
    auto [imgch_offset, vch_desc] = reify(new_ic.channel_descriptors.front());
    if (imgch_offset) {
      new_ic.image_channel_offset = *imgch_offset;
      new_ic.channel_descriptors = vch_desc;
      auto grcfg = std::ranges::find_if(gr, [&](auto&& gc) {
        return gc.name == new_ic.gridding_configuration;
      });
      if (grcfg != gr.end())
        assert(result.size() < max_num_imaging_configurations);
      result.emplace_back(*grcfg, new_ic);
    }
  }
  return result;
}

auto
Configuration::visibility_channels_partition_kind(
  ImageConfiguration const& img) const -> L::PartitionKind {

  auto const& cd = img.channel_descriptors;
  if (cd.empty())
    return LEGION_DISJOINT_INCOMPLETE_KIND;

  auto properties =
    [this](auto&& desc) -> std::array<bool, 2> /* disjoint, complete */ {
    auto [lo, hi] = desc.bounds();
    assert(sample_channel_range_offset <= lo);
    assert(hi < sample_channel_range_offset + sample_channel_range_size);
    return {
      desc.width <= desc.stride,
      desc.width >= desc.stride && lo == sample_channel_range_size
        && hi == sample_channel_range_offset + sample_channel_range_size - 1};
  };

  auto kind = properties(cd.front());
  auto end = cd.front().bounds()[1];
  for (auto&& desc : std::ranges::drop_view(cd, 1)) {
    auto props = properties(desc);
    auto [lo, hi] = desc.bounds();
    kind = {kind[0] && props[0] && end < lo, kind[1] && props[1] && lo <= end};
    end = hi;
  }
  if (kind == std::array{true, true})
    return LEGION_DISJOINT_COMPLETE_KIND;
  if (kind == std::array{true, false})
    return LEGION_DISJOINT_INCOMPLETE_KIND;
  log().fatal("aliased visibility channel partitions should not occur");
  return LEGION_COMPUTE_KIND;
}

auto
Configuration::image_channel_configurations(ImageConfiguration const& im) const
  -> std::vector<std::array<frequency_t, 2>> {

  std::vector<std::array<frequency_t, 2>> result;
  auto vcbs = im.visibility_channel_blocks();
  result.reserve(vcbs.size());
  std::ranges::transform(vcbs, std::back_inserter(result), [this](auto&& vcb) {
    auto& [lo, hi] = vcb;
    return std::array{
      sample_channel0_frequency_hz + sample_channel_width_hz * lo,
      sample_channel0_frequency_hz + sample_channel_width_hz * hi};
  });
  return result;
}

auto
Configuration::visibility_channel_scale(
  unsigned channel,
  ImageConfiguration const& im) const -> std::array<uvw_value_t, 3> {

  auto c_to_m = cycles_to_meters(channel);
  return {
    c_to_m(uvw_value_t{1.0} / im.cell_width_arcsec),
    c_to_m(uvw_value_t{1.0} / im.cell_height_arcsec),
    c_to_m(uvw_value_t{1.0} / im.w_depth_arcsec)};
}

auto
Configuration::image_channel_scale(
  unsigned channel,
  ImageConfiguration const& im) const -> std::array<uvw_value_t, 3> {

  auto vcb = im.visibility_channel_blocks();
  assert(im.image_channel_offset <= channel);
  assert(channel < vcb.size() + im.image_channel_offset);
  return visibility_channel_scale(
    vcb[channel - im.image_channel_offset][1], im);
}

auto
Configuration::pipeline_instance_id() const -> std::string {
  std::ostringstream oss;
  if (!rcp_instance_id.empty())
    oss << rcp_instance_id << "-";
  oss << sample_channel_range_offset << "-"
      << sample_channel_range_offset + sample_channel_range_size - 1;
  return oss.str();
}

auto
Configuration::do_show() const -> bool {
  return !error.empty() || !help.empty() || show || dry_run;
}

auto
Configuration::do_run() const -> bool {
  return !dry_run && error.empty() && help.empty();
}

auto
Configuration::write_config(std::ofstream& of) -> void {
  of << "##\n## symcam configuration file\n##\n"
     << "\n#\n# top-level options\n#\n";
  ::write_config(
    of,
    "symcam",
    options(*this),
    std::make_tuple("symcam.rcp-instance-id"s, rcp_instance_id),
    std::make_tuple(
      "symcam.num-sample-channels-full-band"s, num_sample_channels_full_band),
    std::make_tuple(
      "symcam.sample-channel0-frequency"s, sample_channel0_frequency_hz),
    std::make_tuple("symcam.sample-channel-width"s, sample_channel_width_hz),
    std::make_tuple(
      "symcam.sample-channel-range-offset"s, sample_channel_range_offset),
    std::make_tuple(
      "symcam.sample-channel-range-size"s, sample_channel_range_size),
    std::make_tuple("symcam.array-u-extent"s, array_extent_m[0]),
    std::make_tuple("symcam.array-v-extent"s, array_extent_m[1]),
    std::make_tuple("symcam.min-fill-report"s, min_fill_report_interval_sec),
    std::make_tuple("symcam.initial-events-file"s, initial_events_file),
    std::make_tuple("symcam.state-capture-offset"s, state_capture_offset),
    std::make_tuple("symcam.live-capture-offset"s, live_capture_offset),
    std::make_tuple(
      "symcam.child-task-per-integration"s, child_task_per_integration));

  of << "\n#\n# data-capture options\n#\n";
  dc.write_config(of);
  of << "\n#\n# cross-correlation options\n#\n";
  xc.write_config(of);

  std::vector<std::string> gr_names;
  std::ranges::transform(
    gr, std::back_inserter(gr_names), [](auto&& gc) { return gc.name; });
  std::vector<std::string> im_names;
  std::ranges::transform(
    im, std::back_inserter(im_names), [](auto&& ic) { return ic.name; });
  auto component_opts = component_options(gr_names, im_names);
  of << "\n#\n# global gridding options\n#\n";
  ::write_config(
    of,
    "symcam.gr",
    component_opts,
    std::make_tuple("symcam.gr.names"s, gr_names));
  for (auto&& gc : gr) {
    of << "\n#\n# '" << gc.name << "' gridding options\n#\n";
    gc.write_config(of);
  }

  of << "\n#\n# global image options\n#\n";
  ::write_config(
    of,
    "symcam.im",
    component_opts,
    std::make_tuple("symcam.im.names"s, im_names));
  for (auto&& ic : im) {
    of << "\n#\n# '" << ic.name << "' image options\n#\n";
    ic.write_config(of);
  }

  of << "\n#\n# export-grid options\n#\n";
  eg.write_config(of);
}

auto
Configuration::serialized_size() const -> std::size_t {

  std::size_t result =
    rcp::serialized_size(show) + rcp::serialized_size(dry_run)
    + rcp::serialized_size(error) + rcp::serialized_size(help)
    + rcp::serialized_size(input_file) + rcp::serialized_size(rcp_instance_id)
    + rcp::serialized_size(num_sample_channels_full_band)
    + rcp::serialized_size(sample_channel_range_offset)
    + rcp::serialized_size(sample_channel_range_size)
    + rcp::serialized_size(sample_channel0_frequency_hz)
    + rcp::serialized_size(sample_channel_width_hz)
    + rcp::serialized_size(array_extent_m)
    + rcp::serialized_size(min_fill_report_interval_sec)
    + rcp::serialized_size(state_capture_offset)
    + rcp::serialized_size(live_capture_offset)
    + rcp::serialized_size(initial_events_file)
    + rcp::serialized_size(initial_events_script) + dc.serialized_size()
    + xc.serialized_size() + eg.serialized_size()
    + rcp::serialized_size(child_task_per_integration);
  for (auto&& gc : gr)
    result += gc.serialized_size();
  for (auto&& ic : im)
    result += ic.serialized_size();
  return result;
}

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
auto
Configuration::serialize(void* buffer) const -> std::size_t {
  char* ch = reinterpret_cast<char*>(buffer);
  ch += rcp::serialize(show, ch);
  ch += rcp::serialize(dry_run, ch);
  ch += rcp::serialize(error, ch);
  ch += rcp::serialize(help, ch);
  ch += rcp::serialize(input_file, ch);
  ch += rcp::serialize(rcp_instance_id, ch);
  ch += rcp::serialize(num_sample_channels_full_band, ch);
  ch += rcp::serialize(sample_channel_range_offset, ch);
  ch += rcp::serialize(sample_channel_range_size, ch);
  ch += rcp::serialize(sample_channel0_frequency_hz, ch);
  ch += rcp::serialize(sample_channel_width_hz, ch);
  ch += rcp::serialize(array_extent_m, ch);
  ch += rcp::serialize(min_fill_report_interval_sec, ch);
  ch += rcp::serialize(state_capture_offset, ch);
  ch += rcp::serialize(live_capture_offset, ch);
  ch += rcp::serialize(initial_events_file, ch);
  ch += rcp::serialize(initial_events_script, ch);
  ch += dc.serialize(ch);
  ch += xc.serialize(ch);
  ch += rcp::serialize(gr, ch);
  ch += rcp::serialize(im, ch);
  ch += rcp::serialize(eg, ch);
  ch += rcp::serialize(child_task_per_integration, ch);
  return ch - reinterpret_cast<char*>(buffer);
}

auto
Configuration::deserialize(void const* buffer) -> std::size_t {
  char const* ch = reinterpret_cast<char const*>(buffer);
  ch += rcp::deserialize(show, ch);
  ch += rcp::deserialize(dry_run, ch);
  ch += rcp::deserialize(error, ch);
  ch += rcp::deserialize(help, ch);
  ch += rcp::deserialize(input_file, ch);
  ch += rcp::deserialize(rcp_instance_id, ch);
  ch += rcp::deserialize(num_sample_channels_full_band, ch);
  ch += rcp::deserialize(sample_channel_range_offset, ch);
  ch += rcp::deserialize(sample_channel_range_size, ch);
  ch += rcp::deserialize(sample_channel0_frequency_hz, ch);
  ch += rcp::deserialize(sample_channel_width_hz, ch);
  ch += rcp::deserialize(array_extent_m, ch);
  ch += rcp::deserialize(min_fill_report_interval_sec, ch);
  ch += rcp::deserialize(state_capture_offset, ch);
  ch += rcp::deserialize(live_capture_offset, ch);
  ch += rcp::deserialize(initial_events_file, ch);
  ch += rcp::deserialize(initial_events_script, ch);
  ch += dc.deserialize(ch);
  ch += xc.deserialize(ch);
  ch += rcp::deserialize(gr, ch);
  ch += rcp::deserialize(im, ch);
  ch += rcp::deserialize(eg, ch);
  ch += rcp::deserialize(child_task_per_integration, ch);
  return ch - reinterpret_cast<char const*>(buffer);
}
// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)

// ConfigurationSerdez member functions
//
auto
ConfigurationSerdez::serialized_size(Configuration const& val) -> std::size_t {
  return val.serialized_size();
}

auto
ConfigurationSerdez::serialize(Configuration const& val, void* buffer)
  -> std::size_t {
  return val.serialize(buffer);
}

auto
ConfigurationSerdez::deserialize(Configuration& val, void const* buffer)
  -> std::size_t {
  new (&val) Configuration;
  return val.deserialize(buffer);
}

void
ConfigurationSerdez::destroy(Configuration& val) {
  val.~Configuration();
}

CorrelationNotMultipleOfBlock::CorrelationNotMultipleOfBlock(
  Configuration const& config)
  : std::runtime_error(
      "Correlation integration factor ("
      + std::to_string(config.xc.integration_factor)
      + ") is not a multiple of the correlator timesteps block size ("
      + std::to_string(CrossCorrelationConfiguration::timesteps_block_size)
      + ")") {}

PacketNotMultipleOfCorrelation::PacketNotMultipleOfCorrelation(
  Configuration const& config)
  : std::runtime_error(
      "Data capture packet timesteps size ("
      + std::to_string(DataPacketConfiguration::num_timesteps_per_packet)
      + ") is not a multiple of correlation integration factor ("
      + std::to_string(config.xc.integration_factor) + ")") {}

CorrelationNotMultipleOfPacket::CorrelationNotMultipleOfPacket(
  Configuration const& config)
  : std::runtime_error(
      "Correlation integration factor ("
      + std::to_string(config.xc.integration_factor)
      + ") is not a multiple of data capture packet timesteps size ("
      + std::to_string(DataPacketConfiguration::num_timesteps_per_packet)
      + ")") {}

InvalidVisibilityInterval::InvalidVisibilityInterval(
  Configuration const& /*config*/)
  : std::runtime_error(
      "Visibility interval computed from configuration is not valid") {}

InitialEventsFileNotFound::InitialEventsFileNotFound(
  Configuration const& config)
  : std::runtime_error(
      "Path to initial events file not found: " + config.initial_events_file) {}

// Stokes stream insertion
//
auto
operator<<(std::ostream& os, StokesMask const& sto) -> std::ostream& {

  if (sto.val[Stokes::I] > 0)
    os << 'I';
  if (sto.val[Stokes::Q] > 0)
    os << 'Q';
  if (sto.val[Stokes::U] > 0)
    os << 'U';
  if (sto.val[Stokes::V] > 0)
    os << 'V';
  return os;
}

auto
operator>>(std::istream& is, StokesMask& sto) -> std::istream& {

  // regex for Stokes descriptor
  static std::regex regex("(?:(I)?(Q)?(U)?(V)?)");

  static constexpr auto max_sto_str_len = 5; // "IQUV" including '\0'

  sto = StokesMask{};
  // NOLINTNEXTLINE(modernize-avoid-c-arrays,
  // cppcoreguidelines-avoid-c-arrays)
  char buff[max_sto_str_len];
  if (is >> buff) {
    std::string str(buff);
    std::smatch match;
    if (std::regex_match(str, match, regex)) {
      if (match[1].matched)
        sto.val[Stokes::I] = 1;
      if (match[2].matched)
        sto.val[Stokes::Q] = 1;
      if (match[3].matched)
        sto.val[Stokes::U] = 1;
      if (match[4].matched)
        sto.val[Stokes::V] = 1;
    } else {
      is.setstate(is.rdstate() | std::ios_base::failbit);
    }
  }
  return is;
}

// DataCaptureConfiguration stream insertion
//
auto
operator<<(std::ostream& os, DataCaptureConfiguration const& dc)
  -> std::ostream& {

  return os << "max_streams_per_instance [static]: "
            << DataCaptureConfiguration::max_streams_per_instance << '\n'
            << "max_instances_per_shard: " << dc.max_instances_per_shard << '\n'
            << "packet_block_size: " << dc.packet_block_size << '\n'
            << "completion_queue_size: " << dc.completion_queue_size << '\n'
            << "fabric_provider: " << dc.fabric_provider << '\n'
            << "multicast_address: " << dc.multicast_address << '\n'
            << "max_packet_arrival_skew_factor: "
            << dc.max_packet_arrival_skew_factor << '\n'
#ifndef SYMCAM_RCF_SEND_MULTICAST
            << "fast_multicast_period: "
            // << dc.fast_multicast_period() << '\n' TODO: use this, works for
            // gcc 12
            << std::chrono::duration_cast<std::chrono::seconds>(
                 dc.fast_multicast_period().value())
                 .count()
            << " s\n"
            << "slow_multicast_period: "
            << std::chrono::duration_cast<std::chrono::seconds>(
                 dc.slow_multicast_period().value())
                 .count()
            << " s\n"
#endif
            << "sweep_delay: "
            << std::chrono::duration_cast<std::chrono::milliseconds>(
                 dc.sweep_delay())
                 .count()
            << " ms\n"
            << "max_sweep_period: "
            << std::chrono::milliseconds(dc.max_sweep_period_ms).count()
            << " ms\n"
            << "timesteps_tile_size [static]: "
            << DataCaptureConfiguration::timesteps_tile_size << '\n'
            << "min_capture_interval: "
            << std::chrono::duration_cast<std::chrono::milliseconds>(
                 dc.min_capture_interval())
                 .count()
            << " ms\n";
}

// CrossCorrelationConfiguration stream insertion
//
auto
operator<<(std::ostream& os, CrossCorrelationConfiguration const& xc)
  -> std::ostream& {

  return os << "num_bits [static]: " << CrossCorrelationConfiguration::num_bits
            << '\n'
#if defined(RCP_USE_CUDA) && defined(SYMCAM_USE_TENSOR_CORE_CORRELATOR)
            << "tcc_receivers_per_block [static]: "
            << xc.tcc_receivers_per_block << '\n'
#endif // RCP_USE_CUDA && SYMCAM_USE_TENSOR_CORE_CORRELATOR
            << "timesteps_block_size [static]: "
            << CrossCorrelationConfiguration::timesteps_block_size << '\n'
            << "integration_factor: " << xc.integration_factor << '\n';
}

// GriddingConfiguration stream insertion
//
auto
operator<<(std::ostream& os, GriddingConfiguration const& gr) -> std::ostream& {

  return os << "name: " << gr.name << '\n'
            << "stokes_mask: " << gr.stokes_mask << '\n'
            << "test.cf_size: " << gr.test.cf_size << '\n'
            << "test.cf_oversampling_factor: " << gr.test.cf_oversampling_factor
            << '\n'
            << "test.cf_array_padding: " << gr.test.cf_array_padding << '\n'
            << "vector_length: " << gr.vector_length << '\n'
            << "grid_precision [static]: "
            << boost::core::demangle(gr.grid_precision_type_info().name())
            << '\n'
            << "grid compensated sum [static]: " << std::boolalpha
            << gr.grid_uses_compensated_summation() << '\n'
            << "intermediate grid precision [static]: "
            << boost::core::demangle(
                 gr.hpg_intermediate_grid_precision_type_info().name())
            << '\n'
            << "intermediate weights precision [static]: "
            << boost::core::demangle(
                 gr.hpg_intermediate_grid_precision_type_info().name())
            << '\n'
            << "intermediate weights compensated sum [static]: "
            << std::boolalpha
            << gr.hpg_intermediate_weights_uses_compensated_summation() << '\n';
}

// Configuration stream insertion
//
auto
operator<<(std::ostream& os, Configuration const& config) -> std::ostream& {

  auto indent =
    [](std::string const& str, std::string const& tab) -> std::string {
    std::string result;
    result.reserve(str.size() + tab.size());
    result.append(tab);
    for (auto&& ch : std::string_view(str.c_str(), str.size() - 1))
      if (ch != '\n')
        result.push_back(ch);
      else
        result.append(ch + tab);
    result.push_back(str.back());
    return result;
  };

  if (!config.help.empty()) {
    os << config.help;
  } else if (!config.error.empty()) {
    os << config.error;
  } else {
    using Configuration = Configuration;
    auto visibility_interval_repr = [&]() -> std::string {
      try {
        std::ostringstream oss;
        oss << std::chrono::duration_cast<std::chrono::nanoseconds>(
          config.visibility_interval());
        return oss.str();
      } catch (InvalidVisibilityInterval const&) {
        return "INVALID";
      }
    };
    std::string dp_component =
      "Data packet:\n" + indent(show(DataPacketConfiguration()), "  ");
    os << "Configuration:" << '\n' << indent(dp_component, "  ");
    if (!config.rcp_instance_id.empty())
      os << "  rcp_instance_id: " << config.rcp_instance_id << '\n';
    os << "  pipeline_instance_id [dynamic]: " << config.pipeline_instance_id()
       << '\n'
       << "  uvw_value_t [static]: "
       << boost::core::demangle(typeid(Configuration::uvw_value_t).name())
       << '\n'
       << "  frequency_t [static]: "
       << boost::core::demangle(typeid(Configuration::frequency_t).name())
       << '\n'
       << "  cf_value_t [static]: "
       << boost::core::demangle(typeid(Configuration::cf_value_t).name())
       << '\n'
       << "  grid_value_precision_t [static]: "
       << boost::core::demangle(
            typeid(Configuration::grid_value_precision_t).name())
       << '\n'
       << "  use_compensated_grid_summation [static]: " << std::boolalpha
       << Configuration::use_compensated_grid_summation << '\n'
       << "  enable_parallel_compensated_summation [static]: " << std::boolalpha
       << Configuration::enable_parallel_compensated_summation << '\n'
       << "  num_polarizations [static]: " << Configuration::num_polarizations
       << '\n'
       << "  num_receivers [static]: " << Configuration::num_receivers << '\n'
       << "  max_num_subarrays [static]: " << Configuration::max_num_subarrays
       << '\n'
       << "  subarray_name_max_length [static]: "
       << Configuration::subarray_name_max_length << '\n'
       << "  initial_events_file: " << config.initial_events_file << '\n'
       << "  state_capture_offset: " << config.state_capture_offset << '\n'
       << "  live_capture_offset: " << config.live_capture_offset << '\n'
       << "  fill_report_period [dynamic]: " << config.fill_report_period()
       << '\n'
       << "  child_task_per_integration: " << config.child_task_per_integration
       << '\n'
       << "  num_sample_channels_full_band: "
       << config.num_sample_channels_full_band << '\n'
       << "  sample_channel0_frequency: " << config.sample_channel0_frequency_hz
       << " Hz\n"
       << "  sample_channel_width: " << config.sample_channel_width_hz
       << " Hz\n"
       << "  sample_channel_range_offset: "
       << config.sample_channel_range_offset << '\n'
       << "  sample_channel_range_size: " << config.sample_channel_range_size
       << '\n'
       << "  array_extent: " << "(" << config.array_extent_m[0] << " m, "
       << config.array_extent_m[1] << " m)\n"
       << "  sweep_period [dynamic]: "
       << std::chrono::duration_cast<std::chrono::milliseconds>(
            config.sweep_period())
       << '\n'
       << "  correlation_interval [dynamic]: "
       << std::chrono::duration_cast<std::chrono::nanoseconds>(
            config.correlation_interval())
       << '\n'
       << "  visibility_interval [dynamic]: " << visibility_interval_repr()
       << '\n'
       << "  capture_interval [dynamic]: "
       << std::chrono::duration_cast<std::chrono::nanoseconds>(
            config.capture_interval())
       << '\n';
    std::string components = "Data capture:\n" + indent(show(config.dc), "  ")
                             + "Cross correlation:\n"
                             + indent(show(config.xc), "  ");
    for (auto&& g_i : config.imaging_configurations()) {
      auto& [g, i] = g_i;
      components += "Imaging '" + i.name + "':\n";
      std::string img;
      {
        img += "Gridding:\n";
        img += indent(show(g), "  ");
      }
      {
        img += "Image:\n";
        img += indent(show(i), "  ");
      }
      components += indent(img, "  ");
    }
    components += "Grid export:\n" + indent(show(config.eg), "  ");
    os << indent(components, "  ");
  }
  return os;
}

auto
operator<<(std::ostream& os, ImageConfiguration const& img) -> std::ostream& {
  os << "name: " << img.name << '\n'
     << "gridding_configuration: " << img.gridding_configuration << '\n'
     << "width: " << img.width << " px\n"
     << "height: " << img.height << " px\n"
     << "w_size: " << img.w_size << '\n'
     << "cell_width: " << img.cell_width_arcsec << " arcsec\n"
     << "cell_height: " << img.cell_height_arcsec << " arcsec\n"
     << "w_depth: " << img.w_depth_arcsec << " arcsec\n"
     << "stokes_products: " << (img.full_stokes ? "IQUV\n" : "I\n")
     << "image_channel_offset: " << img.image_channel_offset << '\n'
     << "channels [dynamic]: " << ShowVExpanded(img.channel_descriptors)
     << '\n';
  return os;
}

auto
operator<<(std::ostream& os, ExportGridConfiguration const& eg)
  -> std::ostream& {

  return os << "channel_block_size: " << eg.channel_block_size << '\n'
            << "stokes_block_size: " << eg.stokes_block_size << '\n';
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
