// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

/*! \file BaselineChannelRegion.hpp
 */
#include "Configuration.hpp"
#include "StaticFields.hpp"
#include <array>

namespace rcp::symcam {

/*! \brief axes for timestep, baseline and channel */
enum class BaselineChannelAxes {
  baseline = LEGION_DIM_0, /*!< baseline index */
  channel                  /*!< channel */
};

/*! \brief IndexSpec specialization for BaselineChannelAxes
 *
 * \tparam Coord index coordinate value type
 */
template <typename Coord>
using baseline_channel_index_spec_t = IndexSpec<
  BaselineChannelAxes,
  BaselineChannelAxes::baseline,
  BaselineChannelAxes::channel,
  Coord>;

/*! \brief default static bounds for regions with BaselineChannelAxes
 *
 * \tparam Coord index coordinate value type
 *
 * Bounds are time: unbounded; baseline: [0, Configuration::num_baselines - 1];
 * channel: [0, Coord max]
 */
template <typename Coord>
constexpr auto default_baseline_channel_bounds = std::array{
  IndexInterval<Coord>::bounded(0, Configuration::num_baselines - 1),
  IndexInterval<Coord>::left_bounded(0)};

template <typename Coord>
inline auto
baseline_channel_dynamic_bounds(
  dc::channel_type sample_channel_range_offset,
  dc::channel_type sample_channel_range_size)
  -> std::map<BaselineChannelAxes, IndexInterval<Coord>> {
  return {
    {BaselineChannelAxes::channel,
     IndexInterval<Coord>::bounded(
       sample_channel_range_offset,
       sample_channel_range_offset + sample_channel_range_size - 1)}};
}

template <typename Coord>
inline auto
baseline_channel_dynamic_bounds(Configuration const& config)
  -> std::map<BaselineChannelAxes, IndexInterval<Coord>> {
  return baseline_channel_dynamic_bounds<Coord>(
    config.sample_channel_range_offset, config.sample_channel_range_size);
}

/*! \brief field for uvw values */
using UVWField =
  StaticField<std::array<Configuration::uvw_value_t, 3>, field_ids::uvw>;

/*! \brief field for "index" axis coordinate value in a CFRegion */
using CFIndexField = StaticField<coord_t, field_ids::cf_index>;

/*! \brief grid tile mapping field
 *
 * Mapping from (baseline, channel) index to index in GridTilesRegionSpec
 * region
 */
using GridTileMappingField =
  StaticField<L::Point<3, coord_t>, field_ids::grid_tile_mapping>;

/*! \brief field for projection from a BaselineChannelRegion index space to a
 * BsetChannelBaselinePolprodRegion index space */
using VisProjectionField =
  StaticField<L::Rect<3, coord_t>, field_ids::vis_projection>;

/*! \brief UVWs region definition */
using BaselineChannelRegion = StaticRegionSpec<
  baseline_channel_index_spec_t<coord_t>,
  default_baseline_channel_bounds<coord_t>,
  UVWField,
  CFIndexField,
  GridTileMappingField,
  VisProjectionField>;

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
