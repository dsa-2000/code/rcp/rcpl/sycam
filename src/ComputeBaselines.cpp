// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "ComputeBaselines.hpp"
#include <rcp/rcp.hpp>

namespace rcp::symcam {

/*! \brief task to compute baselines */
struct ComputeBaselinesTask
  : public DefaultKokkosTaskMixin<ComputeBaselinesTask, void> {

  /*! \brief baselines region type alias */
  using bl_t = BaselineRegion;

  /*! \brief antenna positions region type alias */
  using rcv_t = ReceiverRegion;

  /*! \brief task name */
  static constexpr const char* task_name = "ComputeBaselines";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id; /*!< task id */

  /*! \brief region indexes */
  enum { receivers_region_index = 0, baselines_region_index, num_regions };

#ifdef RCP_USE_KOKKOS
  /*! \brief compute baselines from antenna positions */
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* /*task*/,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    assert(regions.size() == num_regions);

    auto const receivers = rcv_t::view<execution_space, LEGION_READ_ONLY>(
      regions[receivers_region_index], PositionField{});

    auto const& baselines_region = regions[baselines_region_index];
    bl_t::rect_t bounds = baselines_region;
    auto const baselines = bl_t::view<execution_space, LEGION_WRITE_ONLY>(
      baselines_region, BaselineField{});

    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();
    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);

    K::parallel_for(
      task_name,
      RangePolicy<execution_space>(work_space, bounds.lo[0], bounds.hi[0] + 1),
      KOKKOS_LAMBDA(coord_t const bl) {
        auto [r0, r1] = baseline_to_receiver_pair(bl);
        auto const& rcv0 = receivers(r0);
        auto const& rcv1 = receivers(r1);
        baselines(bl) = BaselineField::value_t{
          rcv1[0] - rcv0[0], rcv1[1] - rcv0[1], rcv1[2] - rcv0[2]};
      });
  }
#else  // !RCP_USE_KOKKOS
  static auto
  serial_body(
    L::Task const* /*task*/,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> void {

    assert(regions.size() == num_regions);

    auto const receivers = rcv_t::values<LEGION_READ_ONLY>(
      regions[receivers_region_index], PositionField{});
    auto& baselines_region = regions[baselines_region_index];
    bl_t::rect_t bounds = baselines_region;
    auto const baselines =
      bl_t::values<LEGION_WRITE_ONLY>(baselines_region, BaselineField{});
    for (L::PointInRectIterator pir(bounds); pir(); ++pir) {
      auto [r0, r1] = baseline_to_receiver_pair(*pir);
      auto const& rcv0 = receivers[r0];
      auto const& rcv1 = receivers[r1];
      baselines[*pir] = BaselineField::value_t{
        rcv1[0] - rcv0[0], rcv1[1] - rcv0[1], rcv1[2] - rcv0[2]};
    }
  }
#endif // RCP_USE_KOKKOS

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID ComputeBaselinesTask::task_id;

ComputeBaselines::ComputeBaselines() {}

auto
ComputeBaselines::launch(
  L::Context ctx,
  L::Runtime* rt,
  rcv_t::LogicalRegion receivers,
  bl_t::LogicalRegion baselines) -> void {

  auto launcher =
    L::TaskLauncher(ComputeBaselinesTask::task_id, L::UntypedBuffer());
  launcher.region_requirements.resize(ComputeBaselinesTask::num_regions);
  launcher.region_requirements[ComputeBaselinesTask::receivers_region_index] =
    receivers.requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{PositionField{}});
  launcher.region_requirements[ComputeBaselinesTask::baselines_region_index] =
    baselines.requirement(
      LEGION_WRITE_ONLY, LEGION_EXCLUSIVE, StaticFields{BaselineField{}});
  rt->execute_task(ctx, launcher);
}

auto
ComputeBaselines::preregister_tasks(L::TaskID tid) -> void {
  PortableTask<ComputeBaselinesTask>::preregister_task_variants(tid);
}

auto
ComputeBaselines::register_tasks(L::Runtime* rt, L::TaskID tid) -> void {
  PortableTask<ComputeBaselinesTask>::register_task_variants(rt, tid);
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
