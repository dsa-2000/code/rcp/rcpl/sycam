// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BsetChannelBaselinePolprodRegion.hpp"
#include "libsymcam.hpp"

namespace rcp::symcam {

/*! launcher for task to compute visibility weights
 */
class NormalizeVisibilities {
public:
  using cbp_t = BsetChannelBaselinePolprodRegion;

  struct Args {
    std::uint32_t integration_factor;
  };

private:
  cbp_t::index_partition_t m_index_partition;

  L::IndexTaskLauncher m_launcher;

  std::shared_ptr<Args> m_args;

public:
  /*! non-default constructor
   *
   * \param index_space index space of region with visibilities
   */
  NormalizeVisibilities(
    L::Context ctx,
    L::Runtime* rt,
    Configuration const& config,
    cbp_t::index_space_t index_space);

  NormalizeVisibilities() = default;

  NormalizeVisibilities(NormalizeVisibilities const&) = default;

  NormalizeVisibilities(NormalizeVisibilities&&) = default;

  NormalizeVisibilities&
  operator=(NormalizeVisibilities const&) = default;

  NormalizeVisibilities&
  operator=(NormalizeVisibilities&&) = default;

  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    cbp_t::LogicalRegion visibilities,
    L::Predicate pred = L::Predicate::TRUE_PRED) -> void;

  static auto
  preregister_tasks(L::TaskID task_id) -> void;

  static auto
  register_tasks(L::Runtime* rt, L::TaskID task_id) -> void;
};
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
