// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "CaptureFSamples.hpp"
#include <bark/log.hpp>

#include <format>
#include <rcp/rcp.hpp>

namespace rcp::symcam {

// Allocator adaptor that interposes construct() calls to
// convert value initialization into default initialization.
template <typename T, typename A = std::allocator<T>>
class default_init_allocator : public A {
  using a_t = std::allocator_traits<A>;

public:
  template <typename U>
  struct rebind {
    using other =
      default_init_allocator<U, typename a_t::template rebind_alloc<U>>;
  };

  using A::A;

  template <typename U>
  void
  construct(U* ptr) noexcept(std::is_nothrow_default_constructible<U>::value) {
    ::new (static_cast<void*>(ptr)) U;
  }
  template <typename U, typename... Args>
  void
  construct(U* ptr, Args&&... args) {
    a_t::construct(static_cast<A&>(*this), ptr, std::forward<Args>(args)...);
  }
};

/*! task to capture packets from F-engine, encapsulating rcp::dc::DataCapture
 *  thread
 *
 * This task can only be registered dynamically (because of layout constraints
 * on field space)
 */
struct CaptureFSamplesTask
  : public SerialOnlyTaskMixin<CaptureFSamplesTask, CaptureFSamples::count_t> {

  /*! \brief task name*/
  static constexpr char const* task_name = "CaptureFSamples";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  enum { dc_ptr_region_index = 0, capture_region_index, num_regions };

  using md_t = FSamplesRegion;

  static auto
  channel_range(md_t::rect_t const& bounds) -> std::string {
    return std::format(
      "[{}..{}]",
      bounds.lo[dim(md_t::axes_t::channel)],
      bounds.hi[dim(md_t::axes_t::channel)]);
  }

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    assert(task->index_point.get_dim() == 2);
    auto* const dc_ptr = DataCaptureRegionSpec::values<LEGION_READ_WRITE>(
      regions[dc_ptr_region_index],
      DataCapturePointerField{})[L::Point<2>(task->index_point)];
    assert(task->arglen == sizeof(CaptureFSamples::Args));
    CaptureFSamples::Args args =
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      *reinterpret_cast<CaptureFSamples::Args const*>(task->args);

    auto const& capture_region = regions[capture_region_index];
    md_t::rect_t bounds(capture_region);

    auto log_point = [&](std::string_view msg) {
      log().debug<std::string, std::string>(
        msg,
        bark::val_field_t{"ts", showt(args.t0.time_since_epoch())},
        bark::val_field_t{"chans", channel_range(bounds)});
    };

    log_point("begin capture");

    auto blksz =
      std::size_t(fpacket_type::num_channels)
      * (bounds.hi[dim(md_t::axes_t::receiver)] - bounds.lo[dim(md_t::axes_t::receiver)] + 1)
      * (bounds.hi[dim(md_t::axes_t::polarization)] - bounds.lo[dim(md_t::axes_t::polarization)] + 1)
      * (bounds.hi[dim(md_t::axes_t::timestep)] - bounds.lo[dim(md_t::axes_t::timestep)] + 1);
    assert(capture_region.get_logical_region().get_dim() == md_t::dim);

    // before initiating a capture, we need to count the number of regions to
    // fill
    int num_pieces{0};
    for (L::PieceIteratorT<md_t::dim, md_t::coord_t> pit(
           capture_region, FSampleField::field_id, true);
         pit();
         pit++) {
      // the region that we pass to DataCapture comprises multiple tiles; we can
      // get a pointer to the entire (tiled) region needed by DataCapture
      // through "pit->lo", but only when a tile aligned to a block boundary of
      // the channel index and the first timestep index is seen in the iteration
      if (
        (((pit->lo[dim(md_t::axes_t::channel)]
           - bounds.lo[dim(md_t::axes_t::channel)])
          % fpacket_type::num_channels)
         == 0)
        && (pit->lo[dim(md_t::axes_t::timestep)] == bounds.lo[dim(md_t::axes_t::timestep)]))
        ++num_pieces;
    }

    // use an atomic type here to prevent memory access order issues between
    // threads (not sure that handshake methods make any guarantees)
    std::atomic<result_t> result{0};
    // use LegionHandshake to allow us to wait on fills to complete; num_pieces
    // is use to provide the number of "external participants"
    if (num_pieces > 0) {
      auto handshake = L::Runtime::create_external_handshake(true, num_pieces);

      // fill completion invokes this callback, which accumulates the fill count
      // and calls the appropriate handshake method
      auto completion_cb = [&](dc_t::fill_result_t const& fill_result) -> void {
        result += std::get<0>(fill_result);
        handshake.ext_handoff_to_legion();
      };

      // capture region does not have an affine index mapping, so we need
      // PieceIteratorT
      for (L::PieceIteratorT<md_t::dim, md_t::coord_t> pit(
             capture_region, FSampleField::field_id, true);
           pit();
           pit++) {
        // the region that we pass to DataCapture comprises multiple tiles; we
        // can get a pointer to the entire (tiled) region needed by DataCapture
        // through "pit->lo", but only when a tile aligned to a block boundary
        // of the channel index and the first timestep index is seen in the
        // iteration
        if (
          (((pit->lo[dim(md_t::axes_t::channel)]
             - bounds.lo[dim(md_t::axes_t::channel)])
            % fpacket_type::num_channels)
           == 0)
          && (pit->lo[dim(md_t::axes_t::timestep)] == bounds.lo[dim(md_t::axes_t::timestep)])) {
          // the following region accessors are valid in Legion only within the
          // bounds *pit; however, we can index the tiled region ourselves
          // outside of Legion accessors if we know the index mapping (of course
          // we're on our own since Legion cannot verify our mapping)
          auto samples = md_t::values<LEGION_READ_WRITE>(
            capture_region, FSampleField{}, *pit);
          auto weights = md_t::values<LEGION_READ_WRITE>(
            capture_region, FWeightField{}, *pit);
          auto bsp = dc_ptr->new_block(
            pit->lo[dim(md_t::axes_t::channel)],
            args.t0.time_since_epoch().count(),
            std::span(samples.ptr(pit->lo), blksz),
            std::span(weights.ptr(pit->lo), blksz));
          dc_ptr->fill(bsp, completion_cb);
        }
      }
      handshake.legion_wait_on_ext();
    }

    log_point("end capture");
    return result;
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar.add_layout_constraint_set(
      capture_region_index,
      samples_layout().constraint_id(traits::layout_style).value());
    L::Runtime::preregister_task_variant<result_t, body<Variant>>(
      registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar.add_layout_constraint_set(
      capture_region_index,
      samples_layout().constraint_id(rt, traits::layout_style).value());
    rt->register_task_variant<result_t, body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID CaptureFSamplesTask::task_id;

CapturedFSamplesPartition::CapturedFSamplesPartition(
  L::Context ctx,
  L::Runtime* rt,
  rcp::dc::channel_type sample_channel_range_offset,
  streams_def_t const& streams_by_shard,
  rcp::dc::channel_type max_dc_per_shard,
  index_space_t is)
  : m_ip(make_partition(
      ctx,
      rt,
      sample_channel_range_offset,
      streams_by_shard,
      max_dc_per_shard,
      is)) {}

auto
CapturedFSamplesPartition::index_partition() const -> index_partition_t {
  return m_ip;
}

auto
CapturedFSamplesPartition::channels_by_shard_and_dc() const
  -> std::vector<std::vector<std::array<rcp::dc::channel_type, 2>>> const& {
  return m_channels_by_shard_and_dc;
}

auto
CapturedFSamplesPartition::make_partition(
  L::Context ctx,
  L::Runtime* rt,
  rcp::dc::channel_type sample_channel_range_offset,
  streams_def_t const& streams_by_shard,
  rcp::dc::channel_type max_dc_per_shard,
  index_space_t fsamples_is) -> index_partition_t {

  auto bounds = rt->get_index_space_domain(fsamples_is).bounds;
  std::vector<DataCaptureRegionSpec::rect_t> cs_rects;
  std::map<
    L::Point<DataCaptureRegionSpec::dim, DataCaptureRegionSpec::coord_t>,
    L::DomainT<md_t::dim, md_t::coord_t>>
    domains;
  m_channels_by_shard_and_dc.reserve(streams_by_shard.size());
  for (auto&& strm : std::views::iota(
         DataCaptureRegionSpec::coord_t{0},
         DataCaptureRegionSpec::coord_t(streams_by_shard.size()))) {
    auto streams_by_dc = rcp::block_distribute(
      streams_by_shard[strm][1], max_dc_per_shard, streams_by_shard[strm][0]);
    auto num_dc = rcp::dc::channel_type(streams_by_dc.size());
    cs_rects.push_back(
      DataCaptureRegionSpec::rect_t{{strm, 0}, {strm, num_dc - 1}});
    for (auto&& dc : std::views::iota(rcp::dc::channel_type{0}, num_dc)) {
      streams_by_dc[dc][0] =
        sample_channel_range_offset
        + streams_by_dc[dc][0]
            * DataPacketConfiguration::num_channels_per_packet;
      streams_by_dc[dc][1] *= DataPacketConfiguration::num_channels_per_packet;
      bounds.lo[dim(md_t::axes_t::channel)] = streams_by_dc[dc][0];
      bounds.hi[dim(md_t::axes_t::channel)] =
        bounds.lo[dim(md_t::axes_t::channel)] + streams_by_dc[dc][1] - 1;
      domains[{strm, dc}] = L::DomainT<md_t::dim, coord_t>(bounds);
    }
    m_channels_by_shard_and_dc.push_back(streams_by_dc);
  }
  return rt->create_partition_by_domain(
    ctx,
    fsamples_is,
    domains,
    rt->create_index_space(ctx, cs_rects),
    true,
    LEGION_DISJOINT_COMPLETE_KIND);
}

CaptureFSamples::CaptureFSamples(
  L::Context ctx,
  L::Runtime* rt,
  Configuration const& config,
  std::size_t shard_point,
  streams_def_t const& streams_by_shard,
  md_t::index_space_t index_space,
  L::FieldSpace field_space,
  L::MappingTagID tag)
  : m_args(std::make_shared<Args>())
  , m_samples_partition(CapturedFSamplesPartition(
      ctx,
      rt,
      config.sample_channel_range_offset,
      streams_by_shard,
      config.dc.max_instances_per_shard,
      index_space)) {

  assert([&]() -> bool {
    std::set<L::FieldID> fields;
    rt->get_field_space_fields(ctx, field_space, fields);
    return fields.contains(FSampleField::field_id)
           && fields.contains(FWeightField::field_id);
  }());
  assert([&]() -> bool {
    auto domain = rt->get_index_space_domain(index_space);
    assert(domain.dense());
    auto bounds = domain.bounds;
    return (bounds.hi[dim(md_t::axes_t::timestep)]
            - bounds.lo[dim(md_t::axes_t::timestep)] + 1)
           == config.capture_interval().count();
  }());

  using dcp_t = DataCaptureRegionSpec;

  // create a region for DataCapture instance pointers
  // color space of m_samples_partition is index by (shard, dc)
  auto is = CapturedFSamplesPartition::color_space_t(
    rt->get_index_partition_color_space_name(
      m_samples_partition.index_partition()));
  auto fs = dcp_t::field_space(ctx, rt);
  m_dc_lr = dcp_t::LogicalRegion{rt->create_logical_region(ctx, is, fs)};
  {
    // map m_dc_lr by shard, and initialize DataCapture instances
    auto ip = create_partition_by_slicing(
      ctx, rt, is, {{dim(DataCaptureAxes::shard), 1}});
    auto lp = dcp_t::LogicalPartition{rt->get_logical_partition(m_dc_lr, ip)};
    auto infos =
      dc_factory_t::find_fabric_interfaces(config.dc.fabric_provider);
    if (infos.empty())
      throw NoMatchingFabricInterfaceError();
    auto const channels_by_dc =
      m_samples_partition.channels_by_shard_and_dc()[shard_point];
    auto const num_dc = channels_by_dc.size();
    m_packet_blocks.reserve(num_dc);
    m_packet_mrs.reserve(num_dc);
    for (std::size_t dc = 0; dc < num_dc; ++dc) {
      m_packet_blocks.emplace_back(
        config.dc.packet_block_size * sizeof(fpacket_type),
        default_init_allocator<char>());
      m_packet_mrs.emplace_back(
        m_packet_blocks.back().data(),
        config.dc.packet_block_size * sizeof(fpacket_type));
      auto pool = std::make_shared<rcp::dc::single_block_synchronized_pool>(
        &m_packet_mrs.back());
      std::set<rcp::dc::channel_type> packet_channel_offsets;
      for (coord_t ch = channels_by_dc[dc][0];
           ch < channels_by_dc[dc][0] + channels_by_dc[dc][1];
           ch += DataPacketConfiguration::num_channels_per_packet)
        packet_channel_offsets.insert(rcp::dc::channel_type(ch));
      m_dcs.push_back(dc_factory_t::create(
        pool,
        config.dc.completion_queue_size,
        infos.front(),
        config.dc.multicast_address,
        packet_channel_offsets,
        config.capture_interval().count(),
        rcp::dc::SweepParameters{
          .delay = config.dc.sweep_delay(), .period = config.sweep_period()},
        config.dc.max_packet_arrival_skew_factor,
        config.dc.fast_multicast_period()));
      m_dcs.back()->start();
      m_dc_ps.push_back(m_dcs.back().get());
    }
    // initialize DataCapturePointerField field of the region
    {
      // create the ExternalMemoryResource for m_dcs
      // NOLINTBEGIN(bugprone-sizeof-expression)
      auto ext = Realm::ExternalMemoryResource(
        m_dc_ps.data(), m_dc_ps.size() * sizeof(m_dc_ps[0]));
      // NOLINTEND(bugprone-sizeof-expression)
      auto launcher =
        L::IndexAttachLauncher(LEGION_EXTERNAL_INSTANCE, m_dc_lr, true);
      launcher.initialize_constraints(
        false, false, {DataCapturePointerField::field_id});
      auto subregion = rt->get_logical_subregion_by_color(lp, shard_point);
      launcher.add_external_resource(subregion, &ext);
      launcher.privilege_fields.insert(DataCapturePointerField::field_id);
      m_dc_ext_resources = rt->attach_external_resources(ctx, launcher);
    }
  }

  {
    // add requirements to fields in m_dc_lr partitioned by
    // m_samples_partition color space
    m_launcher = L::IndexTaskLauncher(
      CaptureFSamplesTask::task_id,
      is,
      L::UntypedBuffer(m_args.get(), sizeof(Args)),
      L::ArgumentMap(),
      L::Predicate::TRUE_PRED /*pred*/,
      false /*must*/,
      0 /*id*/,
      tag /*tag*/);
    m_launcher.region_requirements.resize(CaptureFSamplesTask::num_regions);
    m_dc_ip = rt->create_partition_by_blockify(
      ctx, is, L::Point<2, dcp_t::coord_t>::ONES());
    auto lp = dcp_t::LogicalPartition{
      rt->get_logical_partition(dcp_t::logical_region_t{m_dc_lr}, m_dc_ip)};
    m_launcher.region_requirements[CaptureFSamplesTask::dc_ptr_region_index] =
      lp.requirement(
        0,
        LEGION_READ_WRITE,
        LEGION_EXCLUSIVE,
        m_dc_lr,
        StaticFields{DataCapturePointerField{}});
  }
}

auto
CaptureFSamples::partition_size(L::Runtime* rt) const -> unsigned {
  return rt
    ->get_index_partition_color_space(m_samples_partition.index_partition())
    .get_volume();
}

#ifndef SYMCAM_RCF_SEND_MULTICAST
auto
CaptureFSamples::set_bcast_period(std::chrono::seconds const& period) -> void {
  for (auto&& dc : m_dcs)
    dc->set_bcast_period(period);
}
#endif

auto
CaptureFSamples::destroy(L::Context ctx, L::Runtime* rt) -> void {
  auto detached = rt->detach_external_resources(ctx, m_dc_ext_resources);
  for (auto&& dc : m_dcs)
    dc->stop();
  rt->destroy_field_space(ctx, m_dc_lr.get_field_space());
  rt->destroy_logical_region(ctx, m_dc_lr);
  detached.wait();
}

auto
CaptureFSamples::launch(
  L::Context ctx,
  L::Runtime* rt,
  md_t::LogicalRegion region,
  StreamClock::external_time const& t0,
  L::Predicate pred) -> L::FutureMap {

  static std::size_t const zero{0};

  m_args->t0 = t0;
  auto partition = md_t::LogicalPartition{
    rt->get_logical_partition(region, m_samples_partition.index_partition())};
  m_launcher.region_requirements[CaptureFSamplesTask::capture_region_index] =
    partition.requirement(
      0,
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      region,
      StaticFields{FSampleField{}, FWeightField{}});
  m_launcher.predicate = pred;
  m_launcher.set_predicate_false_result(L::UntypedBuffer(&zero, sizeof(zero)));
  return rt->execute_index_space(ctx, m_launcher);
}

auto
CaptureFSamples::preregister_tasks(L::TaskID task_id) -> void {
  PortableTask<CaptureFSamplesTask>::preregister_task_variants(task_id);
}

auto
CaptureFSamples::register_tasks(L::Runtime* rt, L::TaskID task_id) -> void {
  PortableTask<CaptureFSamplesTask>::register_task_variants(rt, task_id);
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
