// Copyright 2022 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include <hpg/config.hpp>
#include <rcp/DataPacketConfiguration.hpp>
#include <rcp/dc/rcpdc.hpp>
#include <rcp/rcp.hpp>

#include <chrono>
#include <fstream>
#include <optional>
#include <ranges>
#include <stdexcept>
#include <string>
#include <typeinfo>

#include <nlohmann/json.hpp>

namespace rcp::symcam {

/*! \brief data capture configuration */
struct DataCaptureConfiguration {

  /*! \brief maximum number of streams per data capture instance */
  static constexpr auto max_streams_per_instance =
    RCP_MAX_STREAMS_PER_DATA_CAPTURE;

  /*! \brief capture buffer timestep tile size
   *
   * value of 0 corresponds to no tiling
   */
  static constexpr auto timesteps_tile_size =
    SYMCAM_CAPTURE_BUFFER_TIMESTEPS_TILE_SIZE;

  /*! \brief maximum number of data capture instances per shard */
  unsigned max_instances_per_shard;
  /*! \brief size of memory reserved for packet capture, in number of packets */
  std::size_t packet_block_size;
  /*! \brief size of (libfabric) packet capture completion queue */
  std::size_t completion_queue_size;
  /*! \brief libfabric provider name */
  std::string fabric_provider;
  /*! \brief multicast group address for capture parameter multicasts */
  std::string multicast_address;
  /*! \brief maximum packet arrival skew (max degree out of order timestamps
   * across streams) */
  unsigned max_packet_arrival_skew_factor;
  /*! \brief capture parameter fast multicast period (sec)
   *
   * Ignored if RCF sends multicast packets
   */
  unsigned fast_multicast_period_sec;
  /*! \brief capture parameter slow multicast period (sec)
   *
   * Ignored if RCF sends multicast packets
   */
  unsigned slow_multicast_period_sec;
  /*! \brief delay of sweep timestamp relative to system time (ms) */
  unsigned sweep_delay_ms;
  /*! \brief maximum sweep period (ms) */
  unsigned max_sweep_period_ms;
  /*! \brief minimum time span of data capture regions (ms) */
  unsigned min_capture_interval_ms;

  /*! \brief default constructor (defaulted) */
  DataCaptureConfiguration();

  /*! \brief copy constructor (defaulted) */
  DataCaptureConfiguration(DataCaptureConfiguration const&) = default;

  /*! \brief move constructor (defaulted) */
  DataCaptureConfiguration(DataCaptureConfiguration&&) noexcept = default;

  /*! \brief destructor (defaulted) */
  ~DataCaptureConfiguration() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(DataCaptureConfiguration const&)
    -> DataCaptureConfiguration& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(DataCaptureConfiguration&&) noexcept
    -> DataCaptureConfiguration& = default;

  /*! \brief capture parameter fast multicast period
   *
   * \return interval value, if SYMCAM_RCF_SEND_MULTICAST is not defined;
   * std::nullopt, otherwise
   */
  [[nodiscard]] auto
  fast_multicast_period() const noexcept
    -> std::optional<std::chrono::seconds> {
#ifdef SYMCAM_RCF_SEND_MULTICAST
    return std::nullopt;
#else
    return std::chrono::seconds(fast_multicast_period_sec);
#endif
  }

  /*! \brief capture parameter slow multicast period
   *
   * \return interval value, if SYMCAM_RCF_SEND_MULTICAST is not defined;
   * std::nullopt, otherwise
   */
  [[nodiscard]] auto
  slow_multicast_period() const noexcept
    -> std::optional<std::chrono::seconds> {
#ifdef SYMCAM_RCF_SEND_MULTICAST
    return std::nullopt;
#else
    return std::chrono::seconds(slow_multicast_period_sec);
#endif
  }

  /*! \brief sweep timestamp delay relative to system time */
  [[nodiscard]] auto
  sweep_delay() const noexcept -> std::chrono::milliseconds {
    return std::chrono::milliseconds(sweep_delay_ms);
  }

  /*! \brief minimum time span of data capture regions */
  [[nodiscard]] auto
  min_capture_interval() const noexcept -> std::chrono::milliseconds {
    return std::chrono::milliseconds(min_capture_interval_ms);
  }

  /*! \brief write configuration to file stream */
  auto
  write_config(std::ofstream& of) -> void;

  /*! \brief serialized size */
  [[nodiscard]] auto
  serialized_size() const -> std::size_t;

  /*! \brief serialize to buffer */
  auto
  serialize(void* buffer) const -> std::size_t;

  /*! \brief deserialize from buffer */
  auto
  deserialize(void const* buffer) -> std::size_t;
};

/*! \brief cross correlation configuration */
struct CrossCorrelationConfiguration {

  /*! \brief number of bits per real or imaginary part of a sample */
  static constexpr auto num_bits = 4 * sizeof(rcp::dc::fsample_type);

#if defined(RCP_USE_CUDA) && defined(SYMCAM_USE_TENSOR_CORE_CORRELATOR)

  /*! \brief number of receivers per block */
  static constexpr auto tcc_receivers_per_block = 64;

  static_assert(
    128 / num_bits == DataCaptureConfiguration::timesteps_tile_size);

  static constexpr auto timesteps_block_size =
    DataCaptureConfiguration::timesteps_tile_size;

#else

  static constexpr auto timesteps_block_size = 1;

#endif // RCP_USE_CUDA && SYMCAM_USE_TENSOR_CORE_CORRELATOR

  /*! \brief integration factor */
  std::uint64_t integration_factor;

  /*! \brief default constructor (defaulted) */
  CrossCorrelationConfiguration();

  /* \brief copy constructor (defaulted) */
  CrossCorrelationConfiguration(CrossCorrelationConfiguration const&) = default;

  /*! \brief move constructor (defaulted) */
  CrossCorrelationConfiguration(CrossCorrelationConfiguration&&) noexcept =
    default;

  /*! \brief destructor (defaulted) */
  ~CrossCorrelationConfiguration() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(CrossCorrelationConfiguration const&)
    -> CrossCorrelationConfiguration& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(CrossCorrelationConfiguration&&) noexcept
    -> CrossCorrelationConfiguration& = default;

  /*! \brief write configuration to file stream */
  auto
  write_config(std::ofstream& of) -> void;

  /*! \brief serialized size */
  [[nodiscard]] auto
  serialized_size() const -> std::size_t;

  /*! \brief serialize to buffer */
  auto
  serialize(void* buffer) const -> std::size_t;

  /*! \brief deserialize from buffer */
  auto
  deserialize(void const* buffer) -> std::size_t;
};

/*! \brief names for Stokes I only */
struct StokesI {
  static constexpr char const* long_name = "Stokes I";
  static constexpr char const* short_name = "i";
};
/*! \brief names for Stokes IQUV */
struct StokesIQUV {
  static constexpr char const* long_name = "Stokes IQUV";
  static constexpr char const* short_name = "iquv";
};

/*! \brief Stokes parameters */
enum Stokes : unsigned char { I, Q, U, V };

/*! \brief Stokes product mask representation */
struct StokesMask {
  std::array<unsigned char, 4> val;
};

/*! \brief gridding configuration */
struct GriddingConfiguration {

  /*! \brief gridding library enumeration */
  enum class Library {
    hpg,
    // idg
  };

  /*! \brief name of this gridding configuration */
  std::string name;

  /*! \brief mask for image domain Stokes components to grid */
  StokesMask stokes_mask{};

  /*! \brief some temporary, development parameters
   *
   * likely CF values will ultimately need to be read in from a file or
   * something similar, which is why these are temporary parameter
   */
  struct {
    /*! size of CF function support in grid points/pixels */
    unsigned cf_size;

    /*! convolution function oversampling factor */
    unsigned cf_oversampling_factor;

    /*! convolution function array padding
     *
     * padding is on every edge
     */
    unsigned cf_array_padding;
  } test{};

  /*! \brief innermost loop gridding vector length
   *
   * Vector size for iteration over one dimension of CF support during
   * gridding. TODO: should be HPG-specific
   */
  unsigned vector_length{};

  /*! \brief default constructor (defaulted) */
  GriddingConfiguration() = default;

  /*! \brief constructor from name  */
  GriddingConfiguration(std::string_view const& name);

  /*! \brief copy constructor (defaulted) */
  GriddingConfiguration(GriddingConfiguration const&) = default;

  /*! \brief move constructor (defaulted) */
  GriddingConfiguration(GriddingConfiguration&&) noexcept = default;

  /*! \brief destructor (defaulted) */
  ~GriddingConfiguration() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(GriddingConfiguration const&) -> GriddingConfiguration& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(GriddingConfiguration&&) noexcept
    -> GriddingConfiguration& = default;

  /*! \brief write configuration to file stream */
  auto
  write_config(std::ofstream& of) -> void;

  /*! \brief serialized size */
  [[nodiscard]] auto
  serialized_size() const -> std::size_t;

  /*! \brief serialize to buffer */
  auto
  serialize(void* buffer) const -> std::size_t;

  /*! \brief deserialize from buffer */
  auto
  deserialize(void const* buffer) -> std::size_t;

  [[nodiscard]] auto
  stokes_mask_name() const -> std::string;

  [[nodiscard]] auto
  test_cf_oversampling_factor_name() const -> std::string;

  [[nodiscard]] auto
  test_cf_array_padding_name() const -> std::string;

  [[nodiscard]] auto
  test_cf_size_name() const -> std::string;

  [[nodiscard]] auto
  vector_length_name() const -> std::string;

  static auto
  grid_precision_type_info() -> std::type_info const&;

  static auto
  grid_uses_compensated_summation() -> bool;

  static auto
  hpg_intermediate_grid_precision_type_info() -> std::type_info const&;

  static auto
  hpg_intermediate_weights_precision_type_info() -> std::type_info const&;

  static auto
  hpg_intermediate_weights_uses_compensated_summation() -> bool;
};

/* \brief representation of regular visibility channel subset */
struct VisibilityChannelsDescriptor {
  /*! \brief first visibility channel */
  rcp::dc::channel_type first;
  /*! \brief number of consecutive visibility channels */
  rcp::dc::channel_type width;
  /*! \brief stride in visibility channels between consecutive blocks */
  rcp::dc::channel_type stride;
  /*! \brief number of (consecutive) image channels */
  rcp::dc::channel_type num_blocks;

  /*~ \brief default constructor (defaulted) */
  VisibilityChannelsDescriptor() = default;

  /*! \brief single channel constructor
   *
   * \param first visibility channel index
   */
  VisibilityChannelsDescriptor(rcp::dc::channel_type first);

  /*! \brief single block of consecutive channels
   *
   * \param first index of the first visibility channel in the block
   * \param width1 one less than the number of visibility channels in the block
   */
  VisibilityChannelsDescriptor(
    rcp::dc::channel_type first, rcp::dc::channel_type width1);

  /*! \brief sequence of blocks
   *
   * default value of blocks is used to implement indefinite repetition
   *
   * \param first index of the first visibility channel in the block
   * \param width1 one less than the number of visibility channels in a block
   * \param gap gap between blocks
   * \param blocks1 one less than the number of blocks
   */
  VisibilityChannelsDescriptor(
    rcp::dc::channel_type first,
    rcp::dc::channel_type width1,
    rcp::dc::channel_type gap,
    rcp::dc::channel_type blocks1 =
      std::numeric_limits<rcp::dc::channel_type>::max());

  /*! \brief copy constructor (defaulted) */
  VisibilityChannelsDescriptor(VisibilityChannelsDescriptor const&) = default;

  /*! \brief move constructor (defaulted) */
  VisibilityChannelsDescriptor(VisibilityChannelsDescriptor&&) noexcept =
    default;

  /*! \brief destructor (defaulted) */
  ~VisibilityChannelsDescriptor() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(VisibilityChannelsDescriptor const&)
    -> VisibilityChannelsDescriptor& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(VisibilityChannelsDescriptor&&) noexcept
    -> VisibilityChannelsDescriptor& = default;

  /*! \brief span of visibility channels (closed interval) */
  [[nodiscard]] auto
  bounds() const -> std::array<rcp::dc::channel_type, 2>;

  /*! \brief explicit blocks of channel indexes within a given range */
  [[nodiscard]] auto
  blocks() const -> std::vector<std::array<rcp::dc::channel_type, 2>>;

  /*! \brief find block index into which a channel is mapped
   *
   * \return block index to which sample channel is mapped, or nullopt if the
   * channel is unmapped by the descriptor
   */
  [[nodiscard]] auto
  find_block_index(rcp::dc::channel_type channel) const noexcept
    -> std::optional<unsigned>;
};

/*! \brief image configuration */
struct ImageConfiguration {
  using uvw_value_t = RCP_UVW_VALUE_TYPE;

  std::string name;
  std::string gridding_configuration;
  std::vector<VisibilityChannelsDescriptor> channel_descriptors;
  unsigned width{};                 // x
  unsigned height{};                // y
  unsigned w_size{};                // number of w-planes
  uvw_value_t cell_width_arcsec{};  // x
  uvw_value_t cell_height_arcsec{}; // y
  uvw_value_t w_depth_arcsec{};     // w layer size
  bool full_stokes{};               // true: IQUV; false: I
  unsigned image_channel_offset{};  // channel 0 offset in global config

  /*! \brief default constructor (defaulted) */
  ImageConfiguration() = default;

  /*! \brief constructor from name */
  ImageConfiguration(std::string_view const& name);

  /*! \brief full constructor */
  ImageConfiguration(
    std::string_view const& name,
    std::string_view const& gridding_configuration,
    std::vector<VisibilityChannelsDescriptor> const& channel_descriptors_,
    unsigned width_,
    unsigned height_,
    unsigned w_size_,
    uvw_value_t cell_width_arcsec_,
    uvw_value_t cell_height_arcsec_,
    uvw_value_t w_depth_arcsec_,
    bool full_stokes_,
    unsigned image_channel_offset = 0);

  /*! \brief copy constructor (defaulted) */
  ImageConfiguration(ImageConfiguration const&) = default;

  /*! \brief move constructor (defaulted) */
  ImageConfiguration(ImageConfiguration&&) = default;

  /*! \brief destructor (defaulted) */
  ~ImageConfiguration() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(ImageConfiguration const&) -> ImageConfiguration& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ImageConfiguration&&) -> ImageConfiguration& = default;

  [[nodiscard]] auto
  num_channels() const noexcept -> unsigned;

  [[nodiscard]] auto
  num_stokes() const noexcept -> unsigned;

  [[nodiscard]] auto
  visibility_channel_blocks() const
    -> std::vector<std::array<rcp::dc::channel_type, 2>>;

  /*! \brief convert U,V,W in units of cycles to grid X,Y,W-layer coordinates */
  static auto
  uvw_cycles_to_xyw(
    uvw_value_t cell_width,
    uvw_value_t cell_height,
    uvw_value_t w_depth,
    std::array<uvw_value_t, 3> const& uvw) -> std::array<coord_t, 3> {

    return {
      std::lrint(uvw[0] * cell_width),
      std::lrint(uvw[1] * cell_height),
      std::lrint(std::floor(std::abs(uvw[2] * w_depth)))};
  }

  /*! \brief convert U,V,W in units of wavelength to grid X,Y,W-layer
   *  coordinates */
  [[nodiscard]] inline auto
  uvw_cycles_to_xyw(std::array<uvw_value_t, 3> const& uvw) const
    -> std::array<coord_t, 3> {

    return uvw_cycles_to_xyw(
      cell_width_arcsec, cell_height_arcsec, w_depth_arcsec, uvw);
  }

  /*! \brief write configuration to file stream */
  auto
  write_config(std::ofstream& of) -> void;

  /*! \brief serialized size */
  [[nodiscard]] auto
  serialized_size() const -> std::size_t;

  /*! \brief serialize to buffer */
  auto
  serialize(void* buffer) const -> std::size_t;

  /*! \brief deserialize from buffer */
  auto
  deserialize(void const* buffer) -> std::size_t;

  [[nodiscard]] auto
  gridding_configuration_name() const -> std::string;
  [[nodiscard]] auto
  width_name() const -> std::string;
  [[nodiscard]] auto
  height_name() const -> std::string;
  [[nodiscard]] auto
  w_size_name() const -> std::string;
  [[nodiscard]] auto
  cell_width_arcsec_name() const -> std::string;
  [[nodiscard]] auto
  cell_height_arcsec_name() const -> std::string;
  [[nodiscard]] auto
  w_depth_arcsec_name() const -> std::string;
  [[nodiscard]] auto
  full_stokes_name() const -> std::string;
  [[nodiscard]] auto
  channel_descriptors_name() const -> std::string;
  [[nodiscard]] auto
  image_channel_offset_name() const -> std::string;
};

/*! \brief imaging configuration
 *
 * union of a GriddingConfiguration instance and an ImageConfiguration instance
 */
struct ImagingConfiguration {

  /*! \brief maximum size of serialized instance
   *
   * \todo make this configurable?
   */
  static constexpr auto max_serialized_size = 512;

  GriddingConfiguration gr;
  ImageConfiguration im;

  /*! \brief constructor from GriddingConfiguration and ImageConfiguration */
  ImagingConfiguration(GriddingConfiguration gr_, ImageConfiguration im_)
    : gr(std::move(gr_)), im(std::move(im_)) {}

  /*! \brief default constructor (defaulted) */
  ImagingConfiguration() = default;

  /*! \brief copy constructor (defaulted) */
  ImagingConfiguration(ImagingConfiguration const&) = default;

  /*! \brief move constructor (defaulted) */
  ImagingConfiguration(ImagingConfiguration&&) = default;

  /*! \brief destructor */
  ~ImagingConfiguration() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(ImagingConfiguration const&) -> ImagingConfiguration& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ImagingConfiguration&&) -> ImagingConfiguration& = default;

  /*! \brief serialized size */
  [[nodiscard]] auto
  serialized_size() const -> std::size_t;

  /*! \brief serialize to buffer */
  auto
  serialize(void* buffer) const -> std::size_t;

  /*! \brief deserialize from buffer */
  auto
  deserialize(void const* buffer) -> std::size_t;
};

/*! \brief grid export configuration */
struct ExportGridConfiguration {

  unsigned channel_block_size{DataPacketConfiguration::num_channels_per_packet};
  unsigned stokes_block_size{4};

  /*! \brief default constructor (defaulted) */
  ExportGridConfiguration() = default;

  /* \brief copy constructor (defaulted) */
  ExportGridConfiguration(ExportGridConfiguration const&) = default;

  /* \brief move constructor (defaulted) */
  ExportGridConfiguration(ExportGridConfiguration&&) noexcept = default;

  /*! \brief destructor (defaulted) */
  ~ExportGridConfiguration() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(ExportGridConfiguration const&)
    -> ExportGridConfiguration& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ExportGridConfiguration&&) noexcept
    -> ExportGridConfiguration& = default;

  /*! \brief write configuration to file stream */
  auto
  write_config(std::ofstream& of) -> void;

  /*! \brief serialized size */
  [[nodiscard]] auto
  serialized_size() const -> std::size_t;

  /*! \brief serialize to buffer */
  auto
  serialize(void* buffer) const -> std::size_t;

  /*! \brief deserialize from buffer */
  auto
  deserialize(void const* buffer) -> std::size_t;
};

/*! \brief symcam conviguration */
struct Configuration {

  static constexpr unsigned num_polarizations =
    DataPacketConfiguration::num_polarizations;
  static constexpr unsigned num_receivers =
    DataPacketConfiguration::num_receivers;

  static constexpr unsigned num_polarization_products =
    num_polarizations * num_polarizations;
  static constexpr unsigned num_baselines =
    (num_receivers * (num_receivers + 1)) / 2;

  static constexpr unsigned max_num_subarrays = SYMCAM_MAX_NUM_SUBARRAYS;
  static constexpr unsigned subarray_name_max_length =
    SYMCAM_SUBARRAY_NAME_MAX_LENGTH;

  /*! \brief maximum number of ImagingConfigurations
   *
   * \todo use build configuration value
   */
  static constexpr auto max_num_imaging_configurations = 4;

  static constexpr auto name_array_size = 20;
  static constexpr auto max_name_len = name_array_size - 1;

  using uvw_value_t = RCP_UVW_VALUE_TYPE;
  using frequency_t = RCP_FREQUENCY_VALUE_TYPE;
  using cf_value_t = RCP_CF_VALUE_TYPE;
  using grid_value_precision_t = double;

  // TODO: remove the following four constants
  static constexpr auto force_wide_intermediate_grid_sums =
    hpg::force_wide_local_sums;
  static constexpr auto use_compensated_grid_summation =
    hpg::use_compensated_grid_summation;
  static constexpr auto use_compensated_weights_summation =
    hpg::use_compensated_summation_on_patch;
  static constexpr auto enable_parallel_compensated_summation =
    hpg::use_parallel_compensated_summation;

  static constexpr auto fsample_interval =
    DataPacketConfiguration::fsample_interval;

  bool show{};
  bool dry_run{};
  std::string error;
  std::string help;
  std::string input_file;

  std::string rcp_instance_id{};
  rcp::dc::channel_type num_sample_channels_full_band{};
  rcp::dc::channel_type sample_channel_range_offset{};
  rcp::dc::channel_type sample_channel_range_size{};
  frequency_t sample_channel0_frequency_hz{};
  frequency_t sample_channel_width_hz{};
  std::array<unsigned, 3> array_extent_m{};

  /*! \brief minimum fill fraction report interval (sec)
   *
   * 0 means no reporting
   */
  unsigned min_fill_report_interval_sec{};

  /*! \brief initial events file name   */
  std::string initial_events_file;
  /*! \brief initial events script */
  std::string initial_events_script;

  /*! \brief offset of pipeline state capture trigger from data capture
   * completion, expressed as a number of capture task completions (i.e, capture
   * buffers)
   *
   * This sets the deadline for the receipt of events that determine the stream
   * state for capture regions. Positive values are used for deadlines that lag
   * the completion of capture tasks (ultimately leading to some buffering of
   * capture regions), while negative values are used for deadlines that lead
   * the completion of capture tasks (implying that receipts of events must lead
   * their "activation" times). A value of 0 indicates that the trigger occurs
   * immediately upon the completion of a capture task.
   */
  int state_capture_offset{0};

  /*! \brief offset of pipeline "live" state capture trigger from data capture
   * completion, expressed as a number of capture task completions (i.e, capture
   * buffers)
   *
   * This sets the deadline for the receipt of events that determine the stream
   * "live" state for capture regions. Positive values are used for deadlines
   * that lag the completion of capture tasks, while negative values are used
   * for deadlines that lead the completion of capture tasks. A value of 0
   * indicates that the live state is computed using the timestamp of the
   * capture buffer itself. This value should always be strictly negative to
   * ensure that capture buffers are available for captured data prior to the
   * arrival of the data (positive values are valid, however.)
   */
  int live_capture_offset{-1};

  DataCaptureConfiguration dc;
  CrossCorrelationConfiguration xc;
  std::vector<GriddingConfiguration> gr;
  std::vector<ImageConfiguration> im;
  ExportGridConfiguration eg;

  /*! \brief flag to control whether processing of a group of visibilities for a
   * single timestamp occurs in a child task or not
   */
  bool child_task_per_integration{true};

  /*! \brief default constructor (defaulted) */
  Configuration() = default;

  /*! \brief constructor from command line arguments */
  Configuration(int argc, char* const* argv, bool no_write = false);

  /*! \brief copy constructor (defaulted) */
  Configuration(Configuration const&) = default;

  /*! \brief move constructor (defaulted) */
  Configuration(Configuration&&) = default;

  /*! \brief destructor (defaulted) */
  ~Configuration() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(Configuration const&) -> Configuration& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(Configuration&&) -> Configuration& = default;

  /*! \brief period of data capture sweeps */
  [[nodiscard]] auto
  sweep_period() const -> std::chrono::milliseconds;

  /*! \brief time interval of a single data capture region */
  [[nodiscard]] auto
  capture_interval() const -> DataPacketConfiguration::fsample_duration_t;

  /*! \brief fill fraction report period
   *
   * measured in number of capture regions
   */
  [[nodiscard]] auto
  fill_report_period() const -> unsigned;

  /*! \brief correlation interval */
  [[nodiscard]] auto
  correlation_interval() const -> DataPacketConfiguration::fsample_duration_t;

  /*! \brief visibility interval
   *
   * note that correlation_interval() is the interval of the visibilities from
   * the cross-correlator, while the quantity returned from this function is the
   * interval after (potentially) additional integration
   */
  [[nodiscard]] auto
  visibility_interval() const -> std::chrono::nanoseconds;

  /*! \brief maximum number of correlation timesteps per capture region
   *
   * The value is basically ceil(capture_interval(), correlation_interval()).
   */
  [[nodiscard]] auto
  max_correlation_timesteps_per_capture_interval() const -> std::size_t;

  /*! \brief ratio of visibility_interval() to
   *  CrossCorrelationConfiguration::correlation_interval()
   */
  [[nodiscard]] auto
  visibility_integration_factor() const -> std::uint64_t;

  [[nodiscard]] auto
  imaging_configurations() const -> std::vector<ImagingConfiguration>;

  [[nodiscard]] auto
  visibility_channels_partition_kind(ImageConfiguration const&) const
    -> L::PartitionKind;

  /*! \brief frequency limits of image channels
   *
   * center frequencies of the visibility channels at the limits of each image
   * channel
   */
  [[nodiscard]] auto
  image_channel_configurations(ImageConfiguration const& im) const
    -> std::vector<std::array<frequency_t, 2>>;

  /*! \brief size of grid pixel in meters for a visibility channel index
   *
   *  Note that a value is returned regardless of whether or not the visibility
   *  channel is within any of the image channels.
   */
  [[nodiscard]] auto
  visibility_channel_scale(unsigned channel, ImageConfiguration const& im) const
    -> std::array<uvw_value_t, 3>;

  /*! \brief size of grid pixel in meters for the highest frequency visibility
   * in an image channel
   */
  [[nodiscard]] auto
  image_channel_scale(unsigned channel, ImageConfiguration const& im) const
    -> std::array<uvw_value_t, 3>;

  /*! \brief wavelength for a visibility channel index */
  [[nodiscard]] inline auto constexpr channel_wavelength(unsigned ch) const
    -> frequency_t {
    return rcp::wavelength(
      sample_channel0_frequency_hz + float(ch) * sample_channel_width_hz);
  }

  /*! \brief converter from meters to wave cycles for a visibility channel index
   */
  [[nodiscard]] auto
  meters_to_cycles(unsigned ch) const {
    return rcp::meters_to_cycles(
      sample_channel0_frequency_hz + float(ch) * sample_channel_width_hz);
  }

  /*! \brief converter from wave cycles to meters for a visibility channel index
   */
  [[nodiscard]] auto
  cycles_to_meters(unsigned ch) const {
    return rcp::cycles_to_meters(
      sample_channel0_frequency_hz + float(ch) * sample_channel_width_hz);
  }

  /*! \brief convert U,V,W to grid X,Y,W-layer coordinates for a visibility
   * channel index
   */
  auto
  uvw_meters_to_xyw(
    unsigned ch,
    ImageConfiguration const& im,
    std::ranges::range auto&& uvw) const {

    return im.uvw_cycles_to_xyw(
      uvw | std::views::transform(meters_to_cycles(ch)));
  }

  /*! \brief pipeline instance name */
  [[nodiscard]] auto
  pipeline_instance_id() const -> std::string;

  [[nodiscard]] auto
  do_show() const -> bool;

  [[nodiscard]] auto
  do_run() const -> bool;

  /*! \brief write configuration to file stream */
  auto
  write_config(std::ofstream& of) -> void;

  [[nodiscard]] auto
  serialized_size() const -> std::size_t;

  auto
  serialize(void* buffer) const -> std::size_t;

  auto
  deserialize(void const* buffer) -> std::size_t;

private:
  [[nodiscard]] auto
  image_interval(std::chrono::seconds const& max_interval) const
    -> std::chrono::nanoseconds;
};

/*! \brief serialization/deserialization functions for Configuration */
class ConfigurationSerdez {
public:
  using FIELD_TYPE = Configuration;
  static std::size_t const MAX_SERIALIZED_SIZE = 8192;

  static auto
  serialized_size(FIELD_TYPE const& val) -> std::size_t;

  static auto
  serialize(FIELD_TYPE const& val, void* buffer) -> std::size_t;

  static auto
  deserialize(FIELD_TYPE& val, void const* buffer) -> std::size_t;

  static void
  destroy(FIELD_TYPE& val);
};

/*! \brief error representing correlation integration that is not a multiple of
 * the timesteps block size */
class CorrelationNotMultipleOfBlock : public std::runtime_error {

public:
  CorrelationNotMultipleOfBlock(Configuration const&);
  CorrelationNotMultipleOfBlock() = delete;
  CorrelationNotMultipleOfBlock(CorrelationNotMultipleOfBlock const&) noexcept =
    default;
  CorrelationNotMultipleOfBlock(CorrelationNotMultipleOfBlock&&) = delete;
  auto
  operator=(CorrelationNotMultipleOfBlock const&) noexcept
    -> CorrelationNotMultipleOfBlock& = default;
  auto
  operator=(CorrelationNotMultipleOfBlock&&) noexcept
    -> CorrelationNotMultipleOfBlock& = delete;
  ~CorrelationNotMultipleOfBlock() override = default;
};

/*! \brief error representing packet size that is not a multiple of correlation
 * integration */
class PacketNotMultipleOfCorrelation : public std::runtime_error {

public:
  PacketNotMultipleOfCorrelation(Configuration const&);
  PacketNotMultipleOfCorrelation() = delete;
  PacketNotMultipleOfCorrelation(
    PacketNotMultipleOfCorrelation const&) noexcept = default;
  PacketNotMultipleOfCorrelation(PacketNotMultipleOfCorrelation&&) = delete;
  auto
  operator=(PacketNotMultipleOfCorrelation const&) noexcept
    -> PacketNotMultipleOfCorrelation& = default;
  auto
  operator=(PacketNotMultipleOfCorrelation&&) noexcept
    -> PacketNotMultipleOfCorrelation& = delete;
  ~PacketNotMultipleOfCorrelation() override = default;
};

/*! \brief error representing correlation integration this is not a multiple of
 * the packet size */
class CorrelationNotMultipleOfPacket : public std::runtime_error {

public:
  CorrelationNotMultipleOfPacket(Configuration const&);
  CorrelationNotMultipleOfPacket() = delete;
  CorrelationNotMultipleOfPacket(
    CorrelationNotMultipleOfPacket const&) noexcept = default;
  CorrelationNotMultipleOfPacket(CorrelationNotMultipleOfPacket&&) = delete;
  auto
  operator=(CorrelationNotMultipleOfPacket const&) noexcept
    -> CorrelationNotMultipleOfPacket& = default;
  auto
  operator=(CorrelationNotMultipleOfPacket&&) noexcept
    -> CorrelationNotMultipleOfPacket& = delete;
  ~CorrelationNotMultipleOfPacket() override = default;
};

/*! \brief error for invalid visibility interval
 *
 * This can happen when the configuration does not result in a valid visibility
 * interval.
 */
class InvalidVisibilityInterval : public std::runtime_error {

public:
  InvalidVisibilityInterval(Configuration const&);
  InvalidVisibilityInterval() = delete;
  InvalidVisibilityInterval(InvalidVisibilityInterval const&) noexcept =
    default;
  InvalidVisibilityInterval(InvalidVisibilityInterval&&) = delete;
  auto
  operator=(InvalidVisibilityInterval const&) noexcept
    -> InvalidVisibilityInterval& = default;
  auto
  operator=(InvalidVisibilityInterval&&) noexcept
    -> InvalidVisibilityInterval& = delete;
  ~InvalidVisibilityInterval() override = default;
};

/*! \brief error for invalid path to initial events file
 */
class InitialEventsFileNotFound : public std::runtime_error {

public:
  InitialEventsFileNotFound(Configuration const&);
  InitialEventsFileNotFound() = delete;
  InitialEventsFileNotFound(InitialEventsFileNotFound const&) noexcept =
    default;
  InitialEventsFileNotFound(InitialEventsFileNotFound&&) = delete;
  auto
  operator=(InitialEventsFileNotFound const&) noexcept
    -> InitialEventsFileNotFound& = default;
  auto
  operator=(InitialEventsFileNotFound&&) noexcept
    -> InitialEventsFileNotFound& = delete;
  ~InitialEventsFileNotFound() override = default;
};

/*! \brief stream insertion operator for StokesMask */
auto
operator<<(std::ostream&, StokesMask const&) -> std::ostream&;

/*! \brief stream extraction operator for StokesMask */
auto
operator>>(std::istream&, StokesMask&) -> std::istream&;

/*! \brief stream insertion operator for DataCaptureConfiguration */
auto
operator<<(std::ostream&, DataCaptureConfiguration const&) -> std::ostream&;

/*! \brief stream insertion operator for CrossCorrelationConfiguration */
auto
operator<<(std::ostream&, CrossCorrelationConfiguration const&)
  -> std::ostream&;

/*! \brief stream insertion operator for Configuration */
auto
operator<<(std::ostream&, Configuration const&) -> std::ostream&;

/*! \brief stream insertion operator for ImageConfiguration */
auto
operator<<(std::ostream&, ImageConfiguration const&) -> std::ostream&;

/*! \brief stream insertion operator for ExportGridConfiguration */
auto
operator<<(std::ostream&, ExportGridConfiguration const&) -> std::ostream&;

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
