// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

/*! \file BaselineRegion.hpp
 *
 * Definition of regions indexed by baseline.
 */
#include "Configuration.hpp"
#include "StaticFields.hpp"

#include <array>

namespace rcp::symcam {

/*! \brief axes for baseline indexing */
enum class BaselineAxes {
  index = LEGION_DIM_0 /*!< baseline index */
};

/*! \brief IndexSpec specialization for BaselineAxes
 *
 * \tparam Coord index coordinate value type
 */
template <typename Coord>
using baseline_index_spec_t =
  IndexSpec<BaselineAxes, BaselineAxes::index, BaselineAxes::index, Coord>;

/*! \brief default static bounds for regions with BaselineAxes
 *
 * \tparam Coord index coordinate value type
 *
 * Bounds are [0, Configuration::num_baselines - 1]
 */
template <typename Coord>
constexpr auto default_baseline_bounds = std::array{
  IndexInterval<Coord>::bounded(0, Configuration::num_baselines - 1)};

/*! \brief field for baseline */
using BaselineField =
  StaticField<std::array<Configuration::uvw_value_t, 3>, field_ids::baseline>;

/*! \brief baselines region definition */
using BaselineRegion = StaticRegionSpec<
  baseline_index_spec_t<int>,
  default_baseline_bounds<int>,
  BaselineField>;

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
