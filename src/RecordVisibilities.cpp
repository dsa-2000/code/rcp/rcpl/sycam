// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "RecordVisibilities.hpp"
#include "EventProcessing.hpp"
#include "Serializable.hpp"
#include "libsymcam.hpp"

#include <rcp/Mapper.hpp>
#include <rcp/rcp.hpp>
#ifdef SYMCAM_ENABLE_ADIOS2
#include <adios2/common/ADIOSTypes.h>
#endif
#include <bark/log.hpp>
#include <cstring>

/*! \file RecordVisibilities.cpp
 */

// Don't define USE_COMPACT_LAYOUT, as the current implementation for adios2 i/o
// isn't correct wnen the value is defined. Unfortunately, since the
// implementation can't work without it, adios2 support is basically broken. The
// way to fix it would be to restrict the channel range for the visibilities
// region requrement of the task launch to just the selected channels, and then
// request a compact layout for that subregion. Because the channel list is a
// field of the State region, it cannot be accessed (without blocking) in the
// main task, so there would have to be an intermediate level task launched to
// create the subregion. FIXME
//
// use compact layout for physical instances
// #define USE_COMPACT_LAYOUT

namespace {

/*! \brief replace (final) '%' character in path, if any, with instance_name */
auto
insert_instance_name(
  std::
    array<char, rcp::symcam::RecordVisibilities::max_instance_name_size> const&
      instance_name,
  std::filesystem::path const& path) -> std::filesystem::path {

  std::filesystem::path result = path;
  auto filename = result.filename().string();
  auto pct_pos = filename.rfind('%');
  if (pct_pos != std::string::npos) {
    filename.erase(pct_pos, 1);
    filename.insert(
      pct_pos, instance_name.data(), std::strlen(instance_name.data()));
    result.replace_filename(filename);
  }
  return result;
}
} // namespace

namespace rcp::symcam {

#ifdef SYMCAM_ENABLE_HDF5
template <typename F>
auto
check_h5c(F&& fn) {
  auto result = fn();
  if (result < 0)
    std::abort();
  return result;
}

class QuietH5Exceptions {

  H5E_auto2_t m_prev_handler{nullptr};

public:
  QuietH5Exceptions() {
    auto err = H5Eget_auto2(H5E_DEFAULT, &m_prev_handler, nullptr);
    if (err < 0)
      m_prev_handler = nullptr;
    else
      check_h5c([&]() { return H5Eset_auto2(H5E_DEFAULT, nullptr, nullptr); });
  }
  QuietH5Exceptions(QuietH5Exceptions const&) = delete;
  QuietH5Exceptions(QuietH5Exceptions&&) noexcept = default;
  auto
  operator=(QuietH5Exceptions const&) -> QuietH5Exceptions& = delete;
  auto
  operator=(QuietH5Exceptions&&) noexcept -> QuietH5Exceptions& = default;
  virtual ~QuietH5Exceptions() {
    check_h5c(
      [&]() { return H5Eset_auto2(H5E_DEFAULT, m_prev_handler, nullptr); });
  }
};

template <typename T>
struct h5t_native {
  static constexpr auto
  type() -> H5::PredType const* {
    return nullptr;
  }
};
template <>
struct h5t_native<char> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_CHAR;
  }
};
template <>
struct h5t_native<unsigned char> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_UCHAR;
  }
};
template <>
struct h5t_native<short> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_SHORT;
  }
};
template <>
struct h5t_native<unsigned short> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_USHORT;
  }
};
template <>
struct h5t_native<int> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_INT;
  }
};
template <>
struct h5t_native<unsigned int> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_UINT;
  }
};
template <>
struct h5t_native<long> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_LONG;
  }
};
template <>
struct h5t_native<unsigned long> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_ULONG;
  }
};
template <>
struct h5t_native<long long> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_LLONG;
  }
};
template <>
struct h5t_native<unsigned long long> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_ULLONG;
  }
};
template <>
struct h5t_native<float> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_FLOAT;
  }
};
template <>
struct h5t_native<double> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_DOUBLE;
  }
};
template <>
struct h5t_native<long double> {
  static constexpr auto
  type() noexcept -> H5::PredType const* {
    return &H5::PredType::NATIVE_LDOUBLE;
  }
};
template <typename T>
H5::PredType const& h5t_native_t = *(h5t_native<T>::type());

template <typename T>
struct complex_typename {
  static constexpr char const* name = "";
};

template <>
struct complex_typename<std::complex<float>> {
  static constexpr char const* const name = "COMPLEX_FLOAT";
};

template <>
struct complex_typename<std::complex<double>> {
  static constexpr char const* const name = "COMPLEX_DOUBLE";
};
#endif // SYMCAM_ENABLE_HDF5

auto
reset_handle(RecordVisibilitiesHandle* handle) -> void {
  *handle = POSIXHandle{-1};
}

auto
valid_handle(RecordVisibilitiesHandle const* handle) -> bool {
  return !std::holds_alternative<POSIXHandle>(*handle)
         || (std::get<POSIXHandle>(*handle).fd >= 0);
}

/*! \brief task to record visibilities */
class RecordVisibilitiesTask {
public:
  static constexpr char const* task_name = "RecordVisibilitiesTask";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  static constexpr auto enabled_variants =
    variant_set{task_variant<TaskVariant::IO>{}};

  using cbp_t = BsetChannelBaselinePolprodRegion;
  using part_t = DataPartitionsRegion;
  using state_t = st::StateRegion;
  using vrec_t = VisRecorderRegion;

  using handle_t = int;

  struct Args {
    std::array<char, RecordVisibilities::max_instance_name_size> instance_name;
    StreamClock::duration interval;
    int prev_state_region_index;
    int current_state_region_index;
    int next_state_region_index;
  };

  enum {
    recorder_handle_region_index = 0,
    visibilities_region_index,
    partitions_region_index,
    num_fixed_regions,
  };

  using domain_factors_t = std::array<std::vector<cbp_t::coord_t>, cbp_t::dim>;

  static auto
  xprod_factorization(
    std::vector<std::array<std::array<cbp_t::coord_t, cbp_t::dim>, 2>> const&
      rects) -> domain_factors_t {
    std::set<cbp_t::coord_t> channels;
    std::set<cbp_t::coord_t> baselines;
    std::set<cbp_t::coord_t> polarization_products;
    for (auto&& rect : rects) {
      std::ranges::copy(
        std::views::iota(
          rect[0][dim(cbp_t::axes_t::channel)],
          rect[1][dim(cbp_t::axes_t::channel)] + 1),
        std::inserter(channels, channels.end()));
      std::ranges::copy(
        std::views::iota(
          rect[0][dim(cbp_t::axes_t::baseline)],
          rect[1][dim(cbp_t::axes_t::baseline)] + 1),
        std::inserter(baselines, baselines.end()));
      std::ranges::copy(
        std::views::iota(
          rect[0][dim(cbp_t::axes_t::polarization_product)],
          rect[1][dim(cbp_t::axes_t::polarization_product)] + 1),
        std::inserter(polarization_products, polarization_products.end()));
    }
    domain_factors_t result;
    result[dim(cbp_t::axes_t::channel)] =
      std::vector(channels.begin(), channels.end());
    result[dim(cbp_t::axes_t::baseline)] =
      std::vector(baselines.begin(), baselines.end());
    result[dim(cbp_t::axes_t::polarization_product)] =
      std::vector(polarization_products.begin(), polarization_products.end());
    return result;
  }

  static auto
  rectangles(L::Domain const& domain)
    -> std::vector<std::array<std::array<cbp_t::coord_t, cbp_t::dim>, 2>> {
    std::vector<std::array<std::array<cbp_t::coord_t, cbp_t::dim>, 2>> result;
    for (L::RectInDomainIterator<cbp_t::dim, cbp_t::coord_t> rid(domain); rid();
         rid++) {
      static_assert(cbp_t::dim == 3);
      std::array<cbp_t::coord_t, cbp_t::dim> lo{
        rid->lo[0], rid->lo[1], rid->lo[2]};
      std::array<cbp_t::coord_t, cbp_t::dim> hi{
        rid->hi[0], rid->hi[1], rid->hi[2]};
      result.push_back({lo, hi});
    }
    return result;
  }

  static constexpr char const* visibilities_name = "visibilities";
  static constexpr char const* baseline_type_name = "BASELINE_TYPE";
  static constexpr char const* channels_name = "channels";
  static constexpr char const* baselines_name = "baselines";
  static constexpr char const* time_axis_attr_name = "TIME_AXIS";
  static constexpr char const* baseline_axis_attr_name = "BASELINE_AXIS";
  static constexpr char const* channel_axis_attr_name = "CHANNEL_AXIS";
  static constexpr char const* polprod_axis_attr_name =
    "POLARIZATION_PRODUCT_AXIS";
  static constexpr char const* epoch_attr_name = "EPOCH";
  static constexpr char const* interval_attr_name = "INTERVAL";

#ifdef SYMCAM_ENABLE_HDF5
  static auto
  free_h5_handle(H5Handle& handle) noexcept -> void {

    QuietH5Exceptions qex;

    // file
    if (H5::IdComponent::isValid(handle.file.getId()))
      handle.file.close();

    // datatypes
    if (H5::IdComponent::isValid(handle.visibility_type.getId()))
      handle.visibility_type.close();

    // data set
    if (H5::IdComponent::isValid(handle.visibility_data_set.getId()))
      handle.visibility_data_set.close();

    // memory data space
    if (H5::IdComponent::isValid(handle.mem_space.getId()))
      handle.mem_space.close();
  }

  static auto
  create_h5_handle(
    std::filesystem::path const& path,
    cbp_t::rect_t const& domain,
    L::Domain const& subdomain,
    StreamClock::external_time const& epoch,
    StreamClock::duration const& interval) -> std::optional<H5Handle> {

    QuietH5Exceptions qex;

    std::optional<H5Handle> result = H5Handle{};
    auto subdomain_rectangles = rectangles(subdomain);
    assert(!subdomain_rectangles.empty());
    auto subdomain_factors = xprod_factorization(subdomain_rectangles);
    try {
      // create file
      result->file = H5::H5File(path.c_str(), H5F_ACC_EXCL);

      // create complex datatype for visibilities
      hsize_t two{2};
      result->visibility_type = H5::ArrayType(
        h5t_native_t<VisibilityField::value_t::value_type>, 1, &two);
      result->visibility_type.commit(
        result->file, complex_typename<VisibilityField::value_t>::name);

      // create receiver pair datatype
      auto baseline_type = H5::ArrayType(h5t_native_t<cbp_t::coord_t>, 1, &two);
      baseline_type.commit(result->file, baseline_type_name);

      // create visibilities data space
      auto const time_axis = 0;
      auto const channel_axis = dim(cbp_t::axes_t::channel) + 1;
      auto const baseline_axis = dim(cbp_t::axes_t::baseline) + 1;
      auto const polprod_axis = dim(cbp_t::axes_t::polarization_product) + 1;
      std::array<hsize_t, cbp_t::dim + 1> dims{};
      std::ranges::copy(
        subdomain_factors
          | std::views::transform(&std::vector<cbp_t::coord_t>::size),
        &dims[1]);
      dims[time_axis] = 0;
      std::array<hsize_t, cbp_t::dim + 1> maxdims = dims;
      maxdims[time_axis] = H5S_UNLIMITED;
      auto file_space = H5::DataSpace(dims.size(), dims.data(), maxdims.data());
      H5::DSetCreatPropList dcpl;
      dims[time_axis] = 1;
      dcpl.setChunk(dims.size(), dims.data());
      result->visibility_data_set = result->file.createDataSet(
        visibilities_name, result->visibility_type, file_space, dcpl);
      dcpl.close();

      // write channel list to root group as attribute
      {
        auto file_channels_space = H5::DataSpace(1, &dims[channel_axis]);
        auto file_channels = result->file.createAttribute(
          channels_name, h5t_native_t<cbp_t::coord_t>, file_channels_space);
        file_channels.write(
          h5t_native_t<cbp_t::coord_t>,
          subdomain_factors[dim(cbp_t::axes_t::channel)].data());
        file_channels.close();
        file_channels_space.close();
      }
      // write baseline list (receiver pairs) to root group as attribute
      {
        auto file_baselines_space = H5::DataSpace(1, &dims[baseline_axis]);
        auto file_baselines = result->file.createAttribute(
          baselines_name, baseline_type, file_baselines_space);
        std::vector<std::array<cbp_t::coord_t, 2>> baselines;
        baselines.reserve(dims[baseline_axis]);
        std::ranges::copy(
          subdomain_factors[dim(cbp_t::axes_t::baseline)]
            | std::views::transform(baseline_to_receiver_pair<cbp_t::coord_t>),
          std::back_inserter(baselines));
        file_baselines.write(baseline_type, baselines.data());
        file_baselines.close();
        file_baselines_space.close();
      }
      baseline_type.close();

      // write time axis index to data set as attribute
      auto scalar_space = H5::DataSpace();
      auto time_axis_index = result->visibility_data_set.createAttribute(
        time_axis_attr_name, h5t_native_t<int>, scalar_space);
      time_axis_index.write(h5t_native_t<int>, &time_axis);
      time_axis_index.close();

      // write channel axis index to data set as attribute
      auto channel_axis_index = result->visibility_data_set.createAttribute(
        channel_axis_attr_name, h5t_native_t<int>, scalar_space);
      channel_axis_index.write(h5t_native_t<int>, &channel_axis);
      channel_axis_index.close();

      // write baseline axis index to data set as attribute
      auto baseline_axis_index = result->visibility_data_set.createAttribute(
        baseline_axis_attr_name, h5t_native_t<int>, scalar_space);
      baseline_axis_index.write(h5t_native_t<int>, &baseline_axis);
      baseline_axis_index.close();

      // write polarization product axis index to data set as attribute
      auto polprod_axis_index = result->visibility_data_set.createAttribute(
        polprod_axis_attr_name, h5t_native_t<int>, scalar_space);
      polprod_axis_index.write(h5t_native_t<int>, &polprod_axis);
      polprod_axis_index.close();

      // write epoch to root group as attribute
      auto epoch_val = epoch.time_since_epoch().count();
      auto epoch = result->file.createAttribute(
        epoch_attr_name, h5t_native_t<decltype(epoch_val)>, scalar_space);
      epoch.write(h5t_native_t<decltype(epoch_val)>, &epoch_val);
      epoch.close();

      // write interval to root group as attribute
      auto interval_val = interval.count();
      auto interval = result->file.createAttribute(
        interval_attr_name, h5t_native_t<decltype(interval_val)>, scalar_space);
      interval.write(h5t_native_t<decltype(interval_val)>, &interval_val);
      interval.close();

      // set up the hdf5 dataspace for data in memory
#ifndef USE_COMPACT_LAYOUT
      std::array<hsize_t, cbp_t::dim> domain_dims{};
      for (auto&& idx : std::views::iota(0, cbp_t::dim))
        domain_dims[idx] = domain.hi[idx] - domain.lo[idx] + 1;
      result->mem_space = H5::DataSpace(domain_dims.size(), domain_dims.data());
      decltype(dims) count;
      std::ranges::fill(count, 1);
      decltype(dims) start;
      decltype(dims) block;
      for (auto&& idx : std::views::iota(0, cbp_t::dim)) {
        start[idx] = subdomain_rectangles[0][0][idx] - domain.lo[idx];
        block[idx] =
          subdomain_rectangles[0][1][idx] - subdomain_rectangles[0][0][idx] + 1;
      }
      result->mem_space.selectHyperslab(
        H5S_SELECT_SET, count.data(), start.data(), nullptr, block.data());
      for (auto&& rect : std::views::drop(subdomain_rectangles, 1)) {
        std::ranges::copy(rect[0], &start[1]);
        for (auto&& idx : std::views::iota(0, cbp_t::dim)) {
          start[idx] = rect[0][idx] - domain.lo[idx];
          block[idx] = rect[1][idx] - rect[0][idx] + 1;
        }
        result->mem_space.selectHyperslab(
          H5S_SELECT_OR, count.data(), start.data(), nullptr, block.data());
      }
#else
      result->mem_space = H5::DataSpace(dims.size(), dims.data());
#endif // USE_COMPACT_LAYOUT
      log().debug(
        std::format("Opened {} for visibility recording", path.string()));
    } catch (H5::Exception const& ex) {
      log().warn<std::string, std::string>(
        "Failed to open visibility recording file",
        bark::val_field_t{"path", path.string()},
        bark::val_field_t{"error", ex.getDetailMsg()});
      free_h5_handle(*result);
      result.reset();
    }
    return result;
  }

  static auto
  close_handle(H5Handle& handle) -> void {
    free_h5_handle(handle);
  }

  static auto
  write_to_handle(H5Handle& handle, VisibilityField::value_t const* buff)
    -> void {
    // extend the data set by one step in the time dimension
    auto file_space = handle.visibility_data_set.getSpace();
    file_space.selectAll();
    std::array<hsize_t, cbp_t::dim + 1> dims{};
    std::array<hsize_t, cbp_t::dim + 1> maxdims{};
    assert(file_space.getSimpleExtentNdims() == dims.size());
    file_space.getSimpleExtentDims(dims.data(), maxdims.data());
    ++dims[0];
    handle.visibility_data_set.extend(dims.data());
    file_space.setExtentSimple(dims.size(), dims.data(), maxdims.data());
    // select all elements at the added time value
    decltype(dims) start{};
    std::ranges::fill(start, 0);
    start[0] = dims[0] - 1;
    decltype(dims) count{};
    std::ranges::fill(count, 1);
    auto block = dims;
    block[0] = 1;
    file_space.selectHyperslab(
      H5S_SELECT_SET, count.data(), start.data(), nullptr, block.data());
    // write the data to the dataset
    handle.visibility_data_set.write(
      buff, handle.visibility_type, handle.mem_space, file_space);
    file_space.close();
  }
#endif // SYMCAM_ENABLE_HDF5

#ifdef SYMCAM_ENABLE_ADIOS2
  static auto
  create_adios2_handle(
    std::filesystem::path const& path,
    adios2::Params const& params,
    cbp_t::rect_t const& domain,
    L::Domain const& subdomain,
    StreamClock::external_time const& epoch,
    StreamClock::duration const& interval) -> std::optional<Adios2Handle> {

    std::optional<Adios2Handle> result = Adios2Handle{};
    result->io = result->adios.DeclareIO("RecordVisibilities");
    result->io.SetEngine(params.at("engine_type"));
    result->io.SetParameters(params);

    auto const channel_axis = dim(cbp_t::axes_t::channel);
    auto const baseline_axis = dim(cbp_t::axes_t::baseline);
    auto const polprod_axis = dim(cbp_t::axes_t::polarization_product);
    result->io.DefineAttribute(channel_axis_attr_name, channel_axis);
    result->io.DefineAttribute(baseline_axis_attr_name, baseline_axis);
    result->io.DefineAttribute(polprod_axis_attr_name, polprod_axis);
    result->io.DefineAttribute(
      epoch_attr_name, epoch.time_since_epoch().count());
    result->io.DefineAttribute(interval_attr_name, interval.count());

    auto subdomain_rectangles = rectangles(subdomain);
    assert(!subdomain_rectangles.empty());
    auto subdomain_factors = xprod_factorization(subdomain_rectangles);

    // define channel list variable
    auto const& channels = subdomain_factors[dim(cbp_t::axes_t::channel)];
    auto channels_var = result->io.DefineVariable<cbp_t::coord_t>(
      channels_name, {}, {}, {channels.size()}, true);

    // define baseline (as receiver pair) list variable
    auto const& baselines = subdomain_factors[dim(cbp_t::axes_t::baseline)];
    auto baselines_var = result->io.DefineVariable<cbp_t::coord_t>(
      baselines_name, {}, {}, {baselines.size(), 2}, true);

    // define visibilities variable
    std::vector<size_t> dims(cbp_t::dim);
    std::ranges::copy(
      subdomain_factors
        | std::views::transform(&std::vector<cbp_t::coord_t>::size),
      dims.data());
    result->io.DefineVariable<VisibilityField::value_t>(
      visibilities_name, {}, {}, dims, true);

    // open engine
    result->engine = result->io.Open(path, adios2::Mode::Write);
    result->engine.BeginStep();
    // write channels list variable, first time step only
    result->engine.Put(channels_var, channels.data());
    // write receiver pairs list, first time step only
    std::vector<cbp_t::coord_t> rcv_pairs;
    rcv_pairs.reserve(2 * baselines.size());
    for (auto&& bl : baselines) {
      auto [rcvA, rcvB] = baseline_to_receiver_pair(bl);
      rcv_pairs.push_back(rcvA);
      rcv_pairs.push_back(rcvB);
    }
    result->engine.Put(baselines_var, rcv_pairs.data());
    return result;
  }

  static auto
  close_handle(Adios2Handle& handle) -> void {
    if (handle.initial_step)
      handle.engine.EndStep();
    handle.engine.Close();
  }

  static auto
  write_to_handle(Adios2Handle& handle, VisibilityField::value_t const* buff)
    -> void {
#ifndef USE_COMPACT_LAYOUT
    std::abort();
#endif
    if (!handle.initial_step)
      handle.engine.BeginStep();
    handle.engine.Put(
      handle.io.InquireVariable<VisibilityField::value_t>(visibilities_name),
      buff);
    handle.engine.EndStep();
    handle.initial_step = false;
  }

#endif // SYMCAM_ENABLE_ADIOS2

  static auto
  create_handle(
    Args const& args,
    st::State::DataStream const& data_stream,
    cbp_t::rect_t const& domain,
    L::Domain subdomain,
    StreamClock::external_time const& epoch,
    StreamClock::duration const& interval)
    -> std::optional<RecordVisibilitiesHandle> {

    std::optional<RecordVisibilitiesHandle> result;
    auto path = data_stream.filepath();
    if (path.is_absolute()) {
      auto path_with_id = insert_instance_name(args.instance_name, path);
      auto const js_params =
        nlohmann::json::parse(data_stream.parameters)
          .get<std::unordered_map<std::string, nlohmann::json>>();
      std::map<std::string, std::string> params;
      std::ranges::copy(
        js_params | std::views::transform([&](auto&& kv) {
          auto& [key, value] = kv;
          return std::make_pair(key, value.template get<std::string>());
        }),
        std::inserter(params, params.end()));
      if (
        !params.contains("engine_type") && path_with_id.extension() == ".h5") {
#ifdef SYMCAM_ENABLE_HDF5
        result =
          create_h5_handle(path_with_id, domain, subdomain, epoch, interval);
#else
        log().warn(
          "HDF5 is not enabled in symcam: visibilities will not be "
          "recorded",
          bark::val_field_t{"path", path.string()});
#endif // SYMCAM_ENABLE_HDF5
      } else if (params.contains("engine_type")) {
#ifdef SYMCAM_ENABLE_ADIOS2
        result = create_adios2_handle(
          path_with_id, params, domain, subdomain, epoch, interval);
#else
        log().warn(
          "ADIOS2 is not enabled in symcam: visibilities will not be "
          "recorded",
          bark::val_field_t{"path", path.string()});
#endif // SYMCAM_ENABLE_ADIOS2
      } else {
        log().warn(
          "Plain binary POSIX file visibility recording not implemented");
      }
    } else {
      log().warn(
        "Path to visibility recording file is not absolute: ignoring "
        "command",
        bark::val_field_t{"path", path.string()});
    }
    return result;
  }

  static auto
  close_handle(POSIXHandle& handle) -> void {
    if (handle.fd >= 0)
      std::abort();
  }

  static auto
  write_to_handle(
    POSIXHandle& /*handle*/, VisibilityField::value_t const* /*buff*/) -> void {
    std::abort();
  }

  static auto
  ch_rect(rcp::dc::channel_type lo, rcp::dc::channel_type hi)
    -> L::Rect<cbp_t::dim, cbp_t::coord_t> {
    L::Rect<cbp_t::dim, cbp_t::coord_t> result;
    result.lo[dim(cbp_t::axes_t::channel)] = lo;
    result.hi[dim(cbp_t::axes_t::channel)] = hi;
    result.lo[dim(cbp_t::axes_t::baseline)] = -10'000'000;
    result.hi[dim(cbp_t::axes_t::baseline)] = 10'000'000;
    result.lo[dim(cbp_t::axes_t::polarization_product)] = -100;
    result.hi[dim(cbp_t::axes_t::polarization_product)] = 100;
    return result;
  };

  static auto
  io_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    assert(regions.size() >= num_fixed_regions);
    assert(task->arglen == sizeof(Args));

    // we don't do parallel i/o (yet), so only one point task should do anything
    //
    // TODO: remove this check once parallel i/o is implemented
    if (task->index_point != task->index_domain.lo())
      return;

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    Args const& args = *reinterpret_cast<Args const*>(task->args);
    // recording target handle
    auto const& recorder_handle_region = regions[recorder_handle_region_index];
    auto& handle = vrec_t::values<LEGION_READ_WRITE>(
      recorder_handle_region,
      RecordingHandleField{})[vrec_t::rect_t{recorder_handle_region}.lo];

    // previous stream state
    std::optional<st::StateField::value_t> prev_state;
    if (args.prev_state_region_index >= num_fixed_regions) {
      prev_state = state_t::values<LEGION_READ_ONLY>(
        regions[args.prev_state_region_index],
        st::StateField{})[state_t::rect_t{regions[args.prev_state_region_index]}
                            .lo];
    }
    // current stream state
    assert(args.current_state_region_index >= num_fixed_regions);
    auto const current_state = state_t::values<LEGION_READ_ONLY>(
      regions[args.current_state_region_index], st::StateField{})
      [state_t::rect_t{regions[args.current_state_region_index]}.lo];
    // next stream state
    std::optional<st::StateField::value_t> next_state;
    if (args.next_state_region_index >= num_fixed_regions) {
      next_state = state_t::values<LEGION_READ_ONLY>(
        regions[args.next_state_region_index],
        st::StateField{})[state_t::rect_t{regions[args.next_state_region_index]}
                            .lo];
    }
    // subarray
    auto subarray = part_t::rect_t{regions[partitions_region_index]}
                      .lo[dim(part_t::axes_t::subarray)];

    // filepaths for previous, current and next states
    std::filesystem::path prev_filepath;
    if (prev_state)
      prev_filepath = prev_state->record_data_stream(subarray).filepath();
    auto current_data_stream = current_state.record_data_stream(subarray);
    auto current_filepath = current_data_stream.filepath();
    std::filesystem::path next_filepath;
    if (next_state)
      next_filepath = next_state->record_data_stream(subarray).filepath();

    if (!current_filepath.empty()) {

      // create a new output file if needed
      if (prev_filepath != current_filepath) {
        // visibilities partition by subarray
        auto const partition = part_t::values<LEGION_READ_ONLY>(
          regions[partitions_region_index],
          VisibilitiesSubarrayPartitionField{})[subarray];
        // domain for visibilities in this subarray
        auto subarray_domain = rt->get_index_space_domain(
          get_visibilities_subarray_subspace(rt, partition, subarray));
        // domain for selected channels
        std::vector<L::Rect<cbp_t::dim, cbp_t::coord_t>> channel_rects;
        for (auto&& interval : current_data_stream.channel_intervals)
          channel_rects.push_back(ch_rect(interval[0], interval[1]));
        auto channels_is = rt->create_index_space(ctx, channel_rects);
        auto channels_domain = rt->get_index_space_domain(channels_is);
        // intersection of domain for visibilities in this subarray within
        // selected channels
        auto subarray_channels_domain =
          L::Domain(channels_domain).intersection(subarray_domain);
        rt->destroy_index_space(ctx, channels_is);

        auto maybe_handle = create_handle(
          args,
          current_data_stream,
          cbp_t::rect_t{regions[visibilities_region_index]},
          subarray_channels_domain,
          current_state.timestamp(),
          args.interval);
        if (!maybe_handle)
          return;
        handle = std::move(*maybe_handle);
      }

      if (valid_handle(&handle)) {
        // This task is expected to map the entire visibilities region, and then
        // access only the elements within the subarray and selected channels.
        // Doing an index launch over the partition of visibilities by subarray
        // would be an alternative, but we're using this design since it seems
        // simpler given the current design of sequential iteration over
        // subarrays.
        auto const* visibilities =
          cbp_t::values<LEGION_READ_ONLY>(
            regions[visibilities_region_index], VisibilityField{})
            .ptr(cbp_t::rect_t{regions[visibilities_region_index]}.lo);
        std::visit(
          [&](auto&& hdl) { write_to_handle(hdl, visibilities); }, handle);
      }

      if (next_filepath != current_filepath) {
        // close file (handle)
        std::visit([](auto&& hdl) { close_handle(hdl); }, handle);
        reset_handle(&handle);
      }
    }
  }

  template <TaskVariant Variant>
  static auto
  body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {
    static_assert(Variant == TaskVariant::IO);
    io_body(task, regions, ctx, rt);
  }

  /*! \brief visibilities layout
   *
   * Don't need PortableLayout here.
   */
  static auto
  vis_layout(L::Runtime* rt) -> L::LayoutConstraintID {
    static L::LayoutConstraintID result;
    static std::once_flag flag;
    std::call_once(flag, [&]() {
      L::LayoutConstraintRegistrar registrar(
        L::FieldSpace::NO_SPACE, "record_vis layout");
      // we want the axis order (polarization product, baseline, channel) in
      // order of increasing strides, i.o.w. the row-major ordering of the
      // logical dimensions. This produces a data set in which the element
      // order appears more "natural" .
      registrar.add_constraint(L::OrderingConstraint(
        {L::DimensionKind(cbp_t::axes_t::polarization_product),
         L::DimensionKind(cbp_t::axes_t::baseline),
         L::DimensionKind(cbp_t::axes_t::channel),
         LEGION_DIM_F},
        true /* contiguous */));
#ifdef USE_COMPACT_LAYOUT
      registrar.add_constraint(
        L::SpecializedConstraint(LEGION_COMPACT_SPECIALIZE));
#endif
      if (rt == nullptr)
        result = L::Runtime::preregister_layout(registrar);
      else
        result = rt->register_layout(registrar);
    });
    return result;
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.add_layout_constraint_set(
      visibilities_region_index, vis_layout(nullptr));
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.add_layout_constraint_set(
      visibilities_region_index, vis_layout(rt));
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID RecordVisibilitiesTask::task_id;

RecordVisibilities::RecordVisibilities(
  L::Context ctx,
  L::Runtime* rt,
  Configuration const& config,
  std::size_t shard_point,
  std::size_t num_shards)
  : m_interval(config.visibility_interval())
  , m_launcher(RecordVisibilitiesTask::task_id, L::UntypedBuffer())
  , m_vrec_handle(std::make_unique<decltype(m_vrec_handle)::element_type>()) {

  auto name = config.pipeline_instance_id();
  assert(name.size() < sizeof(m_instance_name));
  std::strncpy(m_instance_name.data(), name.c_str(), sizeof(m_instance_name));
  m_instance_name[sizeof(m_instance_name) - 1] = '\0';

  // create a region for handle instance
  auto maybe_is = vrec_t::index_space(
    ctx,
    rt,
    {{vrec_t::axes_t::shard, vrec_t::range_t::right_bounded(num_shards - 1)}});
  assert(maybe_is);
  // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
  auto is = maybe_is.value();
  auto fs = vrec_t::field_space(ctx, rt);
  auto vrec_lr = vrec_t::LogicalRegion{rt->create_logical_region(ctx, is, fs)};
  auto ip = rt->create_equal_partition(ctx, is, is);
  m_vrec_lp = {
    vrec_t::LogicalPartition{rt->get_logical_partition(vrec_lr, ip)}, vrec_lr};

  // Initialize VisRecorder instances.
  {
    reset_handle(m_vrec_handle.get());
    auto ext_vrec_handle = Realm::ExternalMemoryResource(
      m_vrec_handle.get(), sizeof(*m_vrec_handle));
    auto launcher =
      L::IndexAttachLauncher(LEGION_EXTERNAL_INSTANCE, vrec_lr, true);
    launcher.initialize_constraints(
      false, false, {RecordingHandleField::field_id});
    auto subregion =
      rt->get_logical_subregion_by_color(m_vrec_lp.partition(), shard_point);
    launcher.add_external_resource(subregion, &ext_vrec_handle);
    launcher.privilege_fields.insert(RecordingHandleField::field_id);
    m_vrec_ext_resources = rt->attach_external_resources(ctx, launcher);
  }

  m_launcher.region_requirements.resize(
    RecordVisibilitiesTask::num_fixed_regions);
  auto vrec0_lr = RegionPair<vrec_t::LogicalRegion>(
    vrec_t::LogicalRegion{rt->get_logical_subregion_by_color(
      vrec_t::logical_partition_t{m_vrec_lp.partition()},
      L::Point<1, vrec_t::coord_t>{0})},
    vrec_lr);
  m_launcher
    .region_requirements[RecordVisibilitiesTask::recorder_handle_region_index] =
    vrec0_lr.requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{RecordingHandleField{}});
}

RecordVisibilities::RecordVisibilities(
  L::Context ctx, L::Runtime* rt, Serialized const& serialized)
  : m_vrec_lp{serialized.m_vrec_lp}
  , m_interval{serialized.m_interval}
  , m_instance_name{serialized.m_instance_name}
  , m_launcher(RecordVisibilitiesTask::task_id, L::UntypedBuffer()) {

  m_launcher.region_requirements.resize(
    RecordVisibilitiesTask::num_fixed_regions);
  auto vrec0_lr = RegionPair<vrec_t::LogicalRegion>(
    vrec_t::LogicalRegion{rt->get_logical_subregion_by_color(
      vrec_t::logical_partition_t{serialized.m_vrec_lp.partition()},
      L::Point<1, vrec_t::coord_t>{0})},
    serialized.m_vrec_lp.parent(rt));
  m_launcher
    .region_requirements[RecordVisibilitiesTask::recorder_handle_region_index] =
    vrec0_lr.requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{RecordingHandleField{}});
}

auto
RecordVisibilities::serialized(L::Context ctx, L::Runtime* rt) const noexcept
  -> Serialized {
  return {m_vrec_lp, m_interval, m_instance_name};
}

auto
RecordVisibilities::child_requirements(L::Context ctx, L::Runtime* rt) const
  -> ChildRequirements {

  return {
    {m_vrec_lp.parent(rt).requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{RecordingHandleField{}},
      std::nullopt,
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP)},
    {},
    {}};
}

auto
RecordVisibilities::destroy(L::Context ctx, L::Runtime* rt) -> void {
  auto detached = rt->detach_external_resources(ctx, m_vrec_ext_resources);
  rt->destroy_index_space(ctx, m_vrec_lp.parent(rt).get_index_space());
  rt->destroy_field_space(ctx, m_vrec_lp.parent(rt).get_field_space());
  rt->destroy_logical_region(ctx, m_vrec_lp.parent(rt));
  detached.wait();
}

auto
RecordVisibilities::init_launcher(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<cbp_t::LogicalRegion> const& visibilities,
  std::array<RegionPair<state_t::LogicalRegion>, 3> const& states,
  L::UntypedBuffer arg) -> void {

  auto* argp = reinterpret_cast<RecordVisibilitiesTask::Args*>(arg.get_ptr());
  argp->interval = m_interval;
  argp->prev_state_region_index = -1;
  argp->current_state_region_index = -1;
  argp->next_state_region_index = -1;

  std::ranges::copy(m_instance_name, argp->instance_name.data());
  int region_index = RecordVisibilitiesTask::num_fixed_regions;
  std::vector<L::RegionRequirement> state_reqs;
  assert(states[1].region() != state_t::LogicalRegion{});
  auto state0_select = st::State0Select(ctx, rt, states[1]);
  if (states[0].region() != state_t::LogicalRegion{}) {
    argp->prev_state_region_index = region_index++;
    state_reqs.push_back(state0_select(states[0]).requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}}));
  }
  argp->current_state_region_index = region_index++;
  state_reqs.push_back(state0_select(states[1]).requirement(
    LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}}));
  if (states[2].region() != state_t::LogicalRegion{}) {
    argp->next_state_region_index = region_index++;
    state_reqs.push_back(state0_select(states[2]).requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}}));
  }
  m_launcher.region_requirements.resize(
    RecordVisibilitiesTask::num_fixed_regions + state_reqs.size());
  std::ranges::copy(
    state_reqs,
    &m_launcher.region_requirements[RecordVisibilitiesTask::num_fixed_regions]);
  m_launcher
    .region_requirements[RecordVisibilitiesTask::visibilities_region_index] =
    visibilities.requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{VisibilityField{}});
  m_launcher.argument = arg;
}

auto
RecordVisibilities::update_launcher_for_subarray(
  L::Runtime* rt,
  PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
  unsigned subarray_slot) -> void {

  auto partitions_subregion = get_partitions_region_for_subarray(
    rt, partitions_by_subarray, subarray_slot);
  // partitioning isn't done in task launch, rather the partitions are used
  // to guide iteration when recording (by both subarray and channel)
  m_launcher
    .region_requirements[RecordVisibilitiesTask::partitions_region_index] =
    partitions_subregion.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{VisibilitiesSubarrayPartitionField{}});
}

auto
RecordVisibilities::launch(
  L::Context ctx,
  L::Runtime* rt,
  PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
  RegionPair<cbp_t::LogicalRegion> const& visibilities,
  std::array<RegionPair<state_t::LogicalRegion>, 3> const& states,
  st::subarray_predicates_array_t const& predicates) -> void {

  auto do_record = predicate_slice_subarray_predicates(
    predicates, st::SubarrayStreamPredicate::is_recording_visibilities);
  RecordVisibilitiesTask::Args args;
  auto arg = L::UntypedBuffer(&args, sizeof(args));
  init_launcher(ctx, rt, visibilities, states, arg);
  for (auto&& subarray_slot : subarray_slots()) {
    m_launcher.predicate = do_record(subarray_slot);
    update_launcher_for_subarray(rt, partitions_by_subarray, subarray_slot);
    rt->execute_task(ctx, m_launcher);
  }
}

auto
RecordVisibilities::launch(
  L::Context ctx,
  L::Runtime* rt,
  PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
  RegionPair<cbp_t::LogicalRegion> const& visibilities,
  std::array<RegionPair<state_t::LogicalRegion>, 3> const& states,
  st::AllSubarraysQuery const& state_query) -> void {

  auto do_record = state_query.do_record_visibilities();
  RecordVisibilitiesTask::Args args;
  auto arg = L::UntypedBuffer(&args, sizeof(args));
  init_launcher(ctx, rt, visibilities, states, arg);
  for (auto&& subarray_slot : subarray_slots()) {
    if (do_record.at(subarray_slot)) {
      update_launcher_for_subarray(rt, partitions_by_subarray, subarray_slot);
      rt->execute_task(ctx, m_launcher);
    }
  }
}

auto
RecordVisibilities::preregister_tasks(L::TaskID task_id) -> void {
#ifdef SYMCAM_ENABLE_HDF5
  H5::H5Library::open();
#endif // SYMCAM_ENABLE_HDF5
  PortableTask<RecordVisibilitiesTask>::preregister_task_variants(task_id);
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
