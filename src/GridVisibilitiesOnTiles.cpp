// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "GridVisibilitiesOnTiles.hpp"
#include "GridRegion.hpp"
#include "GridTileRegion.hpp"
#include "libsymcam.hpp"

#include <hpg/config.hpp>
#include <hpg/core_rc.hpp>
#include <hpg/gridding_rc.hpp>
#include <legion/legion_config.h>
#include <rcp/Mapper.hpp>

#include <algorithm>
#include <limits>
#include <rcp/rcp.hpp>
#include <vector>

using namespace rcp::symcam;

namespace {

template <bool CompensatedSum>
auto
register_grid_reduction_op(Legion::Runtime* /*rt*/) -> void {};

template <>
[[maybe_unused]] auto
register_grid_reduction_op<true>(Legion::Runtime* rt) -> void {
  if (rt == nullptr) {
    GridReduction<true>::REDOP_ID =
      rcp::L::Runtime::generate_static_reduction_id();
    rcp::L::Runtime::register_reduction_op<GridReduction<true>>(
      GridReduction<true>::REDOP_ID);
  } else {
    GridReduction<true>::REDOP_ID = rt->generate_dynamic_reduction_id();
    rcp::L::Runtime::register_reduction_op<GridReduction<true>>(
      GridReduction<true>::REDOP_ID);
  }
};

template <bool CompensatedSum>
auto
register_weight_reduction_op(Legion::Runtime* /*rt*/) -> void {};

template <>
[[maybe_unused]] auto
register_weight_reduction_op<true>(Legion::Runtime* rt) -> void {
  if (rt == nullptr) {
    WeightsReduction<true>::REDOP_ID =
      rcp::L::Runtime::generate_static_reduction_id();
    rcp::L::Runtime::register_reduction_op<WeightsReduction<true>>(
      WeightsReduction<true>::REDOP_ID);
  } else {
    WeightsReduction<true>::REDOP_ID = rt->generate_dynamic_reduction_id();
    rcp::L::Runtime::register_reduction_op<WeightsReduction<true>>(
      WeightsReduction<true>::REDOP_ID);
  }
};
} // namespace

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
Legion::ProjectionID GriddingProjection::projection_id;

auto
GriddingProjection::project(
  L::LogicalRegion /*region*/,
  L::DomainPoint const& /*point*/,
  L::Domain const& /*domain*/) -> L::LogicalRegion {
  // unused
  assert(false);
  return L::LogicalRegion::NO_REGION;
}

auto
GriddingProjection::project(
  L::LogicalPartition upper_bound,
  L::DomainPoint const& point,
  L::Domain const& /*domain*/) -> L::LogicalRegion {

  auto ip = upper_bound.get_index_partition();
  L::DomainPoint pt;
  pt.dim = runtime->get_index_partition_color_space(ip).get_dim();
  std::copy(&point.point_data[0], &point.point_data[pt.dim], pt.point_data);
  if (runtime->has_index_subspace(ip, pt))
    return runtime->get_logical_subregion_by_color(upper_bound, pt);
  return L::LogicalRegion::NO_REGION;
}

auto
GriddingProjection::get_depth() const -> unsigned {
  return 0;
}

auto
GriddingProjection::is_functional() const -> bool {
  return true;
}

namespace rcp::symcam {

template struct SummableReduction<GridPixelField::rvalue_t>;
template struct SummableReduction<GridWeightField::rvalue_t>;

/*! scalar type for all polarization products of a visibility value
 *
 * Values of this type can be used in Kokkos reductions
 *
 * \tparam T complex of visibility values
 */
template <typename T>
class visprod {

  std::array<T, Configuration::num_polarization_products> m_vals{};

public:
  RCP_INLINE_FUNCTION
  visprod() = default;
  RCP_INLINE_FUNCTION
  visprod(const visprod& rhs) = default;
  RCP_INLINE_FUNCTION
  visprod(visprod&&) noexcept = default;
  RCP_INLINE_FUNCTION
  auto
  operator=(visprod const&) -> visprod& = default;
  RCP_INLINE_FUNCTION
  auto
  operator=(visprod&&) noexcept -> visprod& = default;
  RCP_INLINE_FUNCTION
  ~visprod() = default;
  RCP_INLINE_FUNCTION
  auto
  operator+=(visprod const& src) -> visprod& {
    for (int i = 0; i < int(Configuration::num_polarization_products); ++i)
      m_vals[i] += src.m_vals[i];
    return *this;
  }
  RCP_INLINE_FUNCTION constexpr auto
  vals(std::size_t index) -> T& {
    assert(index < m_vals.size()); // opt-in bounds check
    return m_vals[index];          // NOLINT
  }
  RCP_INLINE_FUNCTION constexpr auto
  vals(std::size_t index) const -> T const& {
    assert(index < m_vals.size()); // opt-in bounds check
    return m_vals[index];          // NOLINT
  }
};

/** almost atomic complex addition
 *
 * We want atomic addition to avoid the use of locks when possible, that is, to
 * be implemented only by either a real atomic addition function or a
 * compare-and-swap loop. We define this function in order to specialize it in
 * cases where 128 bit CAS functions don't exist (CUDA and HIP), and we want at
 * least a chance of compiling to the actual atomic add functions. In the
 * specialized cases, with only simple complex arguments, this function is
 * almost atomic since the real and imaginary components are updated
 * sequentially, albeit non-deterministically between real and imaginary parts
 * (on top of atomic updates being non-deterministic to begin with).
 */
template <typename execution_space, typename T>
RCP_INLINE_FUNCTION void
quasi_atomic_add(T* const acc, T const& val) {
  Kokkos::atomic_add(acc, val);
}

#ifdef RCP_USE_CUDA
template <>
RCP_INLINE_FUNCTION void
quasi_atomic_add<Kokkos::Cuda, Kokkos::complex<double>>(
  Kokkos::complex<double>* const acc, Kokkos::complex<double> const& val) {

  Kokkos::atomic_add(&acc->real(), val.real());
  Kokkos::atomic_add(&acc->imag(), val.imag());
}
template <>
RCP_INLINE_FUNCTION void
quasi_atomic_add<Kokkos::Cuda, Kokkos::complex<float>>(
  Kokkos::complex<float>* const acc, Kokkos::complex<float> const& val) {

  Kokkos::atomic_add(&acc->real(), val.real());
  Kokkos::atomic_add(&acc->imag(), val.imag());
}
#endif // RCP_USE_CUDA

/*! portable sincos() */
#ifdef RCP_USE_CUDA
#pragma nv_exec_check_disable
#endif
template <typename execution_space, typename T>
RCP_FORCEINLINE_FUNCTION void
sincos(T ph, T* sn, T* cs) {
  *sn = std::sin(ph);
  *cs = std::cos(ph);
}

#ifdef RCP_USE_CUDA
template <>
RCP_FORCEINLINE_FUNCTION void
sincos<Kokkos::Cuda, float>(float ph, float* sn, float* cs) {
  ::sincosf(ph, sn, cs);
}

template <>
RCP_FORCEINLINE_FUNCTION void
sincos<Kokkos::Cuda, double>(double ph, double* sn, double* cs) {
  ::sincos(ph, sn, cs);
}
#endif // RCP_USE_CUDA

/*! convert phase to complex value */
template <typename execution_space, typename T>
RCP_FORCEINLINE_FUNCTION auto
cphase(T ph) -> Kokkos::complex<T> {
  Kokkos::complex<T> result;
  sincos<execution_space, T>(ph, &result.imag(), &result.real());
  return result;
}

/*! magnitude of K::complex<T> value */
RCP_INLINE_FUNCTION auto
mag(auto const& val) {
  return std::hypot(val.real(), val.imag());
}

} // end namespace rcp::symcam

namespace Kokkos { // reduction identities must be defined in Kokkos namespace
/*! reduction identity of visprod */
template <typename T>
struct reduction_identity<rcp::symcam::visprod<T>> {
  static KOKKOS_INLINE_FUNCTION auto
  sum() -> rcp::symcam::visprod<T> {
    return rcp::symcam::visprod<T>();
  }
};
} // end namespace Kokkos

namespace rcp::symcam {

struct GriddingTask : DefaultKokkosTaskMixin<GriddingTask, void> {
  static constexpr char const* task_name = "GriddingTask";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  enum {
    grid_region_index = 0,
    weights_region_index,
    mueller_region_index,
    visibilities_region_index,
    visibility_weights_region_index,
    uvws_region_index,
    cf_array_region_index,
    num_regions
  };

  using cbp_t = BsetChannelBaselinePolprodRegion;
  using bc_t = BaselineChannelRegion;
  using grid_t = GridRegion;
  using gw_t = GridWeightsRegion;
  using cf_t = CFRegion;
  using mi_t = MuellerIndexRegion;

  template <typename execution_space>
  using member_type = typename TeamPolicy<execution_space>::member_type;

  /*! values maintained in Kokkos scratch memory for inner loop of gridding
   */
  struct ScratchMemT {
    VisibilityField::kvalue_t vis;
    ForwardIndexField::value_t mindex;
  };
  /*! view type for vector of values of type ScratchMemT */
  template <typename execution_space>
  using ScratchView =
    K::View<ScratchMemT*, typename execution_space::scratch_memory_space>;

  // deduce the value type of intermediate grid accumulation value as the
  // product of a visibility and a CF value, or set it to the final grid
  // precision if forced
  using intermediate_grid_sum_t = std::conditional_t<
    Configuration::force_wide_intermediate_grid_sums,
    accval_value_t<GridPixelField::kvalue_t>,
    std::common_type_t<VisibilityField::kvalue_t, CFValueField::kvalue_t>>;

  static_assert(
    sizeof(GridPixelField::kvalue_t) >= sizeof(intermediate_grid_sum_t));
  static_assert(std::is_same_v<
                accval_value_t<GridWeightField::value_t>,
                accval_value_t<GridPixelField::kvalue_t>::value_type>);

  // deduce value type of intermediate sum-of-weights accumulation value
  //
  // - if CF value precision equals grid value precision, then intermediate
  //   value precision is the same and compensated summation is determined by
  //   configuration
  //
  // - if CF value precision is narrower than grid value precision, then
  //   intermediate value precision is determined by whether compensated
  //   summation (for sum-of-weights) is enabled
  //
  //   - if enabled, then intermediate value precision is narrow and compensated
  //     summation is used
  //
  //   - if not enabled, then intermediate value precision is wide and
  //     compensated summation is not used
  using intermediate_sum_weights_t = std::conditional_t<
    std::is_same_v<
      CFValueField::kvalue_t,
      accval_value_t<GridPixelField::kvalue_t>>,
    weights_accval<CFValueField::kvalue_t>,
    std::conditional_t<
      is_compensated_sum_v<weights_accval<CFValueField::kvalue_t>>,
      weights_accval<CFValueField::kvalue_t>,
      accval_value_t<GridPixelField::kvalue_t>>>;

  static RCP_INLINE_FUNCTION auto
  vis_to_uvw_rect(cbp_t::rect_t const& vr) -> bc_t::rect_t {
    bc_t::rect_t result;
    result.lo[dim(bc_t::axes_t::baseline)] =
      vr.lo[dim(cbp_t::axes_t::baseline)];
    result.lo[dim(bc_t::axes_t::channel)] = vr.lo[dim(cbp_t::axes_t::channel)];
    result.hi[dim(bc_t::axes_t::baseline)] =
      vr.hi[dim(cbp_t::axes_t::baseline)];
    result.hi[dim(bc_t::axes_t::channel)] = vr.hi[dim(cbp_t::axes_t::channel)];
    return result;
  }

  /*! compute grid and CF coordinates needed for resampling of a value at a
   *  (floating point) UV coordinate
   *
   * Computed coordinates may refer to the points in the domain of the CF
   * function translated to the position of the visibility on the fine
   * (oversampled) grid.
   *
   * The four returned coordinates are as follows
   * - leftmost grid coordinate of (visibility-centered) CF support
   * - leftmost major CF coordinate within CF support
   * - the "minor" coordinate of the CF, always non-negative
   * - offset of visibility (on fine grid) to nearest major grid
   *   point (positive or negative)
   *
   * The value of the minor coordinate must be between 0 and oversampling -
   * 1 (inclusive); its computation proceeds as follows:
   *
   * - G is the grid coordinate nearest position
   * - fine_offset is the distance from (visibility) position to nearest
   *   grid coordinate, (G - position) * oversampling
   * - points at which CF are evaluated are {(I - (position - G)) *
   *   oversampling} or {I * oversampling + fine_offset} for I in some range
   *   of integers
   * - the left edge of the support of CF is nominally at CFArray::padding
   * - CFArray employs a decomposed form of 1d index i as (i / oversampling,
   *   i % oversampling), where the second component is always between 0 and
   *   oversampling - 1
   * - if fine_offset >= 0, {I * oversampling +
   *   fine_offset}, and the CF indexes are (I, fine_offset)
   * - if fine_offset < 0, {I * oversampling + fine_offset}
   *   = {(I - 1) * oversampling + (fine_offset + oversampling)}, and the CF
   *   indexes are (I - 1, fine_offset + oversampling)
   *
   * \return array comprising four coordinates
   */
  static RCP_INLINE_FUNCTION auto
  resampling_coords(
    coord_t cf_lo_padded,
    cf_t::Params const& params,
    Configuration::uvw_value_t coord,
    Configuration::uvw_value_t cell_size) -> std::array<coord_t, 4> {

    double const position = double(cell_size) * coord;
    coord_t grid_coord = std::lround(position);
    coord_t const ovs{params.oversampling};
    coord_t const fine_offset =
      std::lround((double(grid_coord) - position) * double(ovs));
    assert(-ovs < fine_offset && fine_offset < ovs);
    auto const cf_lo = cf_lo_padded + params.padding;
    grid_coord += cf_lo;
    coord_t cf_minor{};
    coord_t cf_major{};
    if (fine_offset >= 0) {
      cf_minor = fine_offset;
      cf_major = cf_lo;
      assert(cf_minor < ovs);
    } else {
      cf_minor = ovs + fine_offset;
      cf_major = cf_lo - 1;
      assert(0 <= cf_minor);
    }
    return {grid_coord, cf_major, cf_minor, fine_offset};
  }

  template <
    typename execution_space,
    typename VvT,
    typename VwvT,
    typename CfvT,
    typename MiT,
    typename GvT,
    typename GwT>
  static RCP_INLINE_FUNCTION void
  grid_visibility(
    member_type<execution_space> const& team_member,
    VvT const& vis,
    VwvT const& vwgt,
    CfvT const& cf,
    Configuration::cf_value_t const cf_im_factor,
    MiT const& mindexes,
    GvT const& grid,
    GwT const& grid_weights) {

    // compute the values of the phase screen along the Y axis now and store
    // the results in scratch memory because gridding on the Y axis accesses
    // the phase screen values for every row of the Mueller matrix column
    //
    // K::parallel_for(
    //   K::TeamVectorRange(team_member, N_Y),
    //   [=](const int Y) {
    //     phi_Y(Y) = vis.m_phi0[1] + Y * vis.m_dphi[1];
    //   });
    // team_member.team_barrier();

    // initialize the values in scratch memory
    ScratchView<execution_space> scratch(
      team_member.team_scratch(0), Configuration::num_polarization_products);
    K::parallel_for(
      K::TeamVectorRange(team_member, Configuration::num_polarization_products),
      [=](int const pp) {
        // multiply visibilities by their weights.
        scratch(pp).vis = vis(pp) * vwgt(pp);
        // copy Mueller indexes to scratch memory
        scratch(pp).mindex = mindexes(pp);
      });
    team_member.team_barrier();

    visprod<intermediate_sum_weights_t> vsumwgt;
    // parallel loop over grid X; remember, these indexes are now 0-based
    auto const N_X = grid.extent(dim(grid_t::axes_t::x));
    auto const N_Y = grid.extent(dim(grid_t::axes_t::y));
    K::parallel_reduce(
      K::TeamThreadRange(team_member, N_X),
      [&](auto grid_x, auto& vsumwgt_l) {
        // auto phi_X = vis.m_phi0[0] + X * vis.m_dphi[0];
        // loop over grid Y
        K::parallel_for(
          K::ThreadVectorRange(team_member, N_Y), [&](auto grid_y) {
            // const cf_t screen = cphase<execution_space>(phi_X + phi_Y(Y));
            intermediate_grid_sum_t gv;
            // loop over visibility polarization products
            for (int pp = 0; pp < int(Configuration::num_polarization_products);
                 ++pp) {
              if (auto const mindex = scratch(pp).mindex; mindex >= 0) {
                auto cfv = cf(grid_x, grid_y, mindex);
                // cfv must not have reference type
                static_assert(std::is_object_v<decltype(cfv)>);
                cfv.imag() *= cf_im_factor;
                gv += cfv * scratch(pp).vis /* * screen */;
                vsumwgt_l.vals(pp) += cfv;
              }
            }
            quasi_atomic_add<execution_space>(
              &grid(grid_x, grid_y),
              typename std::remove_cvref_t<decltype(grid)>::value_type(gv));
          });
      },
      K::Sum<decltype(vsumwgt), execution_space>(vsumwgt));
    // compute final weight and add it to weights
    K::single(K::PerTeam(team_member), [&]() {
      accval_value_t<WeightField::value_t> wgt{0};
      for (int pp = 0; pp < int(Configuration::num_polarization_products); ++pp)
        wgt +=
          mag(static_cast<accval_value_t<intermediate_sum_weights_t> const&>(
            vsumwgt.vals(pp)))
          * vwgt(pp);
      K::atomic_add(
        &grid_weights(),
        typename std::remove_cvref_t<decltype(grid_weights)>::value_type(wgt));
    });
  }

  template <
    typename execution_space,
    typename VisT,
    typename VwgtT,
    typename UvwT,
    typename CfiT,
    typename CfvT,
    typename CfpT,
    typename MiT,
    typename GvT,
    typename GpT,
    typename GwT>
  static void
  grid_visibilities(
    execution_space work_space,
    VisT const& visibilities,
    VwgtT const& visibility_weights,
    UvwT const& uvws,
    CfiT const& cfis,
    CfvT const& cfvs,
    CfpT const& cfv_params,
    MiT const& mueller_indexes,
    MiT const& mueller_transpose_indexes,
    GvT const& grid,
    GpT const& grid_params,
    GwT const& grid_weights,
    int vector_length,
    StokesMask stokes_mask) {

    using namespace std::string_literals;

    auto const baseline0 = visibilities.begin(dim(cbp_t::axes_t::baseline));
    auto const num_baselines =
      visibilities.extent(dim(cbp_t::axes_t::baseline));
    auto const channel0 = visibilities.begin(dim(cbp_t::axes_t::channel));
    auto const num_channels = visibilities.extent(dim(cbp_t::axes_t::channel));
    auto const stokes0 = grid.begin(dim(grid_t::axes_t::stokes));
    auto const num_stokes = grid.extent(dim(grid_t::axes_t::stokes));
    auto const num_vis_vect =
      visibilities.size() / Configuration::num_polarization_products;

    coord_t const padding{cfv_params.padding};
    static_assert(
      Configuration::num_polarization_products
      == mi_t::static_bounds[1].size());
    auto const scratch_size = K::PerTeam(
      ScratchView<execution_space>::shmem_size(mi_t::static_bounds[0].size()));

    K::parallel_for(
      "gridding",
      TeamPolicy<execution_space>(
        work_space, num_stokes * num_vis_vect, K::AUTO, vector_length)
        .set_scratch_size(0, scratch_size),
      KOKKOS_LAMBDA(member_type<execution_space> const& team_member) {
        auto vv_sto_idx = team_member.league_rank();
        auto const sto = vv_sto_idx % num_stokes + stokes0;
        vv_sto_idx /= num_stokes;
        auto const ch = vv_sto_idx % num_channels + channel0;
        vv_sto_idx /= num_channels;
        auto const bl = vv_sto_idx % num_baselines + baseline0;
        vv_sto_idx /= num_baselines;
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index)
        if (stokes_mask.val[sto] == 0)
          return;
        auto vis = KX::subview(visibilities, ch, bl, K::ALL);
        auto vwgt = KX::subview(visibility_weights, ch, bl, K::ALL);
        auto& uvw = uvws(bl, ch);
        // auto cfv_bounds = cfv_md_t::values_view_bounds(cfv);
        auto [grid_x, cf_x, cf_xm, fo_x] = resampling_coords(
          cfvs.begin(dim(cf_t::axes_t::x_major)),
          cfv_params,
          uvw[0],
          grid_params.cell_size[0]);
        auto [grid_y, cf_y, cf_ym, fo_y] = resampling_coords(
          cfvs.begin(dim(cf_t::axes_t::y_major)),
          cfv_params,
          uvw[1],
          grid_params.cell_size[1]);

        coord_t ext_x = cfvs.extent(dim(cf_t::axes_t::x_major)) - 2 * padding;
        coord_t ext_y = cfvs.extent(dim(cf_t::axes_t::y_major)) - 2 * padding;
        // note: the subview axes that don't have the value K::ALL, below,
        // lose their index offsets
        auto grid_weight_subview = KX::subview(
          grid_weights,
          sto,
          grid_weights.begin(dim(gw_t::axes_t::w_plane)),
          grid_weights.begin(dim(gw_t::axes_t::channel)));
        auto grid_subview = KX::subview(
          grid,
          K::pair(grid_x, grid_x + ext_x),
          K::pair(grid_y, grid_y + ext_y),
          sto,
          grid.begin(dim(grid_t::axes_t::w_plane)),
          grid.begin(dim(grid_t::axes_t::channel)));
        auto cf_subview = KX::subview(
          cfvs,
          K::pair(cf_x, cf_x + ext_x),
          cf_xm,
          K::pair(cf_y, cf_y + ext_y),
          cf_ym,
          K::ALL,
          cfis(bl, ch));
        auto mindexes = KX::subview(
          (uvw[2] >= 0 ? mueller_indexes : mueller_transpose_indexes),
          sto,
          K::ALL);
        grid_visibility<execution_space>(
          team_member,
          vis,
          vwgt,
          cf_subview,
          ((uvw[2] >= 0) ? -1 : 1),
          mindexes,
          grid_subview,
          grid_weight_subview);
      });
  }

  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    using namespace std::string_literals;

    assert(task->arglen == sizeof(GridVisibilitiesOnTiles::GriddingArgs));
    auto const& args =
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      *reinterpret_cast<GridVisibilitiesOnTiles::GriddingArgs const*>(
        task->args);

    auto const grid = [&]() {
      if constexpr (GridVisibilitiesOnTiles::
                      use_reduction_privileges_on_grid_pixels_and_weights) {
        return grid_t::view<
          execution_space,
          GridReduction<Configuration::use_compensated_grid_summation>>(
          regions[grid_region_index], GridPixelField{});
      } else {
        return grid_t::view<execution_space, LEGION_READ_WRITE>(
          regions[grid_region_index], GridPixelField{});
      }
    }();

    log().debug("grid "s + show(grid_t::rect_t(regions[grid_region_index])));

    auto grid_params3 = grid_t::retrieve_params(
      rt, grid_t::LogicalRegion{task->regions[grid_region_index].parent});

    auto const weights = [&]() {
      if constexpr (GridVisibilitiesOnTiles::
                      use_reduction_privileges_on_grid_pixels_and_weights) {
        return gw_t::view<
          execution_space,
          WeightsReduction<Configuration::use_compensated_grid_summation>>(
          regions[weights_region_index], GridWeightField{});

      } else {
        return gw_t::view<execution_space, LEGION_READ_WRITE>(
          regions[weights_region_index], GridWeightField{});
      }
    }();

    auto const mueller_indexes = mi_t::view<execution_space, LEGION_READ_ONLY>(
      regions[mueller_region_index], ForwardIndexField{});
    auto const mueller_transpose_indexes =
      mi_t::view<execution_space, LEGION_READ_ONLY>(
        regions[mueller_region_index], InverseIndexField{});

    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);
    execution_space work_space =
      rt->get_executing_processor(ctx).kokkos_work_space();

    auto const& vis_region = regions[visibilities_region_index];
    auto const& vis_wgt_region = regions[visibility_weights_region_index];
    auto const& uvw_region = regions[uvws_region_index];
    auto const& cfv_region = regions[cf_array_region_index];

    auto cfv_params = cf_t::retrieve_params(
      rt,
      cf_t::LogicalRegion(
        cf_t::logical_region_t(task->regions[cf_array_region_index].parent)));
    auto const cfvs =
      cf_t::view<execution_space, LEGION_READ_ONLY>(cfv_region, CFValueField{});

    // a few views that are optional in HPG, which we don't use
    K::View<hpg::flag_t***> flags;
    K::View<hpg::freq_fp_t*> frequencies;
    K::View<hpg::phase_fp_t**> phases;
    K::View<GridPixelField::kvalue_t***> model;

    // HPG uses a 2d cell size array
    hpg::runtime::impl::core::rc::grid_t::Params grid_params2;
    std::ranges::copy_n(
      grid_params3.cell_size.begin(), 2, grid_params2.cell_size.begin());

    // HPG only supports gridding to a single W-plane and channel
    auto grid_bounds = grid_t::rect_t{regions[grid_region_index]};
    assert(
      grid_bounds.lo[dim(grid_t::axes_t::w_plane)]
      == grid_bounds.hi[dim(grid_t::axes_t::w_plane)]);
    auto w_plane = grid_bounds.lo[dim(grid_t::axes_t::w_plane)];
    assert(
      grid_bounds.lo[dim(grid_t::axes_t::channel)]
      == grid_bounds.hi[dim(grid_t::axes_t::channel)]);
    auto channel = grid_bounds.lo[dim(grid_t::axes_t::channel)];
    auto grid_sv = KX::subview(grid, K::ALL, K::ALL, K::ALL, w_plane, channel);
    auto weights_sv = KX::subview(weights, K::ALL, w_plane, channel);

    // FIXME: use symcam (Legion) logger
    auto logger = hpg::runtime::impl::core::rc::ClogLogger{};

    [[maybe_unused]] std::string im_name(args.name.data());
    // do gridding
    for (auto piece = L::PieceIteratorT<cbp_t::dim, cbp_t::coord_t>(
           vis_region, VisibilityField::field_id, true);
         piece();
         piece++) {
      assert(piece->lo[dim(cbp_t::axes_t::polarization_product)] == 0);
      assert(
        piece->hi[dim(cbp_t::axes_t::polarization_product)]
        == Configuration::num_polarization_products - 1);
      log().debug("'"s + im_name + "' gridding domain "s + show(*piece));
      auto const visibilities = cbp_t::view<execution_space, LEGION_READ_ONLY>(
        vis_region, VisibilityField{}, *piece);
      auto const visibility_weights =
        cbp_t::view<execution_space, LEGION_READ_ONLY>(
          vis_wgt_region, WeightField{}, *piece);
      auto const uvws = bc_t::view<execution_space, LEGION_READ_ONLY>(
        uvw_region, UVWField{}, vis_to_uvw_rect(*piece));
      auto const cfis = bc_t::view<execution_space, LEGION_READ_ONLY>(
        uvw_region, CFIndexField{}, vis_to_uvw_rect(*piece));

      [[maybe_unused]] auto rc =
        hpg::runtime::impl::core::rc::gridding::run_gridder<false, true, true>(
          work_space,
          visibilities,
          visibility_weights,
          flags,
          frequencies,
          phases,
          uvws,
          cfis,
          std::vector{cfvs},
          cfv_params.padding,
          mueller_indexes,
          mueller_transpose_indexes,
          grid_sv,
          grid_params2,
          weights_sv,
          model,
          args.stokes_mask.val,
          1,
          false,
          logger);
    }
    log().debug("done gridding");
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;

    constexpr auto for_reduction = GridVisibilitiesOnTiles::
      use_reduction_privileges_on_grid_pixels_and_weights;
    registrar.set_leaf();
    registrar
      .add_layout_constraint_set(
        grid_region_index,
        grid_layout()
          .constraint_id({traits::layout_style, for_reduction})
          .value())
      .add_layout_constraint_set(
        weights_region_index,
        grid_weights_layout()
          .constraint_id({traits::layout_style, for_reduction})
          .value())
      .add_layout_constraint_set(
        cf_array_region_index,
        cf_layout().constraint_id(traits::layout_style).value());

    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;

    constexpr auto for_reduction = GridVisibilitiesOnTiles::
      use_reduction_privileges_on_grid_pixels_and_weights;
    registrar.set_leaf();
    registrar
      .add_layout_constraint_set(
        grid_region_index,
        grid_layout()
          .constraint_id(rt, {traits::layout_style, for_reduction})
          .value())
      .add_layout_constraint_set(
        weights_region_index,
        grid_weights_layout()
          .constraint_id(rt, {traits::layout_style, for_reduction})
          .value())
      .add_layout_constraint_set(
        cf_array_region_index,
        cf_layout().constraint_id(rt, traits::layout_style).value());

    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID GriddingTask::task_id;

} // end namespace rcp::symcam

namespace {
/*! test whether the rightmost dimension of a domain is degenerate
 *
 * Degeneracy of a dimension being defined as having a size of one.
 */
template <int dim>
auto
is_right_dim_degenerate(Legion::DomainT<dim, rcp::coord_t> const& dom) -> bool {
  // if we iterate through the points of the domain in row-major order and the
  // rightmost dimension is not degenerate, then the point values of all but
  // the rightmost dimension must repeat eventually
  Legion::Point<dim - 1, rcp::coord_t> prev;
  for (auto&& idx : std::views::iota(0U, unsigned(dim) - 1))
    prev[idx] = -1;
  for (Legion::PointInDomainIterator pid(dom, false); pid(); pid++) {
    Legion::Point<dim - 1, rcp::coord_t> pt;
    for (auto&& idx : std::views::iota(0U, unsigned(dim) - 1))
      pt[idx] = pid[idx];
    if (pt == prev)
      return false;
    prev = pt;
  }
  return true;
}
} // namespace

GridVisibilitiesOnTiles::GridVisibilitiesOnTiles(
  L::Context ctx,
  L::Runtime* rt,
  std::array<char, Configuration::name_array_size> const& name,
  int vector_length,
  StokesMask stokes_mask,
  grid_t::LogicalPartition grid_partition,
  gw_t::LogicalPartition weights_partition,
  mi_t::LogicalRegion mueller_region)
  : m_grid_partition(grid_partition)
  , m_weights_partition(weights_partition)
  , m_mueller_indexes(mueller_region)
  , m_launcher(
      GriddingTask::task_id,
      L::IndexSpace::NO_SPACE,
      L::UntypedBuffer(),
      L::ArgumentMap()) {

  m_gridding_args.name = name;
  m_gridding_args.vector_length = vector_length;
  m_gridding_args.stokes_mask = stokes_mask;

  // grid and weights regions
  m_launcher.region_requirements.resize(GriddingTask::num_regions);
  if constexpr (use_reduction_privileges_on_grid_pixels_and_weights) {
    m_launcher.region_requirements[GriddingTask::grid_region_index] =
      grid_partition.requirement(
        GriddingProjection::projection_id,
        GridReduction<Configuration::use_compensated_grid_summation>::REDOP_ID,
        LEGION_ATOMIC,
        rt,
        StaticFields{GridPixelField{}});
    m_launcher.region_requirements[GriddingTask::weights_region_index] =
      weights_partition.requirement(
        GriddingProjection::projection_id,
        WeightsReduction<
          Configuration::use_compensated_grid_summation>::REDOP_ID,
        LEGION_ATOMIC,
        rt,
        StaticFields{GridWeightField{}});
  } else {
    m_launcher.region_requirements[GriddingTask::grid_region_index] =
      grid_partition.requirement(
        GriddingProjection::projection_id,
        LEGION_READ_WRITE,
        LEGION_EXCLUSIVE,
        rt,
        StaticFields{GridPixelField{}});
    m_launcher.region_requirements[GriddingTask::weights_region_index] =
      weights_partition.requirement(
        GriddingProjection::projection_id,
        LEGION_READ_WRITE,
        LEGION_EXCLUSIVE,
        rt,
        StaticFields{GridWeightField{}});
  }
  m_launcher.region_requirements[GriddingTask::mueller_region_index] =
    mueller_region.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{ForwardIndexField{}, InverseIndexField{}});
}

template <typename T>
auto
cf_value_requirement(Legion::Runtime* rt, T const& lr)
  -> Legion::RegionRequirement {
  if constexpr (std::is_same_v<T, rcp::RegionPair<CFRegion::LogicalRegion>>) {
    return lr.requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, rcp::StaticFields{CFValueField{}});
  } else {
    return lr.requirement(
      GriddingProjection::projection_id,
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      rt,
      rcp::StaticFields{CFValueField{}});
  }
}

void
GridVisibilitiesOnTiles::launch(
  L::Context ctx,
  L::Runtime* rt,
  PartitionPair<cbp_t::LogicalPartition> const& visibility_partition,
  PartitionPair<cbp_t::LogicalPartition> const& visibility_weights_partition,
  PartitionPair<bc_t::LogicalPartition> const& uvw_partition,
  cfv_region_t const& cf_array_region,
  L::Predicate pred) {

  using tiles_t = GridTileRegion;

  // set predicate and global_arg of m_launcher
  m_launcher.predicate = std::move(pred);
  m_launcher.global_arg =
    L::UntypedBuffer(&m_gridding_args, sizeof(m_gridding_args));

  auto launch_space = rt->get_index_partition_color_space_name<
    cbp_t::dim,
    cbp_t::coord_t,
    tiles_t::dim + 1,
    tiles_t::coord_t>(cbp_t::index_partition_t(
    visibility_partition.partition().get_index_partition()));
  m_launcher.launch_space = launch_space;

  // TODO: remove after verification with updated mapper
  //
  // set the Mapper::collective_instance_tag on the grid and weights regions
  // as needed (see is_right_dim_degenerate(), which will test the tile index
  // dimension for multiplicity of x,y(,stokes) tiles for any (channel,
  // w-plane) value)
  // if (is_right_dim_degenerate(rt->get_index_space_domain(launch_space))) {
  //   m_launcher.region_requirements[GriddingTask::grid_region_index].tag &=
  //     ~Mapper::collective_instance_tag;
  //   m_launcher.region_requirements[GriddingTask::weights_region_index].tag &=
  //     ~Mapper::collective_instance_tag;
  // } else {
  //   m_launcher.region_requirements[GriddingTask::grid_region_index].tag |=
  //     Mapper::collective_instance_tag;
  //   m_launcher.region_requirements[GriddingTask::weights_region_index].tag |=
  //     Mapper::collective_instance_tag;
  // }

  // add requirements for the current chunk of visibilities, uvws,
  // cf_indexes and cf_arrays
  m_launcher.region_requirements[GriddingTask::visibilities_region_index] =
    visibility_partition.requirement(
      rt,
      0,
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{VisibilityField{}});
  m_launcher
    .region_requirements[GriddingTask::visibility_weights_region_index] =
    visibility_weights_partition.requirement(
      rt, 0, LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{WeightField{}});
  m_launcher.region_requirements[GriddingTask::uvws_region_index] =
    uvw_partition.requirement(
      rt,
      0,
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{UVWField{}, CFIndexField{}});
  m_launcher.region_requirements[GriddingTask::cf_array_region_index] =
    cf_value_requirement(rt, cf_array_region);
  rt->execute_index_space(ctx, m_launcher);
}

auto
GridVisibilitiesOnTiles::preregister_tasks(L::TaskID task_id) -> void {

  GriddingProjection::projection_id =
    L::Runtime::generate_static_projection_id();
  L::Runtime::preregister_projection_functor(
    GriddingProjection::projection_id, new GriddingProjection());

  register_grid_reduction_op<Configuration::use_compensated_grid_summation>(
    nullptr);
  register_weight_reduction_op<Configuration::use_compensated_grid_summation>(
    nullptr);
  PortableTask<GriddingTask>::preregister_task_variants(task_id);
}

auto
GridVisibilitiesOnTiles::register_tasks(L::Runtime* rt, L::TaskID task_id)
  -> void {

  GriddingProjection::projection_id = rt->generate_dynamic_projection_id();
  rt->register_projection_functor(
    GriddingProjection::projection_id, new GriddingProjection());

  register_grid_reduction_op<Configuration::use_compensated_grid_summation>(rt);
  register_weight_reduction_op<Configuration::use_compensated_grid_summation>(
    rt);
  PortableTask<GriddingTask>::register_task_variants(rt, task_id);
}

auto
GridVisibilitiesOnTiles::grid_precision_type_info() -> std::type_info const& {
  return typeid(accval_value_t<GridPixelField::kvalue_t>::value_type);
}

auto
GridVisibilitiesOnTiles::grid_uses_compensated_summation() -> bool {
  return is_compensated_sum_v<GridPixelField::kvalue_t>;
}

auto
GridVisibilitiesOnTiles::hpg_intermediate_grid_precision_type_info()
  -> std::type_info const& {
  return typeid(GriddingTask::intermediate_grid_sum_t::value_type);
}

auto
GridVisibilitiesOnTiles::hpg_intermediate_weights_precision_type_info()
  -> std::type_info const& {
  return typeid(
    accval_value_t<GriddingTask::intermediate_sum_weights_t>::value_type);
}

auto
GridVisibilitiesOnTiles::hpg_intermediate_weights_uses_compensated_summation()
  -> bool {
  return is_compensated_sum_v<GriddingTask::intermediate_sum_weights_t>;
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
