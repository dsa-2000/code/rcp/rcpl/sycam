// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "libsymcam.hpp"

#include <map>
#include <tuple>

#include <libtcc/Correlator.h>

namespace rcp::symcam {

class CUDAContext {

  cudaStream_t m_stream;
  std::map<std::tuple<unsigned, unsigned>, tcc::Correlator> m_correlators;

public:
  CUDAContext() noexcept : m_stream(nullptr) {}

  CUDAContext(const CUDAContext&) = delete;
  CUDAContext(CUDAContext&&) = delete;
  CUDAContext&
  operator=(const CUDAContext&) = delete;
  CUDAContext&
  operator=(CUDAContext&&) = delete;

  auto
  cuda_stream() -> cudaStream_t;

  auto
  correlator(unsigned num_timesteps, unsigned num_channels) -> tcc::Correlator;
};

auto
get_cuda_stream() -> cudaStream_t;

auto
get_correlator(unsigned num_timesteps, unsigned num_channels)
  -> tcc::Correlator;

/*! launcher of task to load CUDA libraries for every GPU in the machine
 */
class LoadCUDALibraries {
public:
  static constexpr const char* task_name = "LoadCUDALibraries";

  struct Args {
    unsigned num_timesteps;
    unsigned num_channels;
  };

private:
  static L::TaskID task_id;

public:
  static void
  launch(
    L::Context ctx,
    L::Runtime* rt,
    unsigned num_timesteps,
    unsigned num_channels);

  static void
  task(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const&,
    L::Context,
    L::Runtime*);

  static void
  preregister(L::TaskID task_id);
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
