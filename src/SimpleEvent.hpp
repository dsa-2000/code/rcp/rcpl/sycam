// Copyright 2023-2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "StreamState.hpp"

/*! \file SimpleEvent.hpp
 *
 * Base class for event with only a timestamp for payload.
 */

namespace rcp::symcam::st {

struct JSONSimpleEvent {

  /*! \brief JSON event timestamp value metadata member name */
  static constexpr char const* timestamp_tag = "timestamp";

  /*! \brief test for timestamp_tag member */
  [[nodiscard]] static auto
  has_timestamp(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get tag */
  [[nodiscard]] static auto
  get_timestamp(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get tag */
  [[nodiscard]] static auto
  get_timestamp(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief test for valid format */
  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

// forward declaration of SimpleEvent
template <typename Event>
struct SimpleEvent;

// forward declaration of from_json() for SimpleEvent<Event> types
template <typename Event>
auto
from_json(nlohmann::json const& js, SimpleEvent<Event>& evt) -> void;

/*! \brief error representing event with invalid "tag" value */
struct EventTagError : public JSONError {
  EventTagError(char const* val)
    : JSONError(std::string("Event with invalid 'tag' value: '") + val + "'") {}
};

/*! \brief error representing wrong target type for JSON deserialization */
struct BadEventTagError : public JSONError {

  /*! \brief constructor
   *
   * \param expected tag name of deserialized type
   * \param provided tag name in JSON
   */
  BadEventTagError(
    std::string_view const& expected, std::string_view const& provided)
    : JSONError(
        std::string("JSON event with tag '") + provided.data()
        + "' cannot be deserialized to type '" + expected.data() + "'") {}
};

/*! \brief error representing failure to parse timestamp value */
struct TimestampParseError : public JSONError {
  TimestampParseError(char const* val)
    : JSONError(std::string("Failed to parse timestamp value '") + val + "'") {}
};

/*! \brief error representing missing timestamp value */
struct NoTimestampError : public JSONError {
  NoTimestampError() : JSONError("Event timestamp is missing") {}
};

/*! \brief error representing failure to parse duration value */
struct DurationParseError : public JSONError {
  DurationParseError(char const* val)
    : JSONError(std::string("Failed to parse duration value '") + val + "'") {}
};

/* \brief parse a value that could be a duration
 *
 * Durations may be represented by any string that can be parsed as
 * a signed integer or floating point number immediately followed by
 * a suffix in the set ("h", "min", "s", "ms", "us", "µs", "ns").
 *
 * \param str u8string_view to parse
 * \result optional StreamClock::duration, non-empty iff parsing
 * succeeded
 */
auto
parse_duration(std::u8string_view const& str)
  -> std::optional<StreamClock::duration>;

/*! \brief duration as string */
auto
to_string(StreamClock::duration const& duration) -> std::string;

/*! \brief parse a value that could be a timespec
 *
 * A timespec can be a either a fixed (external) timestamp, or a
 * duration that represents a time relative to an (undefined)
 * epoch. A fixed timestamp is represented by a string that looks
 * like a duration value, which represents the time since the
 * StreamClock epoch. A relative timestamp is represented by a
 * leading Δ (U0394) character, followed immediately by an
 * duration-like string.
 *
 * \param str u8string_view to parse
 * \result optional timestamp_t, non-empty iff parsing succeeded
 */
auto
parse_timestamp(std::u8string_view const& str)
  -> std::optional<StreamClock::timestamp_t>;

/*! \brief timestamp as string */
auto
to_string(StreamClock::timestamp_t const& timestamp) -> std::string;

/*! \brief stream event without payload (beyond timestamp)
 *
 * Base class of events for ExtendedState
 */
template <typename Event>
struct SimpleEvent : public StreamStateEvent<ExtendedState, Event> {

  friend auto
  from_json<>(nlohmann::json const& js, SimpleEvent<Event>& evt) -> void;

  /*! \brief default constructor (defaulted) */
  SimpleEvent() noexcept = default;
  /*! \brief constructor from timestamp value */
  SimpleEvent(StreamClock::timestamp_t const& ts) noexcept
    : StreamStateEvent<ExtendedState, Event>(ts) {}
  /*! \brief copy constructor (defaulted) */
  SimpleEvent(SimpleEvent const&) noexcept = default;
  /*! \brief move constructor (defaulted) */
  SimpleEvent(SimpleEvent&&) noexcept = default;
  /*! \brief copy constructor with updated timestamp */
  SimpleEvent(SimpleEvent const& event, StreamClock::timestamp_t const& ts)
    : StreamStateEvent<ExtendedState, Event>(event, ts) {}
  /*! \brief move constructor with updated timestamp */
  SimpleEvent(SimpleEvent&& event, StreamClock::timestamp_t const& ts)
    : StreamStateEvent<ExtendedState, Event>(std::move(event), ts) {}

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(SimpleEvent const&) -> SimpleEvent& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(SimpleEvent&&) noexcept -> SimpleEvent& = default;
  /*! \brief destructor (defaulted) */
  ~SimpleEvent() override = default;

  /*! \brief class tag */
  [[nodiscard]] auto
  tag() const noexcept -> char const* override {
    return Event::class_tag;
  }

  /*! \brief apply event to state
   *
   * \param st state instance
   * \return copy of st, after application of event
   */
  [[nodiscard]] auto
  apply_to(ExtendedState&& st) const -> ExtendedState override {
    // NOLINTNEXTLINE(performance-move-const-arg)
    return this->set_timestamp(
      static_cast<Event const&>(*this).apply_to_no_ts(std::move(st)));
  }
};

/*! \brief convert SimpleEvent instance to JSON
 *
 * Form defined by nlohmann::json
 */
template <typename Event>
auto
to_json(nlohmann::json& js, SimpleEvent<Event> const& evt) -> void {
  js = {
    {JSONValue::meta_tag_name, Event::class_tag},
    {JSONValue::meta_value_name,
     {{JSONSimpleEvent::timestamp_tag, to_string(evt.timestamp())}}}};
}

/*! \brief convert JSON value to SimpleEvent instance
 *
 * Form defined by nlohmann::json
 */
template <typename Event>
auto
from_json(nlohmann::json const& js, SimpleEvent<Event>& evt) -> void {
  if (!JSONValue::is_valid(js))
    throw JSONValueFormatError(js);

  auto tag = JSONValue::get_tag(js);
  if (tag != std::string_view(Event::class_tag))
    throw BadEventTagError(Event::class_tag, tag.c_str());

  auto jsval = JSONValue::get_value(js);
  if (!JSONSimpleEvent::is_valid(jsval))
    throw NoTimestampError();
  auto const timestamp_str =
    JSONSimpleEvent::get_timestamp(jsval).get<std::string>();
  auto const maybe_timestamp =
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    parse_timestamp(reinterpret_cast<char8_t const*>(timestamp_str.c_str()));
  if (!maybe_timestamp)
    throw TimestampParseError(timestamp_str.c_str());
  evt.timestamp() = *maybe_timestamp;
}

/*! \brief poison pill event
 *
 * Sets "alive" state to "false"
 */
struct PoisonPill : public SimpleEvent<PoisonPill> {

  static constexpr char const* class_tag = "PoisonPill";

  /*! \brief inherited constructors */
  using SimpleEvent<PoisonPill>::SimpleEvent;

  /*! \brief set state's "alive" status to false
   *
   * \param st state instance
   * \return copy of st, but with m_is_alive set to false
   */
  [[nodiscard]] static auto
  apply_to_no_ts(ExtendedState&& st) -> ExtendedState;
};

} // end namespace rcp::symcam::st

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
