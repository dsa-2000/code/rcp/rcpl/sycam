// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

/*! \file EventProcessing.hpp
 *
 * Event log processing for symcam
 */
#include "BaselineRegion.hpp"
#include "EventLogRegion.hpp"
#include "EventScript.hpp"
#include "StateRegion.hpp"
#include "StreamState.hpp"
#include "libsymcam.hpp"
#include <experimental/mdarray>
#include <experimental/mdspan>
#include <legion/legion_types.h>

#include <compare>
#include <memory>
#include <mutex>
#include <ranges>
#include <type_traits>
#include <variant>

namespace rcp::symcam::st {

/*! \brief predicate type corresponding to State::is_alive() */
struct AlivePredicate {};
/*! \brief predicate for State::has_same_subarrays() with preceding state */
struct SubarrayChangePredicate {};
/*! \brief predicate for non-empty State::fftw_wisdom_import_path */
struct FFTWImportPredicate {};
/*! \brief predicate for non-empty State::fftw_wisdom_export_path */
struct FFTWExportPredicate {};
/*! \brief predicates for State Legion::Future */
using StatePredicate = std::variant<
  AlivePredicate,
  SubarrayChangePredicate,
  FFTWImportPredicate,
  FFTWExportPredicate>;
static_assert(std::is_trivially_copyable_v<StatePredicate>);

/*! \brief axes for region of stream predicates by subarray */
enum class SubarrayStreamPredicateAxes { subarray = LEGION_DIM_0, predicate };

/*! \brief stream predicates for every subarray */
enum class SubarrayStreamPredicate {
  is_subarray = 0,
  is_imaging_start,
  is_imaging,
  is_imaging_end,
  is_recording_visibilities,
  is_pre_calibration_flagging,
  do_fftw_import,
  do_fftw_export,
  _last = do_fftw_export
};

/*! \brief type definition for array of all stream predicates for all
 * subarrays */
using subarray_predicates_array_t = std::experimental::mdarray<
  L::Predicate,
  std::experimental::extents<
    unsigned,
    Configuration::max_num_subarrays,
    static_cast<unsigned>(SubarrayStreamPredicate::_last) + 1>>;

/*! \brief type definition for (non-owning) array of a single predicate for all
 * subarrays */
using subarray_predicates_t = std::mdspan<
  L::Predicate const,
  std::experimental::extents<
    subarray_predicates_array_t::size_type,
    Configuration::max_num_subarrays>,
  std::experimental::layout_stride>;

/*! \brief get an array over all subarrays of a single predicate from an array
 * over all subarrays of all predicates */
auto
predicate_slice_subarray_predicates(
  subarray_predicates_array_t const& subarray_predicates,
  SubarrayStreamPredicate const& predicate) -> subarray_predicates_t;

/*! \brief type alias for an array of Legion::Future values over all
 * subarrays */
using subarray_futures_t =
  std::array<L::Future, Configuration::max_num_subarrays>;

/*! \brief get an array over all subarrays of L::Future values derived from a
 * single predicate from an array over all subarrays of all predicates */
auto
predicate_slice_subarray_futures(
  L::Context ctx,
  L::Runtime* rt,
  subarray_predicates_array_t const& subarray_predicates,
  SubarrayStreamPredicate const& predicate) -> subarray_futures_t;

/*! \brief intermediary class for symcam stream event and state processing
 *
 * Provides access to an EventLog instance through the Legion task
 * interface. Accesses to the EventLog member by this class are protected by
 * locking the mutex member -- all other accesses to that EventLog, for example
 * following the receipt of external event notifications, must be similarly
 * protected to ensure thread safety.
 */
class EventProcessing {
public:
  using state_t = StateRegion;
  using elog_t = EventLogRegion;

private:
  std::shared_ptr<EventLog> m_elog;

  std::shared_ptr<std::mutex> m_elog_mtx;

  EventLogPtrsBlock m_elog_block;

  elog_t::index_space_t m_elog_is;

  elog_t::LogicalRegion m_elog_lr;

  elog_t::LogicalPartition m_elog_lp;

  L::ExternalResources m_ext_resources;

  BaselineRegion::index_space_t m_baseline_index_space;

  L::IndexSpaceT<1, int> m_subarrays_color_space;

  L::IndexTaskLauncher m_record_launcher;

  std::optional<
    std::tuple<StreamClock::external_time, RegionPair<state_t::LogicalRegion>>>
    m_most_recent_point{};

public:
  EventProcessing() = default;

  EventProcessing(
    L::Context ctx,
    L::Runtime* rt,
    BaselineRegion::index_space_t baseline_index_space,
    std::shared_ptr<EventLog> const& log,
    std::shared_ptr<std::mutex> const& mtx,
    std::size_t shard_point,
    std::size_t num_shards);

  EventProcessing(EventProcessing const&) = delete;

  EventProcessing(EventProcessing&&) noexcept = default;

  auto
  operator=(EventProcessing const&) -> EventProcessing& = delete;

  auto
  operator=(EventProcessing&&) noexcept -> EventProcessing& = default;

  virtual ~EventProcessing() = default;

  void
  destroy(L::Context ctx, L::Runtime* rt);

  static auto
  preregister_tasks(std::array<L::TaskID, 5> const& task_ids) -> void;

  static auto
  register_tasks(L::Runtime* rt, std::array<L::TaskID, 5> const& task_ids)
    -> void;

  /*! \brief record event script
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param script EventScript instance
   */
  auto
  record(L::Context ctx, L::Runtime* rt, EventScript const& events) -> void {

    // serialize event script to JSON, then launch task
    launch_record(ctx, rt, to_string(events.as_json()));
  }

  /*! \brief record events
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param events collection of events
   */
  template <typename Events>
    requires std::ranges::range<Events>
               && std::convertible_to<
                 std::ranges::range_value_t<Events>,
                 std::shared_ptr<StreamStateEventBase<State> const>>
  auto
  record(L::Context ctx, L::Runtime* rt, Events const& events) -> void {

    // serialize events (vector) to JSON, then launch task
    std::vector<nlohmann::json> events_js;
    for (auto&& eventp : events)
      events_js.push_back(as_json(*eventp));
    launch_record(ctx, rt, to_string(nlohmann::json{events_js}));
  }

  /*! \brief roll up the event log to timestamp values and update states at
   * those points
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param points vector of (timestamp, state region) pairs
   * \param trigger optional future that acts as a trigger for when the task
   *        should run
   * \param pred task predicate
   *
   * Apply all the queued events prior to each of the (sorted) timestamps to the
   * event log's initial state in time order, and update the event log's
   * "initial" state.
   */
  auto
  rollup(
    L::Context ctx,
    L::Runtime* rt,
    std::vector<std::tuple<
      StreamClock::external_time,
      RegionPair<state_t::LogicalRegion>>> points,
    std::optional<L::Future> const& trigger = std::nullopt,
    L::Predicate pred = L::Predicate::TRUE_PRED) -> void;

  /*! \brief get current state of event log */
  auto
  current_state(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& state) -> void;

  /*! \brief create predicate for State:is_alive() from one element of a
   *  State(s) region
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param states LogicalRegions to test (LEGION_READ_ONLY permission)
   *
   * \return predicate derived from disjunction of State::is_alive() applied to
   * states
   */
  static auto
  is_alive(
    L::Context ctx,
    L::Runtime* rt,
    std::vector<RegionPair<state_t::LogicalRegion>> const& states)
    -> L::Predicate {
    return query_state(ctx, rt, states, AlivePredicate{});
  }

  /*! \brief create predicate for change of subarrays
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param state_lr LogicalRegion for returned State value
   *        (LEGION_READ_ONLY permission)
   * \param offset index of State to test in state_lr
   *
   * \return predicate derived from State::is_alive() applied to the state value
   *         in state_lr at index offset
   */
  static auto
  is_subarray_change(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& prev,
    RegionPair<state_t::LogicalRegion> const& current) -> L::Predicate {
    if (prev.region() == state_t::LogicalRegion{})
      return L::Predicate::TRUE_PRED;
    return query_state(ctx, rt, {prev, current}, SubarrayChangePredicate{});
  }

  /*! \brief create predicate for non-empty State:fftw_wisdom_import_path()
   *  value from a State region
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param state LogicalRegion to test (LEGION_READ_ONLY permission)
   *
   * \return predicate for State::fftw_wisdom_import_path()[0] != '\0' applied
   * to state
   */
  static auto
  do_fftw_import(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& state) -> L::Predicate {
    return query_state(ctx, rt, {state}, FFTWImportPredicate{});
  }

  /*! \brief create predicate for non-empty State:fftw_wisdom_export_path()
   *  value from a State region
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param state LogicalRegion to test (LEGION_READ_ONLY permission)
   *
   * \return predicate for State::fftw_wisdom_export_path()[0] != '\0' applied
   * to state
   */
  static auto
  do_fftw_export(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& state) -> L::Predicate {
    return query_state(ctx, rt, {state}, FFTWExportPredicate{});
  }

  /*! \brief create all subarray predicates for an interval
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param stream states for interval of interest as well as those for the
   *        intervals immediately preceding and succeeding that interval (in
   *        time order)
   */
  static auto
  subarray_predicates(
    L::Context ctx,
    L::Runtime* rt,
    std::array<RegionPair<state_t::LogicalRegion>, 3> const& states)
    -> subarray_predicates_array_t;

private:
  auto
  launch_record(L::Context ctx, L::Runtime* rt, std::string const& serialized)
    -> void;

  /*! \brief create Legion predicate from a State region StatePredicate
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param state_lr LogicalRegion for returned State value
   *        (LEGION_READ_ONLY permission)
   * \param pred, StatePredicate value
   *
   * \return Legion predicate derived from pred applied to state value in
   *         state_lr
   */
  static auto
  query_state(
    L::Context ctx,
    L::Runtime* rt,
    std::vector<RegionPair<state_t::LogicalRegion>> const& states,
    StatePredicate const& pred) -> L::Predicate;
};

/*! \brief select the state subregion at index 0
 *
 * Under control replication, when running with multiple shards, each shard
 * processes its own event log, but in order to avoid inconsistencies in event
 * processing among the shards, only the event log at shard 0 should ever be
 * used by any shard to query the global State. Instances of this class are
 * transformation functors which select the subregion of States regions
 * corresponding to (shard) index 0.
 *
 * Instances of this class are should never escape the context in which they
 * were constructed (because they capture a Legion Context and Runtime).
 */
class State0Select {
  using state_t = StateRegion;

  /*! \brief Legion runtime pointer */
  L::Runtime* m_rt;

  /*! \brief complete index space partition */
  state_t::index_partition_t m_ip;

  /*! \brief State region field space */
  L::FieldSpace m_fs;

public:
  /*! \brief constructor from a model region
   *
   * \param model State region that has the index and field space of all regions
   * to be subsequently transformed
   */
  State0Select(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& model);

  /*! \brief default constructor (deleted) */
  State0Select() = delete;

  /*! \brief copy constructor (defaulted) */
  State0Select(State0Select const&) = default;

  /*! \brief move constructor (defaulted) */
  State0Select(State0Select&&) noexcept = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(State0Select const&) -> State0Select& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(State0Select&&) noexcept -> State0Select& = default;

  /* \brief destructor (defaulted) */
  ~State0Select() = default;

  auto
  operator()(RegionPair<state_t::LogicalRegion> const& state) const
    -> RegionPair<state_t::LogicalRegion>;
};

} // end namespace rcp::symcam::st

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
