// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

/*! \file StreamEventLog.hpp
 *
 * Events with reference to data stream timestamps.
 */

#include "libsymcam.hpp"

#include <compare>
#include <memory>
#include <numeric>
#include <queue>
#include <ranges>
#include <variant>
#include <vector>

namespace rcp::symcam {

/*! \brief stream event base class for given state type
 *
 * \tparam State stream state type
 */
template <typename State>
class StreamStateEventBase {

  /*! \brief event stream timestamp at which the event occurs */
  StreamClock::timestamp_t m_timestamp{};

protected:
  /*! \brief event timestamp reference */
  auto
  timestamp() noexcept -> StreamClock::timestamp_t& {
    return m_timestamp;
  }

public:
  using state_t = State;

  /*! \brief default constructor (defaulted) */
  StreamStateEventBase() noexcept = default;
  /*! \brief constructor from timestamp value */
  StreamStateEventBase(StreamClock::timestamp_t const& ts) noexcept
    : m_timestamp(ts) {}
  /*! \brief copy constructor (defaulted) */
  StreamStateEventBase(StreamStateEventBase const&) noexcept = default;
  /*! \brief move constructor (defaulted) */
  StreamStateEventBase(StreamStateEventBase&&) noexcept = default;
  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(StreamStateEventBase const&) noexcept
    -> StreamStateEventBase& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(StreamStateEventBase&&) noexcept -> StreamStateEventBase& = default;
  /*! \brief destructor (defaulted) */
  virtual ~StreamStateEventBase() noexcept = default;

  /*! \brief class tag */
  [[nodiscard]] virtual auto
  tag() const noexcept -> char const* = 0;

  /*! \brief event timestamp */
  [[nodiscard]] auto
  timestamp() const noexcept -> StreamClock::timestamp_t const& {
    return m_timestamp;
  }

  /*! \brief apply event to a state
   *
   * \param st initial State
   * \return final State
   * \sa apply_to(State&&) const
   */
  [[nodiscard]] auto
  apply_to(State const& st) const -> State {
    return apply_to(State{st});
  }

  /*! \brief apply event to a state
   *
   * \sa apply_to(State const&) const
   */
  [[nodiscard]] virtual auto
  apply_to(State&& st) const -> State = 0;

  [[nodiscard]] auto
  set_timestamp(State&& st) const -> State {
    return std::move(st).set_timestamp(timestamp());
  }

  [[nodiscard]] auto
  set_timestamp(const State& st) const -> State {
    return st.set_timestamp(timestamp());
  }

  /*! \brief fix event timestamp to epoch
   *
   * Only changes a relative timestamp to an external timestamp; if instance has
   * an external timestamp, this method does nothing.
   *
   * \param epoch timestamp epoch
   *
   * \sa with_fixed_epoch(StreamClock::external_time const&) &&
   */
  [[nodiscard]] virtual auto
  with_fixed_epoch(StreamClock::external_time const& epoch)
    const& -> std::unique_ptr<StreamStateEventBase<State>> = 0;

  /*! \brief fix event timestamp to epoch
   *
   * \sa with_fixed_epoch(StreamClock::external_time const&) const&
   */
  [[nodiscard]] virtual auto
  with_fixed_epoch(
    StreamClock::external_time const&
      epoch) && -> std::unique_ptr<StreamStateEventBase<State>> = 0;

  /*! \brief change event timestamp by an offset value
   *
   * \param ts offset value
   *
   * \sa with_timestamp_offset(StreamClock::duration const&) &&
   */
  [[nodiscard]] virtual auto
  with_timestamp_offset(StreamClock::duration const& duration)
    const& -> std::unique_ptr<StreamStateEventBase<State>> = 0;

  /*! \brief change event timestamp by an offset value
   *
   * \sa with_timestamp_offset(StreamClock::duration const&) const&
   */
  [[nodiscard]] virtual auto
  with_timestamp_offset(
    StreamClock::duration const&
      duration) && -> std::unique_ptr<StreamStateEventBase<State>> = 0;

  /*! \brief change event timestamp
   *
   * \param ts timestamp value
   *
   * \sa with_timestamp(StreamClock::external_time const&) &&
   */
  [[nodiscard]] virtual auto
  with_timestamp(StreamClock::external_time const& ts)
    const& -> std::unique_ptr<StreamStateEventBase<State>> = 0;

  /*! \brief change event timestamp
   *
   * \sa with_timestamp(StreamClock::external_time const&) const&
   */
  [[nodiscard]] virtual auto
  with_timestamp(StreamClock::external_time const&
                   ts) && -> std::unique_ptr<StreamStateEventBase<State>> = 0;
};

/*! \brief stream event base class
 *
 * \tparam State stream state type
 * \tparam Timestamp event timestamp type, either StreamClock::external_time or
 * StreamClock::local_time
 * \tparam Event derived class type
 *
 * CRTP
 */
template <typename State, typename Event>
class StreamStateEvent : public StreamStateEventBase<State> {
public:
  /*! \brief default constructor (defaulted) */
  StreamStateEvent() noexcept = default;
  /*! \brief constructor from timestamp value */
  StreamStateEvent(StreamClock::timestamp_t const& ts) noexcept
    : StreamStateEventBase<State>(ts) {}
  /*! \brief copy constructor (defaulted) */
  StreamStateEvent(StreamStateEvent const&) noexcept = default;
  /*! \brief move constructor (defaulted) */
  StreamStateEvent(StreamStateEvent&&) noexcept = default;
  /*! \brief copy constructor with updated timestamp */
  StreamStateEvent(
    StreamStateEvent const& /*event*/, StreamClock::timestamp_t const& ts)
    : StreamStateEventBase<State>(ts) {}
  /*! \brief move constructor with updated timestamp */
  StreamStateEvent(
    StreamStateEvent&& /*event*/, StreamClock::timestamp_t const& ts)
    : StreamStateEventBase<State>(ts) {}
  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(StreamStateEvent const&) noexcept -> StreamStateEvent& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(StreamStateEvent&&) noexcept -> StreamStateEvent& = default;
  /*! \brief destructor (defaulted) */
  virtual ~StreamStateEvent() noexcept = default;

  auto
  operator<=>(StreamStateEvent const& rhs) const -> std::partial_ordering {
    return this->timestamp() <=> rhs.timestamp();
  }

  auto
  operator==(StreamStateEvent const& rhs) const -> bool {
    return std::is_eq(this->timestamp() <=> rhs.timestamp());
  }
  // }

protected:
  /*! \brief Event class helper for with_fixed_epoch
   */
  [[nodiscard]] auto
  fix_epoch(StreamClock::external_time const& epoch) const& -> Event {

    return Event(static_cast<Event const&>(*this)).fix_epoch(epoch);
  }

  /*! \brief Event class helper for with_fixed_epoch
   */
  [[nodiscard]] auto
  fix_epoch(StreamClock::external_time const& epoch) && -> Event {

    auto const ts = this->timestamp();
    if (std::holds_alternative<StreamClock::relative_time>(ts))
      return Event(
        static_cast<Event&&>(std::move(*this)),
        epoch + std::get<StreamClock::relative_time>(ts));
    return static_cast<Event&&>(std::move(*this));
  }

  /*! \brief Event class helper for with_timestamp_offset
   */
  [[nodiscard]] auto
  fix_timestamp_offset(StreamClock::duration const& duration) const& -> Event {

    return Event(static_cast<Event const&>(*this))
      .fix_timestamp_offset(duration);
  }

  /*! \brief Event class helper for with_timestamp_offset
   */
  [[nodiscard]] auto
  fix_timestamp_offset(StreamClock::duration const& duration) && -> Event {

    return Event(
      static_cast<Event&&>(std::move(*this)), this->timestamp() + duration);
  }

  /*! \brief Event class helper for with_timestamp
   */
  [[nodiscard]] auto
  fix_timestamp(StreamClock::external_time const& ts) const& -> Event {

    return Event(static_cast<Event const&>(*this)).fix_timestamp(ts);
  }

  /*! \brief Event class helper for with_timestamp
   */
  [[nodiscard]] auto
  fix_timestamp(StreamClock::external_time const& ts) && -> Event {

    return Event(static_cast<Event&&>(std::move(*this)), ts);
  }

public:
  [[nodiscard]] auto
  with_fixed_epoch(StreamClock::external_time const& epoch)
    const& -> std::unique_ptr<StreamStateEventBase<State>> override {

    return std::make_unique<Event>(fix_epoch(epoch));
  }

  [[nodiscard]] auto
  with_fixed_epoch(
    StreamClock::external_time const&
      epoch) && -> std::unique_ptr<StreamStateEventBase<State>> override {
    return std::make_unique<Event>(std::move(*this).fix_epoch(epoch));
  }

  [[nodiscard]] auto
  with_timestamp_offset(StreamClock::duration const& duration)
    const& -> std::unique_ptr<StreamStateEventBase<State>> override {
    return std::make_unique<Event>(fix_timestamp_offset(duration));
  }

  [[nodiscard]] auto
  with_timestamp_offset(
    StreamClock::duration const&
      duration) && -> std::unique_ptr<StreamStateEventBase<State>> override {
    return std::make_unique<Event>(
      std::move(*this).fix_timestamp_offset(duration));
  }

  [[nodiscard]] auto
  with_timestamp(StreamClock::external_time const& ts)
    const& -> std::unique_ptr<StreamStateEventBase<State>> override {
    return std::make_unique<Event>(fix_timestamp(ts));
  }

  [[nodiscard]] auto
  with_timestamp(
    StreamClock::external_time const&
      ts) && -> std::unique_ptr<StreamStateEventBase<State>> override {
    return std::make_unique<Event>(std::move(*this).fix_timestamp(ts));
  }
};

template <typename State>
class StreamEventLog;

/*! \brief stream state base class
 *
 * \tparam State derived class type
 *
 * CRTP
 */
template <typename State>
class StreamStateBase {

  StreamClock::external_time m_timestamp{StreamClock::external_time::min()};

protected:
  friend class StreamEventLog<State>;
  friend class StreamStateEventBase<State>;

  [[nodiscard]] auto
  set_timestamp(StreamClock::timestamp_t const& ts) const& -> State {
    return State(static_cast<State const&>(*this)).set_timestamp(ts);
  }

  [[nodiscard]] auto
  set_timestamp(StreamClock::timestamp_t const& ts) && -> State {
    std::visit(
      overloaded{
        [this](StreamClock::external_time const& et) {
          assert(et >= m_timestamp);
          m_timestamp = et;
        },
        [this](StreamClock::relative_time const& rt) {
          assert(rt >= StreamClock::relative_time{0});
          m_timestamp += rt;
        }},
      ts);
    return std::move(static_cast<State&>(*this));
  }

public:
  /*! \brief whether state has callbacks pre and post event application */
  static auto constexpr has_apply_event_callbacks = false;

  StreamStateBase() = default;

  StreamStateBase(StreamClock::external_time const& ts) : m_timestamp(ts) {}

  /*! \brief timestamp (const version) */
  [[nodiscard]] auto
  timestamp() const noexcept -> StreamClock::external_time const& {
    return m_timestamp;
  }

  /*! \brief apply an event to this state to create a new state
   *
   * \param evt event to apply
   *
   * \return StreamState after application of the event to this value
   *
   * \sa apply(StreamStateEventBase<State> const& event) &&
   */
  [[nodiscard]] auto
  apply(StreamStateEventBase<State> const& evt) const& -> State {
    return evt.apply_to(static_cast<State const&>(*this));
  }

  /*! \brief apply an event to this state to create a new state
   *
   * \sa apply(StreamStateEventBase<State> const&) const&
   */
  [[nodiscard]] auto
  apply(StreamStateEventBase<State> const& evt) && -> State {
    return evt.apply_to(static_cast<State&&>(std::move(*this)));
  }
};

template <typename State>
using StreamEventQueue =
  std::vector<std::shared_ptr<StreamStateEventBase<State> const>>;

/*! \brief event log class
 *
 * \tparam State stream state class
 */
template <typename State>
class StreamEventLog {

  /*! \brief flag for primary event log */
  bool m_primary{true};

  /*! \brief initial state of the log */
  State m_state{};

  /*! \brief ordered queue of unapplied events in the log */
  StreamEventQueue<State> m_event_queue{};

public:
  /*! \brief state type */
  using state_t = State;

  /*! \brief default constructor (defaulted) */
  StreamEventLog() = default;
  /*! \brief constructor initial state and primary log flag */
  StreamEventLog(State initial_state, bool primary)
    : m_primary{primary}, m_state(std::move(initial_state)) {}
  /*! \brief copy constructor (defaulted) */
  StreamEventLog(StreamEventLog const&) = default;
  /*! \brief move constructor (defaulted) */
  StreamEventLog(StreamEventLog&&) noexcept = default;
  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(StreamEventLog const&) -> StreamEventLog& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(StreamEventLog&&) noexcept -> StreamEventLog& = default;
  /*! \brief destructor (defaulted) */
  virtual ~StreamEventLog() = default;

protected:
  /*! \brief add event to sorted event queue
   *
   * Events with equal timestamps are sorted according to the order that they
   * are added, with later additions following earlier ones.
   */
  auto
  enqueue_event(std::shared_ptr<StreamStateEventBase<State> const> const& event)
    -> void {
    if (m_primary) {
      auto event_ts = event->timestamp();
      auto greater = std::ranges::find_if(
        m_event_queue, [&](auto&& evt) { return evt->timestamp() > event_ts; });
      m_event_queue.insert(greater, event);
    }
  }

  /*! \brief remove first element of event queue */
  auto
  dequeue_front_event() -> void {
    m_event_queue.erase(m_event_queue.begin());
  }

public:
  /*! \brief query for whether this is the primary event log*/
  [[nodiscard]] auto
  is_primary() const -> bool {
    return m_primary;
  }

  /*! \brief record an event
   *
   * \param event event, to be added to the log
   *
   * \return StreamEventLog instance with the current initial state and event
   *         added to the queue of unapplied events
   *
   * Note that an event with a timestamp not strictly after the value of
   * timestamp() is ignored, but for a log message.
   *
   * This version does not modify the target.
   *
   * \sa record(std::shared_ptr<StreamStateEventBase<State>> const&,
   *            StreamClock::duration const&) && -> StreamEventLog
   */
  [[nodiscard]] auto
  record(
    std::shared_ptr<StreamStateEventBase<State>> const& event,
    StreamClock::duration const& offset = StreamClock::duration{
      0}) const& -> StreamEventLog {
    return StreamEventLog(*this).record(event, offset);
  }

  /*! \brief record an event immediately
   *
   * \param event event, to be applied to the log
   *
   * \return StreamEventLog instance with the current initial state and event
   *         added to the queue of unapplied events with earliest possible
   *         timestamp
   *
   * This version does not modify the target.
   *
   * \sa record_immediate(std::shared_ptr<StreamStateEventBase<State>> const&)
   *     && -> StreamEventLog
   */
  [[nodiscard]] auto
  record_immediate(std::shared_ptr<StreamStateEventBase<State>> const& event)
    const& -> StreamEventLog {
    return StreamEventLog(*this).record_immediate(event);
  }

  /*! \brief record an event
   *
   * \param event event, to be moved to the log
   *
   * \return StreamEventLog instance with the current initial state and event
   *         added to the queue of unapplied events
   *
   * Note that an event with a timestamp not strictly after the value of
   * timestamp() is ignored, but for a log message.
   *
   * All other class methods that record events end up calling this method.
   *
   * This version modifies the target.
   */
  [[nodiscard]] auto
  record(
    std::shared_ptr<StreamStateEventBase<State>> const& event,
    StreamClock::duration const& offset = StreamClock::duration{
      0}) && -> StreamEventLog {

    // fix the event timestamp to a StreamClock::external_time value, and create
    // a shared_ptr for the event
    std::shared_ptr<StreamStateEventBase<State> const> eventp =
      event->with_timestamp_offset(offset)->with_fixed_epoch(next_timestamp());
    assert(
      std::holds_alternative<StreamClock::external_time>(eventp->timestamp()));

    // put the event (shared_ptr) onto the queue, if it's not in the past
    auto event_ts = eventp->timestamp();
    if (event_ts > timestamp()) {
      enqueue_event(eventp);
    } else if (m_primary) {
      log().info("Event received by 'StreamEventLog::record()' with timestamp "
                 "in the past");
    }
    return std::move(*this);
  }

  /*! \brief record an event immediately
   *
   * \param event event, to be applied to the log
   *
   * \return StreamEventLog instance with the current initial state and event
   *         added to the queue of unapplied events with earliest possible
   *         timestamp
   *
   * This version modifies the target.
   */
  [[nodiscard]] auto
  record_immediate(std::shared_ptr<StreamStateEventBase<State>> const&
                     event) && -> StreamEventLog {

    // fix the event timestamp to a StreamClock::external_time value, and create
    // a shared_ptr for the event
    std::shared_ptr<StreamStateEventBase<State> const> eventp =
      event->with_timestamp(next_timestamp());
    enqueue_event(eventp);
    return std::move(*this);
  }

  /*! \brief record events
   *
   * \param events events (in a range) to record
   *
   * This version does not modify the target.
   *
   * \sa record(Events&&, StreamClock::duration const&) && -> StreamEventLog
   */
  template <typename Events>
    requires std::ranges::range<Events>
               && std::convertible_to<
                 std::ranges::range_value_t<Events>,
                 std::shared_ptr<StreamStateEventBase<State> const>>
  [[nodiscard]] auto
  record(
    Events&& events,
    StreamClock::duration const& offset = StreamClock::duration{
      0}) const& -> StreamEventLog {

    return StreamEventLog(*this).record(std::forward<Events>(events), offset);
  }

  /*! \brief record immediate events
   *
   * \param events events (in a range) to record
   *
   * This version does not modify the target.
   *
   * \sa record_immediate(Events&&, StreamClock::duration const&) && ->
   * StreamEventLog
   */
  template <typename Events>
    requires std::ranges::range<Events>
               && std::convertible_to<
                 std::ranges::range_value_t<Events>,
                 std::shared_ptr<StreamStateEventBase<State> const>>
  [[nodiscard]] auto
  record_immediate(Events&& events) const& -> StreamEventLog {

    return StreamEventLog(*this).record_immediate(std::forward<Events>(events));
  }

  /*! \brief record events
   *
   * \param events events (in a range) to record
   *
   * This version modifies the target.
   *
   * \sa record(EventV&&, StreamClock::duration const&) const& -> StreamEventLog
   */
  template <typename Events>
    requires std::ranges::range<Events>
               && std::convertible_to<
                 std::ranges::range_value_t<Events>,
                 std::shared_ptr<StreamStateEventBase<State> const>>
  [[nodiscard]] auto
  record(
    Events&& events,
    StreamClock::duration const& offset = StreamClock::duration{
      0}) && -> StreamEventLog {

    return std::accumulate(
      std::ranges::begin(events),
      std::ranges::end(events),
      std::move(*this),
      [&](auto&& log, auto&& evt) {
        return std::forward<decltype(log)>(log).record(evt, offset);
      });
  }

  /*! \brief record immediate events
   *
   * \param events events (in a range) to record
   *
   * This version modifies the target.
   *
   * \sa record(EventV&&, StreamClock::duration const&) const& -> StreamEventLog
   */
  template <typename Events>
    requires std::ranges::range<Events>
               && std::convertible_to<
                 std::ranges::range_value_t<Events>,
                 std::shared_ptr<StreamStateEventBase<State> const>>
  [[nodiscard]] auto
  record_immediate(Events&& events) && -> StreamEventLog {

    return std::accumulate(
      std::ranges::begin(events),
      std::ranges::end(events),
      std::move(*this),
      [&](auto&& log, auto&& evt) {
        return std::forward<decltype(log)>(log).record_immediate(evt);
      });
  }

  /*! \brief state with events applied up to a timestamp value
   *
   * \param ts timestamp
   *
   * \return StreamState starting with the current initial state in the log
   *         after application of all queued events having timestamps not later
   *         than ts
   *
   * Note that if ts is less than the value of timestamp(), the value of
   * state() is returned (*i.e*, this function cannot return an accurate
   * state for times prior to the timestamp for the current initial state).
   */
  [[nodiscard]] auto
  state_at(StreamClock::external_time const& ts) const& -> State {
    return rollup(ts).m_state;
  }

  /*! \brief roll up the event log to some timestamp value
   *
   * \param ts timestamp
   *
   * \return StreamEventLog rolled up to the given timestamp
   *
   * Apply all the queued events prior to ts in time order to the current
   * initial state, and create a new log based on that state and all events from
   * this log that were not applied.
   *
   * \sa rollup(StreamEvent::StreamClock::time_point const&) &&
   */
  [[nodiscard]] auto
  rollup(StreamClock::external_time const& ts) const& -> StreamEventLog {
    return StreamEventLog(*this).rollup(ts);
  }

  /*! \brief roll up the event log to some timestamp value
   *
   * \sa rollup(StreamEvent::StreamClock::time_point const&) const&
   */
  [[nodiscard]] auto
  rollup(StreamClock::external_time const& ts) && -> StreamEventLog {
    assert(m_primary || m_event_queue.empty());
    bool updated_timestamp{ts > timestamp()};
    if constexpr (State::has_apply_event_callbacks) {
      while (!m_event_queue.empty()
             && m_event_queue.front()->timestamp() <= ts) {
        updated_timestamp = true;
        auto event = m_event_queue.front();
        m_state =
          std::move(m_state).pre_apply(*event).apply(*event).post_apply(*event);
        dequeue_front_event();
      }

    } else {
      while (!m_event_queue.empty()
             && m_event_queue.front()->timestamp() <= ts) {
        updated_timestamp = true;
        m_state = std::move(m_state).apply(*m_event_queue.front());
        dequeue_front_event();
      }
    }
    if (updated_timestamp)
      m_state = std::move(m_state).set_timestamp(ts);
    return std::move(*this);
  }

  /*! \brief roll up the entire event log
   *
   * \return StreamEventLog rolled up last event in the log
   *
   * Apply all the queued events to the current initial state, and create a new
   * log based on that state.
   *
   * \sa rollup_all() &&
   */
  [[nodiscard]] auto
  rollup_all() const& -> StreamEventLog {
    return StreamEventLog(*this).rollup_all();
  }

  /*! \brief roll up the entire event log
   *
   * \sa rollup_all() const&
   */
  [[nodiscard]] auto
  rollup_all() && -> StreamEventLog {
    assert(m_primary || m_event_queue.empty());
    auto end_ts = timestamp();
    if constexpr (State::has_apply_event_callbacks) {
      while (!m_event_queue.empty()) {
        auto event = m_event_queue.front();
        end_ts = std::get<StreamClock::external_time>(event->timestamp());
        m_state =
          std::move(m_state).pre_apply(*event).apply(*event).post_apply(*event);
        dequeue_front_event();
      }

    } else {
      while (!m_event_queue.empty()) {
        m_state = std::move(m_state).apply(*m_event_queue.front());
        end_ts =
          std::get<StreamClock::external_time>(m_event_queue.top->timestamp());
        dequeue_front_event();
      }
    }
    m_state = std::move(m_state).set_timestamp(end_ts);
    return std::move(*this);
  }

  /*! \brief roll up the event log to some timestamp value, returning a vector
   *         of errors
   *
   * \tparam Error base class of exceptions to catch and return
   *
   * \param ts timestamp
   *
   * \return StreamEventLog rolled up to the given timestamp, and a vector of
   *         error instances encountered in applying events to the state
   *
   * Apply all the queued events prior to ts in time order to the current
   * initial state, and create a new log based on that state and all events from
   * this log that were not applied.
   *
   * \sa saferollup(StreamEvent::StreamClock::time_point const&) &&
   */
  template <typename Error>
  [[nodiscard]] auto
  safe_rollup(StreamClock::external_time const& ts)
    const& -> std::tuple<StreamEventLog, std::vector<Error>> {
    return StreamEventLog<Error>(*this).safe_rollup(ts);
  }

  /*! \brief roll up the event log to some timestamp value
   *
   * \tparam Error base class of exceptions to catch and return
   *
   * \param ts timestamp
   *
   * \return StreamEventLog rolled up to the given timestamp, and a vector of
   *         error instances encountered in applying events to the state
   *
   * Apply all the queued events prior to ts in time order to the current
   * initial state, and create a new log based on that state and all events from
   * this log that were not applied.
   *
   * \sa safe_rollup(StreamEvent::StreamClock::time_point const&) const&
   */
  template <typename Error>
  [[nodiscard]] auto
  safe_rollup(StreamClock::external_time const&
                ts) && -> std::tuple<StreamEventLog, std::vector<Error>> {
    assert(m_primary || m_event_queue.empty());
    bool updated_timestamp{false};
    std::vector<Error> errors;
    if constexpr (State::has_apply_event_callbacks) {
      while (!m_event_queue.empty()
             && m_event_queue.front()->timestamp() <= ts) {
        updated_timestamp = true;
        auto event = m_event_queue.front();
        try {
          auto new_state =
            std::move(m_state).pre_apply(*event).apply(*event).post_apply(
              *event);
          m_state = std::move(new_state);
        } catch (Error const& err) {
          errors.push_back(err);
        }
        dequeue_front_event();
      }
    } else {
      while (!m_event_queue.empty()
             && m_event_queue.front()->timestamp() <= ts) {
        updated_timestamp = true;
        try {
          auto new_state = std::move(m_state).apply(*m_event_queue.front());
          m_state = std::move(new_state);
        } catch (Error const& err) {
          errors.push_back(err);
        }
        dequeue_front_event();
      }
    }
    if (updated_timestamp)
      m_state = std::move(m_state).set_timestamp(ts);
    return {std::move(*this), std::move(errors)};
  }

  /*! \brief timestamp for the initial state of this log */
  [[nodiscard]] auto
  timestamp() const noexcept -> StreamClock::external_time {
    return m_state.timestamp();
  }

  /*! \brief smallest timestamp strictly greater than timestamp for initial
   *  state of this log */
  [[nodiscard]] auto
  next_timestamp() const noexcept -> StreamClock::external_time {
    auto result = m_state.timestamp();
    return ++result;
  }

  /*! \brief initial state */
  [[nodiscard]] auto
  state() const noexcept -> State const& {
    return m_state;
  }

  /*! \brief initial state */
  [[nodiscard]] auto
  state() noexcept -> State& {
    return m_state;
  }
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
