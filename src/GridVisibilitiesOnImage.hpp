// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BaselineChannelRegion.hpp"
#include "BsetChannelBaselinePolprodRegion.hpp"
#include "CFRegion.hpp"
#include "Configuration.hpp"
#include "DataPartitionsRegion.hpp"
#include "EventProcessing.hpp"
#include "GridRegion.hpp"
#include "GridTileRegion.hpp"
#include "GridWeightsRegion.hpp"
#include "MuellerIndexRegion.hpp"
#include "Serializable.hpp"
#include "StateRegion.hpp"
#include "StreamState.hpp"
#include "libsymcam.hpp"

#include <optional>
#include <rcp/rcp.hpp>

namespace rcp::symcam {

/*! \brief generator for grid/image regions
 *
 * In addition to the generated regions, the function to compute a tiled
 * partition of grids is also available.
 */
class GridRegionGenerator {
public:
  using grid_t = GridRegion;
  using tiles_t = GridTileRegion;

private:
  /*! parameters attached to every generated grid */
  grid_t::Params m_grid_params;

  /*! \brief simple grid region generator */
  DenseArrayRegionGenerator<grid_t::static_region_t> m_gen;

public:
  struct Serialized {
    grid_t::Params m_grid_params;
    DenseArrayRegionGenerator<grid_t::static_region_t>::Serialized m_gen;
  };

  GridRegionGenerator(
    L::Context ctx, L::Runtime* rt, ImagingConfiguration const& img);

  GridRegionGenerator(
    L::Context ctx, L::Runtime* rt, Serialized const& serialized);

  [[nodiscard]] auto
  serialized(L::Context ctx, L::Runtime* rt) const -> Serialized;

  [[nodiscard]] auto
  parent_requirements(L::Context ctx, L::Runtime* rt) const
    -> ChildRequirements;

  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void;

  /*! get a fresh region */
  auto
  make(L::Context ctx, L::Runtime* rt) -> grid_t::LogicalRegion;

  /*! \brief compute a tiled partition of a grid region
   *
   * \param ctx Legion context
   * \param rt Legion runtime
   * \param is grid index space
   * \param config symcam Configuration instance
   * \param imaging_config ImagingConfiguration instance
   *
   * Returns two values:
   *
   * - grid_t::index_partition_t: a partition of the grid with a 3-dimensional
   *   index comprising grid channel, w plane and some partition of the grid
   *   planes in the UV directions (dependent upon grid channel and w plane)
   *
   * - tiles_t::LogicalRegion: a region of the grid tile bounds
   *
   * \todo The UV partition is fixed in this implementation to include a single
   * tile, centered on the UV plane, that is large enough to contain all the
   * baselines (as determined by Configuration::array_extent_m[]). It would be
   * nice to allow other, and/or more varied partitions, but it is best to defer
   * that until we have a better idea of what those other possibilities might
   * be, and how to represent them well.
   */
  static auto
  partition_by_tiles(
    L::Context ctx,
    L::Runtime* rt,
    grid_t::index_space_t grid_index_space,
    Configuration const& config,
    ImagingConfiguration const& imaging_config)
    -> std::tuple<grid_t::index_partition_t, tiles_t::LogicalRegion>;
};

/*! generator for weights indexed by grid channel, w-plane and Stokes index */
class WeightsRegionGenerator
  : public DenseArrayRegionGenerator<GridWeightsRegion> {

public:
  WeightsRegionGenerator(
    L::Context ctx, L::Runtime* rt, ImageConfiguration const& im);
};

enum class IndexAxes { index = LEGION_DIM_0, subarray };

/*! \brief field for unsigned integer counter */
using CountField = StaticField<unsigned, field_ids::count>;

/* \brief field for timestamp */
using TimestampField =
  StaticField<StreamClock::external_time, field_ids::timestamp>;

using GridAndWeightMetadataRegion = StaticRegionSpec<
  IndexSpec<IndexAxes, IndexAxes::index, IndexAxes::subarray, coord_t>,
  std::array{
    IndexInterval<coord_t>::bounded(0, 0),
    IndexInterval<coord_t>::bounded(0, Configuration::max_num_subarrays - 1)},
  CountField,
  TimestampField>;

/*! GridVisibilitiesOnImage
 */
class GridVisibilitiesOnImage {
public:
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using grid_t = GridRegion;
  using gw_t = GridWeightsRegion;
  using cf_t = CFRegion;
  using mi_t = MuellerIndexRegion;
  using tiles_t = GridTileRegion;
  using bc_t = BaselineChannelRegion;
  using state_t = st::StateRegion;
  using gmd_t = GridAndWeightMetadataRegion;
  using part_t = DataPartitionsRegion;

  struct SubarrayRegions {
    /*! \brief grid region */
    grid_t::LogicalRegion grid;

    /*! \brief grid weight region */
    gw_t::LogicalRegion grid_weight;

    /*! \brief tiled partition of grid */
    grid_t::index_partition_t grid_partition;

    /*! index partition of grid_weight by channel and w-plane */
    gw_t::index_partition_t grid_weight_partition;

    /*! \brief grid tile boundaries */
    tiles_t::LogicalRegion tile_bounds;
  };

private:
  /*! index space of visibilities */
  cbp_t::index_space_t m_visibilities_index_space;

  /*! index space of visibility UVWs */
  bc_t::index_space_t m_uvws_index_space;

  /*! \brief ImagingConfiguration for this instance */
  ImagingConfiguration m_imaging_configuration{};

  /*! grid generator */
  GridRegionGenerator m_grid_generator;

  /*! weights generator */
  WeightsRegionGenerator m_weights_generator;

  std::array<char, Configuration::name_array_size> m_name;

  int m_vector_length;

  /*! Stokes plane mask for gridding */
  StokesMask m_stokes_mask;

  /*! \brief region to track state of grid region buffer usage */
  gmd_t::LogicalRegion m_grid_metadata;

  /*! \brief partition of m_grid_metadata for all subarrays */
  gmd_t::LogicalPartition m_grid_metadata_partition;

  /*! \brief regions for each subarray */
  sarray<SubarrayRegions> m_subarray_regions;

  /*! \brief region of Mueller matrix indexes */
  mi_t::LogicalRegion m_mueller_indexes_region;

public:
  struct Serialized {
    cbp_t::index_space_t m_visibilities_index_space;
    bc_t::index_space_t m_uvws_index_space;
    std::array<char, Configuration::name_array_size> m_name;
    gmd_t::LogicalRegion m_grid_metadata;
    gmd_t::LogicalPartition m_grid_metadata_partition;
    sarray<SubarrayRegions> m_subarray_regions;
    mi_t::LogicalRegion m_mueller_indexes_region;
    std::array<char, ImagingConfiguration::max_serialized_size>
      m_imaging_configuration;
  };

  GridVisibilitiesOnImage(
    L::Context ctx,
    L::Runtime* rt,
    Configuration const& config,
    ImagingConfiguration const& img_config,
    mi_t::LogicalRegion const& mueller_indexes_region);

  GridVisibilitiesOnImage() = delete;

  GridVisibilitiesOnImage(GridVisibilitiesOnImage const&) = delete;

  GridVisibilitiesOnImage(GridVisibilitiesOnImage&&) = default;

  GridVisibilitiesOnImage(
    L::Context ctx, L::Runtime* rt, Serialized const& serialized) noexcept;

  auto
  operator=(GridVisibilitiesOnImage const&)
    -> GridVisibilitiesOnImage& = delete;

  auto
  operator=(GridVisibilitiesOnImage&&) -> GridVisibilitiesOnImage& = delete;

  ~GridVisibilitiesOnImage() = default;

  /*! \brief serialized form */
  [[nodiscard]] auto
  serialized(L::Context ctx, L::Runtime* rt) const -> Serialized;

  /*! \brief requirements for child tasks that use Serialized values as
   *  arguments */
  [[nodiscard]] auto
  child_requirements(L::Context ctx, L::Runtime* rt) const -> ChildRequirements;

  /*! submit visibilities and uvw values for gridding
   */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& state,
    PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    sarray<RegionPair<cbp_t::LogicalRegion>> const& visibility_weights,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    cf_t::LogicalRegion const& cf_arrays,
    st::subarray_predicates_array_t const& predicates) const -> void;

  /*! submit visibilities and uvw values for gridding
   */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& state,
    PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    sarray<RegionPair<cbp_t::LogicalRegion>> const& visibility_weights,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    cf_t::LogicalRegion const& cf_arrays,
    st::AllSubarraysQuery const& state_query) const -> void;

  /*! \brief set up the next grid and weights region */
  auto
  next_grid_and_weights(
    L::Context ctx,
    L::Runtime* rt,
    st::subarray_predicates_array_t const& predicates) const -> void;

  auto
  next_grid_and_weights(
    L::Context ctx,
    L::Runtime* rt,
    st::AllSubarraysQuery const& state_query) const -> void;

  [[nodiscard]] auto
  grid_and_weight_metadata() const -> gmd_t::LogicalPartition;

  [[nodiscard]] auto
  grids() const -> sarray<grid_t::LogicalRegion>;

  [[nodiscard]] auto
  grid_weights() const -> sarray<gw_t::LogicalRegion>;

  [[nodiscard]] auto
  tile_bounds() const -> sarray<tiles_t::LogicalRegion>;

  [[nodiscard]] auto
  grid_partitions() const -> sarray<grid_t::index_partition_t>;

  [[nodiscard]] auto
  name() const -> std::string;

  [[nodiscard]] auto
  imaging_configuration() const noexcept -> ImagingConfiguration;

  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void;

  static auto
  preregister_tasks(std::array<L::TaskID, 3> const& task_ids) -> void;

  static auto
  register_tasks(L::Runtime* rt, std::array<L::TaskID, 3> const& task_ids)
    -> void;

private:
  /*! \brief get sub-region of grid metadata for a subarray */
  [[nodiscard]] auto
  subarray_grid_metadata(L::Runtime* rt, int subarray) const
    -> gmd_t::LogicalRegion;

  [[nodiscard]] static auto
  deserialize_imaging_configuration(
    std::array<char, ImagingConfiguration::max_serialized_size> const& buffer)
    -> ImagingConfiguration;

  auto
  init_next_grid_and_weights_launcher(
    L::Runtime* rt, int subarray_slot, L::TaskLauncher& launcher) const -> void;

  auto
  init_imaging_launcher(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<state_t::LogicalRegion> const& state,
    RegionPair<cbp_t::LogicalRegion> const& visibilities,
    RegionPair<bc_t::LogicalRegion> const& uvws,
    cf_t::LogicalRegion const& cf_arrays,
    L::UntypedBuffer arg) const -> L::TaskLauncher;

  auto
  update_imaging_launcher(
    L::Runtime* rt,
    PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
    sarray<RegionPair<cbp_t::LogicalRegion>> const& visibility_weights,
    unsigned subarray_slot,
    L::TaskLauncher& launcher) const -> void;
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
