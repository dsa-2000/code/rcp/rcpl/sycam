// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "LoadCUDALibraries.hpp"
#include "Configuration.hpp"

#include <mappers/default_mapper.h>

using namespace rcp::symcam;

auto
CUDAContext::cuda_stream() -> cudaStream_t {

  // FIXME: error handling
  if (m_stream == nullptr)
    cudaStreamCreateWithFlags(&m_stream, cudaStreamNonBlocking);
  return m_stream;
}

auto
CUDAContext::correlator(unsigned num_timesteps, unsigned num_channels)
  -> tcc::Correlator {

  // N.B: we're assuming that all GPUs in the machine have the same architecture
  // (one reason: tcc selects CUDA device 0 when loading/JIT-compiling its code,
  // and there's no parameter that affects that selection)

  // FIXME: error handling
  auto key = std::make_tuple(num_timesteps, num_channels);
  if (!m_correlators.contains(key)) {
    auto corr = tcc::Correlator(
      CrossCorrelationConfiguration::num_bits,
      Configuration::num_receivers,
      num_channels,
      num_timesteps,
      Configuration::num_polarizations,
      CrossCorrelationConfiguration::tcc_receivers_per_block);
    m_correlators.emplace(key, corr);
  }
  return m_correlators.at(key);
}

static auto
get_cuda_context() -> CUDAContext& {

  static CUDAContext CUDA_CONTEXTS[LEGION_MAX_NUM_PROCS];
  Legion::Processor const proc = Legion::Processor::get_executing_processor();
  assert(proc.kind() == Legion::Processor::TOC_PROC);
  return CUDA_CONTEXTS[proc.id & (LEGION_MAX_NUM_PROCS - 1)];
}

auto
rcp::symcam::get_cuda_stream() -> cudaStream_t {
  return get_cuda_context().cuda_stream();
}

auto
rcp::symcam::get_correlator(unsigned num_timesteps, unsigned num_channels)
  -> tcc::Correlator {
  return get_cuda_context().correlator(num_timesteps, num_channels);
}

void
LoadCUDALibraries::launch(
  L::Context ctx,
  L::Runtime* rt,
  unsigned num_timesteps,
  unsigned num_channels) {

  auto tr = trace();

  auto tunable = Legion::Mapping::DefaultMapper::DefaultTunables ::
    DEFAULT_TUNABLE_GLOBAL_GPUS;
  auto num_gpus = rt->select_tunable_value(ctx, tunable).get<std::size_t>();
  if (num_gpus == 0)
    return;
  // Launch the initialization task onto each GPU.
  auto launch_space = rt->create_index_space(ctx, L::Rect<1>{0, num_gpus - 1});
  Args args{num_timesteps, num_channels};
  rt->execute_index_space(
      ctx,
      L::IndexTaskLauncher{
        task_id,
        launch_space,
        L::UntypedBuffer(&args, sizeof(args)),
        L::ArgumentMap{}})
    .wait_all_results(true /* silence_warnings */);
}

void
LoadCUDALibraries::task(
  L::Task const* task,
  std::vector<L::PhysicalRegion> const&,
  L::Context,
  L::Runtime*) {

  assert(task->arglen == sizeof(Args));
  Args const& args = *reinterpret_cast<Args const*>(task->args);

  get_cuda_stream();
  get_correlator(args.num_timesteps, args.num_channels);
}

void
LoadCUDALibraries::preregister(L::TaskID tid) {
  LoadCUDALibraries::task_id = task_id;
  L::TaskVariantRegistrar registrar(LoadCUDALibraries::task_id, task_name);
  registrar.add_constraint(L::ProcessorConstraint(L::Processor::TOC_PROC));
  registrar.set_leaf();
  L::Runtime::preregister_task_variant<task>(registrar, task_name);
}

Legion::TaskID LoadCUDALibraries::task_id;

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
