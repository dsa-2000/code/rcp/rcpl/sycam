// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"

namespace rcp::symcam {

/*! \brief launcher for task to produce data capture fill fraction reports
 */
class ReportFillFraction {
public:
  /*! \brief task arguments */
  struct Args {
    /*! \brief number of expected F-engine samples in every region */
    std::size_t capture_volume;
  };

private:
  /*! \brief period between reports */
  unsigned m_period;

  Args m_args;

  L::TaskLauncher m_launcher;

  /*! \brief record of fill counts */
  std::vector<L::Future> m_fill_counts;

public:
  ReportFillFraction(Configuration const& config, std::size_t capture_volume);

  ReportFillFraction() = default;

  ReportFillFraction(ReportFillFraction const&) = default;

  ReportFillFraction(ReportFillFraction&&) = default;

  auto
  operator=(ReportFillFraction const&) -> ReportFillFraction& = default;

  auto
  operator=(ReportFillFraction&&) -> ReportFillFraction& = default;

  /*! \brief record a fill count, launch the task every m_period calls */
  auto
  record(L::Context ctx, L::Runtime* rt, L::Future new_count, L::Predicate live)
    -> void;

  static auto
  preregister_tasks(L::TaskID task_id) -> void;

  static auto
  register_tasks(L::Runtime* rt, L::TaskID task_id) -> void;
};
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
