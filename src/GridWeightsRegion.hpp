// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include "StaticFields.hpp"
#include "libsymcam.hpp"

/*! \file GridWeightsRegion.hpp
 *
 * Definitions for GridWeightsRegion
 *
 * \fixme add bset axis to GridWeightsAxes
 */

namespace rcp::symcam {

/*! \brief axes for Stokes component, w-plane and channel */
enum class GridWeightsAxes {
  stokes = LEGION_DIM_0, /*!< Stokes component */
  w_plane,               /*!< W-plane */
  channel,               /*!< channel */
};

/*! \brief IndexSpec specialization for GridWeightAxes
 *
 * \tparam Coord index coordinate value type
 */
template <typename Coord>
using grid_weights_index_spec_t = IndexSpec<
  GridWeightsAxes,
  GridWeightsAxes::stokes,
  GridWeightsAxes::channel,
  Coord>;

/*! \brief default static bounds for regions with GridWeightsAxes
 *
 * \tparam Coord index coordinate value type
 *
 * Bounds are stokes: [0, Configuration::num_polarization_products - 1],
 * w_plane: [0, Coord max], channel: [0, Coord max].
 */
template <typename Coord>
constexpr auto default_grid_weights_bounds = std::array{
  IndexInterval<Coord>::bounded(
    0, Configuration::num_polarization_products - 1),
  IndexInterval<Coord>::left_bounded(0),
  IndexInterval<Coord>::left_bounded(0)};

/* \brief weight field definition */
using GridWeightField = StaticField<
  accval<Configuration::grid_value_precision_t>,
  field_ids::grid_weight>;

/*! \brief region definition with GridWeightsAxes */
using GridWeightsRegion = StaticRegionSpec<
  grid_weights_index_spec_t<coord_t>,
  default_grid_weights_bounds<coord_t>,
  GridWeightField>;

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
