// Copyright 2023-2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "SimpleEvent.hpp"

#include <string>
/*! \file StringValueEvent.hpp
 *
 * Base class for event with a timestamp and a string value
 */

namespace rcp::symcam::st {

struct JSONStringValueEvent : public JSONSimpleEvent {

  /*! \brief JSON string value event value member tag */
  static constexpr char const* value_tag = "value";

  /*! \brief test for value member */
  [[nodiscard]] static auto
  has_value(nlohmann::json const& js) noexcept -> bool {
    return js.contains(value_tag);
  }

  /*! \brief get string value */
  [[nodiscard]] static auto
  get_value(nlohmann::json const& js) -> nlohmann::json const& {
    return js.at(value_tag);
  }

  /*! \brief get string value */
  [[nodiscard]] static auto
  get_value(nlohmann::json& js) -> nlohmann::json& {
    return js.at(value_tag);
  }

  /*! \brief test for valid format */
  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool {
    return JSONSimpleEvent::is_valid(js) && has_value(js)
           && get_value(js).is_string();
  }
};

template <typename Event>
class StringValueEvent;

// forward declaration of from_json() for StringValueEvent<Event> types
template <typename Event>
auto
from_json(nlohmann::json const& js, StringValueEvent<Event>& evt) -> void;

/*! \brief stream event with subarray payload */
template <typename Event>
class StringValueEvent : public SimpleEvent<Event> {

  std::string m_value{};

  friend auto
  from_json<>(nlohmann::json const& js, StringValueEvent<Event>& evt) -> void;

public:
  /*! \brief default constructor (defaulted) */
  StringValueEvent() noexcept = default;
  /*! \brief constructor from subarray (name) and timestamp */
  StringValueEvent(
    std::string_view value, StreamClock::timestamp_t const& ts) noexcept
    : SimpleEvent<Event>(ts), m_value(value) {}
  /*! \brief copy constructor (defaulted) */
  StringValueEvent(StringValueEvent const&) = default;
  /*! \brief move constructor (defaulted) */
  StringValueEvent(StringValueEvent&&) noexcept = default;
  /*! \brief copy constructor with updated timestamp */
  StringValueEvent(
    StringValueEvent const& event, StreamClock::timestamp_t const& ts)
    : SimpleEvent<Event>(event, ts), m_value(event.m_value) {}
  /*! \brief move constructor with updated timestamp */
  StringValueEvent(StringValueEvent&& event, StreamClock::timestamp_t const& ts)
    : SimpleEvent<Event>(std::move(event), ts)
    , m_value(std::move(event).m_value) {}

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(StringValueEvent const&) -> StringValueEvent& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(StringValueEvent&&) noexcept -> StringValueEvent& = default;
  /*! \brief destructor (defaulted) */
  ~StringValueEvent() override = default;

  [[nodiscard]] auto
  value() const -> std::string const& {
    return m_value;
  }

protected:
  [[nodiscard]] auto
  value() -> std::string& {
    return m_value;
  }
};

/*! \brief convert StringValueEvent instance to JSON
 *
 * Form defined by nlohmann::json
 */
template <typename Event>
auto
to_json(nlohmann::json& js, StringValueEvent<Event> const& evt) -> void {
  js = {
    {JSONValue::meta_tag_name, Event::class_tag},
    {JSONValue::meta_value_name,
     {{JSONStringValueEvent::timestamp_tag, to_string(evt.timestamp())},
      {JSONStringValueEvent::value_tag, evt.value()}}}};
}

/*! \brief convert JSON value to StringValueEvent instance
 *
 * Form defined by nlohmann::json
 */
template <typename Event>
auto
from_json(nlohmann::json const& js, StringValueEvent<Event>& evt) -> void {
  if (!JSONValue::is_valid(js))
    throw JSONValueFormatError(js);

  auto tag = JSONValue::get_tag(js);
  if (tag != std::string_view(Event::class_tag))
    throw BadEventTagError(Event::class_tag, tag.c_str());

  auto jsval = JSONValue::get_value(js);
  if (!JSONStringValueEvent::is_valid(jsval))
    throw JSONValueFormatError(js);

  auto const timestamp_str =
    JSONStringValueEvent::get_timestamp(jsval).get<std::string>();
  auto const maybe_timestamp =
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    parse_timestamp(reinterpret_cast<char8_t const*>(timestamp_str.c_str()));
  if (!maybe_timestamp)
    throw TimestampParseError(timestamp_str.c_str());
  evt.timestamp() = *maybe_timestamp;

  auto const value_str =
    JSONStringValueEvent::get_value(jsval).get<std::string>();
  evt.value() = value_str;
}

/*! \brief import fftw wisdom event */
struct ImportFFTWWisdom : public StringValueEvent<ImportFFTWWisdom> {

  static constexpr char const* class_tag = "ImportFFTWWisdom";

  /*! \brief inherited constructors */
  using StringValueEvent<ImportFFTWWisdom>::StringValueEvent;

  /*! \brief set state's fftw_wisdom_import_path
   *
   * \param st state instance
   * \return copy of st, but with m_is_alive set to false
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState {
    return std::move(st).set_fftw_wisdom_import_path(value());
  }
};

/*! \brief export fftw wisdom event */
struct ExportFFTWWisdom : public StringValueEvent<ExportFFTWWisdom> {

  static constexpr char const* class_tag = "ExportFFTWWisdom";

  /*! \brief inherited constructors */
  using StringValueEvent<ExportFFTWWisdom>::StringValueEvent;

  /*! \brief set state's fftw_wisdom_import_path
   *
   * \param st state instance
   * \return copy of st, but with m_is_alive set to false
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState {
    return std::move(st).set_fftw_wisdom_export_path(value());
  }
};

} // end namespace rcp::symcam::st

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
