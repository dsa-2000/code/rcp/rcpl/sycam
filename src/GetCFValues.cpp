// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "GetCFValues.hpp"
#include "CFRegion.hpp"
#include "MuellerIndexRegion.hpp"

#include <mappers/default_mapper.h>
#include <rcp/rcp.hpp>
#ifdef RCP_USE_KOKKOS
#include <Kokkos_Random.hpp>
#endif // RCP_USE_KOKKOS

using namespace rcp::symcam;

namespace rcp::symcam {

/*! \brief task to initialize CF values */
struct CFInitTask : public DefaultKokkosTaskMixin<CFInitTask, void> {

  /*! \brief task name */
  static constexpr char const* task_name = "CFInit";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief region indexes */
  enum { values_region_index = 0, mueller_region_index, num_regions };

  using cf_t = CFRegion;
  using mi_t = MuellerIndexRegion;

#ifdef RCP_USE_KOKKOS
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(bool));
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    bool full_stokes = *reinterpret_cast<bool const*>(task->args);

    // initialize the mueller index regions
    auto const& mueller_region = regions[mueller_region_index];
    auto const mueller_indexes =
      mi_t::values<LEGION_WRITE_ONLY>(mueller_region, ForwardIndexField{});
    auto const inverse_mueller_indexes =
      mi_t::values<LEGION_WRITE_ONLY>(mueller_region, InverseIndexField{});

    using mpt_t = L::Point<2, coord_t>;
    for (L::PointInRectIterator<2, coord_t> pir(mueller_region); pir(); pir++) {
      mueller_indexes[*pir] = -1;
      inverse_mueller_indexes[*pir] = -1;
    }
    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,
    // readability-magic-numbers)
    if (full_stokes) {
      // full Stokes sparse Mueller matrix
      mueller_indexes[mpt_t{0, 0}] = 0;
      mueller_indexes[mpt_t{0, 3}] = 1;
      mueller_indexes[mpt_t{1, 1}] = 2;
      mueller_indexes[mpt_t{1, 2}] = 3;
      mueller_indexes[mpt_t{2, 1}] = 4;
      mueller_indexes[mpt_t{2, 2}] = 5;
      mueller_indexes[mpt_t{3, 0}] = 6;
      mueller_indexes[mpt_t{3, 3}] = 7;

      inverse_mueller_indexes[mpt_t{0, 0}] = mueller_indexes[mpt_t{0, 0}];
      inverse_mueller_indexes[mpt_t{0, 3}] = 8;
      inverse_mueller_indexes[mpt_t{1, 1}] = mueller_indexes[mpt_t{1, 1}];
      inverse_mueller_indexes[mpt_t{1, 2}] = 9;
      inverse_mueller_indexes[mpt_t{2, 1}] = 10;
      inverse_mueller_indexes[mpt_t{2, 2}] = mueller_indexes[mpt_t{2, 2}];
      inverse_mueller_indexes[mpt_t{3, 0}] = 11;
      inverse_mueller_indexes[mpt_t{3, 3}] = mueller_indexes[mpt_t{3, 3}];
    } else {
      // Stokes I Mueller matrix
      mueller_indexes[mpt_t{0, 0}] = 0;
      mueller_indexes[mpt_t{0, 3}] = 1;
      inverse_mueller_indexes[mpt_t{0, 0}] = mueller_indexes[mpt_t{0, 0}];
      inverse_mueller_indexes[mpt_t{0, 3}] = 2;
    }
    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,
    // readability-magic-numbers)

    // initialize the CF values
    auto const& values_region = regions[values_region_index];
    L::Rect<cf_t::dim, coord_t> bounds = values_region;
    auto const values = cf_t::view<execution_space, LEGION_WRITE_ONLY>(
      values_region, CFValueField{});

    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);
    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();

    log().debug(std::string("init cf ") + rcp::show(bounds));
    K::Random_XorShift64_Pool<execution_space> random_pool(2000);
    K::parallel_for(
      task_name,
      mdrange_policy<
        execution_space,
        cf_t,
        cf_t::axes_t::x_major,
        cf_t::axes_t::y_major,
        cf_t::axes_t::mueller,
        cf_t::axes_t::index,
        cf_t::axes_t::x_minor,
        cf_t::axes_t::y_minor>(work_space, bounds),
      KOKKOS_LAMBDA(
        coord_t const x_major,
        coord_t const y_major,
        coord_t const mlr,
        coord_t const idx,
        coord_t const x_minor,
        coord_t const y_minor) {
        auto gen = random_pool.get_state();
        if constexpr (std::is_same_v<Configuration::cf_value_t, float>) {
          values(x_major, x_minor, y_major, y_minor, mlr, idx) =
            K::complex{gen.frand(-1, 1), gen.frand(-1, 1)};
        } else {
          values(x_major, x_minor, y_major, y_minor, mlr, idx) =
            K::complex{gen.drand(-1, 1), gen.drand(-1, 1)};
        }
        random_pool.free_state(gen);
      });
  }
#else  // !RCP_USE_KOKKOS
  static auto
  serial_body(
    L::Task const*,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context,
    L::Runtime*) -> result_t {

    assert(regions.size() == 1);

    auto& values_region = regions[values_region_index];
    auto const values =
      cf_md_t::values<LEGION_WRITE_ONLY>(values_region, CFValueField{});

    std::mt19937 gen(2000);
    std::uniform_real_distribution<Configuration::cf_value_t> u1(
      Configuration::cf_value_t{-1}, Configuration::cf_value_t{1});
    for (L::PointInRectIterator pir(values_region); pir(); ++pir)
      values[*pir] = std::complex{u1(gen), u1(gen)};
  }
#endif // RCP_USE_KOKKOS

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar.add_layout_constraint_set(
      values_region_index,
      cf_layout().constraint_id(traits::layout_style).value());
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar.add_layout_constraint_set(
      values_region_index,
      cf_layout().constraint_id(rt, traits::layout_style).value());
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID CFInitTask::task_id;

/*! task to create CF index */
struct CFIndexTask : DefaultKokkosTaskMixin<CFIndexTask, void> {
  /*! \brief task name */
  static constexpr char const* task_name = "CFIndexTask";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief region indexes */
  enum { uvws_region_index = 0, cf_indexes_region_index, num_regions };

  using bc_t = BaselineChannelRegion;

#ifdef RCP_USE_KOKKOS
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(GetCFValues::IndexTaskArgs));
    // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)
    GetCFValues::IndexTaskArgs const& args =
      *reinterpret_cast<GetCFValues::IndexTaskArgs const*>(task->args);
    // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)
    auto const& w_depth_arcsec = args.w_depth_arcsec;

    auto const& uvws_region = regions[uvws_region_index];
    bc_t::rect_t bounds = uvws_region;
    auto const uvws =
      bc_t::view<execution_space, LEGION_READ_ONLY>(uvws_region, UVWField{});

    auto const cf_indexes = bc_t::view<execution_space, LEGION_WRITE_ONLY>(
      regions[cf_indexes_region_index], CFIndexField{});

    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);
    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();

    K::parallel_for(
      task_name,
      mdrange_policy<
        execution_space,
        bc_t,
        bc_t::axes_t::baseline,
        bc_t::axes_t::channel>(work_space, bounds),
      KOKKOS_LAMBDA(bc_t::coord_t const bl, bc_t::coord_t const ch) {
        cf_indexes(bl, ch) =
          std::lrint(std::floor(std::abs(uvws(bl, ch)[2] * w_depth_arcsec)));
      });
  }
#else  // !RCP_USE_KOKKOS
  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context,
    L::Runtime*) -> result_t {

    assert(regions.size() == 2);
    assert(task->arglen == sizeof(GetCFValues::IndexTaskArgs));
    GetCFValues::IndexTaskArgs const& args =
      *reinterpret_cast<GetCFValues::IndexTaskArgs const*>(task->args);
    uvw_value_t const& w_depth_arcsec = args.w_depth_arcsec;

    auto& uvws_region = regions[uvws_region_index];
    uvw_t::rect_t bounds = uvws_region;
    auto const uvws = uvw_t::values<LEGION_READ_ONLY>(uvws_region, UVWField{});

    auto& cf_indexes_region = regions[cf_indexes_region_index];
    auto const cf_indexes =
      uvw_t::values<LEGION_WRITE_ONLY>(cf_indexes_region, CFIndexField{});

    for (L::PointInRectIterator pir(uvws_region); pir(); ++pir) {
      cf_indexes[*pir] =
        std::lrint(std::floor(std::abs(uvws[*pir][2] * w_depth_arcsec)));
    }
  }
#endif // RCP_USE_KOKKOS

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    auto layout = uvws_layout().constraint_id(traits::layout_style).value();
    registrar.add_layout_constraint_set(uvws_region_index, layout)
      .add_layout_constraint_set(cf_indexes_region_index, layout);
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    auto layout = uvws_layout().constraint_id(rt, traits::layout_style).value();
    registrar.add_layout_constraint_set(uvws_region_index, layout)
      .add_layout_constraint_set(cf_indexes_region_index, layout);
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID CFIndexTask::task_id;

GetCFValues::GetCFValues(
  L::Context ctx, L::Runtime* rt, ImagingConfiguration const& img)
  : m_cf_arrays(cf_array_region(ctx, rt, img))
  , m_index_task_args{img.im.w_depth_arcsec} {

  // create the mueller matrix index arrays
  auto mfs = mi_t::field_space(ctx, rt);
  auto maybe_mis = mi_t::index_space(ctx, rt);
  assert(maybe_mis);
  // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
  auto mis = maybe_mis.value();
  m_mueller_indexes =
    mi_t::LogicalRegion{rt->create_logical_region(ctx, mis, mfs)};

  // fill the region with random values
  L::TaskLauncher launcher(
    CFInitTask::task_id,
    L::UntypedBuffer(&img.im.full_stokes, sizeof(img.im.full_stokes)));
  launcher.region_requirements.resize(CFInitTask::num_regions);
  launcher.region_requirements[CFInitTask::values_region_index] =
    m_cf_arrays.requirement(
      LEGION_WRITE_ONLY, LEGION_EXCLUSIVE, ::rcp::StaticFields{CFValueField{}});
  launcher.region_requirements[CFInitTask::mueller_region_index] =
    m_mueller_indexes.requirement(
      LEGION_WRITE_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{ForwardIndexField{}, InverseIndexField{}});
  rt->execute_task(ctx, launcher);

  rt->destroy_index_space(ctx, mis);
  rt->destroy_field_space(ctx, mfs);
}

GetCFValues::GetCFValues(
  L::Context /*ctx*/, L::Runtime* /*rt*/, Serialized const& serialized)
  : m_cf_arrays{serialized.m_cf_arrays}
  , m_index_task_args{serialized.m_index_task_args}
  , m_mueller_indexes{serialized.m_mueller_indexes} {}

auto
GetCFValues::serialized(L::Context ctx, L::Runtime* rt) const noexcept
  -> Serialized {
  return {m_cf_arrays, m_index_task_args, m_mueller_indexes};
}

auto
GetCFValues::child_requirements(L::Context ctx, L::Runtime* rt) const noexcept
  -> ChildRequirements {
  return {
    {m_cf_arrays.requirement(
       LEGION_READ_ONLY,
       LEGION_EXCLUSIVE,
       StaticFields{CFValueField{}},
       std::nullopt,
       std::nullopt,
       L::Mapping::DefaultMapper::VIRTUAL_MAP),
     m_mueller_indexes.requirement(
       LEGION_READ_ONLY,
       LEGION_EXCLUSIVE,
       StaticFields{ForwardIndexField{}, InverseIndexField{}},
       std::nullopt,
       std::nullopt,
       L::Mapping::DefaultMapper::VIRTUAL_MAP)},
    {},
    {}};
}

auto
GetCFValues::mueller_indexes_region() const -> mi_t::LogicalRegion const& {
  return m_mueller_indexes;
}

void
GetCFValues::destroy(L::Context ctx, L::Runtime* rt) {
  rt->destroy_logical_region(ctx, m_cf_arrays);
  rt->destroy_logical_region(ctx, m_mueller_indexes);
}

auto
GetCFValues::launch(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  L::Predicate pred) const -> cf_t::LogicalRegion {

  auto uvw_is = uvws.region().get_index_space();
  auto cf_index_ip = bc_t::create_partition_by_slicing(
    ctx,
    rt,
    uvw_is,
    {{bc_t::axes_t::channel,
      DataPacketConfiguration::num_channels_per_packet}});
  auto launcher = L::IndexTaskLauncher(
    CFIndexTask::task_id,
    rt->get_index_partition_color_space(cf_index_ip),
    L::UntypedBuffer(&m_index_task_args, sizeof(m_index_task_args)),
    L::ArgumentMap());
  launcher.region_requirements.resize(CFIndexTask::num_regions);

  auto uvw_lp = bc_t::LogicalPartition{
    rt->get_logical_partition(uvws.region(), cf_index_ip)};
  launcher.region_requirements[CFIndexTask::cf_indexes_region_index] =
    uvw_lp.requirement(
      0,
      LEGION_WRITE_ONLY,
      LEGION_EXCLUSIVE,
      uvws.parent(),
      StaticFields{CFIndexField{}});
  launcher.region_requirements[CFIndexTask::uvws_region_index] =
    uvw_lp.requirement(
      0,
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      uvws.parent(),
      StaticFields{UVWField{}});
  // NOLINTNEXTLINE(performance-unnecessary-value-param)
  launcher.predicate = pred;
  rt->execute_index_space(ctx, launcher);
  return m_cf_arrays;
}

auto
GetCFValues::cf_array_region(
  L::Context ctx,
  L::Runtime* rt,
  ImagingConfiguration const& img) -> cf_t::LogicalRegion {

  // create a region of CF array values
  cf_t::Params params{
    .oversampling = img.gr.test.cf_oversampling_factor,
    .padding = img.gr.test.cf_array_padding};
  auto fs = cf_t::field_space(ctx, rt);
  auto maybe_is = cf_t::index_space(
    ctx,
    rt,
    cf_t::extents_to_bounds(
      {img.gr.test.cf_size, img.gr.test.cf_size},
      (img.im.full_stokes ? 12 : 3),
      img.im.w_size,
      params));
  assert(maybe_is);
  // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
  auto is = maybe_is.value();
  auto result = cf_t::LogicalRegion{rt->create_logical_region(ctx, is, fs)};
  cf_t::attach_params(rt, result, params);
  rt->destroy_index_space(ctx, is);
  rt->destroy_field_space(ctx, fs);
  return result;
}

auto
GetCFValues::preregister_tasks(L::TaskID init_task_id, L::TaskID index_task_id)
  -> void {
  PortableTask<CFInitTask>::preregister_task_variants(init_task_id);
  PortableTask<CFIndexTask>::preregister_task_variants(index_task_id);
}

auto
GetCFValues::register_tasks(
  L::Runtime* rt, L::TaskID init_task_id, L::TaskID index_task_id) -> void {
  PortableTask<CFInitTask>::register_task_variants(rt, init_task_id);
  PortableTask<CFIndexTask>::register_task_variants(rt, index_task_id);
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
