// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "StreamState.hpp"
#include "EventScript.hpp"
#include "ImagingEvent.hpp"
#include "SimpleEvent.hpp"
#include "StringValueEvent.hpp"
#include "SubarrayEvent.hpp"
#include "libsymcam.hpp"
#include <bits/ranges_algo.h>
#include <bits/ranges_algobase.h>
#include <chrono>
#include <cstring>
#include <fftw3.h>
#include <fstream>
#include <ranges>
#include <sstream>
#include <stdexcept>
#include <utility>

using namespace rcp::symcam::st;

auto
StateMixin::DataStream::clear() -> void {
  std::ranges::fill(path, '\0');
  std::ranges::fill(channel_intervals, invalid_channel_interval);
}

auto
StateMixin::DataStream::filepath() const -> std::filesystem::path {
  return path.data();
}

auto
StateMixin::is_alive() const noexcept -> bool {
  return m_is_alive;
}

auto
StateMixin::subarray_partition() const -> L::IndexPartitionT<1, int> {
  return m_subarray_partition;
}

auto
StateMixin::is_imaging(unsigned slot) const -> bool {
  return imaging_id(slot).has_value();
}

auto
StateMixin::imaging_id(unsigned slot) const -> std::optional<img_id_t> {
  auto id = m_imaging.at(slot);
  if (id >= 0)
    return id;
  return std::nullopt;
}

auto
StateMixin::field_center() const -> rotation_t {
  static constexpr rotation_t result{1, 0, 0, 0, 1, 0, 0, 0, 1};
  return result;
}

auto
StateMixin::record_data_stream(unsigned slot) -> DataStream& {
  return m_record_data_streams.at(slot);
}

auto
StateMixin::record_data_stream(unsigned slot) const -> DataStream const& {
  return m_record_data_streams.at(slot);
}

auto
StateMixin::is_pre_calibration_flagging(unsigned slot) -> bool& {
  return m_pre_calibration_flagging.at(slot);
}

auto
StateMixin::is_pre_calibration_flagging(unsigned slot) const -> bool {
  return m_pre_calibration_flagging.at(slot);
}

auto
StateMixin::fftw_wisdom_import_path() const noexcept -> path_t const& {
  return m_fftw_wisdom_import_path;
}

auto
StateMixin::fftw_wisdom_import_path() noexcept -> path_t& {
  return m_fftw_wisdom_import_path;
}

auto
StateMixin::fftw_wisdom_export_path() const noexcept -> path_t const& {
  return m_fftw_wisdom_export_path;
}

auto
StateMixin::fftw_wisdom_export_path() noexcept -> path_t& {
  return m_fftw_wisdom_export_path;
}

auto
StateMixin::do_fftw_wisdom_import(bool local) const -> void {

  // because of restrictions of control replication, calls to Legion loggers
  // cannot depend on the value of "local"
  log().debug(std::format(
    "Importing fftw wisdom from '{}'", m_fftw_wisdom_import_path.data()));
  std::filesystem::path dbl_filepath(m_fftw_wisdom_import_path.data());
  if (dbl_filepath.is_absolute()) {
#ifdef SYMCAM_ENABLE_FFTW_DOUBLE
    if (
      local
      && fftw_import_wisdom_from_filename(dbl_filepath.string().data()) == 0)
      std::cerr << std::format(
        "Failed to import '{}' fftw wisdom file\n", dbl_filepath.string());
#endif // SYMCAM_ENABLE_FFTW_DOUBLE
#ifdef SYMCAM_ENABLE_FFTW_FLOAT
    auto flt_filepath = float_wisdom_file_path(dbl_filepath);
    if (
      local
      && fftwf_import_wisdom_from_filename(flt_filepath.string().data()) == 0)
      std::cerr << std::format(
        "Failed to import '{}' fftwf wisdom file\n", flt_filepath.string());
#endif // SYMCAM_ENABLE_FFTW_FLOAT
  } else {
    log().warn("Path to fftw wisdom file is not absolute: no wisdom imported");
  }
}

auto
StateMixin::do_fftw_wisdom_export(bool local) const -> void {

  // because of restrictions of control replication, calls to Legion loggers
  // cannot depend on the value of "local"
  log().debug(std::format(
    "Exporting fftw wisdom to '{}'", m_fftw_wisdom_export_path.data()));
  std::filesystem::path dbl_filepath(m_fftw_wisdom_export_path.data());
  if (dbl_filepath.is_absolute()) {
#ifdef SYMCAM_ENABLE_FFTW_DOUBLE
    if (
      local
      && fftw_export_wisdom_to_filename(dbl_filepath.string().data()) == 0)
      std::cerr << std::format(
        "Failed to export '{}' fftw wisdom file\n", dbl_filepath.string());
#endif // SYMCAM_ENABLE_FFTW_DOUBLE
#ifdef SYMCAM_ENABLE_FFTW_FLOAT
    auto flt_filepath = float_wisdom_file_path(dbl_filepath);
    if (
      local
      && fftwf_export_wisdom_to_filename(flt_filepath.string().data()) == 0)
      std::cerr << std::format(
        "Failed to export '{}' fftwf wisdom file\n", flt_filepath.string());
#endif // SYMCAM_ENABLE_FFTW_FLOAT
  } else {
    log().warn("Path to fftw wisdom file is not absolute: no wisdom exported");
  }
}

State::State() { init(); }

State::State(ExtendedState const& extd) : StreamStateBase(extd.timestamp()) {

  init();
  m_is_alive = extd.is_alive();
  m_subarray_partition = extd.subarray_partition();
  m_fftw_wisdom_import_path = extd.fftw_wisdom_import_path();
  m_fftw_wisdom_export_path = extd.fftw_wisdom_export_path();
  for (auto&& sa : extd.subarrays()) {
    auto maybe_slot = extd.find_subarray(std::get<0>(sa));
    assert(maybe_slot);
    // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
    m_subarray_names.at(maybe_slot.value()) = std::get<0>(sa);
  }
  for (auto&& img : m_imaging) {
    auto slot = std::distance(m_imaging.data(), &img);
    auto maybe_imaging_id = extd.imaging_id(slot);
    if (maybe_imaging_id)
      img = *maybe_imaging_id;
  }
  for (auto&& ds : m_record_data_streams) {
    auto slot = std::distance(m_record_data_streams.data(), &ds);
    ds = extd.record_data_stream(slot);
  }
  for (auto&& flg : m_pre_calibration_flagging) {
    auto slot = std::distance(m_pre_calibration_flagging.data(), &flg);
    flg = extd.is_pre_calibration_flagging(slot);
  }
}

auto
State::init() -> void {
  std::ranges::fill(m_subarray_names, subarray_name_t{'\0'});
  std::ranges::fill(m_imaging, -1);
  std::ranges::for_each(m_record_data_streams, &StateMixin::DataStream::clear);
  std::ranges::fill(m_pre_calibration_flagging, false);
  std::ranges::fill(m_fftw_wisdom_import_path, '\0');
  std::ranges::fill(m_fftw_wisdom_export_path, '\0');
}

auto
State::has_same_subarrays(State const& st) const -> bool {
  for (auto&& nm : m_subarray_names) {
    auto const& stnm =
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index)
      st.m_subarray_names[std::distance(m_subarray_names.data(), &nm)];
    if (std::strcmp(nm.data(), stnm.data()) != 0)
      return false;
  }
  return true;
}

auto
State::subarray_names() const -> std::vector<subarray_name_t> {
  std::vector<subarray_name_t> result;
  std::ranges::copy(
    m_subarray_names
      | std::views::filter([](auto&& sa) { return sa[0] != '\0'; }),
    std::back_inserter(result));
  return result;
}

auto
State::find_subarray(subarray_name_t const& subarray_name) const
  -> std::optional<unsigned> {
  auto const* found = std::ranges::find_if(m_subarray_names, [&](auto&& sn) {
    return std::strcmp(subarray_name.data(), sn.data()) == 0;
  });
  if (found == m_subarray_names.end())
    return std::nullopt;
  return std::distance(m_subarray_names.begin(), found);
}

auto
State::has_subarray(unsigned slot) const -> bool {
  return m_subarray_names.at(slot)[0] != '\0';
}

auto
State::is_subarray(
  std::optional<State> const& /*prev*/,
  State const& current,
  std::optional<State> const& /*next*/,
  unsigned slot) -> bool {

  return current.has_subarray(slot);
}

auto
State::is_imaging_start(
  std::optional<State> const& prev,
  State const& current,
  std::optional<State> const& /*next*/,
  unsigned slot) -> bool {

  auto current_id = current.imaging_id(slot);
  if (!prev)
    return current_id.has_value();
  auto prev_id = prev->imaging_id(slot);
  // start imaging iff an imaging id that is present in current interval
  // is not present in the previous interval
  return current_id.has_value()
         && (!prev_id.has_value() || (current_id.value() != prev_id.value()));
}

SubarrayQuery::SubarrayQuery(
  unsigned slot,
  std::optional<State> const& prev,
  State const& current,
  std::optional<State> const& next)
  : m_slot{slot}, m_prev{prev}, m_current{current}, m_next{next} {}

auto
SubarrayQuery::is_subarray() const -> bool {
  return m_current.has_subarray(m_slot);
}

auto
SubarrayQuery::do_imaging_start() const -> bool {

  auto current_id = m_current.imaging_id(m_slot);
  if (!m_prev)
    return current_id.has_value();
  auto prev_id = m_prev->imaging_id(m_slot);
  // start imaging iff an imaging id that is present in current interval
  // is not present in the previous interval
  return current_id.has_value()
         && (!prev_id.has_value() || (current_id.value() != prev_id.value()));
}

auto
SubarrayQuery::do_imaging() const -> bool {

  return m_current.is_imaging(m_slot);
}

auto
SubarrayQuery::do_imaging_stop() const -> bool {

  auto current_id = m_current.imaging_id(m_slot);
  if (!m_next)
    return current_id.has_value();
  auto next_id = m_next->imaging_id(m_slot);
  // end imaging iff an imaging id that is present in current interval is
  // not present in the next interval
  return current_id.has_value()
         && (!next_id.has_value() || (current_id.value() != next_id.value()));
}

auto
SubarrayQuery::do_record_visibilities() const -> bool {
  // here we're just looking for a non-empty current path -- sorting out the
  // different actions open handle, write visibilities, and/or close handle is
  // left to the recording task
  return !m_current.record_data_stream(m_slot).filepath().empty();
}

auto
SubarrayQuery::do_pre_calibration_flagging() const -> bool {
  return m_current.is_pre_calibration_flagging(m_slot);
}

auto
SubarrayQuery::do_fftw_wisdom_export() const noexcept -> bool {
  return m_current.fftw_wisdom_export_path()[0] != '\0';
}

auto
SubarrayQuery::do_fftw_wisdom_import() const noexcept -> bool {
  return m_current.fftw_wisdom_import_path()[0] != '\0';
}

AllSubarraysQuery::AllSubarraysQuery(
  std::optional<State> const& prev,
  State const& current,
  std::optional<State> const& next)
  : m_prev{prev}, m_current{current}, m_next{next} {}

auto
AllSubarraysQuery::is_alive() const -> bool {

  return m_current.is_alive();
}

auto
AllSubarraysQuery::do_subarray_change() const -> bool {

  return !m_prev || !m_next || !m_prev->has_same_subarrays(m_current);
}

auto
AllSubarraysQuery::is_subarray() const -> sarray<bool> {

  return all_subarrays(&SubarrayQuery::is_subarray);
}

auto
AllSubarraysQuery::do_imaging_start() const -> sarray<bool> {

  return all_subarrays(&SubarrayQuery::do_imaging_start);
}

auto
AllSubarraysQuery::do_imaging() const -> sarray<bool> {

  return all_subarrays(&SubarrayQuery::do_imaging);
}

auto
AllSubarraysQuery::do_imaging_stop() const -> sarray<bool> {

  return all_subarrays(&SubarrayQuery::do_imaging_stop);
}

auto
AllSubarraysQuery::do_record_visibilities() const -> sarray<bool> {
  return all_subarrays(&SubarrayQuery::do_record_visibilities);
}

auto
AllSubarraysQuery::do_pre_calibration_flagging() const -> sarray<bool> {
  return all_subarrays(&SubarrayQuery::do_pre_calibration_flagging);
}

auto
AllSubarraysQuery::do_fftw_wisdom_export() const -> sarray<bool> {
  return all_subarrays(&SubarrayQuery::do_fftw_wisdom_export);
}

auto
AllSubarraysQuery::do_fftw_wisdom_import() const -> sarray<bool> {
  return all_subarrays(&SubarrayQuery::do_fftw_wisdom_import);
}

ExtendedState::ExtendedState(
  L::Context /*ctx*/, L::Runtime* /*rt*/, Configuration config)
  : m_config(std::move(config)) {
  init();
}

ExtendedState::ExtendedState(ExtendedState const& other) noexcept
  : StreamStateBase(other)
  , m_config(other.m_config)
  , m_subarrays(other.m_subarrays) {
  // event and data streams are associated with resources allocated by other, so
  // we don't copy them
  m_is_alive = other.m_is_alive;
  m_subarray_partition = other.m_subarray_partition;
  m_imaging = other.m_imaging;
}

auto
ExtendedState::destroy() noexcept -> void {
  for (auto&& slot : subarray_slots())
    *this = std::move(*this).reset_subarray(slot);
}

auto
ExtendedState::operator=(ExtendedState const& rhs) noexcept -> ExtendedState& {
  if (this != &rhs) {
    // event and data streams are associated with resources allocated by rhs, so
    // we don't copy them
    m_is_alive = rhs.m_is_alive;
    m_subarrays = rhs.m_subarrays;
    m_subarray_partition = rhs.m_subarray_partition;
    m_imaging = rhs.m_imaging;
    for (auto&& slot : subarray_slots())
      *this = std::move(*this).reset_record_data_stream(slot);
  }
  return *this;
}

auto
ExtendedState::pre_apply(
  StreamStateEventBase<ExtendedState> const& evt) const& -> ExtendedState {
  return ExtendedState(*this).pre_apply(evt);
}

auto
ExtendedState::pre_apply(
  StreamStateEventBase<ExtendedState> const& evt) && -> ExtendedState {
  if (evt.tag() == StopRecordEventStream::class_tag)
    record_event(&evt);
  return std::move(*this);
}

auto
ExtendedState::post_apply(
  StreamStateEventBase<ExtendedState> const& evt) const& -> ExtendedState {
  return ExtendedState(*this).post_apply(evt);
}

auto
ExtendedState::post_apply(
  StreamStateEventBase<ExtendedState> const& evt) && -> ExtendedState {
  if (evt.tag() != StopRecordEventStream::class_tag)
    record_event(&evt);
  return std::move(*this);
}

auto
ExtendedState::set_alive(bool alive) const& noexcept -> ExtendedState {
  return ExtendedState(*this).set_alive(alive);
}

auto
ExtendedState::set_alive(bool alive) && noexcept -> ExtendedState {
  if (!alive) {
    for (auto&& slot : subarray_slots())
      *this = std::move(*this).reset_subarray(slot);
    init();
  }
  m_is_alive = alive;
  return std::move(*this);
}

auto
ExtendedState::subarrays() const
  -> std::vector<std::tuple<subarray_name_t, subarray_t>> {

  std::vector<std::tuple<subarray_name_t, subarray_t>> result;
  std::ranges::copy(
    m_subarrays | std::views::filter([](auto&& sa) {
      return std::get<0>(sa)[0] != '\0';
    }),
    std::back_inserter(result));
  return result;
}

auto
ExtendedState::subarray_names() const -> std::vector<subarray_name_t> {

  std::vector<subarray_name_t> result;
  std::ranges::copy(
    m_subarrays | std::views::filter([](auto&& sa) {
      return std::get<0>(sa)[0] != '\0';
    }) | std::views::transform([](auto&& sa) { return std::get<0>(sa); }),
    std::back_inserter(result));
  return result;
}

auto
ExtendedState::find_subarray(subarray_name_t const& subarray_name) const
  -> std::optional<unsigned> {
  auto const* found = std::ranges::find_if(m_subarrays, [&](auto&& sa) {
    return std::strcmp(std::get<0>(sa).data(), subarray_name.data()) == 0;
  });
  if (found == m_subarrays.end())
    return std::nullopt;
  return std::distance(m_subarrays.begin(), found);
}

auto
ExtendedState::has_same_subarrays(ExtendedState const& st) const -> bool {
  for (auto&& nm_sa : m_subarrays) {
    auto const& [nm, sa] = nm_sa;
    auto const& [stnm, stsa] =
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index)
      st.m_subarrays[std::distance(m_subarrays.data(), &nm_sa)];
    if (std::strcmp(nm.data(), stnm.data()) != 0 || sa != stsa)
      return false;
  }
  return true;
}

auto
ExtendedState::set_subarray(
  unsigned slot,
  subarray_name_t const& subarray_name,
  subarray_t const& subarray) const& -> ExtendedState {
  return ExtendedState(*this).set_subarray(slot, subarray_name, subarray);
}

auto
ExtendedState::set_subarray(
  unsigned slot,
  subarray_name_t const& subarray_name,
  subarray_t const& subarray) && -> ExtendedState {
  *this = std::move(*this).reset_imaging(slot).reset_record_event_stream(slot);
  m_subarrays.at(slot) = {subarray_name, subarray};
  return std::move(*this);
}

auto
ExtendedState::reset_subarray(unsigned slot) const& -> ExtendedState {
  return ExtendedState(*this).reset_subarray(slot);
}

auto
ExtendedState::reset_subarray(unsigned slot) && -> ExtendedState {
  m_subarrays.at(slot) = empty_subarray();
  return std::move(*this)
    .reset_imaging(slot)
    .reset_record_event_stream(slot)
    .reset_record_data_stream(slot)
    .set_pre_calibration_flagging(slot, false);
}

auto
ExtendedState::get_subarray(
  unsigned slot) const& -> std::optional<subarray_t const*> {
  if (std::get<0>(m_subarrays.at(slot))[0] == '\0')
    return std::nullopt;
  return &std::get<1>(m_subarrays.at(slot));
}

auto
ExtendedState::set_subarray_partition(
  L::IndexPartitionT<1, int> partition) const& -> ExtendedState {
  return ExtendedState(*this).set_subarray_partition(partition);
}

auto
ExtendedState::set_subarray_partition(
  L::IndexPartitionT<1, int> partition) && -> ExtendedState {
  m_subarray_partition = partition;
  return std::move(*this);
}

auto
ExtendedState::set_imaging(
  unsigned slot, img_id_t const& id) const& -> ExtendedState {
  return ExtendedState(*this).set_imaging(slot, id);
}

auto
ExtendedState::set_imaging(
  unsigned slot, img_id_t const& id) && -> ExtendedState {
  if (id < 0)
    throw InvalidImagingId(id);
  m_imaging.at(slot) = id;
  return std::move(*this);
}

auto
ExtendedState::reset_imaging(unsigned slot) const& -> ExtendedState {
  return ExtendedState(*this).reset_imaging(slot);
}

auto
ExtendedState::reset_imaging(unsigned slot) && -> ExtendedState {
  m_imaging.at(slot) = -1;
  return std::move(*this);
}

auto
ExtendedState::set_record_event_stream(
  unsigned slot,
  std::filesystem::path const& filepath) const& -> ExtendedState {

  return ExtendedState(*this).set_record_event_stream(slot, filepath);
}

auto
ExtendedState::set_record_event_stream(
  unsigned slot, std::filesystem::path const& filepath) && -> ExtendedState {

  // stop the current recording regardless of validity of filepath
  *this = std::move(*this).reset_record_event_stream(slot);
  if (!filepath.is_absolute())
    throw RecordEventStreamFilepathError(filepath);
  auto new_stream = std::ofstream(filepath);
  if (!new_stream)
    throw RecordEventStreamFilepathError(filepath);
  auto& [current_stream, sep] = m_record_event_streams.at(slot);
  current_stream = std::move(new_stream);
  current_stream << "{\"" << EventScript::script_tag << "\":[{\""
                 << EventScript::block_events_tag << "\":[";
  sep[0] = '\0';
  return std::move(*this);
}

auto
ExtendedState::reset_record_event_stream(
  unsigned slot) const& -> ExtendedState {

  return ExtendedState(*this).reset_record_event_stream(slot);
}

auto
ExtendedState::reset_record_event_stream(unsigned slot) && -> ExtendedState {

  auto& current_stream = std::get<0>(m_record_event_streams.at(slot));
  if (current_stream.is_open()) {
    current_stream << "]}]}";
    current_stream.close();
  }
  return std::move(*this);
}

auto
ExtendedState::record_event(StreamStateEventBase<ExtendedState> const* event)
  -> void {
  std::optional<unsigned> slot;
  std::string tag = event->tag();
  if (tag == StartImaging::class_tag)
    slot = find_subarray(dynamic_cast<StartImaging const*>(event)->name());
  else if (tag == StopImaging::class_tag)
    slot = find_subarray(dynamic_cast<StopImaging const*>(event)->name());
  else if (tag == DestroySubarray::class_tag)
    slot = find_subarray(dynamic_cast<DestroySubarray const*>(event)->name());
  else if (tag == CreateSubarray::class_tag)
    slot = dynamic_cast<CreateSubarray const*>(event)->slot();
  else if (tag == StartRecordEventStream::class_tag)
    slot =
      find_subarray(dynamic_cast<StartRecordEventStream const*>(event)->name());
  else if (tag == StopRecordEventStream::class_tag)
    slot =
      find_subarray(dynamic_cast<StopRecordEventStream const*>(event)->name());
  else if (tag == StartRecordDataStream::class_tag)
    slot =
      find_subarray(dynamic_cast<StartRecordDataStream const*>(event)->name());
  else if (tag == StopRecordDataStream::class_tag)
    slot =
      find_subarray(dynamic_cast<StopRecordDataStream const*>(event)->name());
  if (slot)
    record_event(*slot, event);
  else
    std::ranges::for_each(
      subarray_slots(), [&](auto&& slot) { record_event(slot, event); });
}

auto
ExtendedState::record_event(
  unsigned slot, StreamStateEventBase<ExtendedState> const* event) -> void {

  auto& [current_stream, sep] = m_record_event_streams.at(slot);
  if (current_stream.is_open()) {
    assert(sep[0] == '\0' || (sep[0] == ',' && sep[1] == '\0'));
    current_stream << sep.data() << as_json(*event);
    sep[0] = ',';
    sep[1] = '\0';
  }
}

auto
ExtendedState::float_wisdom_file_path(std::filesystem::path const& path)
  -> std::filesystem::path {
  auto result =
    path.parent_path() / std::filesystem::path(path.stem().string() + "f");
  if (path.has_extension())
    result += path.extension();
  return result;
}

auto
ExtendedState::set_record_data_stream(
  unsigned slot,
  ChannelSet const& channels,
  std::unordered_map<std::string, std::string> const& parameters,
  std::filesystem::path const& filepath) const& -> ExtendedState {

  return ExtendedState(*this).set_record_data_stream(
    slot, channels, parameters, filepath);
}

auto
ExtendedState::set_record_data_stream(
  unsigned slot,
  ChannelSet const& channels,
  std::unordered_map<std::string, std::string> const& parameters,
  std::filesystem::path const& filepath) && -> ExtendedState {

  nlohmann::json params_js = parameters;
  auto params_str = params_js.dump();
  if (params_str.size() + 1 > StateMixin::max_parameters_length)
    throw ParametersTooLongError(params_str);

  auto in = channels.intersection(m_config);
  // configure the data output stream only when channels selects something in
  // our configured channel range
  if (!in.empty()) {
    if (filepath.string().size() + 1 > sizeof(path_t))
      throw FilepathTooLongError(filepath);
    auto ds = m_record_data_streams.at(slot);
    ds.clear();
    std::ranges::copy(
      std::views::take(in.intervals(), ds.channel_intervals.size()),
      ds.channel_intervals.begin());
    std::strncpy(ds.path.data(), filepath.c_str(), sizeof(ds.path));
    ds.path[sizeof(ds.path) - 1] = '\0';
    std::strncpy(
      ds.parameters.data(), params_str.c_str(), sizeof(ds.parameters));
    ds.parameters[sizeof(ds.parameters) - 1] = '\0';
    m_record_data_streams.at(slot) = ds;
  } else {
    m_record_data_streams.at(slot).clear();
  }
  return std::move(*this);
}

auto
ExtendedState::reset_record_data_stream(unsigned slot) const& -> ExtendedState {
  return ExtendedState(*this).reset_record_data_stream(slot);
}

auto
ExtendedState::reset_record_data_stream(unsigned slot) && -> ExtendedState {
  m_record_data_streams.at(slot).clear();
  return std::move(*this);
}

auto
ExtendedState::init() -> void {
  std::ranges::fill(m_subarrays, empty_subarray());
  std::ranges::fill(m_imaging, -1);
  for (auto&& rec : m_record_event_streams)
    rec = {std::ofstream{}, std::array{'\0', '\0'}};
  std::ranges::for_each(m_record_data_streams, &StateMixin::DataStream::clear);
  std::ranges::fill(m_pre_calibration_flagging, false);
}

auto
ExtendedState::empty_subarray()
  -> std::tuple<subarray_name_t, subarray_t> const& {
  static const std::tuple<subarray_name_t, subarray_t> result = {
    subarray_name_t{'\0'}, ReceiverPairs()};
  return result;
}

auto
ExtendedState::set_pre_calibration_flagging(
  unsigned slot, bool state) const& -> ExtendedState {
  return ExtendedState(*this).set_pre_calibration_flagging(slot, state);
}

auto
ExtendedState::set_pre_calibration_flagging(
  unsigned slot, bool state) && -> ExtendedState {
  m_pre_calibration_flagging.at(slot) = state;
  return std::move(*this);
}

auto
ExtendedState::set_fftw_wisdom_import_path(
  std::filesystem::path const& path) const& -> ExtendedState {
  return ExtendedState(*this).set_fftw_wisdom_import_path(path);
}

auto
ExtendedState::set_fftw_wisdom_import_path(
  std::filesystem::path const& path) && -> ExtendedState {

  auto const pathstr = path.string();
  if (pathstr.size() >= max_path_length)
    throw std::runtime_error("FFTW wisdom file path name too long");
  auto& mypath = fftw_wisdom_import_path();
  std::ranges::fill(mypath, '\0');
  std::ranges::copy(pathstr, mypath.begin());
  return std::move(*this);
}

auto
ExtendedState::set_fftw_wisdom_export_path(
  std::filesystem::path const& path) const& -> ExtendedState {
  return ExtendedState(*this).set_fftw_wisdom_export_path(path);
}

auto
ExtendedState::set_fftw_wisdom_export_path(
  std::filesystem::path const& path) && -> ExtendedState {

  auto const pathstr = path.string();
  if (pathstr.size() >= max_path_length)
    throw std::runtime_error("FFTW wisdom file path name too long");
  auto& mypath = fftw_wisdom_export_path();
  std::ranges::fill(mypath, '\0');
  std::ranges::copy(pathstr, mypath.begin());
  return std::move(*this);
}

auto
ExtendedState::reset_fftw_wisdom_paths() const& -> ExtendedState {
  return ExtendedState(*this).reset_fftw_wisdom_paths();
}

auto
ExtendedState::reset_fftw_wisdom_paths() && -> ExtendedState {
  std::ranges::fill(fftw_wisdom_import_path(), '\0');
  std::ranges::fill(fftw_wisdom_export_path(), '\0');
  return std::move(*this);
}

auto
JSONValue::has_tag(nlohmann::json const& js) noexcept -> bool {
  return js.contains(meta_tag_name);
}

auto
JSONValue::get_tag(nlohmann::json const& js) -> std::string {
  return js.at(meta_tag_name).get<std::string>();
}

auto
JSONValue::has_value(nlohmann::json const& js) noexcept -> bool {
  return js.contains(meta_value_name);
}

auto
JSONValue::get_value(nlohmann::json const& js) -> nlohmann::json const& {
  return js.at(meta_value_name);
}

auto
JSONValue::get_value(nlohmann::json& js) -> nlohmann::json& {
  return js.at(meta_value_name);
}

auto
JSONValue::is_valid(nlohmann::json const& js) noexcept -> bool {
  return has_tag(js) && js.at(meta_tag_name).is_string() && has_value(js);
}

auto
rcp::symcam::st::operator<<(std::ostream& os, ExtendedState const& st)
  -> std::ostream& {

  std::ostringstream oss;
  oss << "ExtendedState{ timestamp: "
      << st.timestamp().time_since_epoch().count()
      << ", is_alive: " << st.is_alive() << ", imaging: [";
  char const* sep = "";
  auto subarrays = st.subarrays();
  for (auto&& sa : subarrays) {
    auto& name = std::get<0>(sa);
    auto slot = st.find_subarray(name);
    if (slot && st.is_imaging(*slot)) {
      // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
      oss << sep << "(" << name.data() << ": " << st.imaging_id(*slot).value()
          << ")";
      sep = ", ";
    }
  }
  oss << "] }";
  return os << oss.str();
}

auto
rcp::symcam::st::operator<<(std::ostream& os, State const& st)
  -> std::ostream& {

  std::ostringstream oss;
  oss << "State{ timestamp: " << show(st.timestamp())
      << ", is_alive: " << st.is_alive() << ", imaging: [";
  char const* sep = "";
  auto subarrays = st.subarray_names();
  for (auto&& name : subarrays) {
    auto slot = st.find_subarray(name);
    if (slot && st.is_imaging(*slot)) {
      // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
      oss << sep << "(" << name.data() << ": " << st.imaging_id(*slot).value()
          << ")";
      sep = ",";
    }
  }
  oss << "], recording: [";
  sep = "";
  for (auto&& name : subarrays) {
    auto slot = st.find_subarray(name);
    if (slot) {
      auto ds = st.record_data_stream(*slot);
      if (!ds.filepath().empty()) {
        oss << sep << "(" << name.data() << ": " << ds.filepath() << ")";
        sep = ",";
      }
    }
  }
  oss << "], flagging: [";
  sep = "";
  for (auto&& name : subarrays) {
    auto slot = st.find_subarray(name);
    if (slot && st.is_pre_calibration_flagging(*slot)) {
      oss << sep << name.data();
      sep = ",";
    }
  }
  oss << "] }";
  return os << oss.str();
}

auto
rcp::symcam::st::parse_duration(std::u8string_view const& str)
  -> std::optional<StreamClock::duration> {

  auto cvt =
    [&](std::u8string const& suffix, [[maybe_unused]] auto const& duration)
    -> std::optional<StreamClock::duration> {
    using duration_t = std::remove_cvref_t<decltype(duration)>;
    if (str.ends_with(suffix)) {
      auto const val = str.substr(0, str.size() - suffix.size());
      // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)
      //  try conversion to integral type
      {
        char8_t* end{nullptr};
        auto const ts = std::strtoll(
          reinterpret_cast<char const*>(val.data()),
          reinterpret_cast<char**>(&end),
          0);
        if (end == val.data() + val.size())
          return StreamClock::duration{duration_t{ts}};
      }
      // try conversion to floating point type
      {
        char8_t* end{nullptr};
        auto const ts = std::strtod(
          reinterpret_cast<char const*>(val.data()),
          reinterpret_cast<char**>(&end));
        if (end == val.data() + val.size())
          return std::chrono::duration_cast<StreamClock::duration>(
            std::chrono::duration<double, typename duration_t::period>{ts});
      }
      // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)
    }
    return std::nullopt;
  };

  return [&](auto&&... args) {
    std::optional<StreamClock::duration> result;
    [[maybe_unused]] auto rc =
      (bool(result = cvt(std::get<0>(args), std::get<1>(args))) || ...);
    return result;
  }(std::tuple{std::u8string(u8"ns"), std::chrono::nanoseconds{}},
         std::tuple{std::u8string(u8"µs"), std::chrono::microseconds{}},
         std::tuple{std::u8string(u8"us"), std::chrono::microseconds{}},
         std::tuple{std::u8string(u8"ms"), std::chrono::milliseconds{}},
         std::tuple{std::u8string(u8"s"), std::chrono::seconds{}},
         std::tuple{std::u8string(u8"min"), std::chrono::minutes{}},
         std::tuple{std::u8string(u8"h"), std::chrono::hours{}});
}

auto
rcp::symcam::st::to_string(StreamClock::duration const& duration)
  -> std::string {
  std::ostringstream oss;
  oss << duration;
  return oss.str();
}

auto
rcp::symcam::st::parse_timestamp(std::u8string_view const& str)
  -> std::optional<StreamClock::timestamp_t> {

  static auto const delta = std::u8string{u8"Δ"};
  if (!str.empty()) {
    if (str.starts_with(delta)) {
      // a relative timestamp...looks like a duration after the Δ char
      auto const duration = parse_duration(str.substr(delta.size()));
      if (duration.has_value())
        return StreamClock::relative_time{duration.value()};
    } else {
      // a fixed (external) timestamp; interpreted as the time since
      // the StreamClock epoch (not the event log epoch!)
      auto const duration = parse_duration(str);
      if (duration.has_value()) {
        return StreamClock::external_time{duration.value()};
      }
    }
  }
  return std::nullopt;
}

auto
rcp::symcam::st::to_string(StreamClock::timestamp_t const& timestamp)
  -> std::string {
  std::ostringstream oss;
  std::visit(
    overloaded{
      [&](StreamClock::external_time const& ts) {
        oss << ts.time_since_epoch();
      },
      [&](StreamClock::relative_time const& ts) { oss << "Δ" << ts; }},
    timestamp);
  return oss.str();
}

auto
rcp::symcam::st::get_event(nlohmann::json const& event_js)
  -> std::unique_ptr<StreamStateEventBase<ExtendedState>> {
  if (!JSONValue::is_valid(event_js))
    throw JSONValueFormatError(event_js);
  auto tag = JSONValue::get_tag(event_js);
  if (tag == PoisonPill::class_tag)
    return get_event<PoisonPill>(event_js);
  if (tag == StartImaging::class_tag)
    return get_event<StartImaging>(event_js);
  if (tag == StopImaging::class_tag)
    return get_event<StopImaging>(event_js);
  if (tag == CreateSubarray::class_tag)
    return get_event<CreateSubarray>(event_js);
  if (tag == DestroySubarray::class_tag)
    return get_event<DestroySubarray>(event_js);
  if (tag == StartRecordEventStream::class_tag)
    return get_event<StartRecordEventStream>(event_js);
  if (tag == StopRecordEventStream::class_tag)
    return get_event<StopRecordEventStream>(event_js);
  if (tag == ImportFFTWWisdom::class_tag)
    return get_event<ImportFFTWWisdom>(event_js);
  if (tag == ExportFFTWWisdom::class_tag)
    return get_event<ExportFFTWWisdom>(event_js);
  if (tag == StartRecordDataStream::class_tag)
    return get_event<StartRecordDataStream>(event_js);
  if (tag == StopRecordDataStream::class_tag)
    return get_event<StopRecordDataStream>(event_js);
  if (tag == StartPreCalibrationFlagging::class_tag)
    return get_event<StartPreCalibrationFlagging>(event_js);
  if (tag == StopPreCalibrationFlagging::class_tag)
    return get_event<StopPreCalibrationFlagging>(event_js);
  throw EventTagError(tag.c_str());
}

auto
rcp::symcam::st::as_json(StreamStateEventBase<ExtendedState> const& event)
  -> nlohmann::json {
  std::string tag = event.tag();
  nlohmann::json result;
  if (tag == PoisonPill::class_tag)
    result = dynamic_cast<PoisonPill const&>(event);
  else if (tag == StartImaging::class_tag)
    result = dynamic_cast<StartImaging const&>(event);
  else if (tag == StopImaging::class_tag)
    result = dynamic_cast<StopImaging const&>(event);
  else if (tag == CreateSubarray::class_tag)
    result = dynamic_cast<CreateSubarray const&>(event);
  else if (tag == DestroySubarray::class_tag)
    result = dynamic_cast<DestroySubarray const&>(event);
  else if (tag == StartRecordEventStream::class_tag)
    result = dynamic_cast<StartRecordEventStream const&>(event);
  else if (tag == StopRecordEventStream::class_tag)
    result = dynamic_cast<StopRecordEventStream const&>(event);
  else if (tag == ImportFFTWWisdom::class_tag)
    result = dynamic_cast<ImportFFTWWisdom const&>(event);
  else if (tag == ExportFFTWWisdom::class_tag)
    result = dynamic_cast<ExportFFTWWisdom const&>(event);
  else if (tag == StartRecordDataStream::class_tag)
    result = dynamic_cast<StartRecordDataStream const&>(event);
  else if (tag == StopRecordDataStream::class_tag)
    result = dynamic_cast<StopRecordDataStream const&>(event);
  else if (tag == StartPreCalibrationFlagging::class_tag)
    result = dynamic_cast<StartPreCalibrationFlagging const&>(event);
  else if (tag == StopPreCalibrationFlagging::class_tag)
    result = dynamic_cast<StopPreCalibrationFlagging const&>(event);
  else
    throw EventTagError(tag.c_str());
  return result;
}

template class rcp::symcam::StreamEventLog<rcp::symcam::st::ExtendedState>;

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
