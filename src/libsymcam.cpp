// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "libsymcam.hpp"

#include "BsetChannelBaselinePolprodRegion.hpp"
#include "CFRegion.hpp"
#include "GridRegion.hpp"
#include "GridWeightsRegion.hpp"

#include <algorithm>
#include <rcp/FSamplesRegion.hpp>
#include <rcp/rcp.hpp>

using namespace rcp::symcam;

namespace rcp::symcam {

LogSink<Tag> log_symcam;

auto
init_log_sink(std::string_view const& name) -> void {
  log_symcam = LogSink<Tag>(name);
}

auto
samples_layout() -> PortableLayout<> const& {
  using md_t = FSamplesRegion;
  // Different variants for CPU and GPU do not exist as we're attempting to
  // allow the copy from CPU to GPU to be as simple as possible, meaning without
  // any reordering.
  static const auto add_constraints =
    [](L::LayoutConstraintRegistrar& registrar) {
      registrar.add_constraint(L::OrderingConstraint(
        {L::DimensionKind(md_t::axes_t::timestep),
         L::DimensionKind(md_t::axes_t::polarization),
         L::DimensionKind(md_t::axes_t::receiver),
         L::DimensionKind(md_t::axes_t::channel),
         LEGION_DIM_F},
        true /* contiguous */));
      if constexpr (!legion_is_unaware_of_samples_tiling) {
        // matching FBlock sample array bounds to this region requires tiling of
        // the channel and timestep axes
        registrar
          .add_constraint(L::TilingConstraint(
            L::DimensionKind(md_t::axes_t::timestep),
            DataPacketConfiguration::timesteps_tile_size))
          .add_constraint(
            L::TilingConstraint(L::DimensionKind(md_t::axes_t::channel), 1));
      }
    };
  static PortableLayout<> layout{
    {{LayoutStyle::CPU, add_constraints}, {LayoutStyle::GPU, add_constraints}},
    "samples layout"};
  return layout;
}

auto
visibilities_layout() -> PortableLayout<> const& {
  using cbp_t = BsetChannelBaselinePolprodRegion;
  static PortableLayout<> layout{
    {{LayoutStyle::CPU,
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(L::OrderingConstraint(
          {L::DimensionKind(cbp_t::axes_t::channel),
           L::DimensionKind(cbp_t::axes_t::baseline),
           L::DimensionKind(cbp_t::axes_t::polarization_product),
           LEGION_DIM_F},
          true /* contiguous */));
      }},
     {LayoutStyle::GPU,
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(L::OrderingConstraint(
          {L::DimensionKind(cbp_t::axes_t::polarization_product),
           L::DimensionKind(cbp_t::axes_t::baseline),
           L::DimensionKind(cbp_t::axes_t::channel),
           LEGION_DIM_F},
          true /* contiguous */));
      }}},
    "visibilities layout"};
  return layout;
}

auto
uvws_layout() -> PortableLayout<> const& {
  using bc_t = BaselineChannelRegion;

  static PortableLayout<> result{
    {{LayoutStyle::CPU,
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(L::OrderingConstraint(
          {L::DimensionKind(bc_t::axes_t::channel),
           L::DimensionKind(bc_t::axes_t::baseline),
           LEGION_DIM_F},
          true /* contiguous */));
      }},
     {LayoutStyle::GPU,
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(L::OrderingConstraint(
          {L::DimensionKind(bc_t::axes_t::baseline),
           L::DimensionKind(bc_t::axes_t::channel),
           LEGION_DIM_F},
          true /* contiguous */));
      }}},
    "UVWs layout"};
  return result;
}

auto
cf_layout() -> PortableLayout<> const& {

  using cf_t = CFRegion;

  static PortableLayout<> result{
    {{LayoutStyle::CPU,
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(L::OrderingConstraint(
          {L::DimensionKind(cf_t::axes_t::mueller),
           L::DimensionKind(cf_t::axes_t::y_major),
           L::DimensionKind(cf_t::axes_t::x_major),
           L::DimensionKind(cf_t::axes_t::index),
           L::DimensionKind(cf_t::axes_t::y_minor),
           L::DimensionKind(cf_t::axes_t::x_minor),
           LEGION_DIM_F},
          true /* contiguous */));
      }},
     {LayoutStyle::GPU,
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(L::OrderingConstraint(
          {L::DimensionKind(cf_t::axes_t::x_major),
           L::DimensionKind(cf_t::axes_t::y_major),
           L::DimensionKind(cf_t::axes_t::mueller),
           L::DimensionKind(cf_t::axes_t::index),
           L::DimensionKind(cf_t::axes_t::x_minor),
           L::DimensionKind(cf_t::axes_t::y_minor),
           LEGION_DIM_F},
          true /* contiguous */));
      }}},
    "CF values layout"};
  return result;
}

auto
grid_ordering() -> L::OrderingConstraint {
  using grid_t = GridRegion;
  return L::OrderingConstraint(
    {L::DimensionKind(grid_t::axes_t::x),
     L::DimensionKind(grid_t::axes_t::y),
     L::DimensionKind(grid_t::axes_t::stokes),
     L::DimensionKind(grid_t::axes_t::w_plane),
     L::DimensionKind(grid_t::axes_t::channel),
     LEGION_DIM_F},
    true);
}

auto
grid_layout() -> PortableLayout<OptionalReductionLayoutStyle> const& {
  // Optimized for access by (HPG) gridder; used on both CPU and GPU.
  static PortableLayout<OptionalReductionLayoutStyle> result{
    {{{LayoutStyle::CPU, false},
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(grid_ordering());
      }},
     {{LayoutStyle::CPU, true},
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(grid_ordering())
          .add_constraint(
            L::SpecializedConstraint(LEGION_AFFINE_REDUCTION_SPECIALIZE));
      }},
     {{LayoutStyle::GPU, false},
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(grid_ordering());
      }},
     {{LayoutStyle::GPU, true},
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(grid_ordering())
          .add_constraint(
            L::SpecializedConstraint(LEGION_AFFINE_REDUCTION_SPECIALIZE));
      }}},
    "grid layout"};
  return result;
}

auto
grid_weights_layout() -> PortableLayout<OptionalReductionLayoutStyle> const& {
  using gw_t = GridWeightsRegion;
  static const L::OrderingConstraint ordering{
    {L::DimensionKind(gw_t::axes_t::stokes),
     L::DimensionKind(gw_t::axes_t::w_plane),
     L::DimensionKind(gw_t::axes_t::channel),
     LEGION_DIM_F},
    true /* contiguous */};
  static PortableLayout<OptionalReductionLayoutStyle> result{
    {{{LayoutStyle::CPU, false},
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(ordering);
      }},
     {{LayoutStyle::CPU, true},
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(ordering).add_constraint(
          L::SpecializedConstraint(LEGION_AFFINE_REDUCTION_SPECIALIZE));
      }},
     {{LayoutStyle::GPU, false},
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(ordering);
      }},
     {{LayoutStyle::GPU, true},
      [](L::LayoutConstraintRegistrar& registrar) {
        registrar.add_constraint(ordering).add_constraint(
          L::SpecializedConstraint(LEGION_AFFINE_REDUCTION_SPECIALIZE));
      }}},
    "grid weights layout"};
  return result;
}

ReceiverSet::ReceiverSet(std::set<receiver_t> receivers)
  : m_receivers(std::move(receivers)) {
  if (std::ranges::any_of(
        m_receivers, [](auto&& rcv) { return !in_range(rcv); }))
    throw std::out_of_range("One or more receiver indexes is out of range");
}

ReceiverSet::ReceiverSet(std::initializer_list<receiver_t> init)
  : ReceiverSet(std::set<receiver_t>(init)) {
  if (std::ranges::any_of(
        m_receivers, [](auto&& rcv) { return !in_range(rcv); }))
    throw std::out_of_range("One or more receiver indexes is out of range");
}

auto
ReceiverSet::size() const -> std::size_t {
  return m_receivers.size();
}

auto
ReceiverSet::empty() const -> bool {
  return m_receivers.empty();
}

auto
ReceiverSet::receivers() const -> std::set<receiver_t> const& {
  return m_receivers;
}

auto
ReceiverSet::contains(receiver_t receiver) const -> bool {
  return m_receivers.contains(receiver);
}

auto
ReceiverSet::merge(ReceiverSet const& other) const& -> ReceiverSet {
  return ReceiverSet{*this}.merge(other);
}

auto
ReceiverSet::merge(ReceiverSet const& other) && -> ReceiverSet {
  std::ranges::copy(
    other.m_receivers, std::inserter(m_receivers, m_receivers.end()));
  return std::move(*this);
}

auto
ReceiverSet::operator==(ReceiverSet const& rhs) const -> bool {
  return m_receivers == rhs.m_receivers;
}

auto
ReceiverSet::operator!=(ReceiverSet const& rhs) const -> bool {
  return !(*this == rhs);
}

auto
ReceiverSet::all() -> ReceiverSet const& {
  static const ReceiverSet result = []() {
    std::vector<receiver_t> receivers(Configuration::num_receivers);
    std::iota(receivers.begin(), receivers.end(), 0);
    return std::set<receiver_t>(receivers.begin(), receivers.end());
  }();
  return result;
}

auto
ReceiverSet::none() -> ReceiverSet const& {
  static const ReceiverSet result;
  return result;
};

ReceiverPairs::ReceiverPairs(set_t pairs) : m_pairs(std::move(pairs)) {

  if (std::ranges::any_of(m_pairs, [](auto&& pair) {
        return pair[0] >= Configuration::num_receivers
               || pair[1] >= Configuration::num_receivers;
      }))
    throw std::out_of_range("One or more receiver indexes is out of range");
}

auto
ReceiverPairs::size() const -> std::size_t {
  return m_pairs.size();
}

auto
ReceiverPairs::empty() const -> bool {
  return m_pairs.empty();
}

auto
ReceiverPairs::pairs() const -> set_t {
  return m_pairs;
}

auto
ReceiverPairs::pairs_p() const -> set_t const* {
  return &m_pairs;
}

auto
ReceiverPairs::contains(pair_t const& pair) const -> bool {
  return m_pairs.contains(pair);
}

auto
ReceiverPairs::merge(ReceiverPairs const& other) const& -> ReceiverPairs {
  return ReceiverPairs(m_pairs).merge(other);
}

auto
ReceiverPairs::merge(ReceiverPairs const& other) && -> ReceiverPairs {
  std::ranges::copy(other, std::inserter(m_pairs, m_pairs.end()));
  return *this;
}

auto
ReceiverPairs::operator==(ReceiverPairs const& rhs) const -> bool {
  return m_pairs == rhs.m_pairs;
}

auto
ReceiverPairs::operator!=(ReceiverPairs const& rhs) const -> bool {
  return !(*this == rhs);
}

auto
ReceiverPairs::begin() const -> iterator_t {
  return {*this};
}

auto
ReceiverPairs::end() -> iterator_t {
  return {};
}

ReceiverPairsIterator::ReceiverPairsIterator(ReceiverPairs const& pairs)
  : m_current(pairs.pairs_p()->begin()), m_end(pairs.pairs_p()->end()) {}

auto
ReceiverPairsIterator::operator++() -> ReceiverPairsIterator& {
  ++m_current;
  return *this;
}
auto
ReceiverPairsIterator::operator++(int) -> ReceiverPairsIterator {
  ReceiverPairsIterator result{*this};
  ++(*this);
  return result;
}

auto
ReceiverPairsIterator::operator*() const -> ReceiverPairSetBase::pair_t {
  return *m_current;
}

auto
ReceiverPairsIterator::operator==(ReceiverPairsIterator const& rhs) const
  -> bool {
  return (m_current == rhs.m_current) || (at_end() && rhs.at_end());
}

auto
ReceiverPairsIterator::at_end() const -> bool {
  return m_current == m_end;
}

ReceiverSetOuterProduct::ReceiverSetOuterProduct(
  ReceiverSet const& left_and_right)
  : ReceiverSetOuterProduct(left_and_right, left_and_right) {}

ReceiverSetOuterProduct::ReceiverSetOuterProduct(
  ReceiverSet left, ReceiverSet right)
  : m_left(std::move(left)), m_right(std::move(right)) {}

auto
ReceiverSetOuterProduct::size() const -> std::size_t {
  return m_left.size() * m_right.size();
}

auto
ReceiverSetOuterProduct::empty() const -> bool {
  return size() == 0;
}

auto
ReceiverSetOuterProduct::left() const -> ReceiverSet const& {
  return m_left;
}

auto
ReceiverSetOuterProduct::right() const -> ReceiverSet const& {
  return m_right;
}

auto
ReceiverSetOuterProduct::pairs() const -> set_t {
  return {begin(), end()};
}

auto
ReceiverSetOuterProduct::contains(pair_t const& pair) const -> bool {
  return (m_left.contains(pair[0]) && m_right.contains(pair[1]))
         || (m_left.contains(pair[1]) && m_right.contains(pair[0]));
}

auto
ReceiverSetOuterProduct::merge(
  ReceiverSetOuterProduct const& other) const& -> ReceiverSetOuterProduct {
  return ReceiverSetOuterProduct(*this).merge(other);
}

auto
ReceiverSetOuterProduct::merge(
  ReceiverSetOuterProduct const& other) && -> ReceiverSetOuterProduct {
  m_left = std::move(m_left).merge(other.m_left);
  m_right = std::move(m_right).merge(other.m_right);
  return std::move(*this);
}

auto
ReceiverSetOuterProduct::operator==(ReceiverSetOuterProduct const& rhs) const
  -> bool {
  return pairs() == rhs.pairs();
}

auto
ReceiverSetOuterProduct::operator!=(ReceiverSetOuterProduct const& rhs) const
  -> bool {
  return !(*this == rhs);
}

auto
ReceiverSetOuterProduct::begin() const -> iterator_t {
  return {*this};
}

auto
ReceiverSetOuterProduct::end() -> iterator_t {
  return {};
}

ReceiverSetOuterProductIterator::ReceiverSetOuterProductIterator(
  ReceiverSetOuterProduct const& outer) {
  // NOLINTBEGIN(cppcoreguidelines-prefer-member-initializer)
  m_outer = &outer;
  m_row_index = 0;
  m_col_index = 0;
  if (!at_end() && !m_outer->contains({m_row_index, m_col_index}))
    operator++();

  // auto left_rcv = outer.left().receivers();
  // auto right_rcv = outer.right().receivers();
  // m_left_begin = left_rcv.begin();
  // m_left_current = m_left_begin;
  // m_left_end = left_rcv.end();
  // m_right_begin = right_rcv.begin();
  // m_right_current = m_right_begin;
  // m_right_end = right_rcv.end();
  // NOLINTEND(cppcoreguidelines-prefer-member-initializer)
}

auto
ReceiverSetOuterProductIterator::operator++()
  -> ReceiverSetOuterProductIterator& {
  do {
    if (++m_col_index > m_row_index) {
      m_col_index = 0;
      ++m_row_index;
    }
  } while (!at_end() && !m_outer->contains({m_row_index, m_col_index}));
  return *this;
}

auto
ReceiverSetOuterProductIterator::operator++(int)
  -> ReceiverSetOuterProductIterator {
  ReceiverSetOuterProductIterator result(*this);
  ++(*this);
  return result;
}

auto
ReceiverSetOuterProductIterator::operator--()
  -> ReceiverSetOuterProductIterator& {
  do {
    if (m_col_index-- == 0) {
      --m_row_index;
      m_col_index = m_row_index;
    }
  } while (!at_end() && !m_outer->contains({m_row_index, m_col_index}));
  return *this;
}

auto
ReceiverSetOuterProductIterator::operator--(int)
  -> ReceiverSetOuterProductIterator {
  ReceiverSetOuterProductIterator result(*this);
  --(*this);
  return result;
}

auto
ReceiverSetOuterProductIterator::operator+=(difference_type diff)
  -> ReceiverSetOuterProductIterator& {
  auto idx = baseline_to_receiver_pair(baseline() + diff);
  m_row_index = idx[0];
  m_col_index = idx[1];
  return *this;
}

auto
ReceiverSetOuterProductIterator::operator+(difference_type diff) const
  -> ReceiverSetOuterProductIterator {
  ReceiverSetOuterProductIterator result{*this};
  result += diff;
  return result;
}

auto
ReceiverSetOuterProductIterator::operator-=(difference_type diff)
  -> ReceiverSetOuterProductIterator& {
  auto idx = baseline_to_receiver_pair(baseline() - diff);
  m_row_index = idx[0];
  m_col_index = idx[1];
  return *this;
}

auto
ReceiverSetOuterProductIterator::operator-(difference_type diff) const
  -> ReceiverSetOuterProductIterator {
  ReceiverSetOuterProductIterator result{*this};
  result -= diff;
  return result;
}

auto
ReceiverSetOuterProductIterator::operator-(
  ReceiverSetOuterProductIterator const& other) const -> difference_type {
  return static_cast<difference_type>(baseline())
         - static_cast<difference_type>(other.baseline());
}

auto
ReceiverSetOuterProductIterator::operator*() const
  -> ReceiverPairSetBase::pair_t {
  return {m_row_index, m_col_index};
}

auto
ReceiverSetOuterProductIterator::operator[](difference_type diff) const
  -> ReceiverPairSetBase::pair_t {
  return *(*this + diff);
}

auto
ReceiverSetOuterProductIterator::operator==(
  ReceiverSetOuterProductIterator const& rhs) const -> bool {
  return ((m_row_index == rhs.m_row_index) && (m_col_index == rhs.m_col_index));
}

auto
ReceiverSetOuterProductIterator::operator<=>(
  ReceiverSetOuterProductIterator const& rhs) const -> std::strong_ordering {
  return baseline() <=> rhs.baseline();
}

auto
ReceiverSetOuterProductIterator::at_end() const -> bool {
  return m_row_index == Configuration::num_receivers;
}

auto
operator+(
  ReceiverSetOuterProductIterator::difference_type diff,
  ReceiverSetOuterProductIterator const& it)
  -> ReceiverSetOuterProductIterator {
  return it + diff;
}

auto
operator-(
  ReceiverSetOuterProductIterator::difference_type diff,
  ReceiverSetOuterProductIterator const& it)
  -> ReceiverSetOuterProductIterator {
  return it - diff;
}

ChannelSet::ChannelSet(std::vector<std::array<channel_t, 2>> const& intervals) {
  for (auto&& interval : intervals)
    add_in(interval);
}

ChannelSet::ChannelSet(std::initializer_list<std::array<channel_t, 2>> init) {
  for (auto&& interval : init)
    add_in(interval);
}

auto
ChannelSet::add_in(std::array<channel_t, 2> const& interval) -> void {
  // find the first of m_intervals whose right end is not less than the left end
  // of "interval"
  auto lb = std::lower_bound(
    m_intervals.begin(),
    m_intervals.end(),
    interval[0],
    [](auto&& ival, auto&& ch) {
      // add one on left so that contiguous intervals are merged
      return ival[1] + 1 < ch;
    });
  if (lb == m_intervals.end()) {
    // the left end of "interval" is beyond the right end of m_intervals, with a
    // gap
    m_intervals.push_back(interval);
  } else {
    // find the first of m_intervals whose left end is greater than the right
    // end of "interval"
    auto ub = std::upper_bound(
      lb,
      m_intervals.end(),
      interval[1] + 1, // add one so that contiguous intervals are merged
      [](auto&& ch, auto&& ival) { return ch < ival[0]; });
    *lb = {
      std::min((*lb)[0], interval[0]), std::max((*(ub - 1))[1], interval[1])};
    m_intervals.erase(lb + 1, ub);
  }
}

auto
ChannelSet::size() const -> std::size_t {
  std::size_t result{0};
  for (auto&& interval : m_intervals)
    result += interval[1] - interval[0] + 1;
  return result;
}

auto
ChannelSet::empty() const -> bool {
  return m_intervals.empty();
}

auto
ChannelSet::intervals() const -> std::vector<std::array<channel_t, 2>> const& {
  return m_intervals;
}

auto
ChannelSet::channels() const -> std::vector<channel_t> {
  std::vector<channel_t> result;
  for (auto&& interval : m_intervals)
    for (auto&& ch :
         std::views::iota(interval[0], static_cast<channel_t>(interval[1] + 1)))
      result.push_back(ch);
  return result;
}

auto
ChannelSet::contains(channel_t channel) const -> bool {
  return std::ranges::find_if(
           m_intervals,
           [=](auto&& interval) {
             return interval[0] <= channel && channel <= interval[1];
           })
         != m_intervals.end();
}

auto
ChannelSet::add(ChannelSet const& other) const& -> ChannelSet {
  return ChannelSet(*this).add(other);
}

auto
ChannelSet::add(ChannelSet const& other) && -> ChannelSet {
  for (auto&& interval : other.m_intervals)
    add_in(interval);
  return std::move(*this);
}

auto
ChannelSet::operator==(ChannelSet const& rhs) const -> bool {
  return m_intervals == rhs.m_intervals;
}

auto
ChannelSet::operator!=(ChannelSet const& rhs) const -> bool {
  return !(*this == rhs);
}

auto
ChannelSet::all(Configuration const& config) -> ChannelSet {
  return {
    {channel_t{0},
     static_cast<channel_t>(config.num_sample_channels_full_band - 1)}};
}

auto
ChannelSet::none() -> ChannelSet const& {
  static ChannelSet result;
  return result;
}

auto
ChannelSet::intersection(Configuration const& config) const -> ChannelSet {
  std::vector<std::array<channel_t, 2>> intervals;
  channel_t const ch_end =
    config.sample_channel_range_offset + config.sample_channel_range_size;
  std::optional<channel_t> first;
  for (auto&& ch :
       std::views::iota(config.sample_channel_range_offset, ch_end)) {
    if (contains(ch)) {
      first = first.value_or(ch);
    } else if (first) {
      intervals.push_back({*first, static_cast<channel_t>(ch - 1)});
      first.reset();
    }
  }
  if (first)
    intervals.push_back({*first, static_cast<channel_t>(ch_end - 1)});
  return {intervals};
}

std::mutex FFTWBase::mtx{};

} // namespace rcp::symcam

template <>
auto
rcp::get_log_sink<rcp::symcam::Tag>() -> LogSink<rcp::symcam::Tag>& {
  return rcp::symcam::log_symcam;
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
