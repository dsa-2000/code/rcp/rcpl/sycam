// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include <legion.h>

namespace rcp::symcam {

namespace field_ids {
enum : Legion::FieldID {
  baseline = 10,
  cf_value,
  visibilities_subarray_partition,
  visibilities_tile_partition,
  uvws_subarray_partition,
  uvws_image_partition,
  uvws_tile_partition,
  grid_pixel,
  grid_tile,
  forward_index,
  inverse_index,
  position,
  state,
  grid_weight,
  uvw,
  cf_index,
  grid_tile_mapping,
  vis_projection,
  sample_correlation,
  flag_correlation,
  visibility,
  weight,
  baseline_channel_projection,
  count,
  timestamp,
  event_log,
  recording_handle,
  fftw_proxy,
};
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
