// Copyright 2023-2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "ExportGrid.hpp"
#include <rcp/Mapper.hpp>
#include <rcp/rcp.hpp>

using namespace rcp::symcam;

namespace rcp::symcam {

/*! \brief task to export grids
 */
struct ExportGridTask : public SerialOnlyTaskMixin<ExportGridTask, void> {

  using grid_t = GridRegion;
  using gmd_t = GridAndWeightMetadataRegion;

  /*! \brief task name */
  static constexpr const char* task_name = "ExportGrid";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief region indexes */
  enum { grid_metadata_region_index = 0, grid_region_index, num_regions };

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> result_t {

    assert(regions.size() == num_regions);
    assert(task->arglen == sizeof(ExportGrid::Args));

    ExportGrid::Args const& args =
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      *reinterpret_cast<ExportGrid::Args const*>(task->args);

    auto gmd_pt = gmd_t::rect_t{regions[grid_metadata_region_index]}.lo;
    auto const grid_count = gmd_t::values<LEGION_READ_ONLY>(
      regions[grid_metadata_region_index], CountField{})[gmd_pt];
    auto const grid_timestamp = gmd_t::values<LEGION_READ_ONLY>(
      regions[grid_metadata_region_index], TimestampField{})[gmd_pt];

    grid_t::rect_t const bounds(regions[grid_region_index]);
    using b = std::tuple<grid_t::coord_t, grid_t::coord_t>;
    using t = StreamClock::duration::rep;
    log().info<char const*, int, t, t, b, b, b, b, b>(
      "export",
      {"name", args.name.data()},
      {"subarray", gmd_pt[dim(gmd_t::axes_t::subarray)]},
      {"ts", t(grid_timestamp.time_since_epoch().count())},
      {"interval", t(args.vis_interval.count() * grid_count)},
      {"x",
       b(bounds.lo[dim(grid_t::axes_t::x)], bounds.hi[dim(grid_t::axes_t::x)])},
      {"y",
       b(bounds.lo[dim(grid_t::axes_t::y)], bounds.hi[dim(grid_t::axes_t::y)])},
      {"ch",
       b(bounds.lo[dim(grid_t::axes_t::channel)],
         bounds.hi[dim(grid_t::axes_t::channel)])},
      {"sto",
       b(bounds.lo[dim(grid_t::axes_t::stokes)],
         bounds.hi[dim(grid_t::axes_t::stokes)])},
      {"w-plane",
       b(bounds.lo[dim(grid_t::axes_t::w_plane)],
         bounds.hi[dim(grid_t::axes_t::w_plane)])});
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    registrar.set_leaf();
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

L::TaskID ExportGridTask::task_id;

ExportGrid::ExportGrid(
  L::Context ctx,
  L::Runtime* rt,
  Configuration const& config,
  ImagingConfiguration const& img_config) {

  assert(img_config.im.name.size() < m_args.name.size());
  auto namelen = std::min(m_args.name.size() - 1, img_config.im.name.size());
  std::strncpy(m_args.name.data(), img_config.im.name.data(), namelen);
  m_args.name[namelen] = '\0';
  m_args.vis_interval = config.visibility_interval();
}

ExportGrid::ExportGrid(
  L::Context /*ctx*/, L::Runtime* /*rt*/, Serialized const& serialized)
  : m_args(serialized.m_args) {}

auto
ExportGrid::serialized(L::Context /*ctx*/, L::Runtime* /*rt*/) const noexcept
  -> Serialized {
  return {m_args};
}

auto
ExportGrid::child_requirements(
  L::Context /*ctx*/, L::Runtime* /*rt*/) const noexcept -> ChildRequirements {
  return {};
}

auto
ExportGrid::init_launcher() -> L::IndexTaskLauncher {

  auto result = L::IndexTaskLauncher(
    ExportGridTask::task_id,
    L::Domain(),
    L::UntypedBuffer(&m_args, sizeof(m_args)),
    L::ArgumentMap());
  result.region_requirements.resize(ExportGridTask::num_regions);
  return result;
}

auto
ExportGrid::update_launcher(
  L::Context ctx,
  L::Runtime* rt,
  PartitionPair<gmd_t::LogicalPartition> const& grid_metadata,
  sarray<grid_t::LogicalRegion> const& grids,
  unsigned subarray_slot,
  L::IndexTaskLauncher& launcher) -> void {

  m_args.subarray = subarray_slot;
  auto grid = grids.at(subarray_slot);
  auto ip = grid_t::create_partition_by_slicing(
    ctx,
    rt,
    grid.get_index_space(),
    {{grid_t::axes_t::channel, 1}, {grid_t::axes_t::stokes, 1}});
  launcher.launch_domain = rt->get_index_partition_color_space(ip);

  launcher.region_requirements[ExportGridTask::grid_metadata_region_index] =
    subarray_grid_metadata(rt, subarray_slot, grid_metadata)
      .requirement(
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        StaticFields{CountField{}, TimestampField{}},
        std::nullopt);
  launcher.region_requirements[ExportGridTask::grid_region_index] =
    grid_t::LogicalPartition{rt->get_logical_partition(ctx, grid, ip)}
      .requirement(
        0,
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        grid,
        StaticFields{GridPixelField{}});
}

auto
ExportGrid::launch(
  L::Context ctx,
  L::Runtime* rt,
  PartitionPair<gmd_t::LogicalPartition> const& grid_metadata,
  sarray<grid_t::LogicalRegion> const& grids,
  st::subarray_predicates_array_t const& predicates) -> void {

  auto is_imaging_end = predicate_slice_subarray_predicates(
    predicates, st::SubarrayStreamPredicate::is_imaging_end);

  auto launcher = init_launcher();
  for (auto&& subarray_slot : subarray_slots()) {
    update_launcher(ctx, rt, grid_metadata, grids, subarray_slot, launcher);
    launcher.predicate = is_imaging_end(subarray_slot);
    rt->execute_index_space(ctx, launcher);
  }
}

auto
ExportGrid::launch(
  L::Context ctx,
  L::Runtime* rt,
  PartitionPair<gmd_t::LogicalPartition> const& grid_metadata,
  sarray<grid_t::LogicalRegion> const& grids,
  st::AllSubarraysQuery const& state_query) -> void {

  auto do_imaging_stop = state_query.do_imaging_stop();

  auto launcher = init_launcher();
  for (auto&& subarray_slot : filtered_subarray_slots(do_imaging_stop)) {
    update_launcher(ctx, rt, grid_metadata, grids, subarray_slot, launcher);
    rt->execute_index_space(ctx, launcher);
  }
}

auto
ExportGrid::preregister_tasks(L::TaskID tid) -> void {
  PortableTask<ExportGridTask>::preregister_task_variants(tid);
}

auto
ExportGrid::register_tasks(L::Runtime* rt, L::TaskID tid) -> void {
  PortableTask<ExportGridTask>::register_task_variants(rt, tid);
}

auto
ExportGrid::subarray_grid_metadata(
  L::Runtime* rt,
  int subarray,
  PartitionPair<gmd_t::LogicalPartition> const& part)
  -> RegionPair<gmd_t::LogicalRegion> {
  return {
    rt->get_logical_subregion_by_color<gmd_t::dim, gmd_t::coord_t>(
      part.partition(), L::Point<1, int>{subarray}),
    part.parent(rt)};
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
