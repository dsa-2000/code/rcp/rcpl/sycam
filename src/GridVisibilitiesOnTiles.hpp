// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include "libsymcam.hpp"

#include "BaselineChannelRegion.hpp"
#include "BsetChannelBaselinePolprodRegion.hpp"
#include "CFRegion.hpp"
#include "GridRegion.hpp"
#include "GridTileRegion.hpp"
#include "GridWeightsRegion.hpp"
#include "MuellerIndexRegion.hpp"

#include <atomic>
#include <variant>

namespace rcp::symcam {

struct GriddingProjection : public L::ProjectionFunctor {

  GriddingProjection() = default;

  using L::ProjectionFunctor::project;

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::ProjectionID projection_id;

  auto
  project(
    L::LogicalRegion /*region*/,
    L::DomainPoint const& /*poiht*/,
    L::Domain const& /*domain*/) -> L::LogicalRegion override;

  auto
  project(
    L::LogicalPartition upper_bound,
    L::DomainPoint const& point,
    L::Domain const& launch_domain) -> L::LogicalRegion override;

  // NOLINTNEXTLINE(modernize-use-nodiscard)
  auto
  get_depth() const -> unsigned override;

  // NOLINTNEXTLINE(modernize-use-nodiscard)
  auto
  is_functional() const -> bool override;
};

/*! concept for types with addition and summation operators */
template <typename T>
concept Summable = requires(T tx, T ty) {
  tx + ty;
  tx += ty;
};

/*! Legion ReductionOp for Summable values
 *
 * This class is defined for any type with addition and summation operators. It
 * allows for accumulation of values for types not built in to Legion.
 */
template <Summable T>
struct SummableReduction {
public:
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::ReductionOpID REDOP_ID; /*!< operator id */

  using LHS = T; /*!< Summable type */
  using RHS = T; /*!< Summable type */

  /*! add two values */
  template <bool EXCLUSIVE>
  inline void
  add(LHS& lhs, RHS rhs) const {
    if constexpr (EXCLUSIVE) {
      lhs += rhs;
    } else {
      auto lhs_ref = std::atomic_ref(lhs);
      LHS newl;
      // NOLINTBEGIN(cppcoreguidelines-avoid-do-while)
      do {
        newl = lhs_ref;
      } while (!lhs_ref.compare_exchange_weak(newl, newl + rhs));
    }
    // NOLINTEND(cppcoreguidelines-avoid-do-while)
  }

  /*! required function for ReductionOp */
  template <bool EXCLUSIVE>
  void
  apply(LHS& lhs, RHS rhs) const {
    add<EXCLUSIVE>(lhs, rhs);
  }

  static inline RHS const identity{}; /*< identity value */

  /*! required function for ReductionOp */
  template <bool EXCLUSIVE>
  void
  fold(RHS& rhs1, RHS rhs2) const {
    add<EXCLUSIVE>(rhs1, rhs2);
  }
};

template <Summable T>
L::ReductionOpID SummableReduction<T>::REDOP_ID;

/*! \brief alias for ReductionOp of accval<.> values
 *
 * Type depends on value of Configuration::use_compensated_grid_summation.
 */
template <bool CompensatedSum, typename T>
using AccvalReduction =
  std::conditional_t<CompensatedSum, SummableReduction<T>, L::SumReduction<T>>;

/*! alias for Legion ReductionOp on GridPixelField::rvalue_t */
template <bool CompensatedSum>
using GridReduction = AccvalReduction<CompensatedSum, GridPixelField::rvalue_t>;

/*! alias for Legion ReductionOp on WeightsRegionMetadata::value_t */
template <bool CompensatedSum>
using WeightsReduction =
  AccvalReduction<CompensatedSum, GridWeightField::rvalue_t>;

// extern template instance declarations -- these aren't really needed unless
// Configuration::use_compensated_grid_summation is asserted, but for now I
// don't care
extern template struct SummableReduction<GridPixelField::rvalue_t>;
extern template struct SummableReduction<GridWeightField::rvalue_t>;

/*! \brief launcher for task to grid visibilities
 */
class GridVisibilitiesOnTiles {
public:
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using grid_t = GridRegion;
  using gw_t = GridWeightsRegion;
  using cf_t = CFRegion;
  using mi_t = MuellerIndexRegion;
  using bc_t = BaselineChannelRegion;

  struct GriddingArgs {
    std::array<char, Configuration::name_array_size> name;
    int vector_length;
    StokesMask stokes_mask;
  };

  using cfv_region_t = std::conditional_t<
    partition_cf_regions,
    cf_t::LogicalPartition,
    RegionPair<cf_t::LogicalRegion>>;

  static constexpr bool use_reduction_privileges_on_grid_pixels_and_weights =
    false;

private:
  /*! MainTask arguments */
  GriddingArgs m_gridding_args{};

  /*! grid partition */
  grid_t::LogicalPartition m_grid_partition;

  /*! grid weights partition */
  gw_t::LogicalPartition m_weights_partition;

  /*! sparse Mueller matrix index region */
  mi_t::LogicalRegion m_mueller_indexes;

  L::IndexTaskLauncher m_launcher;

public:
  GridVisibilitiesOnTiles(
    L::Context ctx,
    L::Runtime* rt,
    std::array<char, Configuration::name_array_size> const& name,
    int vector_length,
    StokesMask stokes_mask,
    grid_t::LogicalPartition grid_partition,
    gw_t::LogicalPartition weights_partition,
    mi_t::LogicalRegion mueller_region);

  GridVisibilitiesOnTiles(GridVisibilitiesOnTiles const&) = default;

  GridVisibilitiesOnTiles(GridVisibilitiesOnTiles&&) = default;

  auto
  operator=(GridVisibilitiesOnTiles const&)
    -> GridVisibilitiesOnTiles& = default;

  auto
  operator=(GridVisibilitiesOnTiles&&) -> GridVisibilitiesOnTiles& = default;

  ~GridVisibilitiesOnTiles() = default;

  void
  launch(
    L::Context ctx,
    L::Runtime* rt,
    PartitionPair<cbp_t::LogicalPartition> const& visibility_partition,
    PartitionPair<cbp_t::LogicalPartition> const& visibility_weights_partition,
    PartitionPair<bc_t::LogicalPartition> const& uvw_partition,
    cfv_region_t const& cf_array_region,
    L::Predicate pred = L::Predicate::TRUE_PRED);

  static auto
  preregister_tasks(L::TaskID task_id) -> void;

  static auto
  register_tasks(L::Runtime* rt, L::TaskID task_id) -> void;

  static auto
  grid_precision_type_info() -> std::type_info const&;

  static auto
  grid_uses_compensated_summation() -> bool;

  static auto
  hpg_intermediate_grid_precision_type_info() -> std::type_info const&;

  static auto
  hpg_intermediate_weights_precision_type_info() -> std::type_info const&;

  static auto
  hpg_intermediate_weights_uses_compensated_summation() -> bool;
};
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
