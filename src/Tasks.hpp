// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "Configuration.hpp"
#include <rcp/GetConfig.hpp>
#include <rcp/GetEpoch.hpp>

namespace rcp::symcam {

enum TaskIDs : L::TaskID {
  radio_camera_task_id = 1,
  get_epoch_task_id,
  capture_fsamples_task_id,
  cross_correlate_task_id,
  normalize_visibilities_task_id,
  compute_visibility_weights_task_id,
  load_cuda_libraries_task_id,
  get_config_task_id,
  compute_baselines_task_id,
  get_receiver_positions_task_id,
  cf_domains_task_id,
  gridding_task_id,
  compute_uvw_task_id,
  cf_init_task_id,
  cf_index_task_id,
  report_fill_fraction_task_id,
  export_grid_task_id,
  imaging_task_id,
  eventlog_record_events_task_id,
  eventlog_rollup_task_id,
  eventlog_query_task_id,
  eventlog_query_all_task_id,
  eventlog_current_state_task_id,
  update_buffer_tracking_task_id,
  compute_partitions_by_subarray_task_id,
  compute_partitions_by_tile_task_id,
  compute_tile_mappings_task_id,
  fill_uvw_projection_field_task_id,
  fill_vis_projection_field_task_id,
  grid_make_fft_plan_task_id,
  grid_fft_task_id,
  grid_destroy_fft_plan_task_id,
  record_visibilities_task_id,
  precal_flagging_copy_task_id,
  precal_flagging_subarray_task_id,
  precal_flagging_task_id,
  fftw_import_wisdom_task_id,
  fftw_export_wisdom_task_id,
  vis_processing_task_id,
};
using GetEpoch = GetEpoch_<get_epoch_task_id>;
using GetConfig = GetConfig_<ConfigurationSerdez, get_config_task_id>;
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
