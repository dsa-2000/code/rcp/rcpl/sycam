// Copyright 2023-2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "ComputeVisibilityWeights.hpp"

#include "libsymcam.hpp"

#include <cstdint>
#include <rcp/rcp.hpp>
#include <vector>

using namespace rcp::symcam;

namespace rcp::symcam {

/*! task to compute visibility weights
 */
struct ComputeVisibilityWeightsTask
  : public DefaultKokkosTaskMixin<
      ComputeVisibilityWeightsTask,
      ComputeVisibilityWeights::result_t> {

  using samples_t = FSamplesRegion;

  using cbp_t = BsetChannelBaselinePolprodRegion;

  /*! \brief task name */
  static constexpr char const* task_name = "ComputeVisibilityWeights";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief region indexes */
  enum { samples_region_index = 0, visibilities_region_index, num_regions };

#ifdef RCP_USE_KOKKOS
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* /*task*/,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    assert(regions.size() == num_regions);

    cbp_t::rect_t vis_bounds = regions[visibilities_region_index];
    assert(vis_bounds.lo[dim(cbp_t::axes_t::polarization_product)] == 0);
    assert(
      vis_bounds.hi[dim(cbp_t::axes_t::polarization_product)]
      == Configuration::num_polarization_products - 1);
    auto const vis_wgts = cbp_t::view<execution_space, LEGION_WRITE_ONLY>(
      regions[visibilities_region_index], FlagCorrelationField{});

    auto const& smp_wgts_region = regions[samples_region_index];
    samples_t::rect_t smp_bounds = smp_wgts_region;

    auto num_baselines = vis_bounds.hi[dim(cbp_t::axes_t::baseline)]
                         - vis_bounds.lo[dim(cbp_t::axes_t::baseline)] + 1;
    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();
    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);

    result_t total_weight{0};
    using team_policy_t = TeamPolicy<execution_space>;

    if constexpr (!legion_is_unaware_of_samples_tiling) {
      for (L::PieceIteratorT<samples_t::dim, samples_t::coord_t> pit(
             smp_wgts_region, FWeightField::field_id, true);
           pit();
           pit++) {
        auto const swgts = samples_t::view<execution_space, LEGION_READ_ONLY>(
          smp_wgts_region, FWeightField{}, *pit);
        auto const ts_lo = pit->lo[dim(samples_t::axes_t::timestep)];
        auto const ts_hi = pit->hi[dim(samples_t::axes_t::timestep)];
        auto const ch_lo = pit->lo[dim(samples_t::axes_t::channel)];
        auto const num_ch =
          pit->hi[dim(samples_t::axes_t::channel)] - ch_lo + 1;
        auto const num_chpp = num_ch * Configuration::num_polarization_products;
        K::parallel_reduce(
          task_name,
          team_policy_t(work_space, num_baselines * num_chpp, K::AUTO),
          KOKKOS_LAMBDA(
            team_policy_t::member_type const& team,
            result_t& total_weight_team) {
            auto idx = team.league_rank();
            auto const bl =
              idx / num_chpp + vis_bounds.lo[dim(cbp_t::axes_t::baseline)];
            auto const [rcvX, rcvY] = baseline_to_receiver_pair(bl);
            idx %= num_chpp;
            auto const ch =
              idx / Configuration::num_polarization_products + ch_lo;
            auto const pp = idx % Configuration::num_polarization_products;
            auto vwgts = KX::subview(vis_wgts, ch, bl, pp);
            K::single(K::PerTeam(team), [&]() {
              vwgts() = typename decltype(vwgts)::value_type{0};
            });
            team.team_barrier();
            auto swgtsX = KX::subview(
              swgts, ch, rcvX, cbp_t::polarization1_index(pp), K::ALL);
            auto swgtsY = KX::subview(
              swgts, ch, rcvY, cbp_t::polarization0_index(pp), K::ALL);

            result_t wgt{0};
            K::parallel_reduce(
              K::TeamThreadRange(team, ts_lo, ts_hi + 1),
              [&](auto ts, result_t& wgt_l) {
                wgt_l += swgtsX(ts) * swgtsY(ts);
              },
              wgt);

            K::single(K::PerTeam(team), [&]() {
              vwgts() += wgt;
              total_weight_team += wgt;
            });
          },
          total_weight);
      }
    } else {
      auto const* swgts =
        samples_t::values<LEGION_READ_ONLY>(smp_wgts_region, FWeightField{})
          .ptr(smp_bounds.lo);
      auto num_receivers = smp_bounds.hi[dim(samples_t::axes_t::receiver)]
                           - smp_bounds.lo[dim(samples_t::axes_t::receiver)]
                           + 1;
      auto num_timesteps = smp_bounds.hi[dim(samples_t::axes_t::timestep)]
                           - smp_bounds.lo[dim(samples_t::axes_t::timestep)]
                           + 1;
      assert(
        num_timesteps % DataCaptureConfiguration::timesteps_tile_size == 0);
      auto num_timestep_blocks =
        num_timesteps / DataCaptureConfiguration::timesteps_tile_size;
      auto tile_offset = KOKKOS_LAMBDA(
        samples_t::coord_t const ch,
        samples_t::coord_t const rcv,
        samples_t::coord_t const pol,
        samples_t::coord_t const ts) {
        return (ts % DataCaptureConfiguration::timesteps_tile_size)
               + DataCaptureConfiguration::timesteps_tile_size
                   * (pol + DataPacketConfiguration::num_polarizations * (rcv + num_receivers * ((ts / DataCaptureConfiguration::timesteps_tile_size) + num_timestep_blocks * ch)));
      };
      auto const ch_lo = smp_bounds.lo[dim(samples_t::axes_t::channel)];
      auto const num_ch =
        smp_bounds.hi[dim(samples_t::axes_t::channel)] - ch_lo + 1;
      auto const num_chpp = num_ch * Configuration::num_polarization_products;
      using wgtview_t = K::View<
        FWeightField::value_t const*,
        execution_space,
        K::MemoryTraits<K::Unmanaged>>;
      K::parallel_reduce(
        task_name,
        team_policy_t(work_space, num_baselines * num_chpp, K::AUTO),
        KOKKOS_LAMBDA(
          team_policy_t::member_type const& team, result_t& total_weight_team) {
          auto idx = team.league_rank();
          auto const bl =
            idx / num_chpp + vis_bounds.lo[dim(cbp_t::axes_t::baseline)];
          auto const [rcvX, rcvY] = baseline_to_receiver_pair(bl);
          idx %= num_chpp;
          auto const ch =
            idx / Configuration::num_polarization_products + ch_lo;
          auto const pp = idx % Configuration::num_polarization_products;
          auto vwgts = KX::subview(vis_wgts, ch, bl, pp);
          K::single(K::PerTeam(team), [&]() {
            vwgts() = typename decltype(vwgts)::value_type{0};
          });
          team.team_barrier();
          for (auto ts_lo = smp_bounds.lo[dim(samples_t::axes_t::timestep)];
               ts_lo <= smp_bounds.hi[dim(samples_t::axes_t::timestep)];
               ts_lo += DataCaptureConfiguration::timesteps_tile_size) {
            auto offX = tile_offset(
              ch - ch_lo, rcvX, cbp_t::polarization1_index(pp), ts_lo);
            auto swgtsX = wgtview_t(
              swgts + offX, DataCaptureConfiguration::timesteps_tile_size);
            auto offY = tile_offset(
              ch - ch_lo, rcvY, cbp_t::polarization0_index(pp), ts_lo);
            auto swgtsY = wgtview_t(
              swgts + offY, DataCaptureConfiguration::timesteps_tile_size);
            result_t wgt{0};
            K::parallel_reduce(
              K::TeamThreadRange(
                team, 0, DataCaptureConfiguration::timesteps_tile_size),
              [&](auto ts, result_t& wgt_l) {
                wgt_l += swgtsX(ts) * swgtsY(ts);
              },
              wgt);
            K::single(K::PerTeam(team), [&]() {
              vwgts() += wgt;
              total_weight_team += wgt;
            });
          }
        },
        total_weight);
    }
    return total_weight;
  }
#else  // !RCP_USE_KOKKOS
  static auto
  serial_body(
    L::Task const* /*task*/,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> result_t {

    assert(regions.size() == 2);
    auto& vis_wgts_region = regions[visibilities_region_index];
    L::Rect<cbp_t::dim, coord_t> vis_wgts_bounds = vis_wgts_region;
    auto const vis_wgts =
      cbp_t::values<LEGION_READ_WRITE>(vis_wgts_region, FlagCorrelationField{});

    result_t total_weight = 0;
    auto& smp_wgts_region = regions[samples_region_index];
    samples_t::rect_t smp_wgts_bounds = smp_wgts_region;
    auto const smp_wgts =
      samples_t::values<LEGION_READ_ONLY>(smp_wgts_region, FWeightField{});
    for (auto&& ch : range(smp_wgts_bounds, samples_t::axes_t::channel))
      for (auto rcvX = smp_wgts_bounds.lo[dim(samples_t::axes_t::receiver)],
                bl = coord_t(0);
           rcvX <= smp_wgts_bounds.hi[dim(samples_t::axes_t::receiver)];
           ++rcvX)
        for (auto rcvY = smp_wgts_bounds.lo[dim(samples_t::axes_t::receiver)];
             rcvY <= rcvX;
             ++rcvY, ++bl)
          for (auto&& polY :
               range(smp_wgts_bounds, samples_t::axes_t::polarization))
            for (auto&& polX :
                 range(smp_wgts_bounds, samples_t::axes_t::polarization)) {
              coord_t p[]{
                ch, bl, cbp_t::polarization_product_index({polX, polY})};
              auto& vw = vis_wgts[L::Point<cbp_t::dim, coord_t>(p)];
              vw = FlagCorrelationField::value_t{0};
              // FIXME: use blas dot product, something like
              // vis_wgts[{ ch, baseline, polY, polX}] =
              //  dot(
              //     smp_wgts.ptr({ch, rcvX, polX, 0}),
              //     smp_wgts.ptr({ch, rcvY, polY, 0}));
              for (auto&& ts :
                   range(smp_wgts_bounds, samples_t::axes_t::timestep)) {
                auto w = smp_wgts[{ch, rcvX, polX, ts}]
                         * smp_wgts[{ch, rcvY, polY, ts}];
                vw += w;
                total_weight += w;
              }
            }
    return total_weight;
  }
#endif // RCP_USE_KOKKOS

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));

    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar
      .add_layout_constraint_set(
        samples_region_index,
        samples_layout().constraint_id(traits::layout_style).value())
      .add_layout_constraint_set(
        visibilities_region_index,
        visibilities_layout().constraint_id(traits::layout_style).value());
    L::Runtime::preregister_task_variant<result_t, body<Variant>>(
      registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));

    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar
      .add_layout_constraint_set(
        samples_region_index,
        samples_layout().constraint_id(rt, traits::layout_style).value())
      .add_layout_constraint_set(
        visibilities_region_index,
        visibilities_layout().constraint_id(rt, traits::layout_style).value());
    rt->register_task_variant<result_t, body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID ComputeVisibilityWeightsTask::task_id;

} // end namespace rcp::symcam

ComputeVisibilityWeights::ComputeVisibilityWeights(
  L::Context ctx,
  L::Runtime* rt,
  samples_t::index_space_t samples_index_space,
  cbp_t::index_space_t visibilities_index_space) {

  assert([&]() {
    auto samples_domain = rt->get_index_space_domain(samples_index_space);
    auto visibilities_domain =
      rt->get_index_space_domain(visibilities_index_space);
    // the following reflect limitations of the implementation, not the
    // partitioning
    return samples_domain.dense() && visibilities_domain.dense();
  }());

  L::IndexSpaceT<2, coord_t> cs; // common color space of following partitions

  m_samples_index_partition = samples_t::create_partition_by_slicing(
    ctx,
    rt,
    samples_index_space,
    {{samples_t::axes_t::channel,
      DataPacketConfiguration::num_channels_per_packet}});

  // visibilities index partition
  m_visibilities_index_partition = cbp_t::create_partition_by_slicing(
    ctx,
    rt,
    visibilities_index_space,
    {{cbp_t::axes_t::channel,
      DataPacketConfiguration::num_channels_per_packet}});
}

auto
ComputeVisibilityWeights::launch(
  L::Context ctx,
  L::Runtime* rt,
  samples_t::LogicalRegion samples,
  cbp_t::LogicalRegion visibilities,
  L::Predicate pred) -> L::FutureMap {

  static result_t const zero{0};

  auto launcher = L::IndexTaskLauncher(
    ComputeVisibilityWeightsTask::task_id,
    rt->get_index_partition_color_space(m_samples_index_partition),
    L::UntypedBuffer(),
    L::ArgumentMap());

  launcher.region_requirements.resize(
    ComputeVisibilityWeightsTask::num_regions);
  launcher.region_requirements
    [ComputeVisibilityWeightsTask::visibilities_region_index] =
    cbp_t::LogicalPartition{
      rt->get_logical_partition(visibilities, m_visibilities_index_partition)}
      .requirement(
        0,
        LEGION_WRITE_ONLY,
        LEGION_EXCLUSIVE,
        visibilities,
        StaticFields{FlagCorrelationField{}});
  launcher
    .region_requirements[ComputeVisibilityWeightsTask::samples_region_index] =
    samples_t::LogicalPartition{
      rt->get_logical_partition(samples, m_samples_index_partition)}
      .requirement(
        0,
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        samples,
        StaticFields{FWeightField{}});
  launcher.predicate = pred;
  launcher.set_predicate_false_result(L::UntypedBuffer(&zero, sizeof(zero)));
  return rt->execute_index_space(ctx, launcher);
}

auto
ComputeVisibilityWeights::preregister_tasks(L::TaskID tid) -> void {
  PortableTask<ComputeVisibilityWeightsTask>::preregister_task_variants(tid);
}

auto
ComputeVisibilityWeights::register_tasks(L::Runtime* rt, L::TaskID tid)
  -> void {
  PortableTask<ComputeVisibilityWeightsTask>::register_task_variants(rt, tid);
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
