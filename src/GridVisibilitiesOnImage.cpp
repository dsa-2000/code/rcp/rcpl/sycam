// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "GridVisibilitiesOnImage.hpp"
#include "DataPartitionsRegion.hpp"
#include "EventProcessing.hpp"
#include "GridTileRegion.hpp"
#include "GridVisibilitiesOnTiles.hpp"

#include <bits/ranges_algo.h>
#include <default_mapper.h>
#include <legion/legion_config.h>
#include <legion/legion_domain.h>
#include <legion/legion_types.h>
#include <log.hpp>
#include <optional>
#include <ranges>
#include <rcp/Mapper.hpp>
#include <stdexcept>

using namespace rcp::symcam;

namespace rcp::symcam {

/*! task to CF index space domains by CF index value  */
struct CFDomainsTask : public SerialOnlyTaskMixin<CFDomainsTask, L::Domain> {

  using cf_t = CFRegion;
  using bc_t = BaselineChannelRegion;

  static constexpr char const* task_name = "CFDomainsTask";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  static auto
  serial_body(
    L::Task const* /*task*/,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> result_t {

    assert(regions.size() == 1);
    auto constexpr coord_min = std::numeric_limits<cf_t::coord_t>::min();
    auto constexpr coord_max = std::numeric_limits<cf_t::coord_t>::max();
    std::vector<cf_t::rect_t> rects;
    cf_t::rect_t rect;
    rect.lo[dim(cf_t::axes_t::mueller)] = rect.lo[dim(cf_t::axes_t::x_major)] =
      rect.lo[dim(cf_t::axes_t::x_minor)] =
        rect.lo[dim(cf_t::axes_t::y_major)] =
          rect.lo[dim(cf_t::axes_t::y_minor)] = coord_min;
    rect.hi[dim(cf_t::axes_t::mueller)] = rect.hi[dim(cf_t::axes_t::x_major)] =
      rect.hi[dim(cf_t::axes_t::x_minor)] =
        rect.hi[dim(cf_t::axes_t::y_major)] =
          rect.hi[dim(cf_t::axes_t::y_minor)] = coord_max;
    // outer loop over affine pieces
    for (auto piece = L::PieceIteratorT<bc_t::dim, bc_t::coord_t>(
           regions[0], CFIndexField::field_id, true);
         piece();
         piece++) {
      auto const cf_indexes =
        bc_t::values<LEGION_READ_ONLY>(regions[0], CFIndexField{}, *piece);
      // inner loop over points in affine piece
      for (auto pir = L::PointInRectIterator(*piece); pir(); pir++) {
        rect.lo[dim(cf_t::axes_t::index)] = rect.hi[dim(cf_t::axes_t::index)] =
          cf_indexes[*pir];
        rects.push_back(rect);
      }
    }
    auto is = rt->create_index_space(ctx, rects);
    auto result = rt->get_index_space_domain(ctx, is);
    rt->destroy_index_space(ctx, is);
    return result;
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    L::Runtime::preregister_task_variant<result_t, body<Variant>>(
      registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    rt->register_task_variant<result_t, body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
Legion::TaskID CFDomainsTask::task_id;

GridRegionGenerator::GridRegionGenerator(
  L::Context ctx, L::Runtime* rt, ImagingConfiguration const& img)
  : m_grid_params{
      {img.im.cell_width_arcsec,
       img.im.cell_height_arcsec,
       img.im.w_depth_arcsec}} {

  grid_t::dynamic_bounds_t grid_bounds = {
    {grid_t::axes_t::x,
     IndexInterval<grid_t::coord_t>::bounded(
       -(grid_t::coord_t{img.im.width} / 2),
       -(grid_t::coord_t{img.im.width} / 2) + img.im.width - 1)},
    {grid_t::axes_t::y,
     IndexInterval<grid_t::coord_t>::bounded(
       -(grid_t::coord_t{img.im.height} / 2),
       -(grid_t::coord_t{img.im.height} / 2) + img.im.height - 1)},
    {grid_t::axes_t::channel,
     IndexInterval<grid_t::coord_t>::bounded(
       img.im.image_channel_offset,
       img.im.image_channel_offset + img.im.num_channels() - 1)},
    {grid_t::axes_t::stokes,
     IndexInterval<grid_t::coord_t>::right_bounded(img.im.num_stokes() - 1)},
    {grid_t::axes_t::w_plane,
     IndexInterval<grid_t::coord_t>::right_bounded(
       std::max(img.im.w_size, 1U) - 1)}};
  m_gen =
    DenseArrayRegionGenerator<grid_t::static_region_t>(ctx, rt, grid_bounds);
}

void
GridRegionGenerator::destroy(L::Context ctx, L::Runtime* rt) {
  m_gen.destroy(ctx, rt);
}

auto
GridRegionGenerator::make(L::Context ctx, L::Runtime* rt)
  -> grid_t::LogicalRegion {
  auto result = m_gen.make(ctx, rt);
  grid_t::attach_params(rt, result, m_grid_params);
  return result;
}

auto
GridRegionGenerator::partition_by_tiles(
  L::Context ctx,
  L::Runtime* rt,
  grid_t::index_space_t grid_index_space,
  Configuration const& config,
  ImagingConfiguration const& imaging_config)
  -> std::tuple<grid_t::index_partition_t, tiles_t::LogicalRegion> {

  auto bounds = rt->get_index_space_domain(grid_index_space).bounds;

  // grid index space partition by channel, and w plane
  assert(
    imaging_config.im.image_channel_offset
    == bounds.lo[dim(grid_t::axes_t::channel)]);
  auto ch_w_ip = grid_t::create_partition_by_slicing(
    ctx,
    rt,
    grid_index_space,
    {{grid_t::axes_t::channel, 1, bounds.lo[dim(grid_t::axes_t::channel)]},
     {grid_t::axes_t::w_plane, 1, bounds.lo[dim(grid_t::axes_t::w_plane)]}});

  // xy partition
  //
  // For now only take the center portion of the xy plane that should contain
  // all the gridded visibilities in each of the channels
  tiles_t::Params tile_params{
    .universal_uvw_tiles =
      imaging_config.im.w_size == 1 && imaging_config.im.w_depth_arcsec == 0};
  // boundaries of central tile as function of dimension
  auto center = [&](unsigned dim) {
    return [&, dim](auto&& scales) -> std::vector<Configuration::uvw_value_t> {
      // widen the tile region about one pixel in each direction to
      // accommodate rounding
      auto edge = config.array_extent_m.at(dim) + scales.at(dim);
      return {-edge, edge};
    };
  };

  // image channel scales
  std::vector<std::array<Configuration::uvw_value_t, 3>> ch_scales(
    imaging_config.im.num_channels());
  std::ranges::generate(ch_scales, [&, ch = 0]() mutable {
    return config.image_channel_scale(
      imaging_config.im.image_channel_offset + ch++, imaging_config.im);
  });
  // center tile boundaries, x dimension
  auto cx = ch_scales | std::views::transform(center(0));
  // center tile boundaries, y dimension
  auto cy = ch_scales | std::views::transform(center(1));
  // compute tiled grid partition, extending tiles by half max CF size in each
  // direction
  auto ixy_ip = grid_t::create_ixy_tile_partition(
    ctx,
    rt,
    grid_index_space,
    config,
    imaging_config.im,
    cx,
    cy,
    (imaging_config.gr.test.cf_size + 1) / 2,
    (imaging_config.gr.test.cf_size + 1) / 2);
  // number of tiles in x direction, by image channel
  std::vector<std::size_t> num_tiles_x;
  std::ranges::transform(cx, std::back_inserter(num_tiles_x), [](auto&& cuts) {
    return cuts.size() - 1;
  });
  // number of tiles in y direction, by image channel
  std::vector<std::size_t> num_tiles_y;
  std::ranges::transform(cy, std::back_inserter(num_tiles_y), [](auto&& cuts) {
    return cuts.size() - 1;
  });
  // the color space for index partition will generally be sparse, since the xy
  // partition may be sparse, although this isn't the case for ixy_ip
  std::vector<tiles_t::rect_t> ch_w_uv_rects;
  auto ch_lo = bounds.lo[dim(grid_t::axes_t::channel)];
  for (auto&& ch : dim_range<grid_t>(grid_t::axes_t::channel, bounds))
    for (auto&& wp : dim_range<grid_t>(grid_t::axes_t::w_plane, bounds)) {
      tiles_t::rect_t rect;
      rect.lo[dim(tiles_t::axes_t::channel)] =
        rect.hi[dim(tiles_t::axes_t::channel)] = ch;
      rect.lo[dim(tiles_t::axes_t::w_plane)] =
        rect.hi[dim(tiles_t::axes_t::w_plane)] = wp;
      rect.lo[dim(tiles_t::axes_t::uv)] = 0;
      rect.hi[dim(tiles_t::axes_t::uv)] =
        coord_t(num_tiles_x.at(ch - ch_lo) * num_tiles_y.at(ch - ch_lo)) - 1;
      ch_w_uv_rects.push_back(rect);
    }
  auto ch_w_uv_cs = rt->create_index_space(ctx, ch_w_uv_rects);
  grid_t::index_partition_t ip =
    rt->create_pending_partition(ctx, grid_index_space, ch_w_uv_cs);

  // each subspace of the ip partition is the intersection of a subspace in
  // the ch_w_ip partition and one in the ixy_ip partition
  for (L::PointInDomainIterator pid(rt->get_index_space_domain(ch_w_uv_cs));
       pid();
       pid++) {
    // the tiles are linearly ordered in the ip partition; linearize the 2-d
    // tile indexes in column-major order
    auto nx = num_tiles_x.at(pid[dim(tiles_t::axes_t::channel)] - ch_lo);
    std::vector<grid_t::index_space_t> handles{
      rt->get_index_subspace(ch_w_ip, L::Point<2, coord_t>{pid[0], pid[1]}),
      rt->get_index_subspace(
        ixy_ip,
        L::Point<3, coord_t>{
          pid[0], coord_t(pid[2] / nx), coord_t(pid[2] % nx)})};
    rt->create_index_space_intersection(ctx, ip, *pid, handles);
  }

  // create region for tile bounds, and write values to it (inline)
  tiles_t::LogicalRegion tiles_region{};
  {
    auto fs = rt->create_field_space(ctx);
    auto fa = rt->create_field_allocator(ctx, fs);
    fa.allocate_field(sizeof(GridTileField::value_t), GridTileField::field_id);
    tiles_region =
      tiles_t::LogicalRegion{rt->create_logical_region(ctx, ch_w_uv_cs, fs)};
    tiles_t::attach_params(rt, tiles_region, tile_params);
    L::RegionRequirement req(
      tiles_region, LEGION_WRITE_ONLY, LEGION_EXCLUSIVE, tiles_region);
    req.add_field(GridTileField::field_id);
    auto pr = rt->map_region(ctx, req);
    auto const tiles =
      tiles_t::values<LEGION_WRITE_ONLY, false>(pr, GridTileField{});
    for (L::PointInDomainIterator pid(rt->get_index_space_domain(ch_w_uv_cs));
         pid();
         pid++) {
      auto subspace_bounds =
        rt->get_index_space_domain(rt->get_index_subspace(ip, *pid)).bounds;
      GridTileField::value_t tb;
      tb.lo[tiles_t::bound_x] = subspace_bounds.lo[dim(grid_t::axes_t::x)];
      tb.lo[tiles_t::bound_y] = subspace_bounds.lo[dim(grid_t::axes_t::y)];
      tb.hi[tiles_t::bound_x] = subspace_bounds.hi[dim(grid_t::axes_t::x)];
      tb.hi[tiles_t::bound_y] = subspace_bounds.hi[dim(grid_t::axes_t::y)];
      tiles.write(*pid, tb);
      log().debug("at " + show(*pid) + ": bounds " + show(tb));
    }
    rt->unmap_region(ctx, pr);
  }

  // m_tiles_ip = tiles_t::create_partition_by_slicing(
  //   ctx, rt, ch_w_uv_cs, {{tiles_t::axes_t::channel, 1}});
  return {ip, tiles_region};
}

WeightsRegionGenerator::WeightsRegionGenerator(
  L::Context ctx, L::Runtime* rt, ImageConfiguration const& im)
  : DenseArrayRegionGenerator(
      ctx,
      rt,
      std::map<spec_t::axes_t, spec_t::range_t>{
        {spec_t::axes_t::channel,
         spec_t::range_t::bounded(
           im.image_channel_offset,
           im.image_channel_offset + im.num_channels() - 1)},
        {spec_t::axes_t::stokes,
         spec_t::range_t::right_bounded(im.num_stokes() - 1)},
        {spec_t::axes_t::w_plane,
         spec_t::range_t::right_bounded(std::max(im.w_size, 1U) - 1)}}) {}

/*! \brief task update active grid and grid weights regions */
struct RenewGridAndWeightTask
  : public SerialOnlyTaskMixin<RenewGridAndWeightTask, void> {

  using grid_t = GridRegion;
  using gw_t = GridWeightsRegion;
  using gmd_t = GridAndWeightMetadataRegion;

  static constexpr char const* task_name = "RenewGridAndWeightTask";

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  enum {
    grid_metadata_region_index = 0,
    grid_region_index,
    grid_weights_region_index,
    num_regions
  };

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    auto gmd_pt = gmd_t::rect_t{regions[grid_metadata_region_index]}.lo;
    auto subarray_slot = gmd_pt[dim(gmd_t::axes_t::subarray)];
    log().debug("renew regions", bark::val_field_t{"subarray", subarray_slot});

    gmd_t::values<LEGION_WRITE_ONLY>(
      regions[grid_metadata_region_index], CountField{})[gmd_pt] = 0;
    gmd_t::values<LEGION_WRITE_ONLY>(
      regions[grid_metadata_region_index], TimestampField{})[gmd_pt] =
      StreamClock::external_time::min();
    rt->unmap_all_regions(ctx);
    grid_t::LogicalRegion{task->regions[grid_region_index].region}.fill_fields(
      ctx,
      rt,
      std::make_tuple(GridPixelField{}, GridPixelField::value_t{0, 0}));
    gw_t::LogicalRegion{task->regions[grid_weights_region_index].region}
      .fill_fields(
        ctx,
        rt,
        std::make_tuple(GridWeightField{}, GridWeightField::value_t{0}));
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
Legion::TaskID RenewGridAndWeightTask::task_id;

struct GridVisibilitiesOnImageTask
  : public SerialOnlyTaskMixin<GridVisibilitiesOnImageTask, void> {

  using grid_t = GridRegion;
  using gw_t = GridWeightsRegion;
  using gmd_t = GridAndWeightMetadataRegion;
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using cf_t = CFRegion;
  using mi_t = MuellerIndexRegion;
  using tiles_t = GridTileRegion;
  using bc_t = BaselineChannelRegion;
  using state_t = st::StateRegion;
  using part_t = DataPartitionsRegion;

  static constexpr char const* task_name = "GridVisibilitiesOnImageTask";
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  enum {
    grid_metadata_region_index = 0,
    state_region_index,
    mueller_region_index,
    visibilities_region_index,
    visibility_weights_region_index,
    uvws_region_index,
    cf_arrays_region_index,
    grid_region_index,
    grid_weights_region_index,
    partitions_region_index,
    num_regions
  };

  struct Args {
    std::array<char, Configuration::name_array_size> name{};
    int vector_length{};
    StokesMask stokes_mask{};
    grid_t::index_partition_t grid_partition;
    gw_t::index_partition_t weights_partition;
  };

  /*! \brief set up CF value regions
   *
   * Two result types are an unpartitioned CF array region, and a partitioned CF
   * array sub-region. This function is provided to accommodate the value of
   * partition_cf_regions with support for type inference using that value
   * without compile errors (an if constexpr guard on this code placed directly
   * in body() will fail to compile, but inside a template function the
   * discarded branch is not checked).
   *
   * In partitioned case, on return, cf_index_partition contains a partition
   * of cf_index_region that is the projection of the uvw_partition index
   * partition onto cf_index_region.
   *
   * \param ctx Legion Context
   * \param rt Legion Runtime
   * \param cf_array_region CF value region
   * \param uvw_partition uvw region partition
   *
   * \result CF value region for gridding
   */
  template <bool PART_CF>
  static auto
  get_cf_value_region(
    L::Context ctx,
    L::Runtime* rt,
    RegionPair<cf_t::LogicalRegion> const& cf_array_region,
    bc_t::LogicalPartition const& uvw_partition) {

    if constexpr (PART_CF) {
      cf_t::LogicalPartition result;
      // Since the CFIndexField values are just values of the 'index'
      // dimension in cf_array_region, we need to "expand" those index values
      // to contain all CF values at each index, which we implement by
      // constructing a Domain for all of those CF values. To construct those
      // domains, we launch CFDomainsTask, and in turn use the FutureMap
      // returned by that task launch for the map from colors to Domains in a
      // call to create_partition_by_domain().
      auto cs = rt->get_index_partition_color_space_name<
        bc_t::dim,
        bc_t::coord_t,
        tiles_t::dim + 1,
        tiles_t::coord_t>(
        bc_t::index_partition_t(uvw_partition.get_index_partition()));
      L::IndexTaskLauncher cfdomains_launcher(
        CFDomainsTask::task_id, cs, L::UntypedBuffer(), L::ArgumentMap());
      cfdomains_launcher.set_predicate_false_result(
        L::UntypedBuffer(&L::Domain::NO_DOMAIN, sizeof(L::Domain)));
      cfdomains_launcher.add_region_requirement(uvw_partition.requirement(
        0,
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        rt,
        StaticFields{CFIndexField{}}));
      auto domains = rt->execute_index_space(ctx, cfdomains_launcher);
      result = cf_t::LogicalPartition{
        cf_t::logical_partition_t{rt->get_logical_partition(
          ctx,
          cf_array_region.region(),
          rt->create_partition_by_domain(
            ctx,
            cf_t::index_space_t(cf_array_region.region().get_index_space()),
            domains,
            cs))}};
      return result;
    } else {
      return cf_array_region;
    }
  }

  static auto
  serial_body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    assert(task->arglen == sizeof(Args));
    Args const& args = *reinterpret_cast<Args const*>(task->args);

    // update buffer accumulation count
    auto gmd_pt = gmd_t::rect_t{regions[grid_metadata_region_index]}.lo;
    auto subarray_slot = gmd_pt[dim(gmd_t::axes_t::subarray)];

    log().debug("grid subarray ", bark::val_field_t{"slot", subarray_slot});

    gmd_t::values<LEGION_READ_WRITE>(
      regions[grid_metadata_region_index], CountField{})[gmd_pt] += 1;
    // update buffer timestamp
    auto& timestamp = gmd_t::values<LEGION_READ_WRITE>(
      regions[grid_metadata_region_index], TimestampField{})[gmd_pt];
    auto const& state_region = regions[state_region_index];
    auto const state = state_t::values<LEGION_READ_ONLY>(
      state_region, st::StateField{})[state_t::rect_t{state_region}
                                        .lo[dim(state_t::axes_t::shard)]];
    if (timestamp == StreamClock::external_time::min())
      timestamp = state.timestamp();

    auto grid_region =
      grid_t::LogicalRegion{task->regions[grid_region_index].region};
    auto grid_weights_region =
      gw_t::LogicalRegion{task->regions[grid_weights_region_index].region};

    // data partitions
    auto const& partitions_region = regions[partitions_region_index];
    auto const partitions_pt = part_t::rect_t{partitions_region}.lo;
    auto const visibilities_ip = part_t::values<LEGION_READ_ONLY>(
      partitions_region, VisibilitiesTilePartitionField{})[partitions_pt];
    assert(visibilities_ip.exists());
    auto const uvws_ip = part_t::values<LEGION_READ_ONLY>(
      partitions_region, UVWsTilePartitionField{})[partitions_pt];
    assert(uvws_ip.exists());

    // grid region must have parameters attached to do gridding
    assert(grid_t::has_params(rt, grid_region));

    GridVisibilitiesOnTiles gridder(
      ctx,
      rt,
      args.name,
      args.vector_length,
      args.stokes_mask,
      grid_t::LogicalPartition{
        rt->get_logical_partition(ctx, grid_region, args.grid_partition)},
      gw_t::LogicalPartition{rt->get_logical_partition(
        ctx, grid_weights_region, args.weights_partition)},
      mi_t::LogicalRegion{task->regions[mueller_region_index].region});

    RegionPair<bc_t::LogicalRegion> uvw_region{
      task->regions[uvws_region_index]};
    auto uvw_partition = bc_t::LogicalPartition{rt->get_logical_partition(
      bc_t::logical_region_t{uvw_region.region()}, uvws_ip)};

    RegionPair<cbp_t::LogicalRegion> vis_region{
      task->regions[visibilities_region_index]};
    auto vis_partition = cbp_t::LogicalPartition{rt->get_logical_partition(
      cbp_t::logical_region_t{vis_region.region()}, visibilities_ip)};

    RegionPair<cbp_t::LogicalRegion> vis_wgts_region{
      task->regions[visibility_weights_region_index]};
    auto vis_wgt_partition = cbp_t::LogicalPartition{rt->get_logical_partition(
      cbp_t::logical_region_t{vis_wgts_region.region()}, visibilities_ip)};

    RegionPair<cf_t::LogicalRegion> cf_array_region{
      task->regions[cf_arrays_region_index]};
    // cf_array_regions must have parameters attached, as they are required for
    // gridding
    assert(cf_t::has_params(rt, cf_array_region.region()));

    // compute partition of CF indexes and CF arrays
    auto cf_value_region = get_cf_value_region<partition_cf_regions>(
      ctx, rt, cf_array_region, uvw_partition);

    // all regions used by the following task (should) have been mapped
    // virtually in this task, so there's no need to unmap them before the task
    // launch
    gridder.launch(
      ctx,
      rt,
      PartitionPair{vis_partition, vis_region.region()},
      PartitionPair{vis_wgt_partition, vis_wgts_region.region()},
      PartitionPair{uvw_partition, uvw_region.region()},
      cf_value_region);
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
Legion::TaskID GridVisibilitiesOnImageTask::task_id;

GridVisibilitiesOnImage::GridVisibilitiesOnImage(
  L::Context ctx,
  L::Runtime* rt,
  Configuration const& config,
  ImagingConfiguration const& img_config,
  mi_t::LogicalRegion const& mueller_indexes)
  : m_imaging_configuration(img_config)
  , m_grid_generator{ctx, rt, img_config}
  , m_weights_generator{ctx, rt, img_config.im}
  , m_vector_length(int(std::max(img_config.gr.vector_length, 1U)))
  , m_stokes_mask(img_config.gr.stokes_mask)
  , m_mueller_indexes_region(mueller_indexes) {

  m_name.fill('\0');
  assert(img_config.im.name.size() < m_name.size());
  std::ranges::copy(img_config.im.name, m_name.begin());

  log().debug(
    "'" + img_config.im.name + "' gridding Stokes mask " + show(m_stokes_mask));

  // create m_grid_metadata regions
  {
    auto fs = gmd_t::field_space(ctx, rt);
    auto maybe_is = gmd_t::index_space(ctx, rt);
    assert(maybe_is);
    // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
    auto is = maybe_is.value();
    m_grid_metadata =
      gmd_t::LogicalRegion{rt->create_logical_region(ctx, is, fs)};
    auto ip = gmd_t::create_partition_by_slicing(
      ctx, rt, is, {{gmd_t::axes_t::subarray, 1}});
    m_grid_metadata_partition =
      gmd_t::LogicalPartition{rt->get_logical_partition(m_grid_metadata, ip)};
  }

  // create grid and grid weights regions
  std::ranges::generate(m_subarray_regions, [&]() -> SubarrayRegions {
    auto grid = m_grid_generator.make(ctx, rt);
    auto [grid_partition, tile_bounds] =
      GridRegionGenerator::partition_by_tiles(
        ctx,
        rt,
        grid_t::index_space_t{grid.get_index_space()},
        config,
        img_config);
    auto grid_weight = m_weights_generator.make(ctx, rt);
    auto grid_weight_partition = gw_t::create_partition_by_slicing(
      ctx,
      rt,
      gw_t::index_space_t{grid_weight.get_index_space()},
      {{gw_t::axes_t::channel, 1, img_config.im.image_channel_offset},
       {gw_t::axes_t::w_plane, 1, 0}});
    return {
      .grid = grid,
      .grid_weight = grid_weight,
      .grid_partition = grid_partition,
      .grid_weight_partition = grid_weight_partition,
      .tile_bounds = tile_bounds};
  });
}

GridVisibilitiesOnImage::GridVisibilitiesOnImage(
  L::Context ctx, L::Runtime* rt, Serialized const& serialized) noexcept
  : m_visibilities_index_space(serialized.m_visibilities_index_space)
  , m_uvws_index_space(serialized.m_uvws_index_space)
  , m_imaging_configuration(
      deserialize_imaging_configuration(serialized.m_imaging_configuration))
  , m_grid_generator{ctx, rt, m_imaging_configuration}
  , m_weights_generator{ctx, rt, m_imaging_configuration.im}
  , m_name(serialized.m_name)
  , m_vector_length(std::max(m_imaging_configuration.gr.vector_length, 1U))
  , m_stokes_mask(m_imaging_configuration.gr.stokes_mask)
  , m_grid_metadata(serialized.m_grid_metadata)
  , m_grid_metadata_partition(serialized.m_grid_metadata_partition)
  , m_subarray_regions(serialized.m_subarray_regions)
  , m_mueller_indexes_region(serialized.m_mueller_indexes_region) {}

auto
GridVisibilitiesOnImage::serialized(L::Context ctx, L::Runtime* rt) const
  -> Serialized {

  auto result = Serialized{
    m_visibilities_index_space,
    m_uvws_index_space,
    m_name,
    m_grid_metadata,
    m_grid_metadata_partition,
    m_subarray_regions,
    m_mueller_indexes_region};
  if (
    m_imaging_configuration.serialized_size()
    > result.m_imaging_configuration.size())
    throw std::runtime_error(
      "Serialized size of ImagingConfiguration is too large");
  m_imaging_configuration.serialize(result.m_imaging_configuration.data());
  return result;
}

auto
GridVisibilitiesOnImage::child_requirements(
  L::Context ctx, L::Runtime* rt) const -> ChildRequirements {

  std::vector<L::RegionRequirement> regreqs;
  regreqs.push_back(m_grid_metadata.requirement(
    LEGION_READ_WRITE,
    LEGION_EXCLUSIVE,
    StaticFields{TimestampField{}, CountField{}},
    std::nullopt,
    std::nullopt,
    L::Mapping::DefaultMapper::VIRTUAL_MAP));
  regreqs.push_back(m_mueller_indexes_region.requirement(
    LEGION_READ_ONLY,
    LEGION_EXCLUSIVE,
    StaticFields{ForwardIndexField{}, InverseIndexField{}},
    std::nullopt,
    std::nullopt,
    L::Mapping::DefaultMapper::VIRTUAL_MAP));
  for (auto&& subarray_region : m_subarray_regions) {
    regreqs.push_back(subarray_region.grid.requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{GridPixelField{}},
      std::nullopt,
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP));
    regreqs.push_back(subarray_region.grid_weight.requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{GridWeightField{}},
      std::nullopt,
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP));
    regreqs.push_back(subarray_region.tile_bounds.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{GridTileField{}},
      std::nullopt,
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP));
  }
  return {regreqs, {}, {}};
}

auto
GridVisibilitiesOnImage::init_imaging_launcher(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<state_t::LogicalRegion> const& state,
  RegionPair<cbp_t::LogicalRegion> const& visibilities,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  cf_t::LogicalRegion const& cf_arrays,
  L::UntypedBuffer arg) const -> L::TaskLauncher {

  auto result = L::TaskLauncher(GridVisibilitiesOnImageTask::task_id, arg);
  result.region_requirements.resize(GridVisibilitiesOnImageTask::num_regions);

  result
    .region_requirements[GridVisibilitiesOnImageTask::mueller_region_index] =
    m_mueller_indexes_region.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{ForwardIndexField{}, InverseIndexField{}});

  result.region_requirements[GridVisibilitiesOnImageTask::state_region_index] =
    st::State0Select(ctx, rt, state)(state).requirement(
      LEGION_READ_ONLY, LEGION_EXCLUSIVE, StaticFields{st::StateField{}});
  result.region_requirements
    [GridVisibilitiesOnImageTask::visibilities_region_index] =
    visibilities.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{VisibilityField{}},
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);
  result.region_requirements[GridVisibilitiesOnImageTask::uvws_region_index] =
    uvws.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{UVWField{}, CFIndexField{}},
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);
  result
    .region_requirements[GridVisibilitiesOnImageTask::cf_arrays_region_index] =
    cf_arrays.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{CFValueField{}},
      std::nullopt,
      std::nullopt,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);
  return result;
}

auto
GridVisibilitiesOnImage::update_imaging_launcher(
  L::Runtime* rt,
  PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
  sarray<RegionPair<cbp_t::LogicalRegion>> const& visibility_weights,
  unsigned subarray_slot,
  L::TaskLauncher& launcher) const -> void {

  auto& subarray_region = m_subarray_regions.at(subarray_slot);

  auto* args = reinterpret_cast<GridVisibilitiesOnImageTask::Args*>(
    launcher.argument.get_ptr());

  args->grid_partition = subarray_region.grid_partition;
  args->weights_partition = subarray_region.grid_weight_partition;

  auto partitions_subregion = get_partitions_region_for_subarray(
    rt, partitions_by_subarray, subarray_slot);
  launcher.region_requirements
    [GridVisibilitiesOnImageTask::visibility_weights_region_index] =
    visibility_weights.at(subarray_slot)
      .requirement(
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        StaticFields{WeightField{}},
        std::nullopt,
        L::Mapping::DefaultMapper::VIRTUAL_MAP);
  launcher
    .region_requirements[GridVisibilitiesOnImageTask::partitions_region_index] =
    partitions_subregion.requirement(
      LEGION_READ_ONLY,
      LEGION_EXCLUSIVE,
      StaticFields{VisibilitiesTilePartitionField{}, UVWsTilePartitionField{}});

  launcher.region_requirements
    [GridVisibilitiesOnImageTask::grid_metadata_region_index] =
    subarray_grid_metadata(rt, subarray_slot)
      .requirement(
        LEGION_READ_WRITE,
        LEGION_EXCLUSIVE,
        StaticFields{CountField{}, TimestampField{}},
        std::nullopt,
        m_grid_metadata);

  launcher.region_requirements[GridVisibilitiesOnImageTask::grid_region_index] =
    subarray_region.grid.requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{GridPixelField{}},
      std::nullopt,
      subarray_region.grid,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);

  launcher.region_requirements
    [GridVisibilitiesOnImageTask::grid_weights_region_index] =
    subarray_region.grid_weight.requirement(
      LEGION_READ_WRITE,
      LEGION_EXCLUSIVE,
      StaticFields{GridWeightField{}},
      std::nullopt,
      subarray_region.grid_weight,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);
}

auto
GridVisibilitiesOnImage::launch(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<state_t::LogicalRegion> const& state,
  PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
  RegionPair<cbp_t::LogicalRegion> const& visibilities,
  std::array<
    RegionPair<cbp_t::LogicalRegion>,
    Configuration::max_num_subarrays> const& visibility_weights,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  cf_t::LogicalRegion const& cf_arrays,
  st::subarray_predicates_array_t const& predicates) const -> void {

  auto is_imaging = predicate_slice_subarray_predicates(
    predicates, st::SubarrayStreamPredicate::is_imaging);

  GridVisibilitiesOnImageTask::Args args;
  args.name = m_name;
  args.vector_length = m_vector_length;
  args.stokes_mask = m_stokes_mask;

  auto launcher = init_imaging_launcher(
    ctx,
    rt,
    state,
    visibilities,
    uvws,
    cf_arrays,
    L::UntypedBuffer(&args, sizeof(args)));
  for (auto&& subarray_slot : subarray_slots()) {
    update_imaging_launcher(
      rt, partitions_by_subarray, visibility_weights, subarray_slot, launcher);
    launcher.predicate = is_imaging(subarray_slot);
    rt->execute_task(ctx, launcher);
  }
}

auto
GridVisibilitiesOnImage::launch(
  L::Context ctx,
  L::Runtime* rt,
  RegionPair<state_t::LogicalRegion> const& state,
  PartitionPair<part_t::LogicalPartition> const& partitions_by_subarray,
  RegionPair<cbp_t::LogicalRegion> const& visibilities,
  sarray<RegionPair<cbp_t::LogicalRegion>> const& visibility_weights,
  RegionPair<bc_t::LogicalRegion> const& uvws,
  cf_t::LogicalRegion const& cf_arrays,
  st::AllSubarraysQuery const& state_query) const -> void {

  auto do_imaging = state_query.do_imaging();

  GridVisibilitiesOnImageTask::Args args;
  args.name = m_name;
  args.vector_length = m_vector_length;
  args.stokes_mask = m_stokes_mask;

  auto launcher = init_imaging_launcher(
    ctx,
    rt,
    state,
    visibilities,
    uvws,
    cf_arrays,
    L::UntypedBuffer(&args, sizeof(args)));
  for (auto&& subarray_slot : filtered_subarray_slots(do_imaging)) {
    update_imaging_launcher(
      rt, partitions_by_subarray, visibility_weights, subarray_slot, launcher);
    rt->execute_task(ctx, launcher);
  }
}

auto
GridVisibilitiesOnImage::init_next_grid_and_weights_launcher(
  L::Runtime* rt, int subarray_slot, L::TaskLauncher& launcher) const -> void {

  assert(launcher.task_id == RenewGridAndWeightTask::task_id);
  launcher.region_requirements.resize(RenewGridAndWeightTask::num_regions);
  auto const& subarray = m_subarray_regions.at(subarray_slot);
  launcher
    .region_requirements[RenewGridAndWeightTask::grid_metadata_region_index] =
    subarray_grid_metadata(rt, subarray_slot)
      .requirement(
        LEGION_WRITE_ONLY,
        LEGION_EXCLUSIVE,
        StaticFields{CountField{}, TimestampField{}},
        std::nullopt,
        m_grid_metadata);
  // the grid field privileges must be LEGION_READ_WRITE (not
  // LEGION_WRITE_ONLY) due to requirements of L::Runtime::fill_field()
  launcher.region_requirements[RenewGridAndWeightTask::grid_region_index] =
    subarray.grid.requirement(
      LEGION_WRITE_DISCARD,
      LEGION_EXCLUSIVE,
      StaticFields{GridPixelField{}},
      std::nullopt,
      subarray.grid,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);
  launcher
    .region_requirements[RenewGridAndWeightTask::grid_weights_region_index] =
    subarray.grid_weight.requirement(
      LEGION_WRITE_DISCARD,
      LEGION_EXCLUSIVE,
      StaticFields{GridWeightField{}},
      std::nullopt,
      subarray.grid_weight,
      L::Mapping::DefaultMapper::VIRTUAL_MAP);
}

auto
GridVisibilitiesOnImage::next_grid_and_weights(
  L::Context ctx,
  L::Runtime* rt,
  st::subarray_predicates_array_t const& predicates) const -> void {

  auto is_imaging_start = predicate_slice_subarray_predicates(
    predicates, st::SubarrayStreamPredicate::is_imaging_start);

  L::TaskLauncher launcher(RenewGridAndWeightTask::task_id, L::UntypedBuffer());
  for (auto&& subarray_slot : subarray_slots()) {
    init_next_grid_and_weights_launcher(rt, subarray_slot, launcher);
    launcher.predicate = is_imaging_start(subarray_slot);
    rt->execute_task(ctx, launcher);
  }
}

auto
GridVisibilitiesOnImage::next_grid_and_weights(
  L::Context ctx,
  L::Runtime* rt,
  st::AllSubarraysQuery const& state_query) const -> void {

  auto is_imaging_start = state_query.do_imaging_start();

  L::TaskLauncher launcher(RenewGridAndWeightTask::task_id, L::UntypedBuffer());
  for (auto&& subarray_slot : subarray_slots()) {
    if (is_imaging_start.at(subarray_slot)) {
      init_next_grid_and_weights_launcher(rt, subarray_slot, launcher);
      rt->execute_task(ctx, launcher);
    }
  }
}

auto
GridVisibilitiesOnImage::grid_and_weight_metadata() const
  -> gmd_t::LogicalPartition {
  return m_grid_metadata_partition;
}

auto
GridVisibilitiesOnImage::grids() const -> sarray<grid_t::LogicalRegion> {
  sarray<grid_t::LogicalRegion> result;
  std::ranges::transform(
    m_subarray_regions, result.data(), [](auto&& sr) { return sr.grid; });
  return result;
}

auto
GridVisibilitiesOnImage::grid_weights() const -> sarray<gw_t::LogicalRegion> {
  sarray<gw_t::LogicalRegion> result;
  std::ranges::transform(m_subarray_regions, result.data(), [](auto&& sr) {
    return sr.grid_weight;
  });
  return result;
}

auto
GridVisibilitiesOnImage::tile_bounds() const -> sarray<tiles_t::LogicalRegion> {
  sarray<tiles_t::LogicalRegion> result;
  std::ranges::transform(m_subarray_regions, result.data(), [](auto&& sr) {
    return sr.tile_bounds;
  });
  return result;
}

auto
GridVisibilitiesOnImage::grid_partitions() const
  -> sarray<grid_t::index_partition_t> {
  sarray<grid_t::index_partition_t> result;
  std::ranges::transform(m_subarray_regions, result.data(), [](auto&& sr) {
    return sr.grid_partition;
  });
  return result;
}

auto
GridVisibilitiesOnImage::name() const -> std::string {
  return m_name.data();
}

auto
GridVisibilitiesOnImage::imaging_configuration() const noexcept
  -> ImagingConfiguration {
  return m_imaging_configuration;
}

void
GridVisibilitiesOnImage::destroy(L::Context ctx, L::Runtime* rt) {
  m_grid_generator.destroy(ctx, rt);
  m_weights_generator.destroy(ctx, rt);
  if (m_grid_metadata != gmd_t::LogicalRegion{}) {
    rt->destroy_index_space(ctx, m_grid_metadata.get_index_space());
    rt->destroy_field_space(ctx, m_grid_metadata.get_field_space());
    rt->destroy_logical_region(ctx, m_grid_metadata);
  }
  for (auto&& subarray : m_subarray_regions) {
    rt->destroy_logical_region(ctx, subarray.grid);
    rt->destroy_logical_region(ctx, subarray.grid_weight);
    rt->destroy_index_space(ctx, subarray.tile_bounds.get_index_space());
    rt->destroy_field_space(ctx, subarray.tile_bounds.get_field_space());
    rt->destroy_logical_region(ctx, subarray.tile_bounds);
  }
}

auto
GridVisibilitiesOnImage::preregister_tasks(
  std::array<L::TaskID, 3> const& task_ids) -> void {
  PortableTask<GridVisibilitiesOnImageTask>::preregister_task_variants(
    task_ids[0]);
  PortableTask<CFDomainsTask>::preregister_task_variants(task_ids[1]);
  PortableTask<RenewGridAndWeightTask>::preregister_task_variants(task_ids[2]);
}

auto
GridVisibilitiesOnImage::register_tasks(
  L::Runtime* rt, std::array<L::TaskID, 3> const& task_ids) -> void {
  PortableTask<GridVisibilitiesOnImageTask>::register_task_variants(
    rt, task_ids[0]);
  PortableTask<CFDomainsTask>::register_task_variants(rt, task_ids[1]);
  PortableTask<RenewGridAndWeightTask>::register_task_variants(rt, task_ids[2]);
}

auto
GridVisibilitiesOnImage::subarray_grid_metadata(
  L::Runtime* rt, int subarray) const -> gmd_t::LogicalRegion {
  return gmd_t::LogicalRegion{
    rt->get_logical_subregion_by_color<gmd_t::dim, gmd_t::coord_t>(
      m_grid_metadata_partition, L::Point<1, int>{subarray})};
}

auto
GridVisibilitiesOnImage::deserialize_imaging_configuration(
  std::array<char, ImagingConfiguration::max_serialized_size> const& buffer)
  -> ImagingConfiguration {

  ImagingConfiguration result;
  result.deserialize(buffer.data());
  return result;
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
