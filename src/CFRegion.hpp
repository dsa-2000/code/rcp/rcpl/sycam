// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BaselineChannelRegion.hpp"
#include "Configuration.hpp"
#include "StaticFields.hpp"

#include <complex>

namespace rcp::symcam {

/*! \brief CF values region index space axes definition
 *
 * the "index" axis is simply a generic index for a sequence of convolution
 * function arrays, it is likely a linearized version of some multi-dimensional
 * index, for example (parallactic angle, baseline class, w, channel, stokes)
 */
enum class CFAxes {
  x_major = LEGION_DIM_0,
  x_minor = LEGION_DIM_1,
  y_major = LEGION_DIM_2,
  y_minor = LEGION_DIM_3,
  mueller = LEGION_DIM_4,
  index = LEGION_DIM_5
};

#ifdef RCP_USE_KOKKOS
/*! \brief CF value field definition */
using CFValueField = StaticField<
  std::complex<Configuration::cf_value_t>,
  field_ids::cf_value,
  nullptr,
  Kokkos::complex<Configuration::cf_value_t>>;
#else  // !RCP_USE_KOKKOS
/*! \brief CF value field definition */
using CFValueField =
  StaticField<std::complex<Configuration::cf_value_t>, field_ids::cf_value>;
#endif // !RCP_USE_KOKKOS

/*! \brief CF region definition
 *
 * A 6-d index space with a single field CFValueField. Index coordinate values
 * are constrained to be non-negative on x_minor, y_minor, mueller and index
 * axes.
 */
struct CFRegion
  : public ::rcp::StaticRegionSpec<
      ::rcp::IndexSpec<CFAxes, CFAxes::x_major, CFAxes::index, coord_t>,
      std::array{
        ::rcp::IndexInterval<coord_t>::unbounded(),
        ::rcp::IndexInterval<coord_t>::left_bounded(0),
        ::rcp::IndexInterval<coord_t>::unbounded(),
        ::rcp::IndexInterval<coord_t>::left_bounded(0),
        ::rcp::IndexInterval<coord_t>::left_bounded(0),
        ::rcp::IndexInterval<coord_t>::left_bounded(0)},
      CFValueField> {

  /*! \brief parameters included as semantic information in regions */
  struct Params {
    unsigned oversampling; /*!< CF oversampling factor */
    unsigned padding;      /*!< CF padding on both ends of both "major" axes */
  };
  /*! \brief semantic tag for Params */
  static constexpr L::SemanticTag params_tag = 15;

  /*! \brief attach Params to a region */
  static void
  attach_params(L::Runtime* rt, LogicalRegion region, Params const& params);

  /*! \brief test for Params on a region */
  [[nodiscard]] static auto
  has_params(L::Runtime* rt, LogicalRegion region) -> bool;

  /*! \brief read Params from a region */
  [[nodiscard]] static auto
  retrieve_params(L::Runtime* rt, LogicalRegion region) -> Params const&;

  /*! \brief convert linearized X or Y coordinate to decomposed form
   * (major/minor pair) */
  static auto
  decomposed(coord_t coord, unsigned oversampling) -> std::array<coord_t, 2> {
    auto major = std::lrint(std::floor(double(coord) / oversampling));
    return {major, coord - major * oversampling};
  }

  /*! \brief convert decomposed coordinate (major/minor pair) to linearized form
   */
  static auto
  linearized(std::array<coord_t, 2> const& coord, unsigned oversampling) {
    return coord[0] * oversampling + coord[1];
  }

  /*! \brief convert CF extents to bounds
   *
   * \param extents CF extents, no padding, in coarse coordinates
   * \param num_mueller extent of CF region in "mueller" dimension
   * \param num_indexes extent of CF region in "index" dimension
   * \param params region Params value
   */
  static auto
  extents_to_bounds(
    std::array<unsigned, 2> const& extents,
    unsigned num_mueller,
    unsigned num_indexes,
    Params const& params) -> dynamic_bounds_t;
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
