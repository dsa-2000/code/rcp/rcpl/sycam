// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "EventProcessing.hpp"
#include "GridRegion.hpp"
#include "GridVisibilitiesOnImage.hpp"
#include "Serializable.hpp"
#include "StateRegion.hpp"

#include <array>

namespace rcp::symcam {

/*! launcher for task to export grids
 */
class ExportGrid {
public:
  using grid_t = GridRegion;
  using gmd_t = GridAndWeightMetadataRegion;
  using state_t = st::StateRegion;

  struct Args {
    std::array<char, 20> name; /*!< \fixme size should be configuration value */
    std::chrono::nanoseconds vis_interval;
    int subarray;
  };

private:
  Args m_args;

public:
  struct Serialized {
    Args m_args;
  };

  ExportGrid(
    L::Context ctx,
    L::Runtime* rt,
    Configuration const& config,
    ImagingConfiguration const& img_config);

  ExportGrid(L::Context ctx, L::Runtime* rt, Serialized const& serialized);

  ExportGrid(ExportGrid const&) = default;

  ExportGrid(ExportGrid&&) noexcept = default;

  virtual ~ExportGrid() = default;

  auto
  operator=(ExportGrid const&) -> ExportGrid& = default;

  auto
  operator=(ExportGrid&&) noexcept -> ExportGrid& = default;

  /*! \brief serialized form */
  [[nodiscard]] auto
  serialized(L::Context ctx, L::Runtime* rt) const noexcept -> Serialized;

  /*! \brief requirements for child tasks that use Serialized values as
   *  arguments */
  [[nodiscard]] auto
  child_requirements(L::Context ctx, L::Runtime* rt) const noexcept
    -> ChildRequirements;

  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    PartitionPair<gmd_t::LogicalPartition> const& grid_metadata,
    sarray<grid_t::LogicalRegion> const& grids,
    st::subarray_predicates_array_t const& predicates) -> void;

  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    PartitionPair<gmd_t::LogicalPartition> const& grid_metadata,
    sarray<grid_t::LogicalRegion> const& grids,
    st::AllSubarraysQuery const& state_query) -> void;

  static auto
  preregister_tasks(L::TaskID task_id) -> void;

  static auto
  register_tasks(L::Runtime* rt, L::TaskID task_id) -> void;

  static auto
  subarray_grid_metadata(
    L::Runtime* rt,
    int subarray,
    PartitionPair<gmd_t::LogicalPartition> const& part)
    -> RegionPair<gmd_t::LogicalRegion>;

private:
  auto
  init_launcher() -> L::IndexTaskLauncher;

  auto
  update_launcher(
    L::Context ctx,
    L::Runtime* rt,
    PartitionPair<gmd_t::LogicalPartition> const& grid_metadata,
    sarray<grid_t::LogicalRegion> const& grids,
    unsigned subarray_slot,
    L::IndexTaskLauncher& launcher) -> void;
};
} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
