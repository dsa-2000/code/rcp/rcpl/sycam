// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "CrossCorrelate.hpp"

#ifdef RCP_USE_CUDA
#include "LoadCUDALibraries.hpp"
#endif

#include <optional>
#include <vector>

using namespace rcp::symcam;

#ifdef RCP_USE_KOKKOS
namespace Kokkos { // reduction identity must be defined in Kokkos namespace
template <>
struct reduction_identity<SampleCorrelationField::kvalue_t> {
  KOKKOS_FORCEINLINE_FUNCTION static auto
  sum() -> SampleCorrelationField::kvalue_t {
    return 0;
  }
};
} // namespace Kokkos
#endif // RCP_USE_KOKKOS

namespace rcp::symcam {

/*! task to do cross correlation on region of (unflagged) samples
 */
struct CrossCorrelateTask {

  using samples_t = FSamplesRegion;

  using cbp_t = BsetChannelBaselinePolprodRegion;

  /*! \brief task name */
  static constexpr const char* task_name = "CrossCorrelate";

  /*! \brief task id */
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id;

  /*! \brief task region indexes */
  enum { samples_region_index = 0, visibilities_region_index, num_regions };

  /*! \brief enabled variants
   *
   * Enable a CPU version (satisfied by using Kokkos, otherwise a native
   * implementation), and a GPU version if CUDA is enabled (either tensor-core
   * correlator or Kokkos)
   */
  static constexpr auto enabled_variants = variant_set(
#if !defined(RCP_USE_KOKKOS) || defined(RCP_USE_KOKKOS_SERIAL)
    task_variant<TaskVariant::Serial>{}
#endif
#ifdef RCP_USE_KOKKOS_OPENMP
    ,
    task_variant<TaskVariant::OpenMP>{}
#endif
#if defined(RCP_USE_CUDA) && defined(SYMCAM_USE_TENSOR_CORE_CORRELATOR)        \
  || defined(RCP_USE_KOKKOS_CUDA)
    ,
    task_variant<TaskVariant::Cuda>{}
#endif
  );

#if defined(RCP_USE_CUDA) && defined(SYMCAM_USE_TENSOR_CORE_CORRELATOR)
  static auto
  tcc_body(
    L::Task const*,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> void {

    assert(regions.size() == num_regions);

    auto& vis_region = regions[visibilities_region_index];
    vis_t::rect_t vis_bounds = vis_region;
    assert(vis_bounds.lo[dim(vis_t::axes_t::polarization_product)] == 0);
    assert(
      vis_bounds.hi[dim(vis_t::axes_t::polarization_product)]
      == Configuration::num_polarization_products - 1);
    auto const visibilities =
      vis_t::values<LEGION_WRITE_ONLY>(vis_region, SampleCorrelationField{});

    auto& smp_region = regions[samples_region_index];
    samples_t::rect_t smp_bounds = smp_region;
    auto const samples_pieces = L::PieceIteratorT<samples_t::dim, coord_t>(
      smp_region, FSampleField::field_id, true);
    auto const samples = samples_t::values<LEGION_READ_ONLY>(
      smp_region, FSampleField{}, *samples_pieces);

    auto const num_smp_ts = smp_bounds.hi[dim(samples_t::axes_t::timestep)]
                            - smp_bounds.lo[dim(samples_t::axes_t::timestep)]
                            + 1;
    auto const num_channels = vis_bounds.hi[dim(vis_t::axes_t::channel)]
                              - vis_bounds.lo[dim(vis_t::axes_t::channel)] + 1;
    auto xcorr = get_correlator(num_smp_ts, num_channels);
    xcorr.launchAsync(
      get_cuda_stream(),
      CUdeviceptr(visibilities.ptr(vis_bounds)),
      CUdeviceptr(samples.ptr(samples_pieces->lo)));
  }
#endif

  static RCP_INLINE_FUNCTION auto
  prod(FSampleField::value_t x, FSampleField::value_t y) {
#ifdef RCP_USE_KOKKOS
    using result_type = SampleCorrelationField::kvalue_t;
#else
    using result_type = SampleCorrelationField::value_t;
#endif
    using value_type = result_type::value_type;

    constexpr auto num_bits = CrossCorrelationConfiguration::num_bits;
    constexpr auto shift = 8 * sizeof(value_type) - num_bits;
    auto promote = [&](FSampleField::value_t fsv) -> result_type {
      return {
        (value_type(fsv.i) << shift) >> shift,
        (value_type(fsv.i) << (shift - num_bits)) >> shift};
    };
    auto vx = promote(x);
    return result_type{vx.real(), -vx.imag()} * promote(y);
  }

#ifdef RCP_USE_KOKKOS
  template <typename execution_space>
  static auto
  kokkos_body(
    L::Task const* /*task*/,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    assert(regions.size() == 2);

    cbp_t::rect_t vis_bounds = regions[visibilities_region_index];
    assert(vis_bounds.lo[dim(cbp_t::axes_t::polarization_product)] == 0);
    assert(
      vis_bounds.hi[dim(cbp_t::axes_t::polarization_product)]
      == Configuration::num_polarization_products - 1);
    auto const visibilities = cbp_t::view<execution_space, LEGION_WRITE_ONLY>(
      regions[visibilities_region_index], SampleCorrelationField{});

    auto const& smp_region = regions[samples_region_index];

    auto num_baselines = vis_bounds.hi[dim(cbp_t::axes_t::baseline)]
                         - vis_bounds.lo[dim(cbp_t::axes_t::baseline)] + 1;
    auto work_space = rt->get_executing_processor(ctx).kokkos_work_space();
    assert(
      rt->get_executing_processor(ctx).kind()
      == KokkosProcessor<execution_space>::kind);

    using team_policy_t = TeamPolicy<execution_space>;

    for (L::PieceIteratorT<samples_t::dim, samples_t::coord_t> pit(
           smp_region, FSampleField::field_id, true);
         pit();
         pit++) {
      auto const smp = samples_t::view<execution_space, LEGION_READ_ONLY>(
        smp_region, FSampleField{}, *pit);
      auto const ts_lo = pit->lo[dim(samples_t::axes_t::timestep)];
      auto const ts_hi = pit->hi[dim(samples_t::axes_t::timestep)];
      auto const ch_lo = pit->lo[dim(samples_t::axes_t::channel)];
      auto const num_ch = pit->hi[dim(samples_t::axes_t::channel)] - ch_lo + 1;
      auto const num_chpp = num_ch * Configuration::num_polarization_products;
      K::parallel_for(
        task_name,
        team_policy_t(work_space, num_baselines * num_chpp, K::AUTO),
        KOKKOS_LAMBDA(team_policy_t::member_type const& team) {
          auto idx = team.league_rank();
          auto const bl =
            idx / num_chpp + vis_bounds.lo[dim(cbp_t::axes_t::baseline)];
          auto const [rcvX, rcvY] = baseline_to_receiver_pair(bl);
          idx %= num_chpp;
          auto const ch =
            idx / Configuration::num_polarization_products + ch_lo;
          auto const pp = idx % Configuration::num_polarization_products;
          auto vis = KX::subview(visibilities, ch, bl, pp);
          K::single(K::PerTeam(team), [&]() {
            vis() = typename decltype(vis)::value_type{0};
          });
          team.team_barrier();
          auto smpX =
            KX::subview(smp, ch, rcvX, cbp_t::polarization1_index(pp), K::ALL);
          auto smpY =
            KX::subview(smp, ch, rcvY, cbp_t::polarization0_index(pp), K::ALL);

          SampleCorrelationField::kvalue_t sprod{0};
          K::parallel_reduce(
            K::TeamThreadRange(team, ts_lo, ts_hi + 1),
            [&](auto ts, SampleCorrelationField::kvalue_t& sprod_l) {
              sprod_l += prod(smpX(ts), smpY(ts));
            },
            sprod);

          K::single(K::PerTeam(team), [&]() { vis() += sprod; });
        });
    }
  }
#else  // !RCP_USE_KOKKOS
  static void
  serial_body(
    L::Task const* /*task*/,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) {

    assert(regions.size() == 2);

    auto& vis_region = regions[visibilities_region_index];
    cbp_t::rect_t vis_bounds = vis_region;
    assert(vis_bounds.lo[dim(cbp_t::axes_t::polarization_product)] == 0);
    assert(
      vis_bounds.hi[dim(cbp_t::axes_t::polarization_product)]
      == Configuration::num_polarization_products - 1);
    auto visibilities =
      cbp_t::values<LEGION_WRITE_ONLY>(vis_region, SampleCorrelationField{});

    auto& smp_region = regions[samples_region_index];
    samples_t::rect_t smp_bounds = smp_region;
    auto samples =
      samples_t::values<LEGION_READ_ONLY>(smp_region, FSampleField{});

    cbp_t::coord_t vpt[cbp_t::dim];
    vpt = vis_bounds.lo;
    visibilities[L::Point<cbp_t::dim, coord_t>{vpt}] =
      SampleCorrelationField::value_t{0};
    for (auto&& ch : range(smp_bounds, samples_t::axes_t::channel)) {
      vpt[dim(cbp_t::axes_t::channel)] = ch;
      for (cbp_t::coord_t
             rcvX = smp_bounds.lo[dim(samples_t::axes_t::receiver)],
             bl = 0;
           rcvX <= smp_bounds.hi[dim(samples_t::axes_t::receiver)];
           ++rcvX) {
        for (cbp_t::coord_t rcvY =
               smp_bounds.lo[dim(samples_t::axes_t::receiver)];
             rcvY <= rcvX;
             ++rcvY, ++bl) {
          vpt[dim(cbp_t::axes_t::baseline)] = bl;
          for (auto&& polY :
               range(smp_bounds, samples_t::axes_t::polarization)) {
            for (auto&& polX :
                 range(smp_bounds, samples_t::axes_t::polarization)) {
              vpt[dim(cbp_t::axes_t::polarization_product)] =
                cbp_t::polarization_product_index({polX, polY});
              for (auto&& ts : range(smp_bounds, samples_t::axes_t::timestep)) {
                visibilities[L::Point<cbp_t::dim, coord_t>{vpt}] += prod(
                  samples[{ch, rcvX, polX, ts}], samples[{ch, rcvY, polY, ts}]);
              }
            }
          }
        }
      }
    }
  }
#endif // RCP_USE_KOKKOS

  template <TaskVariant Variant>
  static auto
  body(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context ctx,
    L::Runtime* rt) -> void {

    if constexpr (Variant == TaskVariant::Serial) {
#ifndef RCP_USE_KOKKOS
      serial_body(task, regions, ctx, rt);
#elif defined(RCP_USE_KOKKOS_SERIAL)
      kokkos_body<typename task_variant_traits<Variant>::ExecutionSpace>(
        task, regions, ctx, rt);
#endif
    } else if constexpr (Variant == TaskVariant::OpenMP) {
      kokkos_body<typename task_variant_traits<Variant>::ExecutionSpace>(
        task, regions, ctx, rt);
    } else {
      static_assert(Variant == TaskVariant::Cuda);
#if defined(RCP_USE_CUDA) && defined(SYMCAM_USE_TENSOR_CORE_CORRELATOR)
      tcc_body(task, regions, ctx, rt);
#elif defined(RCP_USE_KOKKOS_CUDA)
      kokkos_body<typename task_variant_traits<Variant>::ExecutionSpace>(
        task, regions, ctx, rt);
#endif
    }
  }

  template <TaskVariant Variant>
  static auto
  preregister_task_variant(L::TaskVariantRegistrar& registrar) -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar
      .add_layout_constraint_set(
        samples_region_index,
        samples_layout().constraint_id(traits::layout_style).value())
      .add_layout_constraint_set(
        visibilities_region_index,
        visibilities_layout().constraint_id(traits::layout_style).value());
    L::Runtime::preregister_task_variant<body<Variant>>(registrar, task_name);
  }

  template <TaskVariant Variant>
  static auto
  register_task_variant(L::Runtime* rt, L::TaskVariantRegistrar& registrar)
    -> void {
    static_assert(enabled_variants.contains(task_variant<Variant>{}));
    using traits = task_variant_traits<Variant>;
    registrar.set_leaf();
    registrar
      .add_layout_constraint_set(
        samples_region_index,
        samples_layout().constraint_id(rt, traits::layout_style).value())
      .add_layout_constraint_set(
        visibilities_region_index,
        visibilities_layout().constraint_id(rt, traits::layout_style).value());
    rt->register_task_variant<body<Variant>>(registrar);
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID CrossCorrelateTask::task_id;

} // end namespace rcp::symcam

CrossCorrelate::CrossCorrelate(
  L::Context ctx,
  L::Runtime* rt,
  samples_t::index_space_t samples_index_space,
  cbp_t::index_space_t visibilities_index_space) {

  assert([&]() {
    auto samples_domain = rt->get_index_space_domain(samples_index_space);
    auto visibilities_domain =
      rt->get_index_space_domain(visibilities_index_space);
    // the following reflect limitations of the implementation, not the
    // partitioning
    return samples_domain.dense() && visibilities_domain.dense();
  }());

  L::IndexSpaceT<2, coord_t> cs; // common color space of following partitions

  m_samples_index_partition = samples_t::create_partition_by_slicing(
    ctx,
    rt,
    samples_index_space,
    {{samples_t::axes_t::channel,
      DataPacketConfiguration::num_channels_per_packet}});
  // visibilities index partition
  m_visibilities_index_partition = cbp_t::create_partition_by_slicing(
    ctx,
    rt,
    visibilities_index_space,
    {{cbp_t::axes_t::channel,
      DataPacketConfiguration::num_channels_per_packet}});

  m_launcher = L::IndexTaskLauncher(
    CrossCorrelateTask::task_id,
    rt->get_index_partition_color_space(m_samples_index_partition),
    L::UntypedBuffer(),
    L::ArgumentMap());
}

void
CrossCorrelate::launch(
  L::Context ctx,
  L::Runtime* rt,
  samples_t::LogicalRegion samples,
  cbp_t::LogicalRegion visibilities,
  L::Predicate pred) {

  m_launcher.region_requirements.resize(CrossCorrelateTask::num_regions);
  m_launcher
    .region_requirements[CrossCorrelateTask::visibilities_region_index] =
    cbp_t::LogicalPartition{
      rt->get_logical_partition(visibilities, m_visibilities_index_partition)}
      .requirement(
        0,
        LEGION_WRITE_ONLY,
        LEGION_EXCLUSIVE,
        visibilities,
        StaticFields{SampleCorrelationField{}});
  m_launcher.region_requirements[CrossCorrelateTask::samples_region_index] =
    samples_t::LogicalPartition{
      rt->get_logical_partition(samples, m_samples_index_partition)}
      .requirement(
        0,
        LEGION_READ_ONLY,
        LEGION_EXCLUSIVE,
        samples,
        StaticFields{FSampleField{}});
  m_launcher.predicate = pred;
  rt->execute_index_space(ctx, m_launcher);
}

auto
CrossCorrelate::preregister_tasks(L::TaskID tid) -> void {
  PortableTask<CrossCorrelateTask>::preregister_task_variants(tid);
}

auto
CrossCorrelate::register_tasks(L::Runtime* rt, L::TaskID tid) -> void {
  PortableTask<CrossCorrelateTask>::register_task_variants(rt, tid);
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
