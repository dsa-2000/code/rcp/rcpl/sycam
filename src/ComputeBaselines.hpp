// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BaselineRegion.hpp"
#include "ReceiverRegion.hpp"

namespace rcp::symcam {

/*! \brief launcher for task to compute baselines */
class ComputeBaselines {
public:
  using rcv_t = ReceiverRegion;
  using bl_t = BaselineRegion;

  /*! \brief default constructor */
  ComputeBaselines();

  /*! \brief copy constructor (defaulted) */
  ComputeBaselines(ComputeBaselines const&) = default;

  /*! \brief move constructor (defaulted) */
  ComputeBaselines(ComputeBaselines&&) noexcept = default;

  /*! \brief destructor (defaulted) */
  ~ComputeBaselines() = default;

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(ComputeBaselines const&) -> ComputeBaselines& = default;

  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ComputeBaselines&&) -> ComputeBaselines& = default;

  /*! \brief launch task */
  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    rcv_t::LogicalRegion receivers,
    bl_t::LogicalRegion baselines) -> void;

  /*! \brief pre-register task with Legion runtime */
  static auto
  preregister_tasks(L::TaskID tid) -> void;

  /*! \brief register task with Legion runtime */
  static auto
  register_tasks(L::Runtime* rt, L::TaskID tid) -> void;
};
} // namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
