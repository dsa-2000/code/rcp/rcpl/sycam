// Copyright 2023-2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "SubarrayEvent.hpp"

/*! \file ImagingEvent.hpp
 *
 * Base class for event with a timestamp, a subarray id and an imaging id for
 * payload.
 */

namespace rcp::symcam::st {

/*! \brief error representing failure to parse imaging id value */
struct ImagingIdParseError : public JSONError {
  ImagingIdParseError(State::img_id_t id)
    : JSONError(
        std::string("Failed to parse imaging id value '") + std::to_string(id)
        + "'") {}
};

struct InvalidImagingId : public JSONError {
  InvalidImagingId(State::img_id_t id)
    : JSONError("Invalid imaging id, value " + std::to_string(id)) {}
};

struct JSONImagingEvent : public JSONSubarrayEvent {

  /*! \brief JSON imaging event id value member tag */
  static constexpr char const* imaging_id_tag = "id";

  /*! \brief test for imaging id member */
  [[nodiscard]] static auto
  has_imaging_id(nlohmann::json const& js) noexcept -> bool;

  /*! \brief get imaging id value */
  [[nodiscard]] static auto
  get_imaging_id(nlohmann::json const& js) -> nlohmann::json const&;

  /*! \brief get imaging id value */
  [[nodiscard]] static auto
  get_imaging_id(nlohmann::json& js) -> nlohmann::json&;

  /*! \brief test for valid format */
  [[nodiscard]] static auto
  is_valid(nlohmann::json const& js) noexcept -> bool;
};

template <typename Event>
class ImagingEvent;

// forward declaration of from_json() for ImagingEvent<Event> types
template <typename Event>
auto
from_json(nlohmann::json const& js, ImagingEvent<Event>& evt) -> void;

template <typename Event>
class ImagingEvent : public SubarrayEvent<Event> {

  ExtendedState::img_id_t m_img_id{-1};

  friend auto
  from_json<>(nlohmann::json const& js, ImagingEvent<Event>& evt) -> void;

public:
  /*! \brief default constructor (defaulted) */
  ImagingEvent() noexcept = default;
  /*! \brief constructor from subarray (name), imaging id, and timestamp */
  ImagingEvent(
    subarray_name_t const& subarray_name,
    ExtendedState::img_id_t const& id,
    StreamClock::timestamp_t const& ts) noexcept
    : SubarrayEvent<Event>(subarray_name, ts), m_img_id(id) {}
  /*! \brief copy constructor (defaulted) */
  ImagingEvent(ImagingEvent const&) noexcept = default;
  /*! \brief move constructor (defaulted) */
  ImagingEvent(ImagingEvent&&) noexcept = default;
  /*! \brief copy constructor with updated timestamp */
  ImagingEvent(ImagingEvent const& event, StreamClock::timestamp_t const& ts)
    : SubarrayEvent<Event>(event, ts), m_img_id(event.m_img_id) {}
  /*! \brief move constructor with updated timestamp */
  ImagingEvent(ImagingEvent&& event, StreamClock::timestamp_t const& ts)
    : SubarrayEvent<Event>(std::move(event), ts), m_img_id(event.m_img_id) {}

  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(ImagingEvent const&) -> ImagingEvent& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(ImagingEvent&&) noexcept -> ImagingEvent& = default;
  /*! \brief destructor (defaulted) */
  ~ImagingEvent() override = default;

  [[nodiscard]] auto
  imaging_id() const -> ExtendedState::img_id_t const& {
    return m_img_id;
  }

protected:
  [[nodiscard]] auto
  imaging_id() -> ExtendedState::img_id_t& {
    return m_img_id;
  }
};

/*! \brief convert ImagingEvent instance to JSON
 *
 * Form defined by nlohmann::json
 */
template <typename Event>
auto
to_json(nlohmann::json& js, ImagingEvent<Event> const& evt) -> void {
  js = {
    {JSONValue::meta_tag_name, Event::class_tag},
    {JSONValue::meta_value_name,
     {{JSONImagingEvent::timestamp_tag, to_string(evt.timestamp())},
      {JSONImagingEvent::imaging_id_tag, evt.imaging_id()},
      {JSONImagingEvent::subarray_tag, evt.name().data()}}}};
}

/*! \brief start imaging event
 *
 * Sets "imaging" state to "true"
 */
struct StartImaging : public ImagingEvent<StartImaging> {

  static constexpr char const* class_tag = "StartImaging";

  /*! \brief inherited constructors */
  using ImagingEvent<StartImaging>::ImagingEvent;

  /*! \brief set state's "imaging" status to true
   *
   * \param st state instance
   *
   * \return copy of st, but with imaging status for affected id altered
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState;
};

/*! \brief stop imaging event
 *
 * Sets "imaging" state to "false"
 */
struct StopImaging : public SubarrayEvent<StopImaging> {

  static constexpr char const* class_tag = "StopImaging";

  /*! \brief inherited constructors */
  using SubarrayEvent<StopImaging>::SubarrayEvent;

  /*! \brief set state's "imaging" status to false
   *
   * \param st state instance
   *
   * \return copy of st, but with imaging status for affected id altered
   */
  [[nodiscard]] auto
  apply_to_no_ts(ExtendedState&& st) const -> ExtendedState;
};

/*! \brief convert JSON value to ImagingEvent instance
 *
 * Form defined by nlohmann::json
 */
template <typename Event>
auto
from_json(nlohmann::json const& js, ImagingEvent<Event>& evt) -> void {
  if (!JSONValue::is_valid(js))
    throw JSONValueFormatError(js);

  auto tag = JSONValue::get_tag(js);
  if (tag != std::string_view(Event::class_tag))
    throw BadEventTagError(Event::class_tag, tag.c_str());

  auto jsval = JSONValue::get_value(js);
  if (!JSONImagingEvent::is_valid(jsval))
    throw JSONValueFormatError(js);

  auto const timestamp_str =
    JSONImagingEvent::get_timestamp(jsval).get<std::string>();
  auto const maybe_timestamp =
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    parse_timestamp(reinterpret_cast<char8_t const*>(timestamp_str.c_str()));
  if (!maybe_timestamp)
    throw TimestampParseError(timestamp_str.c_str());
  evt.timestamp() = *maybe_timestamp;

  evt.imaging_id() = JSONImagingEvent::get_imaging_id(jsval).get<int>();
  if (evt.imaging_id() < 0)
    throw ImagingIdParseError(evt.imaging_id());

  auto const subarray_str =
    JSONImagingEvent::get_subarray(jsval).get<std::string>();
  if (subarray_str.size() >= subarray_name_t{}.size())
    throw SubarrayNameLengthError(subarray_str);
  std::ranges::fill(evt.name(), '\0');
  std::ranges::copy(subarray_str, evt.name().data());
}

} // end namespace rcp::symcam::st

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
