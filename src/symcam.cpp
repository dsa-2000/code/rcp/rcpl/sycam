// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent

/*! \file symcam.cpp
 *
 * Main application code for symcam, an implementation of the DSA-2000 RCP
 * imaging pipeline code.
 */
#include "Configuration.hpp"
#include "Tasks.hpp"
#include "libsymcam.hpp"

#include <bark/log.hpp>
#include <bits/ranges_algo.h>
#include <bits/ranges_algobase.h>
#include <memory>
#include <ranges>
#include <rcp/FSamplesRegion.hpp>
#include <rcp/GetConfig.hpp>
#include <rcp/GetEpoch.hpp>
#include <rcp/Mapper.hpp>

#include "BaselineChannelRegion.hpp"
#include "BaselineRegion.hpp"
#include "BsetChannelBaselinePolprodRegion.hpp"
#include "CFRegion.hpp"
#include "GridRegion.hpp"

#include "CaptureFSamples.hpp"
#include "ComputeBaselines.hpp"
#include "ComputeUVW.hpp"
#include "ComputeVisibilitiesPartitions.hpp"
#include "ComputeVisibilityWeights.hpp"
#include "CrossCorrelate.hpp"
#include "EventProcessing.hpp"
#include "EventScript.hpp"
#include "ExportGrid.hpp"
#include "FFTWSupport.hpp"
#include "FlagVisibilities.hpp"
#include "GetCFValues.hpp"
#include "GetReceiverPositions.hpp"
#include "GridFourierTransform.hpp"
#include "GridVisibilitiesOnImage.hpp"
#include "GridVisibilitiesOnTiles.hpp"
#include "NormalizeVisibilities.hpp"
#include "RecordVisibilities.hpp"
#include "ReportFillFraction.hpp"
#include "StreamState.hpp"
#ifdef RCP_USE_CUDA
#include "LoadCUDALibraries.hpp"
#endif
#include "VisProcessing.hpp"

#include <chrono>
#include <cmath>
#include <optional>
#include <tuple>
#include <type_traits>

namespace rcp::symcam {

// some consistency checks in field values
static_assert(
  std::is_same_v<
    BaselineChannelProjectionField::value_t,
    L::Point<BaselineChannelRegion::dim, BaselineChannelRegion::coord_t>>);

static_assert(std::is_same_v<
              UVWsSubarrayPartitionField::value_t,
              BaselineChannelRegion::index_partition_t>);

static_assert(std::is_same_v<
              UVWsImagePartitionField::value_t,
              BaselineChannelRegion::index_partition_t>);

static_assert(std::is_same_v<
              UVWsTilePartitionField::value_t,
              BaselineChannelRegion::index_partition_t>);

static_assert(std::is_same_v<CFIndexField::value_t, CFRegion::coord_t>);

static_assert(std::is_same_v<
              VisibilitiesSubarrayPartitionField::value_t,
              BsetChannelBaselinePolprodRegion::index_partition_t>);

static_assert(std::is_same_v<
              VisibilitiesTilePartitionField::value_t,
              BsetChannelBaselinePolprodRegion::index_partition_t>);

static_assert(std::is_same_v<
              VisProjectionField::value_t,
              L::Rect<
                BsetChannelBaselinePolprodRegion::dim,
                BsetChannelBaselinePolprodRegion::coord_t>>);

static_assert(std::is_same_v<
              GridTileMappingField::value_t,
              L::Point<GridTileRegion::dim, GridTileRegion::coord_t>>);

static_assert(
  std::is_same_v<GridTileField::value_t, L::Rect<2, GridRegion::coord_t>>);

/*! \brief main task */
struct RadioCamera {

  using bc_t = BaselineChannelRegion;
  using cbp_t = BsetChannelBaselinePolprodRegion;
  using state_t = st::StateRegion;
  using part_t = DataPartitionsRegion;

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::TaskID task_id; //!< task id

  /*! \brief compound type for timestamp, visibilities region, and state
   *         predicates
   */
  struct FutureVisibilities {
    RegionPair<cbp_t::LogicalRegion> visibilities;
    RegionPair<bc_t::LogicalRegion> uvws;
    std::array<RegionPair<state_t::LogicalRegion>, 3> stream_states;
    L::Predicate is_alive;
  };

  /*! \brief helper class to wrap parameters
   *
   * Introduces member variables so that some functions don't need
   * to pass a large number of arguments.
   */
  class RC {

    /*! \brief FSamplesRegionMetadata type alias */
    using fsmp_t = FSamplesRegion::LogicalRegion;
    /*! \brief type alias for a timestamp and samples region */
    using ts_fsmp_t = std::tuple<StreamClock::external_time, fsmp_t>;
    /*! \brief type alias for a timestamp and state region (pair) */
    using ts_state_t = std::
      tuple<StreamClock::external_time, RegionPair<state_t::LogicalRegion>>;

    L::Context m_ctx; //!< Legion context
    L::Runtime* m_rt; //!< Legion runtime

    Configuration m_config;           //!< application configuration
    st::EventScript m_initial_events; /*!< initial events script */

    // generator instances
    /*! \brief baselines region generator */
    DenseArrayRegionGenerator<BaselineRegion> m_baseline_region_generator;
    BaselineRegion::LogicalRegion m_baselines;
    /*! \brief samples region generator */
    DenseArrayRegionGenerator<FSamplesRegion> m_fsamples_generator;
    // generator for single point StateRegion regions
    DenseArrayRegionGenerator<state_t> m_state_gen;

    /*! \brief visibilities region generator */
    DynamicDenseArrayRegionGenerator<BsetChannelBaselinePolprodRegion>
      m_vis_generator;
    decltype(m_vis_generator)::spec_t::dynamic_bounds_t m_vis_bounds;

    // task launcher instances
    /*! \brief data capture task launcher */
    CaptureFSamples m_capture_fsamples;
    /*! \brief sample values cross-correlation task launcher */
    CrossCorrelate m_cross_correlate;
    /*! \brief sample flags (weights) cross-correlation task launcher */
    ComputeVisibilityWeights m_compute_visibility_weights;
    /*! \brief visibility normalization task launcher */
    NormalizeVisibilities m_normalize_visibilities;
    /*! \brief compute visibilities partition task launcher */
    std::shared_ptr<ComputeVisibilitiesPartitions>
      m_compute_visibilities_partitions;
    /*! \brief UVW computation task launcher */
    ComputeUVW m_compute_uvw;
    /*! \brief pre-calibration visibility flagger */
    std::shared_ptr<FlagVisibilitiesStub> m_flag_visibilities;
    /*! \brief CF retrieval task launchers
     *
     * One per imager instance.
     */
    std::shared_ptr<std::vector<GetCFValues>> m_get_cfs;
    /*! \brief imaging task launchers
     *
     * One per imager instance.
     */
    std::shared_ptr<std::vector<GridVisibilitiesOnImage>> m_imagers;
    /*! \brief image fft plans */
    std::shared_ptr<std::vector<
      std::array<GridFourierTransform::Plan, Configuration::max_num_subarrays>>>
      m_fft_plans;
    /*! \brief futures from calls to GridFourierTransform::make_plan */
    std::vector<L::Future> m_fft_plan_futures;

    /*! \brief grid exporter launchers
     *
     * One per imager instance.
     */
    std::shared_ptr<std::vector<ExportGrid>> m_exporters;

    /*! \brief visibilities recording launcher */
    std::shared_ptr<RecordVisibilities> m_record_visibilities;

    std::unique_ptr<VisProcessing> m_vis_processing;

    /*! \brief fftw support*/
    FFTWSupport m_fftw_support;

    /*! \brief result of most recent call to m_fftw_support.import_wisdom() */
    L::Future m_imported_wisdom;

    /*! \brief result of most recent call to m_fftw_support.export_wisdom() */
    L::Future m_exported_wisdom;

    // state maintained by run loop
    /*! \brief pipeline states, resolved when capture_task() completes */
    std::vector<L::Future> m_live_states;

    /*! \brief event log instance */
    std::shared_ptr<st::EventLog> m_event_log;
    /*! \brief event log mutex */
    std::shared_ptr<std::mutex> m_event_log_mtx;

    std::shared_ptr<st::EventProcessing> m_event_processing;

    L::coord_t m_shard_point;

    L::coord_t m_num_shards;

  public:
    /*! \brief constructor
     *
     * \param task task instance
     * \param ctx Legion context
     * \param rt Legion runtime
     */
    RC(L::Task const* task, L::Context ctx, L::Runtime* rt)
      : m_ctx(ctx), m_rt(rt) {

      assert(
        [&]() -> bool { return task->get_shard_domain().get_dim() == 1; }());
      m_shard_point = task->get_shard_point()[0];
      m_num_shards = task->get_total_shards();

      // get configuration
      {
        GetConfig get_config(m_ctx, m_rt);
        m_config = get_config.launch(m_ctx, m_rt);
        get_config.destroy(m_ctx, m_rt);
      }
      if (m_shard_point == 0 && m_config.do_show())
        std::cout << m_config << '\n';
      if (!m_config.do_run())
        return; // (m_config.error.size() > 0) ? EXIT_FAILURE : EXIT_SUCCESS;

      // log with pipeline instance id only if RCP instance id is not empty
      if (!m_config.rcp_instance_id.empty())
        init_log_sink(m_config.pipeline_instance_id());

      // we might need some fftw support early, which also requires m_state_gen
      // to be initialized
      m_state_gen = DenseArrayRegionGenerator<state_t>(
        m_ctx,
        m_rt,
        {{state_t::axes_t::shard,
          state_t::range_t::right_bounded(m_num_shards - 1)}});
      m_fftw_support = FFTWSupport(m_ctx, m_rt, m_shard_point, m_num_shards);

      // parse initial events script
      m_event_log = std::make_shared<st::EventLog>(
        st::ExtendedState{m_ctx, m_rt, m_config}, m_shard_point == 0);
      m_event_log_mtx = std::make_shared<std::mutex>();
      if (!m_config.initial_events_script.empty()) {
        try {
          m_initial_events = st::EventScript{nlohmann::json::parse(
            m_config.initial_events_script, nullptr, true, true)};
        } catch (std::exception const& err) {
          m_config.error =
            std::string("Initial events script contains invalid JSON: ")
            + err.what();
          log().error(m_config.error);
          return;
        }
        // gather initial events that are in the relative past
        auto [past, future] = m_initial_events.partition_relative_past_events();
        std::vector<std::shared_ptr<StreamStateEventBase<st::ExtendedState>>>
          past_events;
        std::ranges::transform(
          past, std::back_inserter(past_events), [](auto&& ev) {
            return std::shared_ptr(st::get_event(ev));
          });
        // record past events and roll up the event log to apply them
        *m_event_log = std::move(*m_event_log).record_immediate(past_events);
        *m_event_log =
          std::move(*m_event_log).rollup(m_event_log->next_timestamp());
        // replace m_initial_events with future events
        m_initial_events = future;
      }

      rcp::dc::channel_type num_shards = m_num_shards;
      // use term "stream" to indicate all packets with a common starting
      // channel (channel_offset in FPacket)
      rcp::dc::channel_type num_streams =
        m_config.sample_channel_range_size
        / DataPacketConfiguration::num_channels_per_packet;
      auto streams_by_shard = rcp::block_distribute(num_streams, num_shards);

#ifdef RCP_USE_CUDA
      LoadCUDALibraries::launch(
        m_ctx,
        m_rt,
        m_config.xc.integration_factor,
        DataPacketConfiguration::num_channels_per_packet);
#endif

      // region generators
      m_baseline_region_generator = DenseArrayRegionGenerator<BaselineRegion>(
        m_ctx, m_rt, BaselineRegion::dynamic_bounds_t{});
      m_baselines = m_baseline_region_generator.make(m_ctx, m_rt);
      m_fsamples_generator = DenseArrayRegionGenerator<FSamplesRegion>(
        m_ctx,
        m_rt,
        FSamplesRegion::dynamic_bounds_t{
          {FSamplesRegion::axes_t::channel,
           IndexInterval<FSamplesRegion::coord_t>::bounded(
             m_config.sample_channel_range_offset,
             m_config.sample_channel_range_offset
               + m_config.sample_channel_range_size - 1)},
          {FSamplesRegion::axes_t::timestep,
           IndexInterval<FSamplesRegion::coord_t>::right_bounded(
             m_config.capture_interval().count() - 1)}});
      m_vis_generator = DynamicDenseArrayRegionGenerator<cbp_t>(2);
      assert(m_config.max_correlation_timesteps_per_capture_interval() == 1);
      m_vis_bounds = {
        {cbp_t::axes_t::channel,
         IndexInterval<cbp_t::coord_t>::bounded(
           m_config.sample_channel_range_offset,
           m_config.sample_channel_range_offset
             + m_config.sample_channel_range_size - 1)},
        {cbp_t::axes_t::baseline,
         IndexInterval<cbp_t::coord_t>::right_bounded(
           Configuration::num_baselines - 1)}};
      assert(m_vis_generator.index_space(m_ctx, m_rt, m_vis_bounds));
      cbp_t::index_space_t vis_index_space =
        // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
        m_vis_generator.index_space(m_ctx, m_rt, m_vis_bounds).value();

      // task launchers
      m_capture_fsamples = CaptureFSamples(
        m_ctx,
        m_rt,
        m_config,
        m_shard_point,
        streams_by_shard,
        m_fsamples_generator.index_space(),
        m_fsamples_generator.field_space(),
        Mapper::level0_sharding_tag | Mapper::high_priority_tag);
      m_cross_correlate = CrossCorrelate(
        m_ctx, m_rt, m_fsamples_generator.index_space(), vis_index_space);
      m_compute_visibility_weights = ComputeVisibilityWeights(
        m_ctx, m_rt, m_fsamples_generator.index_space(), vis_index_space);
      m_normalize_visibilities =
        NormalizeVisibilities(m_ctx, m_rt, m_config, vis_index_space);
      m_compute_uvw = ComputeUVW(m_ctx, m_rt, m_config);

      // create event_processing
      m_event_processing = std::make_shared<st::EventProcessing>(
        m_ctx,
        m_rt,
        m_baselines.get_index_space(),
        m_event_log,
        m_event_log_mtx,
        m_shard_point,
        m_num_shards);
      handle_point_events(); // point events at start

      m_get_cfs = std::make_shared<std::remove_cvref_t<decltype(*m_get_cfs)>>();
      m_imagers = std::make_shared<std::remove_cvref_t<decltype(*m_imagers)>>();
      m_fft_plans =
        std::make_shared<std::remove_cvref_t<decltype(*m_fft_plans)>>();
      m_exporters =
        std::make_shared<std::remove_cvref_t<decltype(*m_exporters)>>();
      GridFourierTransform::Plan::id_t idx = 0;
      for (auto&& img : m_config.imaging_configurations()) {
        m_get_cfs->emplace_back(m_ctx, m_rt, img);
        m_imagers->emplace_back(
          m_ctx,
          m_rt,
          m_config,
          img,
          m_get_cfs->back().mueller_indexes_region());
        {
          // make FFT plans for each image first for subarray 0, then for the
          // other subarrays using Plan.substitute_grids()
          auto grids = m_imagers->back().grids();
          auto grid0 = grids[0];
          auto ip = GridRegion::create_partition_by_slicing(
            m_ctx,
            m_rt,
            grid0.get_index_space(),
            {{GridRegion::axes_t::channel, 1},
             {GridRegion::axes_t::stokes, 1},
             {GridRegion::axes_t::w_plane, 1}});
          auto lp = GridRegion::LogicalPartition{m_rt->get_logical_partition(
            GridRegion::logical_region_t{grid0}, ip)};
          auto plan0 = GridFourierTransform::Plan(m_rt, img, idx++, 1, lp);
          std::
            array<GridFourierTransform::Plan, Configuration::max_num_subarrays>
              plans{};
          plans[0] = plan0;
          for (auto&& plan : std::views::drop(plans, 1)) {
            auto idx = std::distance(plans.data(), &plan);
            plan = plan0.substitute_grids(m_rt, grids[idx], grids[idx]);
          }
          m_fft_plans->emplace_back(plans);
          std::ranges::copy(
            GridFourierTransform::make_plan(
              m_ctx, m_rt, m_fft_plans->back().front(), m_imported_wisdom),
            std::back_inserter(m_fft_plan_futures));
        }
        m_exporters->emplace_back(m_ctx, m_rt, m_config, img);
      }
      m_record_visibilities = std::make_shared<RecordVisibilities>(
        m_ctx, m_rt, m_config, m_shard_point, m_num_shards);
    }

    RC(RC const&) = delete;

    RC(RC&&) = delete;

    auto
    operator=(RC const&) -> RC& = delete;

    auto
    operator=(RC&&) -> RC& = delete;

  private:
    /*! \brief test whether pipeline is still alive
     *
     * Also removes completed Futures from m_live_states.
     *
     * \return result of Future for latest completed m_live_states element
     */
    auto
    alive() -> bool {
      bool result = true;
      while (!m_live_states.empty() && m_live_states.front().is_ready()) {
        result = result && m_live_states.front().get<bool>();
        m_live_states.erase(m_live_states.begin());
      }
      log().debug(
        "live captures", bark::val_field_t{"n", m_live_states.size()});
      return result;
    }

    /*! \brief launch some data capture tasks
     *
     * \param live predicate passed to capture tasks, used as condition on
     *        execution of capture tasks
     *
     * \return future for the total number of samples captured by the task
     */
    auto
    capture_task(
      StreamClock::external_time const& ts,
      FSamplesRegion::LogicalRegion const& fsamples,
      L::Predicate const& live) -> L::Future {

      fsamples.fill_fields(
        m_ctx,
        m_rt,
        std::nullopt,
        live,
        std::make_tuple(FSampleField{}, FSampleField::value_t{0}),
        std::make_tuple(FWeightField{}, FWeightField::value_t{0}));

      // auto [t0, fsamples] = FIXME...remove commented section
      //   m_fcapture_generator.next(m_ctx, m_rt, rcp::dc::fsample_type{0}, 0);
      [[maybe_unused]] auto now = std::chrono::system_clock::now();
      log().debug<std::string, std::string>(
        "got fcapture region", {"t0", show(ts)}, {"diff", showt(ts - now)});
      auto filled = m_rt->reduce_future_map(
        m_ctx,
        m_capture_fsamples.launch(m_ctx, m_rt, fsamples, ts, live),
        L::SumReduction<std::size_t>::REDOP_ID);
      return filled;
    }

    /*! \brief process a sequence of visibilities
     *
     * This function should be called after any integration of visibilities
     *
     * \param baselines baselines region
     * \param fvs vector of FutureVisibilities values
     */
    void
    process_vis(
      BaselineRegion::LogicalRegion const& baselines,
      std::vector<FutureVisibilities>&& fvs) {

      for (auto&& fv : fvs) {
        m_compute_uvw.launch(
          m_ctx,
          m_rt,
          m_baselines,
          {fv.stream_states[1], fv.stream_states[2]},
          fv.uvws,
          fv.is_alive);
        if (m_config.child_task_per_integration)
          m_vis_processing->launch(
            m_ctx,
            m_rt,
            fv.visibilities,
            fv.uvws,
            fv.stream_states,
            fv.is_alive);
        else
          m_vis_processing->launch(
            m_ctx,
            m_rt,
            fv.visibilities,
            fv.uvws,
            fv.stream_states,
            st::EventProcessing::subarray_predicates(
              m_ctx, m_rt, fv.stream_states),
            fv.is_alive);
      }
    }

    /*! \brief process a region of captured F-engine samples
     *
     * \param baselines baselines region
     * \param ts_fsmp timestamp and data capture region
     * \param is_alive "pipeline is live" predicate
     * \param stream_state stream state for the samples at visibility timestep
     */
    void
    process_fsamples(
      BaselineRegion::LogicalRegion const& baselines,
      FSamplesRegion::LogicalRegion const& fsamples,
      cbp_t::LogicalRegion const& visibilities,
      bc_t::LogicalRegion const& uvws,
      std::vector<RegionPair<state_t::LogicalRegion>> const& stream_states,
      L::Predicate const& is_alive) {

      m_cross_correlate.launch(m_ctx, m_rt, fsamples, visibilities, is_alive);
      m_compute_visibility_weights.launch(
        m_ctx, m_rt, fsamples, visibilities, is_alive);
      std::vector<FutureVisibilities> fvs;
      m_normalize_visibilities.launch(m_ctx, m_rt, visibilities, is_alive);
      // the simple case of only one timestep
      fvs.emplace_back(
        visibilities,
        uvws,
        std::array{stream_states[0], stream_states[1], stream_states[2]},
        is_alive);
      process_vis(baselines, std::move(fvs));
    }

    auto
    next_state_regions(
      std::deque<ts_state_t>& queue,
      DenseArrayRegionGenerator<state_t>& gen,
      StreamClock::external_time const& capture_ts,
      long num_total_states) -> std::vector<ts_state_t> {

      std::vector<ts_state_t> result;
      // first two states regions are the same as the previous last two
      assert(!queue.empty());
      result.push_back(queue.front());
      queue.pop_front();
      if (queue.empty()) // can occur at start
        queue.emplace_back(capture_ts, gen.make(m_ctx, m_rt));
      result.push_back(queue.front());
      queue.pop_front();
      // add states_per_capture_interval more states, with new timestamps
      std::ranges::generate_n(
        std::back_inserter(result),
        num_total_states - 2,
        [&, ts = capture_ts + m_config.visibility_interval()]() mutable
        -> ts_state_t {
          if (queue.empty())
            queue.emplace_back(ts, gen.make(m_ctx, m_rt));
          auto next = queue.front();
          queue.pop_front();
          std::get<0>(next) = ts;
          ts += m_config.visibility_interval();
          return next;
        });
      // push the last two states back onto the front of states_queue
      queue.push_front(result.back());
      queue.push_front(result.at(result.size() - 2));
      assert(static_cast<long>(result.size()) == num_total_states);
      return result;
    }

    static auto
    enqueue_state_regions(
      std::vector<ts_state_t> const& ts_states,
      std::deque<ts_state_t>& queue) -> void {

      std::ranges::copy(
        ts_states | std::views::take(ts_states.size() - 2)
          | std::views::filter([](auto&& ts_state) {
              return std::get<1>(ts_state).region() != state_t::LogicalRegion{};
            }),
        std::back_inserter(queue));
    }

    auto
    process_capture(
      std::vector<ts_state_t> const& ts_states,
      fsmp_t const& capture,
      BaselineRegion::LogicalRegion const& baselines,
      cbp_t::LogicalRegion const& visibilities,
      bc_t::LogicalRegion const& uvws,
      L::Future fill_trigger,
      ReportFillFraction& fill_reporter,
      L::Predicate const& is_alive) -> L::Predicate {

      L::Predicate result = is_alive;
      if (!ts_states.empty()) {
        assert(ts_states.size() > 2);

        // Roll up event log to determine states, skipping initial states in the
        // list, as they are already known. The number of states to skip depends
        // on whether the set of states is for the initial capture region or
        // not: when not the initial region, skip two, otherwise, skip one.
        bool initial_iteration =
          std::get<1>(ts_states.front()).region() == state_t::LogicalRegion{};
        // TODO: remove timestamp from ts_states, and let the fill_trigger
        // provide it (which should help illuminate the data dependency of the
        // rollup task on the timestamp value)
        m_event_processing->rollup(
          m_ctx,
          m_rt,
          std::vector(
            ts_states.begin() + (initial_iteration ? 1 : 2), ts_states.end()),
          fill_trigger);

        // extract states only from ts_states (rollup copies timestamps to those
        // regions for further reference)
        std::vector<RegionPair<state_t::LogicalRegion>> next_states;
        next_states.reserve(ts_states.size());
        std::ranges::copy(
          ts_states | std::views::transform([](auto&& ts_state) {
            return std::get<1>(ts_state);
          }),
          std::back_inserter(next_states));

        // do bookkeeping for fill reporting and monitoring live status
        result = st::EventProcessing::is_alive(
          m_ctx,
          m_rt,
          std::vector(next_states.begin() + 1, next_states.end() - 1));
        m_live_states.push_back(m_rt->get_predicate_future(m_ctx, is_alive));
        fill_reporter.record(m_ctx, m_rt, fill_trigger, is_alive);
        m_imported_wisdom = m_fftw_support.import_wisdom(
          m_ctx,
          m_rt,
          next_states[1],
          st::EventProcessing::do_fftw_import(m_ctx, m_rt, next_states[1]));
        m_exported_wisdom = m_fftw_support.export_wisdom(
          m_ctx,
          m_rt,
          next_states[1],
          st::EventProcessing::do_fftw_export(m_ctx, m_rt, next_states[1]));

        // now we can process the samples in the capture region
        process_fsamples(
          baselines, capture, visibilities, uvws, next_states, is_alive);
      }
      return result;
    }

    auto
    handle_point_events() -> void {
      auto st = RegionPair{m_state_gen.make(m_ctx, m_rt)};
      m_event_processing->current_state(m_ctx, m_rt, st);
      m_imported_wisdom = m_fftw_support.import_wisdom(
        m_ctx, m_rt, st, st::EventProcessing::do_fftw_import(m_ctx, m_rt, st));
      m_exported_wisdom = m_fftw_support.export_wisdom(
        m_ctx, m_rt, st, st::EventProcessing::do_fftw_export(m_ctx, m_rt, st));
      m_rt->destroy_logical_region(m_ctx, st.region());
    }

  public:
    /*! \brief run the radio camera pipeline */
    auto
    run() -> void {

      using namespace std::chrono_literals;

      // must check the following again, as the task() function doesn't do it
      if (!m_config.do_run())
        return; // (m_config.error.size() > 0) ? EXIT_FAILURE : EXIT_SUCCESS;

      {
        auto receivers = GetReceiverPositions(m_config).launch(m_ctx, m_rt);
        ComputeBaselines compute_baselines;
        compute_baselines.launch(m_ctx, m_rt, receivers, m_baselines);
        m_rt->destroy_logical_region(m_ctx, receivers);
      }

      // expected number of F-engine samples in every capture region
      auto capture_volume =
        m_rt->get_index_space_domain(m_fsamples_generator.index_space())
          .volume();
      ReportFillFraction fill_reporter(m_config, capture_volume);
      // TODO: remove the following when the multicast stuff goes away
      // completely
      //
      // auto constexpr fast_mcast_level = 0.5F;
      // auto constexpr slow_mcast_level = 0.25F;

      constexpr auto invalid_timestamp = StreamClock::external_time::min();

      // state regions get recycled, as allowed by m_config.state_capture_offset
      std::deque<ts_state_t> states_queue;
      // seed states_queue with a single region that is a proxy for the state
      // prior to the first captured samples. This is required to be an empty
      // region in order that EventProcessing handles it correctly.
      states_queue.emplace_back(invalid_timestamp, state_t::LogicalRegion{});

      // states are needed during iteration over incoming data regions, and for
      // every such data region, we need not only the state for the interval to
      // which the region is associated, but the immediately preceding and
      // succeeding intervals, as well (for endpoint state values)
      auto const states_per_capture_interval = 3;

      // predicate for capture task is based on a prior "is_alive" state, by a
      // number of capture intervals
      log().warn_if(
        [&]() { return m_config.live_capture_offset >= 0; },
        "live-capture-offset value is non-negative: capture tasks may lose "
        "data");
      std::deque<L::Predicate> liveness_predicates;
      std::ranges::fill_n(
        std::back_inserter(liveness_predicates),
        -(m_config.live_capture_offset - 1),
        L::Predicate::TRUE_PRED);

      // States regions are filled subject to a trigger (time), which is
      // provided by incoming samples. Depending on the value of
      // m_config.state_capture_offset, the triggering samples can either lead
      // or lag the capture region associated with the states region. These
      // relations are maintained through the use of two queues, one for capture
      // regions and states, and the other for triggers (futures) that occur
      // (resp, are fulfilled) whenever some capture task completes. Timestamps
      // in these queues are capture timestamps, and primarily exist for
      // reporting purposes.
      std::deque<std::tuple<StreamClock::external_time, L::Future>>
        pending_triggers;
      std::deque<
        std::tuple<StreamClock::external_time, fsmp_t, std::vector<ts_state_t>>>
        pending_capture_states;
      if (m_config.state_capture_offset < 0)
        std::ranges::generate_n(
          std::back_inserter(pending_triggers),
          -m_config.state_capture_offset,
          [&]() -> decltype(pending_triggers)::value_type {
            return {
              invalid_timestamp,
              L::Future::from_value<CaptureFSamples::count_t>(0)};
          });
      else if (m_config.state_capture_offset > 0)
        std::ranges::generate_n(
          std::back_inserter(pending_capture_states),
          m_config.state_capture_offset,
          [&]() -> decltype(pending_capture_states)::value_type {
            return {
              invalid_timestamp, m_fsamples_generator.make(m_ctx, m_rt), {}};
          });

      // one visibility region is enough
      auto maybe_vis = m_vis_generator.make(m_ctx, m_rt, m_vis_bounds);
      assert(maybe_vis);
      // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
      auto visibilities = maybe_vis.value();

      // m_flag_visibilities needs a visibilities region to initialize it
      m_flag_visibilities =
        std::make_shared<FlagVisibilitiesStub>(m_ctx, m_rt, visibilities);

      // the uvw region shape is similar to the visibilities region shape
      auto uvws = m_compute_uvw.make(m_ctx, m_rt);
      m_compute_visibilities_partitions =
        std::make_shared<ComputeVisibilitiesPartitions>(
          m_ctx, m_rt, visibilities);

      // wait for FFT planning to complete
      for (auto&& future : m_fft_plan_futures)
        future.wait();
      m_fft_plan_futures.clear();

      m_vis_processing = std::make_unique<VisProcessing>(
        m_ctx,
        m_rt,
        m_baselines,
        m_fft_plans,
        m_compute_visibilities_partitions,
        m_flag_visibilities,
        m_record_visibilities,
        m_exporters,
        m_get_cfs,
        m_imagers);

      // get the timestamp epoch (to mark start of processing)
      auto epoch_holdoff = 2s;
      auto epoch =
        GetEpoch().launch(m_ctx, m_rt).get<GetEpoch::value_t>() + epoch_holdoff;

      // fix up initial event script by changing relative timestamps to fixed
      // timestamps based on epoch
      m_initial_events = std::move(m_initial_events).with_fixed_epoch(epoch);
      if (m_initial_events.size() > 0)
        m_event_processing->record(m_ctx, m_rt, m_initial_events);

      StreamClock::external_time capture_ts = epoch;
      //  bool fast_mcast = true;
      while (alive()) {
        // launch capture task
        auto fsamples = m_fsamples_generator.make(m_ctx, m_rt);
        auto fill =
          capture_task(capture_ts, fsamples, liveness_predicates.front());

        // get state regions with timestamps
        std::vector<ts_state_t> states = next_state_regions(
          states_queue, m_state_gen, capture_ts, states_per_capture_interval);

        // push fill onto the pending_triggers queue, and push fsamples and
        // states onto the pending_capture_states queue. Do not skip this step,
        // as it introduces the proper alignment between the capture task
        // completion and the states resolution.
        pending_triggers.emplace_back(capture_ts, fill);
        pending_capture_states.emplace_back(capture_ts, fsamples, states);

        // take front elements of both pending_triggers and
        // pending_capture_states, and process them
        auto [next_trigger_ts, next_trigger] = pending_triggers.front();
        pending_triggers.pop_front();
        auto [next_capture_ts, next_capture, next_ts_states] =
          pending_capture_states.front();
        auto is_alive = liveness_predicates.front();
        liveness_predicates.pop_front();
        pending_capture_states.pop_front();
        liveness_predicates.push_back(process_capture(
          next_ts_states,
          next_capture,
          m_baselines,
          visibilities,
          uvws,
          next_trigger,
          fill_reporter,
          is_alive));
        // return states and capture region to queues
        enqueue_state_regions(next_ts_states, states_queue);
        m_rt->destroy_logical_region(m_ctx, next_capture);

        // prepare for next iteration of the loop
        capture_ts += m_config.capture_interval();
      }
      log().debug("died");
      for (auto&& trigger : pending_triggers)
        std::get<1>(trigger).wait();
      for (auto&& capture_state : pending_capture_states) {
        auto [next_capture_ts, next_capture, next_ts_states] = capture_state;
        m_rt->destroy_logical_region(m_ctx, next_capture);
        std::vector<RegionPair<state_t::LogicalRegion>> next_states;
        next_states.reserve(next_ts_states.size());
        std::ranges::copy(
          next_ts_states | std::views::transform([](auto&& ts_state) {
            return std::get<1>(ts_state);
          }),
          std::back_inserter(next_states));
        enqueue_state_regions(next_ts_states, states_queue);
      }
      for (auto&& ts_state : states_queue)
        m_rt->destroy_logical_region(m_ctx, std::get<1>(ts_state).region());
      m_rt->issue_execution_fence(m_ctx);
      log().debug("completed");
      m_flag_visibilities->destroy(m_ctx, m_rt);
      m_rt->destroy_logical_region(m_ctx, visibilities);
    }

    /*! \brief destructor */
    ~RC() {
      if (m_config.do_run()) {
        m_baseline_region_generator.destroy(m_ctx, m_rt);
        m_compute_uvw.destroy(m_ctx, m_rt);
        m_vis_generator.destroy(m_ctx, m_rt);
        m_capture_fsamples.destroy(m_ctx, m_rt);
        for (auto&& gcfs : *m_get_cfs)
          gcfs.destroy(m_ctx, m_rt);
        for (auto&& img : *m_imagers)
          img.destroy(m_ctx, m_rt);
        m_record_visibilities->destroy(m_ctx, m_rt);
        m_compute_visibilities_partitions->destroy(m_ctx, m_rt);
        *m_event_log = std::move(*m_event_log).rollup_all();
        // point events at end -- must occur after "rollup_all()"
        handle_point_events();
        m_exported_wisdom.wait();
        m_event_processing->destroy(m_ctx, m_rt);
        m_event_log->state().destroy();
        for (auto&& future : m_fft_plan_futures)
          future.wait();
        for (auto&& plans : *m_fft_plans)
          std::ranges::for_each(
            GridFourierTransform::destroy_plan(m_ctx, m_rt, plans.front()),
            [&](auto&& future) { future.wait(); });
        m_rt->issue_execution_fence(m_ctx);
        m_fftw_support.destroy(m_ctx, m_rt);
        m_rt->destroy_logical_region(m_ctx, m_baselines);
      }
    }
  };

  /*! \brief radio camera task implementation */
  static auto
  task(
    L::Task const* task,
    std::vector<L::PhysicalRegion> const& /*regions*/,
    L::Context ctx,
    L::Runtime* rt) -> void {

    RC(task, ctx, rt).run();
  }

  /*! \brief preregister the RadioCamera task with Legion runtime */
  static auto
  preregister(L::TaskID task_id) -> void {
    RadioCamera::task_id = task_id;
    L::TaskVariantRegistrar registrar(RadioCamera::task_id, "RC variant");
    registrar.add_constraint(L::ProcessorConstraint(L::Processor::LOC_PROC));
    registrar.set_replicable();
    L::Runtime::preregister_task_variant<task>(registrar, "RC task");
  }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::TaskID RadioCamera::task_id;

} // end namespace rcp::symcam

auto
main(int argc, char* argv[]) -> int {

  namespace L = Legion;

  using namespace rcp::symcam;

  L::Runtime::initialize(&argc, &argv);

  RadioCamera::preregister(radio_camera_task_id);
  GetConfig::preregister();
  GetEpoch::preregister();
  CaptureFSamples::preregister_tasks(capture_fsamples_task_id);
  CrossCorrelate::preregister_tasks(cross_correlate_task_id);
  ComputeVisibilityWeights::preregister_tasks(
    compute_visibility_weights_task_id);
  NormalizeVisibilities::preregister_tasks(normalize_visibilities_task_id);
  ComputeBaselines ::preregister_tasks(compute_baselines_task_id);
  GetReceiverPositions ::preregister_tasks(get_receiver_positions_task_id);
  GridVisibilitiesOnTiles::preregister_tasks(gridding_task_id);
  GridVisibilitiesOnImage::preregister_tasks(
    {cf_domains_task_id, imaging_task_id, update_buffer_tracking_task_id});
  ComputeUVW::preregister_tasks(
    {compute_uvw_task_id, fill_vis_projection_field_task_id});
  GetCFValues::preregister_tasks(cf_init_task_id, cf_index_task_id);
  ReportFillFraction::preregister_tasks(report_fill_fraction_task_id);
  ExportGrid::preregister_tasks(export_grid_task_id);
  st::EventProcessing::preregister_tasks(
    {eventlog_record_events_task_id,
     eventlog_rollup_task_id,
     eventlog_query_task_id,
     eventlog_query_all_task_id,
     eventlog_current_state_task_id});
  ComputeVisibilitiesPartitions::preregister_tasks(
    {compute_partitions_by_subarray_task_id,
     compute_partitions_by_tile_task_id,
     compute_tile_mappings_task_id,
     fill_uvw_projection_field_task_id});
  GridFourierTransform::preregister_tasks(
    {grid_make_fft_plan_task_id,
     grid_fft_task_id,
     grid_destroy_fft_plan_task_id});
  RecordVisibilities::preregister_tasks(record_visibilities_task_id);
  FlagVisibilitiesStub::preregister_tasks(
    {precal_flagging_copy_task_id,
     precal_flagging_subarray_task_id,
     precal_flagging_task_id});
  FFTWSupport::preregister_tasks(
    {fftw_import_wisdom_task_id, fftw_export_wisdom_task_id});
  VisProcessing::preregister_tasks(vis_processing_task_id);
#ifdef RCP_USE_CUDA
  LoadCUDALibraries::preregister(load_cuda_libraries_task_id);
#endif
  L::Runtime::set_top_level_task_id(RadioCamera::task_id);
  rcp::Mapper::preregister();

  return L::Runtime::start(argc, argv);
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
