// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

/*! \file CaptureFSamples.hpp
 *
 * Data capture for symcam.
 */

#include "Configuration.hpp"
#include "libsymcam.hpp"
#include <rcp/FSamplesRegion.hpp>

#include <rcp/dc/DataCaptureOFI.hpp>
#include <rcp/dc/rcpdc.hpp>

#include <algorithm>
#include <array>
#include <chrono>
#include <execution>
#include <memory>
#include <optional>
#include <ranges>
#include <rcp/rcp.hpp>
#include <stdexcept>
#include <vector>

namespace rcp::symcam {

/*! \brief rcp::dc::FPacket specialization for F-engine packets */
using fpacket_type = rcp::dc::FPacket<
  std::chrono::duration_cast<StreamClock::duration>(
    DataPacketConfiguration::fsample_interval)
    .count(),
  DataPacketConfiguration::num_channels_per_packet,
  DataPacketConfiguration::num_polarizations,
  DataPacketConfiguration::num_timesteps_per_packet,
  DataPacketConfiguration::timesteps_tile_size>;

/*! \brief rcp::dc::FBlock specialization for FSamplesRegion */
using fblock_type = rcp::dc::FBlock<
  std::chrono::duration_cast<StreamClock::duration>(
    DataPacketConfiguration::fsample_interval)
    .count(),
  DataPacketConfiguration::num_receivers,
  DataPacketConfiguration::num_channels_per_packet,
  DataPacketConfiguration::num_polarizations,
  DataCaptureConfiguration::timesteps_tile_size>;

/*! type for a sequence of stream ranges
 *
 * array values are start (offset) and size of stream range
 */
using streams_def_t = std::vector<std::array<rcp::dc::channel_type, 2>>;

/*! implementation to copy F-engine values from packet to block
 */
template <rcp::dc::IsFPacket P, rcp::dc::IsFBlock B>
struct FillImpl {

  using pkt_mdspan_type = decltype(std::declval<P>().fsamples_mdspan());
  using bsmp_mdspan_type = decltype(std::declval<B>().samples_mdspan());
  using bwgt_mdspan_type = decltype(std::declval<B>().weights_mdspan());
  using size_type = typename pkt_mdspan_type::size_type;

  static_assert(P::timesteps_tile_size == B::timesteps_tile_size);

  static constexpr auto
  ts_tile_size() {
    if constexpr (P::timesteps_tile_size > 0) {
      return P::timesteps_tile_size;
    } else {
      return P::num_timesteps;
    }
  }

  static constexpr auto
  num_ts_tiles() {
    if constexpr (P::timesteps_tile_size > 0) {
      return P::mapping().num_ts_tiles();
    } else {
      return 1;
    }
  }

  template <typename T>
  static constexpr auto
  polarization_timestep_subtile_size() -> std::size_t {
    return P::num_polarizations * P::timesteps_tile_size * sizeof(T);
  }

  /*! fill function
   *
   * signature as required by rcp::dc::DataCapture
   */
  static void
  copy_packet_to_block(
    std::size_t /*num_ts*/,
    pkt_mdspan_type const& packet,
    size_type pkt_ts0,
    bsmp_mdspan_type const& samples,
    bwgt_mdspan_type const& weights,
    size_type receiver,
    size_type blk_ts0) {

    auto channels = std::views::iota(size_type{0}, packet.extent(0));
    auto num_polarizations = packet.extent(1);
    auto ts_tiles = std::views::iota(size_type{0}, num_ts_tiles());
    auto pt_range =
      std::views::iota(size_type{0}, num_polarizations * ts_tile_size());
    // TODO: without weights, could use the following if tile sizes are
    // identical in packet and block
    //
    // auto subtile_size =
    //   polarization_timestep_subtile_size<rcp::dc::fsample_type>();

    std::for_each(
      std::execution::par_unseq, // TODO: best?
      channels.begin(),
      channels.end(),
      [&](auto&& ch) {
        std::for_each(
          std::execution::par_unseq, // TODO: best?
          ts_tiles.begin(),
          ts_tiles.end(),
          [&](auto&& ts_tile) {
            auto pkt = &packet(ch, 0, pkt_ts0 + ts_tile * ts_tile_size());
            auto smp =
              &samples(receiver, ch, 0, blk_ts0 + ts_tile * ts_tile_size());
            auto wgt =
              &weights(receiver, ch, 0, blk_ts0 + ts_tile * ts_tile_size());
            // TODO: see above
            //
            // std::memcpy(smp, pkt, subtile_size);
            std::ranges::for_each(pt_range, [&](auto&& pt) {
              if (!DataPacketConfiguration::is_flagged(pkt[pt])) {
                smp[pt] = pkt[pt];
                wgt[pt] = 1;
              } else {
                smp[pt] = rcp::dc::fsample_type{0};
                wgt[pt] = 0;
              }
            });
          });
      });
  }
};

/*! \brief specialization of rcp::dc::DataCaptureOFIFactory */
using dc_factory_t = rcp::dc::DataCaptureOFIFactory<
  fpacket_type,
  fblock_type,
  DataCaptureConfiguration::max_streams_per_instance,
  FillImpl>;

/*! specialization of rcp::dc::DataCaptureOFI for rcl */
using dc_t = dc_factory_t::data_capture_type;

/*! \brief data capture pointer field */
using DataCapturePointerField = StaticField<dc_t*, 31>;

/*! \brief axes for data capture pointer region */
enum class DataCaptureAxes { shard = LEGION_DIM_0, dc };

/*! \brief data capture pointer region */
using DataCaptureRegionSpec = StaticRegionSpec<
  IndexSpec<DataCaptureAxes, DataCaptureAxes::shard, DataCaptureAxes::dc, int>,
  std::array{
    IndexInterval<int>::left_bounded(0), IndexInterval<int>::left_bounded(0)},
  DataCapturePointerField>;

/*! partition of FSamplesRegion along channel axis by (control
 * replication shard, data capture instance) indexes
 *
 * channel range for color (shard, dc) given streams_def_t value sd is
 * (sd[shard][dc][0] * fpacket_type::num_channels, sd[shard][dc][0] +
 * sd[shard][dc][1] * fpacket_type::num_channels - 1)
 */
class CapturedFSamplesPartition {

  std::vector<std::vector<std::array<rcp::dc::channel_type, 2>>>
    m_channels_by_shard_and_dc;

  FSamplesRegion::index_partition_t m_ip;

public:
  using md_t = FSamplesRegion;
  using index_space_t = typename md_t::index_space_t;
  using index_partition_t = typename md_t::index_partition_t;
  using color_space_t =
    L::IndexSpaceT<DataCaptureRegionSpec::dim, DataCaptureRegionSpec::coord_t>;

  /*! non-default constructor
   *
   * \param streams
   * \param is index space for FSamplesRegion
   */
  CapturedFSamplesPartition(
    L::Context ctx,
    L::Runtime* rt,
    rcp::dc::channel_type sample_channel_range_offset,
    streams_def_t const& streams_by_shard,
    rcp::dc::channel_type max_dc_per_shard,
    index_space_t is);

  CapturedFSamplesPartition() = default;
  CapturedFSamplesPartition(CapturedFSamplesPartition const&) = default;
  CapturedFSamplesPartition(CapturedFSamplesPartition&&) = default;
  auto
  operator=(CapturedFSamplesPartition const&)
    -> CapturedFSamplesPartition& = default;
  auto
  operator=(CapturedFSamplesPartition&&)
    -> CapturedFSamplesPartition& = default;
  ~CapturedFSamplesPartition() = default;

  [[nodiscard]] auto
  index_partition() const -> index_partition_t;

  [[nodiscard]] auto
  channels_by_shard_and_dc() const
    -> std::vector<std::vector<std::array<rcp::dc::channel_type, 2>>> const&;

  auto
  make_partition(
    L::Context ctx,
    L::Runtime* rt,
    rcp::dc::channel_type sample_channel_range_offset,
    streams_def_t const& streams_by_shard,
    rcp::dc::channel_type max_dc_per_shard,
    index_space_t fsamples_is) -> index_partition_t;
};

/*! task to capture packets from F-engine, encapsulating rcp::dc::DataCapture
 *  thread
 *
 * This task can only be registered dynamically (because of layout constraints
 * on field space)
 */
class CaptureFSamples {

public:
  struct Args {
    StreamClock::external_time t0;
  };

  /*! \brief value type for the elements of the FutureMap returned by a task
   * launch
   */
  using count_t = std::size_t;

  /*! \brief type alias for data capture thread pointer region */
  using dcp_t = DataCaptureRegionSpec;

private:
  L::IndexTaskLauncher m_launcher;

  std::shared_ptr<Args> m_args;

  std::vector<std::vector<char>> m_packet_blocks;

  std::vector<rcp::dc::single_block_memory_resource> m_packet_mrs;

  std::vector<std::unique_ptr<dc_t>> m_dcs;

  std::vector<dc_t*> m_dc_ps;

  DataCaptureRegionSpec::LogicalRegion m_dc_lr;

  CapturedFSamplesPartition m_samples_partition;

  L::IndexPartitionT<2, dcp_t::coord_t> m_dc_ip;

  L::ExternalResources m_dc_ext_resources;

public:
  using md_t = FSamplesRegion;

  CaptureFSamples() = default;

  CaptureFSamples(
    L::Context ctx,
    L::Runtime* rt,
    Configuration const& config,
    std::size_t shard_point,
    streams_def_t const& streams_by_shard,
    md_t::index_space_t index_space,
    L::FieldSpace field_space,
    L::MappingTagID tag = 0);

  CaptureFSamples(CaptureFSamples const&) = delete;

  CaptureFSamples(CaptureFSamples&&) noexcept = default;

  auto
  operator=(CaptureFSamples const&) -> CaptureFSamples& = delete;

  auto
  operator=(CaptureFSamples&&) noexcept -> CaptureFSamples& = default;

  virtual ~CaptureFSamples() = default;

  auto
  partition_size(L::Runtime* rt) const -> unsigned;

#ifndef SYMCAM_RCF_SEND_MULTICAST
  auto
  set_bcast_period(std::chrono::seconds const& period) -> void;
#endif

  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void;

  auto
  launch(
    L::Context ctx,
    L::Runtime* rt,
    md_t::LogicalRegion region,
    StreamClock::external_time const& t0,
    L::Predicate pred = L::Predicate::TRUE_PRED) -> L::FutureMap;

  static auto
  preregister_tasks(L::TaskID task_id) -> void;

  static auto
  register_tasks(L::Runtime* rt, L::TaskID task_id) -> void;
};

/*! type for exception when no matching OFI fabric is found at runtime */
class NoMatchingFabricInterfaceError : public std::runtime_error {
public:
  NoMatchingFabricInterfaceError()
    : std::runtime_error("No matching OFI fabric interface") {}
  NoMatchingFabricInterfaceError(
    NoMatchingFabricInterfaceError const&) noexcept = default;
  NoMatchingFabricInterfaceError(NoMatchingFabricInterfaceError&&) = delete;
  ~NoMatchingFabricInterfaceError() override = default;
  auto
  operator=(NoMatchingFabricInterfaceError const&) noexcept
    -> NoMatchingFabricInterfaceError& = default;
  auto
  operator=(NoMatchingFabricInterfaceError&&)
    -> NoMatchingFabricInterfaceError& = delete;
};

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
