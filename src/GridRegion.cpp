// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "GridRegion.hpp"

using namespace rcp::symcam;

void
GridRegion::attach_params(
  L::Runtime* rt, LogicalRegion region, Params const& params) {

  rt->attach_semantic_information(region, params_tag, &params, sizeof(params));
}

auto
GridRegion::has_params(L::Runtime* rt, LogicalRegion region) -> bool {

  void const* result = nullptr;
  std::size_t size{};
  return rt->retrieve_semantic_information(
    region, params_tag, result, size, true);
}

auto
GridRegion::retrieve_params(L::Runtime* rt, LogicalRegion region)
  -> GridRegion::Params const& {

  void const* result = nullptr;
  std::size_t size{};
  rt->retrieve_semantic_information(region, params_tag, result, size);
  assert(size == sizeof(Params));
  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
  return *reinterpret_cast<Params const*>(result);
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
