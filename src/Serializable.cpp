// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "Serializable.hpp"

#include <ranges>

namespace rcp::symcam {

ChildRequirements::ChildRequirements(
  std::vector<Legion::RegionRequirement> const& region_requirements,
  std::vector<Legion::OutputRequirement> const& output_requirements,
  std::vector<Legion::IndexSpaceRequirement> const& index_space_requirements)
  : m_region_requirements(region_requirements)
  , m_output_requirements(output_requirements)
  , m_index_space_requirements(index_space_requirements) {}

ChildRequirements::ChildRequirements(
  std::vector<Legion::RegionRequirement>&& region_requirements,
  std::vector<Legion::OutputRequirement>&& output_requirements,
  std::vector<Legion::IndexSpaceRequirement>&&
    index_space_requirements) noexcept
  : m_region_requirements(std::move(region_requirements))
  , m_output_requirements(std::move(output_requirements))
  , m_index_space_requirements(std::move(index_space_requirements)) {}

auto
ChildRequirements::operator+=(ChildRequirements const& rhs)
  -> ChildRequirements& {

  std::ranges::copy(
    rhs.m_region_requirements, std::back_inserter(m_region_requirements));
  std::ranges::copy(
    rhs.m_output_requirements, std::back_inserter(m_output_requirements));
  std::ranges::copy(
    rhs.m_index_space_requirements,
    std::back_inserter(m_index_space_requirements));
  return *this;
}

auto
ChildRequirements::operator+(ChildRequirements const& other) const
  -> ChildRequirements {
  return ChildRequirements(*this) += other;
}

auto
ChildRequirements::region_requirements() const
  -> std::vector<Legion::RegionRequirement> const& {
  return m_region_requirements;
}

auto
ChildRequirements::region_requirements()
  -> std::vector<Legion::RegionRequirement>& {
  return m_region_requirements;
}

auto
ChildRequirements::output_requirements() const
  -> std::vector<Legion::OutputRequirement> const& {
  return m_output_requirements;
}

auto
ChildRequirements::output_requirements()
  -> std::vector<Legion::OutputRequirement>& {
  return m_output_requirements;
}

auto
ChildRequirements::index_space_requirements() const
  -> std::vector<Legion::IndexSpaceRequirement> const& {
  return m_index_space_requirements;
}

auto
ChildRequirements::index_space_requirements()
  -> std::vector<Legion::IndexSpaceRequirement>& {
  return m_index_space_requirements;
}

} // end namespace rcp::symcam

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
