# Design {#design}
[TOC]

## Definitions {#definitions}

- Imaging pipeline (IPL)

  RCP imaging pipeline. Produces images.

- RCF output data stream

  A temporal sequence of data packets from the RCF for a fixed,
  subsequence of voltage data channels corresponding to the packet
  format for data produced by the RCF.

- NC channel

  RCF output data with channel width of 130.2 kHz.

- AC channel

  RCF output data with channel width of 8.138 kHz.

- BC channel

  RCF output data with channel width of 1.017 kHz.

- RCP input data stream

  Identical to RCF output data stream.

- Image

  A single instance of an image produced by the RCP.

- Image class

  A specific type of image produced by the RCP, defined by its image
  parameters, including pixel size, UV cell size, set of RCF channels
  contributing to the image, output channel identifier (channel index
  and width), Stokes product identifier and integration time.

- Image stream

  For each RCP image class, a temporal sequence of images of that
  class.

- WC (wide continuum) image

  An RCP image with channel width 130 MHz, in Stokes I only. The total
  frequency coverage of all WC images spans the full 0.7-2.0 GHz
  bandwidth with 10 images.

- MP (medium polarimetric) image

  An RCP image with channel width 2.604 MHz, in any one of IQUV Stokes
  parameters. Total frequency coverage of all MP images spans the full
  0.7-2.0002 GHz bandwidth with 500 images (in every Stokes parameter).

- NL (narrow line) image

  An RCP image with channel width 130.2 kHz, in Stokes I only. Total
  frequency coverage of all NL images is from 0.7 GHz to 1.42912 GHz
  with 5600 images.

- ZA (zoom A) image

  An RCP image with channel width 8.138 kHz, in Stokes I only. 4192
  channels are produced in a tunable window in the range 1.388 - 1.422
  GHz (approximately).

- ZB (zoom B) image

  An RCP image with channel resolution 1.017 kHz, in Stokes I
  only. 960 channels are produced in a tunable window in the range
  1.41993 – 1.4208 GHz (approximately).

- IPL logical instance

  For each RCP image class, the sequence of processing stages in the
  IPL required to produce an image stream.

- IPL physical instance

  A group of inter-communicating computer processes that implement one
  or more IPL logical instances.

- radio camera processor instance

  A collection of IPL physical instances processing a given subset of
  RCF output data streams.

  In DSA-2000, four RCP instances are defined:
  - RCP-L: NC channels below 1.42912 GHz, producing NL and MP image
    streams
  - RCP-H: NC channels above 1.42912 GHz, producing MP image streams
  - RCP-A: AC channels, producing ZA image streams
  - RCP-B: BC channels, producing ZB image streams

## Architecture

### Channel slice decomposition {#channel-slice-decomposition}

The set of image streams induces a non-disjoint partition of the
channels from the RCF according to the set of RCF voltage data
channels that are required for each of the image streams. Because this
partition is non-disjoint, the RCP output data streams are chunked
across multiple channels, and every RCF output data stream is sent to
only a single network interface in the set of RCP processing nodes, it
is necessary in all cases that every IPL physical instance implements
more than one IPL logical instance. For example, as the MP images each
combine twenty input channels, all of those input channels must arrive
at the network interface for a single IPL physical instance; in
addition, each of the NL images within that span of twenty channels
must be produced by that same physical instance. Thus, in this
example, one IPL physical instance implements twenty-four IPL logical
instances (one MP image for each of four Stokes products, and 20 NL
images in one Stokes product).

As suggested by the preceding example, processing by the RCP subsystem
of data that flow from the RCF subsystem may be constructed to be
fully data-parallel across various simple partitions of the set of
frequency channels. In other words, data processing in the RCP may be
divided on the basis of disjoint subsets of channels, which we call
*slices*, in such a way that each of the slices can be processed
identically and independently of all other slices. Assuming the
existence of a sliced data partition for which the data parallelism
property holds, irrespective of its exact definition, the set of
executable programs comprising the RCP in its entirety may be
sub-divided into units of deployment that correspond to the slices in
such a partition.

As there is no inter-slice dependency for processing, each of these
deployed units of execution (physical instances) may run without
exchanging data with any other unit. Furthermore, since processing of
each of the channel slices is identical, the physical instances may be
executing independent copies of a single program. Thus the RCP
subsystem may be implemented as multiple and independent sets of
executable processes running copies of a single program.

Unfortunately, the decomposition of the RCP as described above depends
on the existence of a sliced channel decomposition with the data
parallelism property, which may not hold in all cases. Nevertheless,
the deployment model as described may still be possible if the
dependencies between slices are sufficiently "weak". The DSA-2000 zoom
band channels may be examples of instances without a slice channel
decomposition, due to dependencies on calibration solutions computed
from channels in the full band. Another way of thinking about these
weak dependencies is on the basis of channels processed by a physical
instance *vs* external inputs to the physical instance: channel slices
correspond to voltage sample frequency channels processed by an
instance, while weak dependencies correspond to external inputs to
processing by an instance. In these cases, a design with a weak
(tolerant) dependence and loose coupling of the zoom bands to the full
band may be sufficient to support the deployment of zoom band physical
instances as if they were independent and identical.

### Physical instances

Note that the term "physical instance" should not be understood to be
implemented by a single executable process in all cases. Rather, a
physical instance may exist as a set of tightly coupled processes that
implement the RCP processing for a single channel slice. Similarly, it
is allowed that a single physical instance span across more than one
node in a compute cluster. It is, of course, possible for a physical
instance to be instantiated as a single process on a single node, but
such a configuration is not required by the design. A goal of the IPL
design is to separate the static design of the code that implements an
imaging pipeline from the instantiation of a radio camera deployed as
a set of processes running on a group of nodes. This principle allows
the design of the software for IPL to proceed independently of details
regarding the runtime configuration of a radio camera instance, which
improves the flexibility and applicability of RCP code.

One important aspect of the implementation of IPL as a set of physical
instances, is that each of the physical instances defines a failure
domain of the IPL. What this means in practice is that if a process in
a physical instance fails, that single physical instance may itself
fail, but the others remain unaffected. Physical instances may be
started, stopped and restarted independently of any other physical
instance in the RCP.

### Imaging pipeline

Every physical instance of IPL implements a pipeline that ingests data
from the RCF in a channel slice, and produces all image streams that
depend on that channel slice. The only image streams that are not
directly created by physical instances are the WC images, which are
instead produced from the MP Stokes I images in a downstream image
processing stage.

For the channels in the RCP-L and RCP-H instances, the number of
channels in a slice will be uniform across the band (except perhaps on
one edge), and the size of a slice should be equal to the number of
channels across which calibration solutions are to be
computed. Depending on the number of channels that a single node is
able to process, the size of the slice will thus determine the number
of nodes needed to implement a pipeline physical instance. Note that
if the slice is smaller than the number of channels that could be
processed by a single node, the architecture allows, but does not
require, either increasing the slice size to the number of channels
processed per node, or assigning multiple slices to every node.

For the channels in the RCP-A and RCP-B instances, the slice size may
be set to any divisor of the number of channels processed per
node. The exact value may be selected by any desired criteria
(performance, likely), but there is no constraint due to calibration,
as it is assumed that calibration solutions will be acquired by the
zoom band physical instances from external sources (*i.e*,
RCP-L). Although the slice size could be one in these channels because
only spectral channel images are created from these instances, that
would likely be a low performing configuration due to the number of
physical instances required over the band.

### symcam

`symcam` is the name of the program that implements the IPL physical
instances. This program is configurable at runtime to support a wide
range of use cases, and is capable of running as a single process, or
as a set of tightly coupled, inter-communicating processes. In generic
terms, an IPL physical instance is an SPMD-style parallel program
running the `symcam` executable. A significant feature of a physical
instance is that every process in the instance has access to data in
all the channels of the slice on which it operates.

The architecture of `symcam` is based on an asynchronous, task-based
programming model with implicit data movement, capable of expressing
both task and data parallelism. The framework that implements the
programming model is "Legion", which is described in more detail
[below](#legion), but simply understanding the model as a tree of
asynchronously executing tasks annotated with explicit data
dependencies operating on logically named data regions and sub-regions
is a sufficiently accurate conceptual model to begin.

Thus `symcam` implements a data processing pipeline constructed of
several stages, beginning with a data capture stage and ending with an
image export stage. In many cases, there is a one-to-one
correspondence between pipeline stages and tasks in the programming
model, although there are some exceptions. This is not to say that
every chunk of data flowing through the pipeline triggers the same set
of tasks -- for example, image export only occurs about once every ten
minutes, while visibilities are gridded once every 1.5 seconds.

## Design

### Major building blocks

#### Legion {#legion}

The `symcam` application is designed around
[Legion](https://legion.stanford.edu), a data-centric, task-based
parallel programming system. Legion provides a scalable,
high-performance framework for creating distributed, parallel
programs, using both task and data parallelism. Programs using Legion
are written as trees of tasks operating on logical, named data
regions, with implicit data movement and a deferred execution
model. Legion-based programs are written at the level of logical task
and data partitions, and with a notation for tasks' data
dependencies. The Legion runtime system is the able to exploit the
parallelism implicit in the program's expression of these dependencies
to efficiently use the resources on a target machine (cluster) to
execute the program. The programming model is based on deferred
execution (a generalization of asynchronous execution), leaving the
details of task scheduling and data movement to the Legion
scheduler. Despite the relative complexity of a program's execution,
running in parallel over many memory address spaces and processors
with potentially asynchronous communication, the program as written
has sequential semantics, which greatly simplifies reasoning about the
code.

The control of program execution at runtime is not entirely left to
the implementation of the Legion scheduler, as users may provide
customized callback functions (in the Legion "mapping interface") that
are executed by the scheduler at various points in the task's
progression through the task execution pipeline. These two aspects of
writing a Legion-based program are entirely orthogonal in their
functionality: the main application program expresses the static,
functional task and data partitioning, while the mapping interface
provides points of optimization that allow a dependence on the machine
configuration at runtime. A valid Legion-based program will run to
completion under any valid implementation of the mapping interface
callbacks, which allows testing of the correctness of an application
independent of the machine on which it executes.

A significant aspect of Legion is the ability of the scheduler to
determine not only when a task will run, but also on which processor
the task will run, and which one of any number of variant task
implementations will run (all such decisions being moderated *via* the
mapping interface). This feature supports the implementation of "leaf
tasks", that is, those at the leaf nodes of the task tree, for various
combinations of processor type, memory type and location, and/or data
layout constraints (among other possible constraints), widening the
scope of optimization under different conditions at runtime. While one
could spend significant effort trying to write code for many possible
runtime configurations *a priori*, a more efficient approach is to
develop new leaf task variants in response to the analysis of
profiling results gathered from tests using target machine
configurations of the greatest interest to the user/developer. The
latter approach supports an efficient development path that largely
decouples the development and optimization of the larger scale program
(*i.e*, non-leaf tasks and mapper callbacks) from the development and
optimization of the low-level computational kernels.

The Legion task interface is based on multi-dimensional (dense or
sparse) arrays of values, associated with requirements for, at a
minimum, access privilege (*e.g*, read, read/write) and region
coherence (*e.g*, exclusive, simultaneous). As described previously,
task implementations (or variants) may additionally prescribe
constraints on regions, such as array layouts, and field offsets and
alignments, among other possibilities. The interface is very generic,
and supports the implementation of leaf tasks in a wide variety of
languages including C++, OpenMP, CUDA, FORTRAN and Python. Another
flavor of leaf task implementation "language" is Kokkos, which we
describe next.

#### Kokkos

The [Kokkos](https://kokkos.github.io) programming model
inter-operates with Legion reasonably well, and is used by `symcam`
extensively. The [Kokkos
abstract](https://kokkos.github.io/about/abstract) describes the
project as follows

> The Kokkos C++ Performance Portability EcoSystem is a production
> level solution for writing modern C++ applications in a hardware
> agnostic way. It is part of the US Department of Energies Exascale
> Project – the leading effort in the US to prepare the HPC community
> for the next generation of super computing platforms. The EcoSystem
> consists of multiple libraries addressing the primary concerns for
> developing and maintaining applications in a portable way.

The implementation of most leaf tasks in `symcam` using Kokkos
provides efficient task variants for both CPU and GPU processors with
minimal effort.

To underline the high performance achievable with kernels written in
C++ using Kokkos, two examples are presented: gridding and visibility
weights computation (correlation of sample flag values). In both of
these examples, we provide qualitative comparisons to the performance
of the tensor-core correlator (TCC) code, which is a highly
specialized CUDA code (adopted from ASTRON) used by `symcam` in the
cross-correlation stage of processing. Currently, the gridding kernel
in `symcam` is implemented using a modified version of the
[HPG](https://gitlab.com/dsa-2000/code/common/hpg) library, itself
implemented using Kokkos. HPG provides high performance gridding on
CUDA devices, is currently being integrated with
[CASA](https://casa.nrao.edu), and is also used for the VLA sky survey
([VLASS](https://public.nrao.edu/vlass)). The kernels implemented for
`symcam` are expected to perform at least as well as the HPG
implementation, and early test results support this
expectation. Another comparison to note is that the time to execute a
gridding kernel is qualitatively similar (*i.e,* not different by
orders of magnitude) to the time required for cross-correlation with
TCC. The second example is the implementation of the visibility
weights task, which performs the cross-correlation of sample
flags/weights to compute visibility weights, and has a simple Kokkos
implementation in `symcam`. Recent profiling test results demonstrate
that the time to execute the cross-correlation of sample flags in the
weights task does not exceed the time to compute the visibilities
themselves using TCC. Acknowledging the higher complexity of TCC
operating on 4+4-bit complex values, the performance of the Kokkos
implementation of the weights task is outstanding, especially given
the ease with which it was implemented.

### symcam

As previously described, `symcam` implements an IPL physical instance
using a pipeline comprising a number of stages, in which the stages
are implemented as tasks. We provide a high-level overview of each of
the `symcam` modules, adding descriptions of configuration parameters
that affect the implementation of the tasks defined by those modules,
as needed. After the high-level overview, we describe how the pipeline
is created from these tasks by `symcam` application.

A notable exception to the one-to-one correspondence of stages and
tasks is the gridding stage, which is implemented using sub-tasks to
increase the degree of parallelism. The single exception to
sequential semantics in the `symcam` code resides in the data capture
stage, which is non-deterministic by its nature, but the regions
associated with captured data retain the normal, sequential semantics
through the remainder of the pipeline.

Note that in the following we use the terms "machine" and "processor"
as used by Legion. "Machine" refers to the complete, distributed set
of processors and memories available to the program. "Processor"
refers to any execution units that are able to execute tasks anywhere
in the machine. Legion associates a processor type with every
processor in the machine, for example CPU or GPU; we will use that
distinction whenever it is relevant in the context.

#### Data parallelism

It is important to understand, as a Legion program, all processes in
an IPL physical instance run the same executable code, and the
`symcam` program logic is independent of the process that is executing
the program. Thus, when a simple task that will execute on a single
processor is launched, although all processes are calling the same
function to launch the task at the level of application code, the
Legion run-time system ensures that the task executes on a single
processor, and that the input and output data are copied, as needed,
so that all processes see the same result. Similarly, in parallel
"index task" launches, data regions are described, along with some
partitions thereof, identically on all processes (calling tasks, to be
precise), as is the function call that launches the index task,
without any distinction between processes.

`symcam` typically expresses user-configurable data parallelism for
tasks through the specification of "block sizes" along various axes of
data array index spaces used by the input and output regions of a
task. A block size B on some axis of an index space partitions that
axis such that block j includes the index values in the range [j * B,
(j + 1) * B) (intersected with the values of the index space on that
axis.) Block sizes of this sort are used to partition the index space
of various task regions, and subsequently, partition the task launch
in a simple, user configurable way.

#### Task parallelism

Task parallelism is achieved in `symcam` through the Legion task
scheduler. This is accomplished at the application level largely
through region requirements, which specify field privilege and
coherence attributes, that are provided to task launches. In addition
to scheduling the execution of tasks on processors in the machine, the
task scheduler ensures that data regions are moved or copied around
the machine, as needed, prior to starting the task, and after
completion of the task to satisfy all region requirements. In this
way, not only is the execution of tasks parallelized (potentially
reordered and/or concurrent), but so is the data movement required for
the tasks to access the data they require, with consistency and
coherence across the machine.

#### Tasks {#Tasks}

Tasks in `symcam` are generally implemented using a similar pattern,
as described here. At the application level, task types are
represented by classes, and the configuration of a task type is
defined through the creation of instances of the class. Task classes
define constructors through which the configuration of tasks
occurs. These constructors often configure the static task parameters,
frequently computing partitions reflecting data parallelism. This
design is adopted to maximize the utility of task definitions under a
wide range of conditions. Task class instances typically contain one
or more Legion task launchers, but these are not are not directly
accessible by users of the class. Instead, these classes define a
`launch()` method for starting run-time task instances. Once
constructed, instances of these classes are used to start tasks
repeatedly, as the pipeline processes incoming data. The `launch()`
method arguments define input and output regions for one run-time
instance of the task. Exceptions to this pattern exist, typically in
cases where only one instance of a task is executed in the lifetime of
an IPL physical instance.

##### CaptureFSamples {#CaptureFSamples}

Acquire channelized voltage sample data. Fill a timestamped,
zero-initialized data region of sample data values and weights with
received, flagged voltage data. Filling stops, and the task returns
the region after any of the following conditions occur:
- all expected data (with the given timestamp, channels, *etc*) are
  received;
- a packet arrives with a timestamp difference greater than the sum of
  the final timestamp of a capture region and the configurable
  "maximum packet arrival skew factor" multiplied by the inter-packet
  timestamp interval;
- a system clock time greater than the sum of the final timestamp of a
  capture region and a configurable "sweep delay" interval has passed.

In addition to filling a region with the sample values, the task
inspects the received data values and compares them with the special
"flagged" value:
- when a received data value is equal to the "flagged" value, the
  associated data value in the region is set to zero;
- when a received data value is not equal to the "flagged" value, the
  data value is copied to the associated data value in the region, and
  the associated weight value in the region is set to one.

Threads are created by `symcam` during initialization to interface
with the network stack asynchronously, outside direct control by the
Legion runtime. The `CaptureFSamples` task provides these threads with
memory regions into which captured data values and weights may be
copied after they are received. To prevent data loss, this design
requires that this task be executed with a region having a given
timestamp prior to the receipt of the first sample to be written into
that region. The current implementation needs further testing under
high-load conditions to ensure that this condition is met
robustly. Modifications and designs that are more tolerant of Legion
task scheduling latency are under consideration. Although the
asynchronous threads introduce non-determinism into the `symcam`
program, the non-determinism remains hidden within `symcam` by the
`CaptureFSamples` task, and the rest of the pipeline maintains the
usual, sequential semantics of a Legion program. `CaptureFSamples`
tasks are always completed, and a sequence of tasks is completed in
the order of their region timestamps as long as the sequence is
launched in time order.

[CaptureFSamples class documentation](#rcp::rcpl::symcam::CaptureFSamples)

##### ComputeBaselines {#ComputeBaselines}

Compute baselines from receiver positions. Executed by `symcam` once,
during initialization.

[ComputeBaselines class documentation](#rcp::rcpl::symcam::ComputeBaselines)

##### ComputeUVW {#ComputeUVW}

Compute UVW values given baselines, frequency channels and a rotation matrix.

[ComputeUVW class documentation](#rcp::rcpl::symcam::ComputeUVW)

##### ComputeVisibilityWeights {#ComputeVisibilityWeights}

Perform cross-correlation on a region of channel weight values. The
number of timestamps in the data region is set by the configurable
"integration factor" parameter, which represents the integration time
for the correlation. This task has only Kokkos implementations for all
processor types, and thus does not have a similar constraint on the
integration factor as does the `CrossCorrelate` task.

[ComputeVisibilityWeights class documentation](#rcp::rcpl::symcam::ComputeVisibilityWeights)

##### CrossCorrelate {#CrossCorrelate}

Perform cross-correlation on a region of channel data values. The
number of timestamps in the data region is set by the configurable
"integration factor" parameter, which represents the integration time
for the correlation. Note that the tensor-core correlator (TCC)
library code is used by this task implementation on CUDA GPUs, which
imposes the restriction that the integration factor must be a multiple
of 32 (for the RCF data format).

[CrossCorrelate class documentation](#rcp::rcpl::symcam::CrossCorrelate)

##### ExportGrid {#ExportGrid}

Export the values of a grid/image instance to a file. This task writes
values for a complete grid/image instance, and the sum-of-weights
values for all the planes (channels and Stokes products) in the
grid/image instance. Typically executed when the full integration time
of an image has been reached.

[ExportGrid class documentation](#rcp::rcpl::symcam::ExportGrid)

##### GetCFValues {#GetCFValues}

Compute the sub-region of the complete CF values region for a set of
UVW values on a sub-region of baselines and frequency channels. The
task is used to select the CF sub-region that has CF values needed for
gridding a given set of visibilities. Primarily computes the values of
the (linearized) "index" dimension of the CF region on a set of
baselines and channels. The "index" dimension value is currently only
a function of W value, but it could be extended to incorporate
additional parameters, such as antenna id or parallactic angle, as
needed. Construction of the launcher initializes the array of all CF
values with [CFInitTask](#rcp::rcpl::symcam::CFInitTask). The task
class that is used to provide the main task functionality is
[CFIndexTask](#rcp::rcpl::symcam::CFIndexTask).

[GetCFValues class documentation](#rcp::rcpl::symcam::GetCFValues)

##### GetConfig {#GetConfig}

Get the configuration for the current `symcam` process. Every `symcam`
configuration parameter can obtain a value through an environment
variable, configuration file variable, and command line
argument. Executed by a single processor, the result is distributed
across the machine to reduce file system I/O.

[GetConfig class documentation](#rcp::rcpl::symcam::GetConfig)

##### GetEpoch {#GetEpoch}

Get the current system clock value, rounded up to the nearest whole
second. Executed by a single processor, the result is distributed
across the machine to avoid system clock differences among
processors. Used to synchronize timestamped data regions across
processes.

[GetEpoch class documentation](#rcp::rcpl::GetEpoch)

##### GetReceiverPositions {#GetReceiverPositions}

Get receiver positions. Executed by `symcam` once, during initialization.

[GetReceiverPositions class documentation](#rcp::rcpl::symcam::GetReceiverPositions)

##### GridVisibilitiesOnImage {#GridVisibilitiesOnImage}

Perform gridding for a set of visibilities on a set of images in some
image class. This task spawns sub-tasks for preparation required for
gridding visibilities, and then the gridding itself. Partitioning of
visibilities for gridding, for example by projection to image channel
and W-plane, may be data dependent in some cases. Spawning the
`GridVisibilitiesOnImage` task from the `symcam` main loop decouples
progress in the main loop (in particular, to run `CaptureFSamples`)
from the progress of gridding. Testing has shown this two-level task
gridding design to be an important optimization, which works well with
the Legion task scheduler.

The `launch()` method may or may not spawn sub-tasks, depending on
whether the gridder configuration `symcam.gr.*.min-num-timesteps`
threshold for the number of buffered visibility sets has been
met. Whenever sub-tasks are launched, grid and weights regions may be
returned from the `launch()` method if the integration time for the
current image has been met after gridding the provided set(s) of
visibilities. Grid and weights region values that are returned are in
the form of bare region handles, not wrapped by a "future". Whenever
grid and weights regions are returned from `launch()`, new regions are
immediately created.

Upon initialization, an instance of the task (launcher) class creates
instances of the task (launcher) classes
[PartitionUVWByTiles](#rcp::rcpl::symcam::PartitionUVWByTiles) and
[PartitionVisibilitiesByTiles](#rcp::rcpl::symcam::PartitionVisibilitiesByTiles).
Both of these instances are forwarded to the
[GridVisibilitiesOnImage](#rcp::rcpl::symcam::GridVisibilitiesOnImage)
task implementation.

The task implementation proceeds as follows:

- Create a launcher for
  [GridVisibilitiesOnTiles](#rcp::rcpl::symcam::GridVisibilitiesOnTiles)
  task.
- Proceeding in batches or subsets of the set of visibilities (of
  limited size no greater than the value of build configuration
  parameter `Symcam_MAX_NUM_GRIDDING_TIMESTEPS`), iterate over the
  following steps:

  - Launch a task using the class
    [PartitionUVWByTiles](#rcp::rcpl::symcam::PartitionUVWByTiles) to
    compute the partitioning of the index space of the UVW value
    region (*i.e*, the index space for frequency channels and
    baselines) by the pre-image of the partitioning of UVW values by
    their mapping onto grid region tiles.

    In its simplest form, this task maps visibility channels to image
    channels. Every image definition in a `symcam` configuration
    implies a mapping from visibility channels to image channels; the
    pre-image of this mapping for every image channel then defines a
    partition of the visibility channels in the image definition.

    More complex mappings, which use a tiled decomposition of a single
    grid plane are supported. The most significant application for a
    tiling of this nature is the use of a single central tile that is
    large enough for all the points on the grid that are updated by
    gridding a set of visibilities, yet is smaller than the full image
    grid size. A properly sized central tile is sufficient for
    gridding any set of visibilities with a known UV extent, and
    reduces the size of memory required for accessing grid values
    during gridding. Use of even more complex tilings is supported,
    and functional; however, one aim of tiling is to implement a
    caching mechanism for grid tiles that further limits memory use on
    the processor used for gridding (GPU), and this is not yet
    implemented.

  - Launch a task using the class
    [PartitionVisibilitiesByTiles](#rcp::rcpl::symcam::PartitionVisibilitiesByTiles)
    to compute the partitioning of the visibilities based on the
    partitioning of the baseline-channel index space just computed.

  - Launch a task to grid visibilities by tiles using the class
    [GridVisibilitiesOnTiles](#rcp::rcpl::symcam::GridVisibilitiesOnTiles). A
    single (partitioned) task launch is sufficient to spawn gridding
    tasks for all tiles (which includes all image channels.)

[GridVisibilitiesOnImage class documentation](#rcp::rcpl::symcam::GridVisibilitiesOnImage)

##### IntegrateVisibilities {#IntegrateVisibilities}

Integrate visibilities (and associated weights) after correlation and
before normalization. Implemented, but under-developed, and not
currently used.

[IntegrateVisibilities class documentation](#rcp::rcpl::symcam::IntegrateVisibilities)

##### LoadCUDALibraries {#LoadCUDALibraries}

Load the tensor-core correlator (TCC) library onto every CUDA
processor in the machine. This task runs once on every CUDA processor
during `symcam` initialization. As the TCC code is compiled
just-in-time, this task requires one or two seconds to complete.

[LoadCUDALibraries class documentation](#rcp::rcpl::symcam::LoadCUDALibraries)

##### NormalizeVisibilities {#NormalizeVisibilities}

Divide every complex integer visibility in a region by its
(correlated) weight value, creating a single precision complex
floating point normalized visibility.

[NormalizeVisibilities class documentation](#rcp::rcpl::symcam::NormalizeVisibilities)

##### ReportFillFraction {#ReportFillFraction}

Periodically log the sample region fill fraction value. The
configurable parameter "minimum fill report" sets the approximate
period at which the report is generated.

[ReportFillFraction class documentation](#rcp::rcpl::symcam::ReportFillFraction)

##### TrackDeadCycles {#TrackDeadCycles}

Count the number of consecutive empty sample regions returned from the
`CaptureFSamples` task in the main loop of `symcam`. The configurable
parameter "maximum dead cycles" can be used to set an exit condition
for `symcam` using the count of consecutive empty sample regions.

[TrackDeadCycles class documentation](#rcp::rcpl::symcam::TrackDeadCycles)

#### Configuration

Configuration of `symcam` includes both build-time and run-time
parameters. Note that some of the build-time parameters are acquired
from the configuration of the `librcp` library dependency.

Run-time configuration parameters may be set from a configuration
file, environment variables or command line parameters. The
(ascending) priority order of every run-time parameter value is 1)
configuration file, 2) environment variable, and 3) command line
parameter (of course the name of the configuration file cannot itself
be obtained from a configuration file).

A complete listing of run-time configuration parameters may be printed
with the `--help` command line option. Command line options also exist
to write a new configuration file, or update an existing configuration
file according to parameter values determined by the invoked instance
of `symcam`. Finally, the complete set of configuration parameters,
including build-time parameters, run-time parameters and dynamically
computed parameters may be printed with the `--show` or `--dry-run`
command line options.

Note that the sets of gridding configurations and images are
extensible. The default gridding configuration and image names need
not be maintained. Some quirks currently exist around parsing of these
names and their associated configuration parameters; although a user
is free to try alternative approaches, it is recommended that most or
all of the gridding, image and imaging parameters be set through a
configuration file rather than environment variables or command line
options.

#### Imaging pipeline design

To begin the description of the design of `symcam`, we present the
imaging pipeline design diagram below. The following diagram
represents the data flow or data processing pipeline portion of the
design, including only those tasks that are invoked in the the main
loop of the program. Note that the main loop iterates over a sequence
of blocks of voltage sample data and weights, but there is not a
one-to-one correspondence between sample data blocks and pipeline
stages or tasks in all cases.

![Data processing pipeline](@ref https://static.structurizr.com/workspace/82100/diagrams/IPL-component.png){width=75%}
![Diagram key](@ref https://static.structurizr.com/workspace/82100/diagrams/IPL-component-key.png){width=25%}

Interactive, continually updated versions of the above diagrams can be
found on the DSA-2000 software architecture [documentation web
site](https://structurizr.com/share/82100).

Processing outline:

- Read configuration. [GetConfig](#GetConfig) task is launched, and
  the application waits until that task is completed.

- Compute distribution of RCP input streams to Legion program shards
  and data capture threads. An IPL physical instance is (potentially)
  a multi-process program with RCF output streams assigned to constant
  network interfaces in the physical instance. With this design, it is
  imperative that the sub-regions of a data capture region partitioned
  by streams are assigned to tasks that are local to the data capture
  thread that is expected to receive the data in that stream. For
  `symcam` to implement this design, it is required to use the
  "control-replication" branch of Legion. In the control-replication
  branch of Legion, the processes spawned at the initiation of a
  program, in this case processes in an IPL physical instance, are
  referred to as shards.

- Load tensor-core correlator
  code. [LoadCUDALibraries](#LoadCUDALibraries) is launched, and the
  application waits until that task is completed.

- Create region generator instances. "Region generator" classes are
  implemented by `symcam` for the creation of named regions for data
  values in the pipeline. This is a simple mechanism for wrapping code
  that creates a sequence of regions for values of a specific type, in
  a specific shape, as determined by the current configuration.

- Instantiate task class instances (as described in [Tasks](#Tasks))
  with the current configuration.

- Get receiver positions. Launch
  [GetReceiverPositions](#GetReceiverPositions) task.

- Compute baselines. Launch [ComputeBaselines](#ComputeBaselines)
  task.

- Get timestamp of initial sample to process. Launch
  [GetEpoch](#GetEpoch) task, and the application waits until the task
  is completed. Every process adds a small (common) value to the
  timestamp value returned from the task, which then defines the
  epoch.

- Main loop. Continue until the [TrackDeadCycles](#TrackDeadCycles)
  condition returned from capture tasks is asserted. Deferred
  execution of tasks makes the testing of the exit condition, and the
  post-exit processing a bit more complicated than might be initially
  understood. First, deferred execution allows main loop iterations to
  proceed well ahead of the completion of tasks spawned in the main
  loop body, staring with the capture tasks. This feature requires
  that the "futures" returned by the capture tasks be maintained in a
  queue, and their values tested for the "dead cycles" condition only
  after their completion, which will normally occur after many
  subsequent iterations. Second, after the loop exits, it is also
  necessary to cancel or bypass the tasks of all previously issued
  task launches that have not started executing. Bypassing task
  execution is implemented through the use of Legion task
  predicates. Such predicate values are created for each iteration of
  the pipeline, to be provided to the task launches in the current
  iteration. The predicates created at the beginning of a loop
  iteration are the conjunction of the previous iteration's predicate
  and the value of the "dead cycles" condition for the current
  iteration. This design allows the main loop iteration to be
  responsive to real-time events (*i.e*, absence of input data
  stream), while continuing to provide task launches to the Legion
  task scheduler well in advance of the arrival of input data for
  every timestamp value.

  Next, we outline the tasks launched directly in the `symcam` main
  loop. Note that all of these tasks are predicated on the "live"
  condition, as described previously.

  - Launch a data capture task, [CaptureFSamples](#CaptureFSamples),
    for the next timestamp. Capture regions are sized to:

    - gather a sufficiently large number of data packet samples to
      limit the rate at which these regions are created and these
      tasks are completed, and
    - contain a multiple of the number of time steps required for a
      single visibility, and
    - contain a multiple of the time step tile size required by the
      tensor-core correlator library.

    Currently, there is a limited number of logical regions created
    for data capture; regions that are reused cyclically by the
    capture region generator (and thus by the main loop). While this
    design is intended to limit the size of the working set for data
    capture, it is not a robust solution. Such functionality properly
    belongs in the implementation of a Legion mapper class for
    `symcam`, where control of the size of the working set can be
    implemented directly --- work that is planned for the near future.

  - Create a region for visibilities. Size of region (number of
    visibility time steps) is dependent upon the size of the capture
    region; for example, one capture region may contain sufficient
    samples for multiple visibility time steps.

  - Launch task for correlation of samples,
    [CrossCorrelate](#CrossCorrelate).

  - Launch task for correlation of sample weights,
    [ComputeVisibilityWeights](#ComputeVisibilityWeights).

  - Launch task to compute baseline UVW coordinates,
    [ComputeUVW](#ComputeUVW), based on field center coordinates for
    the visibilities timestamp.

  - Launch task to normalize visibilities,
    [NormalizeVisibilities](#NormalizeVisibilities).

  - For each image definition, execute the following steps. Image
    definitions are provided by the `symcam` configuration; for
    example, in RCP-L, there are two image definitions, MP and NL.

    - Launch task to associate gridding convolution function kernel
      index values with visibilities, [GetCFValues](#GetCFValues).

    - Launch imager task,
      [GridVisibilitiesOnImage](#GridVisibilitiesOnImage). Imager task
      launches return a result value that contains an optional image
      grid (post-FFT), where the optional value is only present when
      the integration time for a single image has been
      exceeded. Imager tasks proceed as described in
      [GridVisibilitiesOnImage](#GridVisibilitiesOnImage).

    - Launch tasks to export completed image grids,
      [ExportGrid](#ExportGrid), whenever the optional value returned
      by an [GridVisibilitiesOnImage](#GridVisibilitiesOnImage) task
      launch is present.

  - Launch data capture fill reporter task,
    [ReportFillFraction](#ReportFillFraction). Occasionally (as
    configured) this task logs the data capture fill fraction.

- Flush pipeline. This step is required to release any resources
  (regions) that are being maintained in the pipeline pending
  otherwise absent completion conditions, the primary example being
  allocated but incomplete image grids.

#### Events {#Events}

In order for `symcam` to do real-time processing of data streams from
RCF, it must additionally acquire live metadata from MNC, and also MNC
control inputs. Metadata and control inputs occur asynchronously with
respect to the input data flow, but will generally be associated with
"activation" times, which can be referenced to timestamps in the input
data stream in order to synchronize the metadata and control inputs
with the data stream. Because events are created by external processes
and are delivered *via* a network, they must be received by `symcam`
processes asynchronously. On the other hand, the `symcam` main loop
issues deferred-task launches, generally well ahead of the data
processing pipeline execution. Synchronization of the asynchronous
metadata and control event stream with the deferred execution data
processing stream is achieved using an *event queue*.

The event queue maintains both a timestamped initial state, the
*auxiliary state*, together with an ordered sequence of timestamped
events. The auxiliary state represents the totality of the metadata
values and control values known to `symcam` at some given timestamp
value. Every event can be applied to an auxiliary state value to
produce another auxiliary state value. To obtain the auxiliary state
at some time "t" that is later than the initial auxiliary state
timestamp, the sequence of events with timestamps preceding "t" is
used to produce a sequence of auxiliary states, the final value of
which will be the auxiliary state at time "t".

The metadata and control inputs are received by a concurrent processor
thread running outside the control of the Legion runtime. Received
inputs are transformed into events, which are then inserted into a
queue ordered by the events' activation times. If there are cases in
which inputs do not carry an activation time, then the receiving
thread should assign an activation time to those events according to
some explicit, fixed protocol. If such a protocol is needed, it should
not use the system clock of the receiving computer; all times are
referenced to timestamps in the input data stream, and there should be
no assumptions by the event handler of any relation between data
stream timestamps and the system clock.

In general, throughout `symcam`, times are referenced to timestamps
associated with data in the pipeline, not the system clock. This rule
removes the need to synchronize the system clock time and the DSA-2000
time reference, and supports a deterministic data processing flow. The
only exception to this rule is in the data capture stage of the
pipeline, where the system clock must be used to ensure that data
capture tasks are completed in the absence of timely incoming data
packets. The effective time used to enact flushing of uncompleted data
capture tasks is offset from the system clock by a configurable amount
("sweep delay", see [CaptureFSamples](#CaptureFSamples)), which is
recommended to have a value on the order of a second or two, well
outside the expected accuracy of the system clock relative to the
external time reference.

An auxiliary state for any timestamp may be computed, as needed, by
the `symcam` main loop. In order that the event queue does not grow in
size indefinitely, it can be "rolled up" to a later timestamp value,
which works by replacing the auxiliary state with the state that
results from the application of the events up to the later time, in
sequence. Whenever an auxiliary state is computed, that auxiliary
state will become the new initial state maintained by the event
queue. This allows all events preceding the new initial state to be
released by the event queue, keeping its total size in check. Note
that updating the initial state in the event queue under this design
generally suggests that auxiliary states be computed in increasing
time order, in order to avoid losing access to accurate states at
prior times. The `symcam` main loop is designed to compute auxiliary
states in time order. The implementation of the `symcam` event queue
uses the [StreamEventLog class](#rcp::rcpl::symcam::StreamEventLog).

Upon completion of [CaptureFSamples](#CaptureFSamples) tasks, a
sequence of one or more auxiliary states is computed at various
timestamp values. The timestamp values in the sequence are determined
by the times for which the auxiliary state will be needed by `symcam`
at any point in the pipeline for the timestamped data in the capture
region. To account for latency of metadata and control inputs, it is
likely necessary to compute auxiliary states for a given capture
region upon the completion of a later capture region.

#### Determinism {#Determinism}

The state of a `symcam` process should be knowable by the control
system (or user) for any time based solely on the timestamped control
inputs sent by the control system to `symcam`, together with knowledge
of deterministic rule-based state transitions in `symcam`. The idea
behind this principle is that the state of `symcam` can be determined
externally without resorting to queries or waiting for some event
initiated by `symcam`. In particular, whenever some "mode" or pipeline
state change is commanded, the mode will persist until either:

- another command is received that ends the mode, or
- the mode has a lifetime that is explicitly provided in the original
command, or can be computed according to a deterministic algorithm.

For example, if the control system sends a command for `symcam` to
begin creating an image at time "t" (that is, data stream timestamp
value, to be more precise), either the control system will send
another command to end the integration of the image at time "t + T",
or the value of "T" is known in advance by both the control system and
`symcam`, and `symcam` will stop the integration without an additional
command input. In any case, the determinism requirement extends only
to the commanded or expected state of `symcam` -- exceptional
conditions or states, and real-time monitor point values, may be
non-deterministic.

### symcam mapper

In this section, we describe the design of the Legion Mapper class
used by `symcam`. As of the time of writing, the mapper used by
`symcam` is defined in the `librcpl` library, and is a subclass of the
`Legion::Mapping::DefaultMapper` class, with slight modifications
thereof. We describe here the currently implemented Mapper class in
`librcpl`, which has proven sufficient for the current state of
`symcam` development. We plan to implement a Mapper class specifically
for `symcam`, although much of the design of that class will depend
upon detailed performance analysis of `symcam` as it is being
developed and tested in a variety of contexts.

#### Current mapper {#current-mapper}

The Mapper class defined in `librcpl` has changes relative to the
Legion default Mapper as follows.

- Reuse of grid instances over multiple gridding tasks. Consecutive
  gridding tasks launched for gridding visibilities to a single image
  grid should continue to update values in a common memory region to
  minimize memory usage and copy overhead. As the default mapper does
  not implement reuse of so-called reduction regions, this is a
  capability implemented by the `librcpl` Mapper class.
- Use of collective instances for gridding. Legion collective
  instances are needed whenever tasks gridding to a single image grid
  span multiple GPUs. This usage occurs, for example, with the MP
  images, wherein the data in the twenty channels being averaged are
  resident in multiple GPU memories. The modification to the default
  mapper here is minor: the `librcpl` Mapper only tests the
  "collective instance" mapping tag and selects the task option to
  check for collective regions on that basis.
- Definition of a sharding functor to create an association of input
  streams to processes (*a.k.a*, Legion shards).

#### Future mapper

Control of the size of the working set for `symcam` processes is an
important factor in its design, because the rate at which data flow
through the pipeline limits the amount of buffering that is possible
on any realistically provisioned system. In terms of the Legion
runtime system, the scheduler must respond to back-pressure in the
pipeline to avoid creating too many concurrently resident, large data
regions. `symcam` currently has two configuration parameters,
`symcam.max-capture-regions` and `symcam.max-visibility-regions` which
exist to address this situation by forcing reuse of existing
regions. The implementation is, however, an insufficiently robust
solution for the issue, as it creates a limit on the number of
distinct _logical_, not _physical_, regions. This difference can
become apparent in some cases, (presumably) on the boundaries of the
lifetimes of physical instances, when the number of physical instances
may briefly exceed the number of logical regions. Legion mappers are
designed to support the sorts of decisions needed to limit the size of
a working set, and a robust solution to these issues will be
implemented in a `symcam` mapper.

Other optimizations for `symcam` will be implemented in a mapper class
as they are identified in testing.
