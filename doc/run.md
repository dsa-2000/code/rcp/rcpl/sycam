# Running symcam {#run}
[TOC]

## Introduction

Despite `symcam` being capable of distributed, multi-process
deployment, all testing conducted to this point has been single node,
single GPU, using only a single process.

## Input sources

### no input

It is possible to run `symcam` without any input data flow. Because a
timeout mechanism exists for input data packet capture, without any
input data flow, the `symcam` pipeline will process a stream of
zero-valued, flagged voltage sample data. Two configuration parameters
are especially useful for this use case, `symcam.max-dead-cycles` and
`symcam.dead-cycles-holdoff`, both of which can be used to configure
the period of time that `symcam` continues to run without any input
data flow.

### fsim

The `fsim` simulator can be used to send data packets over the
network, as if the RCF subsystem were sending data. The `symcam`
configuration parameters `symcam.dc.fabric-provider` and
`symcam.dc.multicast-address` must be used to ensure that that `fsim`
and `symcam` processes can communicate and create a stream of data
packets. Little testing has been done to this point with `fsim`.

## Single node, single GPU

In this case, only a single `symcam` process is used, which provides
the simplest possible deployment. The three main run-time
configuration inputs are the shell environment, `symcam` configuration
file, and command line arguments. An example of how to run `symcam`
with this deployment is provided below. Note that there are many
possible ways to configure and run `symcam`, and the following example
presents merely one option.

Note that Legion provides many run time options, several of which
provide to Legion a description of the machine resources available to
the program, and which may be reduced relative to the physical machine
configuration. Other Legion options affect the Legion task scheduler;
the values shown in the example below are not intended to be
definitive or canonical for `symcam`, merely illustrative.

This example derives from a test using a single node with two GPUs,
one of which is made available to `symcam`. In this example there is
no input source to `symcam`.

``` shell
$ export SYMCAM_CONFIG_FILE=/path/to/config/file
$ CUDA_VISIBLE_DEVICES=1 \
  symcam -ll:cpu 2 -ll:gpu 1 -ll:ocpu 1 -ll:othr 4 -ll:onuma 0 \
  -ll:csize 30000 -ll:fsize 19000 -lg:sched 4 -lg:window 256 \
  -level symcam=1 --symcam-dead-cycles-holdoff 30 \
  --symcam.max-dead-cycles 10 --show
```

## Single node, multiple GPUs

No testing with such a `symcam` deployment has been conducted. At this
time, there is an important constraint imposed by Kokkos that must be
considered: because a process using Kokkos can only access a single
GPU, multiple processes must be used for `symcam` in this case. While
this constraint has no effect on the `symcam` source code, at build
time as well as run time the constraint must be taken into account. At
build time, the Legion dependency must be one that has been configured
to use a network communication layer; although the network may not be
used by `symcam` for inter-process communication in this case, it is
required to start and initialize the `symcam` process group. At run
time, the appropriate launcher executable must be used to start the
`symcam` process group.

TODO: how to tell each `symcam` process which GPU to use?

TODO: launch command example

## Multiple node, multiple GPUs

No testing with such a `symcam` deployment has been conducted. In
general, the setup will follow that described in the previous section,
although in this case, `symcam` inter-process communication will occur
over the network. Again, no change to `symcam` source code is needed,
and the ability of a `symcam` executable to run in this deployment
will depend solely on the linked Legion library.

TODO: launch command example
