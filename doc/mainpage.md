# symcam documentation {#mainpage}
[TOC]

Welcome to the documentation for `symcam`, the SYnthesis iMaging
CAMera application software.

Documentation here is divided into two main sections: high level
project documentation, and much lower level source code documentation.

Project documentation:
- [Design](#design)
- [Build](#build)
- [Deployment](#deployment)
- [Run](#run)

Source code documentation is linked in the navigation tree in the
sidebar.

`symcam` software development is a part of the
[DSA-2000](https://deepsynoptic.org/overview) radio telescope project,
made possible by [Caltech](https://www.caltech.edu), [Owens Valley
Radio Observatory](https://www.ovro.caltech.edu), the [National
Science Foundation](https://nsf.org), [Schmidt
Futures](https://schmidtfutures.com), and the
[NanoGrav](https://nanograv.org) collaboration.
