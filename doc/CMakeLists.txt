# Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
# SPDX-License-Identifier: BSD-2-Clause-Patent

# set some doxygen config values
#
set(DOXYGEN_EXCLUDE doc/doxygen-awesome-css doc/templates)
# TODO: add a variable to control these definitions
set(DOXYGEN_PREDEFINED
  RCPL_USE_KOKKOS
  RCPL_USE_CUDA
  RCPL_USE_OPENMP
  RCPL_USE_KOKKOS_SERIAL
  RCPL_USE_KOKKOS_OPENMP
  RCPL_USE_KOKKOS_CUDA)
set(DOXYGEN_EXTRACT_PRIVATE YES)
set(DOXYGEN_EXTRACT_STATIC YES)

set(DOXYGEN_TAGFILES doc/libstdc++.tag=http://gcc.gnu.org/onlinedocs/libstdc++/latest-doxygen)
set(DOXYGEN_GENERATE_TAGFILE "${CMAKE_CURRENT_BINARY_DIR}/html/symcam.tag")

set(DOXYGEN_LAYOUT_FILE doc/DoxygenLayout.xml)
set(DOXYGEN_HTML_HEADER doc/header.html)
set(DOXYGEN_HTML_EXTRA_STYLESHEET doc/doxygen-awesome-css/doxygen-awesome.css
  doc/doxygen-awesome-css/doxygen-awesome-sidebar-only.css
  doc/doxygen-awesome-css/doxygen-awesome-sidebar-only-darkmode-toggle.css)
set(DOXYGEN_HTML_EXTRA_FILES doc/doxygen-awesome-css/doxygen-awesome-darkmode-toggle.js
  doc/doxygen-awesome-css/doxygen-awesome-paragraph-link.js
  doc/doxygen-awesome-css/doxygen-awesome-interactive-toc.js)
set(DOXYGEN_HTML_COLORSTYLE LIGHT)
set(DOXYGEN_DOT_IMAGE_FORMAT svg)
set(DOXYGEN_INTERACTIVE_SVG YES)
set(DOXYGEN_GENERATE_TREEVIEW YES)
set(DOXYGEN_STRIP_FROM_PATH src doc)

# create 'doxygen' target for documentation
#
doxygen_add_docs(
  doxygen
  src doc
  ALL
  WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/..")

install(DIRECTORY
  "${CMAKE_CURRENT_BINARY_DIR}/html/"
  DESTINATION "${CMAKE_INSTALL_DOCDIR}/html")

# target to create PDF version of design documentation suitable for
# DSA-2000 document repository
add_custom_target(d2k25
  COMMAND ./dx2pd.sh > "${CMAKE_CURRENT_BINARY_DIR}/D2k-00025-RCP-DES-vXXXXX.pdf"
  WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
  USES_TERMINAL)
