# Build {#build}
[TOC]

## Releases

Versioned `symcam` source code releases are available on gitlab.com at
http://gitlab.com/dsa-2000/code/rcp/rcpl/symcam/-/releases. Calendar-based
version numbers are currently used for the package. Note that
[Spack](#spack) may be used to download, build and install release
versions, as well.

## CMake

The build system for `symcam` is based on CMake. The build system
enforces dependency version requirements, but makes no assumptions
about the location of dependencies on file systems; simply stated, the
management of dependencies follows the standard, modern CMake pattern
(using `find_package`). The build process relies on dependent package
"config files" that are provided by the installed, dependent packages,
or by "find modules" that are provided by another source (in some
cases, CMake itself.)  Transitive dependencies of `symcam` may provide
their own versions of "find modules" for some of their dependencies,
but `symcam` itself does not use "find modules". Generally speaking,
for the entire tree of `symcam` dependencies, if those dependencies
have been properly installed and made known to CMake, then the
configuration phase of the `symcam` CMake build should simply work,
finding the correct versions of all dependent packages. Any failure to
locate a dependency of the correct version should result in an error
reported during the CMake configuration phase.

`symcam` itself has a number of build-time configuration parameters,
and in some cases important build-time parameters are inherited from
dependencies. An important example of this is the `librcp` library,
which sets parameters defining the shape of the sample data packets
from RCF. In practice this means that one should be careful that the
configuration phase of CMake selects the dependent packages that have
the intended configuration.

Another dependency from which `symcam` inherits some of its
configuration is `legion`. Most significantly, the `legion`
configuration (*via* `librcpl`) determines whether OpenMP, CUDA, HIP
and/or Kokkos support are enabled in `symcam`.

### symcam build-time configuration parameters

- `Symcam_USE_TENSOR_CORE_CORRELATOR`

  Use the tensor-core correlator library for correlation of voltage samples.

- `Symcam_MAX_CORRELATION_REGIONS`

  In post-correlation integration, the maximum number of "regions" per
  integration. The number of visibility timesteps in a "region" is not
  simple, depending on values of run-time configuration
  parameters. This parameter is best left at its default value unless
  warnings are logged by `symcam` at run time. The effect of this
  parameter on total resource usage is insignificant, and leaving the
  value on the high side has no real negative consequences. Also note
  that this parameter may eventually be removed.

- `Symcam_MAX_NUM_GRIDDING_TIMESTEPS`

  The maximum number of visibility timesteps per gridding task
  launch. The gridding task can operate on a set of visibilities over
  one or more timesteps; this parameter sets a static upper limit on
  the number of these timesteps. Note that `symcam` may select a
  number of timesteps per task at run time that is different from, but
  will not exceed, this value.

- `Symcam_CAPTURE_BUFFER_TIMESTEPS_TILE_SIZE`

  Tile size along the timestep axis of data capture regions, where a
  value of 0 indicates no tiling. The value of this parameter must
  satisfy the constraints of the tensor-core correlator library when
  `symcam` is configured to use it. Note that this value is
  independent of the timestep tile size in the RCF data packets
  (`Librcp_PACKET_TIMESTEPS_TILE_SIZE`) -- `symcam` will reorder the
  values in the received arrays as needed -- but a value that matches
  the packet tiling may provide some efficiency in data capture.

- `Symcam_GRID_VALUE_PRECISION`

  The precision of grid pixels used for gridding. Valid choices are
  "float" and "double".

- `Symcam_USE_COMPENSATED_SUMMATION`

  Use compensated summation in accumulating to pixels during gridding.

- `Symcam_USE_INTERMEDIATE_SUM_WEIGHTS_COMPENSATED_SUMMATION`

  Use compensated summation for intermediate (loop-internal)
  sum-of-weights accumulation values.

- `Symcam_FORCE_WIDE_INTERMEDIATE_GRID_PRECISION`

  Require use of grid pixel precision for intermediate (loop-internal)
  grid pixel values. When the value is `OFF`, the precision of
  affected variables may be different from the grid pixel values,
  depending on the precision grid pixels, visibilities and CF kernel
  values.

- `Symcam_ENABLE_PARALLEL_COMPENSATED_SUMMATION`

  Enable extension to Kahan compensated summation algorithm for
  parallel summation.

- `Symcam_BUILD_DOCS`

  Build the documentation.

### librcpl build-time configuration parameters

These are listed here for reference.

- `Librcpl_MAX_STREAMS_PER_DATA_CAPTURE`

  The maximum number of RCF "streams" a data capture instance can
  accept. The actual number of streams per data capture instance in
  `symcam` is configured at run time. This parameter affects the
  (preliminary) communication protocol used for sending the RCP
  destination addresses of RCF streams to the RCF. As this protocol
  has not been finalized, and is currently only in use by `symcam` and
  `fsim`, not RCF, it is defined in `librcpl`, not `librcp`.

### librcp build-time configuration parameters

These are listed here for reference.

- `Librcp_NUM_POLARIZATIONS`

  Number of voltage sample polarizations.

- `Librcp_NUM_RECEIVERS`

  Number of receivers.

- `Librcp_NUM_CHANNELS_PER_PACKET`

  Number of channels per F-engine packet.

- `Librcp_NUM_TIMESTEPS_PER_PACKET`

  Number of timesteps per F-engine packet.

- `Librcp_PACKET_TIMESTEPS_TILE_SIZE`

  F-engine packet tile size on time axis (0: no tiling).

- `Librcp_SAMPLE_INTERVAL_NS`

  F-engine sample interval (ns).

- `Librcp_UVW_VALUE_TYPE`

  Value type for UVW coordinates. Valid choices: "float" and "double".

- `Librcp_FREQUENCY_VALUE_TYPE`

  Value type for channel frequencies. Valid choices: "float" and
  "double".

- `Librcp_CF_VALUE_TYPE`

  Value type for convolution function kernels. Valid choices: "float"
  and "double".

### Legion build-time configuration parameters

Some of the more significant Legion configuration parameters are
listed here for reference. Users should refer to Legion documentation
for complete information. In some cases, constraints imposed by
`symcam` are given below. Note that network-related parameters may be
important depending on the `symcam` use case, but these are not listed
below, as appropriate test cases have not yet been developed.

- `Legion_USE_CUDA`

  Value may be `ON` or `OFF`.

- `Legion_GPU_REDUCTIONS`

  Value must be `ON` whenever `Legion_USE_CUDA=ON`.

- `Legion_CUDA_ARCH`

  Target CUDA architecture.

- `Legion_HIJACK_CUDART`

  May be `ON` or `OFF`, but is recommended to have the opposite value
  of `Legion_USE_Kokkos`.

- `Legion_USE_OpenMP`

  Value may be `ON` or `OFF`.

- `Legion_USE_Kokkos`

  Value may be `ON` or `OFF`, but `ON` is highly recommended.

- `Legion_REDOP_COMPLEX`

  Value must be `ON`

- `Legion_MAX_DIM`

  Value must be at least 6.

## Spack {#spack}

A Spack package exists for `symcam` (and all of its
dependencies). Using Spack to build and install `symcam` may simplify
those tasks, as well as support reproducibility for installed
builds. Note that, through the selection of `symcam` package variants,
Spack can configure, build and install the entire software stack
needed for the selected variants, in many cases greatly simplifying
these processes, and avoiding common configuration and build
errors. The Spack package is available from the [DSA-2000 Spack
package repository](https://gitlab.com/dsa-2000/code/spack), or the
"dsa2000" branch of [this Spack
fork](https://github.com/mpokorny/spack).
