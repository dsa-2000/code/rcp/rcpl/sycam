# Deployment {#deployment}
[TOC]

This section describes in greater detail the configuration of IPL
physical instances in a radio camera instance
(as defined in [design document](@ref design)). The `symcam` process
groups described below are manifestations of IPL physical instances in
a radio camera instance.

## Run-time configuration

An instance of the radio camera processor is deployed as a set of
processes running on some nodes of a cluster. The design of `symcam`
is in the SPMD style, and, with the support of the Legion runtime,
every process in single SPMD process group has transparent access to
the union of frequency channels received by the processes in the
group. In general, process groups may span one or more nodes of a
cluster. We define the `symcam` process groups on the basis of channel
slices, as described in the [design document](@ref design). The
deployment of processes in a radio camera processor instance is
entirely a run-time configuration issue -- the same statically defined
`symcam` executable is used in all cases.

## Failure domain

Because the processes in a group are tightly coupled through their
communication, every such process group is a failure domain of the
radio camera processor instance. Failure of any process in a group
causes a failure of all the processes in the group, but does not cause
a failure of a process in any other group. The size of the channel
slices must always be between the number of visibility frequency
channels needed to produce the images defined by the radio camera
configuration (sixteen in the case of the DSA-2000 RCP-L and RCP-H
instances; one, for the RCP-A and RCP-B instances), and the complete
set of channels in the radio camera processor instance. While it is
possible to define a single process group that spans all channels,
such a configuration is counter-productive to maximizing robustness.

## Robustness

For maximum robustness of the radio camera instance, in light of the
failure domain property of a tightly coupled process group, the size
of the channel slices should be made as small as possible within the
previously stated limits (note that this is not, however, an
architectural constraint). The defining characteristic of a process
group is the number of voltage/visibility frequency channels that the
group accesses to complete its data processing, which in practice will
often be the number of channels required to compute calibration
solutions. Although the number of channels received by a radio camera
processor node is independently configurable, that value does create
another practical lower limit on the size of the channel subsets of a
process group. Note that the application of calibration solutions does
not require access to the visibility data in the channels from which
those solutions were derived, which may reduce the size of process
groups in radio camera instances that do not compute calibration
solutions (relative to those that do compute calibration solutions.)

For DSA-2000 radio camera instances, the following channel slice sizes
are currently planned:
- RCP-L, RCP-H: thirty-two channels, determined by the number of
  channels processed by the GPUs in a node, and the number of channels
  needed to compute calibration solutions;
- RCP-A, RCP-B: forty channels, determined by the number of channels
  processed per by the GPUs in a node.

## Size of process group

This is a technical (implementation) note on the number of processes
in a process group, and it is not essential for understanding the
deployment principles for `symcam` process groups. While the principle
described above -- partitioning the voltage/visibility frequency
channel space, and defining process groups on the basis of the subsets
of that partition -- is key to the design of `symcam`, it does not
directly address the number of processes in each process group. At a
minimum, one process per node that receives voltage data within the
channel range of a partition subset is required, but the
implementation of `symcam` introduces other factors that may increase
that number.

For a Legion program in general it is desirable to run with a single
process per node, which provides access to all resources on that node,
as defined by Legion command line arguments, to the process. However,
Legion programs are not restricted to such deployment configurations,
and it is also possible to run multiple processes of a process group
on every node, without any change to the source code. This capability
is important to `symcam` because of constraints in Kokkos on the
number of GPUs per process it can support: namely, Kokkos supports
only one GPU per process. Thus, a `symcam` process group must be
created with one process per GPU per node rather than one process per
node. To reiterate, this constraint, imposed by Kokkos, has no effect
on the `symcam` source code whatsoever -- in the future, when Kokkos
no longer has this constraint and Legion adapts accordingly, only the
deployment configuration of `symcam` process groups will need to
change.

### Example DSA-2000 deployment

![Imaging pipeline deployment](@ref https://static.structurizr.com/workspace/82100/diagrams/RCP-deployment.png){width=75%}
![Diagram key](@ref https://static.structurizr.com/workspace/82100/diagrams/RCP-deployment-key.png){width=25%}
