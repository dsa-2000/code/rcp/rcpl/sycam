#!/bin/bash
sed "s/@ref https:/https:/g" design.md |\
    sed "s/#rcp::/https:\/\/dsa-2000.gitlab.io\/code\/rcp\/rcpl\/symcam\/classrcp_1_1/g" |\
    sed "s/rcpl::/rcpl_1_1/g" |\
    sed "s/symcam::\([^)]*\)/symcam_1_1\1.html/g" |\
    sed "s/\[TOC\]//g" |\
    cat frontmatter.yml - |\
    pandoc -t pdf -s -V colorlinks -V links-as-notes --number-sections \
           -V logo=templates/pandoc/d2k-logo.png --template templates/pandoc/template.latex
